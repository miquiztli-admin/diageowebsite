﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UploadCSVFromIPad.aspx.cs" Inherits="ExtractData_UploadCSVFromIPad" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>UploadCSVFromIPad</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function ValidateUpload() {
            if (document.getElementById('FileUpload').value == '') {
                alert('คุณยังไม่ได้เลือกไฟล์');
                return false;
            }
            else
                return true;
        }
        function ClearBrowse() {
            document.getElementById('FileUpload').value = '';
//            document.getElementById('ddlBranName').selectIndex = 0;
            return true;
        }
        function DialogConfirm() {
        
            if (document.getElementById('FileUpload').value == '') 
            {
                alert('คุณยังไม่ได้เลือกไฟล์');
                return false;
            }
            
            if (document.getElementById('ddlBranName').value == '0') 
            {
                alert('คุณยังไม่ได้เลือก Brand');
                return false;
            }

            if (confirm('คุญต้องการอัพโหลดข้อมูลไฟล์นี้ ใช่รึไม่?'))
                return true;
            else
                return false;
            }
        
    </script>
    <style type="text/css">
        #FieldSet
        {
            height: 155px;
        }
    </style>
</head>
<body>
    <form id="UploadFile" runat="server">
    <div>
        <fieldset id="FieldSet" runat="server">
        <legend id="HeaderPage" runat="server" class="ntextblack">UploadCSV</legend>
        <br />
        <table align="center" style="vertical-align:middle">
            <tr>
                <td align="center">
                    <asp:Label ID="lblUpload" runat="server" Text="อัพโหลดไฟล์ : " CssClass="ntextblack"></asp:Label>&nbsp;
                    <asp:FileUpload ID="FileUpload" runat="server"/>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <asp:DropDownList ID="ddlBranName" runat="server" Width="120px" CssClass="ntextblack">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnUpload" runat="server" Text="Upload File" CssClass="ntextblack" 
                        Width="120px" OnClientClick="return DialogConfirm();" 
                        onclick="btnUpload_Click" />
                    </td>
            </tr>
        </table>
        </fieldset>
    </div>
    </form>
</body>
</html>