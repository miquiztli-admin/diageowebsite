﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DIAGEO.BLL;
using System.Data;
using System.Collections;

public partial class ExtractData_ProvincePopup : BasePage
{
    #region Property

    public ArrayList ArrProvinceID
    {
        get { return (ArrayList)Session["ArrProvinceID" + TabNumber.ToString()]; }
        set { Session["ArrProvinceID" + TabNumber.ToString()] = value; }
    }

    public ArrayList ArrProvinceName
    {
        get { return (ArrayList)Session["ArrProvinceName" + TabNumber.ToString()]; }
        set { Session["ArrProvinceName" + TabNumber.ToString()] = value; }
    }

    public int TabNumber
    {
        get { return (int)ViewState["TabNumber"]; }
        set { ViewState["TabNumber"] = value; }
    }

    #endregion

    #region Method

    private void BindProvince()
    {
        ProvinceBLL bllProvince = new ProvinceBLL();
        DataTable dtProvince = new DataTable("Province");
        //DataRow dr;
        try
        {
            dtProvince = bllProvince.SelectAllAndOrderByName();

            //dr = dtProvince.NewRow();
            //dr["provinceId"] = 0;
            //dr["name"] = "ไม่ระบุ";
            //dtProvince.Rows.InsertAt(dr, 0);

            dgProvince.DataSource = dtProvince;
            dgProvince.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("BindProvince :: " + ex.Message);
        }
    }

    private void KeepItemProvince()
    {
        int Index;
        try
        {
            if (ArrProvinceID == null)
            {
                ArrProvinceID = new ArrayList();
                ArrProvinceName = new ArrayList();
            }
            for (int i = 0; i < dgProvince.Items.Count; i++)
            {
                CheckBox chk = (CheckBox)dgProvince.Items[i].FindControl("chkProvince");
                if (chk.Checked == true)
                {
                    if (ArrProvinceID.Count == 0)
                    {
                        ArrProvinceID.Add(dgProvince.Items[i].Cells[2].Text.ToString());
                        ArrProvinceName.Add(dgProvince.Items[i].Cells[1].Text.ToString());
                    }
                    else
                    {
                        for (int j = 0; j < ArrProvinceID.Count; j++)
                        {
                            if (dgProvince.Items[i].Cells[2].Text.ToString().Equals(ArrProvinceID[j].ToString()))
                                break;
                            if (j == ArrProvinceID.Count - 1)
                            {
                                ArrProvinceID.Add(dgProvince.Items[i].Cells[2].Text.ToString());
                                ArrProvinceName.Add(dgProvince.Items[i].Cells[1].Text.ToString());
                            }
                        }
                    }
                }
                else
                {
                    if (ArrProvinceID.Count > 0)
                    {
                        Index = -1;
                        for (int j = 0; j < ArrProvinceID.Count; j++)
                        {
                            if (dgProvince.Items[i].Cells[2].Text.ToString().Equals(ArrProvinceID[j].ToString()))
                                Index = j;
                        }
                        if (Index != -1)
                        {
                            ArrProvinceID.RemoveAt(Index);
                            ArrProvinceName.RemoveAt(Index);
                        }
                    }
                }
            }

            //ArrProvinceID = new ArrayList();
            //ArrProvinceName = new ArrayList();
            //for (int i = 0; i < dgProvince.Items.Count; i++)
            //{
            //    CheckBox chk = (CheckBox)dgProvince.Items[i].FindControl("chkProvince");
            //    if (chk.Checked == true)
            //    {
            //        ArrProvinceID.Add(dgProvince.Items[i].Cells[2].Text.ToString());
            //        ArrProvinceName.Add(dgProvince.Items[i].Cells[1].Text.ToString());
            //    }
            //}
        }
        catch (Exception ex)
        {
            throw new Exception("KeepItemProvince :: " + ex.Message);
        }
    }

    private void BindItemProvince()
    {
        try
        {
            if (ArrProvinceID.Count > 0)
            {
                for (int i = 0; i < dgProvince.Items.Count; i++)
                {
                    CheckBox chk = (CheckBox)dgProvince.Items[i].FindControl("chkProvince");
                    for (int j = 0; j < ArrProvinceID.Count; j++)
                    {
                        if (dgProvince.Items[i].Cells[2].Text.ToString().Equals(ArrProvinceID[j].ToString()))
                        {                         
                            chk.Checked = true;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindItemProvince :: " + ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                TabNumber = int.Parse(Request.QueryString["tabNumber"].ToString());
                BindProvince();
                //if (int.Parse(Request.QueryString["item"].ToString()) > 0)
                if (ArrProvinceID != null)
                {
                    //ArrOutletID = (ArrayList)Session["ArrOutletID" + TabNumber.ToString()];
                    BindItemProvince();
                }

                //TabNumber = int.Parse(Request.QueryString["tabNumber"].ToString());
                //BindProvince();

                //if (ArrProvinceID == null)
                //    ArrProvinceID = new ArrayList();
                //if (ArrProvinceID.Count > 0)
                //    BindItemProvince();
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvincePopup.Page_Load :: " + ex.Message);
        }
    }

    protected void chkHeaderProvince_OnCheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox chkHead = (CheckBox)sender;

            for (int i = 0; i < dgProvince.Items.Count; i++)
            {
                CheckBox chk = (CheckBox)dgProvince.Items[i].FindControl("chkProvince");
                chk.Checked = chkHead.Checked;
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvincePopup.chkHeaderProvince_OnCheckedChanged :: " + ex.Message);
        }
    }

    protected void btnSelect_Click(object sender, EventArgs e)
    {
        try
        {
            KeepItemProvince();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "closePopup", "window.close();", true);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvincePopup.btnSelect_Click :: " + ex.Message);
        }
    }

    #endregion
}
