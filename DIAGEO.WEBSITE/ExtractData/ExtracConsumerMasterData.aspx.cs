﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DIAGEO.BLL;
using DIAGEO.COMMON;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Configuration;
using System.Globalization;


public partial class ExtracConsumerMasterData : BasePage
{
    public string RecordOFExport
    {
        get {
            return ViewState["RecordOFExport"].ToString(); 
        }
        set {
            ViewState["RecordOFExport"] = value;
        }
    }
    public int countAddID
    {
        get
        {
            if (ViewState["countAddID"] == null)
                return 0;

            return int.Parse(ViewState["countAddID"].ToString());
        }
        set
        {
            ViewState["countAddID"] = value;
        }
    }
    public string strwhere
    {
        get
        {
            if (ViewState["strwhere"] == null)
                return "";

            return ViewState["strwhere"].ToString();
        }
        set
        {
            ViewState["strwhere"] = value;
        }
    }
    public bool cbCheckAll
    {
        get
        {
            if (ViewState["cbCheckAll"] == null)
                return false;

            return (bool)ViewState["cbCheckAll"];
        }
        set
        {
            ViewState["cbCheckAll"] = value;
        }
    }
    public DataTable dtColumnName
    {
        get
        {
            if (ViewState["dtColumnName"] == null)
                return new DataTable();

            return (DataTable)ViewState["dtColumnName"];
        }
        set
        {
            ViewState["dtColumnName"] = value;
        }
    }
    public DataTable dtCondition
    {
        get
        {
            if (ViewState["dtCondition"] == null)
                return new DataTable();

            return (DataTable)ViewState["dtCondition"];
        }
        set
        {
            ViewState["dtCondition"] = value;
        }
    }


    private void HideConditionDiv()
    {
        ddl2.Visible = false;
        ddl3.Visible = false;
        ddl4.Visible = false;
        ddl5.Visible = false;
        ddl6.Visible = false;
        ddl7.Visible = false;
    }

    private void BindData()
    {
        OutletBLL bllOutlet;
        ProvinceBLL bllProvince;
        ConsumerBLL bllConsumer;
        try
        {
            bllOutlet = new OutletBLL();
            bllProvince = new ProvinceBLL();
            bllConsumer = new ConsumerBLL();
            DataTable dtOutlet = new DataTable();
            DataTable dtProvince = new DataTable();
            DataTable dt = new DataTable();
            dtOutlet = bllOutlet.SelectActiveOutletOrderByName("T");
            dtProvince = bllProvince.SelectAllAndOrderByName();
            dtColumnName = bllConsumer.SelectColumnVExtract();
            if (dtOutlet.Rows.Count > 0)
            {
                
                ddlOutlet.DataSource = dtOutlet;
                ddlOutlet.DataTextField = "name";
                ddlOutlet.DataValueField = "outletId";
                ddlOutlet.DataBind();
                ddlOutlet.Items.Insert(0, new ListItem("Outlet", "0"));

                ddlOutlet2.DataSource = dtOutlet;
                ddlOutlet2.DataTextField = "name";
                ddlOutlet2.DataValueField = "outletId";
                ddlOutlet2.DataBind();
                ddlOutlet2.Items.Insert(0, new ListItem("Outlet", "0"));

                ddlRecentOutlet.DataSource = dtOutlet;
                ddlRecentOutlet.DataTextField = "name";
                ddlRecentOutlet.DataValueField = "outletId";
                ddlRecentOutlet.DataBind();
                ddlRecentOutlet.Items.Insert(0, new ListItem("All", "0"));
            }
            if (dtProvince.Rows.Count > 0)
            {
                ddlProvince.DataSource = dtProvince;
                ddlProvince.DataTextField = "name";
                ddlProvince.DataValueField = "provinceId";
                ddlProvince.DataBind();
                ddlProvince.Items.Insert(0, new ListItem("Unknown", "0"));
            }
            if (dtColumnName.Rows.Count > 0)
            {
                dgSearch.DataSource = dtColumnName;
                dgSearch.DataBind();
            }
            dgCriteria.DataSource = dt;
            dgCriteria.DataBind();

            cbAllField.Checked = cbCheckAll;
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtracConsumerMasterData.BindData :: " + ex.Message);
        }

    }
    private void ClearData()
    {
        try
        {
            ddlGender.SelectedIndex = 0;
            ddlOutlet.SelectedIndex = 0;
            ddlProvince.SelectedIndex = 0;
            ddlRecentOutlet.SelectedIndex = 0;
            txtActivePeriodFrom.Text = "";
            txtActivePeriodTo.Text = "";
            txtDateBirthFrom.Text = "";
            txtDateBirthTo.Text = "";
            txtFrequencyFrom.Text = "";
            txtFrequencyTo.Text = "";
            txtRangeFrom.Text = "";
            txtRangeTo.Text = "";
        }
        catch (Exception ex)
        {
            throw new Exception("ClearData::" + ex.Message);
        }
    }

    private string CreateQuery(string ColumnName,bool count)
    {
        try
        {
            string queryString = "";
            if (count)
            {
                queryString = "select count(consumerId)as consumerId FROM dbo.V_ExtractConsumer ";
            }
            else
            {
                if (ColumnName == "")
                    queryString = "select * FROM dbo.V_ExtractConsumer ";
                else
                    queryString = "select " + ColumnName + " FROM dbo.V_ExtractConsumer ";
            }
            string style = "";
            if ((rdlExport.SelectedValue == "All" || rdlExport.SelectedValue == "Restricted") && rdlMatch.SelectedValue == "Match all")
                style = "style1";
            else if (rdlExport.SelectedValue == "All" && rdlMatch.SelectedValue == "Match any")
                style = "style2";
            else if (rdlExport.SelectedValue == "Restricted" && rdlMatch.SelectedValue == "Match any")
                style = "style3";
            strwhere = "";
            string queryStr = "";
            foreach (DataRow dr in dtCondition.Rows)
            {
                queryStr = QueryCondition(queryString, style, dr);
            }

            if (queryStr == "")
            {
                if (count)
                {
                    queryStr = "select count(consumerId)as consumerId FROM dbo.V_ExtractConsumer ";
                }
                else
                {
                    if (ColumnName == "")
                        queryStr = "select * FROM dbo.V_ExtractConsumer ";
                    else
                        queryStr = "select " + ColumnName + " FROM dbo.V_ExtractConsumer ";
                }
            }
            return queryStr;
        }
        catch (Exception ex)
        {
            throw new Exception("CreateCondition::" + ex.Message);
        }
    }
    private string QueryCondition(string queryStr, string style ,DataRow dr)
    {
        try
        {
            string ReturnQuery = "";
            if (style == "style1")
            {
                if (strwhere == "")
                    strwhere += "where consumerId in " + GetCondition(dr["Type"].ToString(), dr , 1);
                else
                    strwhere += " and consumerId in " + GetCondition(dr["Type"].ToString(), dr, 1);
            }
            else if (style == "style2")
            {
                if (strwhere == "")
                    strwhere += " where " + GetCondition(dr["Type"].ToString(), dr, 2);
                else
                    strwhere += " union all " + queryStr + " where " + GetCondition(dr["Type"].ToString(), dr, 2);
            }
            else if (style == "style3")
            {
                if (strwhere == "")
                    strwhere += " where " + GetCondition(dr["Type"].ToString(), dr, 2);
                else
                    strwhere += " union " + queryStr + " where " + GetCondition(dr["Type"].ToString(), dr, 2);
            }
            ReturnQuery = queryStr + strwhere;
            return ReturnQuery;
        }
        catch (Exception ex)
        {
            throw new Exception("QueryCondition::" + ex.Message);
        }
    }
    private string GetCondition(string type, DataRow dr, int style)
    {
        try
        {
            string ReturnStr = "";
            if (type == "Register Outlet")
            {
                string OutletID = dr["ValueData"].ToString();
                if (style == 1)
                    ReturnStr = "(select consumerId from consumer where consumer.registerOutletId = " + OutletID + ") ";
                else
                    ReturnStr = " V_ExtractConsumer.registerOutletId = " + OutletID + " ";
            }
            else if (type == "Repeat Outlet")
            {
                string OutletID = dr["ValueData"].ToString();
                string NumFrom = dr["RangeFrom"].ToString();
                string NumTo = dr["RangeTo"].ToString();
                string Recently = dr["DateFrom"].ToString();
                string RecentlyData = dr["RecentlyData"].ToString();
                if (style == 1)
                {
                    ReturnStr = "(select Consumer.consumerId  from consumer " +
                                    " inner join PlatformMember on consumer.consumerId = PlatformMember.consumerId and PlatformMember.platformId = 2 " +
                                    " inner join SmartTouchDB..ST_tsRepeat on PlatformMember.memberCode = ST_tsRepeat.Repeat_Consumer " +
                                    " where ST_tsRepeat.Repeat_Outlet = " + OutletID + " ";
                    if (Recently != "")
                    {
                        ReturnStr = ReturnStr + " and consumer.lastVisitOutletId = " + RecentlyData + " ";
                    }
                    ReturnStr = ReturnStr + " group by Consumer.consumerId having count(consumer.consumerId) between " + NumFrom + " and " + NumTo + " )";
                }
                else
                {
                    ReturnStr = " consumerId in ";
                    ReturnStr += "(select Consumer.consumerId from consumer " +
                                    " inner join PlatformMember on consumer.consumerId = PlatformMember.consumerId and PlatformMember.platformId = 2 " +
                                    " inner join SmartTouchDB..ST_tsRepeat on PlatformMember.memberCode = ST_tsRepeat.Repeat_Consumer " +
                                    " where ST_tsRepeat.Repeat_Outlet = " + OutletID + " ";
                    if (Recently != "")
                    {
                        ReturnStr = ReturnStr + " and consumer.lastVisitOutletId = " + RecentlyData + " ";
                    }
                    ReturnStr += " group by Consumer.consumerId having count(consumer.consumerId) between " + NumFrom + " and " + NumTo + " )";
                }
            }
            else if (type == "Age")
            {
                string AgeFrom = dr["RangeFrom"].ToString();
                string AgeTo = dr["RangeTo"].ToString();
                if (style == 1)
                    ReturnStr = "(select consumerId from consumer where DATEDIFF(YEAR,consumer.dateOfBirth,GETDATE()) between " + AgeFrom + " and " + AgeTo + " ) ";
                else
                    ReturnStr = " DATEDIFF(YEAR,V_ExtractConsumer.dateOfBirth,GETDATE()) between " + AgeFrom + " and " + AgeTo + " ";
            }
            else if (type == "Gender")
            {
                string gender = dr["ValueData"].ToString();
                if (style == 1)
                    ReturnStr = "( select consumerId from consumer where consumer.gender = '" + gender + "' ) ";
                else
                    ReturnStr = " V_ExtractConsumer.gender = '" + gender + "' ";
            }
            else if (type == "Date of Birth")
            {
                string BODFrom = dr["DateFrom"].ToString();
                string BODTo = dr["DateTo"].ToString();
                if (style == 1)
                    ReturnStr = "(select consumerId from consumer where Convert(int,Replace(STR(MONTH(consumer.dateOfBirth),2),' ','0')+Replace(STR(DAY(consumer.dateOfBirth),2),' ','0')) " +
                                " between Convert(int,Replace(STR(MONTH(Convert(datetime,'" + BODFrom + "',103)),2),' ','0')+ Replace(STR(DAY(Convert(datetime,'" + BODFrom + "',103)),2),' ','0')) " +
                                " and  Convert(int,Replace(STR(MONTH(Convert(datetime,'" + BODTo + "',103)),2),' ','0')+Replace(STR(MONTH(Convert(datetime,'" + BODTo + "',103)),2),' ','0')) )";
                else
                    ReturnStr = " Convert(int,Replace(STR(MONTH(V_ExtractConsumer.dateOfBirth),2),' ','0')+Replace(STR(DAY(V_ExtractConsumer.dateOfBirth),2),' ','0')) " +
                                " between Convert(int,Replace(STR(MONTH(Convert(datetime,'" + BODFrom + "',103)),2),' ','0')+ Replace(STR(DAY(Convert(datetime,'" + BODFrom + "',103)),2),' ','0')) " +
                                " and  Convert(int,Replace(STR(MONTH(Convert(datetime,'" + BODTo + "',103)),2),' ','0')+Replace(STR(MONTH(Convert(datetime,'" + BODTo + "',103)),2),' ','0')) )";

            }
            else if (type == "Province")
            {
                string Province = dr["Value"].ToString();
                if (style == 1)
                    ReturnStr = "( select consumerId from consumer where consumer.homeProvince = '" + Province + "' ) ";
                else
                    ReturnStr = " V_ExtractConsumer.homeProvince = '" + Province + "' ";
            }
            else if (type == "Active Period")
            {
                string APDFrom = dr["DateFrom"].ToString();
                string APDTo = dr["DateTo"].ToString();
                if (style == 1)
                    ReturnStr = "(select consumerId from consumer where consumer.updateDate between '" + APDFrom + "' and '" + APDTo + "') ";
                else
                    ReturnStr = " V_ExtractConsumer.updateDate between '" + APDFrom + "' and '" + APDTo + "' ";
            }
            return ReturnStr;
        }
        catch (Exception ex)
        {
            throw new Exception("GetCondition::" + ex.Message);
        }
    }
    private DataTable GetConsumerDataByCondition(string queryStr,int startRow , int MaxRow)
    {
        try
        {
            DataTable dtReturnData = new DataTable();
            string timeout = ConfigurationSettings.AppSettings["ConnectionTimeout"].ToString();
            string ConnectionString = ConfigurationSettings.AppSettings["Main.ConnectionString"].ToString() + timeout;
            SqlConnection Conn = new SqlConnection(ConnectionString);
            Conn.Open();
            queryStr = "select * FROM ( SELECT ROW_NUMBER() OVER (ORDER BY consumerId) AS RowRank ,* From ( " +
                queryStr +
                " ) AS MSAGENTMASTER  ) AS MSAGENTMASTERWITHROWNUMBER WHERE   RowRank > "
                + startRow.ToString() + "  AND RowRank <= (" + startRow.ToString() + " + " + MaxRow + ")";
            SqlDataAdapter DA = new SqlDataAdapter(queryStr, Conn);
            DA.Fill(dtReturnData);
            if (Conn.State == ConnectionState.Open)
            {
                Conn.Close();
            }

            return dtReturnData;
        }
        catch (Exception ex)
        {
            throw new Exception("GetConsumerDataByCondition::" + ex.Message);
        }
    }
    private bool CreateCSVFile(DataTable dt, string strFilePath)
    {
        try
        {
            string directory =  strFilePath;
            StringBuilder sb = new StringBuilder();

            sb = new StringBuilder();
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                sb.Append(dt.Columns[i].ColumnName);
                sb.Append(i == dt.Columns.Count - 1 ? "\n" : "|");
            }
            foreach (DataRow row in dt.Rows)
            {
                foreach (DataColumn column in dt.Columns)
                {
                    sb.Append(row[column.ColumnName].ToString() + '|');
                }
                sb.Remove(sb.Length - 1, 1);
                sb.Append(Environment.NewLine);
            }

            File.AppendAllText(directory, sb.ToString(), Encoding.UTF8);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
        }

        return true;
    }
    public void FileStreamDownload(string filePath, string newFileNameDownload, bool deleteFile)
    {
        try
        {
            FileStream liveStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[liveStream.Length + 1];

            liveStream.Read(buffer, 0, (int)liveStream.Length);
            liveStream.Close();
            HttpContext.Current.Response.Clear();

            Response.ContentType = "application/octet-stream";
            Response.ContentEncoding = Encoding.UTF8;
            Response.AddHeader("Content-Length", buffer.Length.ToString());
            Response.AddHeader("Content-Disposition", "attachment; filename=" + newFileNameDownload);
            Response.BinaryWrite(buffer);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            throw new Exception("FileStreamDownload :: " + ex.Message);
        }
        finally
        {
            if (deleteFile)
                File.Delete(filePath);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "txtDateBirthFrom", "Javascript:getdatecontrol1('txtDateBirthFrom');", true);
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "txtDateBirthTo", "Javascript:getdatecontrol1('txtDateBirthTo');", true);
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "txtActivePeriodFrom", "Javascript:getdatecontrol1('txtActivePeriodFrom');", true);
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "txtActivePeriodTo", "Javascript:getdatecontrol1('txtActivePeriodTo');", true);

        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.btnExport);
        
        try
        {
            if (!Page.IsPostBack)
            {
                BindData();
                HideConditionDiv();
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtracConsumerMasterData.Page_Load :: " + ex.Message);
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        ConsumerBLL consumerBLL;
        DataTable dtConsumer = null;
        try
        {
            consumerBLL = new ConsumerBLL();
            //for test
            //TextBox1.Text = "";
            string ColumnName = "";
            foreach (DataGridItem di in dgSearch.Items)
            {
                HtmlInputCheckBox chkBx = (HtmlInputCheckBox)di.FindControl("cbField");
                if (chkBx != null && chkBx.Checked)
                {
                    Label lbl = (Label)di.FindControl("lbColumnName");
                    //TextBox1.Text += lbl.Text + ", ";
                    ColumnName += lbl.Text + ", ";
                }
            }
            //for test
            string FileName = "Consumer_" + DateTime.Now.ToString("ddMMyyyyHHmmssss") + ".csv";
            //dtConsumer = consumerBLL.SelectConsumerData();
            if (ColumnName.Length > 2)
                ColumnName = ColumnName.Remove(ColumnName.Length - 2);
            string query = CreateQuery(ColumnName,true);
            dtConsumer = GetConsumerDataByCondition(query, 0, 1);
            RecordOFExport = dtConsumer.Rows[0][1].ToString();
            query = CreateQuery(ColumnName, false);
            decimal round = Convert.ToDecimal(RecordOFExport) / Convert.ToDecimal(10000);
            round = decimal.Ceiling(round);
            for (int i = 1; i <= round; i++)
            {
                if (i == 1)
                {
                    dtConsumer = GetConsumerDataByCondition(query,0,10000);
                }
                else
                {
                    dtConsumer = GetConsumerDataByCondition(query,(i - 1) * 10000, 10000);
                } 
                CreateCSVFile(dtConsumer, Server.MapPath("~//FileUpload//ExtractData//") + "\\" + FileName);
            }
            FileStreamDownload(Server.MapPath("~//FileUpload//ExtractData//") + FileName, FileName, true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btn_AddCondition_Click(object sender, EventArgs e)   
    {
        DataTable dt = new DataTable();
        DataRow dr;
        try
        {
                if (dtCondition == null)
                    dtCondition = new DataTable();
                if (dtCondition.Columns.Count == 0)
                {
                    dt.Columns.Add("Type");
                    dt.Columns.Add("Value");
                    dt.Columns.Add("ValueData");
                    dt.Columns.Add("RangeFrom");
                    dt.Columns.Add("RangeTo");
                    dt.Columns.Add("DateFrom");
                    dt.Columns.Add("DateTo");
                    dt.Columns.Add("RecentlyData");
                }
                else
                    dt = dtCondition;
                string ValueDDL = ddlCrit.SelectedValue;
                dr = dt.NewRow();
                switch (ValueDDL)
                {
                    case "1":
                        if (ddlOutlet.SelectedIndex != 0)
                        {
                            dr["Type"] = "Register Outlet";
                            dr["Value"] = ddlOutlet.SelectedItem.Text.ToString();
                            dr["ValueData"] = ddlOutlet.SelectedValue.ToString();
                            dt.Rows.Add(dr);
                        }
                        else
                        {
                            JsClientAlert("RegisterOutlet", "Please select outlet.");
                        }
                        break;
                    case "2":
                        if (ddlOutlet2.SelectedIndex != 0)
                        {
                            if (txtFrequencyFrom.Text.ToString() == "")
                                JsClientAlert("txtFrequencyFrom", "Please insert value at FrequencyFrom.");
                            else if (txtFrequencyTo.Text.ToString() == "")
                                JsClientAlert("txtFrequencyTo", "Please insert value at FrequencyTo.");
                            else
                            {
                                dr["Type"] = "Repeat Outlet";
                                dr["Value"] = ddlOutlet2.SelectedItem.Text.ToString();
                                dr["ValueData"] = ddlOutlet2.SelectedValue.ToString();
                                dr["RangeFrom"] = txtFrequencyFrom.Text.ToString();
                                dr["RangeTo"] = txtFrequencyTo.Text.ToString();
                                dr["DateFrom"] = ddlRecentOutlet.SelectedItem.Text.ToString();
                                dr["RecentlyData"] = ddlRecentOutlet.SelectedValue.ToString();
                                dt.Rows.Add(dr);
                            }
                        }
                        else
                        {
                            JsClientAlert("RepeatOutlet", "Please select outlet.");
                        }
                        break;
                    case "3":
                        if (txtRangeFrom.Text.ToString() == "")
                            JsClientAlert("txtRangeFrom", "Please insert value at RangeFrom.");
                        else if (txtRangeTo.Text.ToString() == "")
                            JsClientAlert("txtRangeTo", "Please insert value at RangeTo.");
                        else
                        {
                            dr["Type"] = "Age";
                            dr["RangeFrom"] = txtRangeFrom.Text.ToString();
                            dr["RangeTo"] = txtRangeTo.Text.ToString();
                            dt.Rows.Add(dr);
                        }
                        break;
                    case "4":
                        dr["Type"] = "Gender";
                        dr["Value"] = ddlGender.SelectedItem.Text.ToString();
                        dr["ValueData"] = ddlGender.SelectedValue.ToString();
                        dt.Rows.Add(dr);
                        break;
                    case "5":
                        if (txtDateBirthFrom.Text.ToString() != "")
                        {
                            if (txtDateBirthTo.Text.ToString() != "")
                            {
                                if (DateTime.ParseExact(txtDateBirthFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(txtDateBirthTo.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture))
                                    JsClientAlert("txtDateBirth", "Please insert value at DateTo more than DateFrom");
                                else
                                {
                                    dr["Type"] = "Date of Birth";
                                    dr["DateFrom"] = txtDateBirthFrom.Text.ToString();
                                    dr["DateTo"] = txtDateBirthTo.Text.ToString();
                                    dt.Rows.Add(dr);
                                }
                            }
                            else
                            {
                                JsClientAlert("txtDateBirthTo", "Please insert DateBirthTo.");
                            }
                        }
                        else
                        {
                            JsClientAlert("txtDateBirthFrom", "Please insert DateBirthFrom.");
                        }
                        break;
                    case "6":
                        if (ddlProvince.SelectedIndex != 0)
                        {
                            dr["Type"] = "Province";
                            dr["Value"] = ddlProvince.SelectedItem.Text.ToString();
                            dr["ValueData"] = ddlProvince.SelectedValue.ToString();
                            dt.Rows.Add(dr);
                        }
                        break;
                    case "7":
                        if (txtActivePeriodFrom.Text.ToString() != "")
                        {
                            if (txtActivePeriodTo.Text.ToString() != "")
                            {
                                if (DateTime.ParseExact(txtActivePeriodFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture) > DateTime.ParseExact(txtActivePeriodTo.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture))
                                    JsClientAlert("txtActivePeriodFrom", "Please insert value at DateTo more than DateFrom");
                                else
                                {
                                    dr["Type"] = "Active Period";
                                    dr["DateFrom"] = txtActivePeriodFrom.Text.ToString();
                                    dr["DateTo"] = txtActivePeriodTo.Text.ToString();
                                    dt.Rows.Add(dr);
                                }
                            }
                            else
                            {
                                JsClientAlert("txtActivePeriodTo", "Please insert ActivePeriodTo.");
                            }
                        }
                        else
                        {
                            JsClientAlert("txtActivePeriodFrom", "Please insert ActivePeriodFrom.");
                        }
                        break;
                }
                

                dtCondition = dt;

                dgCriteria.DataSource = dtCondition;
                dgCriteria.DataBind();
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtracConsumerMasterData.btn_AddCondition_Click :: " + ex.Message);
        }
    }
    protected void ddlCrit_SelectedIndexChanged1(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddl = (DropDownList)sender;
            string sddlID = ddl.ID;
            ClearData();
            ddl1.Visible = false;
            ddl2.Visible = false;
            ddl3.Visible = false;
            ddl4.Visible = false;
            ddl5.Visible = false;
            ddl6.Visible = false;
            ddl7.Visible = false;
            if (cbAllField.Checked)
                cbCheckAll = true;
            else
                cbCheckAll = false;
            switch (Convert.ToInt32(ddl.SelectedValue))
            {
                case 1:
                    ddl1.Visible = true;
                    break;
                case 2:
                    ddl2.Visible = true;

                    break;
                case 3:
                    ddl3.Visible = true;
                    break;
                case 4:
                    ddl4.Visible = true;
   
                    break;
                case 5:
                    ddl5.Visible = true;

                    break;
                case 6:
                    ddl6.Visible = true;

                    break;
                case 7:
                    ddl7.Visible = true;

                    break;
                default:
                    ddl1.Visible = true;
                    break;
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtractData_ExtractCampaignSpecificData.ddlEDM_SelectedIndexChanged :: " + ex.Message);
        }
    }
    protected void btn_RemoveCondition_Click(object sender, EventArgs e)
    {
        try
        {
            string del = "";
            countAddID = 0;
            foreach (DataGridItem di in dgCriteria.Items)
            {
                HtmlInputCheckBox chkBx = (HtmlInputCheckBox)di.FindControl("cbTypeCriteria");
                if (chkBx != null && chkBx.Checked)
                {
                    del += countAddID.ToString() + ",";
                }
                countAddID++;
            }
            if (del.Length > 0)
               del = del.Remove(del.Length - 1);

            string[] strDel = del.Split(',');
            for (int i = strDel.Count() - 1; i >= 0;i-- )
            {
                dtCondition.Rows[int.Parse(strDel[i])].Delete();
            }

            dgCriteria.DataSource = dtCondition;
            dgCriteria.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("btn_RemoveCondition_Click" + ex.Message);
        }
    }
    protected void ClearCheck(object sender, EventArgs e)
    {
        try
        {
            CheckBox chk = sender as CheckBox;
            bool check = chk.Checked;
            foreach (DataGridItem di in dgCriteria.Items)
            {
                HtmlInputCheckBox chkBx = (HtmlInputCheckBox)di.FindControl("cbTypeCriteria");
                chkBx.Checked = check;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("btn_RemoveCondition_Click" + ex.Message);
        }
    }

}
