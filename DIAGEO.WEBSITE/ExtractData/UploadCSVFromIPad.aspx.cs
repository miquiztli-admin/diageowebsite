﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using DIAGEO.BLL;
using DIAGEO.COMMON;
using System.Collections;
using System.Data.SqlClient;
using System.Globalization;
using System.Data.OleDb;

public partial class ExtractData_UploadCSVFromIPad : BasePage
{
    #region Variable

    private string _rootPath;

    #endregion

    #region Property
    public string RootPath
    {
        get { return _rootPath; }
        set { _rootPath = value; }
    }
    public string FileFullPath //RootDirect + FileName
    {
        get { return (string)ViewState["FileFullPath"]; }
        set { ViewState["FileFullPath"] = value; }
    }
    public string RootDirect
    {
        get { return (string)ViewState["RootDirect"]; }
        set { ViewState["RootDirect"] = value; }
    }
    public string CSVOutletName
    {
        get { return (string)ViewState["CSVOutletName"]; }
        set { ViewState["CSVOutletName"] = value; }
    }
    public string CSVRegisterDate
    {
        get { return (string)ViewState["CSVRegisterDate"]; }
        set { ViewState["CSVRegisterDate"] = value; }
    }
    public string RowErorr
    {
        get { return (string)ViewState["RowErorr"]; }
        set { ViewState["RowErorr"] = value; }
    }
    public int CountRows
    {
        get { return (int)ViewState["CountRows"]; }
        set { ViewState["CountRows"] = value; }
    }
    #endregion

    #region Medhod

    private void BindType(DropDownList ddlBranName)
    {
        try
        {
            ddlBranName.Items.Add(new ListItem("-- Please select --", "0"));
            ddlBranName.Items.Add(new ListItem("JW Black", "JW Black"));
            ddlBranName.Items.Add(new ListItem("JW Red", "JW Red"));
            ddlBranName.Items.Add(new ListItem("Smirnoff", "Smirnoff"));
            ddlBranName.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception("BindType :: " + ex.Message);
        }
    }

    public bool ValidateSave()
    {

        try
        {
            if (ddlBranName.SelectedValue == "0")
            {
                JsClientAlert("UploadCSVFromIPad", "โปรดเลือก BrandMember");
                return false;
            }
            return true;
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateSave :: " + ex.Message);
        }
    }

    private bool CheckFile(string FileName)
    {
        string FileType = "";
        try
        {
            FileType = FileName.Substring(FileName.IndexOf('.') + 1);
            if (FileType.ToLower().ToString().Equals("csv"))
                return true;
            else
                return false;
        }
        catch (Exception ex)
        {
            throw new Exception("CheckFile :: " + ex.Message);
        }
    }

    private DataTable GetDataTableFromCsv(bool isFirstRowHeader) 
    {
        try
        {
            RootPath = Server.MapPath("~\\").ToString();
            string PathUpload = ConfigurationManager.AppSettings["PathFileUpload"].ToString();
            string FileName = Path.GetFileName(FileUpload.FileName);
            FileName = FileName.Replace(" ","_");
            FileFullPath = "";

            if (RootDirect != null && FileName != null)
                CopyBackupAndDeleteExcelFile(RootDirect, FileName, false, true);

            RootDirect = RootPath + PathUpload;
            FileFullPath = Path.Combine(RootDirect, FileName);

            FileUpload.SaveAs(FileFullPath);

            string header = isFirstRowHeader ? "Yes" : "No";
            string sql = @"SELECT * FROM [" + FileName + "]";
            OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + RootDirect + ";Extended Properties=\"Text;HDR=" + header + "\"");
            OleDbCommand command = new OleDbCommand(sql, connection);
            OleDbDataAdapter adapter = new OleDbDataAdapter(command);

            DataTable dataTable = new DataTable();
            dataTable.Locale = CultureInfo.CurrentCulture;
            adapter.Fill(dataTable);
            CopyBackupAndDeleteExcelFile(RootDirect, FileName, false, true);
            return dataTable;
        }
        catch (Exception ex)
        {
            throw new Exception("GetDataTableFromCsv" + ex.Message);
        }
    }

    private void UploadCSVFromIpad()
    {
        DataTable dtCSV;
        DataTable dtConsumer;
        DataRow dr;
        string CSVFirstName, CSVLastName, CSVMOBILENUMBER, CSVEMAIL, CSVBrandPreference, CSVOptIn, CSVBrand4Aresponse, CSVOptIn2, BarndName;
        DateTime CSVBirthday, CSVRegisterIsDate;
        string formatInPutDateTime = "M/d/yyyy hh:mm:ss tt";
        ConsumerBLL consumerBLL;
        User user;

        try
        {
            if (FileUpload.HasFile)
            {
                if (ValidateSave())
                {
                    if (CheckFile(FileUpload.FileName))
                    {
                        dtCSV = GetDataTableFromCsv(false);
                        if ((dtCSV.Rows.Count > 1) && (dtCSV.Columns.Count >= 9))
                        {
                            RowErorr = "";
                            CountRows = 0;
                            dtConsumer = new DataTable("Consumer");

                            dtConsumer.Columns.Add("FirstName", typeof(string));
                            dtConsumer.Columns.Add("LastName", typeof(string));
                            dtConsumer.Columns.Add("Birthday", typeof(DateTime));
                            dtConsumer.Columns.Add("MOBILENUMBER", typeof(string));
                            dtConsumer.Columns.Add("EMAIL", typeof(string));
                            dtConsumer.Columns.Add("BrandPreference", typeof(string));
                            dtConsumer.Columns.Add("OptIn", typeof(string));
                            dtConsumer.Columns.Add("4Aresponse", typeof(string));
                            dtConsumer.Columns.Add("OptIn2", typeof(string));
                            dtConsumer.Columns.Add("OutletName", typeof(string));
                            dtConsumer.Columns.Add("RegisterDate", typeof(DateTime));
                            dtConsumer.Columns.Add("BarndName", typeof(string));

                            for (int i = 0; i < dtCSV.Rows.Count; i++)
                            {
                                dr = dtCSV.Rows[i];

                                if (i == 0)
                                {
                                    CSVOutletName = dr["F1"].ToString();
                                    CSVRegisterDate = dr["F3"].ToString();
                                }
                                else
                                {
                                    if (!(DateTime.TryParseExact(CSVRegisterDate, formatInPutDateTime, CultureInfo.InvariantCulture, DateTimeStyles.None, out CSVRegisterIsDate)))
                                    {
                                        JsClientAlert("ErorrRegisterDate", " ข้อมูล RegisterDate ใน Row ที่ 1 ไม่ถูกต้อง");
                                    }
                                    CSVFirstName = dr["F1"].ToString();
                                    CSVLastName = dr["F2"].ToString();
                                    if (!(DateTime.TryParseExact(dr["F3"].ToString(), formatInPutDateTime, CultureInfo.InvariantCulture, DateTimeStyles.None, out CSVBirthday)))
                                    {
                                        if (RowErorr == "")
                                        {
                                            RowErorr = (i + 1).ToString();
                                        }
                                        else
                                        {
                                            RowErorr = RowErorr + "," + (i + 1).ToString();
                                        }
                                    }
                                    else
                                    {
                                        CountRows = CountRows + 1;
                                    }
                                    CSVMOBILENUMBER = dr["F4"].ToString();
                                    CSVEMAIL = dr["F5"].ToString();
                                    CSVBrandPreference = dr["F6"].ToString();
                                    CSVOptIn = dr["F7"].ToString();
                                    CSVBrand4Aresponse = dr["F8"].ToString();
                                    CSVOptIn2 = dr["F9"].ToString();
                                    BarndName = ddlBranName.SelectedValue;

                                    dtConsumer.Rows.Add(new object[] { CSVFirstName, CSVLastName, CSVBirthday, CSVMOBILENUMBER, CSVEMAIL, CSVBrandPreference, CSVOptIn, CSVBrand4Aresponse, CSVOptIn2, CSVOutletName, CSVRegisterIsDate, BarndName });
                                }

                            }

                            if (RowErorr == "")
                            {
                                consumerBLL = new ConsumerBLL();
                                user = new User();

                                if (consumerBLL.InserOrUpdateConsumerByIpad(dtConsumer, user.UserID.ToString()))
                                {
                                    ddlBranName.SelectedIndex = 0;
                                    JsClientAlert("Success", " เพิ่มข้อมูลเป็นจำนวน " + CountRows + " Row เรียบร้อย ");
                                }
                                else
                                {
                                    JsClientAlert("ErorrInsert", "บันทึกไม่สำเร็จ ชื่อร้าน หรือ ชื่อแบรน ไม่มีในฐานข้อมูล");
                                }
                            }
                            else
                            {
                                ddlBranName.SelectedIndex = 0;
                                JsClientAlert("RowErorr", "ข้อมูลมีปัญหา Row ที่ " + RowErorr);
                            }

                        }
                        else
                        {
                            JsClientAlert("FileNoData", "ไม่มีข้อมูลอยู่ในfile หรือ รูปแบบข้อมูลไม่ถูกต้อง");
                        }
                    }
                    else
                    {
                        JsClientAlert("FileNotExcel", "ไฟล์ผิดชนิด ต้องอัพโหลดไฟล์ CSV เท่านั้น");
                    }
                }
            }
            else
            {
                JsClientAlert("FileNotFound", "กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!IsPostBack)
            {
                BindType(ddlBranName);
            }
            else
            {
               
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UploadFile.Page_Load" + ex.Message);
        }
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {
            UploadCSVFromIpad();
        }
        catch (Exception ex)
        {
            JsClientAlert("Erorr", "" + ex.Message);
        }
    }

    #endregion

}