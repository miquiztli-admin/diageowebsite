﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using DIAGEO.BLL;
using ITOS.Utility.CustomControl;
using System.Configuration;
using System.Globalization;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Text;
using System.IO;

public partial class ExtractData_ExtractionConsumersData : BasePage
{

    #region Property

    public int TabNumber
    {
        get { return (int)ViewState["TabNumber"]; }
        set { ViewState["TabNumber"] = value; }
    }
    public DataTable dtCondition
    {
        get { return (DataTable)Session["dtCondition" + TabNumber]; }
        set { Session["dtCondition" + TabNumber] = value; }
    }
    public string Mode
    {
        get { return (string)Session["Mode"]; }
        set { Session["Mode"] = value; }
    }

    #endregion

    #region Method

    #region Fill Data To DataTable

    private DataTable GetDataOutboundEventMaster(DataTable dt, TextBox txtEventName, TextBox dpEventDateFrom, TextBox dpEventDateTo, string ExcludeBlockList)
    {
        DIAGEO.COMMON.User User = new DIAGEO.COMMON.User();
        DataRow dr;
        try
        {
            #region Schema dt
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("EventName");
                dt.Columns.Add("EventDateFrom");
                dt.Columns.Add("EventDateTo");
                dt.Columns.Add("ExcludeBlockList");
                dt.Columns.Add("CreateBy");
                dt.Columns.Add("CreateDate");
                dt.Columns.Add("UpdateBy");
                dt.Columns.Add("UpdateDate");
                dt.Columns.Add("Flag");
            }
            #endregion

            dr = dt.NewRow();
            dr["EventName"] = txtEventName.Text;
            dr["EventDateFrom"] = ConvertDatetimeFromDatepicker(dpEventDateFrom.Text);
            dr["EventDateTo"] = ConvertDatetimeFromDatepicker(dpEventDateTo.Text);
            dr["ExcludeBlockList"] = ExcludeBlockList;
            if (Mode.ToLower().Equals("add"))
            {
                dr["CreateBy"] = User.UserID;
                dr["CreateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                dr["UpdateBy"] = User.UserID;
                dr["UpdateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
            }
            dr["Flag"] = "Event";
            dt.Rows.Add(dr);

            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception("GetDataOutboundEventMaster :: " + ex.Message);
        }
    }

    private DataTable GetDataOutboundEvent(DataTable dt, string DataGroup, TextBox txtEventName, TextBox txtSenderName, DropDownList ddlBrand, CheckBoxList chkListBrand,
                        RadioButtonList rbtnListGender, CheckBoxList chkListValid, TextBox txtAgeFrom, TextBox txtAgeTo,
                        CheckBoxList chkListMonth, TextBox txtOpenEmail, TextBox txtReceiveEmail, RadioButtonList rbtnEmailActive,
                        RadioButtonList rbtnMobileActive, TextBox txtEmailRate, CheckBox chkIncludeCallCenter, CheckBox chkIncludeEDM,
                        CheckBox chkIncludeSMS)
    {
        DIAGEO.COMMON.User User = new DIAGEO.COMMON.User();
        DataRow dr;
        string check;
        try
        {
            #region Schema dt
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("DataGroup");
                dt.Columns.Add("EventName");
                dt.Columns.Add("SenderName");
                dt.Columns.Add("BrandID");
                dt.Columns.Add("brandPre100Pipers");
                dt.Columns.Add("brandPre100Pipers8Y");
                dt.Columns.Add("brandPreAbsolut");
                dt.Columns.Add("brandPreBaileys");
                dt.Columns.Add("brandPreBallentine");
                dt.Columns.Add("brandPreBenmore");
                dt.Columns.Add("brandPreBlend");
                dt.Columns.Add("brandPreChivas");
                dt.Columns.Add("brandPreDewar");
                dt.Columns.Add("brandPreHennessy");
                dt.Columns.Add("brandPreJwBlack");
                dt.Columns.Add("brandPreJwGold");
                dt.Columns.Add("brandPreJwGreen");
                dt.Columns.Add("brandPreJwRed");
                dt.Columns.Add("brandPreSmirnoff");
                dt.Columns.Add("brandPreOther");
                dt.Columns.Add("brandPreNotdrink");
                dt.Columns.Add("Gender");
                dt.Columns.Add("ValidDiageo");
                dt.Columns.Add("ValidOutlet");
                dt.Columns.Add("AgeFrom");
                dt.Columns.Add("AgeTo");
                dt.Columns.Add("MonthJan");
                dt.Columns.Add("MonthFeb");
                dt.Columns.Add("MonthMar");
                dt.Columns.Add("MonthApr");
                dt.Columns.Add("MonthMay");
                dt.Columns.Add("MonthJun");
                dt.Columns.Add("MonthJul");
                dt.Columns.Add("MonthAug");
                dt.Columns.Add("MonthSep");
                dt.Columns.Add("MonthOct");
                dt.Columns.Add("MonthNov");
                dt.Columns.Add("MonthDec");
                dt.Columns.Add("OpenEmail");
                dt.Columns.Add("ReceiveEmail");
                dt.Columns.Add("EmailActive");
                dt.Columns.Add("MobileActive");
                dt.Columns.Add("EmailRate");
                dt.Columns.Add("IncludeCallCenter");
                dt.Columns.Add("IncludeEDM");
                dt.Columns.Add("IncludeSMS");
                dt.Columns.Add("CreateBy");
                dt.Columns.Add("CreateDate");
                dt.Columns.Add("UpdateBy");
                dt.Columns.Add("UpdateDate");
            }
            #endregion

            dr = dt.NewRow();
            dr["DataGroup"] = DataGroup;
            dr["EventName"] = txtEventName.Text;
            dr["SenderName"] = txtSenderName.Text;
            dr["BrandID"] = ddlBrand.SelectedValue.ToString();

            for (int i = 0; i < chkListBrand.Items.Count; i++)
            {
                check = (chkListBrand.Items[i].Selected) ? "T" : "F";
                switch (chkListBrand.Items[i].Value.ToString())
                {
                    case "brandPre100Pipers":
                        dr["brandPre100Pipers"] = check;
                        break;
                    case "brandPre100Pipers8Y":
                        dr["brandPre100Pipers8Y"] = check;
                        break;
                    case "brandPreAbsolut":
                        dr["brandPreAbsolut"] = check;
                        break;
                    case "brandPreBaileys":
                        dr["brandPreBaileys"] = check;
                        break;
                    case "brandPreBallentine":
                        dr["brandPreBallentine"] = check;
                        break;
                    case "brandPreBenmore":
                        dr["brandPreBenmore"] = check;
                        break;
                    case "brandPreBlend":
                        dr["brandPreBlend"] = check;
                        break;
                    case "brandPreChivas":
                        dr["brandPreChivas"] = check;
                        break;
                    case "brandPreDewar":
                        dr["brandPreDewar"] = check;
                        break;
                    case "brandPreHennessy":
                        dr["brandPreHennessy"] = check;
                        break;
                    case "brandPreJwBlack":
                        dr["brandPreJwBlack"] = check;
                        break;
                    case "brandPreJwGold":
                        dr["brandPreJwGold"] = check;
                        break;
                    case "brandPreJwGreen":
                        dr["brandPreJwGreen"] = check;
                        break;
                    case "brandPreJwRed":
                        dr["brandPreJwRed"] = check;
                        break;
                    case "brandPreSmirnoff":
                        dr["brandPreSmirnoff"] = check;
                        break;
                    case "brandPreOther":
                        dr["brandPreOther"] = check;
                        break;
                    case "brandPreNotdrink":
                        dr["brandPreNotdrink"] = check;
                        break;
                }
            }

            for (int i = 0; i < rbtnListGender.Items.Count; i++)
            {
                if (rbtnListGender.Items[i].Selected)
                    dr["Gender"] = rbtnListGender.Items[i].Value.ToString();
            }

            for (int i = 0; i < chkListValid.Items.Count; i++)
            {
                check = (chkListValid.Items[i].Selected) ? "T" : "F";
                switch (chkListValid.Items[i].Value)
                {
                    case "D":
                        dr["ValidDiageo"] = check;
                        break;
                    case "O":
                        dr["ValidOutlet"] = check;
                        break;
                }
            }

            dr["AgeFrom"] = txtAgeFrom.Text;
            dr["AgeTo"] = txtAgeTo.Text;

            for (int i = 0; i < chkListMonth.Items.Count; i++)
            {
                check = (chkListMonth.Items[i].Selected) ? "T" : "F";
                switch (chkListMonth.Items[i].Value)
                {
                    case "MonthJan":
                        dr["MonthJan"] = check;
                        break;
                    case "MonthFeb":
                        dr["MonthFeb"] = check;
                        break;
                    case "MonthMar":
                        dr["MonthMar"] = check;
                        break;
                    case "MonthApr":
                        dr["MonthApr"] = check;
                        break;
                    case "MonthMay":
                        dr["MonthMay"] = check;
                        break;
                    case "MonthJun":
                        dr["MonthJun"] = check;
                        break;
                    case "MonthJul":
                        dr["MonthJul"] = check;
                        break;
                    case "MonthAug":
                        dr["MonthAug"] = check;
                        break;
                    case "MonthSep":
                        dr["MonthSep"] = check;
                        break;
                    case "MonthOct":
                        dr["MonthOct"] = check;
                        break;
                    case "MonthNov":
                        dr["MonthNov"] = check;
                        break;
                    case "MonthDec":
                        dr["MonthDec"] = check;
                        break;
                }
            }

            dr["OpenEmail"] = txtOpenEmail.Text;
            dr["ReceiveEmail"] = txtReceiveEmail.Text;

            for (int i = 0; i < rbtnEmailActive.Items.Count; i++)
            {
                if (rbtnEmailActive.Items[i].Selected)
                    dr["EmailActive"] = rbtnEmailActive.Items[i].Value.ToString();
            }

            for (int i = 0; i < rbtnMobileActive.Items.Count; i++)
            {
                if (rbtnMobileActive.Items[i].Selected)
                    dr["MobileActive"] = rbtnMobileActive.Items[i].Value.ToString();
            }

            dr["EmailRate"] = txtEmailRate.Text;
            dr["IncludeCallCenter"] = (chkIncludeCallCenter.Checked) ? "T" : "F";
            dr["IncludeEDM"] = (chkIncludeEDM.Checked) ? "T" : "F";
            dr["IncludeSMS"] = (chkIncludeSMS.Checked) ? "T" : "F";
            if (Mode.ToLower().Equals("add"))
            {
                dr["CreateBy"] = User.UserID;
                dr["CreateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
            }
            else
            {
                dr["UpdateBy"] = User.UserID;
                dr["UpdateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
            }
            dt.Rows.Add(dr);

            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception("GetSchemaOutboundEvent :: " + ex.Message);
        }
    }

    private DataTable GetDataEventPlatform(DataTable dt, string DataGroup, ArrayList ArrPlatformID)
    {
        DIAGEO.COMMON.User User = new DIAGEO.COMMON.User();
        DataRow dr;
        try
        {
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("PlatformID");
                dt.Columns.Add("DataGroup");
                dt.Columns.Add("CreateBy");
                dt.Columns.Add("CreateDate");
                dt.Columns.Add("UpdateBy");
                dt.Columns.Add("UpdateDate");
            }

            for (int i = 0; i < ArrPlatformID.Count; i++)
            {
                dr = dt.NewRow();
                dr["PlatformID"] = ArrPlatformID[i].ToString();
                dr["DataGroup"] = DataGroup;
                if (Mode.ToLower().Equals("add"))
                {
                    dr["CreateBy"] = User.UserID;
                    dr["CreateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                }
                else
                {
                    dr["UpdateBy"] = User.UserID;
                    dr["UpdateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                }
                dt.Rows.Add(dr);
            }

            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception("GetDataEventPlatform :: " + ex.Message);
        }
    }

    private DataTable GetDataEventOutlet(DataTable dt, string DataGroup, ArrayList ArrOutletID)
    {
        DIAGEO.COMMON.User User = new DIAGEO.COMMON.User();
        DataRow dr;
        try
        {
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("OutletID");
                dt.Columns.Add("DataGroup");
                dt.Columns.Add("CreateBy");
                dt.Columns.Add("CreateDate");
                dt.Columns.Add("UpdateBy");
                dt.Columns.Add("UpdateDate");
            }

            for (int i = 0; i < ArrOutletID.Count; i++)
            {
                dr = dt.NewRow();
                dr["OutletID"] = ArrOutletID[i].ToString();
                dr["DataGroup"] = DataGroup;
                if (Mode.ToLower().Equals("add"))
                {
                    dr["CreateBy"] = User.UserID;
                    dr["CreateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                }
                else
                {
                    dr["UpdateBy"] = User.UserID;
                    dr["UpdateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                }
                dt.Rows.Add(dr);
            }

            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception("GetDataEventOutlet :: " + ex.Message);
        }
    }

    private DataTable GetDataEventProvince(DataTable dt, string DataGroup, ArrayList ArrProvinceID)
    {
        DIAGEO.COMMON.User User = new DIAGEO.COMMON.User();
        DataRow dr;
        try
        {
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("ProvinceID");
                dt.Columns.Add("DataGroup");
                dt.Columns.Add("CreateBy");
                dt.Columns.Add("CreateDate");
                dt.Columns.Add("UpdateBy");
                dt.Columns.Add("UpdateDate");
            }

            for (int i = 0; i < ArrProvinceID.Count; i++)
            {
                dr = dt.NewRow();
                dr["ProvinceID"] = ArrProvinceID[i].ToString();
                dr["DataGroup"] = DataGroup;
                if (Mode.ToLower().Equals("add"))
                {
                    dr["CreateBy"] = User.UserID;
                    dr["CreateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                }
                else
                {
                    dr["UpdateBy"] = User.UserID;
                    dr["UpdateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                }
                dt.Rows.Add(dr);
            }

            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception("GetDataEventProvince :: " + ex.Message);
        }
    }

    private DataTable GetDataEventVisitCondition(DataTable dt, string DataGroup, DataTable dtCondition)
    {
        DIAGEO.COMMON.User User = new DIAGEO.COMMON.User();
        DataRow dr;
        try
        {
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("DataGroup");
                dt.Columns.Add("TypeID");
                dt.Columns.Add("OutletID");
                dt.Columns.Add("DateFrom");
                dt.Columns.Add("DateTo");
                dt.Columns.Add("LastActivity");
                dt.Columns.Add("Visit");
                dt.Columns.Add("CreateBy");
                dt.Columns.Add("CreateDate");
                dt.Columns.Add("UpdateBy");
                dt.Columns.Add("UpdateDate");
            }

            for (int i = 0; i < dtCondition.Rows.Count; i++)
            {
                dr = dt.NewRow();
                dr["DataGroup"] = DataGroup;

                dr["TypeID"] = dtCondition.Rows[i]["TypeID"].ToString();
                dr["OutletID"] = dtCondition.Rows[i]["OutletID"].ToString();
                dr["DateFrom"] = ConvertDatetimeFromDatepicker(dtCondition.Rows[i]["DateFrom"].ToString());
                dr["DateTo"] = ConvertDatetimeFromDatepicker(dtCondition.Rows[i]["DateTo"].ToString());
                dr["LastActivity"] = dtCondition.Rows[i]["LastActivity"].ToString();
                dr["Visit"] = dtCondition.Rows[i]["Visit"].ToString();

                if (Mode.ToLower().Equals("add"))
                {
                    dr["CreateBy"] = User.UserID;
                    dr["CreateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                }
                else
                {
                    dr["UpdateBy"] = User.UserID;
                    dr["UpdateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                }
                dt.Rows.Add(dr);
            }

            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception("GetDataEventVisitCondition :: " + ex.Message);
        }
    }

    #endregion

    private string Group, EventId;

    private void ClearSessionInEventPage()
    {
        int Tab = 0, Index = 0;
        try
        {
            Tab = tabContainer.Tabs.Count;
            for (int i = 0; i < Tab; i++)
            {
                Index = i + 1;
                Session["ArrPlatformID" + Index] = null;
                Session["ArrPlatformName" + Index] = null;
                Session["ArrOutletID" + Index] = null;
                Session["ArrOutletName" + Index] = null;
                Session["ArrProvinceID" + Index] = null;
                Session["ArrProvinceName" + Index] = null;
                Session["dtCondition" + Index] = null;
            }

        }
        catch (Exception ex)
        {
            throw new Exception("ClearSessionInEventPage :: " + ex.Message);
        }
    }

    private void BindControl()
    {
        OutletBLL bllOutlet = new OutletBLL();
        DataTable dtOutlet = new DataTable();
        try
        {
            dtOutlet = bllOutlet.SelectActiveOutletOrderByName("T");

            //tab1
            BindBrandDropDownList(tab1_ddlBrand);
            BindBrandCheckBox(tab1_chkListBrand);
            BindGender(tab1_rbtnListGender);
            BindMonth(tab1_chkListMonth);
            BindType(tab1_ddlType);
            BindOutlet(tab1_ddlOutlet, dtOutlet);
            TypeCondition(tab1_ddlType, tab1_dpDateFrom, tab1_dpDateTo, tab1_txtLastActivity, tab1_txtVisit, tab1_ddlOutlet);
            BindDataGrid(tab1_dgCondition);

            //tab2
            BindBrandDropDownList(tab2_ddlBrand);
            BindBrandCheckBox(tab2_chkListBrand);
            BindGender(tab2_rbtnListGender);
            BindMonth(tab2_chkListMonth);
            BindType(tab2_ddlType);
            BindOutlet(tab2_ddlOutlet, dtOutlet);
            TypeCondition(tab2_ddlType, tab2_dpDateFrom, tab2_dpDateTo, tab2_txtLastActivity, tab2_txtVisit, tab2_ddlOutlet);
            BindDataGrid(tab2_dgCondition);

            //tab3
            BindBrandDropDownList(tab3_ddlBrand);
            BindBrandCheckBox(tab3_chkListBrand);
            BindGender(tab3_rbtnListGender);
            BindMonth(tab3_chkListMonth);
            BindType(tab3_ddlType);
            BindOutlet(tab3_ddlOutlet, dtOutlet);
            TypeCondition(tab3_ddlType, tab3_dpDateFrom, tab3_dpDateTo, tab3_txtLastActivity, tab3_txtVisit, tab3_ddlOutlet);
            BindDataGrid(tab3_dgCondition);
        }
        catch (Exception ex)
        {
            throw new Exception("BindControl :: " + ex.Message);
        }
    }

    private void BindBrandDropDownList(DropDownList ddlBrand)
    {
        BrandBLL bllBrand = new BrandBLL();
        DataTable dtBrand = new DataTable("Brand");
        try
        {
            dtBrand = bllBrand.SelectActiveBrandOrderByName("T");
            ddlBrand.DataTextField = "Brand_Name";
            ddlBrand.DataValueField = "Brand_ID";
            ddlBrand.DataSource = dtBrand;
            ddlBrand.DataBind();

            ddlBrand.Items.Insert(0, new ListItem("-- Please Select --", "0"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindBrandDropDownList :: " + ex.Message);
        }
    }

    private void BindBrandCheckBox(CheckBoxList chkList)
    {
        try
        {
            chkList.Items.Add(new ListItem("100 Pipers", "brandPre100Pipers"));
            chkList.Items.Add(new ListItem("100 Pipers8Y", "brandPre100Pipers8Y"));
            chkList.Items.Add(new ListItem("Absolut", "brandPreAbsolut"));
            chkList.Items.Add(new ListItem("Baileys", "brandPreBaileys"));
            chkList.Items.Add(new ListItem("Ballentine", "brandPreBallentine"));
            chkList.Items.Add(new ListItem("Benmore", "brandPreBenmore"));
            chkList.Items.Add(new ListItem("Blend285", "brandPreBlend"));
            chkList.Items.Add(new ListItem("Chivas", "brandPreChivas"));
            chkList.Items.Add(new ListItem("Dewar", "brandPreDewar"));
            chkList.Items.Add(new ListItem("Hennessy", "brandPreHennessy"));
            chkList.Items.Add(new ListItem("JW Black", "brandPreJwBlack"));
            chkList.Items.Add(new ListItem("JW Gold", "brandPreJwGold"));
            chkList.Items.Add(new ListItem("JW Green", "brandPreJwGreen"));
            chkList.Items.Add(new ListItem("JW Red", "brandPreJwRed"));
            chkList.Items.Add(new ListItem("Smirnoff", "brandPreSmirnoff"));
            chkList.Items.Add(new ListItem("Other", "brandPreOther"));
            chkList.Items.Add(new ListItem("Not Drink", "brandPreNotdrink"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindBrand :: " + ex.Message);
        }
    }

    private void BindGender(RadioButtonList rbtnListGender)
    {
        try
        {
            rbtnListGender.Items.Add(new ListItem("Male", "MALE"));
            rbtnListGender.Items.Add(new ListItem("Female", "FEMALE"));
            rbtnListGender.Items.Add(new ListItem("All", "ALL"));
            rbtnListGender.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception("BindGender :: " + ex.Message);
        }
    }

    private void BindMonth(CheckBoxList chkList)
    {
        try
        {
            chkList.Items.Add(new ListItem("January", "MonthJan"));
            chkList.Items.Add(new ListItem("February", "MonthFeb"));
            chkList.Items.Add(new ListItem("March", "MonthMar"));
            chkList.Items.Add(new ListItem("April", "MonthApr"));
            chkList.Items.Add(new ListItem("May", "MonthMay"));
            chkList.Items.Add(new ListItem("June", "MonthJun"));
            chkList.Items.Add(new ListItem("July", "MonthJul"));
            chkList.Items.Add(new ListItem("August", "MonthAug"));
            chkList.Items.Add(new ListItem("September", "MonthSep"));
            chkList.Items.Add(new ListItem("October", "MonthOct"));
            chkList.Items.Add(new ListItem("November", "MonthNov"));
            chkList.Items.Add(new ListItem("December", "MonthDec"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindBrand :: " + ex.Message);
        }
    }

    private void BindType(DropDownList ddlType)
    {
        try
        {
            ddlType.Items.Add(new ListItem("-- Please select --", "0"));
            ddlType.Items.Add(new ListItem("Register", "1"));
            ddlType.Items.Add(new ListItem("Last Visited", "2"));
            ddlType.Items.Add(new ListItem("Ever Visited", "3"));
            ddlType.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception("BindType :: " + ex.Message);
        }
    }

    private void BindOutlet(DropDownList ddlOutlet, DataTable dt)
    {
        try
        {
            ddlOutlet.DataTextField = "name";
            ddlOutlet.DataValueField = "outletId";
            ddlOutlet.DataSource = dt;
            ddlOutlet.DataBind();

            ddlOutlet.Items.Insert(0, new ListItem("All Outlet", "0"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindOutlet :: " + ex.Message);
        }
    }

    private void TypeCondition(DropDownList ddlType, TextBox dpDateFrom, TextBox dpDateTo, TextBox txtLastActivity, TextBox txtVisit, DropDownList ddlOutlet)
    {
        try
        {
            if (ddlType.SelectedValue.Equals("1"))
            {
                dpDateFrom.Enabled = true;
                dpDateTo.Enabled = true;

                ddlOutlet.Enabled = true;

                txtLastActivity.Text = "";
                txtLastActivity.Enabled = false;

                txtVisit.Text = "";
                txtVisit.Enabled = false;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Dp1", "Javascript:getdatecontrolChil();", true);

            }
            else if (ddlType.SelectedValue.Equals("2"))
            {
                dpDateFrom.Text = "";
                dpDateFrom.Enabled = false;

                ddlOutlet.Enabled = true;

                dpDateTo.Text = "";
                dpDateTo.Enabled = false;

                txtLastActivity.Enabled = true;

                txtVisit.Text = "";
                txtVisit.Enabled = false;
            }
            else if (ddlType.SelectedValue.Equals("3"))
            {
                dpDateFrom.Enabled = true;
                dpDateTo.Enabled = true;

                ddlOutlet.Enabled = true;

                txtLastActivity.Text = "";
                txtLastActivity.Enabled = false;

                txtVisit.Enabled = true;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Dp2", "Javascript:getdatecontrolChil();", true);
            }
            else if (ddlType.SelectedValue.Equals("0"))
            {
                dpDateFrom.Text = "";
                dpDateFrom.Enabled = false;

                ddlOutlet.Enabled = false;

                dpDateTo.Text = "";
                dpDateTo.Enabled = false;

                txtLastActivity.Enabled = false;

                txtVisit.Text = "";
                txtVisit.Enabled = false;
            }
            else
            {
                dpDateFrom.Enabled = true;
                dpDateTo.Enabled = true;
                ddlOutlet.Enabled = true;
                txtLastActivity.Enabled = true;
                txtVisit.Enabled = true;
            }

            //dpDateFrom.ForeColor = (dpDateFrom.Enabled == true) ? System.Drawing.Color.White : System.Drawing.Color.LightGray;
            //dpDateTo.ForeColor = (dpDateTo.Enabled == true) ? System.Drawing.Color.White : System.Drawing.Color.LightGray;
            //ddlOutlet.ForeColor = (ddlOutlet.Enabled == true) ? System.Drawing.Color.White : System.Drawing.Color.LightGray;
            txtLastActivity.BackColor = (txtLastActivity.Enabled == true) ? System.Drawing.Color.White : System.Drawing.Color.LightGray;
            txtVisit.BackColor = (txtVisit.Enabled == true) ? System.Drawing.Color.White : System.Drawing.Color.LightGray;
            dpDateFrom.BackColor = (dpDateFrom.Enabled == true) ? System.Drawing.Color.White : System.Drawing.Color.LightGray;
            dpDateTo.BackColor = (dpDateTo.Enabled == true) ? System.Drawing.Color.White : System.Drawing.Color.LightGray;

            //if (IsPostBack)
            //{
            //string tabString = TabNumber.ToString();

            //if (dpDateFrom.Enabled == true)
            //{
            //JsClientCustom("openImgPopupCalender", "document.getElementById('tabContainer_tabPanel" + tabString + "_tab" + tabString + "_dpDateFrom_imgDate').style.display = 'inline'; document.getElementById('tabContainer_tabPanel" + tabString + "_tab" + tabString + "_dpDateTo_imgDate').style.display = 'inline';");
            //JsClientCustom("changeColor", "document.getElementById('tabContainer_tabPanel" + tabString + "_tab" + tabString + "_dpDateFrom_txtDate').style.background = 'white'; document.getElementById('tabContainer_tabPanel" + tabString + "_tab" + tabString + "_dpDateTo_txtDate').style.background = 'white'");
            //}
            //else
            //{
            //JsClientCustom("closeImgPopupCalender", "document.getElementById('tabContainer_tabPanel" + tabString + "_tab" + tabString + "_dpDateFrom_imgDate').style.display = 'none'; document.getElementById('tabContainer_tabPanel" + tabString + "_tab" + tabString + "_dpDateTo_imgDate').style.display = 'none';");
            //JsClientCustom("changeColor", "document.getElementById('tabContainer_tabPanel" + tabString + "_tab" + tabString + "_dpDateFrom_txtDate').style.background = 'lightgray'; document.getElementById('tabContainer_tabPanel" + tabString + "_tab" + tabString + "_dpDateTo_txtDate').style.background = 'lightgray'");
            //}
            //}
        }
        catch (Exception ex)
        {
            throw new Exception("TypeCondition :: " + ex.Message);
        }
    }

    private void BindDataGrid(DataGrid dgCondition)
    {
        try
        {
            if (dtCondition == null)
                dtCondition = new DataTable();

            dgCondition.DataSource = dtCondition;
            dgCondition.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("BindDataGrid :: " + ex.Message);
        }
    }

    private void BindItemPlatform(Label lblPlatform)
    {
        ArrayList ArrPlatformName;
        int Count = 0;
        try
        {
            lblPlatform.Text = "";
            ArrPlatformName = (ArrayList)Session["ArrPlatformName" + TabNumber.ToString()];

            if (ArrPlatformName == null)
                ArrPlatformName = new ArrayList();
            if (ArrPlatformName.Count > 0)
            {
                for (int i = 0; i < ArrPlatformName.Count; i++)
                {
                    Count++;
                    if ((Count % 3 == 0) && (Count != 0))
                        lblPlatform.Text += Environment.NewLine;

                    if (i != ArrPlatformName.Count - 1)
                        lblPlatform.Text += ArrPlatformName[i].ToString() + ", ";
                    else
                        lblPlatform.Text += ArrPlatformName[i].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindItemPlatform :: " + ex.Message);
        }
    }

    private void BindItemOutlet(Label lblOutlet)
    {
        ArrayList ArrOutletName;
        int Count = 0;
        try
        {
            lblOutlet.Text = "";
            ArrOutletName = (ArrayList)Session["ArrOutletName" + TabNumber.ToString()];

            if (ArrOutletName == null)
                ArrOutletName = new ArrayList();
            if (ArrOutletName.Count > 0)
            {
                for (int i = 0; i < ArrOutletName.Count; i++)
                {
                    Count++;
                    if ((Count % 3 == 0) && (Count != 0))
                        lblOutlet.Text += Environment.NewLine;

                    if (i != ArrOutletName.Count - 1)
                        lblOutlet.Text += ArrOutletName[i].ToString() + ", ";
                    else
                        lblOutlet.Text += ArrOutletName[i].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindItemOutlet :: " + ex.Message);
        }
    }

    private void BindItemProvince(Label lblProvince)
    {
        ArrayList ArrProvinceName;
        int Count = 0;
        try
        {
            lblProvince.Text = "";
            ArrProvinceName = (ArrayList)Session["ArrProvinceName" + TabNumber.ToString()];

            if (ArrProvinceName == null)
                ArrProvinceName = new ArrayList();
            if (ArrProvinceName.Count > 0)
            {
                for (int i = 0; i < ArrProvinceName.Count; i++)
                {
                    Count++;
                    if ((Count % 3 == 0) && (Count != 0))
                        lblProvince.Text += Environment.NewLine;

                    if (i != ArrProvinceName.Count - 1)
                        lblProvince.Text += ArrProvinceName[i].ToString() + ", ";
                    else
                        lblProvince.Text += ArrProvinceName[i].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindItemOutlet :: " + ex.Message);
        }
    }

    private void BindEditItemPlatform(DataTable dtPlatform, Label lblPlatform, string Group)
    {
        int Count = 0;
        DataRow Row;
        ArrayList ArrPlatformID = new ArrayList();
        ArrayList ArrPlatformNAME = new ArrayList();

        try
        {

            if (dtPlatform.Rows.Count != 0)
            {
                for (int i = 0; i < dtPlatform.Rows.Count; i++)
                {
                    Count++;
                    Row = dtPlatform.Rows[i];

                    ArrPlatformID.Add(Row["platformId"].ToString());
                    ArrPlatformNAME.Add(Row["name"].ToString());

                    if ((Count % 3 == 0) && (Count != 0))
                        lblPlatform.Text += Environment.NewLine;

                    if (i != dtPlatform.Rows.Count - 1)
                        lblPlatform.Text += Row["name"].ToString() + ", ";
                    else
                        lblPlatform.Text += Row["name"].ToString().ToString();
                }
                Session["ArrPlatformID" + Group] = ArrPlatformID;
                Session["ArrPlatformName" + Group] = ArrPlatformNAME;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindItemPlatform :: " + ex.Message);
        }
    }

    private void BindEditItemOutlet(DataTable dtOutlet, Label lblOutlet, string Group)
    {
        int Count = 0;
        DataRow Row;
        ArrayList ArrOutletID = new ArrayList();
        ArrayList ArrOutletNAME = new ArrayList();
        try
        {

            if (dtOutlet.Rows.Count != 0)
            {
                for (int i = 0; i < dtOutlet.Rows.Count; i++)
                {
                    Count++;
                    Row = dtOutlet.Rows[i];

                    ArrOutletID.Add(Row["outletId"].ToString());
                    ArrOutletNAME.Add(Row["name"].ToString());

                    if ((Count % 3 == 0) && (Count != 0))
                        lblOutlet.Text += Environment.NewLine;

                    if (i != dtOutlet.Rows.Count - 1)
                        lblOutlet.Text += Row["name"].ToString() + ", ";
                    else
                        lblOutlet.Text += Row["name"].ToString().ToString();
                }
                Session["ArrOutletID" + Group] = ArrOutletID;
                Session["ArrOutletName" + Group] = ArrOutletNAME;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindItemOutlet :: " + ex.Message);
        }
    }

    private void BindEditItemProvince(DataTable dtProvince, Label lblProvince, string Group)
    {
        int Count = 0;
        DataRow Row;
        ArrayList ArrProvinceID = new ArrayList();
        ArrayList ArrProvinceNAME = new ArrayList();
        try
        {

            if (dtProvince.Rows.Count != 0)
            {
                for (int i = 0; i < dtProvince.Rows.Count; i++)
                {
                    Count++;
                    Row = dtProvince.Rows[i];

                    ArrProvinceID.Add(Row["provinceId"].ToString());
                    ArrProvinceNAME.Add(Row["name"].ToString());

                    if ((Count % 3 == 0) && (Count != 0))
                        lblProvince.Text += Environment.NewLine;

                    if (i != dtProvince.Rows.Count - 1)
                        lblProvince.Text += Row["name"].ToString() + ", ";
                    else
                        lblProvince.Text += Row["name"].ToString().ToString();
                }
                Session["ArrProvinceID" + Group] = ArrProvinceID;
                Session["ArrProvinceName" + Group] = ArrProvinceNAME;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindItemOutlet :: " + ex.Message);
        }
    }

    public void BindEditData()
    {
        OutBoundEventMasterBLL outBoundEventMasterBLL;
        OutboundEventBLL outboundEventBLL;
        EventPlatformBLL eventPlatformBLL;
        EventOutletBLL eventOutletBLL;
        EventProvinceBLL eventProvinceBLL;
        EventVisitConditionBLL eventVisitConditionBLL;
        DataTable dtoutBoundEventMaster, dtoutboundEvent, dtPlatform, dtOutlet, dtProvince, dtVisitCondition;
        DataRow RowEventMaster, RowEvent;
        DateTime EventDateFrom, EventDateTo;
        string SourdEventDateFrom, SourdEventDateTo;

        try
        {
            outBoundEventMasterBLL = new OutBoundEventMasterBLL();
            outboundEventBLL = new OutboundEventBLL();
            eventPlatformBLL = new EventPlatformBLL();
            eventOutletBLL = new EventOutletBLL();
            eventProvinceBLL = new EventProvinceBLL();
            eventVisitConditionBLL = new EventVisitConditionBLL();

            EventId = Request.QueryString["EventId"].ToString();
            dtoutBoundEventMaster = outBoundEventMasterBLL.SelectAllByEventID(int.Parse(EventId));
            dtoutboundEvent = outboundEventBLL.SelectByEventID(int.Parse(EventId));
            RowEventMaster = dtoutBoundEventMaster.Rows[0];

            SourdEventDateFrom = RowEventMaster["eventDateFrom"].ToString();
            SourdEventDateTo = RowEventMaster["eventDateTo"].ToString();

            EventDateFrom = new DateTime();
            EventDateTo = new DateTime();

            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
            dateInfo.ShortDatePattern = "MM/dd/yyyy";
            EventDateFrom = Convert.ToDateTime(SourdEventDateFrom, dateInfo);
            EventDateTo = Convert.ToDateTime(SourdEventDateTo, dateInfo);

            SourdEventDateFrom = EventDateFrom.ToString("dd/MM/yyyy");
            SourdEventDateTo = EventDateTo.ToString("dd/MM/yyyy");

            tab1_dpEventDateFrom.Text = SourdEventDateFrom;
            tab1_dpEventDateTo.Text = SourdEventDateTo;

            tab1_txtEventName.Text = RowEventMaster["eventName"].ToString();
            chkExcludeBlockList.Checked = (RowEventMaster["excludeBlockList"].ToString() == "T") ? true : false;


            if (dtoutboundEvent.Rows.Count > 0)
            {
                for (int j = 0; j < dtoutboundEvent.Rows.Count; j++)
                {
                    RowEvent = dtoutboundEvent.Rows[j];

                    Group = RowEvent["dataGroup"].ToString();

                    #region BinData Tab1

                    if (Group == "1")
                    {
                        dtPlatform = eventPlatformBLL.SelectByEventIDAndDataGroup(int.Parse(EventId), int.Parse(Group));
                        dtOutlet = eventOutletBLL.SelectByEventIDAndDataGroup(int.Parse(EventId), int.Parse(Group));
                        dtProvince = eventProvinceBLL.SelectByEventIDAndDataGroup(int.Parse(EventId), int.Parse(Group));
                        dtVisitCondition = eventVisitConditionBLL.SelectByEventIDAndDataGroup(int.Parse(EventId), int.Parse(Group));

                        if (RowEvent["senderName"].ToString() != "" && RowEvent["senderName"] != null)
                        {
                            tab1_txtSenderName.Text = RowEvent["senderName"].ToString();
                        }

                        if (dtPlatform.Rows.Count > 0)
                        {
                            BindEditItemPlatform(dtPlatform, tab1_lblPlatform, Group);
                        }

                        if (dtOutlet.Rows.Count > 0)
                        {
                            BindEditItemOutlet(dtOutlet, tab1_lblOutlet, Group);
                        }

                        if (dtProvince.Rows.Count > 0)
                        {
                            BindEditItemProvince(dtProvince, tab1_lblProvince, Group);
                        }

                        if (RowEvent["brandId"].ToString() != "" && RowEvent["brandId"] != null)
                        {
                            tab1_ddlBrand.SelectedValue = RowEvent["brandId"].ToString();
                        }

                        if (RowEvent["gender"].ToString() != "" && RowEvent["gender"] != null)
                        {
                            tab1_rbtnListGender.SelectedValue = RowEvent["gender"].ToString();
                        }

                        if (RowEvent["startAge"].ToString() != "" && RowEvent["startAge"] != null)
                        {
                            tab1_txtAgeFrom.Text = RowEvent["startAge"].ToString();
                        }

                        if (RowEvent["endAge"].ToString() != "" && RowEvent["endAge"] != null)
                        {
                            tab1_txtAgeTo.Text = RowEvent["endAge"].ToString();
                        }

                        if (RowEvent["openEmailInLastTime"].ToString() != "" && RowEvent["openEmailInLastTime"] != null)
                        {
                            tab1_txtOpenEmail.Text = RowEvent["openEmailInLastTime"].ToString();
                        }

                        if (RowEvent["receiveEmailInLessTime"].ToString() != "" && RowEvent["receiveEmailInLessTime"] != null)
                        {
                            tab1_txtReceiveEmail.Text = RowEvent["receiveEmailInLessTime"].ToString();
                        }

                        if (RowEvent["emailActive"].ToString() != "" && RowEvent["emailActive"] != null)
                        {
                            tab1_rbtnEmailActive.SelectedValue = RowEvent["emailActive"].ToString();
                        }

                        if (RowEvent["mobileActive"].ToString() != "" && RowEvent["mobileActive"] != null)
                        {
                            tab1_rbtnMobileActive.SelectedValue = RowEvent["mobileActive"].ToString();
                        }

                        if (RowEvent["emailRate"].ToString() != "" && RowEvent["emailRate"] != null)
                        {
                            tab1_txtEmailRate.Text = RowEvent["emailRate"].ToString();
                        }

                        if (RowEvent["includeCallcenterBounce"].ToString() != "" && RowEvent["includeCallcenterBounce"] != null)
                        {
                            tab1_chkIncludeCallCenter.Checked = (RowEvent["includeCallcenterBounce"].ToString() == "T") ? true : false;
                        }

                        if (RowEvent["includeEDMBounce"].ToString() != "" && RowEvent["includeEDMBounce"] != null)
                        {
                            tab1_chkIncludeEDM.Checked = (RowEvent["includeEDMBounce"].ToString() == "T") ? true : false;
                        }

                        if (RowEvent["includeSMSBounce"].ToString() != "" && RowEvent["includeSMSBounce"] != null)
                        {
                            tab1_chkIncludeSMS.Checked = (RowEvent["includeSMSBounce"].ToString() == "T") ? true : false;
                        }

                        # region BindData to chkBrand

                        for (int i = 0; i < tab1_chkListBrand.Items.Count; i++)
                        {
                            bool check;
                            switch (tab1_chkListBrand.Items[i].Value.ToString())
                            {
                                case "brandPre100Pipers":
                                    check = (RowEvent["brandPre100Pipers"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPre100Pipers8Y":
                                    check = (RowEvent["brandPre100Pipers8Y"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreAbsolut":
                                    check = (RowEvent["brandPreAbsolut"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreBaileys":
                                    check = (RowEvent["brandPreBaileys"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreBallentine":
                                    check = (RowEvent["brandPreBallentine"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreBenmore":
                                    check = (RowEvent["brandPreBenmore"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreBlend":
                                    check = (RowEvent["brandPreBlend"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreChivas":
                                    check = (RowEvent["brandPreChivas"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreDewar":
                                    check = (RowEvent["brandPreDewar"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreHennessy":
                                    check = (RowEvent["brandPreHennessy"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreJwBlack":
                                    check = (RowEvent["brandPreJwBlack"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreJwGold":
                                    check = (RowEvent["brandPreJwGold"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreJwGreen":
                                    check = (RowEvent["brandPreJwGreen"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreJwRed":
                                    check = (RowEvent["brandPreJwRed"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreSmirnoff":
                                    check = (RowEvent["brandPreSmirnoff"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreOther":
                                    check = (RowEvent["brandPreOther"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreNotdrink":
                                    check = (RowEvent["brandPreNotDrink"].ToString() == "T") ? true : false;
                                    tab1_chkListBrand.Items[i].Selected = check;
                                    break;
                            }
                        }

                        # endregion

                        #region BindData to chkListValid

                        for (int i = 0; i < tab1_chkListValid.Items.Count; i++)
                        {
                            bool check;
                            switch (tab1_chkListValid.Items[i].Value.ToString())
                            {
                                case "D":
                                    check = (RowEvent["validOptInDiageo"].ToString() == "T") ? true : false;
                                    tab1_chkListValid.Items[i].Selected = check;
                                    break;
                                case "O":
                                    check = (RowEvent["validOptInOutlet"].ToString() == "T") ? true : false;
                                    tab1_chkListValid.Items[i].Selected = check;
                                    break;
                            }
                        }

                        #endregion

                        #region BindData to chkListMonth

                        for (int i = 0; i < tab1_chkListMonth.Items.Count; i++)
                        {
                            bool check;
                            switch (tab1_chkListMonth.Items[i].Text.ToString())
                            {
                                case "January":
                                    check = (RowEvent["monthJan"].ToString() == "T") ? true : false;
                                    tab1_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "Fabuary":
                                    check = (RowEvent["monthFeb"].ToString() == "T") ? true : false;
                                    tab1_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "March":
                                    check = (RowEvent["monthMar"].ToString() == "T") ? true : false;
                                    tab1_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "April":
                                    check = (RowEvent["monthApr"].ToString() == "T") ? true : false;
                                    tab1_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "May":
                                    check = (RowEvent["monthMay"].ToString() == "T") ? true : false;
                                    tab1_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "June":
                                    check = (RowEvent["monthJun"].ToString() == "T") ? true : false;
                                    tab1_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "July":
                                    check = (RowEvent["monthJan"].ToString() == "T") ? true : false;
                                    tab1_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "August":
                                    check = (RowEvent["monthJul"].ToString() == "T") ? true : false;
                                    tab1_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "September":
                                    check = (RowEvent["monthSep"].ToString() == "T") ? true : false;
                                    tab1_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "October":
                                    check = (RowEvent["monthOct"].ToString() == "T") ? true : false;
                                    tab1_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "November":
                                    check = (RowEvent["monthNov"].ToString() == "T") ? true : false;
                                    tab1_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "December":
                                    check = (RowEvent["monthDec"].ToString() == "T") ? true : false;
                                    tab1_chkListMonth.Items[i].Selected = check;
                                    break;
                            }
                        }
                        #endregion

                        /// BindData VisitCondition///
                        if (dtVisitCondition.Rows.Count > 0)
                        {
                            TabNumber = 1;
                            dtCondition = dtVisitCondition;
                            tab1_dgCondition.DataSource = dtVisitCondition;
                            tab1_dgCondition.DataBind();
                        }
                        ///------------------------///

                    }

                    #endregion
                    #region BindData Tab2

                    else if (Group == "2")
                    {
                        ibtnAddTab_Click(Events, null);
                        dtPlatform = eventPlatformBLL.SelectByEventIDAndDataGroup(int.Parse(EventId), int.Parse(Group));
                        dtOutlet = eventOutletBLL.SelectByEventIDAndDataGroup(int.Parse(EventId), int.Parse(Group));
                        dtProvince = eventProvinceBLL.SelectByEventIDAndDataGroup(int.Parse(EventId), int.Parse(Group));
                        dtVisitCondition = eventVisitConditionBLL.SelectByEventIDAndDataGroup(int.Parse(EventId), int.Parse(Group));

                        if (RowEvent["senderName"].ToString() != "" && RowEvent["senderName"] != null)
                        {
                            tab2_txtSenderName.Text = RowEvent["senderName"].ToString();
                        }

                        if (dtPlatform.Rows.Count > 0)
                        {
                            BindEditItemPlatform(dtPlatform, tab2_lblPlatform, Group);
                        }

                        if (dtOutlet.Rows.Count > 0)
                        {
                            BindEditItemOutlet(dtOutlet, tab2_lblOutlet, Group);
                        }

                        if (dtProvince.Rows.Count > 0)
                        {
                            BindEditItemProvince(dtProvince, tab2_lblProvince, Group);
                        }

                        if (RowEvent["brandId"].ToString() != "" && RowEvent["brandId"] != null)
                        {
                            tab2_ddlBrand.SelectedValue = RowEvent["brandId"].ToString();
                        }

                        if (RowEvent["gender"].ToString() != "" && RowEvent["gender"] != null)
                        {
                            tab2_rbtnListGender.SelectedValue = RowEvent["gender"].ToString();
                        }

                        if (RowEvent["startAge"].ToString() != "" && RowEvent["startAge"] != null)
                        {
                            tab2_txtAgeFrom.Text = RowEvent["startAge"].ToString();
                        }

                        if (RowEvent["endAge"].ToString() != "" && RowEvent["endAge"] != null)
                        {
                            tab2_txtAgeTo.Text = RowEvent["endAge"].ToString();
                        }

                        if (RowEvent["openEmailInLastTime"].ToString() != "" && RowEvent["openEmailInLastTime"] != null)
                        {
                            tab2_txtOpenEmail.Text = RowEvent["openEmailInLastTime"].ToString();
                        }

                        if (RowEvent["receiveEmailInLessTime"].ToString() != "" && RowEvent["receiveEmailInLessTime"] != null)
                        {
                            tab2_txtReceiveEmail.Text = RowEvent["receiveEmailInLessTime"].ToString();
                        }

                        if (RowEvent["emailActive"].ToString() != "" && RowEvent["emailActive"] != null)
                        {
                            tab2_rbtnEmailActive.SelectedValue = RowEvent["emailActive"].ToString();
                        }

                        if (RowEvent["mobileActive"].ToString() != "" && RowEvent["mobileActive"] != null)
                        {
                            tab2_rbtnMobileActive.SelectedValue = RowEvent["mobileActive"].ToString();
                        }

                        if (RowEvent["emailRate"].ToString() != "" && RowEvent["emailRate"] != null)
                        {
                            tab2_txtEmailRate.Text = RowEvent["emailRate"].ToString();
                        }

                        if (RowEvent["includeCallcenterBounce"].ToString() != "" && RowEvent["includeCallcenterBounce"] != null)
                        {
                            tab2_chkIncludeCallCenter.Checked = (RowEvent["includeCallcenterBounce"].ToString() == "T") ? true : false;
                        }

                        if (RowEvent["includeEDMBounce"].ToString() != "" && RowEvent["includeEDMBounce"] != null)
                        {
                            tab2_chkIncludeEDM.Checked = (RowEvent["includeEDMBounce"].ToString() == "T") ? true : false;
                        }

                        if (RowEvent["includeSMSBounce"].ToString() != "" && RowEvent["includeSMSBounce"] != null)
                        {
                            tab2_chkIncludeSMS.Checked = (RowEvent["includeSMSBounce"].ToString() == "T") ? true : false;
                        }

                        # region BindData to chkBrand

                        for (int i = 0; i < tab2_chkListBrand.Items.Count; i++)
                        {
                            bool check;
                            switch (tab2_chkListBrand.Items[i].Value.ToString())
                            {
                                case "brandPre100Pipers":
                                    check = (RowEvent["brandPre100Pipers"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPre100Pipers8Y":
                                    check = (RowEvent["brandPre100Pipers8Y"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreAbsolut":
                                    check = (RowEvent["brandPreAbsolut"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreBaileys":
                                    check = (RowEvent["brandPreBaileys"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreBallentine":
                                    check = (RowEvent["brandPreBallentine"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreBenmore":
                                    check = (RowEvent["brandPreBenmore"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreBlend":
                                    check = (RowEvent["brandPreBlend"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreChivas":
                                    check = (RowEvent["brandPreChivas"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreDewar":
                                    check = (RowEvent["brandPreDewar"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreHennessy":
                                    check = (RowEvent["brandPreHennessy"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreJwBlack":
                                    check = (RowEvent["brandPreJwBlack"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreJwGold":
                                    check = (RowEvent["brandPreJwGold"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreJwGreen":
                                    check = (RowEvent["brandPreJwGreen"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreJwRed":
                                    check = (RowEvent["brandPreJwRed"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreSmirnoff":
                                    check = (RowEvent["brandPreSmirnoff"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreOther":
                                    check = (RowEvent["brandPreOther"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreNotdrink":
                                    check = (RowEvent["brandPreNotDrink"].ToString() == "T") ? true : false;
                                    tab2_chkListBrand.Items[i].Selected = check;
                                    break;
                            }
                        }

                        # endregion

                        #region BindData to chkListValid

                        for (int i = 0; i < tab2_chkListValid.Items.Count; i++)
                        {
                            bool check;
                            switch (tab2_chkListValid.Items[i].Value.ToString())
                            {
                                case "D":
                                    check = (RowEvent["validOptInDiageo"].ToString() == "T") ? true : false;
                                    tab2_chkListValid.Items[i].Selected = check;
                                    break;
                                case "O":
                                    check = (RowEvent["validOptInOutlet"].ToString() == "T") ? true : false;
                                    tab2_chkListValid.Items[i].Selected = check;
                                    break;
                            }
                        }

                        #endregion

                        #region BindData to chkListMonth

                        for (int i = 0; i < tab2_chkListMonth.Items.Count; i++)
                        {
                            bool check;
                            switch (tab2_chkListMonth.Items[i].Text.ToString())
                            {
                                case "January":
                                    check = (RowEvent["monthJan"].ToString() == "T") ? true : false;
                                    tab2_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "Fabuary":
                                    check = (RowEvent["monthFeb"].ToString() == "T") ? true : false;
                                    tab2_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "March":
                                    check = (RowEvent["monthMar"].ToString() == "T") ? true : false;
                                    tab2_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "April":
                                    check = (RowEvent["monthApr"].ToString() == "T") ? true : false;
                                    tab2_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "May":
                                    check = (RowEvent["monthMay"].ToString() == "T") ? true : false;
                                    tab2_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "June":
                                    check = (RowEvent["monthJun"].ToString() == "T") ? true : false;
                                    tab2_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "July":
                                    check = (RowEvent["monthJan"].ToString() == "T") ? true : false;
                                    tab2_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "August":
                                    check = (RowEvent["monthJul"].ToString() == "T") ? true : false;
                                    tab2_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "September":
                                    check = (RowEvent["monthSep"].ToString() == "T") ? true : false;
                                    tab2_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "October":
                                    check = (RowEvent["monthOct"].ToString() == "T") ? true : false;
                                    tab2_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "November":
                                    check = (RowEvent["monthNov"].ToString() == "T") ? true : false;
                                    tab2_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "December":
                                    check = (RowEvent["monthDec"].ToString() == "T") ? true : false;
                                    tab2_chkListMonth.Items[i].Selected = check;
                                    break;
                            }
                        }
                        #endregion

                        /// BindData VisitCondition///
                        if (dtVisitCondition.Rows.Count > 0)
                        {
                            TabNumber = 2;
                            dtCondition = dtVisitCondition;
                            tab2_dgCondition.DataSource = dtVisitCondition;
                            tab2_dgCondition.DataBind();
                        }
                        ///------------------------///

                    }
                    #endregion
                    #region BindData Tab3
                    else if (Group == "3")
                    {
                        ibtnAddTab_Click(Events, null);
                        dtPlatform = eventPlatformBLL.SelectByEventIDAndDataGroup(int.Parse(EventId), int.Parse(Group));
                        dtOutlet = eventOutletBLL.SelectByEventIDAndDataGroup(int.Parse(EventId), int.Parse(Group));
                        dtProvince = eventProvinceBLL.SelectByEventIDAndDataGroup(int.Parse(EventId), int.Parse(Group));
                        dtVisitCondition = eventVisitConditionBLL.SelectByEventIDAndDataGroup(int.Parse(EventId), int.Parse(Group));

                        if (RowEvent["senderName"].ToString() != "" && RowEvent["senderName"] != null)
                        {
                            tab3_txtSenderName.Text = RowEvent["senderName"].ToString();
                        }

                        if (dtPlatform.Rows.Count > 0)
                        {
                            BindEditItemPlatform(dtPlatform, tab3_lblPlatform, Group);
                        }

                        if (dtOutlet.Rows.Count > 0)
                        {
                            BindEditItemOutlet(dtOutlet, tab3_lblOutlet, Group);
                        }

                        if (dtProvince.Rows.Count > 0)
                        {
                            BindEditItemProvince(dtProvince, tab3_lblProvince, Group);
                        }

                        if (RowEvent["brandId"].ToString() != "" && RowEvent["brandId"] != null)
                        {
                            tab3_ddlBrand.SelectedValue = RowEvent["brandId"].ToString();
                        }

                        if (RowEvent["gender"].ToString() != "" && RowEvent["gender"] != null)
                        {
                            tab3_rbtnListGender.SelectedValue = RowEvent["gender"].ToString();
                        }

                        if (RowEvent["startAge"].ToString() != "" && RowEvent["startAge"] != null)
                        {
                            tab3_txtAgeFrom.Text = RowEvent["startAge"].ToString();
                        }

                        if (RowEvent["endAge"].ToString() != "" && RowEvent["endAge"] != null)
                        {
                            tab3_txtAgeTo.Text = RowEvent["endAge"].ToString();
                        }

                        if (RowEvent["openEmailInLastTime"].ToString() != "" && RowEvent["openEmailInLastTime"] != null)
                        {
                            tab3_txtOpenEmail.Text = RowEvent["openEmailInLastTime"].ToString();
                        }

                        if (RowEvent["receiveEmailInLessTime"].ToString() != "" && RowEvent["receiveEmailInLessTime"] != null)
                        {
                            tab3_txtReceiveEmail.Text = RowEvent["receiveEmailInLessTime"].ToString();
                        }

                        if (RowEvent["emailActive"].ToString() != "" && RowEvent["emailActive"] != null)
                        {
                            tab3_rbtnEmailActive.SelectedValue = RowEvent["emailActive"].ToString();
                        }

                        if (RowEvent["mobileActive"].ToString() != "" && RowEvent["mobileActive"] != null)
                        {
                            tab3_rbtnMobileActive.SelectedValue = RowEvent["mobileActive"].ToString();
                        }

                        if (RowEvent["emailRate"].ToString() != "" && RowEvent["emailRate"] != null)
                        {
                            tab3_txtEmailRate.Text = RowEvent["emailRate"].ToString();
                        }

                        if (RowEvent["includeCallcenterBounce"].ToString() != "" && RowEvent["includeCallcenterBounce"] != null)
                        {
                            tab3_chkIncludeCallCenter.Checked = (RowEvent["includeCallcenterBounce"].ToString() == "T") ? true : false;
                        }

                        if (RowEvent["includeEDMBounce"].ToString() != "" && RowEvent["includeEDMBounce"] != null)
                        {
                            tab3_chkIncludeEDM.Checked = (RowEvent["includeEDMBounce"].ToString() == "T") ? true : false;
                        }

                        if (RowEvent["includeSMSBounce"].ToString() != "" && RowEvent["includeSMSBounce"] != null)
                        {
                            tab3_chkIncludeSMS.Checked = (RowEvent["includeSMSBounce"].ToString() == "T") ? true : false;
                        }

                        # region BindData to chkBrand

                        for (int i = 0; i < tab3_chkListBrand.Items.Count; i++)
                        {
                            bool check;
                            switch (tab3_chkListBrand.Items[i].Value.ToString())
                            {
                                case "brandPre100Pipers":
                                    check = (RowEvent["brandPre100Pipers"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPre100Pipers8Y":
                                    check = (RowEvent["brandPre100Pipers8Y"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreAbsolut":
                                    check = (RowEvent["brandPreAbsolut"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreBaileys":
                                    check = (RowEvent["brandPreBaileys"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreBallentine":
                                    check = (RowEvent["brandPreBallentine"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreBenmore":
                                    check = (RowEvent["brandPreBenmore"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreBlend":
                                    check = (RowEvent["brandPreBlend"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreChivas":
                                    check = (RowEvent["brandPreChivas"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreDewar":
                                    check = (RowEvent["brandPreDewar"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreHennessy":
                                    check = (RowEvent["brandPreHennessy"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreJwBlack":
                                    check = (RowEvent["brandPreJwBlack"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreJwGold":
                                    check = (RowEvent["brandPreJwGold"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreJwGreen":
                                    check = (RowEvent["brandPreJwGreen"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreJwRed":
                                    check = (RowEvent["brandPreJwRed"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreSmirnoff":
                                    check = (RowEvent["brandPreSmirnoff"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreOther":
                                    check = (RowEvent["brandPreOther"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                                case "brandPreNotdrink":
                                    check = (RowEvent["brandPreNotDrink"].ToString() == "T") ? true : false;
                                    tab3_chkListBrand.Items[i].Selected = check;
                                    break;
                            }
                        }

                        # endregion

                        #region BindData to chkListValid

                        for (int i = 0; i < tab3_chkListValid.Items.Count; i++)
                        {
                            bool check;
                            switch (tab3_chkListValid.Items[i].Value.ToString())
                            {
                                case "D":
                                    check = (RowEvent["validOptInDiageo"].ToString() == "T") ? true : false;
                                    tab3_chkListValid.Items[i].Selected = check;
                                    break;
                                case "O":
                                    check = (RowEvent["validOptInOutlet"].ToString() == "T") ? true : false;
                                    tab3_chkListValid.Items[i].Selected = check;
                                    break;
                            }
                        }

                        #endregion

                        #region BindData to chkListMonth

                        for (int i = 0; i < tab3_chkListMonth.Items.Count; i++)
                        {
                            bool check;
                            switch (tab3_chkListMonth.Items[i].Text.ToString())
                            {
                                case "January":
                                    check = (RowEvent["monthJan"].ToString() == "T") ? true : false;
                                    tab3_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "Fabuary":
                                    check = (RowEvent["monthFeb"].ToString() == "T") ? true : false;
                                    tab3_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "March":
                                    check = (RowEvent["monthMar"].ToString() == "T") ? true : false;
                                    tab3_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "April":
                                    check = (RowEvent["monthApr"].ToString() == "T") ? true : false;
                                    tab3_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "May":
                                    check = (RowEvent["monthMay"].ToString() == "T") ? true : false;
                                    tab3_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "June":
                                    check = (RowEvent["monthJun"].ToString() == "T") ? true : false;
                                    tab3_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "July":
                                    check = (RowEvent["monthJan"].ToString() == "T") ? true : false;
                                    tab3_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "August":
                                    check = (RowEvent["monthJul"].ToString() == "T") ? true : false;
                                    tab3_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "September":
                                    check = (RowEvent["monthSep"].ToString() == "T") ? true : false;
                                    tab3_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "October":
                                    check = (RowEvent["monthOct"].ToString() == "T") ? true : false;
                                    tab3_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "November":
                                    check = (RowEvent["monthNov"].ToString() == "T") ? true : false;
                                    tab3_chkListMonth.Items[i].Selected = check;
                                    break;
                                case "December":
                                    check = (RowEvent["monthDec"].ToString() == "T") ? true : false;
                                    tab3_chkListMonth.Items[i].Selected = check;
                                    break;
                            }
                        }
                        #endregion

                        /// BindData VisitCondition///
                        if (dtVisitCondition.Rows.Count > 0)
                        {
                            TabNumber = 3;
                            dtCondition = dtVisitCondition;
                            tab3_dgCondition.DataSource = dtVisitCondition;
                            tab3_dgCondition.DataBind();
                        }
                        ///------------------------///

                    }
                    #endregion
                }
            }
            else
            {

            }

        }
        catch (Exception ex)
        {
            throw new Exception("BindData :: " + ex.Message);
        }
    }

    private string ValidateDataCondition(DropDownList ddlType, DropDownList ddlOutlet, TextBox dpDateFrom, TextBox dpDateTo,
                                    TextBox txtLastActivity, TextBox txtVisit, DataGrid dgCondition)
    {
        bool ValidateData = false;
        bool ValidateFormat = false;
        try
        {

            if (ddlType.SelectedIndex == 1)
            {
                if ((dpDateFrom.Text != "") && (dpDateTo.Text != ""))
                {
                    DateTime TempEventDateFrom, TempEventDateTo;
                    if (!((DateTime.TryParseExact(dpDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TempEventDateFrom) && (DateTime.TryParseExact(dpDateTo.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TempEventDateTo)))))
                    {
                        JsClientAlert("DateFromOrDateTo", " Format Date In DateFrom Or DateTo Incorrect ");
                        ValidateFormat = false;
                    }
                    else
                    {
                        ValidateData = true;
                        if (ConvertDatetimeFromDatepicker(dpDateFrom.Text.Trim()) <= ConvertDatetimeFromDatepicker(dpDateTo.Text.Trim()))
                        {
                            ValidateFormat = true;
                        }
                    }
                }
            }
            else if (ddlType.SelectedIndex == 2)
            {
                if (txtLastActivity.Text.Trim() != "")
                {
                    ValidateData = true;
                    ValidateFormat = true;
                }
            }
            else if (ddlType.SelectedIndex == 3)
            {
                if ((dpDateFrom.Text != "") && (dpDateTo.Text != "") && (txtVisit.Text.Trim() != ""))
                {
                    DateTime TempEventDateFrom, TempEventDateTo;
                    if (!((DateTime.TryParseExact(dpDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TempEventDateFrom) && (DateTime.TryParseExact(dpDateTo.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TempEventDateTo)))))
                    {
                        JsClientAlert("DateFromOrDateTo", " Format Date In DateFrom Or DateTo Incorrect ");
                        ValidateFormat = false;
                    }
                    else
                    {
                        ValidateData = true;
                        if (ConvertDatetimeFromDatepicker(dpDateFrom.Text.Trim()) <= ConvertDatetimeFromDatepicker(dpDateTo.Text.Trim()))
                        {
                            ValidateFormat = true;
                        }

                    }
                }
            }

            if (ValidateData && ValidateFormat)
            {
                if ((!dpDateFrom.Text.Trim().Equals("")) && (!dpDateTo.Text.Trim().Equals("")))
                {

                    if (ConvertDatetimeFromDatepicker(dpDateFrom.Text) > ConvertDatetimeFromDatepicker(dpDateTo.Text))
                    {
                        JsClientAlert("Validate", "EventDateTo can\\'t less than EventDateFrom ");
                        return "Date from can\\'t less then Date To";
                    }
                    else
                    {
                        AddDataToGridData(ddlType, ddlOutlet, dpDateFrom, dpDateTo, txtLastActivity, txtVisit, dgCondition);
                        return "";
                    }
                }
                else
                {
                    AddDataToGridData(ddlType, ddlOutlet, dpDateFrom, dpDateTo, txtLastActivity, txtVisit, dgCondition);
                    return "";
                }

            }
            else
            {
                if (!ValidateData)
                    return "Data \\'Register and Visit Condition\\' not complete.";
                if (!ValidateFormat)
                    return "Date from can\\'t less then Date To";
                return "";
            }
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateCondition :: " + ex.Message);
        }
    }

    private void AddDataToGridData(DropDownList ddlType, DropDownList ddlOutlet, TextBox dpDateFrom, TextBox dpDateTo,
                                    TextBox txtLastActivity, TextBox txtVisit, DataGrid dgCondition)
    {
        DataTable dt = new DataTable();
        DataRow dr;
        try
        {
            if (dtCondition == null)
                dtCondition = new DataTable();

            if (dtCondition.Columns.Count == 0)
            {
                dt.Columns.Add("Type");
                dt.Columns.Add("Outlet");
                dt.Columns.Add("DateFrom");
                dt.Columns.Add("DateTo");
                dt.Columns.Add("LastActivity");
                dt.Columns.Add("Visit");
                dt.Columns.Add("TypeID");
                dt.Columns.Add("OutletID");
            }
            else
                dt = dtCondition;

            dr = dt.NewRow();
            dr["Type"] = ddlType.SelectedItem.Text.ToString();
            dr["Outlet"] = ddlOutlet.SelectedItem.Text.ToString();
            dr["DateFrom"] = (dpDateFrom.Text != "") ? dpDateFrom.Text.ToString() : "";
            dr["DateTo"] = (dpDateTo.Text != "") ? dpDateTo.Text.ToString() : "";
            dr["LastActivity"] = (txtLastActivity.Text.Trim() != "") ? txtLastActivity.Text.Trim().ToString() : "";
            dr["Visit"] = (txtVisit.Text.Trim() != "") ? txtVisit.Text.Trim().ToString() : "";
            dr["TypeID"] = ddlType.SelectedValue.ToString();
            dr["OutletID"] = ddlOutlet.SelectedValue.ToString();
            dt.Rows.Add(dr);

            dtCondition = dt;

            dgCondition.DataSource = dtCondition;
            dgCondition.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("AddDataToGridData :: " + ex.Message);
        }
    }

    private void ClearDataCondition(DropDownList ddlType, DropDownList ddlOutlet, TextBox dpDateFrom, TextBox dpDateTo,
                                    TextBox txtLastActivity, TextBox txtVisit)
    {
        try
        {

            //ddlType.SelectedIndex = 0;
            //ddlType.SelectedValue = "0";
            ddlOutlet.SelectedIndex = 0;
            dpDateFrom.Text = "";
            dpDateTo.Text = "";
            txtLastActivity.Text = "";
            txtVisit.Text = "";

            ddlType_SelectedIndexChanged(ddlType, null);
        }
        catch (Exception ex)
        {
            throw new Exception("ClearDataCondition :: " + ex.Message);
        }
    }

    private void RemoveDataGridCondition(DataGrid dgCondition)
    {
        try
        {

            if (dtCondition != null)
            {/*O_LEK*/
                CheckBox chkBox1;
                CheckBox chkBox2;
                CheckBox chkBox3;
                for (int i = dgCondition.Items.Count - 1; i >= 0; i--)
                {
                    if (dgCondition.Items[i].FindControl("tab1_chkDelete") != null)
                    {
                        chkBox1 = (CheckBox)dgCondition.Items[i].FindControl("tab1_chkDelete");
                        if (chkBox1.Checked == true)
                        {
                            dtCondition.Rows.RemoveAt(dtCondition.Rows.IndexOf(dtCondition.Rows[i]));
                        }
                    }
                    if (dgCondition.Items[i].FindControl("tab2_chkDelete") != null)
                    {
                        chkBox2 = (CheckBox)dgCondition.Items[i].FindControl("tab2_chkDelete");
                        if (chkBox2.Checked == true)
                        {
                            dtCondition.Rows.RemoveAt(dtCondition.Rows.IndexOf(dtCondition.Rows[i]));
                        }
                    }
                    if (dgCondition.Items[i].FindControl("tab3_chkDelete") != null)
                    {
                        chkBox3 = (CheckBox)dgCondition.Items[i].FindControl("tab3_chkDelete");
                        if (chkBox3.Checked == true)
                        {
                            dtCondition.Rows.RemoveAt(dtCondition.Rows.IndexOf(dtCondition.Rows[i]));
                        }
                    }
                    //dtCondition.Rows.RemoveAt(dtCondition.Rows.Count - 1);   
                }
                dgCondition.DataSource = dtCondition;
                dgCondition.DataBind();
                /*O_LEK*/
            }
        }
        catch (Exception ex)
        {
            throw new Exception("RemoveDataGridCondition :: " + ex.Message);
        }
    }

    private void ChangeTabStyle()
    {
        try
        {
            JsClientCustom("key", "document.getElementById('tabName" + (tabContainer.ActiveTabIndex + 1).ToString() + "').style.backgroundColor='white'; document.getElementById('tabName" + (tabContainer.ActiveTabIndex + 1).ToString() + "').style.color='black';");
        }
        catch (Exception ex)
        {
            throw new Exception("ChangeTabStyle :: " + ex.Message);
        }
    }

    private void AddEventDataOnNewTab()
    {
        try
        {
            if (tabPanel2.Visible)
            {
                if (trEventNameTab1.Visible == false)
                {
                    trEventNameTab2.Visible = false;
                    trSenderNameTab2.Visible = true;
                    trEventDateFromTab2.Visible = false;
                    trBrandNameTab2.Visible = false;
                }

                tab2_txtEventName.Text = tab1_txtEventName.Text.Trim();
                tab2_dpEventDateFrom.Text = tab1_dpEventDateFrom.Text;
                tab2_dpEventDateTo.Text = tab1_dpEventDateTo.Text;
            }
            if (tabPanel3.Visible)
            {
                if (trEventNameTab2.Visible == false)
                {
                    trEventNameTab3.Visible = false;
                    trSenderNameTab3.Visible = true;
                    trEventDateFromTab3.Visible = false;
                    trBrandNameTab3.Visible = false;
                }

                tab3_txtEventName.Text = tab1_txtEventName.Text.Trim();
                tab3_dpEventDateFrom.Text = tab1_dpEventDateFrom.Text;
                tab3_dpEventDateTo.Text = tab1_dpEventDateTo.Text;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("AddEventDataOnNewTab :: " + ex.Message);
        }
    }

    private bool ValidateSaveData(TextBox txtEventName, TextBox txtSenderName, TextBox dpEventDateFrom, TextBox dpEventDateTo, string DataGroup)
    {
        try
        {
            if (trEventNameTab1.Visible == true)
            {
                if (txtEventName.Text.Trim().Equals(""))
                {
                    JsClientAlert("ValidateSaveData", "You must have \\'Event Name\\' ");
                    return false;
                }
                if (txtSenderName.Text.Trim().Equals(""))
                {
                    JsClientAlert("ValidateSaveData", "You must have \\'Sender Name\\' ");
                    return false;
                }
                if (dpEventDateFrom.Text.Trim().Equals("") && dpEventDateTo.Text.Trim().Equals(""))
                {
                    JsClientAlert("ValidateSaveData", "You must have \\'EventDateFrom\\' and \\'EventDateTo\\' ");
                    return false;
                }
                else
                {
                    DateTime TempEventDateFrom, TempEventDateTo;
                    if (!((DateTime.TryParseExact(dpEventDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TempEventDateFrom) && (DateTime.TryParseExact(dpEventDateTo.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TempEventDateTo)))))
                    {
                        JsClientAlert("StartDate", " Format Date In DateFrom Or DateTo Incorrect ");
                        return false;
                    }
                    else
                    {
                        if (ConvertDatetimeFromDatepicker(dpEventDateFrom.Text) > ConvertDatetimeFromDatepicker(dpEventDateTo.Text))
                        {
                            JsClientAlert("Validate", "EventDateTo can\\'t less than EventDateFrom ");
                            return false;
                        }
                    }
                }
            }

            if (tab1_txtAgeFrom.Text.Trim().Equals("") && tab1_txtAgeTo.Text.Trim().Equals(""))
            {
                return true;
            }
            else
            {
                if (int.Parse(tab1_txtAgeFrom.Text.ToString()) > int.Parse(tab1_txtAgeTo.Text.ToString()))
                {
                    JsClientAlert("Validate", "AgeTo can\\'t less than AgeFrom In DataGroup1");
                    return false;
                }
            }
            if (tab2_txtAgeFrom.Text.Trim().Equals("") && tab2_txtAgeTo.Text.Trim().Equals(""))
            {
                return true;
            }
            else
            {
                if (int.Parse(tab2_txtAgeFrom.Text.ToString()) > int.Parse(tab2_txtAgeTo.Text.ToString()))
                {
                    JsClientAlert("Validate", "AgeTo can\\'t less than AgeFrom In DataGroup2");
                    return false;
                }
            }
            if (tab3_txtAgeFrom.Text.Trim().Equals("") && tab3_txtAgeTo.Text.Trim().Equals(""))
            {
                return true;
            }
            else
            {
                if (int.Parse(tab3_txtAgeFrom.Text.ToString()) > int.Parse(tab3_txtAgeTo.Text.ToString()))
                {
                    JsClientAlert("Validate", "AgeTo can\\'t less than AgeFrom In DataGroup3");
                    return false;
                }
            }

            return true;

        }
        catch (Exception ex)
        {
            throw new Exception("ValidateSaveData :: " + ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Dp", "Javascript:getdatecontrol();", true);

            if (!IsPostBack)
            {
                Mode = Request.QueryString["mode"].ToString();

                TabNumber = tabContainer.ActiveTabIndex + 1;
                ClearSessionInEventPage();
                BindControl();

                tabContainer.ActiveTab = tabPanel1;
                tabPanel2.Visible = false;
                tabPanel3.Visible = false;
                ChangeTabStyle();

                if (Request.QueryString["FromPage"] != null)
                {
                    if (Request.QueryString["FromPage"].ToString() == "Extract")
                    {
                        trEventNameTab1.Visible = false;
                        trSenderNameTab1.Visible = true;
                        trBrandNameTab1.Visible = false;
                        trEventDateFromTab1.Visible = false;

                        trExtractData.Visible = true;
                        trSaveData.Visible = false;
                    }

                    tabContainer.ScrollBars = ScrollBars.None;
                }
                else
                {
                    trExtractData.Visible = false;
                    trSaveData.Visible = true;
                }

                if (Mode.ToLower().Equals("edit"))
                {
                    BindEditData();


                    tab1_txtEventName.ReadOnly = true;
                    tab1_txtEventName.Enabled = true;
                    tab1_txtEventName.BackColor = System.Drawing.Color.LightGray;

                    tab2_txtEventName.ReadOnly = true;
                    tab2_txtEventName.Enabled = true;
                    tab2_txtEventName.BackColor = System.Drawing.Color.LightGray;

                    tab3_txtEventName.ReadOnly = true;
                    tab3_txtEventName.Enabled = true;
                    tab3_txtEventName.BackColor = System.Drawing.Color.LightGray;


                }
                else
                {
                    tab1_txtEventName.ReadOnly = false;
                    tab1_txtEventName.Enabled = true;
                    tab1_txtEventName.BackColor = System.Drawing.Color.White;

                    tab2_txtEventName.ReadOnly = false;
                    tab2_txtEventName.Enabled = true;
                    tab2_txtEventName.BackColor = System.Drawing.Color.White;

                    tab3_txtEventName.ReadOnly = false;
                    tab3_txtEventName.Enabled = true;
                    tab3_txtEventName.BackColor = System.Drawing.Color.White;
                }
            }
            else
            {

                TabNumber = tabContainer.ActiveTabIndex + 1;
                if (TabNumber == 1)
                {
                    TypeCondition(tab1_ddlType, tab1_dpDateFrom, tab1_dpDateTo, tab1_txtLastActivity, tab1_txtVisit, tab1_ddlOutlet);
                }
                else if (TabNumber == 2)
                {
                    TypeCondition(tab2_ddlType, tab2_dpDateFrom, tab2_dpDateTo, tab2_txtLastActivity, tab2_txtVisit, tab2_ddlOutlet);
                }
                else if (TabNumber == 3)
                {
                    TypeCondition(tab3_ddlType, tab3_dpDateFrom, tab3_dpDateTo, tab3_txtLastActivity, tab3_txtVisit, tab3_ddlOutlet);
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtractConsumerData.Page_Load :: " + ex.Message);
        }
    }

    protected void btnPlatform_Click(object sender, EventArgs e)
    {
        try
        {
            TabNumber = tabContainer.ActiveTabIndex + 1;
            string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/ExtractData/PlatFormPopup.aspx?tabNumber=" + TabNumber + "&date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:250px; center:yes; scroll:yes'); document.getElementById('tabContainer_tabPanel" + TabNumber.ToString() + "_tab" + TabNumber.ToString() + "_btnRefresh').click();";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenPopup", window, true);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtractConsumersData.btnPlatform_Click :: " + ex.Message);
        }
    }

    protected void btnOutlet_Click(object sender, EventArgs e)
    {
        try
        {
            TabNumber = tabContainer.ActiveTabIndex + 1;
            string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/ExtractData/OutletPopup.aspx?tabNumber=" + TabNumber + "&date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:500px; center:yes; scroll:yes'); document.getElementById('tabContainer_tabPanel" + TabNumber.ToString() + "_tab" + TabNumber.ToString() + "_btnRefresh').click();";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenPopup", window, true);
            
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtractConsumersData.btnPlatform_Click :: " + ex.Message);
        }
    }

    protected void btnProvince_Click(object sender, EventArgs e)
    {
        try
        {
            TabNumber = tabContainer.ActiveTabIndex + 1;
            string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/ExtractData/ProvincePopup.aspx?tabNumber=" + TabNumber + "&date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:250px; center:yes; scroll:yes'); document.getElementById('tabContainer_tabPanel" + TabNumber.ToString() + "_tab" + TabNumber.ToString() + "_btnRefresh').click();";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenPopup", window, true);

        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtractConsumersData.btnProvince_Click :: " + ex.Message);
        }
    }

    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            TabNumber = tabContainer.ActiveTabIndex + 1;
            if (TabNumber == 1)
            {
                TypeCondition(tab1_ddlType, tab1_dpDateFrom, tab1_dpDateTo, tab1_txtLastActivity, tab1_txtVisit, tab1_ddlOutlet);
            }
            else if (TabNumber == 2)
            {
                TypeCondition(tab2_ddlType, tab2_dpDateFrom, tab2_dpDateTo, tab2_txtLastActivity, tab2_txtVisit, tab2_ddlOutlet);
            }
            else if (TabNumber == 3)
            {
                TypeCondition(tab3_ddlType, tab3_dpDateFrom, tab3_dpDateTo, tab3_txtLastActivity, tab3_txtVisit, tab3_ddlOutlet);
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtractConsumersData.ddlType_SelectedIndexChanged :: " + ex.Message);
        }
    }

    protected void btnAddCondition_Click(object sender, EventArgs e)
    {
        string Msg = "";
        try
        {


            TabNumber = tabContainer.ActiveTabIndex + 1;

            if (TabNumber == 1)
                Msg = ValidateDataCondition(tab1_ddlType, tab1_ddlOutlet, tab1_dpDateFrom,
                    tab1_dpDateTo, tab1_txtLastActivity, tab1_txtVisit, tab1_dgCondition);
            if (TabNumber == 2)
                Msg = ValidateDataCondition(tab2_ddlType, tab2_ddlOutlet, tab2_dpDateFrom,
                    tab2_dpDateTo, tab2_txtLastActivity, tab2_txtVisit, tab2_dgCondition);
            if (TabNumber == 3)
                Msg = ValidateDataCondition(tab3_ddlType, tab3_ddlOutlet, tab3_dpDateFrom,
                    tab3_dpDateTo, tab3_txtLastActivity, tab3_txtVisit, tab3_dgCondition);

            if (Msg != "")
                JsClientAlert("Validate", Msg);
            else
            {//Clear data
                if (TabNumber == 1)
                    ClearDataCondition(tab1_ddlType, tab1_ddlOutlet, tab1_dpDateFrom, tab1_dpDateTo, tab1_txtLastActivity, tab1_txtVisit);
                if (TabNumber == 2)
                    ClearDataCondition(tab2_ddlType, tab2_ddlOutlet, tab2_dpDateFrom, tab2_dpDateTo, tab2_txtLastActivity, tab2_txtVisit);
                if (TabNumber == 3)
                    ClearDataCondition(tab3_ddlType, tab3_ddlOutlet, tab3_dpDateFrom, tab3_dpDateTo, tab3_txtLastActivity, tab3_txtVisit);
            }

        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtractConsumersData.btnAdd_Click :: " + ex.Message);
        }
    }

    protected void btnRemoveCondition_Click(object sender, EventArgs e)
    {
        try
        {
            TabNumber = tabContainer.ActiveTabIndex + 1;

            if (TabNumber == 1)
                RemoveDataGridCondition(tab1_dgCondition);
            if (TabNumber == 2)
                RemoveDataGridCondition(tab2_dgCondition);
            if (TabNumber == 3)
                RemoveDataGridCondition(tab3_dgCondition);
        }
        catch (Exception ex)
        {
            throw new Exception("ExtractConsumersData.btnRemoveCondition_Click :: " + ex.Message);
        }
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        try
        {
            TabNumber = tabContainer.ActiveTabIndex + 1;
            if (TabNumber == 1)
            {
                BindItemPlatform(tab1_lblPlatform);
                BindItemOutlet(tab1_lblOutlet);
                BindItemProvince(tab1_lblProvince);
            }
            else if (TabNumber == 2)
            {
                BindItemPlatform(tab2_lblPlatform);
                BindItemOutlet(tab2_lblOutlet);
                BindItemProvince(tab2_lblProvince);
            }
            else if (TabNumber == 3)
            {
                BindItemPlatform(tab3_lblPlatform);
                BindItemOutlet(tab3_lblOutlet);
                BindItemProvince(tab3_lblProvince);
            }
        }
        catch (Exception ex)
        {
            JsClientCustom("ErrorMessage", "ExtractConsumersData.btnRefresh_Click :: " + ex.Message);
        }
    }

    protected void tabContainer_ActiveTabChanged(object sender, EventArgs e)
    {
        try
        {
            ChangeTabStyle();
            AddEventDataOnNewTab();
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtractConsumersData.tabContainer_ActiveTabChanged :: " + ex.Message);
        }
    }

    protected void ibtnAddTab_Click(object sender, EventArgs e)
    {
        try
        {
            if ((tab1_txtEventName.Text.Trim() != "") && (tab1_dpEventDateFrom.Text != "") && (tab1_dpEventDateTo.Text != ""))
            {
                if (ConvertDatetimeFromDatepicker(tab1_dpEventDateFrom.Text) > ConvertDatetimeFromDatepicker(tab1_dpEventDateTo.Text))
                    JsClientAlert("Validate", "EventDateTo can\\'t less than EventDateFrom.");
                else
                {
                    if (tabPanel2.Visible == false)
                        tabPanel2.Visible = true;
                    else if (tabPanel3.Visible == false)
                        tabPanel3.Visible = true;
                    else
                        JsClientAlert("AddTab", "DataGroup can\\'t add more 3 tabs.");

                    AddEventDataOnNewTab();
                }
            }
            else
            {
                if (trEventDateFromTab1.Visible == false)
                {
                    if (tabPanel2.Visible == false)
                        tabPanel2.Visible = true;
                    else if (tabPanel3.Visible == false)
                        tabPanel3.Visible = true;
                    else
                        JsClientAlert("AddTab", "DataGroup can\\'t add more 3 tabs.");

                    AddEventDataOnNewTab();
                }
                else
                    JsClientAlert("AddTab", "You must have EventName, EventDateFrom and EventDateTo for add new tabs.");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtractConsumersData.ibtnAddTab_Click :: " + ex.Message);
        }
    }

    protected void ibtnRemoveTab_Click(object sender, EventArgs e)
    {
        try
        {
            TabNumber = tabContainer.ActiveTabIndex + 1;
            if (tabPanel3.Visible == true)
            {
                if (TabNumber == 3)
                    tabContainer.ActiveTabIndex = TabNumber - 2;
                tabPanel3.Visible = false;
            }
            else if (tabPanel2.Visible == true)
            {
                if (TabNumber == 2)
                    tabContainer.ActiveTabIndex = TabNumber - 2;
                tabPanel2.Visible = false;
            }
            else
                JsClientAlert("AddTab", "DataGroup1 can\\'t remove.");

            ChangeTabStyle();
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtractConsumersData.ibtnRemoveTab_Click :: " + ex.Message);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        OutboundEventBLL bllOutboundEvent = new OutboundEventBLL();
        DataTable dtOutboundEventMaster = new DataTable("OutboundEventMaster");
        DataTable dtOutboundEvent = new DataTable("OutboundEvent");
        DataTable dtEventPlatform = new DataTable("EventPlatform");
        DataTable dtEventOutlet = new DataTable("EventOutlet");
        DataTable dtEventProvince = new DataTable("EventProvince");
        DataTable dtEventVisitCondition = new DataTable("EventVisitCondition");
        //by O_Lek
        DataTable dtEventDiageoVip = new DataTable("EventDiageoVip");
        DataTable dtEventOutletVip = new DataTable("EventOutletVip");
        //by O_Lek
        DataTable dtCondition;
        ArrayList ArrPlatformID, ArrOutletID, ArrProvinceID;
        string ExcludeBlockList = "", DataGroup = "";
        bool CheckTab1 = false, CheckTab2 = false, CheckTab3 = false;
        try
        {
            ExcludeBlockList = (chkExcludeBlockList.Checked) ? "T" : "F";
            //by O_Lek
            dtEventDiageoVip = (DataTable)Session["DiageoVIPExcel"];
            dtEventOutletVip = (DataTable)Session["OutletVIPExcel"];
            //by O_Lek
            if (tabPanel1.Visible)
            {
                CheckTab1 = true;
                DataGroup = "1";
                ArrPlatformID = ((ArrayList)Session["ArrPlatformID" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrPlatformID" + DataGroup];
                ArrOutletID = ((ArrayList)Session["ArrOutletID" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrOutletID" + DataGroup];
                ArrProvinceID = ((ArrayList)Session["ArrProvinceID" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrProvinceID" + DataGroup];
                dtCondition = ((DataTable)Session["dtCondition" + DataGroup] == null) ? new DataTable() : (DataTable)Session["dtCondition" + DataGroup];
                if (ValidateSaveData(tab1_txtEventName, tab1_txtSenderName, tab1_dpEventDateFrom, tab1_dpEventDateTo, tabPanel1.HeaderText.ToString()))
                {
                    dtOutboundEventMaster = GetDataOutboundEventMaster(dtOutboundEventMaster, tab1_txtEventName, tab1_dpEventDateFrom, tab1_dpEventDateTo, ExcludeBlockList);
                    dtOutboundEvent = GetDataOutboundEvent(dtOutboundEvent, DataGroup, tab1_txtEventName, tab1_txtSenderName, tab1_ddlBrand, tab1_chkListBrand, tab1_rbtnListGender, tab1_chkListValid, tab1_txtAgeFrom,
                                        tab1_txtAgeTo, tab1_chkListMonth, tab1_txtOpenEmail, tab1_txtReceiveEmail, tab1_rbtnEmailActive, tab1_rbtnMobileActive,
                                        tab1_txtEmailRate, tab1_chkIncludeCallCenter, tab1_chkIncludeEDM, tab1_chkIncludeSMS);
                    dtEventPlatform = GetDataEventPlatform(dtEventPlatform, DataGroup, ArrPlatformID);
                    dtEventOutlet = GetDataEventOutlet(dtEventOutlet, DataGroup, ArrOutletID);
                    dtEventProvince = GetDataEventProvince(dtEventProvince, DataGroup, ArrProvinceID);
                    dtEventVisitCondition = GetDataEventVisitCondition(dtEventVisitCondition, DataGroup, dtCondition);
                }
                else
                    return;
            }
            if (tabPanel2.Visible)
            {
                CheckTab2 = true;
                DataGroup = "2";
                ArrPlatformID = ((ArrayList)Session["ArrPlatform" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrPlatform" + DataGroup];
                ArrOutletID = ((ArrayList)Session["ArrOutletID" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrOutletID" + DataGroup];
                ArrProvinceID = ((ArrayList)Session["ArrProvinceID" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrProvinceID" + DataGroup];
                dtCondition = ((DataTable)Session["dtCondition" + DataGroup] == null) ? new DataTable() : (DataTable)Session["dtCondition" + DataGroup];
                if (ValidateSaveData(tab2_txtEventName, tab2_txtSenderName, tab2_dpEventDateFrom, tab2_dpEventDateTo, tabPanel2.HeaderText.ToString()))
                {
                    dtOutboundEventMaster = GetDataOutboundEventMaster(dtOutboundEventMaster, tab2_txtEventName, tab2_dpEventDateFrom, tab2_dpEventDateTo, ExcludeBlockList);
                    dtOutboundEvent = GetDataOutboundEvent(dtOutboundEvent, DataGroup, tab2_txtEventName, tab2_txtSenderName, tab2_ddlBrand, tab2_chkListBrand, tab2_rbtnListGender, tab2_chkListValid, tab2_txtAgeFrom,
                                        tab2_txtAgeTo, tab2_chkListMonth, tab2_txtOpenEmail, tab2_txtReceiveEmail, tab2_rbtnEmailActive, tab2_rbtnMobileActive,
                                        tab2_txtEmailRate, tab2_chkIncludeCallCenter, tab2_chkIncludeEDM, tab2_chkIncludeSMS);
                    dtEventPlatform = GetDataEventPlatform(dtEventPlatform, DataGroup, ArrPlatformID);
                    dtEventOutlet = GetDataEventOutlet(dtEventOutlet, DataGroup, ArrOutletID);
                    dtEventProvince = GetDataEventProvince(dtEventProvince, DataGroup, ArrProvinceID);
                    dtEventVisitCondition = GetDataEventVisitCondition(dtEventVisitCondition, DataGroup, dtCondition);
                }
                else
                    return;
            }
            if (tabPanel3.Visible)
            {
                CheckTab3 = true;
                DataGroup = "3";
                ArrPlatformID = ((ArrayList)Session["ArrPlatform" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrPlatform" + DataGroup];
                ArrOutletID = ((ArrayList)Session["ArrOutletID" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrOutletID" + DataGroup];
                ArrProvinceID = ((ArrayList)Session["ArrProvinceID" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrProvinceID" + DataGroup];
                dtCondition = ((DataTable)Session["dtCondition" + DataGroup] == null) ? new DataTable() : (DataTable)Session["dtCondition" + DataGroup];
                if (ValidateSaveData(tab3_txtEventName, tab3_txtSenderName, tab3_dpEventDateFrom, tab3_dpEventDateTo, tabPanel3.HeaderText.ToString()))
                {
                    dtOutboundEventMaster = GetDataOutboundEventMaster(dtOutboundEventMaster, tab3_txtEventName, tab3_dpEventDateFrom, tab3_dpEventDateTo, ExcludeBlockList);
                    dtOutboundEvent = GetDataOutboundEvent(dtOutboundEvent, DataGroup, tab3_txtEventName, tab3_txtSenderName, tab3_ddlBrand, tab3_chkListBrand, tab3_rbtnListGender, tab3_chkListValid, tab3_txtAgeFrom,
                                        tab3_txtAgeTo, tab3_chkListMonth, tab3_txtOpenEmail, tab3_txtReceiveEmail, tab3_rbtnEmailActive, tab3_rbtnMobileActive,
                                        tab3_txtEmailRate, tab3_chkIncludeCallCenter, tab3_chkIncludeEDM, tab3_chkIncludeSMS);
                    dtEventPlatform = GetDataEventPlatform(dtEventPlatform, DataGroup, ArrPlatformID);
                    dtEventOutlet = GetDataEventOutlet(dtEventOutlet, DataGroup, ArrOutletID);
                    dtEventProvince = GetDataEventProvince(dtEventProvince, DataGroup, ArrProvinceID);
                    dtEventVisitCondition = GetDataEventVisitCondition(dtEventVisitCondition, DataGroup, dtCondition);
                }
                else
                    return;
            }

            if (Request.QueryString["Mode"].ToString() == "Edit")
            {
                bllOutboundEvent.EventID = int.Parse(Request.QueryString["EventId"].ToString());
                if (bllOutboundEvent.UpdateOutboundEvent(dtOutboundEventMaster, dtOutboundEvent, dtEventPlatform, dtEventOutlet,
                                            dtEventProvince, dtEventVisitCondition, Mode, dtEventDiageoVip/*by O_Lek*/, dtEventOutletVip/*by O_Lek*/))
                {
                    ClearSessionInEventPage();
                    JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();window.opener.document.getElementById('btnSearch').click();");
                }
            }
            else
            {
                if (bllOutboundEvent.InsertOutboundEvent(dtOutboundEventMaster, dtOutboundEvent, dtEventPlatform, dtEventOutlet,
                                            dtEventProvince, dtEventVisitCondition, Mode, dtEventDiageoVip/*by O_Lek*/, dtEventOutletVip/*by O_Lek*/))
                {
                    ClearSessionInEventPage();
                    JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();window.opener.document.getElementById('btnSearch').click();");
                }
            }

        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtractConsumersData.btnSave_Click :: " + ex.Message);
        }
        finally
        {

        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            JsClientCustom("WindowClose", "window.close();");
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroup.btnCancel_Click :: " + ex.Message);
        }
    }

    protected void ClearCheck(object sender, EventArgs e)
    {
        try
        {
            CheckBox chkAll = sender as CheckBox;
            foreach (DataGridItem item in tab1_dgCondition.Items)
            {
                CheckBox tab1_chkDelete = item.FindControl("tab1_chkDelete") as CheckBox;
                if (chkAll.Checked == true)
                    tab1_chkDelete.Checked = true;
                else
                    tab1_chkDelete.Checked = false;
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ClearCheck :: " + ex.Message);
        }
    }

    #endregion

    // O_Lek
    protected void btnBrowseDiageo_Click(object sender, EventArgs e)
    {
        try
        {
            TabNumber = tabContainer.ActiveTabIndex + 1;
            string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/ExtractData/EventVipPopUp.aspx?tabNumber=" + TabNumber + "&date=" + DateTime.Now.ToString() + "&type=1','mywindow','dialogWidth:1000px; dialogHeight:500px; center:yes; scroll:yes'); document.getElementById('tabContainer_tabPanel" + TabNumber.ToString() + "_tab" + TabNumber.ToString() + "_btnRefresh').click(); document.getElementById('btnDiageoVip').click();";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenPopupDiageo", window, true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnDiageoVip_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["DiageoVipPath"] != null)
                lblDiageoVIPPath.Text = Session["DiageoVipPath"].ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnOutletVip_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["OutletVIPPath"] == null)
                Session["OutletVIPPath"] = null;
            lblOutletVipPath.Text = Session["OutletVIPPath"].ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnBrowseOutlet_Click(object sender, EventArgs e)
    {
        try
        {
            TabNumber = tabContainer.ActiveTabIndex + 1;
            string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/ExtractData/EventVipPopUp.aspx?tabNumber=" + TabNumber + "&date=" + DateTime.Now.ToString() + "&type=2','mywindow','dialogWidth:1000px; dialogHeight:500px; center:yes; scroll:yes'); document.getElementById('tabContainer_tabPanel" + TabNumber.ToString() + "_tab" + TabNumber.ToString() + "_btnRefresh').click(); document.getElementById('btnOutletVip').click();";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenPopupOutlet", window, true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    //by O_Lek

    protected void btnExtractData_Click(object sender, EventArgs e)
    {
        OutboundEventBLL bllOutboundEvent = new OutboundEventBLL();
        DataTable dtOutboundEventMaster = new DataTable("OutboundEventMaster");
        DataTable dtOutboundEvent = new DataTable("OutboundEvent");
        DataTable dtEventPlatform = new DataTable("EventPlatform");
        DataTable dtEventOutlet = new DataTable("EventOutlet");
        DataTable dtEventProvince = new DataTable("EventProvince");
        DataTable dtEventVisitCondition = new DataTable("EventVisitCondition");
        DataTable dtEventDiageoVip = new DataTable("EventDiageoVip");
        DataTable dtEventOutletVip = new DataTable("EventOutletVip");
        DataTable dtCondition;
        ArrayList ArrPlatformID, ArrOutletID, ArrProvinceID;
        string ExcludeBlockList = "", DataGroup = "";

        try
        {
            ExcludeBlockList = (chkExcludeBlockList.Checked) ? "T" : "F";
            dtEventDiageoVip = (DataTable)Session["DiageoVIPExcel"];
            dtEventOutletVip = (DataTable)Session["OutletVIPExcel"];

            if (tabPanel1.Visible)
            {
                DataGroup = "1";
                ArrPlatformID = ((ArrayList)Session["ArrPlatformID" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrPlatformID" + DataGroup];
                ArrOutletID = ((ArrayList)Session["ArrOutletID" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrOutletID" + DataGroup];
                ArrProvinceID = ((ArrayList)Session["ArrProvinceID" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrProvinceID" + DataGroup];
                dtCondition = ((DataTable)Session["dtCondition" + DataGroup] == null) ? new DataTable() : (DataTable)Session["dtCondition" + DataGroup];

                dtOutboundEventMaster = GetDataOutboundEventMaster(dtOutboundEventMaster, tab1_txtEventName, tab1_dpEventDateFrom, tab1_dpEventDateTo, ExcludeBlockList);
                if (dtOutboundEventMaster.Rows.Count != 0)
                {
                    dtOutboundEventMaster.Rows[0]["eventName"] = "EXTRACTDATA";
                    dtOutboundEventMaster.Rows[0]["eventDateFrom"] = DateTime.Now;
                    dtOutboundEventMaster.Rows[0]["eventDateTo"] = DateTime.Now;
                }

                dtOutboundEvent = GetDataOutboundEvent(dtOutboundEvent, DataGroup, tab1_txtEventName, tab1_txtSenderName, tab1_ddlBrand, tab1_chkListBrand, tab1_rbtnListGender, tab1_chkListValid, tab1_txtAgeFrom,
                                  tab1_txtAgeTo, tab1_chkListMonth, tab1_txtOpenEmail, tab1_txtReceiveEmail, tab1_rbtnEmailActive, tab1_rbtnMobileActive,
                                  tab1_txtEmailRate, tab1_chkIncludeCallCenter, tab1_chkIncludeEDM, tab1_chkIncludeSMS);
                dtEventPlatform = GetDataEventPlatform(dtEventPlatform, DataGroup, ArrPlatformID);
                dtEventOutlet = GetDataEventOutlet(dtEventOutlet, DataGroup, ArrOutletID);
                dtEventProvince = GetDataEventProvince(dtEventProvince, DataGroup, ArrProvinceID);
                dtEventVisitCondition = GetDataEventVisitCondition(dtEventVisitCondition, DataGroup, dtCondition);
            }
            if (tabPanel2.Visible)
            {
                DataGroup = "2";
                ArrPlatformID = ((ArrayList)Session["ArrPlatform" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrPlatform" + DataGroup];
                ArrOutletID = ((ArrayList)Session["ArrOutletID" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrOutletID" + DataGroup];
                ArrProvinceID = ((ArrayList)Session["ArrProvinceID" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrProvinceID" + DataGroup];
                dtCondition = ((DataTable)Session["dtCondition" + DataGroup] == null) ? new DataTable() : (DataTable)Session["dtCondition" + DataGroup];
                if (ValidateSaveData(tab2_txtEventName, tab2_txtSenderName, tab2_dpEventDateFrom, tab2_dpEventDateTo, tabPanel2.HeaderText.ToString()))
                {
                    dtOutboundEventMaster = GetDataOutboundEventMaster(dtOutboundEventMaster, tab2_txtEventName, tab2_dpEventDateFrom, tab2_dpEventDateTo, ExcludeBlockList);
                    dtOutboundEvent = GetDataOutboundEvent(dtOutboundEvent, DataGroup, tab2_txtEventName, tab2_txtSenderName, tab2_ddlBrand, tab2_chkListBrand, tab2_rbtnListGender, tab2_chkListValid, tab2_txtAgeFrom,
                                      tab2_txtAgeTo, tab2_chkListMonth, tab2_txtOpenEmail, tab2_txtReceiveEmail, tab2_rbtnEmailActive, tab2_rbtnMobileActive,
                                      tab2_txtEmailRate, tab2_chkIncludeCallCenter, tab2_chkIncludeEDM, tab2_chkIncludeSMS);
                    dtEventPlatform = GetDataEventPlatform(dtEventPlatform, DataGroup, ArrPlatformID);
                    dtEventOutlet = GetDataEventOutlet(dtEventOutlet, DataGroup, ArrOutletID);
                    dtEventProvince = GetDataEventProvince(dtEventProvince, DataGroup, ArrProvinceID);
                    dtEventVisitCondition = GetDataEventVisitCondition(dtEventVisitCondition, DataGroup, dtCondition);
                }
                else
                    return;
            }
            if (tabPanel3.Visible)
            {
                DataGroup = "3";
                ArrPlatformID = ((ArrayList)Session["ArrPlatform" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrPlatform" + DataGroup];
                ArrOutletID = ((ArrayList)Session["ArrOutletID" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrOutletID" + DataGroup];
                ArrProvinceID = ((ArrayList)Session["ArrProvinceID" + DataGroup] == null) ? new ArrayList() : (ArrayList)Session["ArrProvinceID" + DataGroup];
                dtCondition = ((DataTable)Session["dtCondition" + DataGroup] == null) ? new DataTable() : (DataTable)Session["dtCondition" + DataGroup];
                if (ValidateSaveData(tab3_txtEventName, tab3_txtSenderName, tab3_dpEventDateFrom, tab3_dpEventDateTo, tabPanel3.HeaderText.ToString()))
                {
                    dtOutboundEventMaster = GetDataOutboundEventMaster(dtOutboundEventMaster, tab3_txtEventName, tab3_dpEventDateFrom, tab3_dpEventDateTo, ExcludeBlockList);
                    dtOutboundEvent = GetDataOutboundEvent(dtOutboundEvent, DataGroup, tab3_txtEventName, tab3_txtSenderName, tab3_ddlBrand, tab3_chkListBrand, tab3_rbtnListGender, tab3_chkListValid, tab3_txtAgeFrom,
                                      tab3_txtAgeTo, tab3_chkListMonth, tab3_txtOpenEmail, tab3_txtReceiveEmail, tab3_rbtnEmailActive, tab3_rbtnMobileActive,
                                      tab3_txtEmailRate, tab3_chkIncludeCallCenter, tab3_chkIncludeEDM, tab3_chkIncludeSMS);
                    dtEventPlatform = GetDataEventPlatform(dtEventPlatform, DataGroup, ArrPlatformID);
                    dtEventOutlet = GetDataEventOutlet(dtEventOutlet, DataGroup, ArrOutletID);
                    dtEventProvince = GetDataEventProvince(dtEventProvince, DataGroup, ArrProvinceID);
                    dtEventVisitCondition = GetDataEventVisitCondition(dtEventVisitCondition, DataGroup, dtCondition);
                }
                else
                    return;
            }

            int eventId = 0;
            if (bllOutboundEvent.InsertOutboundEventExtractData(dtOutboundEventMaster, dtOutboundEvent, dtEventPlatform, dtEventOutlet,
            dtEventProvince, dtEventVisitCondition, Mode, dtEventDiageoVip/*by O_Lek*/, dtEventOutletVip/*by O_Lek*/, ref eventId))
            {
                DataRow itemConfig;
                DataSet dsOutboundConfig = new DataSet();
                DataSet ds = new DataSet();
                string fileName = "";
                ReadOutboundConfigBYOutboundTypeID(ref dsOutboundConfig, eventId);
                foreach (DataRow itemEvent in dsOutboundConfig.Tables[0].DefaultView.ToTable(true, "eventId").Rows)
                {
                    itemConfig = dsOutboundConfig.Tables[0].Select(string.Format("eventId='{0}'", itemEvent["eventId"].ToString()))[0];
                    EventConsumerListForEvent(dsOutboundConfig.Tables[0].Select(string.Format("eventId='{0}'", itemEvent["eventId"].ToString())));

                    fileName = Server.MapPath("~/") + "Extract" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xls";

                    Button senderBtn = sender as Button;
                    
                        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=Excel 12.0;";

                        var myDataGrid = new System.Web.UI.WebControls.DataGrid();
                        myDataGrid.HeaderStyle.Font.Bold = true;

                        string myFile = fileName;
                        var myFileStream = new FileStream(myFile, FileMode.Create, FileAccess.ReadWrite);
                        myFileStream.Close();

                        var myFileStreamAppend = new FileStream(myFile, FileMode.Append, FileAccess.Write);
                        using (var myStreamWriter = new StreamWriter(myFileStreamAppend, Encoding.UTF8))
                        {
                            using (var myHtmlTextWriter = new System.Web.UI.HtmlTextWriter(myStreamWriter))
                            {
                                ReadCountConsumerDataJOINOutbound(ref ds, itemConfig);
                                if (senderBtn.ID != "btnCount")
                                {
                                    decimal round = Convert.ToDecimal(ds.Tables[0].Rows[0][0].ToString()) / Convert.ToDecimal(10000);
                                    round = decimal.Ceiling(round);

                                    for (int i = 1; i <= round; i++)
                                    {
                                        if (i == 1)
                                        {
                                            ReadConsumerDataJOINOutbound(ref ds, itemConfig, 0, 10000);

                                            myDataGrid.DataSource = ds.Tables[0];
                                            myDataGrid.ShowHeader = true;
                                            myDataGrid.DataBind();
                                            myDataGrid.RenderControl(myHtmlTextWriter);
                                        }
                                        else
                                        {
                                            ReadConsumerDataJOINOutbound(ref ds, itemConfig, (i - 1) * 10000, 10000);
                                            myDataGrid.DataSource = ds.Tables[0];
                                            myDataGrid.ShowHeader = false;
                                            myDataGrid.DataBind();
                                            myDataGrid.RenderControl(myHtmlTextWriter);
                                        }

                                        if (i == round)
                                        {
                                            ReadConsumerDataJOINOutboundVIP(ref ds, itemConfig);
                                            myDataGrid.DataSource = ds.Tables[0];
                                            myDataGrid.ShowHeader = false;
                                            myDataGrid.DataBind();
                                            myDataGrid.RenderControl(myHtmlTextWriter);
                                        }
                                    }
                                }
                                else
                                {
                                    JsClientAlert("", "The total amount of record is " + ds.Tables[0].Rows[0][0].ToString() + " records.");
                                    return;
                                }

                                DeleteOutboundExtractData(int.Parse(itemConfig["EventId"].ToString()));
                            }
                        }                   
                }

                byte[] getContent;
                getContent = File.ReadAllBytes(fileName);
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = false;
                Response.BufferOutput = false;
                Response.Charset = "UTF-8";
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Length", getContent.Length.ToString());
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                Response.BinaryWrite(getContent);
                Response.Clear();

                //ClearSessionInEventPage();
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }

            }
        }
        catch (Exception ex)
        {
            ErrorMessage.Text = ex.Message;
        }
        finally
        {

        }
    }

    public void ReadConsumerDataJOINOutbound(ref DataSet ds, DataRow rowConfig, int startRowIndex, int lastRowIndex)
    {
        SqlConnection sqlCon;
        SqlDataAdapter sqlAdap;

        try
        {
            ds = new DataSet();
            sqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Main.ConnectionString"].ToString());
            sqlAdap = new SqlDataAdapter("Sp_Consumer_SelectBYConditionJOINOutboundExtractData", sqlCon);
            sqlAdap.SelectCommand.Parameters.Add(new SqlParameter("@EventId", rowConfig["EventId"].ToString()));
            sqlAdap.SelectCommand.Parameters.Add(new SqlParameter("@StartRowIndex", startRowIndex));
            sqlAdap.SelectCommand.Parameters.Add(new SqlParameter("@MaximumRowsIndex", lastRowIndex));
            sqlAdap.SelectCommand.Parameters.Add(new SqlParameter("@ExcludeBlockList", rowConfig["ExcludeBlockList"].ToString()));
            sqlAdap.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlAdap.Fill(ds);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void ReadConsumerDataJOINOutboundVIP(ref DataSet ds, DataRow rowConfig)
    {
        SqlConnection sqlCon;
        SqlDataAdapter sqlAdap;

        try
        {
            ds = new DataSet();
            sqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Main.ConnectionString"].ToString());
            sqlAdap = new SqlDataAdapter("Sp_Consumer_SelectBYConditionJOINOutboundExtractDataVIP", sqlCon);
            sqlAdap.SelectCommand.Parameters.Add(new SqlParameter("@EventId", rowConfig["EventId"].ToString()));
            sqlAdap.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlAdap.Fill(ds);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void ReadCountConsumerDataJOINOutbound(ref DataSet ds, DataRow rowConfig)
    {
        SqlConnection sqlCon;
        SqlDataAdapter sqlAdap;

        try
        {
            ds = new DataSet();
            sqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Main.ConnectionString"].ToString());
            sqlAdap = new SqlDataAdapter("Sp_Consumer_SelectCountBYConditionJOINOutboundExtractData", sqlCon);
            sqlAdap.SelectCommand.Parameters.Add(new SqlParameter("@EventId", rowConfig["EventId"].ToString()));
            sqlAdap.SelectCommand.Parameters.Add(new SqlParameter("@ExcludeBlockList", rowConfig["ExcludeBlockList"].ToString()));
            sqlAdap.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlAdap.Fill(ds);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void ReadOutboundConfigBYOutboundTypeID(ref DataSet ds, int eventId)
    {
        SqlConnection sqlCon;
        SqlDataAdapter sqlAdap;

        try
        {
            sqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Main.ConnectionString"].ToString());
            sqlAdap = new SqlDataAdapter("Sp_Consumer_SelectOutboundConfigBYOutboundTypeIdExtractData", sqlCon);
            sqlAdap.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlAdap.SelectCommand.Parameters.Add(new SqlParameter("@EventId", eventId));
            sqlAdap.Fill(ds);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public bool EventConsumerListForEvent(DataRow[] rowConfig)
    {
        SqlConnection con;
        SqlTransaction tran = null;
        SqlCommand cmdToExecute = new SqlCommand();
        System.Data.DataTable dtSmartouchEvent = new System.Data.DataTable();
        System.Data.DataRow[] drSmartouchEvent;
        DataSet ds;

        try
        {
            ds = new DataSet();
            con = new SqlConnection(ConfigurationSettings.AppSettings["Main.ConnectionString"].ToString());
            con.Open();

            cmdToExecute = new SqlCommand("Sp_Consumer_EventConsumerListForEventExtractData", con);
            cmdToExecute.CommandTimeout = 0;
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            cmdToExecute.Transaction = tran;

            foreach (DataRow item in rowConfig)
            {
                cmdToExecute.Parameters.Clear();
                cmdToExecute.Parameters.Add(new SqlParameter("@EventId", item["EventId"].ToString()));
                cmdToExecute.Parameters.Add(new SqlParameter("@DataGroup", item["DataGroup"].ToString()));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", 1));
                cmdToExecute.ExecuteNonQuery();

                ReadEventVisitCondition(ref dtSmartouchEvent, int.Parse(item["eventId"].ToString()));
                drSmartouchEvent = dtSmartouchEvent.Select(string.Format("eventId='{0}' AND datagroup='{1}'", item["EventId"].ToString(), item["DataGroup"].ToString()));
                ExtractData(drSmartouchEvent, int.Parse(item["EventId"].ToString()), con, tran);
            }
            return true;
        }
        catch (Exception ex)
        {
            tran.Rollback("ExtractData");
            throw new Exception(ex.Message);
        }
        finally
        {
            cmdToExecute.Connection.Close();
            cmdToExecute.Dispose();
        }
    }

    static void ReadEventVisitCondition(ref System.Data.DataTable dt, int eventID)
    {
        SqlConnection sqlCon;
        SqlDataAdapter sqlAdap;

        try
        {
            sqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Main.ConnectionString"].ToString());
            sqlAdap = new SqlDataAdapter("Sp_EventVisitCondition_SelectEventVisitConditionExtractData", sqlCon);
            sqlAdap.SelectCommand.Parameters.Add(new SqlParameter("@eventId", eventID));
            sqlAdap.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlAdap.Fill(dt);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public bool ExtractData(DataRow[] dr, int eventId, SqlConnection con, SqlTransaction tran)
    {
        System.Data.DataTable dtSmartouchEvent = new System.Data.DataTable();
        SqlCommand cmdToExecute = new SqlCommand();


        try
        {
            cmdToExecute = new SqlCommand("Sp_Consumer_EventExtractDataExtractData", con);
            cmdToExecute.CommandTimeout = 0;
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            cmdToExecute.Transaction = tran;

            if (dr.Length == 0)
            {
                cmdToExecute.Parameters.Clear();
                cmdToExecute.Parameters.Add(new SqlParameter("@Type", DBNull.Value));
                cmdToExecute.Parameters.Add(new SqlParameter("@EventId", eventId));
                cmdToExecute.Parameters.Add(new SqlParameter("@OutletID", DBNull.Value));
                cmdToExecute.Parameters.Add(new SqlParameter("@DateFrom", DBNull.Value));
                cmdToExecute.Parameters.Add(new SqlParameter("@DateTo", DBNull.Value));
                cmdToExecute.Parameters.Add(new SqlParameter("@LastActivityNotOver", DBNull.Value));
                cmdToExecute.Parameters.Add(new SqlParameter("@VisitFrequency", DBNull.Value));
                cmdToExecute.Parameters.Add(new SqlParameter("@UserID", "Diageo"));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", 1));
                cmdToExecute.ExecuteNonQuery();
            }
            else
            {
                foreach (DataRow item in dr)
                {
                    cmdToExecute.Parameters.Clear();
                    cmdToExecute.Parameters.Add(new SqlParameter("@Type", item["typeId"].ToString()));
                    cmdToExecute.Parameters.Add(new SqlParameter("@EventId", eventId));
                    cmdToExecute.Parameters.Add(new SqlParameter("@OutletID", item["outletId"].ToString()));
                    cmdToExecute.Parameters.Add(new SqlParameter("@DateFrom", item["dateFrom"].ToString()));
                    cmdToExecute.Parameters.Add(new SqlParameter("@DateTo", item["dateTo"].ToString()));
                    cmdToExecute.Parameters.Add(new SqlParameter("@LastActivityNotOver", item["lastActivityNotOver"].ToString()));
                    cmdToExecute.Parameters.Add(new SqlParameter("@VisitFrequency", item["visitFrequency"].ToString()));
                    cmdToExecute.Parameters.Add(new SqlParameter("@UserID", "Diageo"));
                    cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", 1));
                    cmdToExecute.ExecuteNonQuery();
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            cmdToExecute.Dispose();
        }
    }

    public void DeleteOutboundExtractData(int eventId)
    {
        System.Data.DataTable dtSmartouchEvent = new System.Data.DataTable();
        SqlCommand cmdToExecute = new SqlCommand();
        SqlConnection sqlCon;
        sqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Main.ConnectionString"].ToString());

        try
        {
            sqlCon.Open();
            cmdToExecute = new SqlCommand("Sp_Consumer_DeleteOutboundExtractData", sqlCon);
            cmdToExecute.CommandTimeout = 0;
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            cmdToExecute.Parameters.Add(new SqlParameter("@EventId", eventId));
            cmdToExecute.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            sqlCon.Close();
            cmdToExecute.Dispose();
        }
    }

    public void ReadConsumerDataGroup(ref DataSet ds, string eventId)
    {
        SqlConnection sqlCon;
        SqlDataAdapter sqlAdap;

        try
        {
            ds = new DataSet();
            sqlCon = new SqlConnection(ConfigurationSettings.AppSettings["Main.ConnectionString"].ToString());
            sqlAdap = new SqlDataAdapter("Sp_ConsumerDataGroupExtractData_SelectAll", sqlCon);
            sqlAdap.SelectCommand.Parameters.Add(new SqlParameter("@EventId", eventId));
            sqlAdap.SelectCommand.CommandType = CommandType.StoredProcedure;
            sqlAdap.Fill(ds);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void ExportExcel(DataTable dt, string filepath, string tablename)
    {
        string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filepath + ";Extended Properties=Excel 12.0 Xml;";
        try
        {
            using (OleDbConnection con = new OleDbConnection(connString))
            {
                con.Open();
                StringBuilder strSQL = new StringBuilder();
                strSQL.Append("CREATE TABLE ").Append("[" + tablename + "]");
                strSQL.Append("(");
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    strSQL.Append("[" + dt.Columns[i].ColumnName + "] text,");
                }
                strSQL = strSQL.Remove(strSQL.Length - 1, 1);
                strSQL.Append(")");

                OleDbCommand cmd = new OleDbCommand(strSQL.ToString(), con);
                cmd.CommandTimeout = 10000;
                cmd.ExecuteNonQuery();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    strSQL = new StringBuilder();
                    StringBuilder strfield = new StringBuilder();
                    StringBuilder strvalue = new StringBuilder();
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        strfield.Append("[" + dt.Columns[j].ColumnName + "]");
                        strvalue.Append("'" + dt.Rows[i][j].ToString().Replace("'", "") + "'");
                        if (j != dt.Columns.Count - 1)
                        {
                            strfield.Append(",");
                            strvalue.Append(",");
                        }
                        else
                        {
                        }
                    }
                    cmd.CommandText = strSQL.Append(" insert into [" + tablename + "]( ")
                        .Append(strfield.ToString())
                        .Append(") values (").Append(strvalue).Append(")").ToString();
                    cmd.ExecuteNonQuery();


                }
                con.Close();

            }
        }
        catch (Exception ex)
        {
            ErrorMessage.Text = ex.Message;
        }
    }

    protected void btnCount_Click(object sender, EventArgs e)
    {
        btnExtractData_Click(btnCount, null);
    }
}
