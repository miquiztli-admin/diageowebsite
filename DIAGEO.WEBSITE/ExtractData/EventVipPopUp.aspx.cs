﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Data;

public partial class ExtractData_EventVipPopUp : BasePage
{
    #region Property
    public string type
    {
        get { return (string)ViewState["type"]; }
        set { ViewState["type"] = value; }
    }
    public string DiageoVipPath
    {
        get { return (string)Session["DiageoVipPath"]; }
        set { Session["DiageoVipPath"] = value; }
    }
    public DataTable DiageoVIPExcel
    {
        get
        {
            if (Session["DiageoVIPExcel"] != null)
                return (DataTable)Session["DiageoVIPExcel"];
            else
                return new DataTable("DiageoVIPExcel");
        }
        set
        {
            if (value == null)
                Session.Remove("DiageoVIPExcel");
            else
                Session["DiageoVIPExcel"] = value;
        }
    }
    public string OutletVIPPath
    {
        get { return (string)Session["OutletVIPPath"]; }
        set { Session["OutletVIPPath"] = value; }
    }
    public DataTable OutletVIPExcel
    {
        get
        {
            if (Session["OutletVIPExcel"] != null)
                return (DataTable)Session["OutletVIPExcel"];
            else
                return new DataTable("OutletVIPExcel");
        }
        set
        {
            if (value == null)
                Session.Remove("OutletVIPExcel");
            else
                Session["OutletVIPExcel"] = value;
        }
    }
    #endregion
    private bool CheckFile(string FileName)
    {
        string FileType = "";
        try
        {
            FileType = FileName.Substring(FileName.IndexOf('.') + 1);
            if (FileType.ToLower().ToString().Equals("xls") || FileType.ToLower().ToString().Equals("xlsx"))
                return true;
            else
                return false;
        }
        catch (Exception ex)
        {
            throw new Exception("CheckFile :: " + ex.Message);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                type =Page.Request.QueryString["type"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Page_Load::" + ex.Message);
        }
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {
            string FullPath = Server.MapPath("~\\");
            string PathUpload = ConfigurationManager.AppSettings["PathFileUpload"].ToString();
            string FileName = Path.GetFileName(FileUpload.FileName);
            FullPath += PathUpload + FileName;
            DiageoVipPath = null;
            DiageoVIPExcel = null;
            OutletVIPExcel = null;
            OutletVIPPath = null;
            if (FileUpload.HasFile)
            {
                if (CheckFile(FileName))
                {
                    DataTable dtExcel = new DataTable();
                    FileUpload.SaveAs(FullPath);
                    dtExcel = ReadData(FullPath, "sheet1");
                    if (dtExcel.Rows.Count > 0)
                    {
                        if (type == "1")
                        {
                            DataTable dtDiageoVIP = new DataTable("DiageoVIP");

                            dtDiageoVIP.Columns.Add("First Name", typeof(string));
                            dtDiageoVIP.Columns.Add("Last Name", typeof(string));
                            dtDiageoVIP.Columns.Add("MOBILE NUMBER", typeof(string));
                            dtDiageoVIP.Columns.Add("EMAIL", typeof(string));
                            foreach (DataRow dr in dtExcel.Rows)
                            {
                                dtDiageoVIP.ImportRow(dr);
                            }
                            DiageoVIPExcel = dtDiageoVIP;
                            DiageoVipPath = FullPath;
                            JsClientAlert("DiageoVIPFileSuccess", "เพิ่มข้อมูลเรียบร้อย");
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePopup", "var btnDiageoVip = window.opener.document.getElementById('btnDiageoVip'); btnDiageoVip.click(); window.close(); ", true);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePopup", "window.close();", true);
                        }
                        else
                        {

                            DataTable dtOutletVip = new DataTable("OutletVIP");

                            dtOutletVip.Columns.Add("First Name", typeof(string));
                            dtOutletVip.Columns.Add("Last Name", typeof(string));
                            dtOutletVip.Columns.Add("MOBILE NUMBER", typeof(string));
                            dtOutletVip.Columns.Add("EMAIL", typeof(string));
                            foreach (DataRow dr in dtExcel.Rows)
                            {
                                dtOutletVip.ImportRow(dr);
                            }
                            OutletVIPExcel = dtOutletVip;
                            OutletVIPPath = FullPath;
                            JsClientAlert("DiageoVIPFileSuccess", "เพิ่มข้อมูลเรียบร้อย");
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePopup", "var btnOutletVip = window.opener.document.getElementById('btnOutletVip'); window.close();", true);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ClosePopup", "window.close();", true);
                        }
                    }
                    else
                    {
                        JsClientAlert("FileNoData", "ไม่มีข้อมูลอยู่ในfile");
                    }
                }
                else
                {
                    JsClientAlert("FileNotExcel", "ไฟล์ผิดชนิด ต้องอัพโหลดไฟล์ excel เท่านั้น");
                }
            }
            else{
                JsClientAlert("FileNotFound", "กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}
