﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExtractionConsumersData.aspx.cs"
    Inherits="ExtractData_ExtractionConsumersData" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="ITOS.Utility.CustomControl" Namespace="ITOS.Utility.CustomControl" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../Style/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    <base target="_self"/>
    
    <script type="text/javascript">
        function getdatecontrol() {
            $(function() {
                var dateBefore1 = null;
                $("#tabContainer_tabPanel1_tab1_dpEventDateFrom").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOn: 'button',
                    dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                    monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                    changeMonth: true,
                    changeYear: true,
                    beforeShow: function() {
                        if ($(this).val() != "") {
                            var arrayDate = $(this).val().split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);

                    },
                    onChangeMonthYear: function() {
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);
                    },
                    onClose: function() {
                        if ($(this).val() != "" && $(this).val() == dateBefore1) {
                            var arrayDate = dateBefore1.split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                    },
                    onSelect: function(dateText, inst) {
                        dateBefore1 = $(this).val();
                        var arrayDate = dateText.split("/");
                        arrayDate[2] = parseInt(arrayDate[2]);
                        $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                    }

                });

            });

            $(function() {
                var dateBefore2 = null;
                $("#tabContainer_tabPanel1_tab1_dpEventDateTo").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOn: 'button',
                    dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                    monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                    changeMonth: true,
                    changeYear: true,
                    beforeShow: function() {
                        if ($(this).val() != "") {
                            var arrayDate = $(this).val().split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);

                    },
                    onChangeMonthYear: function() {
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);
                    },
                    onClose: function() {
                        if ($(this).val() != "" && $(this).val() == dateBefore2) {
                            var arrayDate = dateBefore2.split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                    },
                    onSelect: function(dateText, inst) {
                        dateBefore2 = $(this).val();
                        var arrayDate = dateText.split("/");
                        arrayDate[2] = parseInt(arrayDate[2]);
                        $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                    }

                });

            });

            $(function() {
                var dateBefore5 = null;
                $("#tabContainer_tabPanel2_tab2_dpEventDateFrom").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOn: 'button',
                    dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                    monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                    changeMonth: true,
                    changeYear: true,
                    beforeShow: function() {
                        if ($(this).val() != "") {
                            var arrayDate = $(this).val().split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);

                    },
                    onChangeMonthYear: function() {
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);
                    },
                    onClose: function() {
                        if ($(this).val() != "" && $(this).val() == dateBefore5) {
                            var arrayDate = dateBefore5.split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                    },
                    onSelect: function(dateText, inst) {
                        dateBefore5 = $(this).val();
                        var arrayDate = dateText.split("/");
                        arrayDate[2] = parseInt(arrayDate[2]);
                        $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                    }

                });

            });

            $(function() {
                var dateBefore6 = null;
                $("#tabContainer_tabPanel2_tab2_dpEventDateTo").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOn: 'button',
                    dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                    monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                    changeMonth: true,
                    changeYear: true,
                    beforeShow: function() {
                        if ($(this).val() != "") {
                            var arrayDate = $(this).val().split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);

                    },
                    onChangeMonthYear: function() {
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);
                    },
                    onClose: function() {
                        if ($(this).val() != "" && $(this).val() == dateBefore6) {
                            var arrayDate = dateBefore6.split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                    },
                    onSelect: function(dateText, inst) {
                        dateBefore6 = $(this).val();
                        var arrayDate = dateText.split("/");
                        arrayDate[2] = parseInt(arrayDate[2]);
                        $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                    }

                });

            });

            $(function() {
                var dateBefore9 = null;
                $("#tabContainer_tabPanel3_tab3_dpEventDateFrom").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOn: 'button',
                    dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                    monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                    changeMonth: true,
                    changeYear: true,
                    beforeShow: function() {
                        if ($(this).val() != "") {
                            var arrayDate = $(this).val().split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);

                    },
                    onChangeMonthYear: function() {
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);
                    },
                    onClose: function() {
                        if ($(this).val() != "" && $(this).val() == dateBefore9) {
                            var arrayDate = dateBefore9.split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                    },
                    onSelect: function(dateText, inst) {
                        dateBefore9 = $(this).val();
                        var arrayDate = dateText.split("/");
                        arrayDate[2] = parseInt(arrayDate[2]);
                        $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                    }

                });

            });

            $(function() {
                var dateBefore10 = null;
                $("#tabContainer_tabPanel3_tab3_dpEventDateTo").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOn: 'button',
                    dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                    monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                    changeMonth: true,
                    changeYear: true,
                    beforeShow: function() {
                        if ($(this).val() != "") {
                            var arrayDate = $(this).val().split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);

                    },
                    onChangeMonthYear: function() {
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);
                    },
                    onClose: function() {
                        if ($(this).val() != "" && $(this).val() == dateBefore10) {
                            var arrayDate = dateBefore10.split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                    },
                    onSelect: function(dateText, inst) {
                        dateBefore10 = $(this).val();
                        var arrayDate = dateText.split("/");
                        arrayDate[2] = parseInt(arrayDate[2]);
                        $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                    }

                });

            });
        }
    </script>
    
    <script type="text/javascript">
        function getdatecontrolChil() {
            $(function() {
                var dateBefore3 = null;
                $("#tabContainer_tabPanel1_tab1_dpDateFrom").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOn: 'button',
                    dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                    monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                    changeMonth: true,
                    changeYear: true,
                    beforeShow: function() {
                        if ($(this).val() != "") {
                            var arrayDate = $(this).val().split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);

                    },
                    onChangeMonthYear: function() {
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);
                    },
                    onClose: function() {
                        if ($(this).val() != "" && $(this).val() == dateBefore3) {
                            var arrayDate = dateBefore3.split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                    },
                    onSelect: function(dateText, inst) {
                        dateBefore3 = $(this).val();
                        var arrayDate = dateText.split("/");
                        arrayDate[2] = parseInt(arrayDate[2]);
                        $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                    }

                });

            });

            $(function() {
                var dateBefore4 = null;
                $("#tabContainer_tabPanel1_tab1_dpDateTo").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOn: 'button',
                    dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                    monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                    changeMonth: true,
                    changeYear: true,
                    beforeShow: function() {
                        if ($(this).val() != "") {
                            var arrayDate = $(this).val().split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);

                    },
                    onChangeMonthYear: function() {
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);
                    },
                    onClose: function() {
                        if ($(this).val() != "" && $(this).val() == dateBefore4) {
                            var arrayDate = dateBefore4.split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                    },
                    onSelect: function(dateText, inst) {
                        dateBefore4 = $(this).val();
                        var arrayDate = dateText.split("/");
                        arrayDate[2] = parseInt(arrayDate[2]);
                        $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                    }

                });

            });

            $(function() {
                var dateBefore7 = null;
                $("#tabContainer_tabPanel2_tab2_dpDateFrom").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOn: 'button',
                    dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                    monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                    changeMonth: true,
                    changeYear: true,
                    beforeShow: function() {
                        if ($(this).val() != "") {
                            var arrayDate = $(this).val().split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);

                    },
                    onChangeMonthYear: function() {
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);
                    },
                    onClose: function() {
                        if ($(this).val() != "" && $(this).val() == dateBefore7) {
                            var arrayDate = dateBefore7.split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                    },
                    onSelect: function(dateText, inst) {
                        dateBefore7 = $(this).val();
                        var arrayDate = dateText.split("/");
                        arrayDate[2] = parseInt(arrayDate[2]);
                        $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                    }

                });

            });

            $(function() {
                var dateBefore8 = null;
                $("#tabContainer_tabPanel2_tab2_dpDateTo").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOn: 'button',
                    dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                    monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                    changeMonth: true,
                    changeYear: true,
                    beforeShow: function() {
                        if ($(this).val() != "") {
                            var arrayDate = $(this).val().split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);

                    },
                    onChangeMonthYear: function() {
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);
                    },
                    onClose: function() {
                        if ($(this).val() != "" && $(this).val() == dateBefore8) {
                            var arrayDate = dateBefore8.split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                    },
                    onSelect: function(dateText, inst) {
                        dateBefore8 = $(this).val();
                        var arrayDate = dateText.split("/");
                        arrayDate[2] = parseInt(arrayDate[2]);
                        $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                    }

                });

            });

            $(function() {
                var dateBefore11 = null;
                $("#tabContainer_tabPanel3_tab3_dpDateFrom").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOn: 'button',
                    dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                    monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                    changeMonth: true,
                    changeYear: true,
                    beforeShow: function() {
                        if ($(this).val() != "") {
                            var arrayDate = $(this).val().split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);

                    },
                    onChangeMonthYear: function() {
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);
                    },
                    onClose: function() {
                        if ($(this).val() != "" && $(this).val() == dateBefore11) {
                            var arrayDate = dateBefore11.split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                    },
                    onSelect: function(dateText, inst) {
                        dateBefore11 = $(this).val();
                        var arrayDate = dateText.split("/");
                        arrayDate[2] = parseInt(arrayDate[2]);
                        $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                    }

                });

            });

            $(function() {
                var dateBefore12 = null;
                $("#tabContainer_tabPanel3_tab3_dpDateTo").datepicker({
                    dateFormat: 'dd/mm/yy',
                    showOn: 'button',
                    dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                    monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                    changeMonth: true,
                    changeYear: true,
                    beforeShow: function() {
                        if ($(this).val() != "") {
                            var arrayDate = $(this).val().split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);

                    },
                    onChangeMonthYear: function() {
                        setTimeout(function() {
                            $.each($(".ui-datepicker-year option"), function(j, k) {
                                var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                                $(".ui-datepicker-year option").eq(j).text(textYear);
                            });
                        }, 50);
                    },
                    onClose: function() {
                        if ($(this).val() != "" && $(this).val() == dateBefore12) {
                            var arrayDate = dateBefore12.split("/");
                            arrayDate[2] = parseInt(arrayDate[2]);
                            $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                        }
                    },
                    onSelect: function(dateText, inst) {
                        dateBefore12 = $(this).val();
                        var arrayDate = dateText.split("/");
                        arrayDate[2] = parseInt(arrayDate[2]);
                        $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                    }

                });

            });
        }
    </script>
    
    <style type="text/css">
        .ntab
        {
        	border-color:Black;
            background-color:#2890C7;
            color:White;
            font:bold 13px Arial, Helvetica, sans-serif;
        }
        .ntextred
        {
            color:Red;
            font:bold 13px Arial, Helvetica, sans-serif;
        }
        .nbtn1
        {
        	border-color:Black;
            color:Green;
            font:bold 18px Arial, Helvetica, sans-serif;
        }
        .nbtn2
        {
        	border-color:Black;
            color:Red;
            font:bold 18px Arial, Helvetica, sans-serif;
        }
    </style>
    
    <script language="javascript" type="text/javascript">
    </script>

</head>
<body >
    <form id="frmExtractConsumersData" runat="server">
    <asp:ToolkitScriptManager ID="scriptManager" EnableScriptGlobalization="true" runat="server" AsyncPostBackTimeout="8000000">
    </asp:ToolkitScriptManager>
    <div>
        <fieldset>
            <table align="right">
                <tr>
                    <td>
                        <asp:ImageButton ID="ibtnRemoveTab" runat="server" Width="30px" Text="-" CssClass="nbtn2" 
                            ImageUrl="~/Image/onebit_32.png" onclick="ibtnRemoveTab_Click" />
                    </td>
                    <td></td>
                    <td>
                        <asp:ImageButton ID="ibtnAddTab" runat="server" Width="30px" Text="+" CssClass="nbtn1" 
                            ImageUrl="~/Image/onebit_31.png"  onclick="ibtnAddTab_Click" />
                    </td>
                </tr>
            </table>
        </fieldset>
        <fieldset>
            <asp:TabContainer ID="tabContainer" runat="server" AutoPostBack="true" 
                Height="900px" ScrollBars="Vertical" 
                onactivetabchanged="tabContainer_ActiveTabChanged" ActiveTabIndex="0">
                <%-- Tab 1 --%>
                <asp:TabPanel ID="tabPanel1" runat="server" CssClass="ntextblack">
                    <HeaderTemplate>
                        <span id="tabName1" class="ntab">&nbsp;&nbsp;&nbsp;&nbsp;DataGroup 1&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <fieldset>
                            <legend class="ntextblack">Modify Data Group Detail</legend>
                            <asp:UpdatePanel ID="updatepanel1" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="tab1_Panel" runat="server" Width="1000px">
                                    <table>
                                        <tr id="trEventNameTab1" name="trEventName" runat="server">
                                            <td align="right" class="ntextblack" style="width: 250px">
                                                Event Name<span class="ntextred">*</span>
                                            </td>
                                            <td style="width: 20px">
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="tab1_txtEventName" runat="server" CssClass="ntextblackdata" Width="350px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trSenderNameTab1" name="trSenderName" runat="server">
                                            <td align="right" class="ntextblack">
                                                Sender Name<span class="ntextred">*</span>
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="tab1_txtSenderName" runat="server" Width="500px" CssClass="ntextblackdata"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trBrandNameTab1" name="trBrandName"  runat="server">
                                            <td align="right" class="ntextblack">
                                                Brand
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="tab1_ddlBrand" runat="server" Width="200px" CssClass="ntextblackdata"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="trEventDateFromTab1" name="trEventDateFrom" runat="server">
                                            <td align="right" class="ntextblack">
                                                Event Date From<span class="ntextred">*</span>
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <%--<cc1:DatePicker ID="tab1_dpEventDateFrom" runat="server" 
                                                    imgDirectory="../Image/datepicker/" />--%>
                                                <asp:TextBox ID="tab1_dpEventDateFrom" runat="server" Width="100px" 
                                                    CssClass="ntextblackdata" MaxLength="10"></asp:TextBox>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span class="ntextblack" style="vertical-align:middle">Event Date To<span class="ntextred">*</span></span>
                                                &nbsp;&nbsp;
                                                <asp:TextBox ID="tab1_dpEventDateTo" runat="server" Width="100px" 
                                                    CssClass="ntextblackdata" MaxLength="10"></asp:TextBox>
                                                <%--<cc1:DatePicker ID="tab1_dpEventDateTo" runat="server" 
                                                    imgDirectory="../Image/datepicker/" />--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Platform
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <table>
                                                    <tr>
                                                        <td style="width:160px">
                                                            <asp:Button ID="tab1_btnPlatform" runat="server" CssClass="ntextblack" 
                                                                onclick="btnPlatform_Click" Text="Choose Platform" Width="150px" 
                                                                style="vertical-align:middle" />
                                                        </td>
                                                        <td style="width:400px">
                                                            <asp:Label ID="tab1_lblPlatform" runat="server" CssClass="ntextblack" 
                                                                style="vertical-align:middle"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Outlet
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <table>
                                                    <tr>
                                                        <td style="width:160px">
                                                            <asp:Button ID="tab1_btnOutlet" runat="server" Text="Choose Outlet" CssClass="ntextblack"
                                                                Width="150px" style="vertical-align:middle" onclick="btnOutlet_Click" />
                                                        </td>
                                                        <td style="width:400px">
                                                            <asp:Label ID="tab1_lblOutlet" runat="server" CssClass="ntextblack" 
                                                                style="vertical-align:middle"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack" valign="top">
                                                Brand Preference
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:CheckBoxList ID="tab1_chkListBrand" runat="server" CssClass="ntextblack" 
                                                    RepeatColumns="3" RepeatDirection="Horizontal">
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Gender
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:RadioButtonList ID="tab1_rbtnListGender" runat="server" 
                                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Valid Opt-in
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:CheckBoxList ID="tab1_chkListValid" runat="server" CssClass="ntextblack"
                                                    RepeatDirection="Horizontal" >
                                                    <asp:ListItem Text="Diageo Opt-in" Value="D"></asp:ListItem>
                                                    <asp:ListItem Text="Outlet Opt-in" Value="O"></asp:ListItem>
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Age
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <span class="ntextblack" style="vertical-align: middle">From</span>
                                                <asp:TextBox ID="tab1_txtAgeFrom" runat="server" CssClass="ntextblackdata" MaxLength="3" 
                                                    OnKeyPress="validateNumKey()" Style="vertical-align: middle" Width="80px"></asp:TextBox>
                                                <span class="ntextblack" style="vertical-align: middle">To</span>
                                                <asp:TextBox ID="tab1_txtAgeTo" runat="server" CssClass="ntextblackdata" MaxLength="3" 
                                                    OnKeyPress="validateNumKey()" Style="vertical-align: middle" Width="80px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Month of Birthday
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:CheckBoxList ID="tab1_chkListMonth" runat="server" CssClass="ntextblack" 
                                                    RepeatColumns="3" RepeatDirection="Horizontal">
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Province
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <table>
                                                    <tr>
                                                        <td style="width:160px">
                                                            <asp:Button ID="tab1_btnProvince" runat="server" CssClass="ntextblack" 
                                                                onclick="btnProvince_Click" Text="Choose Province" Width="150px" 
                                                                style="vertical-align:middle" />
                                                        </td>
                                                        <td style="width:400px">
                                                            <asp:Label ID="tab1_lblProvince" runat="server" CssClass="ntextblack" 
                                                                style="vertical-align:middle"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"">
                                                <table style="width:100%">
                                                    <tr>
                                                        <td style="width:100px"></td>
                                                        <td>
                                                            <fieldset>
                                                                <legend class="ntextblack">Register and Visit Condition</legend>
                                                                <table align="center">
                                                                    <tr>
                                                                        <td align="right" class="ntextblack" style="width: 50px">
                                                                            Type
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:DropDownList ID="tab1_ddlType" runat="server" AutoPostBack="True" 
                                                                                CssClass="ntextblackdata" onselectedindexchanged="ddlType_SelectedIndexChanged" 
                                                                                Width="120px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align="right" class="ntextblack" style="width: 70px">
                                                                            Outlet
                                                                        </td>
                                                                        <td style="width: 10px">
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:DropDownList ID="tab1_ddlOutlet" runat="server" CssClass="ntextblackdata" 
                                                                                Width="200px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="right" class="ntextblack">
                                                                            <span class="ntextblack" style="vertical-align: middle">Date From</span>
                                                                            <asp:TextBox ID="tab1_dpDateFrom" runat="server" Width="100px" MaxLength="10" CssClass="ntextblackdata"></asp:TextBox>
                                                                            <%--<cc1:DatePicker ID="tab1_dpDateFrom" runat="server" imgDirectory="../Image/datepicker/" />--%>
                                                                        </td>
                                                                        <td align="right" class="ntextblack">
                                                                            Date To
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="tab1_dpDateTo" runat="server" Width="100px" MaxLength="10" CssClass="ntextblackdata"></asp:TextBox>
                                                                            <%--<cc1:DatePicker ID="tab1_dpDateTo" runat="server" imgDirectory="../Image/datepicker/" />--%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="right" class="ntextblack" colspan="2">
                                                                            Last Activity not over (days)
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="tab1_txtLastActivity" runat="server" MaxLength="3" CssClass="ntextblackdata" 
                                                                                Width="120px" OnKeyPress="validateNumKey()"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="right" class="ntextblack" colspan="2">
                                                                            Visit Frequency/Month (mores)
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="tab1_txtVisit" runat="server" CssClass="ntextblackdata" 
                                                                                Width="120px" OnKeyPress="validateNumKey()" MaxLength="2"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="2">
                                                                            <asp:Button ID="tab1_btnAddCondition" runat="server" CssClass="ntextblack" Text="Add" 
                                                                                Width="80px" onclick="btnAddCondition_Click" />
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" colspan="2">
                                                                            <asp:Button ID="tab1_btnRemoveCondition" runat="server" CssClass="ntextblack" 
                                                                                Text="Remove" Width="80px" onclick="btnRemoveCondition_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table align="center" style="width:800px">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DataGrid ID="tab1_dgCondition" runat="server" AutoGenerateColumns="False" >
                                                                                <AlternatingItemStyle CssClass="gridalternativerowstyle" 
                                                                                    HorizontalAlign="Center" />
                                                                                <Columns>
                                                                                    <asp:TemplateColumn>
                                                                                        <HeaderTemplate>
                                                                                            <asp:CheckBox ID="tab1_chkDeleteAll" OnCheckedChanged="ClearCheck" AutoPostBack="true" runat="server" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="tab1_chkDelete" runat="server" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="Type" HeaderText="Type">
                                                                                        <HeaderStyle Width="120px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Outlet" HeaderText="Outlet">
                                                                                        <HeaderStyle Width="200px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="DateFrom" HeaderText="Date From">
                                                                                        <HeaderStyle Width="100px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="DateTo" HeaderText="Date To">
                                                                                        <HeaderStyle Width="100px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="LastActivity" 
                                                                                        HeaderText="Last Activity">
                                                                                        <HeaderStyle Width="100px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Visit" 
                                                                                        HeaderText="Visit Frequency/Month">
                                                                                        <HeaderStyle Width="200px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="TypeID" Visible="false"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="OutletID" Visible="false"></asp:BoundColumn>
                                                                                </Columns>
                                                                                <HeaderStyle BackColor="Gray" CssClass="gridheaderrowstyle" 
                                                                                    HorizontalAlign="Center" />
                                                                                <ItemStyle CssClass="gridrowstyle" HorizontalAlign="Center" />
                                                                            </asp:DataGrid>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Open 1 Email in last ... times
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="tab1_txtOpenEmail" runat="server" MaxLength="3" CssClass="ntextblackdata" 
                                                    Style="vertical-align: middle" Width="120px" OnKeyPress="validateNumKey()"></asp:TextBox>
                                                <span class="ntextblack" style="vertical-align: middle">Or Receive Email Less 
                                                ... times</span>
                                                <asp:TextBox ID="tab1_txtReceiveEmail" runat="server" MaxLength="3" CssClass="ntextblackdata" 
                                                    Style="vertical-align: middle" Width="120px" OnKeyPress="validateNumKey()"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Email Active
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:RadioButtonList ID="tab1_rbtnEmailActive" runat="server" 
                                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Valid" Value="T"></asp:ListItem>
                                                    <asp:ListItem Text="Invalid" Value="F"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Mobile No Active
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:RadioButtonList ID="tab1_rbtnMobileActive" runat="server" CssClass="ntextblack"
                                                    RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Valid" Value="T"></asp:ListItem>
                                                    <asp:ListItem Text="Invalid" Value="F"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Open Email Rate(Over %)
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="tab1_txtEmailRate" runat="server" MaxLength="3" CssClass="ntextblackdata" 
                                                    Width="120px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Include Bounce
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:CheckBox ID="tab1_chkIncludeCallCenter" runat="server" CssClass="ntextblack" Text="Hardbounce Call Center" />&nbsp;&nbsp;
                                                <asp:CheckBox ID="tab1_chkIncludeEDM" runat="server" CssClass="ntextblack" Text="Hardbounce EDM" />&nbsp;&nbsp;
                                                <asp:CheckBox ID="tab1_chkIncludeSMS" runat="server" CssClass="ntextblack" Text="Hardbounce SMS" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:Button ID="tab1_btnRefresh" runat="server" style="display:none;" onclick="btnRefresh_Click" />
                        </fieldset>
                    </ContentTemplate>
                </asp:TabPanel>
                <%-- Tab 2 --%>
                <asp:TabPanel ID="tabPanel2" runat="server" CssClass="ntextblack">
                    <HeaderTemplate>
                        <span id="tabName2" class="ntab">&nbsp;&nbsp;&nbsp;&nbsp;DataGroup 2&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <fieldset>
                            <legend class="ntextblack">Modify Data Group Detail</legend>
                            <asp:UpdatePanel ID="updatepanel2" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="tab2_Panel" runat="server" Width="1000px">
                                    <table>
                                        <tr id="trEventNameTab2" name="trEventNameTab2" runat="server">
                                            <td align="right" class="ntextblack" style="width: 250px">
                                                Event Name<span class="ntextred">*</span>
                                            </td>
                                            <td style="width: 20px">
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="tab2_txtEventName" runat="server" MaxLength="50" CssClass="ntextblackdata" Width="350px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trSenderNameTab2" name="trSenderNameTab2" runat="server">
                                            <td align="right" class="ntextblack">
                                                Sender Name<span class="ntextred">*</span>
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="tab2_txtSenderName" runat="server" Width="500px" MaxLength="50" CssClass="ntextblackdata"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trBrandNameTab2" name="trBrandNameTab2" runat="server">
                                            <td align="right" class="ntextblack">
                                                Brand
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="tab2_ddlBrand" runat="server" Width="200px" CssClass="ntextblackdata"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="trEventDateFromTab2" name="trEventDateFromTab2" runat="server">
                                            <td align="right" class="ntextblack">
                                                Event Date From<span class="ntextred">*</span>
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <%--<cc1:DatePicker ID="tab2_dpEventDateFrom" runat="server" imgDirectory="../Image/datepicker/" />--%>
                                                <asp:TextBox ID="tab2_dpEventDateFrom" runat="server" Width="100px" MaxLength="10" CssClass="ntextblackdata"></asp:TextBox>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span class="ntextblack" style="vertical-align:middle">Event Date To<span class="ntextred">*</span></span>
                                                &nbsp;&nbsp;
                                                <asp:TextBox ID="tab2_dpEventDateTo" runat="server" Width="100px" MaxLength="10" CssClass="ntextblackdata"></asp:TextBox>
                                                <%--<cc1:DatePicker ID="tab2_dpEventDateTo" runat="server" imgDirectory="../Image/datepicker/" />--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Platform
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <table>
                                                    <tr>
                                                        <td style="width:160px">
                                                            <asp:Button ID="tab2_btnPlatform" runat="server" CssClass="ntextblack" 
                                                                onclick="btnPlatform_Click" Text="Choose Platform" Width="150" style="vertical-align:middle" />
                                                        </td>
                                                        <td style="width:400px">
                                                            <asp:Label ID="tab2_lblPlatform" runat="server" CssClass="ntextblack" Text="" style="vertical-align:middle"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Outlet
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <table>
                                                    <tr>
                                                        <td style="width:160px">
                                                            <asp:Button ID="tab2_btnOutlet" runat="server" Text="Choose Outlet" CssClass="ntextblack"
                                                                Width="150" style="vertical-align:middle" onclick="btnOutlet_Click" />
                                                        </td>
                                                        <td style="width:400px">
                                                            <asp:Label ID="tab2_lblOutlet" runat="server" CssClass="ntextblack" Text="" style="vertical-align:middle"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack" valign="top">
                                                Brand Preference
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:CheckBoxList ID="tab2_chkListBrand" runat="server" CssClass="ntextblack" 
                                                    RepeatColumns="3" RepeatDirection="Horizontal">
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Gender
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:RadioButtonList ID="tab2_rbtnListGender" runat="server" 
                                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Valid Opt-in
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:CheckBoxList ID="tab2_chkListValid" runat="server" CssClass="ntextblack"
                                                    RepeatDirection="Horizontal" >
                                                    <asp:ListItem Text="Diageo Opt-in" Value="D"></asp:ListItem>
                                                    <asp:ListItem Text="Outlet Opt-in" Value="O"></asp:ListItem>
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Age
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <span class="ntextblack" style="vertical-align: middle">From</span>
                                                <asp:TextBox ID="tab2_txtAgeFrom" runat="server" CssClass="ntextblackdata" MaxLength="2" 
                                                    OnKeyPress="validateNumKey()" Style="vertical-align: middle" Width="80px"></asp:TextBox>
                                                <span class="ntextblack" style="vertical-align: middle">To</span>
                                                <asp:TextBox ID="tab2_txtAgeTo" runat="server" CssClass="ntextblackdata" MaxLength="2" 
                                                    OnKeyPress="validateNumKey()" Style="vertical-align: middle" Width="80px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Month of Birthdate
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:CheckBoxList ID="tab2_chkListMonth" runat="server" CssClass="ntextblack" 
                                                    RepeatColumns="3" RepeatDirection="Horizontal">
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Province
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <table>
                                                    <tr>
                                                        <td style="width:160px">
                                                            <asp:Button ID="tab2_btnProvince" runat="server" CssClass="ntextblack" 
                                                                onclick="btnProvince_Click" Text="Choose Province" Width="150" style="vertical-align:middle" />
                                                        </td>
                                                        <td style="width:400px">
                                                            <asp:Label ID="tab2_lblProvince" runat="server" CssClass="ntextblack" Text="" style="vertical-align:middle"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <table style="width:100%">
                                                    <tr>
                                                        <td style="width:100px"></td>
                                                        <td>
                                                            <fieldset>
                                                                <legend class="ntextblack">Register and Visit Condition</legend>
                                                                <table align="center">
                                                                    <tr>
                                                                        <td align="right" class="ntextblack" style="width: 50px">
                                                                            Type
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:DropDownList ID="tab2_ddlType" runat="server" AutoPostBack="true" 
                                                                                CssClass="ntextblackdata" onselectedindexchanged="ddlType_SelectedIndexChanged" 
                                                                                Width="120px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align="right" class="ntextblack" style="width: 70px">
                                                                            Outlet
                                                                        </td>
                                                                        <td style="width: 10px">
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:DropDownList ID="tab2_ddlOutlet" runat="server" CssClass="ntextblackdata" 
                                                                                Width="200px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="right" class="ntextblack">
                                                                            <span class="ntextblack" style="vertical-align: middle">Date From</span>
                                                                            <asp:TextBox ID="tab2_dpDateFrom" runat="server" Width="100px" MaxLength="3" CssClass="ntextblackdata"></asp:TextBox>
                                                                            <%--<cc1:DatePicker ID="tab2_dpDateFrom" runat="server" 
                                                                                imgDirectory="../Image/datepicker/" />--%>
                                                                        </td>
                                                                        <td align="right" class="ntextblack">
                                                                            Date To
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="tab2_dpDateTo" runat="server" Width="100px" MaxLength="3" CssClass="ntextblackdata"></asp:TextBox>
                                                                            <%--<cc1:DatePicker ID="tab2_dpDateTo" runat="server" 
                                                                                imgDirectory="../Image/datepicker/" />--%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="right" class="ntextblack" colspan="2">
                                                                            Last Activity not over (days)
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="tab2_txtLastActivity" runat="server" MaxLength="3" CssClass="ntextblackdata" 
                                                                                Width="120px" OnKeyPress="validateNumKey()"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="right" class="ntextblack" colspan="2">
                                                                            Visit Frequency/Month (mores)
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="tab2_txtVisit" runat="server" CssClass="ntextblackdata" 
                                                                                Width="120px" OnKeyPress="validateNumKey()" MaxLength="2"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="2">
                                                                            <asp:Button ID="tab2_btnAddCondition" runat="server" CssClass="ntextblack" Text="Add" 
                                                                                Width="80px" onclick="btnAddCondition_Click" />
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" colspan="2">
                                                                            <asp:Button ID="tab2_btnRemoveCondition" runat="server" CssClass="ntextblack" 
                                                                                Text="Remove" Width="80px" onclick="btnRemoveCondition_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table align="center" style="width:800px">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DataGrid ID="tab2_dgCondition" runat="server" AutoGenerateColumns="False" >
                                                                                <AlternatingItemStyle CssClass="gridalternativerowstyle" 
                                                                                    HorizontalAlign="Center" />
                                                                                <Columns>
                                                                                    <asp:TemplateColumn>
                                                                                        <HeaderTemplate>
                                                                                            <asp:CheckBox ID="tab2_chkDeleteAll" runat="server" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="tab2_chkDelete" runat="server" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="Type" HeaderText="Type">
                                                                                        <HeaderStyle Width="120px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Outlet" HeaderText="Outlet">
                                                                                        <HeaderStyle Width="200px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="DateFrom" HeaderText="Date From">
                                                                                        <HeaderStyle Width="100px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="DateTo" HeaderText="Date To">
                                                                                        <HeaderStyle Width="100px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="LastActivity" 
                                                                                        HeaderText="Last Activity">
                                                                                        <HeaderStyle Width="100px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Visit" 
                                                                                        HeaderText="Visit Frequency/Month">
                                                                                        <HeaderStyle Width="200px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="TypeID" Visible="false"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="OutletID" Visible="false"></asp:BoundColumn>
                                                                                </Columns>
                                                                                <HeaderStyle BackColor="Gray" CssClass="gridheaderrowstyle" 
                                                                                    HorizontalAlign="Center" />
                                                                                <ItemStyle CssClass="gridrowstyle" HorizontalAlign="Center" />
                                                                            </asp:DataGrid>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Open 1 Email in last ... times
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="tab2_txtOpenEmail" runat="server" MaxLength="3" CssClass="ntextblackdata" 
                                                    Style="vertical-align: middle" Width="120px" OnKeyPress="validateNumKey()"></asp:TextBox>
                                                <span class="ntextblack" style="vertical-align: middle">Or Receive Email Less 
                                                ... times</span>
                                                <asp:TextBox ID="tab2_txtReceiveEmail" runat="server" MaxLength="3" CssClass="ntextblackdata" 
                                                    Style="vertical-align: middle" Width="120px" OnKeyPress="validateNumKey()"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Email Active
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:RadioButtonList ID="tab2_rbtnEmailActive" runat="server" 
                                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Valid" Value="T"></asp:ListItem>
                                                    <asp:ListItem Text="InValid" Value="F"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Mobile No Active
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:RadioButtonList ID="tab2_rbtnMobileActive" runat="server" CssClass="ntextblack"
                                                    RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Valid" Value="T"></asp:ListItem>
                                                    <asp:ListItem Text="InValid" Value="F"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Open Email Rate(Over %)
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="tab2_txtEmailRate" runat="server" MaxLength="3" CssClass="ntextblackdata" 
                                                    Width="120px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Include Bounce
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:CheckBox ID="tab2_chkIncludeCallCenter" runat="server" CssClass="ntextblack" Text="Hardbounce Call Center" />&nbsp;&nbsp;
                                                <asp:CheckBox ID="tab2_chkIncludeEDM" runat="server" CssClass="ntextblack" Text="Hardbounce EDM" />&nbsp;&nbsp;
                                                <asp:CheckBox ID="tab2_chkIncludeSMS" runat="server" CssClass="ntextblack" Text="Hardbounce SMS" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:Button ID="tab2_btnRefresh" runat="server" style="display:none;" onclick="btnRefresh_Click" />
                        </fieldset>
                    </ContentTemplate>
                </asp:TabPanel>
                <%-- Tab 3 --%>
                <asp:TabPanel ID="tabPanel3" runat="server" CssClass="ntextblack">
                    <HeaderTemplate>
                        <span id="tabName3" class="ntab">&nbsp;&nbsp;&nbsp;&nbsp;DataGroup 3&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <fieldset>
                            <legend class="ntextblack">Modify Data Group Detail</legend>
                            <asp:UpdatePanel ID="updatepanel3" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="tab3_Panel" runat="server" Width="1000px">
                                    <table>
                                        <tr id="trEventNameTab3" name="trEventName" runat="server">
                                            <td align="right" class="ntextblack" style="width: 250px">
                                                Event Name<span class="ntextred">*</span>
                                            </td>
                                            <td style="width: 20px">
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="tab3_txtEventName" runat="server" MaxLength="50" CssClass="ntextblackdata" Width="350px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trSenderNameTab3" name="trSenderName" runat="server">
                                            <td align="right" class="ntextblack">
                                                Sender Name<span class="ntextred">*</span>
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="tab3_txtSenderName" runat="server" Width="500px" MaxLength="50" CssClass="ntextblackdata"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trBrandNameTab3" name="trBrandName" runat="server">
                                            <td align="right" class="ntextblack">
                                                Brand
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="tab3_ddlBrand" runat="server" Width="200px" CssClass="ntextblackdata"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="trEventDateFromTab3" name="trEventDateFrom" runat="server">
                                            <td align="right" class="ntextblack">
                                                Event Date From<span class="ntextred">*</span>
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <%--<cc1:DatePicker ID="tab3_dpEventDateFrom" runat="server" imgDirectory="../Image/datepicker/" />--%>
                                                <asp:TextBox ID="tab3_dpEventDateFrom" runat="server" Width="100px" MaxLength="10" CssClass="ntextblackdata"></asp:TextBox>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span class="ntextblack" style="vertical-align:middle">Event Date To<span class="ntextred">*</span></span>
                                                &nbsp;&nbsp;
                                                <asp:TextBox ID="tab3_dpEventDateTo" runat="server" Width="100px" MaxLength="10" CssClass="ntextblackdata"></asp:TextBox>
                                                <%--<cc1:DatePicker ID="tab3_dpEventDateTo" runat="server" imgDirectory="../Image/datepicker/" />--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Platform
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <table>
                                                    <tr>
                                                        <td style="width:160px">
                                                            <asp:Button ID="tab3_btnPlatform" runat="server" CssClass="ntextblack" 
                                                                onclick="btnPlatform_Click" Text="Choose Platform" Width="150" style="vertical-align:middle" />
                                                        </td>
                                                        <td style="width:400px">
                                                            <asp:Label ID="tab3_lblPlatform" runat="server" CssClass="ntextblack" Text="" style="vertical-align:middle"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Outlet
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <table>
                                                    <tr>
                                                        <td style="width:160px">
                                                            <asp:Button ID="tab3_btnOutlet" runat="server" Text="Choose Outlet" CssClass="ntextblack"
                                                                Width="150" style="vertical-align:middle" onclick="btnOutlet_Click" />
                                                        </td>
                                                        <td style="width:400px">
                                                            <asp:Label ID="tab3_lblOutlet" runat="server" CssClass="ntextblack" Text="" style="vertical-align:middle"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack" valign="top">
                                                Brand Preference
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:CheckBoxList ID="tab3_chkListBrand" runat="server" CssClass="ntextblack" 
                                                    RepeatColumns="3" RepeatDirection="Horizontal">
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Gender
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:RadioButtonList ID="tab3_rbtnListGender" runat="server" 
                                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Valid Opt-in
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:CheckBoxList ID="tab3_chkListValid" runat="server" CssClass="ntextblack"
                                                    RepeatDirection="Horizontal" >
                                                    <asp:ListItem Text="Diageo Opt-in" Value="D"></asp:ListItem>
                                                    <asp:ListItem Text="Outlet Opt-in" Value="O"></asp:ListItem>
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Age
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <span class="ntextblack" style="vertical-align: middle">From</span>
                                                <asp:TextBox ID="tab3_txtAgeFrom" runat="server" CssClass="ntextblackdata" MaxLength="2" 
                                                    OnKeyPress="validateNumKey()" Style="vertical-align: middle" Width="80px"></asp:TextBox>
                                                <span class="ntextblack" style="vertical-align: middle">To</span>
                                                <asp:TextBox ID="tab3_txtAgeTo" runat="server" CssClass="ntextblackdata" MaxLength="2" 
                                                    OnKeyPress="validateNumKey()" Style="vertical-align: middle" Width="80px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Month of Birthdate
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:CheckBoxList ID="tab3_chkListMonth" runat="server" CssClass="ntextblack" 
                                                    RepeatColumns="3" RepeatDirection="Horizontal">
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Province
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <table>
                                                    <tr>
                                                        <td style="width:160px">
                                                            <asp:Button ID="tab3_btnProvince" runat="server" CssClass="ntextblack" 
                                                                onclick="btnProvince_Click" Text="Choose Province" Width="150" style="vertical-align:middle" />
                                                        </td>
                                                        <td style="width:400px">
                                                            <asp:Label ID="tab3_lblProvince" runat="server" CssClass="ntextblack" Text="" style="vertical-align:middle"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <table style="width:100%">
                                                    <tr>
                                                        <td style="width:100px"></td>
                                                        <td>
                                                            <fieldset>
                                                                <legend class="ntextblack">Register and Visit Condition</legend>
                                                                <table align="center">
                                                                    <tr>
                                                                        <td align="right" class="ntextblack" style="width: 50px">
                                                                            Type
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:DropDownList ID="tab3_ddlType" runat="server" AutoPostBack="true" 
                                                                                CssClass="ntextblackdata" onselectedindexchanged="ddlType_SelectedIndexChanged" 
                                                                                Width="120px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align="right" class="ntextblack" style="width: 70px">
                                                                            Outlet
                                                                        </td>
                                                                        <td style="width: 10px">
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:DropDownList ID="tab3_ddlOutlet" runat="server" CssClass="ntextblackdata" 
                                                                                Width="200px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="right" class="ntextblack">
                                                                            <span class="ntextblack" style="vertical-align: middle">Date From</span>
                                                                            <asp:TextBox ID="tab3_dpDateFrom" runat="server" Width="100px" MaxLength="10" CssClass="ntextblackdata"></asp:TextBox>
                                                                            <%--<cc1:DatePicker ID="tab3_dpDateFrom" runat="server" 
                                                                                imgDirectory="../Image/datepicker/" />--%>
                                                                        </td>
                                                                        <td align="right" class="ntextblack">
                                                                            Date To
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="tab3_dpDateTo" runat="server" Width="100px" MaxLength="10" CssClass="ntextblackdata"></asp:TextBox>
                                                                            <%--<cc1:DatePicker ID="tab3_dpDateTo" runat="server" 
                                                                                imgDirectory="../Image/datepicker/" />--%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="right" class="ntextblack" colspan="2">
                                                                            Last Activity not over (days)
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="tab3_txtLastActivity" runat="server" MaxLength="3" CssClass="ntextblackdata" 
                                                                                Width="120px" OnKeyPress="validateNumKey()"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                        </td>
                                                                        <td align="right" class="ntextblack" colspan="2">
                                                                            Visit Frequency/Month (mores)
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left">
                                                                            <asp:TextBox ID="tab3_txtVisit" runat="server" CssClass="ntextblackdata" 
                                                                                Width="120px" OnKeyPress="validateNumKey()" MaxLength="2"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="2">
                                                                            <asp:Button ID="tab3_btnAddCondition" runat="server" CssClass="ntextblack" Text="Add" 
                                                                                Width="80px" onclick="btnAddCondition_Click" />
                                                                        </td>
                                                                        <td>
                                                                        </td>
                                                                        <td align="left" colspan="2">
                                                                            <asp:Button ID="tab3_btnRemoveCondition" runat="server" CssClass="ntextblack" 
                                                                                Text="Remove" Width="80px" onclick="btnRemoveCondition_Click" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table align="center" style="width:800px">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DataGrid ID="tab3_dgCondition" runat="server" AutoGenerateColumns="False" >
                                                                                <AlternatingItemStyle CssClass="gridalternativerowstyle" 
                                                                                    HorizontalAlign="Center" />
                                                                                <Columns>
                                                                                    <asp:TemplateColumn>
                                                                                        <HeaderTemplate>
                                                                                            <asp:CheckBox ID="tab3_chkDeleteAll" runat="server" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="tab3_chkDelete" runat="server" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="Type" HeaderText="Type">
                                                                                        <HeaderStyle Width="120px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Outlet" HeaderText="Outlet">
                                                                                        <HeaderStyle Width="200px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="DateFrom" HeaderText="Date From">
                                                                                        <HeaderStyle Width="100px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="DateTo" HeaderText="Date To">
                                                                                        <HeaderStyle Width="100px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="LastActivity" 
                                                                                        HeaderText="Last Activity">
                                                                                        <HeaderStyle Width="100px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="Visit" 
                                                                                        HeaderText="Visit Frequency/Month">
                                                                                        <HeaderStyle Width="200px" />
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="TypeID" Visible="false"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="OutletID" Visible="false"></asp:BoundColumn>
                                                                                </Columns>
                                                                                <HeaderStyle BackColor="Gray" CssClass="gridheaderrowstyle" 
                                                                                    HorizontalAlign="Center" />
                                                                                <ItemStyle CssClass="gridrowstyle" HorizontalAlign="Center" />
                                                                            </asp:DataGrid>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </fieldset>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Open 1 Email in last ... times
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="tab3_txtOpenEmail" runat="server" MaxLength="3" CssClass="ntextblackdata" 
                                                    Style="vertical-align: middle" Width="120px" OnKeyPress="validateNumKey()"></asp:TextBox>
                                                <span class="ntextblack" style="vertical-align: middle">Or Receive Email Less 
                                                ... times</span>
                                                <asp:TextBox ID="tab3_txtReceiveEmail" runat="server" MaxLength="3" CssClass="ntextblackdata" 
                                                    Style="vertical-align: middle" Width="120px" OnKeyPress="validateNumKey()"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Email Active
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:RadioButtonList ID="tab3_rbtnEmailActive" runat="server" 
                                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Valid" Value="T"></asp:ListItem>
                                                    <asp:ListItem Text="InValid" Value="F"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Mobile No Active
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:RadioButtonList ID="tab3_rbtnMobileActive" runat="server" CssClass="ntextblack"
                                                    RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Valid" Value="T"></asp:ListItem>
                                                    <asp:ListItem Text="InValid" Value="F"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Open Email Rate(Over %)
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="tab3_txtEmailRate" runat="server" MaxLength="3" CssClass="ntextblackdata" 
                                                    Width="120px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="ntextblack">
                                                Include Bounce
                                            </td>
                                            <td>
                                            </td>
                                            <td align="left">
                                                <asp:CheckBox ID="tab3_chkIncludeCallCenter" runat="server" CssClass="ntextblack" Text="Hardbounce Call Center" />&nbsp;&nbsp;
                                                <asp:CheckBox ID="tab3_chkIncludeEDM" runat="server" CssClass="ntextblack" Text="Hardbounce EDM" />&nbsp;&nbsp;
                                                <asp:CheckBox ID="tab3_chkIncludeSMS" runat="server" CssClass="ntextblack" Text="Hardbounce SMS" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:Button ID="tab3_btnRefresh" runat="server" style="display:none;" onclick="btnRefresh_Click" />
                        </fieldset>
                    </ContentTemplate>
                </asp:TabPanel>
            </asp:TabContainer>
            <table align="left">
                <tr>
                    <td align="right" style="width: 350px" class="ntextblack">Add Diageo VIP</td>
                    <td style="width: 20px">
                    </td>
                    <td align="left">
                        <asp:Button ID="btnBrowseDiageo" runat="server" Width="140px" 
                            Text="Browse Excel File" CssClass="ntextblack" 
                            onclick="btnBrowseDiageo_Click" />
                          
                    </td>
                    <td style="width: 20px">
                    </td>
                    <td align="left" style="width: 250px" class="ntextblack">
                        <asp:Label ID="lblDiageoVIPPath" runat="server" 
                            Text="Display File Path and File Name "></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="ntextblack">Add VIP Outlet</td>
                    <td></td>
                    <td align="left">
                        <asp:Button ID="btnBrowseOutlet" runat="server" Width="140px" 
                            Text="Browse Excel File" CssClass="ntextblack" 
                            onclick="btnBrowseOutlet_Click" />
                    </td>
                    <td></td>
                    <td align="left" class="ntextblack">
                        <asp:Label ID="lblOutletVipPath" runat="server" 
                            Text="Display File Path and File Name "></asp:Label>
                    &nbsp;</td>
                </tr>
                <tr>
                    <td align="right" class="ntextblack">Exclude Block List</td>
                    <td></td>
                    <td align="left" class="ntextblack">
                        <asp:CheckBox ID="chkExcludeBlockList" runat="server" Text="Yes" />
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="5"></td>
                </tr>
                <tr id="trSaveData" runat="server">
                    <td>
                    </td>
                    <td colspan="4" align="center">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="ntab" 
                            Width="120px" onclick="btnSave_Click" />&nbsp;
                        <asp:Button ID="btnCancel" CssClass="ntab" runat="server" Text="CANCEL" OnClick="btnCancel_Click"
                            TabIndex="3" Width="120px" />
                    </td>
                </tr>
                <tr style="height:10px"><td>&nbsp;</td></tr>
                <tr id="trExtractData" runat="server">
                    <td colspan="1">
                    </td>
                    <td colspan="3" align="center">
                        <asp:Button ID="btnCount" CssClass="ntab" runat="server" Text="COUNT" 
                            onclick="btnCount_Click" />&nbsp;
                        <asp:Button ID="btnExtractData" CssClass="ntab" runat="server" 
                            Text="EXTRACT DATA" onclick="btnExtractData_Click" />&nbsp;
                        <asp:Button ID="btnCancelExtract" CssClass="ntab" runat="server" Text="CANCEL" />                                                                    
                    </td>
                </tr>
            </table>
            <asp:Button ID="btnDiageoVip" runat="server" Text="Save" style="visibility:hidden"
                            Width="120px" onclick="btnDiageoVip_Click"  />
                        <asp:Button ID="btnOutletVip" runat="server" Text="Save" style="visibility:hidden" 
                            Width="120px" onclick="btnOutletVip_Click"  />
        </fieldset>
        
        <asp:Label ID="ErrorMessage" runat="server" Text=""></asp:Label>        
    </div>
    </form>
</body>
</html>
