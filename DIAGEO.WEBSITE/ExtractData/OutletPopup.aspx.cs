﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class ExtractData_PlatFormPopup : BasePage
{
    #region Property

    public DataTable dtOutlet
    {
        get { return (DataTable)ViewState["dtOutlet"]; }
        set { ViewState["dtOutlet"] = value; }
    }

    public ArrayList ArrOutletID
    {
        get { return (ArrayList)Session["ArrOutletID" + TabNumber.ToString()]; }
        set { Session["ArrOutletID" + TabNumber.ToString()] = value; }
    }

    public ArrayList ArrOutletName
    {
        get { return (ArrayList)Session["ArrOutletName" + TabNumber.ToString()]; }
        set { Session["ArrOutletName" + TabNumber.ToString()] = value; }
    }

    public int TabNumber
    {
        get { return (int)ViewState["TabNumber"]; }
        set { ViewState["TabNumber"] = value; }
    }

    #endregion

    #region Method

    private void BindOutlet()
    {
        OutletBLL outletBLL = new OutletBLL();
        try
        {
            dtOutlet = outletBLL.SelectActiveOutletOrderByName("T");
            dgOutlet.DataSource = dtOutlet;
            dgOutlet.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("BindOutlet :: " + ex.Message);
        }
    }

    private void KeepItemOutlet()
    {
        int Index;
        try
        {
            if (ArrOutletID == null)
            {
                ArrOutletID = new ArrayList();
                ArrOutletName = new ArrayList();
            }
            for (int i = 0; i < dgOutlet.Items.Count; i++)
            {
                CheckBox chk = (CheckBox)dgOutlet.Items[i].FindControl("chkOutlet");
                if (chk.Checked == true)
                {
                    if (ArrOutletID.Count == 0)
                    {
                        ArrOutletID.Add(dgOutlet.Items[i].Cells[2].Text.ToString());
                        ArrOutletName.Add(dgOutlet.Items[i].Cells[1].Text.ToString());
                    }
                    else
                    {
                        for (int j = 0; j < ArrOutletID.Count; j++)
                        {
                            if (dgOutlet.Items[i].Cells[2].Text.ToString().Equals(ArrOutletID[j].ToString()))
                                break;
                            if (j == ArrOutletID.Count - 1)
                            {
                                ArrOutletID.Add(dgOutlet.Items[i].Cells[2].Text.ToString());
                                ArrOutletName.Add(dgOutlet.Items[i].Cells[1].Text.ToString());
                            }
                        }
                    }
                }
                else
                {
                    if (ArrOutletID.Count > 0)
                    {
                        Index = -1;
                        for (int j = 0; j < ArrOutletID.Count; j++)
                        {
                            if (dgOutlet.Items[i].Cells[2].Text.ToString().Equals(ArrOutletID[j].ToString()))
                                Index = j;
                        }
                        if (Index != -1)
                        {
                            ArrOutletID.RemoveAt(Index);
                            ArrOutletName.RemoveAt(Index);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("KeepItemOutlet :: " + ex.Message);
        }
    }

    private void BindItemOutlet()
    {
        try
        {
            if (ArrOutletID.Count > 0)
            {
                for (int i = 0; i < dgOutlet.Items.Count; i++)
                {
                    CheckBox chk = (CheckBox)dgOutlet.Items[i].FindControl("chkOutlet");
                    for (int j = 0; j < ArrOutletID.Count; j++)
                    {
                        if (dgOutlet.Items[i].Cells[2].Text.ToString().Equals(ArrOutletID[j].ToString()))
                            chk.Checked = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindItemOutlet :: " + ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                TabNumber = int.Parse(Request.QueryString["tabNumber"].ToString());
                BindOutlet();
                //if (int.Parse(Request.QueryString["item"].ToString()) > 0)
                if (ArrOutletID != null)
                {
                    //ArrOutletID = (ArrayList)Session["ArrOutletID" + TabNumber.ToString()];
                    BindItemOutlet();
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutletPopup.Page_Load :: " + ex.Message);
        }
    }

    protected void chkHeaderOutlet_OnCheckedChanged(object sender, EventArgs e)
    {
        try
        {

            CheckBox chkHead = (CheckBox)sender;

            for (int i = 0; i < dgOutlet.Items.Count; i++)
            {
                CheckBox chk = (CheckBox)dgOutlet.Items[i].FindControl("chkOutlet");
                chk.Checked = chkHead.Checked;
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutletPopup.chkHeaderOutlet_OnCheckedChanged :: " + ex.Message);
        }
    }

    protected void btnSearchOutlet_Click(object sender, EventArgs e)
    {
        DataRow[] drOutlet;
        DataRow drSearch;
        DataTable dtSearch = new DataTable("Outlet");
        try
        {
            KeepItemOutlet();
            if (txtSearchOutlet.Text.Trim().Equals(""))
            {
                dgOutlet.DataSource = dtOutlet;
                dgOutlet.DataBind();
                BindItemOutlet();
            }
            else
            {
                drOutlet = dtOutlet.Select("name LIKE '%" + txtSearchOutlet.Text.Trim() + "%'");
                if (drOutlet.Length > 0)
                {
                    dtSearch = dtOutlet.Clone();
                    foreach (DataRow dr in drOutlet)
                    {
                        drSearch = dtSearch.NewRow();
                        drSearch.ItemArray = dr.ItemArray;
                        dtSearch.Rows.Add(drSearch);
                    }
                    dgOutlet.DataSource = dtSearch;
                    dgOutlet.DataBind();
                    BindItemOutlet();
                }
                else
                    JsClientAlertWithFocus("This Outlet name is not found.", txtSearchOutlet);
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutletPopup.btnSearch_Click :: " + ex.Message);
        }
    }

    protected void btnSelect_Click(object sender, EventArgs e)
    {
        //string outlet = "";
        //CheckBox chkOutlet;
        //ExtractData extractData;
        //DataTable dtOutlet;
        //DataRow drOutletRegisterName;

        try
        {
            KeepItemOutlet();
            //extractData = new ExtractData();
            //dtOutlet = new DataTable();
            //dtOutlet.Columns.Add(new DataColumn("Name"));
            //dtOutlet.Columns.Add(new DataColumn("Value"));
            //for (int i = 0; i <= dgOutlet.Rows.Count - 1; i++)
            //{
            //    chkOutlet = dgOutlet.Rows[i].FindControl("chkOutlet") as CheckBox;
            //    if (chkOutlet.Checked == true)
            //    {
            //        outlet += dgOutlet.DataKeys[i][0].ToString() + ",";
            //        drOutletRegisterName = dtOutlet.NewRow();
            //        drOutletRegisterName[0] = dgOutlet.Rows[i].Cells[1].Text;
            //        drOutletRegisterName[1] = dgOutlet.DataKeys[i][0].ToString();
            //        dtOutlet.Rows.Add(drOutletRegisterName);
            //    }		          
            //}

            //if (Request.QueryString["ComeFrom"].ToString() == "Register")            
            //    extractData.OutletRegisterName = dtOutlet;
            //else
            //    extractData.LastVisitOutlet = dtOutlet;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "closePopup", "window.close();", true);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutletPopup.btnSelect_Click :: " + ex.Message);
        }
    }

    #endregion
}
