﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeFile="ExtractCampaignSpecificData.aspx.cs" Inherits="ExtractData_ExtractCampaignSpecificData" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="ITOS.Utility.CustomControl" Namespace="ITOS.Utility.CustomControl" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../Style/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    <base target="_self"/>
    
   <script type="text/javascript">

       function geteventdtpcontrol() {
           $(function() {
               var dateBefore1 = null;
               $("#dtp_EventDateFrom").datepicker({
                   dateFormat: 'dd/mm/yy',
                   showOn: 'button',
                   dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                   monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                   changeMonth: true,
                   changeYear: true,
                   beforeShow: function() {
                       if ($(this).val() != "") {
                           var arrayDate = $(this).val().split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);

                   },
                   onChangeMonthYear: function() {
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);
                   },
                   onClose: function() {
                       if ($(this).val() != "" && $(this).val() == dateBefore1) {
                           var arrayDate = dateBefore1.split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                   },
                   onSelect: function(dateText, inst) {
                       dateBefore1 = $(this).val();
                       var arrayDate = dateText.split("/");
                       arrayDate[2] = parseInt(arrayDate[2]);
                       $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                   }

               });

           });

           $(function() {
               var dateBefore2 = null;
               $("#dtp_EventDateTo").datepicker({
                   dateFormat: 'dd/mm/yy',
                   showOn: 'button',
                   dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                   monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                   changeMonth: true,
                   changeYear: true,
                   beforeShow: function() {
                       if ($(this).val() != "") {
                           var arrayDate = $(this).val().split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);

                   },
                   onChangeMonthYear: function() {
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);
                   },
                   onClose: function() {
                       if ($(this).val() != "" && $(this).val() == dateBefore2) {
                           var arrayDate = dateBefore2.split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                   },
                   onSelect: function(dateText, inst) {
                       dateBefore2 = $(this).val();
                       var arrayDate = dateText.split("/");
                       arrayDate[2] = parseInt(arrayDate[2]);
                       $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                   }

               });

           });
       
       }

       function getdatecontrol(cName) {
           $(function() {
               var dateBefore1 = null;
               $("#dtp_"+cName+"From_1").datepicker({
                   dateFormat: 'dd/mm/yy',
                   showOn: 'button',
                   dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                   monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                   changeMonth: true,
                   changeYear: true,
                   beforeShow: function() {
                       if ($(this).val() != "") {
                           var arrayDate = $(this).val().split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);

                   },
                   onChangeMonthYear: function() {
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);
                   },
                   onClose: function() {
                       if ($(this).val() != "" && $(this).val() == dateBefore1) {
                           var arrayDate = dateBefore1.split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                   },
                   onSelect: function(dateText, inst) {
                       dateBefore1 = $(this).val();
                       var arrayDate = dateText.split("/");
                       arrayDate[2] = parseInt(arrayDate[2]);
                       $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                   }

               });

           });

           $(function() {
               var dateBefore2 = null;
               $("#dtp_" + cName + "To_1").datepicker({
                   dateFormat: 'dd/mm/yy',
                   showOn: 'button',
                   dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                   monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                   changeMonth: true,
                   changeYear: true,
                   beforeShow: function() {
                       if ($(this).val() != "") {
                           var arrayDate = $(this).val().split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);

                   },
                   onChangeMonthYear: function() {
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);
                   },
                   onClose: function() {
                       if ($(this).val() != "" && $(this).val() == dateBefore2) {
                           var arrayDate = dateBefore2.split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                   },
                   onSelect: function(dateText, inst) {
                       dateBefore2 = $(this).val();
                       var arrayDate = dateText.split("/");
                       arrayDate[2] = parseInt(arrayDate[2]);
                       $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                   }

               });

           });

           $(function() {
               var dateBefore5 = null;
               $("#dtp_" + cName + "From_2").datepicker({
                   dateFormat: 'dd/mm/yy',
                   showOn: 'button',
                   dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                   monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                   changeMonth: true,
                   changeYear: true,
                   beforeShow: function() {
                       if ($(this).val() != "") {
                           var arrayDate = $(this).val().split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);

                   },
                   onChangeMonthYear: function() {
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);
                   },
                   onClose: function() {
                       if ($(this).val() != "" && $(this).val() == dateBefore5) {
                           var arrayDate = dateBefore5.split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                   },
                   onSelect: function(dateText, inst) {
                       dateBefore5 = $(this).val();
                       var arrayDate = dateText.split("/");
                       arrayDate[2] = parseInt(arrayDate[2]);
                       $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                   }

               });

           });

           $(function() {
               var dateBefore6 = null;
               $("#dtp_" + cName + "To_2").datepicker({
                   dateFormat: 'dd/mm/yy',
                   showOn: 'button',
                   dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                   monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                   changeMonth: true,
                   changeYear: true,
                   beforeShow: function() {
                       if ($(this).val() != "") {
                           var arrayDate = $(this).val().split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);

                   },
                   onChangeMonthYear: function() {
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val()) + 543;
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);
                   },
                   onClose: function() {
                       if ($(this).val() != "" && $(this).val() == dateBefore6) {
                           var arrayDate = dateBefore6.split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                   },
                   onSelect: function(dateText, inst) {
                       dateBefore6 = $(this).val();
                       var arrayDate = dateText.split("/");
                       arrayDate[2] = parseInt(arrayDate[2]);
                       $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                   }

               });

           });

           $(function() {
               var dateBefore9 = null;
               $("#dtp_" + cName + "From_3").datepicker({
                   dateFormat: 'dd/mm/yy',
                   showOn: 'button',
                   dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                   monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                   changeMonth: true,
                   changeYear: true,
                   beforeShow: function() {
                       if ($(this).val() != "") {
                           var arrayDate = $(this).val().split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);

                   },
                   onChangeMonthYear: function() {
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);
                   },
                   onClose: function() {
                       if ($(this).val() != "" && $(this).val() == dateBefore9) {
                           var arrayDate = dateBefore9.split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                   },
                   onSelect: function(dateText, inst) {
                       dateBefore9 = $(this).val();
                       var arrayDate = dateText.split("/");
                       arrayDate[2] = parseInt(arrayDate[2]);
                       $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                   }

               });

           });

           $(function() {
               var dateBefore10 = null;
               $("#dtp_" + cName + "To_3").datepicker({
                   dateFormat: 'dd/mm/yy',
                   showOn: 'button',
                   dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                   monthNamesShort: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                   changeMonth: true,
                   changeYear: true,
                   beforeShow: function() {
                       if ($(this).val() != "") {
                           var arrayDate = $(this).val().split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);

                   },
                   onChangeMonthYear: function() {
                       setTimeout(function() {
                           $.each($(".ui-datepicker-year option"), function(j, k) {
                               var textYear = parseInt($(".ui-datepicker-year option").eq(j).val());
                               $(".ui-datepicker-year option").eq(j).text(textYear);
                           });
                       }, 50);
                   },
                   onClose: function() {
                       if ($(this).val() != "" && $(this).val() == dateBefore10) {
                           var arrayDate = dateBefore10.split("/");
                           arrayDate[2] = parseInt(arrayDate[2]);
                           $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                       }
                   },
                   onSelect: function(dateText, inst) {
                       dateBefore10 = $(this).val();
                       var arrayDate = dateText.split("/");
                       arrayDate[2] = parseInt(arrayDate[2]);
                       $(this).val(arrayDate[0] + "/" + arrayDate[1] + "/" + arrayDate[2]);
                   }

               });

           });
       }
    </script>
    
    
    <style type="text/css">
        .ntab
        {
        	border-color:Black;
            background-color:#2890C7;
            color:White;
            font:bold 13px Arial, Helvetica, sans-serif;
        }
        .scrollingControlContainer
        {
            overflow-x: hidden;
            overflow-y: scroll;
        }

        .scrollingCheckBoxList
        {
            border: 1px #808080 solid;
            margin: 0px 0px 0px 0px;
            height: 95px;
        }
        .dataconditionSet
        {
            border: 1px #808080 solid;
            margin: 0px 0px 0px 0px;
        }
    </style>
</head>
<body>
    <form id="frmExtractCampaignSpecificData" runat="server">
    <%--<asp:ToolkitScriptManager ID="scriptManager" EnableScriptGlobalization="true" runat="server" AsyncPostBackTimeout="8000000">
    </asp:ToolkitScriptManager>--%>
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"/>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
    <ContentTemplate>
    <div>
        <fieldset id="FieldSet6" runat="server">
        <legend id="Legend6" runat="server" class="ntextblack">Master Data</legend>
            <table width="90%">
                <tr id="tr2" name="trEventName" runat="server">
                    <td align="right" style="width: 250px" class="ntextblack">
                        Event ID
                    </td>
                    <td style="width: 20px">
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txt_EventID" runat="server" CssClass="ntextblackdata" 
                            Width="350px" Enabled="False"></asp:TextBox>
                    </td>
                </tr>
                <tr id="tr3" name="trEventName" runat="server">
                    <td align="right" style="width: 250px" class="ntextblack">
                        Event Name
                    </td>
                    <td style="width: 20px">
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txt_EventName" runat="server" CssClass="ntextblackdata" Width="350px"></asp:TextBox>
                    </td>
                </tr>
                <tr id="tr8" name="trEventName" runat="server">
                    <td align="right" style="width: 250px" class="ntextblack">
                        Event Date
                    </td>
                    <td style="width: 20px">
                    </td>
                    <td align="left">
                        <span class="ntextblack" style="vertical-align:middle">From</span>
                        &nbsp;&nbsp;
                        <asp:TextBox ID="dtp_EventDateFrom" runat="server" Width="100px" 
                            CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <span class="ntextblack" style="vertical-align:middle">To</span>
                        &nbsp;&nbsp;
                        <asp:TextBox ID="dtp_EventDateTo" runat="server" Width="100px" 
                            CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </fieldset>
    </div>
    <fieldset id="FieldSet5" runat="server">
        <legend id="Legend5" runat="server" class="ntextblack">Data Condition Set 1</legend>
        <table class="ntextblack">
            <tr>
                <td>
                    <div>
                        &nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="chk_Condition_1" runat="server" AutoPostBack="true"
                            oncheckedchanged="chk_Condition_CheckedChanged" />
                        <asp:LinkButton ID="btn_Condition_1" runat="server" Text="Click to Expand / Hide" 
                            onclick="btn_Condition_1_Click" ></asp:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
        <div id="div_Condition_1" runat="server">
        <div>
            <fieldset id="FieldSet" runat="server">
            <legend id="HeaderPage" runat="server" class="ntextblack">Primary Data</legend>
                <table width="90%">
                    <tr id="trEventNameTab1" name="trEventName" runat="server">
                        <td align="right" style="width: 250px" class="ntextblack">
                            Platform
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td align="left">
                            <asp:CheckBoxList ID="chkList_Platform_1" runat="server" CssClass="ntextblack" 
                                RepeatColumns="3" RepeatDirection="Horizontal" AutoPostBack ="true"
                                onselectedindexchanged="chkList_SelectedIndexChanged">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr id="tr1" runat="server">
                        <td align="right" style="width: 250px" class="ntextblack">
                            Message Sender
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txt_SenderName_1" runat="server" CssClass="ntextblackdata" Width="350px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div>
            <fieldset id="FieldSet1" runat="server">
            <legend id="Legend1" runat="server" class="ntextblack">Customer Data</legend>
            <table width="100%" class="ntextblack">
                <tr>
                    <td class="style4">
                        <asp:Label ID="Label1" runat="server" Text="Demographic" 
                            style="text-decoration: underline"></asp:Label>
                    </td>
                    <td style="text-align: right">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    <table width="90%">
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Gender
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rbtn_ListGender_1" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="Male">Male</asp:ListItem>
                                    <asp:ListItem Value="Female">Female</asp:ListItem>
                                    <asp:ListItem Value="All">All</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Age
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <span class="ntextblack" style="vertical-align: middle">From</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="txt_AgeFrom_1" runat="server" CssClass="ntextblackdata" MaxLength="3" 
                                    OnKeyPress="validateNumKey()" Style="vertical-align: middle" Width="80px"></asp:TextBox>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="ntextblack" style="vertical-align: middle">To</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="txt_AgeTo_1" runat="server" CssClass="ntextblackdata" MaxLength="3" 
                                    OnKeyPress="validateNumKey()" Style="vertical-align: middle" Width="80px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Province
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <table>
                                    <tr>
                                        <td style="width:200px">
                                            <asp:Panel ID="Panel1" runat="server" 
                                                CssClass="scrollingControlContainer scrollingCheckBoxList" Width="100%">
                                            <asp:CheckBoxList ID="chk_Province_1" runat="server" CssClass="ntextblack" 
                                                RepeatDirection="Vertical" AutoPostBack ="true" onselectedindexchanged="chkList_SelectedIndexChanged">
                                            </asp:CheckBoxList>
                                            </asp:Panel>
                                        </td>
                                        <td style="width:400px">
                                            <asp:CheckBox ID="chk_IncludeUnknown_1" runat="server" Text="Include Unknown" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Birthday Range
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <span class="ntextblack" style="vertical-align:middle">From</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="dtp_BirthDateFrom_1" runat="server" Width="100px" 
                                    CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="ntextblack" style="vertical-align:middle">To</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="dtp_BirthDateTo_1" runat="server" Width="100px" 
                                    CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Outlet
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:Panel ID="Panel2" runat="server" 
                                    CssClass="scrollingControlContainer scrollingCheckBoxList" Width="40%">
                                <asp:CheckBoxList ID="chk_Outlet_1" runat="server" CssClass="ntextblack" 
                                    RepeatDirection="Vertical" AutoPostBack ="true" onselectedindexchanged="chkList_SelectedIndexChanged">
                                </asp:CheckBoxList>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Brand Preference
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:Panel ID="checkBoxPanel" runat="server" 
                                    CssClass="scrollingControlContainer scrollingCheckBoxList" Width="40%">
                                <asp:CheckBoxList ID="chk_Brand_1" runat="server" CssClass="ntextblack" 
                                    RepeatDirection="Vertical" AutoPostBack ="true" onselectedindexchanged="chkList_SelectedIndexChanged">
                                    <asp:ListItem>All</asp:ListItem>
                                    <asp:ListItem Value="brandPreJwGold">Johnnie Walker Gold Label</asp:ListItem>
                                    <asp:ListItem Value="brandPreJwBlack">Johnnie Walker Black Label</asp:ListItem>
                                    <asp:ListItem Value="brandPreJwRed">Johnnie Walker Red Label</asp:ListItem>
                                    <asp:ListItem Value="brandPreSmirnoff">Smirnoff</asp:ListItem>
                                    <asp:ListItem Value="brandPreBenmore">Benmore</asp:ListItem>
                                    <asp:ListItem Value="brandPreHennessy">Hennessy</asp:ListItem>
                                    <asp:ListItem Value="brandPre100Pipers">100Pipers</asp:ListItem>
                                    <asp:ListItem Value="brandPreAbsolut">Absolut</asp:ListItem>
                                    <asp:ListItem Value="brandPreBallentine">Ballentine</asp:ListItem>
                                    <asp:ListItem Value="brandPreChivas">Chivas regal</asp:ListItem>
                                    <asp:ListItem Value="brandPreBlend">Blend285</asp:ListItem>
                                </asp:CheckBoxList>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Active Period
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <span class="ntextblack" style="vertical-align:middle">From</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="dtp_ActPeriodFrom_1" runat="server" Width="100px" 
                                    CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="ntextblack" style="vertical-align:middle">To</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="dtp_ActPeriodTo_1" runat="server" Width="100px" 
                                    CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Opt-in
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:CheckBoxList ID="chk_OptIn_1" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="validOptInDiageo">Diageo Opt-in</asp:ListItem>
                                    <asp:ListItem Value="validOptInOutlet">Outlet Opt-in</asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                4A's
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:CheckBoxList ID="chk_4A_1" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="Adorer">Adorer</asp:ListItem>
                                    <asp:ListItem Value="Adopter">Adopter</asp:ListItem>
                                    <asp:ListItem Value="Acceptor">Acceptor</asp:ListItem>
                                    <asp:ListItem Value="Available">Available</asp:ListItem>
                                    <asp:ListItem Value="Rejecter">Rejecter</asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                    </td>
                </tr>
            </table>
            </fieldset>
        </div>
        <div>
            <fieldset id="FieldSet2" runat="server">
            <legend id="Legend2" runat="server" class="ntextblack">Communication Data</legend>
            <table width="100%" class="ntextblack">
                <tr>
                    <td>
                    <div>
                        &nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="chk_EDM_1" runat="server" />
                        <asp:Label ID="Label2" runat="server" Text="EDM" 
                            style="text-decoration: underline"></asp:Label>
                    </div>
                    <div>
                        <table width="90%">
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Email Active
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rdbtn_EDMValid_1" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="T">Valid</asp:ListItem>
                                    <asp:ListItem Value="F">Invalid</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Receiving Status
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 200px">
                                            <asp:DropDownList ID="ddl_EDM_1" runat="server" Width="250px" AutoPostBack="true"
                                                CssClass="ntextblackdata" onselectedindexchanged="ddlEDM_SelectedIndexChanged">
                                                <asp:ListItem Value="0">- Select status -</asp:ListItem>
                                                <asp:ListItem Value="101">was in (any of/all of) campaign or etc.</asp:ListItem>
                                                <asp:ListItem Value="102">has opened any email</asp:ListItem>
                                                <asp:ListItem Value="103">has clicked a link in any email</asp:ListItem>
                                                <asp:ListItem Value="104">have never been received any email</asp:ListItem>
                                                <asp:ListItem Value="105">where the number of broadcast</asp:ListItem>
                                                <asp:ListItem Value="106">where the number of email opened/ link clicked</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        <div id="div_EDMTimes_1" runat="server">
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="txtEDMTimes_1" OnKeyPress="validateNumKey()" MaxLength="2" runat="server" CssClass="ntextblackdata" Style="vertical-align: middle" Width="90px"></asp:TextBox>
                                            &nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">Times</span>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        <div id="div_EDMPeriod_1" runat="server">
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">- Select status period</span>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">From</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_EDMFrom_1" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">To</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_EDMTo_1" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                        </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <div>
                                    <asp:Button ID="btn_EDMAddCondition_1" runat="server" CssClass="ntextblack" Text="Add" 
                                        Width="80px" onclick="btnAddCondition_Click" />
                                    <asp:Button ID="btn_DelConditionEDM_1" runat="server" CssClass="ntextblack" 
                                        Text="Remove" Width="80px" onclick="btnRemoveCondition_Click" />
                                </div>
                                <div>
                                    <asp:DataGrid ID="dg_EDMCondition_1" runat="server" AutoGenerateColumns="False" >
                                        <AlternatingItemStyle CssClass="gridalternativerowstyle" 
                                            HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkDeleteAllEDM_1" OnCheckedChanged="ClearCheck" AutoPostBack="true" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkDelete" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="Type" HeaderText="Type">
                                                <HeaderStyle Width="250px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="visitFrequency" HeaderText="Times">
                                                <HeaderStyle Width="30px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateFrom" HeaderText="Date From">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateTo" HeaderText="Date To">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="TypeIDDetail" Visible="false"></asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="Gray" CssClass="gridheaderrowstyle" 
                                            HorizontalAlign="Center" />
                                        <ItemStyle CssClass="gridrowstyle" HorizontalAlign="Center" />
                                    </asp:DataGrid>
                                </div>
                            </td>
                        </tr>
                        </table>
                    </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    <div>
                        &nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="chk_SMS_1" runat="server" />
                        <asp:Label ID="Label3" runat="server" Text="SMS" 
                            style="text-decoration: underline"></asp:Label>
                    </div>
                    <div>
                        <table width="90%">
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Mobile No Active
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rdbtn_SMSValid_1" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="T">Valid</asp:ListItem>
                                    <asp:ListItem Value="F">Invalid</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Receiving Status
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 200px">
                                            <asp:DropDownList ID="ddl_SMS_1" runat="server" Width="250px" 
                                                CssClass="ntextblackdata" AutoPostBack="true"
                                                onselectedindexchanged="ddl_SMSRecsts_SelectedIndexChanged">
                                                <asp:ListItem Value="0">- Select status -</asp:ListItem>
                                                <asp:ListItem Value="201">was in (any of/all of) campaign or etc.</asp:ListItem>
                                                <asp:ListItem Value="202">have never been received any SMS</asp:ListItem>
                                                <asp:ListItem Value="203">where the number of broadcast</asp:ListItem>
                                                <asp:ListItem Value="204">where the number of SMS received</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        <div id="div_SMSTimes_1" runat="server">
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="txtSMSTimes_1" OnKeyPress="validateNumKey()" MaxLength="2" runat="server" CssClass="ntextblackdata" Style="vertical-align: middle" Width="90px"></asp:TextBox>
                                            &nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">Times</span>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        <div id="div_SMSPeriod_1" runat="server">
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">- Select status period</span>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">From</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_SMSFrom_1" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">To</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_SMSTo_1" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                        </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <div>
                                    <asp:Button ID="btn_SMSAddCondition_1" runat="server" CssClass="ntextblack" Text="Add" 
                                        Width="80px" onclick="btn_SMSAddCondition_1_Click" />
                                    <asp:Button ID="btn_DelConditionSMS_1" runat="server" CssClass="ntextblack" 
                                        Text="Remove" Width="80px" onclick="btnRemoveCondition_Click" />
                                </div>
                                <div>
                                    <asp:DataGrid ID="dg_SMSCondition_1" runat="server" AutoGenerateColumns="False" >
                                        <AlternatingItemStyle CssClass="gridalternativerowstyle" 
                                            HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkDeleteAllSMS_1" OnCheckedChanged="ClearCheck" AutoPostBack="true" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkDelete" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="Type" HeaderText="Type">
                                                <HeaderStyle Width="250px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="visitFrequency" HeaderText="Times">
                                                <HeaderStyle Width="30px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateFrom" HeaderText="Date From">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateTo" HeaderText="Date To">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="TypeIDDetail" Visible="false"></asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="Gray" CssClass="gridheaderrowstyle" 
                                            HorizontalAlign="Center" />
                                        <ItemStyle CssClass="gridrowstyle" HorizontalAlign="Center" />
                                    </asp:DataGrid>
                                </div>
                            </td>
                        </tr>
                        </table>
                    </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    <div>
                        &nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="chk_Call_1" runat="server" />
                        <asp:Label ID="Label4" runat="server" Text="Call Center" 
                            style="text-decoration: underline"></asp:Label>
                    </div>
                    <div>
                        <table width="90%">
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Contact Number Active
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rdbtn_CallCenterValid_1" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="T">Valid</asp:ListItem>
                                    <asp:ListItem Value="F">Invalid</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Receiving Status
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                
                                <table width="100%">
                                    <tr>
                                        <td style="width: 200px">
                                            <asp:DropDownList ID="ddl_Call_1" runat="server" Width="250px" 
                                                CssClass="ntextblackdata" AutoPostBack="true"
                                                onselectedindexchanged="ddl_CallRecsts_SelectedIndexChanged">
                                                <asp:ListItem Value="0">- Select status -</asp:ListItem>
                                                <asp:ListItem Value="301">was in (any of/all of) campaign or etc.</asp:ListItem>
                                                <asp:ListItem Value="302">call center status</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        <div id="div_CallTimes_1" runat="server">
                                            &nbsp;&nbsp;
                                            <asp:DropDownList ID="ddl_CallStatus_1" runat="server" Width="150px" 
                                                CssClass="ntextblackdata">
                                                <asp:ListItem Value="">- Select status -</asp:ListItem>
                                                <asp:ListItem Value="1">success</asp:ListItem>
                                                <asp:ListItem Value="0">fail</asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">Status</span>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        <div id="div_CallPeriod_1" runat="server">
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">- Select status period</span>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">From</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_CallFrom_1" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">To</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_CallTo_1" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                        </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <div>
                                    <asp:Button ID="btn_CallAddCondition_1" runat="server" CssClass="ntextblack" Text="Add" 
                                        Width="80px" onclick="btn_CallAddCondition_1_Click" />
                                    <asp:Button ID="btn_DelConditionCal_1" runat="server" CssClass="ntextblack" 
                                        Text="Remove" Width="80px" onclick="btnRemoveCondition_Click" />
                                </div>
                                <div>
                                    <asp:DataGrid ID="dg_CallCondition_1" runat="server" AutoGenerateColumns="False" >
                                        <AlternatingItemStyle CssClass="gridalternativerowstyle" 
                                            HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkDeleteAllCal_1" OnCheckedChanged="ClearCheck" AutoPostBack="true" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkDelete" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="Type" HeaderText="Type">
                                                <HeaderStyle Width="250px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Status" HeaderText="Status">
                                                <HeaderStyle Width="30px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateFrom" HeaderText="Date From">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateTo" HeaderText="Date To">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="TypeIDDetail" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="visitFrequency" Visible="false"></asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="Gray" CssClass="gridheaderrowstyle" 
                                            HorizontalAlign="Center" />
                                        <ItemStyle CssClass="gridrowstyle" HorizontalAlign="Center" />
                                    </asp:DataGrid>
                                </div>
                            </td>
                        </tr>
                        </table>
                    </div>
                    <div>
                        <table>
                        <tr>
                            <td colspan="3">
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="ntextblack">
                                &nbsp;&nbsp;&nbsp;
                                Include Bounce
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:CheckBoxList ID="chk_IncludeBounce_1" runat="server" CssClass="ntextblack" 
                                    RepeatColumns="3" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="includeCallcenterBounce">Hardbounce Call Center</asp:ListItem>
                                    <asp:ListItem Value="includeEDMBounce">Hardbounce EDM</asp:ListItem>
                                    <asp:ListItem Value="includeSMSBounce">Hardbounce SMS</asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        </table>
                    </div>
                    </td>
                </tr>
            </table>
            </fieldset>
        </div>
        </div>
    </fieldset>
    <fieldset id="FieldSet7" runat="server">
        <legend id="Legend7" runat="server" class="ntextblack">Data Condition Set 2</legend>
        <table class="ntextblack">
            <tr>
                <td>
                    <div>
                        &nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="chk_Condition_2" runat="server" AutoPostBack="true"
                            oncheckedchanged="chk_Condition_CheckedChanged" />
                        <asp:LinkButton ID="btn_Condition_2" runat="server" Text="Click to Expand / Hide" 
                            onclick="btn_Condition_2_Click" ></asp:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
        <div id="div_Condition_2" runat="server">
        <div>
            <fieldset id="FieldSet8" runat="server">
            <legend id="Legend8" runat="server" class="ntextblack">Primary Data</legend>
                <table width="90%">
                    <tr id="tr4" name="trEventName" runat="server">
                        <td align="right" style="width: 250px" class="ntextblack">
                            Platform
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td align="left">
                            <asp:CheckBoxList ID="chkList_Platform_2" runat="server" CssClass="ntextblack" 
                                RepeatColumns="3" RepeatDirection="Horizontal" AutoPostBack ="true" onselectedindexchanged="chkList_SelectedIndexChanged">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr id="tr5" runat="server">
                        <td align="right" style="width: 250px" class="ntextblack">
                            Message Sender
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txt_SenderName_2" runat="server" CssClass="ntextblackdata" Width="350px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div>
            <fieldset id="FieldSet9" runat="server">
            <legend id="Legend9" runat="server" class="ntextblack">Customer Data</legend>
            <table width="100%" class="ntextblack">
                <tr>
                    <td class="style4">
                        <asp:Label ID="Label5" runat="server" Text="Demographic" 
                            style="text-decoration: underline"></asp:Label>
                    </td>
                    <td style="text-align: right">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    <table width="90%">
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Gender
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rbtn_ListGender_2" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="M">Male</asp:ListItem>
                                    <asp:ListItem Value="F">Female</asp:ListItem>
                                    <asp:ListItem Value="A">All</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Age
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <span class="ntextblack" style="vertical-align: middle">From</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="txt_AgeFrom_2" runat="server" CssClass="ntextblackdata" MaxLength="3" 
                                    OnKeyPress="validateNumKey()" Style="vertical-align: middle" Width="80px"></asp:TextBox>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="ntextblack" style="vertical-align: middle">To</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="txt_AgeTo_2" runat="server" CssClass="ntextblackdata" MaxLength="3" 
                                    OnKeyPress="validateNumKey()" Style="vertical-align: middle" Width="80px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Province
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <table>
                                    <tr>
                                        <td style="width:200px">
                                            <asp:Panel ID="Panel4" runat="server" 
                                                CssClass="scrollingControlContainer scrollingCheckBoxList" Width="100%">
                                            <asp:CheckBoxList ID="chk_Province_2" runat="server" CssClass="ntextblack" 
                                                RepeatDirection="Vertical" AutoPostBack ="true" onselectedindexchanged="chkList_SelectedIndexChanged">
                                            </asp:CheckBoxList>
                                            </asp:Panel>
                                        </td>
                                        <td style="width:400px">
                                            <asp:CheckBox ID="chk_IncludeUnknown_2" runat="server" Text="Include Unknown" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Birthday Range
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <span class="ntextblack" style="vertical-align:middle">From</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="dtp_BirthDateFrom_2" runat="server" Width="100px" 
                                    CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="ntextblack" style="vertical-align:middle">To</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="dtp_BirthDateTo_2" runat="server" Width="100px" 
                                    CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Outlet
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:Panel ID="Panel5" runat="server" 
                                    CssClass="scrollingControlContainer scrollingCheckBoxList" Width="40%">
                                <asp:CheckBoxList ID="chk_Outlet_2" runat="server" CssClass="ntextblack" 
                                    RepeatDirection="Vertical" AutoPostBack ="true" onselectedindexchanged="chkList_SelectedIndexChanged">
                                </asp:CheckBoxList>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Brand Preference
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:Panel ID="Panel6" runat="server" 
                                    CssClass="scrollingControlContainer scrollingCheckBoxList" Width="40%">
                                <asp:CheckBoxList ID="chk_Brand_2" runat="server" CssClass="ntextblack" 
                                    RepeatDirection="Vertical" AutoPostBack ="true" onselectedindexchanged="chkList_SelectedIndexChanged">
                                    <asp:ListItem>All</asp:ListItem>                                    
                                    <asp:ListItem Value="brandPreJwGold">Johnnie Walker Gold Label</asp:ListItem>
                                    <asp:ListItem Value="brandPreJwBlack">Johnnie Walker Black Label</asp:ListItem>
                                    <asp:ListItem Value="brandPreJwRed">Johnnie Walker Red Label</asp:ListItem>
                                    <asp:ListItem Value="brandPreSmirnoff">Smirnoff</asp:ListItem>
                                    <asp:ListItem Value="brandPreBenmore">Benmore</asp:ListItem>
                                    <asp:ListItem Value="brandPreHennessy">Hennessy</asp:ListItem>
                                    <asp:ListItem Value="brandPre100Pipers">100Pipers</asp:ListItem>
                                    <asp:ListItem Value="brandPreAbsolut">Absolut</asp:ListItem>
                                    <asp:ListItem Value="brandPreBallentine">Ballentine</asp:ListItem>
                                    <asp:ListItem Value="brandPreChivas">Chivas regal</asp:ListItem>
                                    <asp:ListItem Value="brandPreBlend">Blend285</asp:ListItem>
                                </asp:CheckBoxList>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Active Period
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <span class="ntextblack" style="vertical-align:middle">From</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="dtp_ActPeriodFrom_2" runat="server" Width="100px" 
                                    CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="ntextblack" style="vertical-align:middle">To</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="dtp_ActPeriodTo_2" runat="server" Width="100px" 
                                    CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Opt-in
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:CheckBoxList ID="chk_OptIn_2" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="validOptInDiageo">Diageo Opt-in</asp:ListItem>
                                    <asp:ListItem Value="validOptInOutlet">Outlet Opt-in</asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                4A's
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:CheckBoxList ID="chk_4A_2" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="Adorer">Adorer</asp:ListItem>
                                    <asp:ListItem Value="Adopter">Adopter</asp:ListItem>
                                    <asp:ListItem Value="Acceptor">Acceptor</asp:ListItem>
                                    <asp:ListItem Value="Available">Available</asp:ListItem>
                                    <asp:ListItem Value="Rejecter">Rejecter</asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                    </td>
                </tr>
            </table>
            </fieldset>
        </div>
        <div>
            <fieldset id="FieldSet10" runat="server">
            <legend id="Legend10" runat="server" class="ntextblack">Communication Data</legend>
            <table width="100%" class="ntextblack">
                <tr>
                    <td>
                    <div>
                        &nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="chk_EDM_2" runat="server" />
                        <asp:Label ID="Label6" runat="server" Text="EDM" 
                            style="text-decoration: underline"></asp:Label>
                    </div>
                    <div>
                        <table width="90%">
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Email Active
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rdbtn_EDMValid_2" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="T">Valid</asp:ListItem>
                                    <asp:ListItem Value="F">Invalid</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Receiving Status
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 200px">
                                            <asp:DropDownList ID="ddl_EDM_2" runat="server" Width="250px" AutoPostBack="true"
                                                CssClass="ntextblackdata" onselectedindexchanged="ddlEDM_SelectedIndexChanged">
                                                <asp:ListItem Value="0">- Select status -</asp:ListItem>
                                                <asp:ListItem Value="101">was in (any of/all of) campaign or etc.</asp:ListItem>
                                                <asp:ListItem Value="102">has opened any email</asp:ListItem>
                                                <asp:ListItem Value="103">has clicked a link in any email</asp:ListItem>
                                                <asp:ListItem Value="104">have never been received any email</asp:ListItem>
                                                <asp:ListItem Value="105">where the number of broadcast</asp:ListItem>
                                                <asp:ListItem Value="106">where the number of email opened/ link clicked</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        <div id="div_EDMTimes_2" runat="server">
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="txtEDMTimes_2" OnKeyPress="validateNumKey()" MaxLength="2" runat="server" CssClass="ntextblackdata" Style="vertical-align: middle" Width="90px"></asp:TextBox>
                                            &nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">Times</span>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        <div id="div_EDMPeriod_2" runat="server">
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">- Select status period</span>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">From</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_EDMFrom_2" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">To</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_EDMTo_2" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                        </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <div>
                                    <asp:Button ID="btn_EDMAddCondition_2" runat="server" CssClass="ntextblack" Text="Add" 
                                        Width="80px" onclick="btn_EDMAddCondition_2_Click" />
                                    <asp:Button ID="btn_DelConditionEDM_2" runat="server" CssClass="ntextblack" 
                                        Text="Remove" Width="80px" onclick="btnRemoveCondition_Click" />
                                </div>
                                <div>
                                    <asp:DataGrid ID="dg_EDMCondition_2" runat="server" AutoGenerateColumns="False" >
                                        <AlternatingItemStyle CssClass="gridalternativerowstyle" 
                                            HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkDeleteAllEDM_2" OnCheckedChanged="ClearCheck" AutoPostBack="true" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkDelete" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="Type" HeaderText="Type">
                                                <HeaderStyle Width="250px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="visitFrequency" HeaderText="Times">
                                                <HeaderStyle Width="30px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateFrom" HeaderText="Date From">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateTo" HeaderText="Date To">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="TypeIDDetail" Visible="false"></asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="Gray" CssClass="gridheaderrowstyle" 
                                            HorizontalAlign="Center" />
                                        <ItemStyle CssClass="gridrowstyle" HorizontalAlign="Center" />
                                    </asp:DataGrid>
                                </div>
                            </td>
                        </tr>
                        </table>
                    </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    <div>
                        &nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="chk_SMS_2" runat="server" />
                        <asp:Label ID="Label7" runat="server" Text="SMS" 
                            style="text-decoration: underline"></asp:Label>
                    </div>
                    <div>
                        <table width="90%">
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Mobile No Active
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rdbtn_SMSValid_2" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="T">Valid</asp:ListItem>
                                    <asp:ListItem Value="F">Invalid</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Receiving Status
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 200px">
                                            <asp:DropDownList ID="ddl_SMS_2" runat="server" Width="250px" 
                                                CssClass="ntextblackdata" AutoPostBack="true"
                                                onselectedindexchanged="ddl_SMSRecsts_SelectedIndexChanged">
                                                <asp:ListItem Value="0">- Select status -</asp:ListItem>
                                                <asp:ListItem Value="201">was in (any of/all of) campaign or etc.</asp:ListItem>
                                                <asp:ListItem Value="202">have never been received any SMS</asp:ListItem>
                                                <asp:ListItem Value="203">where the number of broadcast</asp:ListItem>
                                                <asp:ListItem Value="204">where the number of SMS received</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        <div id="div_SMSTimes_2" runat="server">
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="txtSMSTimes_2" OnKeyPress="validateNumKey()" MaxLength="2" runat="server" CssClass="ntextblackdata" Style="vertical-align: middle" Width="90px"></asp:TextBox>
                                            &nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">Times</span>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        <div id="div_SMSPeriod_2" runat="server">
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">- Select status period</span>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">From</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_SMSFrom_2" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">To</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_SMSTo_2" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                        </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <div>
                                    <asp:Button ID="btn_SMSAddCondition_2" runat="server" CssClass="ntextblack" Text="Add" 
                                        Width="80px" onclick="btn_SMSAddCondition_2_Click" />
                                    <asp:Button ID="btn_DelConditionSMS_2" runat="server" CssClass="ntextblack" 
                                        Text="Remove" Width="80px" onclick="btnRemoveCondition_Click"/>
                                </div>
                                <div>
                                    <asp:DataGrid ID="dg_SMSCondition_2" runat="server" AutoGenerateColumns="False" >
                                        <AlternatingItemStyle CssClass="gridalternativerowstyle" 
                                            HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkDeleteAllSMS_2" OnCheckedChanged="ClearCheck" AutoPostBack="true" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkDelete" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="Type" HeaderText="Type">
                                                <HeaderStyle Width="250px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="visitFrequency" HeaderText="Times">
                                                <HeaderStyle Width="30px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateFrom" HeaderText="Date From">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateTo" HeaderText="Date To">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="TypeIDDetail" Visible="false"></asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="Gray" CssClass="gridheaderrowstyle" 
                                            HorizontalAlign="Center" />
                                        <ItemStyle CssClass="gridrowstyle" HorizontalAlign="Center" />
                                    </asp:DataGrid>
                                </div>
                            </td>
                        </tr>
                        </table>
                    </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    <div>
                        &nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="chk_Call_2" runat="server" />
                        <asp:Label ID="Label4x" runat="server" Text="Call Center" 
                            style="text-decoration: underline"></asp:Label>
                    </div>
                    <div>
                        <table width="90%">
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Contact Number Active
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rdbtn_CallCenterValid_2" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="T">Valid</asp:ListItem>
                                    <asp:ListItem Value="F">Invalid</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Receiving Status
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                
                                <table width="100%">
                                    <tr>
                                        <td style="width: 200px">
                                            <asp:DropDownList ID="ddl_Call_2" runat="server" Width="250px" 
                                                CssClass="ntextblackdata" AutoPostBack="true"
                                                onselectedindexchanged="ddl_CallRecsts_SelectedIndexChanged">
                                                <asp:ListItem Value="0">- Select status -</asp:ListItem>
                                                <asp:ListItem Value="301">was in (any of/all of) campaign or etc.</asp:ListItem>
                                                <asp:ListItem Value="302">call center status</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        <div id="div_CallTimes_2" runat="server">
                                            &nbsp;&nbsp;
                                            <asp:DropDownList ID="ddl_CallStatus_2" runat="server" Width="150px" 
                                                CssClass="ntextblackdata">
                                                <asp:ListItem Value="">- Select status -</asp:ListItem>
                                                <asp:ListItem Value="1">success</asp:ListItem>
                                                <asp:ListItem Value="0">fail</asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">Status</span>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        <div id="div_CallPeriod_2" runat="server">
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">- Select status period</span>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">From</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_CallFrom_2" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">To</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_CallTo_2" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                        </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <div>
                                    <asp:Button ID="btn_CallAddCondition_2" runat="server" CssClass="ntextblack" Text="Add" 
                                        Width="80px" onclick="btn_CallAddCondition_2_Click" />
                                    <asp:Button ID="btn_DelConditionCal_2" runat="server" CssClass="ntextblack" 
                                        Text="Remove" Width="80px" onclick="btnRemoveCondition_Click"/>
                                </div>
                                <div>
                                    <asp:DataGrid ID="dg_CallCondition_2" runat="server" AutoGenerateColumns="False" >
                                        <AlternatingItemStyle CssClass="gridalternativerowstyle" 
                                            HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkDeleteAllCal_2" OnCheckedChanged="ClearCheck" AutoPostBack="true" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkDelete" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="Type" HeaderText="Type">
                                                <HeaderStyle Width="250px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Status" HeaderText="Status">
                                                <HeaderStyle Width="30px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateFrom" HeaderText="Date From">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateTo" HeaderText="Date To">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="TypeIDDetail" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="visitFrequency" Visible="false"></asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="Gray" CssClass="gridheaderrowstyle" 
                                            HorizontalAlign="Center" />
                                        <ItemStyle CssClass="gridrowstyle" HorizontalAlign="Center" />
                                    </asp:DataGrid>
                                </div>
                            </td>
                        </tr>
                        
                        </table>
                    </div>
                    <div>
                        <table>
                        <tr>
                            <td colspan="3">
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                &nbsp;&nbsp;&nbsp;
                                Include Bounce
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:CheckBoxList ID="chk_IncludeBounce_2" runat="server" CssClass="ntextblack" 
                                    RepeatColumns="3" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="includeCallcenterBounce">Hardbounce Call Center</asp:ListItem>
                                    <asp:ListItem Value="includeEDMBounce">Hardbounce EDM</asp:ListItem>
                                    <asp:ListItem Value="includeSMSBounce">Hardbounce SMS</asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        </table>
                    </div>
                    </td>
                </tr>
            </table>
            </fieldset>
        </div>
        </div>
    </fieldset>
    <fieldset id="FieldSet11" runat="server">
        <legend id="Legend11" runat="server" class="ntextblack">Data Condition Set 3</legend>
        <table class="ntextblack">
            <tr>
                <td>
                    <div>
                        &nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="chk_Condition_3" runat="server" AutoPostBack="true"
                            oncheckedchanged="chk_Condition_CheckedChanged" />
                        <asp:LinkButton ID="btn_Condition_3" runat="server" Text="Click to Expand / Hide" 
                            onclick="btn_Condition_3_Click" ></asp:LinkButton>
                    </div>
                </td>
            </tr>
        </table>
        <div id="div_Condition_3" runat="server">
        <div>
            <fieldset id="FieldSet12" runat="server">
            <legend id="Legend12" runat="server" class="ntextblack">Primary Data</legend>
                <table width="90%">
                    <tr id="tr6" name="trEventName" runat="server">
                        <td align="right" style="width: 250px" class="ntextblack">
                            Platform
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td align="left">
                            <asp:CheckBoxList ID="chkList_Platform_3" runat="server" CssClass="ntextblack" 
                                RepeatColumns="3" RepeatDirection="Horizontal" AutoPostBack ="true" onselectedindexchanged="chkList_SelectedIndexChanged">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr id="tr7" runat="server">
                        <td align="right" style="width: 250px" class="ntextblack">
                            Message Sender
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txt_SenderName_3" runat="server" CssClass="ntextblackdata" Width="350px"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div>
            <fieldset id="FieldSet13" runat="server">
            <legend id="Legend13" runat="server" class="ntextblack">Customer Data</legend>
            <table width="100%" class="ntextblack">
                <tr>
                    <td class="style4">
                        <asp:Label ID="Label8" runat="server" Text="Demographic" 
                            style="text-decoration: underline"></asp:Label>
                    </td>
                    <td style="text-align: right">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    <table width="90%">
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Gender
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rbtn_ListGender_3" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="M">Male</asp:ListItem>
                                    <asp:ListItem Value="F">Female</asp:ListItem>
                                    <asp:ListItem Value="A">All</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Age
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <span class="ntextblack" style="vertical-align: middle">From</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="txt_AgeFrom_3" runat="server" CssClass="ntextblackdata" MaxLength="3" 
                                    OnKeyPress="validateNumKey()" Style="vertical-align: middle" Width="80px"></asp:TextBox>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="ntextblack" style="vertical-align: middle">To</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="txt_AgeTo_3" runat="server" CssClass="ntextblackdata" MaxLength="3" 
                                    OnKeyPress="validateNumKey()" Style="vertical-align: middle" Width="80px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Province
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <table>
                                    <tr>
                                        <td style="width:200px">
                                            <asp:Panel ID="Panel7" runat="server" 
                                                CssClass="scrollingControlContainer scrollingCheckBoxList" Width="100%">
                                            <asp:CheckBoxList ID="chk_Province_3" runat="server" CssClass="ntextblack" 
                                                RepeatDirection="Vertical" AutoPostBack ="true" onselectedindexchanged="chkList_SelectedIndexChanged">
                                            </asp:CheckBoxList>
                                            </asp:Panel>
                                        </td>
                                        <td style="width:400px">
                                            <asp:CheckBox ID="chk_IncludeUnknown_3" runat="server" Text="Include Unknown" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Birthday Range
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <span class="ntextblack" style="vertical-align:middle">From</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="dtp_BirthDateFrom_3" runat="server" Width="100px" 
                                    CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="ntextblack" style="vertical-align:middle">To</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="dtp_BirthDateTo_3" runat="server" Width="100px" 
                                    CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Outlet
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:Panel ID="Panel8" runat="server" 
                                    CssClass="scrollingControlContainer scrollingCheckBoxList" Width="40%">
                                <asp:CheckBoxList ID="chk_Outlet_3" runat="server" CssClass="ntextblack" 
                                    RepeatDirection="Vertical" AutoPostBack ="true" onselectedindexchanged="chkList_SelectedIndexChanged">
                                </asp:CheckBoxList>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Brand Preference
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:Panel ID="Panel9" runat="server" 
                                    CssClass="scrollingControlContainer scrollingCheckBoxList" Width="40%">
                                <asp:CheckBoxList ID="chk_Brand_3" runat="server" CssClass="ntextblack" 
                                    RepeatDirection="Vertical" AutoPostBack ="true" onselectedindexchanged="chkList_SelectedIndexChanged">
                                    <asp:ListItem>All</asp:ListItem>
                                    <asp:ListItem Value="brandPreJwGold">Johnnie Walker Gold Label</asp:ListItem>
                                    <asp:ListItem Value="brandPreJwBlack">Johnnie Walker Black Label</asp:ListItem>
                                    <asp:ListItem Value="brandPreJwRed">Johnnie Walker Red Label</asp:ListItem>
                                    <asp:ListItem Value="brandPreSmirnoff">Smirnoff</asp:ListItem>
                                    <asp:ListItem Value="brandPreBenmore">Benmore</asp:ListItem>
                                    <asp:ListItem Value="brandPreHennessy">Hennessy</asp:ListItem>
                                    <asp:ListItem Value="brandPre100Pipers">100Pipers</asp:ListItem>
                                    <asp:ListItem Value="brandPreAbsolut">Absolut</asp:ListItem>
                                    <asp:ListItem Value="brandPreBallentine">Ballentine</asp:ListItem>
                                    <asp:ListItem Value="brandPreChivas">Chivas regal</asp:ListItem>
                                    <asp:ListItem Value="brandPreBlend">Blend285</asp:ListItem>
                                </asp:CheckBoxList>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Active Period
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <span class="ntextblack" style="vertical-align:middle">From</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="dtp_ActPeriodFrom_3" runat="server" Width="100px" 
                                    CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <span class="ntextblack" style="vertical-align:middle">To</span>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="dtp_ActPeriodTo_3" runat="server" Width="100px" 
                                    CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Opt-in
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:CheckBoxList ID="chk_OptIn_3" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="validOptInDiageo">Diageo Opt-in</asp:ListItem>
                                    <asp:ListItem Value="validOptInOutlet">Outlet Opt-in</asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                4A's
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:CheckBoxList ID="chk_4A_3" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="Adorer">Adorer</asp:ListItem>
                                    <asp:ListItem Value="Adopter">Adopter</asp:ListItem>
                                    <asp:ListItem Value="Acceptor">Acceptor</asp:ListItem>
                                    <asp:ListItem Value="Available">Available</asp:ListItem>
                                    <asp:ListItem Value="Rejecter">Rejecter</asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                    </td>
                </tr>
            </table>
            </fieldset>
        </div>
        <div>
            <fieldset id="FieldSet14" runat="server">
            <legend id="Legend14" runat="server" class="ntextblack">Communication Data</legend>
            <table width="100%" class="ntextblack">
                <tr>
                    <td>
                    <div>
                        &nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="chk_EDM_3" runat="server" />
                        <asp:Label ID="Label9" runat="server" Text="EDM" 
                            style="text-decoration: underline"></asp:Label>
                    </div>
                    <div>
                        <table width="90%">
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Email Active
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rdbtn_EDMValid_3" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="T">Valid</asp:ListItem>
                                    <asp:ListItem Value="F">Invalid</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Receiving Status
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 200px">
                                            <asp:DropDownList ID="ddl_EDM_3" runat="server" Width="250px" AutoPostBack="true"
                                                CssClass="ntextblackdata" onselectedindexchanged="ddlEDM_SelectedIndexChanged">
                                                <asp:ListItem Value="0">- Select status -</asp:ListItem>
                                                <asp:ListItem Value="101">was in (any of/all of) campaign or etc.</asp:ListItem>
                                                <asp:ListItem Value="102">has opened any email</asp:ListItem>
                                                <asp:ListItem Value="103">has clicked a link in any email</asp:ListItem>
                                                <asp:ListItem Value="104">have never been received any email</asp:ListItem>
                                                <asp:ListItem Value="105">where the number of broadcast</asp:ListItem>
                                                <asp:ListItem Value="106">where the number of email opened/ link clicked</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        <div id="div_EDMTimes_3" runat="server">
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="txtEDMTimes_3" OnKeyPress="validateNumKey()" MaxLength="2" runat="server" CssClass="ntextblackdata" Style="vertical-align: middle" Width="90px"></asp:TextBox>
                                            &nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">Times</span>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        <div id="div_EDMPeriod_3" runat="server">
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">- Select status period</span>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">From</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_EDMFrom_3" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">To</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_EDMTo_3" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                        </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <div>
                                    <asp:Button ID="btn_EDMAddCondition_3" runat="server" CssClass="ntextblack" Text="Add" 
                                        Width="80px" onclick="btn_EDMAddCondition_3_Click"  />
                                    <asp:Button ID="btn_DelConditionEDM_3" runat="server" CssClass="ntextblack" 
                                        Text="Remove" Width="80px" onclick="btnRemoveCondition_Click" />
                                </div>
                                <div>
                                    <asp:DataGrid ID="dg_EDMCondition_3" runat="server" AutoGenerateColumns="False" >
                                        <AlternatingItemStyle CssClass="gridalternativerowstyle" 
                                            HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkDeleteAllEDM_3" OnCheckedChanged="ClearCheck" AutoPostBack="true" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkDelete" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="Type" HeaderText="Type">
                                                <HeaderStyle Width="250px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="visitFrequency" HeaderText="Times">
                                                <HeaderStyle Width="30px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateFrom" HeaderText="Date From">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateTo" HeaderText="Date To">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="TypeIDDetail" Visible="false"></asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="Gray" CssClass="gridheaderrowstyle" 
                                            HorizontalAlign="Center" />
                                        <ItemStyle CssClass="gridrowstyle" HorizontalAlign="Center" />
                                    </asp:DataGrid>
                                </div>
                            </td>
                        </tr>
                        </table>
                    </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    <div>
                        &nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="chk_SMS_3" runat="server" />
                        <asp:Label ID="Label10" runat="server" Text="SMS" 
                            style="text-decoration: underline"></asp:Label>
                    </div>
                    <div>
                        <table width="90%">
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Mobile No Active
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rdbtn_SMSValid_3" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="T">Valid</asp:ListItem>
                                    <asp:ListItem Value="F">Invalid</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Receiving Status
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 200px">
                                            <asp:DropDownList ID="ddl_SMS_3" runat="server" Width="250px" 
                                                CssClass="ntextblackdata" AutoPostBack="true"
                                                onselectedindexchanged="ddl_SMSRecsts_SelectedIndexChanged">
                                                <asp:ListItem Value="0">- Select status -</asp:ListItem>
                                                <asp:ListItem Value="201">was in (any of/all of) campaign or etc.</asp:ListItem>
                                                <asp:ListItem Value="202">have never been received any SMS</asp:ListItem>
                                                <asp:ListItem Value="203">where the number of broadcast</asp:ListItem>
                                                <asp:ListItem Value="204">where the number of SMS received</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        <div id="div_SMSTimes_3" runat="server">
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="txtSMSTimes_3" OnKeyPress="validateNumKey()" MaxLength="2" runat="server" CssClass="ntextblackdata" Style="vertical-align: middle" Width="90px"></asp:TextBox>
                                            &nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">Times</span>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        <div id="div_SMSPeriod_3" runat="server">
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">- Select status period</span>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">From</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_SMSFrom_3" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">To</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_SMSTo_3" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                        </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <div>
                                    <asp:Button ID="btn_SMSAddCondition_3" runat="server" CssClass="ntextblack" Text="Add" 
                                        Width="80px" onclick="btn_SMSAddCondition_3_Click" />
                                    <asp:Button ID="btn_DelConditionSMS_3" runat="server" CssClass="ntextblack" 
                                        Text="Remove" Width="80px" onclick="btnRemoveCondition_Click"/>
                                </div>
                                <div>
                                    <asp:DataGrid ID="dg_SMSCondition_3" runat="server" AutoGenerateColumns="False" >
                                        <AlternatingItemStyle CssClass="gridalternativerowstyle" 
                                            HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkDeleteAllSMS_3" OnCheckedChanged="ClearCheck" AutoPostBack="true" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkDelete" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="Type" HeaderText="Type">
                                                <HeaderStyle Width="250px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="visitFrequency" HeaderText="Times">
                                                <HeaderStyle Width="30px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateFrom" HeaderText="Date From">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateTo" HeaderText="Date To">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="TypeIDDetail" Visible="false"></asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="Gray" CssClass="gridheaderrowstyle" 
                                            HorizontalAlign="Center" />
                                        <ItemStyle CssClass="gridrowstyle" HorizontalAlign="Center" />
                                    </asp:DataGrid>
                                </div>
                            </td>
                        </tr>
                        </table>
                    </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    <div>
                        &nbsp;&nbsp;&nbsp;
                        <asp:CheckBox ID="chk_Call_3" runat="server" />
                        <asp:Label ID="Label4xx" runat="server" Text="Call Center" 
                            style="text-decoration: underline"></asp:Label>
                    </div>
                    <div>
                        <table width="90%">
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Contact Number Active
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rdbtn_CallCenterValid_3" runat="server" 
                                    CssClass="ntextblack" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="T">Valid</asp:ListItem>
                                    <asp:ListItem Value="F">Invalid</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                Receiving Status
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                
                                <table width="100%">
                                    <tr>
                                        <td style="width: 200px">
                                            <asp:DropDownList ID="ddl_Call_3" runat="server" Width="250px" 
                                                CssClass="ntextblackdata" AutoPostBack="true"
                                                onselectedindexchanged="ddl_CallRecsts_SelectedIndexChanged">
                                                <asp:ListItem Value="0">- Select status -</asp:ListItem>
                                                <asp:ListItem Value="301">was in (any of/all of) campaign or etc.</asp:ListItem>
                                                <asp:ListItem Value="302">call center status</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                        <div id="div_CallTimes_3" runat="server">
                                            &nbsp;&nbsp;
                                            <asp:DropDownList ID="ddl_CallStatus_3" runat="server" Width="150px" 
                                                CssClass="ntextblackdata">
                                                <asp:ListItem Value="">- Select status -</asp:ListItem>
                                                <asp:ListItem Value="1">success</asp:ListItem>
                                                <asp:ListItem Value="0">fail</asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">Status</span>
                                        </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                        <div id="div_CallPeriod_3" runat="server">
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">- Select status period</span>
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">From</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_CallFrom_3" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span class="ntextblack" style="vertical-align:middle">To</span>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="dtp_CallTo_3" runat="server" Width="100px" 
                                                CssClass="ntextblackdata" Style="vertical-align: middle" MaxLength="10"></asp:TextBox>
                                        </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <div>
                                    <asp:Button ID="btn_CallAddCondition_3" runat="server" CssClass="ntextblack" Text="Add" 
                                        Width="80px" onclick="btn_CallAddCondition_3_Click" />
                                    <asp:Button ID="btn_DelConditionCal_3" runat="server" CssClass="ntextblack" 
                                        Text="Remove" Width="80px" onclick="btnRemoveCondition_Click"/>
                                </div>
                                <div>
                                    <asp:DataGrid ID="dg_CallCondition_3" runat="server" AutoGenerateColumns="False" >
                                        <AlternatingItemStyle CssClass="gridalternativerowstyle" 
                                            HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkDeleteAllCal_3" OnCheckedChanged="ClearCheck" AutoPostBack="true" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkDelete" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="Type" HeaderText="Type">
                                                <HeaderStyle Width="250px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Status" HeaderText="Status">
                                                <HeaderStyle Width="30px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateFrom" HeaderText="Date From">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DateTo" HeaderText="Date To">
                                                <HeaderStyle Width="150px" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="TypeIDDetail" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="visitFrequency" Visible="false"></asp:BoundColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="Gray" CssClass="gridheaderrowstyle" 
                                            HorizontalAlign="Center" />
                                        <ItemStyle CssClass="gridrowstyle" HorizontalAlign="Center" />
                                    </asp:DataGrid>
                                </div>
                            </td>
                        </tr>
                        </table>
                    </div>
                    <div>
                        <table>
                        <tr>
                            <td colspan="3">
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 250px" class="ntextblack">
                                &nbsp;&nbsp;&nbsp;
                                Include Bounce
                            </td>
                            <td style="width: 20px">
                            </td>
                            <td align="left">
                                <asp:CheckBoxList ID="chk_IncludeBounce_3" runat="server" CssClass="ntextblack" 
                                    RepeatColumns="3" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="includeCallcenterBounce">Hardbounce Call Center</asp:ListItem>
                                    <asp:ListItem Value="includeEDMBounce">Hardbounce EDM</asp:ListItem>
                                    <asp:ListItem Value="includeSMSBounce">Hardbounce SMS</asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        </table>
                    </div>
                    </td>
                </tr>
            </table>
            </fieldset>
        </div>
        </div>
    </fieldset>
    
    <div id="div_VIP" runat="server">
        <fieldset id="FieldSet3" runat="server">
        <legend id="Legend3" runat="server" class="ntextblack">Add VIP Recipient</legend>
        <table width="90%" class="ntextblack">
            <tr>
                <td style="width: 40px">
                </td>
                <td align="left">
                    <asp:CheckBox ID="chk_IncludeDiageoVIP" runat="server" Text="Include Diageo VIP" />
                </td>
            </tr>
            <tr>
                <td style="width: 40px">
                </td>
                <td align="left">
                    <asp:CheckBox ID="chk_IncludeOutletVIP" runat="server" Text="Include Outlet VIP" />
                </td>
            </tr>
            <tr>
                <td style="width: 40px">
                </td>
                <td>
                    <asp:Panel ID="Panel3" runat="server" 
                        CssClass="scrollingControlContainer scrollingCheckBoxList" Width="40%">
                    <asp:CheckBoxList ID="chk_OutletVIP" runat="server" CssClass="ntextblack" 
                        RepeatDirection="Vertical" AutoPostBack ="false" onselectedindexchanged="chkList_SelectedIndexChanged">
                    </asp:CheckBoxList>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td style="width: 40px">
                </td>
                <td align="left">
                    <asp:CheckBox ID="chk_ExcludeOtherOutletVIP" runat="server" Text="Exclude Other Outlet VIP" />
                </td>
            </tr>
            <tr>
                <td style="width: 40px">
                </td>
                <td align="left">
                    <asp:CheckBox ID="chk_excludeBlockList" runat="server" Text="Exclude Block list" />
                </td>
            </tr>
        </table>
        </fieldset>
    </div>
    <div id="div_Criteria" runat="server">
        <fieldset id="FieldSet4" runat="server">
        <legend id="Legend4" runat="server" class="ntextblack">Set Criteria (Customize Result)</legend>
            <table width="90%">
                    <tr>
                        <td align="right" style="width: 250px" class="ntextblack">
                            Customer Record Priority
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td align="left">
                            <asp:RadioButtonList ID="rdbtn_RecordPriority" runat="server" 
                                CssClass="ntextblack" RepeatDirection="Horizontal" Width="450px">
                                <asp:ListItem Value="F" Selected="True">Frequently Priority</asp:ListItem>
                                <asp:ListItem Value="R">Recently Priority</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 250px" class="ntextblack">
                            Customer Max Repeat
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddl_CustomerMaxRepeat" runat="server" Width="80px" CssClass="ntextblackdata">
                                <asp:ListItem Value="0">- Qty -</asp:ListItem>
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 250px" class="ntextblack">
                        </td>
                        <td style="width: 20px">
                        </td>
                        <td align="right">
                            <asp:Button ID="btnReset" CssClass="ntab" runat="server" Text="Reset" 
                                onclick="btnReset_Click1" />&nbsp;
                            <asp:Button ID="btnCount" CssClass="ntab" runat="server" Text="Count" onclick="btnCount_Click" />&nbsp;
                            <asp:Button ID="btnExtractData" CssClass="ntab" runat="server" Text="Extract Data" onclick="btnExtractData_Click" />
                            
                        </td>
                    </tr>
                </table>
        </fieldset>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
