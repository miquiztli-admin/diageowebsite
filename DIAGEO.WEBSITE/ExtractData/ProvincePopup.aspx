﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProvincePopup.aspx.cs" Inherits="ExtractData_ProvincePopup" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Province</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="FrmProvince" runat="server">   
    <asp:ToolkitScriptManager ID="scriptManager" EnablePartialRendering="true" EnableScriptGlobalization="true" EnableScriptLocalization="true" runat="server">
    </asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="upHome" runat="server">
    <ContentTemplate>  
    <table align="center">
        <tr><td align="right">
             <asp:Button ID="btnSelect" runat="server" Text="Select" CssClass="ntextblack"
                 onclick="btnSelect_Click" />
        </td></tr>
        <tr><td align="center">
            <asp:DataGrid ID="dgProvince" runat="server" Width="900px" HeaderStyle-CssClass="gridheaderrowstyle"
                AutoGenerateColumns="false" ItemStyle-CssClass="gridrowstyle" 
                AlternatingItemStyle-CssClass="gridalternativerowstyle">
                <Columns>
                    <asp:TemplateColumn>
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkHeaderProvince" runat="server" AutoPostBack="true" OnCheckedChanged="chkHeaderProvince_OnCheckedChanged" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkProvince" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="name" HeaderText="Province Name"></asp:BoundColumn>
                    <asp:BoundColumn DataField="provinceId" Visible="false"></asp:BoundColumn>
                </Columns>            
            </asp:DataGrid>
        </td></tr>        
    </table>
     </ContentTemplate><Triggers></Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>