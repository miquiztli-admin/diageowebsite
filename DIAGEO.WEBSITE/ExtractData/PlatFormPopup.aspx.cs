﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class ExtractData_PlatFormPopup : BasePage
{
    #region Property

    public ArrayList ArrPlatformID
    {
        get { return (ArrayList)Session["ArrPlatformID" + TabNumber.ToString()]; }
        set { Session["ArrPlatformID" + TabNumber.ToString()] = value; }
    }

    public ArrayList ArrPlatformName
    {
        get { return (ArrayList)Session["ArrPlatformName" + TabNumber.ToString()]; }
        set { Session["ArrPlatformName" + TabNumber.ToString()] = value; }
    }

    public int TabNumber
    {
        get { return (int)ViewState["TabNumber"]; }
        set { ViewState["TabNumber"] = value; }
    }

    #endregion

    #region Method

    private void BindPlatform()
    {
        PlatformBLL bllPlatform = new PlatformBLL();
        DataTable dtPlatform = new DataTable();
        try
        {
            dtPlatform = bllPlatform.SelectActivePlatformOrderByName("T");
            dgPlatform.DataSource = dtPlatform;
            dgPlatform.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("BindPlatform :: " + ex.Message);
        }
    }

    private void KeepItemPlatform()
    {
        try
        {
            ArrPlatformID = new ArrayList();
            ArrPlatformName = new ArrayList();
            for (int i = 0; i < dgPlatform.Items.Count; i++)
            {
                CheckBox chk = (CheckBox)dgPlatform.Items[i].FindControl("chkPlatform");
                if (chk.Checked == true)
                {
                    ArrPlatformID.Add(dgPlatform.Items[i].Cells[2].Text.ToString());
                    ArrPlatformName.Add(dgPlatform.Items[i].Cells[1].Text.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("KeepItemPlatform :: " + ex.Message);
        }
    }

    private void BindItemPlatform()
    {
        try
        {
            if (ArrPlatformID == null)
                ArrPlatformID = new ArrayList();
            if (ArrPlatformID.Count > 0)
            {
                for (int i = 0; i < ArrPlatformID.Count; i++)
                {
                    for (int j = 0; j < dgPlatform.Items.Count; j++)
                    {
                        if (ArrPlatformID[i].ToString().Equals(dgPlatform.Items[j].Cells[2].Text.ToString().Replace("&nbsp;", "")))
                        {
                            CheckBox chk = (CheckBox)dgPlatform.Items[j].FindControl("chkPlatform");
                            chk.Checked = true;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindItemPlatform :: " + ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                TabNumber = int.Parse(Request.QueryString["tabNumber"].ToString());

                BindPlatform();
                BindItemPlatform();
            }
        }
        catch (Exception ex)
        {            
            throw new Exception(ex.Message);
        }
    }

    protected void btnSelect_Click(object sender, EventArgs e)
    {
        //string platform = "";
        //CheckBox chkPlatform;
        //ExtractData extractData;
        //DataTable dtPlatForm;
        //DataRow drPlatForm;

        try
        {
            KeepItemPlatform();
            //extractData = new ExtractData();
            //dtPlatForm = new DataTable();
            //dtPlatForm.Columns.Add(new DataColumn("Name"));
            //dtPlatForm.Columns.Add(new DataColumn("Value"));

            //for (int i = 0; i <= gvPlatform.Rows.Count - 1; i++)
            //{
            //    chkPlatform = gvPlatform.Rows[i].FindControl("chkPlatform") as CheckBox;
            //    if (chkPlatform.Checked == true)
            //    {
            //        platform += gvPlatform.DataKeys[i][0].ToString() + ",";
            //        drPlatForm = dtPlatForm.NewRow();
            //        drPlatForm[0] = gvPlatform.Rows[i].Cells[1].Text;
            //        drPlatForm[1] = gvPlatform.DataKeys[i][0].ToString();
            //        dtPlatForm.Rows.Add(drPlatForm);
            //    }		          
            //}

            //if (Request.QueryString["ComeFrom"].ToString() == "PlatForm")
            //    extractData.Platform = dtPlatForm;
            //else
            //    extractData.PlatformCallCenter = dtPlatForm;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "closePopup", "window.close();", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #endregion
}
