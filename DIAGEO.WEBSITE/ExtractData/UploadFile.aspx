﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UploadFile.aspx.cs" Inherits="ExtractData_UploadFile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function ValidateUpload() {
            if (document.getElementById('FileUpload').value == '') {
                alert('คุณยังไม่ได้เลือกไฟล์');
                return false;
            }
            else
                return true;
        }
        function ClearBrowse() {
            document.getElementById('FileUpload').value = '';
            return true;
        }
        function DialogConfirm() {
            if (document.getElementById('FileUpload').value == '') 
            {
                //alert('คุณยังไม่ได้เลือกไฟล์');
                //return false;
            }

            if (confirm('คุญต้องการอัพโหลดข้อมูลไฟล์นี้ ใช่รึไม่?'))
                return true;
            else
                return false;
        }
    </script>
</head>
<body>
    <form id="UploadFile" runat="server">
    <div>
        <fieldset id="FieldSet" runat="server">
        <legend id="HeaderPage" runat="server" class="ntextblack">Upload</legend>
        <br />
        <table align="center" style="vertical-align:middle">
            <tr>
                <td align="center">
                    <asp:Label ID="lblUpload" runat="server" Text="อัพโหลดไฟล์ : " CssClass="ntextblack"></asp:Label>&nbsp;
                    <asp:FileUpload ID="FileUpload" runat="server" />
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td align="center">
                <br />
                    <asp:Label ID="lblCatchMessage" runat="server"></asp:Label>
                    <asp:Button ID="btnView" runat="server" Text="View File" 
                        CssClass="ntextblack" Width="120px" OnClientClick="return ValidateUpload();" 
                        onclick="btnView_Click" />&nbsp;
                    <asp:Button ID="btnClear" runat="server" Text="Clear" 
                        CssClass="ntextblack" Width="120px" OnClientClick="return ClearBrowse();" 
                        onclick="btnClear_Click"/>
                </td>
            </tr>
        </table>
        <br />
        <asp:Panel ID="pnDataGrid" runat="server" HorizontalAlign="Center" ScrollBars="Auto" Visible="false">
            <table align="center" style="vertical-align:middle">
                <tr>
                    <td align="right">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblTextRowCount" runat="server" Text="จำนวนข้อมูลทั้งหมด : " CssClass="ntextblack"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Label ID="lblRowCount" runat="server" CssClass="ntextblack"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:DataGrid ID="dgResultData" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                            HeaderStyle-CssClass="gridheaderrowstyle" ItemStyle-CssClass="gridrowstyle" 
                            AlternatingItemStyle-CssClass="gridalternativerowstyle" 
                            onpageindexchanged="dgResultData_PageIndexChanged" 
                            onitemdatabound="dgResultData_ItemDataBound">
                            <Columns>
                                <asp:BoundColumn DataField="No" HeaderText="NO"></asp:BoundColumn>
                                <asp:BoundColumn DataField="OUTBOUND" HeaderText="OUTBOUND"></asp:BoundColumn>
                                <asp:BoundColumn DataField="MOBILE" HeaderText="MOBILE"></asp:BoundColumn>
                                <asp:BoundColumn DataField="CALLDATE" HeaderText="CALLDATE"></asp:BoundColumn>
                                <asp:BoundColumn DataField="DATEOFBIRTH" HeaderText="DATEOFBIRTH"></asp:BoundColumn>
                                <asp:BoundColumn DataField="OUTLET" HeaderText="OUTLET"></asp:BoundColumn>
                                <asp:BoundColumn DataField="FIRSTNAME" HeaderText="FIRSTNAME"></asp:BoundColumn>
                                <asp:BoundColumn DataField="LASTNAME" HeaderText="LASTNAME"></asp:BoundColumn>
                                <asp:BoundColumn DataField="EMAIL" HeaderText="EMAIL"></asp:BoundColumn>
                                <asp:BoundColumn DataField="RESULTID" HeaderText="RESULTID"></asp:BoundColumn>
                                <asp:BoundColumn DataField="RESULTSUBID" HeaderText="RESULTSUBID"></asp:BoundColumn>
                                <asp:BoundColumn DataField="RESULTSTATUS" HeaderText="RESULTSTATUS"></asp:BoundColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <table align="center">
            <tr>
                <td>
                    <asp:Button ID="btnUpload" runat="server" Text="Upload File" CssClass="ntextblack" 
                        Width="120px" Visible="false" OnClientClick="return DialogConfirm();" onclick="btnUpload_Click" />
                </td>
            </tr>
        </table>
        </fieldset>
    </div>
    </form>
</body>
</html>