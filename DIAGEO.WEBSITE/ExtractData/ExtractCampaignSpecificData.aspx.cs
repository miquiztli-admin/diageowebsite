﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data;
using System.Collections;
using System.Globalization;
using DIAGEO.BLL;

public partial class ExtractData_ExtractCampaignSpecificData : BasePage
{
    public int TabNumber
    {
        get { return (int)ViewState["TabNumber"]; }
        set { ViewState["TabNumber"] = value; }
    }
    public DataTable dtEDMCondition
    {
        get { return (DataTable)Session["dtEDMCondition" + TabNumber]; }
        set { Session["dtEDMCondition" + TabNumber] = value; }
    }
    public DataTable dtSMSCondition
    {
        get { return (DataTable)Session["dtSMSCondition" + TabNumber]; }
        set { Session["dtSMSCondition" + TabNumber] = value; }
    }
    public DataTable dtCallCondition
    {
        get { return (DataTable)Session["dtCallCondition" + TabNumber]; }
        set { Session["dtCallCondition" + TabNumber] = value; }
    }
    public int EventID
    {
        get { return (int)ViewState["EventID"]; }
        set { ViewState["EventID"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "dtp_EventDate", "Javascript:geteventdtpcontrol();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "dtp_BirthDate", "Javascript:getdatecontrol('BirthDate');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "dtp_ActPeriod", "Javascript:getdatecontrol('ActPeriod');", true);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "dtp_EDM", "Javascript:getdatecontrol('EDM');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "dtp_SMS", "Javascript:getdatecontrol('SMS');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "dtp_Call", "Javascript:getdatecontrol('Call');", true);

            if (!IsPostBack)
            {
                if (Request.QueryString["EventID"] != null)
                {
                    EventID = int.Parse(Request.QueryString["EventID"].ToString());
                }
                else
                {
                    EventID = 0;
                }

                BindControl();
                btn_Condition_1.Enabled = false;
                btn_Condition_2.Enabled = false;
                btn_Condition_3.Enabled = false;
                div_Condition_1.Visible = false;
                div_Condition_2.Visible = false;
                div_Condition_3.Visible = false;

                TabNumber = 0;
                if (EventID > 0)
                {
                    BindData();
                }

                //Session["dtEDMCondition" + 1] = null;
                //BindDataGrid(dg_EDMCondition_1,dtEDMCondition);
                //BindDataGrid(dg_SMSCondition_1,dtSMSCondition);
                //BindDataGrid(dg_CallCondition_1, dtCallCondition);
                HideCommunicationDiv();
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Page_Load :: " + ex.Message);
        }
    }

    private void BindControl()
    {
        OutletBLL bllOutlet = new OutletBLL();
        DataTable dtOutlet = new DataTable();

        try
        {
            dtOutlet = bllOutlet.SelectActiveOutletOrderByName("T");
            BindOutlet(chk_Outlet_1, dtOutlet);
            BindOutlet(chk_Outlet_2, dtOutlet);
            BindOutlet(chk_Outlet_3, dtOutlet);
            BindOutlet(chk_OutletVIP, dtOutlet);
            BindProvince();
            BindPlatform();
        }
        catch (Exception ex)
        {
            throw new Exception("BindControl :: " + ex.Message);
        }
    }

    private void BindOutlet(CheckBoxList chkOutlet, DataTable dt)
    {
        try
        {
            chkOutlet.DataTextField = "name";
            chkOutlet.DataValueField = "outletId";
            chkOutlet.DataSource = dt;
            chkOutlet.DataBind();

            chkOutlet.Items.Insert(0, new ListItem("All Outlet", "0"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindOutlet :: " + ex.Message);
        }
    }

    private void BindProvince()
    {
        ProvinceBLL bllProvince = new ProvinceBLL();
        DataTable dtProvince = new DataTable("Province");
        //DataRow dr;
        try
        {
            dtProvince = bllProvince.SelectAllAndOrderByName();

            chk_Province_1.DataTextField = "name";
            chk_Province_1.DataValueField = "provinceId";
            chk_Province_1.DataSource = dtProvince;
            chk_Province_1.DataBind();

            chk_Province_2.DataTextField = "name";
            chk_Province_2.DataValueField = "provinceId";
            chk_Province_2.DataSource = dtProvince;
            chk_Province_2.DataBind();

            chk_Province_3.DataTextField = "name";
            chk_Province_3.DataValueField = "provinceId";
            chk_Province_3.DataSource = dtProvince;
            chk_Province_3.DataBind();

            chk_Province_1.Items.Insert(0, new ListItem("All", "0"));
            chk_Province_2.Items.Insert(0, new ListItem("All", "0"));
            chk_Province_3.Items.Insert(0, new ListItem("All", "0"));

        }
        catch (Exception ex)
        {
            throw new Exception("BindProvince :: " + ex.Message);
        }
    }

    private void BindPlatform()
    {
        PlatformBLL bllPlatform = new PlatformBLL();
        DataTable dtPlatform = new DataTable();
        try
        {
            dtPlatform = bllPlatform.SelectActivePlatformOrderByName("T");

            chkList_Platform_1.DataTextField = "name";
            chkList_Platform_1.DataValueField = "platformId";
            chkList_Platform_1.DataSource = dtPlatform;
            chkList_Platform_1.DataBind();

            chkList_Platform_2.DataTextField = "name";
            chkList_Platform_2.DataValueField = "platformId";
            chkList_Platform_2.DataSource = dtPlatform;
            chkList_Platform_2.DataBind();

            chkList_Platform_3.DataTextField = "name";
            chkList_Platform_3.DataValueField = "platformId";
            chkList_Platform_3.DataSource = dtPlatform;
            chkList_Platform_3.DataBind();

            chkList_Platform_1.Items.Insert(0, new ListItem("All", "0"));
            chkList_Platform_2.Items.Insert(0, new ListItem("All", "0"));
            chkList_Platform_3.Items.Insert(0, new ListItem("All", "0"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindPlatform :: " + ex.Message);
        }
    }

    private void BindData()
    {
        OutBoundEventMasterBLL outBoundEventMasterBLL = new OutBoundEventMasterBLL();
        OutboundEventBLL outboundEventBLL = new OutboundEventBLL();
        
        DataTable dtoutBoundEventMaster, dtoutboundEvent;

        try
        {
            dtoutBoundEventMaster = outBoundEventMasterBLL.SelectAllByEventID(EventID);
            dtoutboundEvent = outboundEventBLL.SelectByEventID(EventID);

            DataRow DrEventMaster;
            DrEventMaster = dtoutBoundEventMaster.Rows[0];

            //Master Data Section
            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
            dateInfo.ShortDatePattern = "MM/dd/yyyy";
            dtp_EventDateFrom.Text = Convert.ToDateTime(DrEventMaster["eventDateFrom"].ToString(), dateInfo).ToString("dd/MM/yyyy");
            dtp_EventDateTo.Text = Convert.ToDateTime(DrEventMaster["eventDateTo"].ToString(), dateInfo).ToString("dd/MM/yyyy");
            txt_EventName.Text = DrEventMaster["eventName"].ToString();
            txt_EventID.Text = EventID.ToString();

            //Add VIP Recipient
            chk_IncludeDiageoVIP.Checked = (DrEventMaster["IncludeDiageoVIP"].ToString() == "T") ? true : false;
            chk_IncludeOutletVIP.Checked = (DrEventMaster["IncludeOutletVIP"].ToString() == "T") ? true : false;
            chk_ExcludeOtherOutletVIP.Checked = (DrEventMaster["ExcludeOtherOutletVIP"].ToString() == "T") ? true : false;
            chk_excludeBlockList.Checked = (DrEventMaster["excludeBlockList"].ToString() == "T") ? true : false;

            SetValueCheckBoxList(chk_OutletVIP, DrEventMaster["IncludeOutletVIPList"].ToString());

            //Set Criteria
            rdbtn_RecordPriority.SelectedValue = DrEventMaster["RecordPriority"].ToString();
            ddl_CustomerMaxRepeat.SelectedValue = DrEventMaster["CustomerMaxRepeat"].ToString();

            //BindDataOutboundEvent
            BindDataOutboundEvent(dtoutboundEvent);


             
        }
        catch (Exception ex)
        {
            throw new Exception("BindData :: " + ex.Message);
        }
    }

    private void BindDataOutboundEvent(DataTable dt)
    {
        string sNum = "";
        EventVisitConditionBLL eventVisitConditionBLL = new EventVisitConditionBLL();

        try
        {
            foreach (DataRow DrOutboundEvent in dt.Rows)
            {
                sNum = DrOutboundEvent["DataGroup"].ToString();

                //Primary Data
                CheckBoxList chkListPlatform = (CheckBoxList)Page.FindControl("chkList_Platform_" + sNum);
                TextBox txtMsgSender = (TextBox)Page.FindControl("txt_SenderName_" + sNum);

                SetValueCheckBoxList(chkListPlatform, DrOutboundEvent["PlatformIDList"].ToString());
                txtMsgSender.Text = DrOutboundEvent["senderName"].ToString();

                //Customer Data
                RadioButtonList rbtn_Gender = (RadioButtonList)Page.FindControl("rbtn_ListGender_" + sNum);
                TextBox txtAgeFrom = (TextBox)Page.FindControl("txt_AgeFrom_" + sNum);
                TextBox txtAgeTo = (TextBox)Page.FindControl("txt_AgeTo_" + sNum);

                TextBox txtBirthDateFrom = (TextBox)Page.FindControl("dtp_BirthDateFrom_" + sNum);
                TextBox txtBirthDateTo = (TextBox)Page.FindControl("dtp_BirthDateTo_" + sNum);

                CheckBoxList chkOutlet = (CheckBoxList)Page.FindControl("chk_Outlet_" + sNum);

                TextBox txtActPeriodFrom = (TextBox)Page.FindControl("dtp_ActPeriodFrom_" + sNum);
                TextBox txtActPeriodTo = (TextBox)Page.FindControl("dtp_ActPeriodTo_" + sNum);


                rbtn_Gender.SelectedValue = DrOutboundEvent["gender"].ToString();
                txtAgeFrom.Text = DrOutboundEvent["startAge"].ToString();
                txtAgeTo.Text = DrOutboundEvent["endAge"].ToString();

                SetDataProvinceList(DrOutboundEvent["ProvinceList"].ToString(), sNum);

                txtBirthDateFrom.Text = DrOutboundEvent["BDRangeFrom"].ToString();
                txtBirthDateTo.Text = DrOutboundEvent["BDRangeTo"].ToString();

                SetValueCheckBoxList(chkOutlet, DrOutboundEvent["OutletIDList"].ToString());

                SetDataBrandPreference(DrOutboundEvent, sNum);

                txtActPeriodFrom.Text = DrOutboundEvent["ActivePeriodDateFrom"].ToString();
                txtActPeriodTo.Text = DrOutboundEvent["ActivePeriodDateTo"].ToString();

                SetDataCheckBoxList(DrOutboundEvent, "chk_OptIn_", sNum);
                SetDataCheckBoxList(DrOutboundEvent, "chk_4A_", sNum);


                //Communication Data
                RadioButtonList rbtn_EDMValid = (RadioButtonList)Page.FindControl("rdbtn_EDMValid_" + sNum);
                RadioButtonList rbtn_SMSValid = (RadioButtonList)Page.FindControl("rdbtn_SMSValid_" + sNum);
                RadioButtonList rbtn_CallCenterValid = (RadioButtonList)Page.FindControl("rdbtn_CallCenterValid_" + sNum);

                DataGrid dg_EDM = (DataGrid)Page.FindControl("dg_EDMCondition_" + sNum);
                DataGrid dg_SMS = (DataGrid)Page.FindControl("dg_SMSCondition_" + sNum);
                DataGrid dg_Call = (DataGrid)Page.FindControl("dg_CallCondition_" + sNum);

                rbtn_EDMValid.SelectedValue = DrOutboundEvent["EDMValid"].ToString();
                rbtn_SMSValid.SelectedValue = DrOutboundEvent["SMSValid"].ToString();
                rbtn_CallCenterValid.SelectedValue = DrOutboundEvent["CallCenterValid"].ToString();

                SetDataCheckBoxList(DrOutboundEvent, "chk_IncludeBounce_", sNum);

                TabNumber = Convert.ToInt32(sNum);
                dtEDMCondition = eventVisitConditionBLL.SelectByEachTypeID(EventID, Convert.ToInt32(sNum), 1);
                if (dtEDMCondition.Rows.Count > 0)
                {
                    BindDataGrid(dg_EDM, dtEDMCondition);
                }
                dtSMSCondition = eventVisitConditionBLL.SelectByEachTypeID(EventID, Convert.ToInt32(sNum), 2);
                if (dtSMSCondition.Rows.Count > 0)
                {
                    BindDataGrid(dg_SMS, dtSMSCondition);
                }
                dtCallCondition = eventVisitConditionBLL.SelectByEachTypeID(EventID, Convert.ToInt32(sNum), 3);
                if (dtCallCondition.Rows.Count > 0)
                {
                    BindDataGrid(dg_Call, dtCallCondition);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindDataOutboundEvent :: " + ex.Message);
        }
    }

    private void SetDataBrandPreference(DataRow dr, string sNum)
    {

        try
        {
            CheckBoxList chkBrand = (CheckBoxList)Page.FindControl("chk_Brand_" + sNum);

            if (            
                dr["brandPreJwGold"].ToString() == "T" &&
                dr["brandPreJwBlack"].ToString() == "T" &&
                dr["brandPreSmirnoff"].ToString() == "T" &&
                dr["brandPreJwRed"].ToString() == "T" &&
                dr["brandPreBenmore"].ToString() == "T" &&
                dr["brandPreAbsolut"].ToString() == "T" &&
                dr["brandPreBallentine"].ToString() == "T" &&
                dr["brandPreBlend"].ToString() == "T" &&
                dr["brandPreChivas"].ToString() == "T" &&
                dr["brandPre100Pipers"].ToString() == "T" &&
                dr["brandPreHennessy"].ToString() == "T"
            )
            {
                chkBrand.Items[0].Selected = true;

                for (int i = 1; i < chkBrand.Items.Count; i++)
                {
                    chkBrand.Items[i].Enabled = false;
                    chkBrand.Items[i].Selected = false;
                }
            }
            else
            {
                for (int i = 1; i < chkBrand.Items.Count; i++)
                {
                    if (dr[chkBrand.Items[i].Value].ToString() == "T")
                    {
                        chkBrand.Items[i].Selected = true;
                    }
                    else
                    {
                        chkBrand.Items[i].Selected = false;
                    }
                }
            }

        }
        catch (Exception ex)
        {
            throw new Exception("SetDataBrandPreference :: " + ex.Message);
        }
    }

    private void SetDataProvinceList(string sProvinceList, string sNum)
    {
        try
        {
            CheckBoxList chkListProvince = (CheckBoxList)Page.FindControl("chk_Province_" + sNum);
            CheckBox chkIncludeUnknown = (CheckBox)Page.FindControl("chk_IncludeUnknown_" + sNum);

            string[] _s = sProvinceList.Split(new char[] { ',' });
            foreach (string x in _s)
            {
                if (x.Contains("''"))
                {
                    chkIncludeUnknown.Checked = true;
                }
            }

            string sProv = sProvinceList.Replace("'","");
            ArrayList values = StringToArrayList(sProv);

            if (values.Contains("Null"))
            {
                chkListProvince.Items[0].Selected = true;
                for (int i = 1; i < chkListProvince.Items.Count; i++)
                {
                    chkListProvince.Items[i].Enabled = false;
                    chkListProvince.Items[i].Selected = false;
                }
            }
            else
            {
                foreach (ListItem li in chkListProvince.Items)
                {
                    if (values.Contains(li.Text))
                        li.Selected = true;
                    else
                        li.Selected = false;
                }
            }


        }
        catch (Exception ex)
        {
            throw new Exception("SetDataProvinceList :: " + ex.Message);
        }
    }

    private void SetValueCheckBoxList(CheckBoxList cbl, string sValues)
    {
        if (!string.IsNullOrEmpty(sValues))
        {
            ArrayList values = StringToArrayList(sValues);

            if (values.Count == (cbl.Items.Count - 1))
            {
                cbl.Items[0].Selected = true;

                for (int i = 1; i < cbl.Items.Count; i++)
                {
                    cbl.Items[i].Enabled = false;
                    cbl.Items[i].Selected = false;
                }
            }
            else
            {
                foreach (ListItem li in cbl.Items)
                {
                    if (values.Contains(li.Value))
                        li.Selected = true;
                    else
                        li.Selected = false;
                }
            }
        }
    }

    private void SetValueProvinceCheckBoxList(CheckBoxList cbl, string sValues)
    {
        if (!string.IsNullOrEmpty(sValues))
        {
            ArrayList values = StringToArrayList(sValues);
            foreach (ListItem li in cbl.Items)
            {
                if (values.Contains(li.Text))
                    li.Selected = true;
                else
                    li.Selected = false;
            }
        }
    }

    private void SetDataCheckBoxList(DataRow dr, string sName, string sNum)
    {
        try
        {
            CheckBoxList chkList = (CheckBoxList)Page.FindControl(sName + sNum);

            for (int i = 1; i < chkList.Items.Count; i++)
            {
                if (dr[chkList.Items[i].Value] != null)
                {
                    if (dr[chkList.Items[i].Value].ToString() == "T")
                    {
                        chkList.Items[i].Selected = true;
                    }
                }
            }

        }
        catch (Exception ex)
        {
            throw new Exception("SetDataCheckBoxList :: " + ex.Message);
        }
    }

    private ArrayList StringToArrayList(string value)
    {
        ArrayList _al = new ArrayList();
        string[] _s = value.Split(new char[] { ',' });

        foreach (string item in _s)
            _al.Add(item);

        return _al;
    }

    protected void btnCount_Click(object sender, EventArgs e)
    {
        try
        {
            SaveData();


        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "btnCount_Click :: " + ex.Message);
        }
      
    }
    protected void btnExtractData_Click(object sender, EventArgs e)
    {

        try
        {
            SaveData();


        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "btnExtractData_Click :: " + ex.Message);
        }
    }

    private void SaveData()
    {
        string sMsg = "";
        int ieventId = 0;
        OutboundEventBLL bllOutboundEvent = new OutboundEventBLL();
        DataTable dtOutboundEventMaster = new DataTable("OutboundEventMaster");
        DataTable dtOutboundEvent = new DataTable("OutboundEvent");
        DataTable dtEventVisitCondition = new DataTable("EventVisitCondition");

        try
        {
            sMsg = ValidateData();
            if (sMsg == "")
            {

                dtOutboundEventMaster = GetOutboundEventMasterData();
                dtOutboundEvent = GetOutboundEvent(ref dtEventVisitCondition);
                bllOutboundEvent.SaveDataExtractCampaign(dtOutboundEventMaster, dtOutboundEvent, dtEventVisitCondition, ref ieventId);

                EventID = ieventId;
                txt_EventID.Text = ieventId.ToString();

            }
            else
            {
                JsClientAlert("ErrorMessage", "ValidateData :: " + sMsg);
            }


        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "btnExtractData_Click :: " + ex.Message);
        }

    }

    protected void ClearCheck(object sender, EventArgs e)
    {
        try
        {
            CheckBox chkAll = sender as CheckBox;
            string sID = chkAll.ID;
            string sNum = sID.Substring(sID.Length - 1, 1);
            string sType = sID.Substring(sID.Length - 5, 3);

            if (sType == "Cal")
            {
                sType = "Call";
            }
            DataGrid dg_ = (DataGrid)Page.FindControl("dg_" + sType + "Condition_" + sNum);

            foreach (DataGridItem item in dg_.Items)
            {
                CheckBox chkDel = item.FindControl("chkDelete") as CheckBox;
                if (chkAll.Checked == true)
                    chkDel.Checked = true;
                else
                    chkDel.Checked = false;
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ClearCheck :: " + ex.Message);
        }
    }

    private void HideCommunicationDiv()
    {
        div_EDMPeriod_1.Visible = false;
        div_EDMTimes_1.Visible = false;
        div_SMSPeriod_1.Visible = false;
        div_SMSTimes_1.Visible = false;
        div_CallPeriod_1.Visible = false;
        div_CallTimes_1.Visible = false;

        div_EDMPeriod_2.Visible = false;
        div_EDMTimes_2.Visible = false;
        div_SMSPeriod_2.Visible = false;
        div_SMSTimes_2.Visible = false;
        div_CallPeriod_2.Visible = false;
        div_CallTimes_2.Visible = false;

        div_EDMPeriod_3.Visible = false;
        div_EDMTimes_3.Visible = false;
        div_SMSPeriod_3.Visible = false;
        div_SMSTimes_3.Visible = false;
        div_CallPeriod_3.Visible = false;
        div_CallTimes_3.Visible = false;
    }

    private void BindDataGrid(DataGrid dgCondition, DataTable dt)
    {
        try
        {
            if (dt == null)
                dt = new DataTable();

            dgCondition.DataSource = dt;
            dgCondition.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("BindDataGrid :: " + ex.Message);
        }
    }
    protected void btnRemoveCondition_Click(object sender, EventArgs e)
    {
        try
        {
            Button btn = (Button)sender;
            string sbtnID = btn.ID;
            string sNum = sbtnID.Substring(sbtnID.Length - 1, 1);
            string sType = sbtnID.Substring(sbtnID.Length - 5, 3);

            if (sType == "Cal")
            {
                sType = "Call";
            }
            DataGrid dg_ = (DataGrid)Page.FindControl("dg_" + sType + "Condition_" + sNum);
            
            ArrayList arrDel = new ArrayList();
            int iRow = 0;
            DataTable dtCondition;
            TabNumber = Convert.ToInt32(sNum);
            switch (sType)
            {
                case "EDM":
                    dtCondition = dtEDMCondition;
                    break;
                case "SMS":
                    dtCondition = dtSMSCondition;
                    break;
                case"Call":
                    dtCondition = dtCallCondition;
                    break;
                default:
                    dtCondition = new DataTable();
                    break;
            }
            
            foreach (DataGridItem di in dg_.Items)
            {
                CheckBox chkBx = (CheckBox)di.FindControl("chkDelete");
                if (chkBx != null && chkBx.Checked)
                {
                    arrDel.Add(iRow.ToString());
                }
                iRow++;
            }

            if (dtCondition.Rows.Count > 0)
            {
                for (int i = arrDel.Count - 1; i >= 0; i--)
                {
                    dtCondition.Rows[int.Parse(arrDel[i].ToString())].Delete();
                }

                dg_.DataSource = dtCondition;
                dg_.DataBind();

                switch (sType)
                {
                    case "EDM":
                        dtEDMCondition = dtCondition;
                        break;
                    case "SMS":
                        dtSMSCondition = dtCondition;
                        break;
                    case "Call":
                        dtCallCondition = dtCondition;
                        break;
                }
            }

        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "btnRemoveCondition_Click :: " + ex.Message);
        }

    }

    protected void btnAddCondition_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddl_EDM_1.SelectedIndex != 0)
            {
                if (ValidateDataCondition(dtp_EDMFrom_1, dtp_EDMTo_1, txtEDMTimes_1,null))
                {
                    TabNumber = 1;
                    DataTable dt = dtEDMCondition;
                    AddDataToGridData(ddl_EDM_1, dtp_EDMFrom_1, dtp_EDMTo_1, txtEDMTimes_1, dg_EDMCondition_1, ref dt);
                    dtEDMCondition = dt;
                    ClearDataDIV("EDMTimes_1");
                    ClearDataDIV("EDMPeriod_1");
                    ddl_EDM_1.SelectedIndex = 0;
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "btnAddCondition_Click :: " + ex.Message);
        }
    }

    protected void btn_SMSAddCondition_1_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddl_SMS_1.SelectedIndex != 0)
            {
                if (ValidateDataCondition(dtp_SMSFrom_1, dtp_SMSTo_1, txtSMSTimes_1,null))
                {
                    TabNumber = 1;
                    DataTable dt = dtSMSCondition;
                    AddDataToGridData(ddl_SMS_1, dtp_SMSFrom_1, dtp_SMSTo_1, txtSMSTimes_1, dg_SMSCondition_1, ref dt);
                    dtSMSCondition = dt;
                    ClearDataDIV("SMSTimes_1");
                    ClearDataDIV("SMSPeriod_1");
                    ddl_SMS_1.SelectedIndex = 0;
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "btn_SMSAddCondition_1_Click :: " + ex.Message);
        }
    }

    protected void btn_CallAddCondition_1_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddl_Call_1.SelectedIndex != 0)
            {
                if (ValidateDataCondition(dtp_CallFrom_1, dtp_CallTo_1,null ,ddl_CallStatus_1))
                {
                    TabNumber = 1;
                    DataTable dt = dtCallCondition;
                    AddDataToGridData_Call(ddl_Call_1, dtp_CallFrom_1, dtp_CallTo_1, ddl_CallStatus_1, dg_CallCondition_1, ref dt);
                    dtCallCondition = dt;
                    ClearDataDIV("CallTimes_1");
                    ClearDataDIV("CallPeriod_1");
                    ddl_Call_1.SelectedIndex = 0;
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "btn_CallAddCondition_1_Click :: " + ex.Message);
        }
    }

    protected void btn_EDMAddCondition_2_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddl_EDM_2.SelectedIndex != 0)
            {
                if (ValidateDataCondition(dtp_EDMFrom_2, dtp_EDMTo_2, txtEDMTimes_2,null))
                {
                    TabNumber = 2;
                    DataTable dt = dtEDMCondition;
                    AddDataToGridData(ddl_EDM_2, dtp_EDMFrom_2, dtp_EDMTo_2, txtEDMTimes_2, dg_EDMCondition_2, ref dt);
                    dtEDMCondition = dt;
                    ClearDataDIV("EDMTimes_2");
                    ClearDataDIV("EDMPeriod_2");
                    ddl_EDM_2.SelectedIndex = 0;
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "btn_EDMAddCondition_2_Click :: " + ex.Message);
        }
    }
    protected void btn_SMSAddCondition_2_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddl_SMS_2.SelectedIndex != 0)
            {
                if (ValidateDataCondition(dtp_SMSFrom_2, dtp_SMSTo_2, txtSMSTimes_2,null))
                {
                    TabNumber = 2;
                    DataTable dt = dtSMSCondition;
                    AddDataToGridData(ddl_SMS_2, dtp_SMSFrom_2, dtp_SMSTo_2, txtSMSTimes_2, dg_SMSCondition_2, ref dt);
                    dtSMSCondition = dt;
                    ClearDataDIV("SMSTimes_2");
                    ClearDataDIV("SMSPeriod_2");
                    ddl_SMS_2.SelectedIndex = 0;
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "btn_SMSAddCondition_2_Click :: " + ex.Message);
        }
    }
    protected void btn_CallAddCondition_2_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddl_Call_2.SelectedIndex != 0)
            {
                if (ValidateDataCondition(dtp_SMSFrom_2, dtp_SMSTo_2, null, ddl_CallStatus_2))
                {
                    TabNumber = 2;
                    DataTable dt = dtCallCondition;
                    AddDataToGridData_Call(ddl_Call_2, dtp_CallFrom_2, dtp_CallTo_2, ddl_CallStatus_2, dg_CallCondition_2, ref dt);
                    dtCallCondition = dt;
                    ClearDataDIV("CallTimes_2");
                    ClearDataDIV("CallPeriod_2");
                    ddl_Call_2.SelectedIndex = 0;
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "btn_CallAddCondition_2_Click :: " + ex.Message);
        }
    }
    protected void btn_EDMAddCondition_3_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddl_EDM_3.SelectedIndex != 0)
            {
                if (ValidateDataCondition(dtp_EDMFrom_3, dtp_EDMTo_3, txtEDMTimes_3,null))
                {
                    TabNumber = 3;
                    DataTable dt = dtEDMCondition;
                    AddDataToGridData(ddl_EDM_3, dtp_EDMFrom_3, dtp_EDMTo_3, txtEDMTimes_3, dg_EDMCondition_3, ref dt);
                    dtEDMCondition = dt;
                    ClearDataDIV("EDMTimes_3");
                    ClearDataDIV("EDMPeriod_3");
                    ddl_EDM_3.SelectedIndex = 0;
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "btn_EDMAddCondition_3_Click :: " + ex.Message);
        }

    }
    protected void btn_SMSAddCondition_3_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddl_SMS_3.SelectedIndex != 0)
            {
                if (ValidateDataCondition(dtp_SMSFrom_3, dtp_SMSTo_3, txtSMSTimes_3,null))
                {
                    TabNumber = 3;
                    DataTable dt = dtSMSCondition;
                    AddDataToGridData(ddl_SMS_3, dtp_SMSFrom_3, dtp_SMSTo_3, txtSMSTimes_3, dg_SMSCondition_3, ref dt);
                    dtSMSCondition = dt;
                    ClearDataDIV("SMSTimes_3");
                    ClearDataDIV("SMSPeriod_3");
                    ddl_SMS_3.SelectedIndex = 0;
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "btn_SMSAddCondition_3_Click :: " + ex.Message);
        }
    }
    protected void btn_CallAddCondition_3_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddl_Call_3.SelectedIndex != 0)
            {
                if (ValidateDataCondition(dtp_SMSFrom_2, dtp_SMSTo_2, null, ddl_CallStatus_3))
                {
                    TabNumber = 3;
                    DataTable dt = dtCallCondition;
                    AddDataToGridData_Call(ddl_Call_3, dtp_CallFrom_3, dtp_CallTo_3, ddl_CallStatus_3, dg_CallCondition_3, ref dt);
                    dtCallCondition = dt;
                    ClearDataDIV("CallTimes_3");
                    ClearDataDIV("CallPeriod_3");
                    ddl_Call_3.SelectedIndex = 0;
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "btn_CallAddCondition_3_Click :: " + ex.Message);
        }
    }

    private void AddDataToGridData(DropDownList ddlType, TextBox dpDateFrom, TextBox dpDateTo,
                                    TextBox txtVisit, DataGrid dgCondition, ref DataTable dtCondition)
    {
        DataTable dt = new DataTable();
        DataRow dr;
        try
        {
            if (dtCondition == null)
                dtCondition = new DataTable();

            if (dtCondition.Columns.Count == 0)
            {
                dt.Columns.Add("Type");
                dt.Columns.Add("DateFrom");
                dt.Columns.Add("DateTo");
                dt.Columns.Add("visitFrequency");
                dt.Columns.Add("TypeIDDetail");
            }
            else
                dt = dtCondition;

            dr = dt.NewRow();
            dr["Type"] = ddlType.SelectedItem.Text.ToString();
            dr["DateFrom"] = (dpDateFrom.Text != "") ? dpDateFrom.Text.ToString() : "";
            dr["DateTo"] = (dpDateTo.Text != "") ? dpDateTo.Text.ToString() : "";
            dr["visitFrequency"] = (txtVisit.Text.Trim() != "") ? txtVisit.Text.Trim().ToString() : "";
            dr["TypeIDDetail"] = ddlType.SelectedValue.ToString();
            dt.Rows.Add(dr);

            dtCondition = dt;

            dgCondition.DataSource = dtCondition;
            dgCondition.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("AddDataToGridData :: " + ex.Message);
        }
    }

    private void AddDataToGridData_Call(DropDownList ddlType, TextBox dpDateFrom, TextBox dpDateTo,
                                    DropDownList ddlStatus, DataGrid dgCondition, ref DataTable dtCondition)
    {
        DataTable dt = new DataTable();
        DataRow dr;
        try
        {
            if (dtCondition == null)
                dtCondition = new DataTable();

            if (dtCondition.Columns.Count == 0)
            {
                dt.Columns.Add("Type");
                dt.Columns.Add("DateFrom");
                dt.Columns.Add("DateTo");
                dt.Columns.Add("Status");
                dt.Columns.Add("TypeIDDetail");
                dt.Columns.Add("visitFrequency");
            }
            else
                dt = dtCondition;

            dr = dt.NewRow();
            dr["Type"] = ddlType.SelectedItem.Text.ToString();
            dr["DateFrom"] = (dpDateFrom.Text != "") ? dpDateFrom.Text.ToString() : "";
            dr["DateTo"] = (dpDateTo.Text != "") ? dpDateTo.Text.ToString() : "";
            dr["Status"] = (ddlStatus.SelectedValue.ToString() != "") ? ddlStatus.SelectedItem.Text.ToString() : "";
            dr["TypeIDDetail"] = ddlType.SelectedValue.ToString();
            dr["visitFrequency"] = ddlStatus.SelectedValue.ToString();
            dt.Rows.Add(dr);

            dtCondition = dt;

            dgCondition.DataSource = dtCondition;
            dgCondition.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("AddDataToGridData :: " + ex.Message);
        }
    }

    protected void ddlEDM_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddl = (DropDownList)sender;
            string sddlID = ddl.ID;
            string sNum = sddlID.Substring(sddlID.Length - 2, 2);
            ClearDataDIV("EDMTimes" + sNum);
            ClearDataDIV("EDMPeriod" + sNum);
            EDMReceivingStsCondition(sddlID, "EDMPeriod" + sNum, "EDMTimes" + sNum);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtractData_ExtractCampaignSpecificData.ddlEDM_SelectedIndexChanged :: " + ex.Message);
        }
    }

    protected void ddl_SMSRecsts_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddl = (DropDownList)sender;
            string sddlID = ddl.ID;
            string sNum = sddlID.Substring(sddlID.Length - 2, 2);
            ClearDataDIV("SMSTimes" + sNum);
            ClearDataDIV("SMSPeriod" + sNum);
            SMSReceivingStsCondition(sddlID, "SMSPeriod" + sNum, "SMSTimes" + sNum);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtractData_ExtractCampaignSpecificData.ddl_SMS_SelectedIndexChanged :: " + ex.Message);
        }
    }

    protected void ddl_CallRecsts_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddl = (DropDownList)sender;
            string sddlID = ddl.ID;
            string sNum = sddlID.Substring(sddlID.Length - 2, 2);
            ClearDataDIV("CallTimes" + sNum);
            ClearDataDIV("CallPeriod" + sNum);
            CallReceivingStsCondition(sddlID, "CallPeriod" + sNum, "CallTimes" + sNum);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ExtractData_ExtractCampaignSpecificData.ddl_Call_SelectedIndexChanged :: " + ex.Message);
        }
    }

    private void ClearDataDIV(string sDiv)
    { 
        HtmlGenericControl div = (HtmlGenericControl) Page.FindControl("div_"+sDiv);
        foreach (Control cntrl in div.Controls)
        {
            if (cntrl is TextBox)
            {
                TextBox txtBox = (TextBox)div.FindControl(cntrl.ID);
                txtBox.Text = "";
            }

            //if (cntrl is DropDownList)
            //{
            //    DropDownList ddl = (DropDownList)div.FindControl(cntrl.ID);
            //    ddl.SelectedIndex = 0;
            //}

            //if (cntrl is CheckBoxList)
            //{
            //    CheckBoxList chk = (CheckBoxList)div.FindControl(cntrl.ID);
            //    foreach (ListItem li in chk.Items)
            //    {
            //        li.Selected = false;
            //    }
            //}
        }
    }

    private void EDMReceivingStsCondition(string sddl, string sPeriod, string sTimes)
    {
        try
        {

            DropDownList ddlReceivingSts = (DropDownList)Page.FindControl(sddl);
            HtmlGenericControl divPeriod = (HtmlGenericControl)Page.FindControl("div_" + sPeriod);
            HtmlGenericControl divTimes = (HtmlGenericControl)Page.FindControl("div_" + sTimes);

            switch (Convert.ToInt32(ddlReceivingSts.SelectedValue))
            {
                case 101:
                case 102:
                case 103:
                case 104:
                    divTimes.Visible = false;
                    divPeriod.Visible = true;
                    break;
                case 105:
                case 106:
                    divTimes.Visible = true;
                    divPeriod.Visible = true;
                    break;
                default:
                    divTimes.Visible = false;
                    divPeriod.Visible = false;
                    break;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("EDMReceivingStsCondition :: " + ex.Message);
        }
    }

    private void SMSReceivingStsCondition(string sddl, string sPeriod, string sTimes)
    {
        try
        {

            DropDownList ddlReceivingSts = (DropDownList)Page.FindControl(sddl);
            HtmlGenericControl divPeriod = (HtmlGenericControl)Page.FindControl("div_" + sPeriod);
            HtmlGenericControl divTimes = (HtmlGenericControl)Page.FindControl("div_" + sTimes);

            switch (Convert.ToInt32(ddlReceivingSts.SelectedValue))
            {
                case 201:
                case 202:
                    divTimes.Visible = false;
                    divPeriod.Visible = true;
                    break;
                case 203:
                case 204:
                    divTimes.Visible = true;
                    divPeriod.Visible = true;
                    break;
                default:
                    divTimes.Visible = false;
                    divPeriod.Visible = false;
                    break;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("SMSReceivingStsCondition :: " + ex.Message);
        }
    }

    private void CallReceivingStsCondition(string sddl, string sPeriod, string sTimes)
    {
        try
        {

            DropDownList ddlReceivingSts = (DropDownList)Page.FindControl(sddl);
            HtmlGenericControl divPeriod = (HtmlGenericControl)Page.FindControl("div_" + sPeriod);
            HtmlGenericControl divTimes = (HtmlGenericControl)Page.FindControl("div_" + sTimes);

            switch (Convert.ToInt32(ddlReceivingSts.SelectedValue))
            {
                case 301:
                    divTimes.Visible = false;
                    divPeriod.Visible = true;
                    break;
                case 302:
                    divTimes.Visible = true;
                    divPeriod.Visible = true;
                    break;
                default:
                    divTimes.Visible = false;
                    divPeriod.Visible = false;
                    break;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("CallReceivingStsCondition :: " + ex.Message);
        }
    }

    protected void btn_Condition_1_Click(object sender, EventArgs e)
    {
        bool bVisible;
        bVisible = div_Condition_1.Visible;

        div_Condition_1.Visible = !bVisible;
    }
    protected void btn_Condition_2_Click(object sender, EventArgs e)
    {
        bool bVisible;
        bVisible = div_Condition_2.Visible;

        div_Condition_2.Visible = !bVisible;
    }
    protected void btn_Condition_3_Click(object sender, EventArgs e)
    {
        bool bVisible;
        bVisible = div_Condition_3.Visible;

        div_Condition_3.Visible = !bVisible;
    }
    protected void chkList_SelectedIndexChanged(object sender, EventArgs e)
    {

        CheckBoxList CheckBoxList = (CheckBoxList)sender;

        if (CheckBoxList.Items[0].Selected)
        {
            for (int i = 1; i < CheckBoxList.Items.Count; i++)
            {
                CheckBoxList.Items[i].Enabled = false;
                CheckBoxList.Items[i].Selected = false;
            }
        }

        else
        {
            for (int i = 0; i < CheckBoxList.Items.Count; i++)
                CheckBoxList.Items[i].Enabled = true;
        }
    }

    private DataTable GetOutboundEventMasterData()
    {
        DIAGEO.COMMON.User User = new DIAGEO.COMMON.User();
        DataTable dtOutboundEventMaster = new DataTable();
        DataRow dr;

        try
        {
            dtOutboundEventMaster.Columns.Add("eventId");
            dtOutboundEventMaster.Columns.Add("eventName");
            dtOutboundEventMaster.Columns.Add("eventDateFrom");
            dtOutboundEventMaster.Columns.Add("eventDateTo");
            dtOutboundEventMaster.Columns.Add("excludeBlockList");

            dtOutboundEventMaster.Columns.Add("flag");

            dtOutboundEventMaster.Columns.Add("IncludeDiageoVIP");
            dtOutboundEventMaster.Columns.Add("IncludeOutletVIP");
            dtOutboundEventMaster.Columns.Add("IncludeOutletVIPList");
            dtOutboundEventMaster.Columns.Add("ExcludeOtherOutletVIP");
            dtOutboundEventMaster.Columns.Add("RecordPriority");
            dtOutboundEventMaster.Columns.Add("CustomerMaxRepeat");

            dtOutboundEventMaster.Columns.Add("CreateBy");
            dtOutboundEventMaster.Columns.Add("CreateDate");
            dtOutboundEventMaster.Columns.Add("UpdateBy");
            dtOutboundEventMaster.Columns.Add("UpdateDate");

            dr = dtOutboundEventMaster.NewRow();

            dr["eventId"] = EventID.ToString();
            dr["eventName"] = txt_EventName.Text;
            dr["eventDateFrom"] = ConvertDatetimeFromDatepicker(dtp_EventDateFrom.Text);
            dr["eventDateTo"] = ConvertDatetimeFromDatepicker(dtp_EventDateTo.Text);
            dr["IncludeDiageoVIP"] = (chk_IncludeDiageoVIP.Checked) ? "T" : "F";
            dr["IncludeOutletVIP"] = (chk_IncludeOutletVIP.Checked) ? "T" : "F";
            dr["IncludeOutletVIPList"] = GetOutletVIPIDList();
            dr["ExcludeOtherOutletVIP"] = (chk_ExcludeOtherOutletVIP.Checked) ? "T" : "F";
            dr["excludeBlockList"] = (chk_excludeBlockList.Checked) ? "T" : "F";
            dr["RecordPriority"] = rdbtn_RecordPriority.SelectedValue;
            dr["CustomerMaxRepeat"] = ddl_CustomerMaxRepeat.SelectedValue;

            //Wait For Modify to Change Flat "A"/"D"
            dr["flag"] = "A";

            dr["CreateBy"] = User.UserID;
            dr["CreateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
            dr["UpdateBy"] = User.UserID;
            dr["UpdateDate"] = DateTime.Now.ToString("dd/MM/yyyy");

            dtOutboundEventMaster.Rows.Add(dr);
            return dtOutboundEventMaster;
        }
        
        catch (Exception ex)
        {
            throw new Exception("GetOutboundEventMasterData :: " + ex.Message);
        }
    }

    private DataTable GetOutboundEvent(ref DataTable dtEventVisitCondition)
    {
        DIAGEO.COMMON.User User = new DIAGEO.COMMON.User();
        DataTable dtOutboundEvent = new DataTable();
        DataRow drOutboundEvent;

        try
        {
            //Create Column for dtOutboundEvent
            dtOutboundEvent.Columns.Add("eventId");
            dtOutboundEvent.Columns.Add("DataGroup");
            dtOutboundEvent.Columns.Add("name");
            dtOutboundEvent.Columns.Add("senderName");

            dtOutboundEvent.Columns.Add("brandPreJwGold");
            dtOutboundEvent.Columns.Add("brandPreJwBlack");
            dtOutboundEvent.Columns.Add("brandPreSmirnoff");
            dtOutboundEvent.Columns.Add("brandPreJwRed");
            dtOutboundEvent.Columns.Add("brandPreJwGreen");
            dtOutboundEvent.Columns.Add("brandPreBenmore");
            dtOutboundEvent.Columns.Add("brandPreBaileys");
            dtOutboundEvent.Columns.Add("brandPreAbsolut");
            dtOutboundEvent.Columns.Add("brandPreBallentine");
            dtOutboundEvent.Columns.Add("brandPreBlend");
            dtOutboundEvent.Columns.Add("brandPreChivas");
            dtOutboundEvent.Columns.Add("brandPreDewar");
            dtOutboundEvent.Columns.Add("brandPre100Pipers");
            dtOutboundEvent.Columns.Add("brandPre100Pipers8Y");
            dtOutboundEvent.Columns.Add("brandPreHennessy");
            dtOutboundEvent.Columns.Add("brandPreOther");
            dtOutboundEvent.Columns.Add("brandPreNotDrink");

            dtOutboundEvent.Columns.Add("gender");
            dtOutboundEvent.Columns.Add("validOptInDiageo");
            dtOutboundEvent.Columns.Add("validOptInOutlet");
            dtOutboundEvent.Columns.Add("startAge");
            dtOutboundEvent.Columns.Add("endAge");
            dtOutboundEvent.Columns.Add("includeSMSBounce");
            dtOutboundEvent.Columns.Add("includeEDMBounce");
            dtOutboundEvent.Columns.Add("includeCallcenterBounce");

            dtOutboundEvent.Columns.Add("PlatformIDList");
            dtOutboundEvent.Columns.Add("OutletIDList");
            dtOutboundEvent.Columns.Add("ProvinceList");
            dtOutboundEvent.Columns.Add("ActivePeriodDateFrom");
            dtOutboundEvent.Columns.Add("ActivePeriodDateTo");
            dtOutboundEvent.Columns.Add("Adorer");
            dtOutboundEvent.Columns.Add("Adopter");
            dtOutboundEvent.Columns.Add("Acceptor");
            dtOutboundEvent.Columns.Add("Available");
            dtOutboundEvent.Columns.Add("Rejecter");

            dtOutboundEvent.Columns.Add("EDMValid");
            dtOutboundEvent.Columns.Add("SMSValid");
            dtOutboundEvent.Columns.Add("CallCenterValid");

            dtOutboundEvent.Columns.Add("BDRangeFrom");
            dtOutboundEvent.Columns.Add("BDRangeTo");

            dtOutboundEvent.Columns.Add("active");

            dtOutboundEvent.Columns.Add("CreateBy");
            dtOutboundEvent.Columns.Add("CreateDate");
            dtOutboundEvent.Columns.Add("UpdateBy");
            dtOutboundEvent.Columns.Add("UpdateDate");

            //Create Column for dtEventVisitCondition
            //dtEventVisitCondition.Columns.Add("conditionId");
            dtEventVisitCondition.Columns.Add("eventId");
            dtEventVisitCondition.Columns.Add("DataGroup");
            dtEventVisitCondition.Columns.Add("typeId");
            dtEventVisitCondition.Columns.Add("TypeIDDetail");
            dtEventVisitCondition.Columns.Add("dateFrom");
            dtEventVisitCondition.Columns.Add("dateTo");
            dtEventVisitCondition.Columns.Add("visitFrequency");
            dtEventVisitCondition.Columns.Add("CreateBy");
            dtEventVisitCondition.Columns.Add("CreateDate");
            dtEventVisitCondition.Columns.Add("UpdateBy");
            dtEventVisitCondition.Columns.Add("UpdateDate");

            for (int i = 1; i <= 3; i++)
            {
                drOutboundEvent = dtOutboundEvent.NewRow();

                //drOutboundEvent["eventId"] = EventID;
                drOutboundEvent["DataGroup"] = i;

                CheckBox chkCondition = (CheckBox)Page.FindControl("chk_Condition_" + i.ToString());
                drOutboundEvent["active"] = (chkCondition.Checked) ? "T" : "F";

                drOutboundEvent["CreateBy"] = User.UserID;
                drOutboundEvent["CreateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                drOutboundEvent["UpdateBy"] = User.UserID;
                drOutboundEvent["UpdateDate"] = DateTime.Now.ToString("dd/MM/yyyy");

                GetPrimaryData(ref drOutboundEvent, i.ToString());
                GetCustomerData(ref drOutboundEvent, i.ToString());
                GetCommunicationData(ref drOutboundEvent, ref dtEventVisitCondition, i.ToString());

                dtOutboundEvent.Rows.Add(drOutboundEvent);
                
            }


            return dtOutboundEvent;
        }
        catch (Exception ex)
        {
            throw new Exception("GetOutboundEvent :: " + ex.Message);
        }
    }

    private void GetPrimaryData(ref DataRow dr, string sNum)
    {
        
        TextBox txtMsgSender = (TextBox)Page.FindControl("txt_SenderName_" + sNum);
        string sPlatformIDList = "";
        try
        {
            sPlatformIDList = GetPlatformIDList(sNum);

            dr["PlatformIDList"] = sPlatformIDList;
            dr["senderName"] = txtMsgSender.Text;

        }
        catch (Exception ex)
        {
            throw new Exception("GetPrimaryData :: " + ex.Message);
        }
    }

    private void GetCustomerData(ref DataRow dr, string sNum)
    {
        string sProvinceList = "";
        string sOutlet = "";

        try
        {
            RadioButtonList rbtn_Gender = (RadioButtonList)Page.FindControl("rbtn_ListGender_" + sNum);
            TextBox txtAgeFrom = (TextBox)Page.FindControl("txt_AgeFrom_" + sNum);
            TextBox txtAgeTo = (TextBox)Page.FindControl("txt_AgeTo_" + sNum);

            TextBox txtBirthDateFrom = (TextBox)Page.FindControl("dtp_BirthDateFrom_" + sNum);
            TextBox txtBirthDateTo = (TextBox)Page.FindControl("dtp_BirthDateTo_" + sNum);

            TextBox txtActPeriodFrom = (TextBox)Page.FindControl("dtp_ActPeriodFrom_" + sNum);
            TextBox txtActPeriodTo = (TextBox)Page.FindControl("dtp_ActPeriodTo_" + sNum);

            sProvinceList = GetProvinceList(sNum);
            sOutlet = GetOutletIDList(sNum);

            dr["gender"] = rbtn_Gender.SelectedValue;
            dr["startAge"] = txtAgeFrom.Text;
            dr["endAge"] = txtAgeTo.Text;
            dr["ProvinceList"] = sProvinceList;

            dr["BDRangeFrom"] = ConvertDatetimeFromDatepicker(txtBirthDateFrom.Text);
            dr["BDRangeTo"] = ConvertDatetimeFromDatepicker(txtBirthDateTo.Text);

            dr["OutletIDList"] = sOutlet;
            
            GetBrandPreference(ref dr, sNum);

            dr["ActivePeriodDateFrom"] = ConvertDatetimeFromDatepicker(txtActPeriodFrom.Text);
            dr["ActivePeriodDateTo"] = ConvertDatetimeFromDatepicker(txtActPeriodTo.Text);

            GetDataCheckBoxList(ref dr, "chk_OptIn_", sNum);
            GetDataCheckBoxList(ref dr, "chk_4A_", sNum);

        }
        catch (Exception ex)
        {
            throw new Exception("GetCustomerData :: " + ex.Message);
        }
    }

    private void GetCommunicationData(ref DataRow drOutbound, ref DataTable dtEventVisit, string sNum)
    {
        try
        {
            RadioButtonList rbtn_EDMValid = (RadioButtonList)Page.FindControl("rdbtn_EDMValid_" + sNum);
            RadioButtonList rbtn_SMSValid = (RadioButtonList)Page.FindControl("rdbtn_SMSValid_" + sNum);
            RadioButtonList rbtn_CallCenterValid = (RadioButtonList)Page.FindControl("rdbtn_CallCenterValid_" + sNum);

            drOutbound["EDMValid"] = rbtn_EDMValid.SelectedValue;
            drOutbound["SMSValid"] = rbtn_SMSValid.SelectedValue;
            drOutbound["CallCenterValid"] = rbtn_CallCenterValid.SelectedValue;

            GetDataCheckBoxList(ref drOutbound, "chk_IncludeBounce_", sNum);

            //Get Data
            GetEDMData(ref dtEventVisit, sNum);
            GetSMSData(ref dtEventVisit, sNum);
            GetCallData(ref dtEventVisit, sNum);

        }
        catch (Exception ex)
        {
            throw new Exception("GetCommunicationData :: " + ex.Message);
        }
    }

    private string GetPlatformIDList(string sNum)
    {
        string sPlatformIDList = "";

        try
        {
            CheckBoxList chkListPlatform = (CheckBoxList)Page.FindControl("chkList_Platform_" + sNum);

            //Get Platform
            if (chkListPlatform.Items[0].Selected)
            {
                for (int i = 1; i < chkListPlatform.Items.Count; i++)
                {
                    sPlatformIDList += "'" + chkListPlatform.Items[i].Value + "'";
                    if (chkListPlatform.Items.Count - 1 > i)
                    {
                        sPlatformIDList += ",";
                    }
                }
            }
            else
            {
                for (int i = 1; i < chkListPlatform.Items.Count; i++)
                {
                    if (chkListPlatform.Items[i].Selected)
                    {
                        if (sPlatformIDList != "")
                        {
                            sPlatformIDList += ",";
                        }
                        sPlatformIDList += "'" + chkListPlatform.Items[i].Value + "'";
                    }
                }
            }

            return sPlatformIDList;
        }
        catch (Exception ex)
        {
            throw new Exception("GetPlatform :: " + ex.Message);
        }
    
    }
    

    private string GetProvinceList(string sNum)
    {
        string sProvinceList = "";

        try
        {
            CheckBoxList chkListProvince = (CheckBoxList)Page.FindControl("chk_Province_" + sNum);
            CheckBox chkIncludeUnknown = (CheckBox)Page.FindControl("chk_IncludeUnknown_" + sNum);

            if (chkListProvince.Items[0].Selected)
            {
                sProvinceList += "Null";
            }
            else
            {
                for (int i = 1; i < chkListProvince.Items.Count; i++)
                {
                    if (chkListProvince.Items[i].Selected)
                    {
                        if (sProvinceList != "")
                        {
                            sProvinceList += ",";
                        }
                        sProvinceList += "'" + chkListProvince.Items[i].Text + "'";
                    }
                }
            }

            if (chkIncludeUnknown.Checked)
            {
                sProvinceList += ",''";
            }

            return sProvinceList;
        }
        catch (Exception ex)
        {
            throw new Exception("GetProvinceList :: " + ex.Message);
        }       
    }

    private string GetOutletIDList(string sNum)
    {
        string sOutletIDList = "";
        try
        {
            CheckBoxList chkOutlet = (CheckBoxList)Page.FindControl("chk_Outlet_" + sNum);

            if (chkOutlet.Items[0].Selected)
            {
                for (int i = 1; i < chkOutlet.Items.Count; i++)
                {
                    sOutletIDList += "'" + chkOutlet.Items[i].Value + "'";
                    if (chkOutlet.Items.Count - 1 > i)
                    {
                        sOutletIDList += ",";
                    }
                }
            }
            else
            {
                for (int i = 1; i < chkOutlet.Items.Count; i++)
                {
                    if (chkOutlet.Items[i].Selected)
                    {
                        if (sOutletIDList != "")
                        {
                            sOutletIDList += ",";
                        }

                        sOutletIDList += "'" + chkOutlet.Items[i].Value + "'";

                    }
                }
            }

            return sOutletIDList;
        }
        catch (Exception ex)
        {
            throw new Exception("GetOutletIDList :: " + ex.Message);
        }
    }

    private string GetOutletVIPIDList()
    {
        string sOutletIDList = "";
        try
        {
            CheckBoxList chkOutletVIP = (CheckBoxList)Page.FindControl("chk_OutletVIP");

            if (chkOutletVIP.Items[0].Selected)
            {
                for (int i = 1; i < chkOutletVIP.Items.Count; i++)
                {
                    sOutletIDList += "'" + chkOutletVIP.Items[i].Value + "'";
                    if (chkOutletVIP.Items.Count - 1 > i)
                    {
                        sOutletIDList += ",";
                    }
                }
            }
            else
            {
                for (int i = 1; i < chkOutletVIP.Items.Count; i++)
                {
                    if (chkOutletVIP.Items[i].Selected)
                    {
                        sOutletIDList += "'" + chkOutletVIP.Items[i].Value + "'";
                        if (chkOutletVIP.Items.Count - 1 > i)
                        {
                            sOutletIDList += ",";
                        }
                    }
                }
            }

            return sOutletIDList;
        }
        catch (Exception ex)
        {
            throw new Exception("GetOutletVIPIDList :: " + ex.Message);
        }
    }

    private void GetBrandPreference(ref DataRow dr, string sNum)
    {
        
        try
        {
            CheckBoxList chkBrand = (CheckBoxList)Page.FindControl("chk_Brand_" + sNum);

            if (chkBrand.Items[0].Selected)
            {
                dr["brandPreJwGold"] = "T";
                dr["brandPreJwBlack"] = "T";
                dr["brandPreSmirnoff"] = "T";
                dr["brandPreJwRed"] = "T";
                dr["brandPreJwGreen"] = null;
                dr["brandPreBenmore"] = "T";
                dr["brandPreBaileys"] = null;
                dr["brandPreAbsolut"] = "T";
                dr["brandPreBallentine"] = "T";
                dr["brandPreBlend"] = "T";
                dr["brandPreChivas"] = "T";
                dr["brandPreDewar"] = null;
                dr["brandPre100Pipers"] = "T";
                dr["brandPre100Pipers8Y"] = null;
                dr["brandPreHennessy"] = "T";
                dr["brandPreOther"] = null;
                dr["brandPreNotDrink"] = null;
            }
            else
            {
                for (int i = 1; i < chkBrand.Items.Count; i++)
                {
                    if (chkBrand.Items[i].Selected)
                    {
                        dr[chkBrand.Items[i].Value] = "T";
                    }
                    else
                    {
                        dr[chkBrand.Items[i].Value] = null;
                    }
                }

                dr["brandPreJwGreen"] = null;
                dr["brandPreBaileys"] = null;
                dr["brandPreDewar"] = null;
                dr["brandPre100Pipers8Y"] = null;
                dr["brandPreOther"] = null;
                dr["brandPreNotDrink"] = null;
            }

        }
        catch (Exception ex)
        {
            throw new Exception("GetBrandPreference :: " + ex.Message);
        }
    }

    private void GetDataCheckBoxList(ref DataRow dr, string sName, string sNum)
    {
        try
        {
            CheckBoxList chkList = (CheckBoxList)Page.FindControl(sName + sNum);

            for (int i = 1; i < chkList.Items.Count; i++)
            {
                if (chkList.Items[i].Selected)
                {
                    dr[chkList.Items[i].Value] = "T";
                }
                else
                {
                    dr[chkList.Items[i].Value] = null;
                }
            }

        }
        catch (Exception ex)
        {
            throw new Exception("GetDataCheckBoxList :: " + ex.Message);
        }
    }

    private void GetEDMData(ref DataTable dtEventVisit, string sNum)
    {
        DIAGEO.COMMON.User User = new DIAGEO.COMMON.User();

        try
        {
            CheckBox chkEDM = (CheckBox)Page.FindControl("chk_EDM_" + sNum);
            DataGrid dg_EDM = (DataGrid)Page.FindControl("dg_EDMCondition_" + sNum);

            if (chkEDM.Checked)
            {
                for (int i = 0; i < dg_EDM.Items.Count; i++)
                {
                    DataRow dr = dtEventVisit.NewRow();

                    dr["DataGroup"] = sNum;
                    dr["typeId"] = 1;
                    dr["TypeIDDetail"] = dg_EDM.Items[i].Cells[5].Text;
                    dr["dateFrom"] = ConvertDatetimeFromDatepicker(dg_EDM.Items[i].Cells[3].Text);
                    dr["dateTo"] = ConvertDatetimeFromDatepicker(dg_EDM.Items[i].Cells[4].Text);
                    dr["visitFrequency"] = dg_EDM.Items[i].Cells[2].Text;

                    dr["CreateBy"] = User.UserID;
                    dr["CreateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                    dr["UpdateBy"] = User.UserID;
                    dr["UpdateDate"] = DateTime.Now.ToString("dd/MM/yyyy");

                    dtEventVisit.Rows.Add(dr);
                }
            }
            
        }
        catch (Exception ex)
        {
            throw new Exception("GetEDMData :: " + ex.Message);
        }
    }

    private void GetSMSData(ref DataTable dtEventVisit, string sNum)
    {
        DIAGEO.COMMON.User User = new DIAGEO.COMMON.User();

        try
        {
            CheckBox chkSMS = (CheckBox)Page.FindControl("chk_SMS_" + sNum);
            DataGrid dg_SMS = (DataGrid)Page.FindControl("dg_SMSCondition_" + sNum);

            if (chkSMS.Checked)
            {
                for (int i = 0; i < dg_SMS.Items.Count; i++)
                {
                    DataRow dr = dtEventVisit.NewRow();

                    dr["DataGroup"] = sNum;
                    dr["typeId"] = 2;
                    dr["TypeIDDetail"] = dg_SMS.Items[i].Cells[5].Text;
                    dr["dateFrom"] = ConvertDatetimeFromDatepicker(dg_SMS.Items[i].Cells[3].Text);
                    dr["dateTo"] = ConvertDatetimeFromDatepicker(dg_SMS.Items[i].Cells[4].Text);
                    dr["visitFrequency"] = dg_SMS.Items[i].Cells[2].Text;

                    dr["CreateBy"] = User.UserID;
                    dr["CreateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                    dr["UpdateBy"] = User.UserID;
                    dr["UpdateDate"] = DateTime.Now.ToString("dd/MM/yyyy");

                    dtEventVisit.Rows.Add(dr);
                }
            }

        }
        catch (Exception ex)
        {
            throw new Exception("GetSMSData :: " + ex.Message);
        }
    }

    private void GetCallData(ref DataTable dtEventVisit, string sNum)
    {
        DIAGEO.COMMON.User User = new DIAGEO.COMMON.User();

        try
        {
            CheckBox chkCall = (CheckBox)Page.FindControl("chk_Call_" + sNum);
            DataGrid dg_Call = (DataGrid)Page.FindControl("dg_CallCondition_" + sNum);

            if (chkCall.Checked)
            {               
                for (int i = 0; i < dg_Call.Items.Count; i++)
                {
                    DataRow dr = dtEventVisit.NewRow();

                    dr["DataGroup"] = sNum;
                    dr["typeId"] = 3;
                    dr["TypeIDDetail"] = dg_Call.Items[i].Cells[5].Text;
                    dr["dateFrom"] = ConvertDatetimeFromDatepicker(dg_Call.Items[i].Cells[3].Text);
                    dr["dateTo"] = ConvertDatetimeFromDatepicker(dg_Call.Items[i].Cells[4].Text);
                    dr["visitFrequency"] = dg_Call.Items[i].Cells[6].Text;

                    dr["CreateBy"] = User.UserID;
                    dr["CreateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                    dr["UpdateBy"] = User.UserID;
                    dr["UpdateDate"] = DateTime.Now.ToString("dd/MM/yyyy");

                    dtEventVisit.Rows.Add(dr);
                }
            }

        }
        catch (Exception ex)
        {
            throw new Exception("GetCallData :: " + ex.Message);
        }
    }

    protected void chk_Condition_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            CheckBox chk = (CheckBox)sender;
            string schkID = chk.ID;
            string sNum = schkID.Substring(schkID.Length - 2, 2);
            LinkButton lnkBtn = (LinkButton)Page.FindControl("btn_Condition" + sNum);
            HtmlGenericControl divCondition = (HtmlGenericControl)Page.FindControl("div_Condition" + sNum);

            lnkBtn.Enabled = chk.Checked;

            if (!chk.Checked)
            {
                divCondition.Visible = false;
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "chk_Condition_CheckedChanged :: " + ex.Message);
        }
        
    }

    private string ValidateData()
    {
        string sRet = "";
        try
        {
            if (txt_EventName.Text == "")
            {
                sRet = "Please Input Event Name";
                return sRet;
            }

            if (dtp_EventDateFrom.Text == "" || dtp_EventDateTo.Text == "")
            {
                sRet = "Please Select Event Date";
                return sRet;
            }

            //Loop for Check Item in each Condition
            bool bCheck = false;
            for (int i = 1; i <= 3; i++)
            {
                CheckBox chk = (CheckBox)Page.FindControl("chk_Condition_" + i.ToString());

                if (chk.Checked)
                { 
                    CheckBoxList chkList = (CheckBoxList)Page.FindControl("chkList_Platform_" + i.ToString());
                    TextBox txtSender = (TextBox)Page.FindControl("txt_SenderName_" + i.ToString());

                    //Loop for Check Item in Checklist
                    for (int c = 0; c < chkList.Items.Count; c++)
                    {
                        if (chkList.Items[c].Selected)
                        {
                            bCheck = true;
                            break;
                        }
                    }

                    if (bCheck == false)
                    {
                        sRet = "Please Select Platform in Data Condition Set " + i.ToString();
                        break;
                    }

                    if (txtSender.Text == "")
                    {
                        sRet = "Please input Message Sender in Data Condition Set " + i.ToString();
                        break;
                    }

                }

            }

            return sRet;
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateData :: " + ex.Message);
        }
    }

    private bool ValidateDataCondition(TextBox dpDateFrom, TextBox dpDateTo, TextBox txtTimes,DropDownList ddlStatus)
    {
        bool bRet = true;
 
        try
        {
            if ((dpDateFrom.Text != "") && (dpDateTo.Text != ""))
            {
                DateTime TempEventDateFrom, TempEventDateTo;
                if (!((DateTime.TryParseExact(dpDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TempEventDateFrom) && (DateTime.TryParseExact(dpDateTo.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TempEventDateTo)))))
                {
                    JsClientAlert("DateFromOrDateTo", " Format Date In DateFrom Or DateTo Incorrect ");
                    return false;
                }
                else
                {
                    if (ConvertDatetimeFromDatepicker(dpDateFrom.Text.Trim()) > ConvertDatetimeFromDatepicker(dpDateTo.Text.Trim()))
                    {
                        JsClientAlert("Validate", "EventDateTo can\\'t less than EventDateFrom ");
                        return false;
                    }
                }
            }
            else
            {
                bRet = false;
            }

            if ((ddlStatus!= null && ddlStatus.Visible == true && ddlStatus.SelectedIndex == 0) || (txtTimes != null && txtTimes.Visible == true && txtTimes.Text == "") || bRet == false)
            {
                JsClientAlert("Validate Data", "Data not complete");
                return false;
            }

            return true;
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateCondition :: " + ex.Message);
        }
    }

    protected void btnReset_Click1(object sender, EventArgs e)
    {
        //ClearDataDIV("Condition_1");
        ClearDataDIV("VIP");
    }
}
