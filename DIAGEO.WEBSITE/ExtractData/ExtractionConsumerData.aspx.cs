﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;
using System.IO;
//using iTextSharp.text.pdf;
//using Microsoft.Office.Interop.Excel;
using System.Threading;
using System.Globalization;
using System.Reflection;
using System.Xml;
using System.Text;

public partial class ExtractData_ExtractionConsumerData : BasePage
{
    public string BaileysListCondition 
    { 
        get 
        {
            if (ViewState["BaileysListCondition"] == null)
                return "";

            return ViewState["BaileysListCondition"].ToString(); 
        } 
        set 
        {
            ViewState["BaileysListCondition"] = value; 
        } 
    }

    public string FourABenmoreListCondition
    {
        get
        {
            if (ViewState["FourABenmoreListCondition"] == null)
                return "";

            return ViewState["FourABenmoreListCondition"].ToString();
        }
        set
        {
            ViewState["FourABenmoreListCondition"] = value;
        }
    }

    public string JwBlackListCondition
    {
        get
        {
            if (ViewState["JwBlackListCondition"] == null)
                return "";

            return ViewState["JwBlackListCondition"].ToString();
        }
        set
        {
            ViewState["JwBlackListCondition"] = value;
        }
    }

    public string JwGoldListCondition
    {
        get
        {
            if (ViewState["JwGoldListCondition"] == null)
                return "";

            return ViewState["JwGoldListCondition"].ToString();
        }
        set
        {
            ViewState["JwGoldListCondition"] = value;
        }
    }

    public string JwGreenListCondition
    {
        get
        {
            if (ViewState["JwGreenListCondition"] == null)
                return "";

            return ViewState["JwGreenListCondition"].ToString();
        }
        set
        {
            ViewState["JwGreenListCondition"] = value;
        }
    }

    public string JwRedListCondition
    {
        get
        {
            if (ViewState["JwRedListCondition"] == null)
                return "";

            return ViewState["JwRedListCondition"].ToString();
        }
        set
        {
            ViewState["JwRedListCondition"] = value;
        }
    }

    public string SmimoffListCondition
    {
        get
        {
            if (ViewState["SmimoffListCondition"] == null)
                return "";

            return ViewState["SmimoffListCondition"].ToString();
        }
        set
        {
            ViewState["SmimoffListCondition"] = value;
        }
    }

    public string AbsoluteListCondition
    {
        get
        {
            if (ViewState["AbsoluteListCondition"] == null)
                return "";

            return ViewState["AbsoluteListCondition"].ToString();
        }
        set
        {
            ViewState["AbsoluteListCondition"] = value;
        }
    }

    public string BallentineListCondition
    {
        get
        {
            if (ViewState["BallentineListCondition"] == null)
                return "";

            return ViewState["BallentineListCondition"].ToString();
        }
        set
        {
            ViewState["BallentineListCondition"] = value;
        }
    }

    public string BlendListCondition
    {
        get
        {
            if (ViewState["BlendListCondition"] == null)
                return "";

            return ViewState["BlendListCondition"].ToString();
        }
        set
        {
            ViewState["BlendListCondition"] = value;
        }
    }

    public string ChivasListCondition
    {
        get
        {
            if (ViewState["ChivasListCondition"] == null)
                return "";

            return ViewState["ChivasListCondition"].ToString();
        }
        set
        {
            ViewState["ChivasListCondition"] = value;
        }
    }

    public string DewarListCondition
    {
        get
        {
            if (ViewState["DewarListCondition"] == null)
                return "";

            return ViewState["DewarListCondition"].ToString();
        }
        set
        {
            ViewState["DewarListCondition"] = value;
        }
    }

    public string FourA100PipersListCondition
    {
        get
        {
            if (ViewState["FourA100PipersListCondition"] == null)
                return "";

            return ViewState["FourA100PipersListCondition"].ToString();
        }
        set
        {
            ViewState["FourA100PipersListCondition"] = value;
        }
    }

    public string FourA100Pipers8yrsListCondition
    {
        get
        {
            if (ViewState["FourA100Pipers8yrsListCondition"] == null)
                return "";

            return ViewState["FourA100Pipers8yrsListCondition"].ToString();
        }
        set
        {
            ViewState["FourA100Pipers8yrsListCondition"] = value;
        }
    }
    
    public string RecordOFExport 
    { 
        get { return ViewState["RecordOFExport"].ToString(); } set { ViewState["RecordOFExport"] = value;} 
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            ISVisiblePlatform();
            ISVisiblePlatformCalcenter();
            ISVisibleOutletRegister();
            ISVisibleLastVisitOutlet();
            if (!Page.IsPostBack)
            {
                BindFourAType();                
            }
        }
        catch (Exception ex)
        {            
            throw new Exception(ex.Message);
        }
    }

    protected void chkBaileys_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //if (chkBaileys.Checked == false)
            //{
            //    chkBaileysList.SelectedIndex = -1;
            //    //chkBaileysList.Enabled = false;
            //}
            //else
            //{
            //    //chkBaileysList.Enabled = true;
            //    chkBaileysList.SelectedIndex = -1;
            //}
        }
        catch (Exception ex)
        {            
            throw new Exception(ex.Message);
        }
    }

    protected void chkBenmore_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //if (chkBenmore.Checked == false)
            //{
            //    chkFourABenmoreList.SelectedIndex = -1;
            //    //chkFourABenmoreList.Enabled = false;
            //}
            //else
            //{
            //    //chkFourABenmoreList.Enabled = true;
            //    chkFourABenmoreList.SelectedIndex = -1;
            //}
        }
        catch (Exception ex)
        {           
            throw new Exception(ex.Message);
        }
    }

    protected void chkBenmoreJwBlack_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //if (chkBenmoreJwBlack.Checked == false)
            //{
            //    chkJwBlackList.SelectedIndex = -1;
            //    //chkJwBlackList.Enabled = false;
            //}
            //else
            //{
            //    //chkJwBlackList.Enabled = true;
            //    chkJwBlackList.SelectedIndex = -1;
            //}

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkJwGold_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //if (chkJwGold.Checked == false)
            //{
            //    chkJwGoldList.SelectedIndex = -1;
            //    //chkJwGoldList.Enabled = false;
            //}
            //else
            //{
            //    //chkJwGoldList.Enabled = true;
            //    chkJwGoldList.SelectedIndex = -1;
            //}

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkJwGreen_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //if (chkJwGreen.Checked == false)
            //{
            //    chkJwGreenList.SelectedIndex = -1;
            //    //chkJwGreenList.Enabled = false;
            //}
            //else
            //{
            //    //chkJwGreenList.Enabled = true;
            //    chkJwGreenList.SelectedIndex = -1;
            //}

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkJwRed_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //if (chkJwRed.Checked == false)
            //{
            //    chkJwRedList.SelectedIndex = -1;
            //    //chkJwRedList.Enabled = false;
            //}
            //else
            //{
            //    //chkJwRedList.Enabled = true;
            //    chkJwRedList.SelectedIndex = -1;
            //}

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkSmirnoff_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //if (chkSmirnoff.Checked == false)
            //{
            //    chkSmirnoffList.SelectedIndex = -1;
            //    //chkSmirnoffList.Enabled = false;
            //}
            //else
            //{
            //    //chkSmirnoffList.Enabled = true;
            //    chkSmirnoffList.SelectedIndex = -1;
            //}
        }
        catch (Exception ex)
        {            
            throw new Exception(ex.Message);
        }
    }

    protected void chkBallentine_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //if (chkBallentine.Checked == false)
            //{
            //    chkBallentineList.SelectedIndex = -1;
            //    //chkBallentineList.Enabled = false;
            //}
            //else
            //{
            //    //chkBallentineList.Enabled = true;
            //    chkBallentineList.SelectedIndex = -1;
            //}
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkBlend_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //if (chkBlend.Checked == false)
            //{
            //    chkBlendList.SelectedIndex = -1;
            //    //chkBlendList.Enabled = false;
            //}
            //else
            //{
            //    //chkBlendList.Enabled = true;
            //    chkBlendList.SelectedIndex = -1;
            //}
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkChivas_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //if (chkChivas.Checked == false)
            //{
            //    chkChivasList.SelectedIndex = -1;
            //    //chkChivasList.Enabled = false;
            //}
            //else
            //{
            //    //chkChivasList.Enabled = true;
            //    chkChivasList.SelectedIndex = -1;
            //}
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkDewar_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //if (chkDewar.Checked == false)
            //{
            //    chkDewarList.SelectedIndex = -1;
            //    //chkDewarList.Enabled = false;
            //}
            //else
            //{
            //    //chkDewarList.Enabled = true;
            //    chkDewarList.SelectedIndex = -1;
            //}
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chk100Pipers_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //if (chk100Pipers.Checked == false)
            //{
            //    chk100PipersList.SelectedIndex = -1;
            //    //chk100PipersList.Enabled = false;
            //}
            //else
            //{
            //    //chk100PipersList.Enabled = true;
            //    chk100PipersList.SelectedIndex = -1;
            //}
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chk100Pipers8yrs_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //if (chk100Pipers8yrs.Checked == false)
            //{
            //    chk100Pipers8yrsList.SelectedIndex = -1;
            //    //chk100Pipers8yrsList.Enabled = false;
            //}
            //else
            //{
            //    //chk100Pipers8yrsList.Enabled = true;
            //    chk100Pipers8yrsList.SelectedIndex = -1;
            //}
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkAbsolute_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //if (chkAbsolute.Checked == false)
            //{
            //    chkAbsoluteList.SelectedIndex = -1;
            //    //chkAbsoluteList.Enabled = false;
            //}
            //else
            //{
            //    //chkAbsoluteList.Enabled = true;
            //    chkAbsoluteList.SelectedIndex = -1;
            //}
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkBaileysList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CreateConditionCheckBoxList(chkBaileysList);
        }
        catch (Exception ex)
        {            
            throw new Exception(ex.Message);
        }
    }

    protected void chkFourABenmoreList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CreateConditionCheckBoxList(chkFourABenmoreList);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkJwBlackList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CreateConditionCheckBoxList(chkJwBlackList);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkJwGoldList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CreateConditionCheckBoxList(chkJwGoldList);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkJwGreenList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CreateConditionCheckBoxList(chkJwGreenList);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkJwRedList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CreateConditionCheckBoxList(chkJwRedList);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkSmirnoffList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CreateConditionCheckBoxList(chkSmirnoffList);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkBallentineList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CreateConditionCheckBoxList(chkBallentineList);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkBlendList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CreateConditionCheckBoxList(chkBlendList);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkChivasList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CreateConditionCheckBoxList(chkChivasList);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkDewarList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CreateConditionCheckBoxList(chkDewarList);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chk100PipersList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CreateConditionCheckBoxList(chk100PipersList);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chk100Pipers8yrsList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CreateConditionCheckBoxList(chk100Pipers8yrsList);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void chkAbsoluteList_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            CreateConditionCheckBoxList(chkAbsoluteList);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnExportData_Click(object sender, EventArgs e)
    {
        ConsumerBLL consumerBLL;
        System.Data.DataTable dtConsumer;
        string platform = "";
        string registerOutlet = "";
        string lastVisitOutlet = "";
        string subscribePlatform = "";
        User user;

        try
        {
            user = new User();
            CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));

            if (CanEdit)
            {
                consumerBLL = new ConsumerBLL();

                if (txtRegisterDateFrom.Value != "" && txtRegisterDateTo.Value == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('โปรดกรอก Register date ให้ครบ');", true);
                    return;
                }

                if (txtRegisterDateFrom.Value == "" && txtRegisterDateTo.Value != "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('โปรดกรอก Register date ให้ครบ');", true);
                    return;
                }

                if (txtLastVisitDateFrom.Value != "" && txtLastVisitDateTo.Value == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('โปรดกรอก Last visit date ให้ครบ');", true);
                    return;
                }

                if (txtLastVisitDateFrom.Value == "" && txtLastVisitDateTo.Value != "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('โปรดกรอก Last visit date ให้ครบ');", true);
                    return;
                }

                if (txtDateOfBirthFrom.Value != "" && txtDateOfBirthTo.Value == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('โปรดกรอก Date of birth ให้ครบ');", true);
                    return;
                }

                if (txtDateOfBirthFrom.Value == "" && txtDateOfBirthTo.Value != "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('โปรดกรอก Date of birth ให้ครบ');", true);
                    return;
                }

                if (txtAgeFrom.Text == "" && txtAgeTo.Text != "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('โปรดกรอก Age ให้ครบ');", true);
                    return;
                }

                if (txtAgeFrom.Text != "" && txtAgeTo.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('โปรดกรอก Age ให้ครบ');", true);
                    return;
                }

                foreach (ListItem item in listPlatform.Items)
                {
                    platform = platform + "'" + item.Value + "'" + ",";
                }

                if (platform != "")
                    platform = platform.Remove(platform.Length - 1);

                foreach (ListItem item in listOutletRegister.Items)
                {
                    registerOutlet = registerOutlet + item.Value + ",";
                }

                if (registerOutlet != "")
                    registerOutlet = registerOutlet.Remove(registerOutlet.Length - 1);

                foreach (ListItem item in listOutletLastVisit.Items)
                {
                    lastVisitOutlet = lastVisitOutlet + item.Value + ",";
                }

                if (lastVisitOutlet != "")
                    lastVisitOutlet = lastVisitOutlet.Remove(lastVisitOutlet.Length - 1);

                foreach (ListItem item in listPlatformCallcenter.Items)
                {
                    subscribePlatform = subscribePlatform + item.Value + ",";
                }

                if (subscribePlatform != "")
                    subscribePlatform = subscribePlatform.Remove(subscribePlatform.Length - 1);

                dtConsumer = consumerBLL.ExtractData(platform, chkBaileys.Checked, chkBenmore.Checked, chkBenmoreJwBlack.Checked, chkJwGold.Checked,
                             chkJwGreen.Checked, chkJwRed.Checked, chkSmirnoff.Checked, chkAbsolute.Checked, chkBallentine.Checked, chkBlend.Checked,
                             chkChivas.Checked, chkDewar.Checked, chk100Pipers.Checked, chk100Pipers8yrs.Checked,
                             BaileysListCondition, FourABenmoreListCondition, JwBlackListCondition, JwGoldListCondition, JwGreenListCondition,
                             JwRedListCondition, SmimoffListCondition, AbsoluteListCondition, BallentineListCondition, BlendListCondition,
                             ChivasListCondition, DewarListCondition, FourA100PipersListCondition, FourA100Pipers8yrsListCondition,
                             rdoGender.SelectedValue, txtAgeFrom.Text, txtAgeTo.Text, ConvertDatetimeFromDatepicker(txtDateOfBirthFrom.Value), ConvertDatetimeFromDatepicker(txtDateOfBirthTo.Value),
                             ddlProvince.SelectedValue, registerOutlet, ConvertDatetimeFromDatepicker(txtRegisterDateFrom.Value), ConvertDatetimeFromDatepicker(txtRegisterDateTo.Value), lastVisitOutlet,
                             ConvertDatetimeFromDatepicker(txtLastVisitDateFrom.Value), ConvertDatetimeFromDatepicker(txtLastVisitDateTo.Value), chkEmail.Checked, chkMobile.Checked, chkCallcenter.Checked, subscribePlatform, 1, 0, 0);

                RecordOFExport = dtConsumer.Rows[0][0].ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "ConfirmSave('" + dtConsumer.Rows[0][0].ToString() + "');", true);
            }
            else
            {
                JsClientCustom("ExtractionConsumerData", "alert('ไม่มีสิทธิ์ Export ข้อมูล');");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnChoosePlatform_Click(object sender, EventArgs e)
    {
        try
        {
            string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + "/DIAGEO.WEBSITE/ExtractData/PlatFormPopup.aspx?ComeFrom=PlatForm&PlatformCode=" + lblPlatform.Text + "','mywindow','dialogWidth:1000px; dialogHeight:250px; center:yes; scroll:yes'); document.getElementById('btnRefresh').click();";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", window, true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        ExtractData extractData;

        try
        {
            extractData = new ExtractData();
            if (extractData.Platform != null)
            {
                if (extractData.Platform != null)
                {
                    listPlatform.DataTextField = "Name";
                    listPlatform.DataValueField = "Value";
                    listPlatform.DataSource = extractData.Platform;
                    listPlatform.DataBind();
                }
            }

            if (extractData.PlatformCallCenter != null)
            {
                if (extractData.PlatformCallCenter != null)
                {
                    listPlatformCallcenter.DataTextField = "Name";
                    listPlatformCallcenter.DataValueField = "Value";
                    listPlatformCallcenter.DataSource = extractData.PlatformCallCenter;
                    listPlatformCallcenter.DataBind();
                }
            }

            if (extractData.OutletRegisterName != null)
            {
                if (extractData.OutletRegisterName != null)
                {
                    listOutletRegister.DataTextField = "Name";
                    listOutletRegister.DataValueField = "Value";
                    listOutletRegister.DataSource = extractData.OutletRegisterName;
                    listOutletRegister.DataBind();
                }
            }

            if (extractData.LastVisitOutlet != null)
            {
                if (extractData.LastVisitOutlet != null)
                {
                    listOutletLastVisit.DataTextField = "Name";
                    listOutletLastVisit.DataValueField = "Value";
                    listOutletLastVisit.DataSource = extractData.LastVisitOutlet;
                    listOutletLastVisit.DataBind();
                }
            }

            ISVisibleOutletRegister();
            ISVisibleLastVisitOutlet();
            ISVisiblePlatform();
            ISVisiblePlatformCalcenter();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnChooseOutletRegister_Click(object sender, EventArgs e)
    {
        try
        {
            string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + "/DIAGEO.WEBSITE/ExtractData/OutletPopup.aspx?ComeFrom=Register&OutletCode=" + lblOutlet.Text + "','mywindow','dialogWidth:1000px; dialogHeight:550px; center:yes; scroll:yes'); document.getElementById('btnRefresh').click();";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", window, true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnChooseOutletLastVisit_Click(object sender, EventArgs e)
    {
        try
        {
            string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + "/DIAGEO.WEBSITE/ExtractData/OutletPopup.aspx?ComeFrom=LastVisit&OutletCode=" + lblOutlet.Text + "','mywindow','dialogWidth:1000px; dialogHeight:550px; center:yes; scroll:yes'); document.getElementById('btnRefresh').click();";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", window, true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnChooseSubscribe_Click(object sender, EventArgs e)
    {
        try
        {
            string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + "/DIAGEO.WEBSITE/ExtractData/PlatFormPopup.aspx?ComeFrom=CallCenter&PlatformCode=" + lblPlatform.Text + "','mywindow','dialogWidth:1000px; dialogHeight:250px; center:yes; scroll:yes'); document.getElementById('btnRefresh').click();";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", window, true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnExportDataInvisible_Click(object sender, EventArgs e)
    {
        ConsumerBLL consumerBLL;
        System.Data.DataTable dtConsumer = null;
        string platform = "";
        string registerOutlet = "";
        string lastVisitOutlet = "";
        string subscribePlatform = "";

        System.Globalization.CultureInfo oldCulture;

        try
        {
            consumerBLL = new ConsumerBLL();
            foreach (ListItem item in listPlatform.Items)
            {
                platform = platform + "'" + item.Value + "'" + ",";
            }

            if (platform != "")
                platform = platform.Remove(platform.Length - 1);

            foreach (ListItem item in listOutletRegister.Items)
            {
                registerOutlet = registerOutlet + item.Value + ",";
            }

            foreach (ListItem item in listOutletLastVisit.Items)
            {
                lastVisitOutlet = lastVisitOutlet + item.Value + ",";
            }

            if (registerOutlet != "")
                registerOutlet = registerOutlet.Remove(registerOutlet.Length - 1);

            foreach (ListItem item in listPlatformCallcenter.Items)
            {
                subscribePlatform = subscribePlatform + item.Value + ",";
            }

            if (subscribePlatform != "")
                subscribePlatform = subscribePlatform.Remove(subscribePlatform.Length - 1);

            decimal round = Convert.ToDecimal(RecordOFExport) / Convert.ToDecimal(10000);
            round = decimal.Ceiling(round);

            if (rdoExport.SelectedIndex == 0)
            {
                #region Excel

                oldCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);

                using (StreamWriter x = new StreamWriter(Server.MapPath("~//FileUpload//ExtractData//") + "ExtractData.xls"))
                {
                    int sheetNumber = 0;
                    x.Write("<?xml version=\"1.0\"?><?mso-application progid=\"Excel.Sheet\"?>");
                    x.Write("<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\" ");
                    x.Write("xmlns:o=\"urn:schemas-microsoft-com:office:office\" ");
                    x.Write("xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
                    x.Write("<Styles><Style ss:ID='sText'>" +
                               "<NumberFormat ss:Format='@'/></Style>");
                    x.Write("<Style ss:ID='sDate'><NumberFormat" +
                               " ss:Format='[$-409]m/d/yy\\ h:mm\\ AM/PM;@'/>");
                    x.Write("</Style></Styles>");

                    DataSet dsExcel = new DataSet();
                    DataTable dtConsumerFirst;

                    dtConsumerFirst = consumerBLL.ExtractData(platform, chkBaileys.Checked, chkBenmore.Checked, chkBenmoreJwBlack.Checked, chkJwGold.Checked,
                             chkJwGreen.Checked, chkJwRed.Checked, chkSmirnoff.Checked, chkAbsolute.Checked, chkBallentine.Checked, chkBlend.Checked,
                             chkChivas.Checked, chkDewar.Checked, chk100Pipers.Checked, chk100Pipers8yrs.Checked,
                             BaileysListCondition, FourABenmoreListCondition, JwBlackListCondition, JwGoldListCondition, JwGreenListCondition,
                             JwRedListCondition, SmimoffListCondition, AbsoluteListCondition, BallentineListCondition, BlendListCondition,
                             ChivasListCondition, DewarListCondition, FourA100PipersListCondition, FourA100Pipers8yrsListCondition, rdoGender.SelectedValue, txtAgeFrom.Text, txtAgeTo.Text, ConvertDatetimeFromDatepicker(txtDateOfBirthFrom.Value), ConvertDatetimeFromDatepicker(txtDateOfBirthTo.Value),
                    ddlProvince.SelectedValue, registerOutlet, ConvertDatetimeFromDatepicker(txtRegisterDateFrom.Value), ConvertDatetimeFromDatepicker(txtRegisterDateTo.Value), lastVisitOutlet,
                    ConvertDatetimeFromDatepicker(txtLastVisitDateFrom.Value), ConvertDatetimeFromDatepicker(txtLastVisitDateTo.Value), chkEmail.Checked, chkMobile.Checked, chkCallcenter.Checked, subscribePlatform, 0, 0, 10000);

                    sheetNumber++;
                    string sheetName = !string.IsNullOrEmpty(dtConsumerFirst.TableName) ?
                           dtConsumerFirst.TableName : "Sheet" + sheetNumber.ToString();
                    x.Write("<Worksheet ss:Name='" + sheetName + "'>");
                    x.Write("<Table>");
                    string[] columnTypes = new string[dtConsumerFirst.Columns.Count];

                    for (int i = 0; i < dtConsumerFirst.Columns.Count; i++)
                    {
                        string colType = dtConsumerFirst.Columns[i].DataType.ToString().ToLower();

                        if (colType.Contains("datetime"))
                        {
                            columnTypes[i] = "DateTime";
                            x.Write("<Column ss:StyleID='sDate'/>");

                        }
                        else if (colType.Contains("string"))
                        {
                            columnTypes[i] = "String";
                            x.Write("<Column ss:StyleID='sText'/>");

                        }
                        else
                        {
                            x.Write("<Column />");

                            if (colType.Contains("boolean"))
                            {
                                columnTypes[i] = "Boolean";
                            }
                            else
                            {
                                //default is some kind of number.
                                columnTypes[i] = "Number";
                            }

                        }
                    }
                    //column headers
                    x.Write("<Row>");
                    foreach (DataColumn col in dtConsumerFirst.Columns)
                    {
                        x.Write("<Cell ss:StyleID='sText'><Data ss:Type='String'>");
                        x.Write(col.ColumnName);
                        x.Write("</Data></Cell>");
                    }
                    x.Write("</Row>");

                    //data

                    for (int j = 1; j <= round; j++)
                    {
                        dsExcel.Tables.Clear();
                        if (j == 1)
                        {
                            dtConsumer = consumerBLL.ExtractData(platform, chkBaileys.Checked, chkBenmore.Checked, chkBenmoreJwBlack.Checked, chkJwGold.Checked,
                             chkJwGreen.Checked, chkJwRed.Checked, chkSmirnoff.Checked, chkAbsolute.Checked, chkBallentine.Checked, chkBlend.Checked,
                             chkChivas.Checked, chkDewar.Checked, chk100Pipers.Checked, chk100Pipers8yrs.Checked,
                             BaileysListCondition, FourABenmoreListCondition, JwBlackListCondition, JwGoldListCondition, JwGreenListCondition,
                             JwRedListCondition, SmimoffListCondition, AbsoluteListCondition, BallentineListCondition, BlendListCondition,
                             ChivasListCondition, DewarListCondition, FourA100PipersListCondition, FourA100Pipers8yrsListCondition, rdoGender.SelectedValue, txtAgeFrom.Text, txtAgeTo.Text, ConvertDatetimeFromDatepicker(txtDateOfBirthFrom.Value), ConvertDatetimeFromDatepicker(txtDateOfBirthTo.Value),
                            ddlProvince.SelectedValue, registerOutlet, ConvertDatetimeFromDatepicker(txtRegisterDateFrom.Value), ConvertDatetimeFromDatepicker(txtRegisterDateTo.Value), lastVisitOutlet,
                            ConvertDatetimeFromDatepicker(txtLastVisitDateFrom.Value), ConvertDatetimeFromDatepicker(txtLastVisitDateTo.Value), chkEmail.Checked, chkMobile.Checked, chkCallcenter.Checked, subscribePlatform, 0, 0, 10000);
                        }
                        else
                        {
                            dtConsumer = consumerBLL.ExtractData(platform, chkBaileys.Checked, chkBenmore.Checked, chkBenmoreJwBlack.Checked, chkJwGold.Checked,
                             chkJwGreen.Checked, chkJwRed.Checked, chkSmirnoff.Checked, chkAbsolute.Checked, chkBallentine.Checked, chkBlend.Checked,
                             chkChivas.Checked, chkDewar.Checked, chk100Pipers.Checked, chk100Pipers8yrs.Checked,
                             BaileysListCondition, FourABenmoreListCondition, JwBlackListCondition, JwGoldListCondition, JwGreenListCondition,
                             JwRedListCondition, SmimoffListCondition, AbsoluteListCondition, BallentineListCondition, BlendListCondition,
                             ChivasListCondition, DewarListCondition, FourA100PipersListCondition, FourA100Pipers8yrsListCondition, rdoGender.SelectedValue, txtAgeFrom.Text, txtAgeTo.Text, ConvertDatetimeFromDatepicker(txtDateOfBirthFrom.Value), ConvertDatetimeFromDatepicker(txtDateOfBirthTo.Value),
                            ddlProvince.SelectedValue, registerOutlet, ConvertDatetimeFromDatepicker(txtRegisterDateFrom.Value), ConvertDatetimeFromDatepicker(txtRegisterDateTo.Value), lastVisitOutlet,
                            ConvertDatetimeFromDatepicker(txtLastVisitDateFrom.Value), ConvertDatetimeFromDatepicker(txtLastVisitDateTo.Value), chkEmail.Checked, chkMobile.Checked, chkCallcenter.Checked, subscribePlatform, 0, (j - 1) * 10000, 10000);
                        }

                        dsExcel.Tables.Add(dtConsumer);
                        bool missedNullColumn = false;
                        foreach (DataRow row in dsExcel.Tables[0].Rows)
                        {
                            x.Write("<Row>");
                            for (int i = 0; i < dsExcel.Tables[0].Columns.Count; i++)
                            {
                                if (!row.IsNull(i))
                                {
                                    if (missedNullColumn)
                                    {
                                        int displayIndex = i + 1;
                                        x.Write("<Cell ss:Index='" + displayIndex.ToString() +
                                                   "'><Data ss:Type='" +
                                                   columnTypes[i] + "'>");
                                        missedNullColumn = false;
                                    }
                                    else
                                    {
                                        x.Write("<Cell><Data ss:Type='" +
                                                   columnTypes[i] + "'>");
                                    }

                                    switch (columnTypes[i])
                                    {
                                        case "DateTime":
                                            x.Write(((DateTime)row[i]).ToString("s"));
                                            break;
                                        case "Boolean":
                                            x.Write(((bool)row[i]) ? "1" : "0");
                                            break;
                                        case "String":
                                            x.Write(row[i].ToString());
                                            break;
                                        default:
                                            x.Write(row[i].ToString());
                                            break;
                                    }

                                    x.Write("</Data></Cell>");
                                }
                                else
                                {
                                    missedNullColumn = true;
                                }
                            }
                            x.Write("</Row>");
                        }
                    }

                    x.Write("</Table></Worksheet>");
                    x.Write("</Workbook>");
                    x.Close();
                    Thread.CurrentThread.CurrentCulture = oldCulture;
                }

                #endregion

                FileStreamDownload(Server.MapPath("~//FileUpload//ExtractData//") + "ExtractData.xls", "ExtractData.xls", true);
            }

            if (rdoExport.SelectedIndex == 1)
            {
                #region CSV

                for (int i = 1; i <= round; i++)
                {
                    if (i == 1)
                    {
                        dtConsumer = consumerBLL.ExtractData(platform, chkBaileys.Checked, chkBenmore.Checked, chkBenmoreJwBlack.Checked, chkJwGold.Checked,
                             chkJwGreen.Checked, chkJwRed.Checked, chkSmirnoff.Checked, chkAbsolute.Checked, chkBallentine.Checked, chkBlend.Checked,
                             chkChivas.Checked, chkDewar.Checked, chk100Pipers.Checked, chk100Pipers8yrs.Checked,
                             BaileysListCondition, FourABenmoreListCondition, JwBlackListCondition, JwGoldListCondition, JwGreenListCondition,
                             JwRedListCondition, SmimoffListCondition, AbsoluteListCondition, BallentineListCondition, BlendListCondition,
                             ChivasListCondition, DewarListCondition, FourA100PipersListCondition, FourA100Pipers8yrsListCondition, rdoGender.SelectedValue, txtAgeFrom.Text, txtAgeTo.Text, ConvertDatetimeFromDatepicker(txtDateOfBirthFrom.Value), ConvertDatetimeFromDatepicker(txtDateOfBirthTo.Value),
                        ddlProvince.SelectedValue, registerOutlet, ConvertDatetimeFromDatepicker(txtRegisterDateFrom.Value), ConvertDatetimeFromDatepicker(txtRegisterDateTo.Value), lastVisitOutlet,
                        ConvertDatetimeFromDatepicker(txtLastVisitDateFrom.Value), ConvertDatetimeFromDatepicker(txtLastVisitDateTo.Value), chkEmail.Checked, chkMobile.Checked, chkCallcenter.Checked, subscribePlatform, 0, 0, 10000);
                    }
                    else
                    {
                        dtConsumer = consumerBLL.ExtractData(platform, chkBaileys.Checked, chkBenmore.Checked, chkBenmoreJwBlack.Checked, chkJwGold.Checked,
                             chkJwGreen.Checked, chkJwRed.Checked, chkSmirnoff.Checked, chkAbsolute.Checked, chkBallentine.Checked, chkBlend.Checked,
                             chkChivas.Checked, chkDewar.Checked, chk100Pipers.Checked, chk100Pipers8yrs.Checked,
                             BaileysListCondition, FourABenmoreListCondition, JwBlackListCondition, JwGoldListCondition, JwGreenListCondition,
                             JwRedListCondition, SmimoffListCondition, AbsoluteListCondition, BallentineListCondition, BlendListCondition,
                             ChivasListCondition, DewarListCondition, FourA100PipersListCondition, FourA100Pipers8yrsListCondition, rdoGender.SelectedValue, txtAgeFrom.Text, txtAgeTo.Text, ConvertDatetimeFromDatepicker(txtDateOfBirthFrom.Value), ConvertDatetimeFromDatepicker(txtDateOfBirthTo.Value),
                        ddlProvince.SelectedValue, registerOutlet, ConvertDatetimeFromDatepicker(txtRegisterDateFrom.Value), ConvertDatetimeFromDatepicker(txtRegisterDateTo.Value), lastVisitOutlet,
                        ConvertDatetimeFromDatepicker(txtLastVisitDateFrom.Value), ConvertDatetimeFromDatepicker(txtLastVisitDateTo.Value), chkEmail.Checked, chkMobile.Checked, chkCallcenter.Checked, subscribePlatform, 0, (i - 1) * 10000, 10000);
                    }

                    DataSet ds = new DataSet();
                    ds.Tables.Add(dtConsumer);
                    ExportDataTableToCSVTransaction(ds, Server.MapPath("~//FileUpload//ExtractData//"));
                }

                #endregion

                FileStreamDownload(Server.MapPath("~//FileUpload//ExtractData//") + "ExtractData.csv", "ExtractData.csv", true);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void ISVisiblePlatform()
    {
        try
        {
            if (listPlatform.Items.Count == 0)
                listPlatform.Visible = false;
            else
                listPlatform.Visible = true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void ISVisiblePlatformCalcenter()
    {
        try
        {
            if (listPlatformCallcenter.Items.Count == 0)
                listPlatformCallcenter.Visible = false;
            else
                listPlatformCallcenter.Visible = true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void ISVisibleOutletRegister()
    {
        try
        {
            if (listOutletRegister.Items.Count == 0)
                listOutletRegister.Visible = false;
            else
                listOutletRegister.Visible = true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void ISVisibleLastVisitOutlet()
    {
        try
        {
            if (listOutletLastVisit.Items.Count == 0)
                listOutletLastVisit.Visible = false;
            else
                listOutletLastVisit.Visible = true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void BindFourAType()
    {
        DataRow dr;
        System.Data.DataTable dt;

        try
        {
            dt = new System.Data.DataTable();
            dt.Columns.Add("Name");
            dt.Columns.Add("Value");

            dr = dt.NewRow();
            dr["Name"] = "Please Select";
            dr["Value"] = "";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Name"] = "Adorers";
            dr["Value"] = "1";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Name"] = "Adopters";
            dr["Value"] = "2";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Name"] = "Acceptors";
            dr["Value"] = "3";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Name"] = "Availables";
            dr["Value"] = "4";
            dt.Rows.Add(dr);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void ExportDataTableToExcel(IEnumerable tables, string fileName)
    {
        using (StreamWriter x = new StreamWriter(fileName))
        {
            int sheetNumber = 0;
            x.Write("<?xml version=\"1.0\"?><?mso-application progid=\"Excel.Sheet\"?>");
            x.Write("<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\" ");
            x.Write("xmlns:o=\"urn:schemas-microsoft-com:office:office\" ");
            x.Write("xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
            x.Write("<Styles><Style ss:ID='sText'>" +
                       "<NumberFormat ss:Format='@'/></Style>");
            x.Write("<Style ss:ID='sDate'><NumberFormat" +
                       " ss:Format='[$-409]m/d/yy\\ h:mm\\ AM/PM;@'/>");
            x.Write("</Style></Styles>");
            foreach (DataTable dt in tables)
            {
                sheetNumber++;
                string sheetName = !string.IsNullOrEmpty(dt.TableName) ?
                       dt.TableName : "Sheet" + sheetNumber.ToString();
                x.Write("<Worksheet ss:Name='" + sheetName + "'>");
                x.Write("<Table>");
                string[] columnTypes = new string[dt.Columns.Count];

                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    string colType = dt.Columns[i].DataType.ToString().ToLower();

                    if (colType.Contains("datetime"))
                    {
                        columnTypes[i] = "DateTime";
                        x.Write("<Column ss:StyleID='sDate'/>");

                    }
                    else if (colType.Contains("string"))
                    {
                        columnTypes[i] = "String";
                        x.Write("<Column ss:StyleID='sText'/>");

                    }
                    else
                    {
                        x.Write("<Column />");

                        if (colType.Contains("boolean"))
                        {
                            columnTypes[i] = "Boolean";
                        }
                        else
                        {
                            //default is some kind of number.
                            columnTypes[i] = "Number";
                        }

                    }
                }
                //column headers
                x.Write("<Row>");
                foreach (DataColumn col in dt.Columns)
                {
                    x.Write("<Cell ss:StyleID='sText'><Data ss:Type='String'>");
                    x.Write(col.ColumnName);
                    x.Write("</Data></Cell>");
                }
                x.Write("</Row>");
                //data
                bool missedNullColumn = false;
                foreach (DataRow row in dt.Rows)
                {
                    x.Write("<Row>");
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (!row.IsNull(i))
                        {
                            if (missedNullColumn)
                            {
                                int displayIndex = i + 1;
                                x.Write("<Cell ss:Index='" + displayIndex.ToString() +
                                           "'><Data ss:Type='" +
                                           columnTypes[i] + "'>");
                                missedNullColumn = false;
                            }
                            else
                            {
                                x.Write("<Cell><Data ss:Type='" +
                                           columnTypes[i] + "'>");
                            }

                            switch (columnTypes[i])
                            {
                                case "DateTime":
                                    x.Write(((DateTime)row[i]).ToString("s"));
                                    break;
                                case "Boolean":
                                    x.Write(((bool)row[i]) ? "1" : "0");
                                    break;
                                case "String":
                                    x.Write(row[i].ToString());
                                    break;
                                default:
                                    x.Write(row[i].ToString());
                                    break;
                            }

                            x.Write("</Data></Cell>");
                        }
                        else
                        {
                            missedNullColumn = true;
                        }
                    }
                    x.Write("</Row>");
                }
                x.Write("</Table></Worksheet>");
            }
            x.Write("</Workbook>");
            x.Close();
        }
    }

    public void FileStreamDownload(string filePath, string newFileNameDownload, bool deleteFile)
    {
        try
        {
            FileStream liveStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[liveStream.Length + 1];
            liveStream.Read(buffer, 0, (int)liveStream.Length);
            liveStream.Close();
            HttpContext.Current.Response.Clear();

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", buffer.Length.ToString());
            Response.AddHeader("Content-Disposition", "attachment; filename=" + newFileNameDownload);
            Response.BinaryWrite(buffer);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            throw new Exception("FileStreamDownload :: " + ex.Message);
        }
        finally
        {
            if (deleteFile)
                File.Delete(filePath);
        }
    }

    public void CreateConditionCheckBoxList(CheckBoxList chkList)
    {
        try
        {
            string chkBaileysListCondition = "";
            string chkFourABenmoreListCondition = "";
            string chkJwBlackListCondition = "";
            string chkJwGoldListCondition = "";
            string chkJwGreenListCondition = "";
            string chkJwRedListCondition = "";
            string chkSmirnoffListCondition = "";
            string chkAbsoluteListCondition = "";
            string chkBallentineListCondition = "";
            string chkBlendListCondition = "";
            string chkChivasListCondition = "";
            string chkDewarListCondition = "";
            string chk100PipersListCondition = "";
            string chk100Pipers8yrsListCondition = "";

            if (chkList.ID == "chkBaileysList")
            {
                foreach (ListItem item in chkBaileysList.Items)
                {
                    if (item.Selected == true)
                    {
                        chkBaileysListCondition += "'" + item.Value + "',";
                    }
                }
            }

            if (chkList.ID == "chkFourABenmoreList")
            {
                foreach (ListItem item in chkFourABenmoreList.Items)
                {
                    if (item.Selected == true)
                    {
                        chkFourABenmoreListCondition += "'" + item.Value + "',";
                    }
                }
            }

            if (chkList.ID == "chkJwBlackList")
            {
                foreach (ListItem item in chkJwBlackList.Items)
                {
                    if (item.Selected == true)
                    {
                        chkJwBlackListCondition += "'" + item.Value + "',";
                    }
                }
            }

            if (chkList.ID == "chkJwGoldList")
            {
                foreach (ListItem item in chkJwGoldList.Items)
                {
                    if (item.Selected == true)
                    {
                        chkJwGoldListCondition += "'" + item.Value + "',";
                    }
                }
            }

            if (chkList.ID == "chkJwGreenList")
            {
                foreach (ListItem item in chkJwGreenList.Items)
                {
                    if (item.Selected == true)
                    {
                        chkJwGreenListCondition += "'" + item.Value + "',";
                    }
                }
            }

            if (chkList.ID == "chkJwRedList")
            {
                foreach (ListItem item in chkJwRedList.Items)
                {
                    if (item.Selected == true)
                    {
                        chkJwRedListCondition += "'" + item.Value + "',";
                    }
                }
            }

            if (chkList.ID == "chkSmirnoffList")
            {
                foreach (ListItem item in chkSmirnoffList.Items)
                {
                    if (item.Selected == true)
                    {
                        chkSmirnoffListCondition += "'" + item.Value + "',";
                    }
                }
            }

            if (chkList.ID == "chkBallentinesList")
            {
                foreach (ListItem item in chkBallentineList.Items)
                {
                    if (item.Selected == true)
                    {
                        chkBallentineListCondition += "'" + item.Value + "',";
                    }
                }
            }

            if (chkList.ID == "chkSmirnoffList")
            {
                foreach (ListItem item in chkBlendList.Items)
                {
                    if (item.Selected == true)
                    {
                        chkBlendListCondition += "'" + item.Value + "',";
                    }
                }
            }

            if (chkList.ID == "chkChivasList")
            {
                foreach (ListItem item in chkChivasList.Items)
                {
                    if (item.Selected == true)
                    {
                        chkChivasListCondition += "'" + item.Value + "',";
                    }
                }
            }

            if (chkList.ID == "chkDewarList")
            {
                foreach (ListItem item in chkDewarList.Items)
                {
                    if (item.Selected == true)
                    {
                        chkDewarListCondition += "'" + item.Value + "',";
                    }
                }
            }

            if (chkList.ID == "chk100PipersList")
            {
                foreach (ListItem item in chk100PipersList.Items)
                {
                    if (item.Selected == true)
                    {
                        chk100PipersListCondition += "'" + item.Value + "',";
                    }
                }
            }

            if (chkList.ID == "chk100Pipers8yrsList")
            {
                foreach (ListItem item in chk100Pipers8yrsList.Items)
                {
                    if (item.Selected == true)
                    {
                        chk100Pipers8yrsListCondition += "'" + item.Value + "',";
                    }
                }
            }

            if (chkList.ID == "chkAbsoluteList")
            {
                foreach (ListItem item in chkAbsoluteList.Items)
                {
                    if (item.Selected == true)
                    {
                        chkAbsoluteListCondition += "'" + item.Value + "',";
                    }
                }
            }

            if (chkBaileysListCondition != "")
                BaileysListCondition = chkBaileysListCondition.Remove(chkBaileysListCondition.Length - 1, 1);

            if (chkFourABenmoreListCondition != "")
                FourABenmoreListCondition = chkFourABenmoreListCondition.Remove(chkFourABenmoreListCondition.Length - 1, 1);

            if (chkJwBlackListCondition != "")
                JwBlackListCondition = chkJwBlackListCondition.Remove(chkJwBlackListCondition.Length - 1, 1);

            if (chkJwGoldListCondition != "")
                JwGoldListCondition = chkJwGoldListCondition.Remove(chkJwGoldListCondition.Length - 1, 1);

            if (chkJwGreenListCondition != "")
                JwGreenListCondition = chkJwGreenListCondition.Remove(chkJwGreenListCondition.Length - 1, 1);

            if (chkJwRedListCondition != "")
                JwRedListCondition = chkJwRedListCondition.Remove(chkJwRedListCondition.Length - 1, 1);

            if (chkSmirnoffListCondition != "")
                SmimoffListCondition = chkSmirnoffListCondition.Remove(chkSmirnoffListCondition.Length - 1, 1);

            if (chkBallentineListCondition != "")
                BallentineListCondition = chkBallentineListCondition.Remove(chkBallentineListCondition.Length - 1, 1);

            if (chkBlendListCondition != "")
                BlendListCondition = chkBlendListCondition.Remove(chkBlendListCondition.Length - 1, 1);

            if (chkChivasListCondition != "")
                ChivasListCondition = chkChivasListCondition.Remove(chkChivasListCondition.Length - 1, 1);

            if (chkDewarListCondition != "")
                DewarListCondition = chkDewarListCondition.Remove(chkDewarListCondition.Length - 1, 1);

            if (chk100PipersListCondition != "")
                FourA100PipersListCondition = chk100PipersListCondition.Remove(chk100PipersListCondition.Length - 1, 1);

            if (chk100Pipers8yrsListCondition != "")
                FourA100Pipers8yrsListCondition = chk100Pipers8yrsListCondition.Remove(chk100Pipers8yrsListCondition.Length - 1, 1);

            if (chkAbsoluteListCondition != "")
                AbsoluteListCondition = chkAbsoluteListCondition.Remove(chkAbsoluteListCondition.Length - 1, 1);

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

}