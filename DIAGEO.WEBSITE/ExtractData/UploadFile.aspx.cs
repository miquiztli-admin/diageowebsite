﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using DIAGEO.BLL;
using System.Collections;

public partial class ExtractData_UploadFile : BasePage
{
    #region Variable

    private string _rootPath;
    private string _rootDirect;
    private string _dateAndFileName;
    private ArrayList _arrID;
    private ArrayList _arrCode;
    private ArrayList _arrData;
    private ArrayList _arrOutlet;
    private ArrayList _arrResultCode;

    private string RowPerPage = ConfigurationSettings.AppSettings["RowPerPage"].ToString();

    #endregion

    #region Property

    public string RootPath
    {
        get { return _rootPath; }
        set { _rootPath = value; }
    }
    public string FileFullPath //RootDirect + FileName
    {
        get { return (string)ViewState["FileFullPath"]; }
        set { ViewState["FileFullPath"] = value; }
    }
    public string RootDirect
    {
        get { return (string)ViewState["RootDirect"]; }
        set { ViewState["RootDirect"] = value; }
    }
    public string DateAndFileName
    {
        get { return (string)ViewState["DateAndFileName"]; }
        set { ViewState["DateAndFileName"] = value; }
    }
    public ArrayList ArrID
    {
        get { return _arrID; }
        set { _arrID = value; }
    }
    public ArrayList ArrCode
    {
        get { return _arrCode; }
        set { _arrCode = value; }
    }
    public ArrayList ArrData
    {
        get { return _arrData; }
        set { _arrData = value; }
    }
    public ArrayList ArrOutlet
    {
        get { return _arrOutlet; }
        set { _arrOutlet = value; }
    }
    public ArrayList ArrResultCode
    {
        get { return _arrResultCode; }
        set { _arrResultCode = value; }
    }
    public DataTable SessionExcel
    {
        get 
        {
            if (Session["SessionExcel"] != null)
                return (DataTable)Session["SessionExcel"];
            else
                return new DataTable("Excel");
        }
        set 
        { 
            if (value == null)
                Session.Remove("SessionExcel");
            else
                Session["SessionExcel"] = value; 
        }
    }
    public DataTable SessionCallCenterMapping
    {
        get
        {
            if (Session["SessionCallCenterMapping"] != null)
                return (DataTable)Session["SessionCallCenterMapping"];
            else
                return new DataTable("CallCenterMapping");
        }
        set
        {
            if (value == null)
                Session.Remove("SessionCallCenterMapping");
            else
                Session["SessionCallCenterMapping"] = value;
        }
    }

    #endregion

    #region Medhod

    private bool CheckFile(string FileName)
    {
        string FileType = "";
        try
        {
            FileType = FileName.Substring(FileName.IndexOf('.') + 1);
            if (FileType.ToLower().ToString().Equals("xls") || FileType.ToLower().ToString().Equals("xlsx"))
                return true;
            else
                return false;
        }
        catch (Exception ex)
        {
            throw new Exception("CheckFile :: " + ex.Message);
        }
    }

    private void GetResultReturn(string ResultID, string ResultSubID)
    {
        CallCenterBounceMappingBLL bllCallCenter = new CallCenterBounceMappingBLL();
        DataTable dtCallCenter = new DataTable("CallCenterBounceMapping");
        string PathResultRule = ConfigurationManager.AppSettings["PathResultRule"].ToString();
        bool checkHasData = false;
        try
        {
            if (ResultID.Length == 1)
                ResultID = "0" + ResultID;
            if (ResultSubID.Length == 1)
                ResultSubID = "0" + ResultSubID;

            //dtCallCenter = bllCallCenter.GetBounceStatus(ResultID + ResultSubID);
            //if (dtCallCenter.Rows.Count > 0)
            //{
            //    ArrResultCode.Add((ResultID + ResultSubID).ToString());
            //    ArrCode.Add(dtCallCenter.Rows[0]["BounceStatus"].ToString());
            //    if (dtCallCenter.Rows[0]["BounceStatus"].ToString().ToUpper().Equals("0"))
            //        ArrData.Add("SUCCESS");
            //    else
            //        ArrData.Add("FAIL");
            //}
            if (SessionCallCenterMapping == null)
                SessionCallCenterMapping = bllCallCenter.SelectAll();

            for (int i = 0; i < SessionCallCenterMapping.Rows.Count; i++)
            {
                if (SessionCallCenterMapping.Rows[i]["Code"].ToString().Equals(ResultID + ResultSubID))
                {
                    ArrResultCode.Add((ResultID + ResultSubID).ToString());
                    ArrCode.Add(SessionCallCenterMapping.Rows[i]["BounceStatus"].ToString());
                    if (SessionCallCenterMapping.Rows[i]["BounceStatus"].ToString().ToUpper().Equals("0"))
                        ArrData.Add("SUCCESS");
                    else
                        ArrData.Add("FAIL");
                    checkHasData = true;
                }
            }

            if (!checkHasData)
            {
                ArrID.RemoveAt(ArrID.Count - 1);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("GetResultReturn :: " + ex.Message);
        }
    }

    private void ClearPage()
    {
        try
        {
            pnDataGrid.Visible = false;
            btnUpload.Visible = false;
            SessionExcel = null;

            FileFullPath = null;
            RootDirect = null;
            DateAndFileName = null;

            lblCatchMessage.Text = "";
        }
        catch (Exception ex)
        {
            throw new Exception("ClearPage :: " + ex.Message);
        }
    }

    private bool ValidateResultIDAndResultSubID(string ResultID, string ResultSubID)
    {
        try
        {
            if (SessionCallCenterMapping != null)
            {
                for (int i = 0; i < SessionCallCenterMapping.Rows.Count; i++)
                    if (SessionCallCenterMapping.Rows[i]["Code"].ToString().Equals(ResultID + ResultSubID))
                        return false;
            }
            return true;
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateResultIDAndResultSubID :: " + ex.Message);
        }
    }

    #endregion

    #region Event
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            { }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UploadFile.Page_Load" + ex.Message);
        }
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        RootPath = Server.MapPath("~\\").ToString();
        string PathUpload = ConfigurationManager.AppSettings["PathFileUpload"].ToString();
        string FileName = Path.GetFileName(FileUpload.FileName);
        FileFullPath = "";
        string Date = DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");

        ArrayList IndexEmpty = new ArrayList();
        CallCenterBounceMappingBLL bllCallCenter = new CallCenterBounceMappingBLL();
        OutboundBLL bllOutbound = new OutboundBLL();
        DataTable dtExcel = new DataTable("ExcelData");

        try
        {
            if (FileUpload.HasFile)
            {
                if (CheckFile(FileName))
                {
                    if (RootDirect != null && DateAndFileName != null)
                        CopyBackupAndDeleteExcelFile(RootDirect, DateAndFileName, false, true);

                    RootDirect = RootPath + PathUpload;
                    DateAndFileName = Date + "_" + FileName; 
                    FileFullPath = RootDirect + DateAndFileName;

                    FileUpload.SaveAs(FileFullPath);
                    dtExcel = ReadData(FileFullPath, "DWDATA");

                    if (dtExcel.Rows.Count > 0)
                    {
                        dtExcel.Columns.Add("No");
                        for (int i = 0; i < dtExcel.Rows.Count; i++)
                        {
                            dtExcel.Rows[i]["No"] = i + 1;
                        }

                        for (int i = dtExcel.Rows.Count; i > 0; i--)
                        {
                            if (dtExcel.Rows[i - 1][CallCenterDataID].ToString() == "" && dtExcel.Rows[i - 1][CallCenterDataSubID].ToString() == ""
                                && dtExcel.Rows[i - 1][CallCenterID].ToString() == "" && dtExcel.Rows[i - 1][CallCenterDataStatus].ToString() == "")
                                dtExcel.Rows.RemoveAt(i - 1);
                        }

                        dtExcel.Columns["No"].SetOrdinal(0);
                        lblRowCount.Text = dtExcel.Rows.Count.ToString() + " เรคคอร์ด ";

                        SessionExcel = dtExcel;
                        SessionCallCenterMapping = bllCallCenter.SelectAll();

                        dgResultData.PageSize = int.Parse(RowPerPage);
                        dgResultData.DataSource = dtExcel;
                        dgResultData.DataBind();

                        pnDataGrid.Visible = true;
                        btnUpload.Visible = true;
                        btnUpload.Enabled = true;

                        ValidateNullExcel(dtExcel);
                    }
                }
                else
                    JsClientAlert("FileNotExcel", "ไฟล์ผิดชนิด ต้องอัพโหลดไฟล์ excel เท่านั้น");
            }
            else
                JsClientAlert("FileNotFound", "กรุณาเลือกไฟล์ที่ต้องการอัพโหลด");
        }
        catch (Exception ex)
        {
            lblCatchMessage.Text = ex.Message;
            JsClientAlert("ErrorMessage", "UploadFile.btnView_Click :: " + ex.Message);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            ClearPage();
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UploadFile.btnClear_Click :: " + ex.Message);
        }
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        OutboundBLL bllOutbound = new OutboundBLL();
        DIAGEO.COMMON.User User = new DIAGEO.COMMON.User();
        DataTable dtExcel = new DataTable("ExcelData");
        bool CheckUpdate = false;

        ArrID = new ArrayList();
        ArrData = new ArrayList();
        ArrCode = new ArrayList();
        ArrOutlet = new ArrayList();
        ArrResultCode = new ArrayList();

        try
        {
            //FileUpload.SaveAs(FileFullPath);
            //dtExcel = ReadData(FileFullPath, "DWDATA");
            dtExcel = SessionExcel;
            //DataRow[] drCol = dtExcel.Select("Outbound <> ''");
            if (dtExcel.Rows.Count > 0)
            {
                foreach (DataRow item in dtExcel.Rows)
                {
                    if (item[CallCenterID].ToString() != "")
                    {
                        ArrID.Add(item[CallCenterID].ToString());
                        ArrOutlet.Add(item[CallCenterOutlet].ToString());
                        GetResultReturn(item[CallCenterDataID].ToString(), item[CallCenterDataSubID].ToString());
                    }
                }
                if (ArrID.Count > 0)
                    CheckUpdate = bllOutbound.UpdateResultData(ArrID, ArrData, ArrResultCode, ArrCode, ArrOutlet, User.UserID.ToString());
            }
            CopyBackupAndDeleteExcelFile(RootDirect, DateAndFileName, true, true);
            if (CheckUpdate)
                JsClientAlert("UploadFileSuccess", "อัพโหลดไฟล์สำเร็จ จำนวน " + SessionExcel.Rows.Count.ToString() + " รายการ");

            ClearPage();
        }
        catch (Exception ex)
        {
            lblCatchMessage.Text = ex.Message;
            JsClientAlert("ErrorMessage", "UploadFile.btnUpload_Click :: " + ex.Message);
        }
        finally
        {
            User.Dispose();
            dtExcel.Dispose();
        }
    }

    protected void dgResultData_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {
        DataTable dtExcel = new DataTable("ExcelData");

        try
        {
            dgResultData.CurrentPageIndex = e.NewPageIndex;

            dgResultData.DataSource = SessionExcel;
            dgResultData.DataBind();
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UploadFile.dgResultData_PageIndexChanged :: " + ex.Message);
        }
    }
    #endregion

    protected void dgResultData_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (e.Item.Cells[1].Text.Trim() == "" || e.Item.Cells[9].Text.Trim() == "" || e.Item.Cells[10].Text.Trim() == "" || e.Item.Cells[11].Text.Trim() == "")
                {
                    e.Item.BackColor = System.Drawing.Color.Red;
                }

                if (e.Item.Cells[1].Text.Trim() == "&nbsp;" || e.Item.Cells[9].Text.Trim() == "&nbsp;" || e.Item.Cells[10].Text.Trim() == "&nbsp;" || e.Item.Cells[11].Text.Trim() == "&nbsp;")
                {
                    e.Item.BackColor = System.Drawing.Color.Red;
                }

                if (ValidateResultIDAndResultSubID(e.Item.Cells[9].Text.Trim(), e.Item.Cells[10].Text.Trim()))
                {
                    e.Item.BackColor = System.Drawing.Color.Red;
                }
            }
        }
        catch (Exception ex)
        {            
            throw new Exception(ex.Message);
        }
    }

    public bool ValidateNullExcel(DataTable dt)
    {
        try
        {
            int i = 0;
            foreach (DataRow item in dt.Rows)
            {
                if (item["RESULTSTATUS"].ToString() == "")
                {
                    i += 1;
                    btnUpload.Enabled = false;
                }
                else if (item["RESULTID"].ToString() == "")
                {
                    i += 1;
                    btnUpload.Enabled = false;
                }
                else if (item["RESULTSUBID"].ToString() == "")
                {
                    i += 1;
                    btnUpload.Enabled = false;
                }
                else if (item["OUTBOUND"].ToString() == "")
                {
                    i += 1;
                    btnUpload.Enabled = false;
                }
                else if (ValidateResultIDAndResultSubID(item["RESULTID"].ToString(), item["RESULTSUBID"].ToString()))
                {
                    i += 1;
                    btnUpload.Enabled = false;
                }
            }

            lblRowCount.Text += " จำนวนข้อมูลที่ผิดพลาด " + i.ToString() + " เรคคอร์ด";

            if (i != 0)
        		 return false;

            return true;
        }
        catch (Exception ex)
        {            
            throw new Exception(ex.Message);
        }
    }
}
