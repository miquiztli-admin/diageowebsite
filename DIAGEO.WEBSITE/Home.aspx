﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script type="text/javascript" src="Jquery/jquery-1.4.4.min.js"></script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="Style/StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-repeat: repeat-x">
    <form id="FrmHome" runat="server">
    <asp:ToolkitScriptManager ID="scriptManager" EnablePartialRendering="true" EnableScriptGlobalization="true" EnableScriptLocalization="true" runat="server"></asp:ToolkitScriptManager>
    <script type="text/javascript" language="javascript">
        var url;
        function ChangeIFrameLocation(loc, menuID) {
            document.getElementById('txtMenuID').value = menuID;
            document.getElementById('txtLoc').value = loc;
            document.getElementById('btnBindMenu').click();           
        }

        function ChangePageLogout(loc) {
            window.location = "Security/FrmLogin.aspx";
        }

        function ChangePage(loc) {
            window.open(loc);
        }

        function OnmouseOverMenu(link) {
            link.style.color = 'yellow';
        }

        function OnmouseOutMenuParent(link) {
            link.style.color = 'white';
        }

        function OnmouseOutMenu(link) {
            link.style.color = 'white';
        }

        function Logout() {
            window.location = "Login.aspx";
        }
    </script>

    <asp:UpdatePanel ID="upHome" runat="server">
        <ContentTemplate>
            <table width="1250px" bgcolor=black>
                <tr style="height:100px">
                    <td align=center width=200 
                        style="border-right-style: solid; border-right-width: 1px; border-right-color: #FFFFFF">                                           
                        <img src="Image/Logo-logon.png" />
                    </td>
                    <td class="ntextwhiteLarge">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DIAGEO Thailand – Consumer Data warehouse</td>
                    <td align="right" valign="top" align=right>
                        <asp:Label ID="lblWelcome" ForeColor="White" Font-Bold="true" runat="server" Text="Welcome , " CssClass="ntextwhite"></asp:Label>
                        <asp:Label ID="lblUserName" ForeColor="White" Font-Bold="True" runat="server" 
                            CssClass="ntextwhite" ></asp:Label>&nbsp; 
                        <font class=" ntextwhite">(</font>&nbsp;<a class="ntextwhite" href=Login.aspx>Logout</a>&nbsp;<font class="ntextwhite">)</font> &nbsp;<a class="ntextwhite"  href=ChangePassword.aspx target="iframeMain">ChangePassword</a>
                    </td>
                </tr>
            </table>
<%--            <table width="1250px" style="background-repeat: repeat-x; background-color:Silver">
                <tr>
                    <td>
                        &nbsp;&nbsp;<asp:Label ID="lblCustomerServiceInformationTitle" ForeColor="Black" Font-Bold="true" runat="server" Text="Data warehouse"></asp:Label>
                    </td>
                </tr>
            </table>--%>
            <table width="1250px" style="border: 1px; border-style: solid; background-color: White">
                <tr>
                    <td id="tdWorkFlowLeft" runat="server" class="ntextwhite" style="width:200px;vertical-align:top;
                        border:0px;border-style:solid; background-color:black">
                       <asp:MultiView ID="mvWorkFlowLeft" runat="server" ActiveViewIndex="0">
                            <asp:View ID="mvMenu" runat="server" oninit="mvMenu_Init">
                                <asp:Accordion ID="MyAccordion" runat="Server" SelectedIndex="-1" 
                                    HeaderSelectedCssClass="accordionHeaderSelected" ContentCssClass="accordionContent"
                                    RequireOpenedPane="false" SuppressHeaderPostbacks="true" Width="200px" >
                                    <Panes>
                                    </Panes>
                                    <HeaderTemplate>
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                    </ContentTemplate>
                                </asp:Accordion>
                            </asp:View>
                        </asp:MultiView>
                    </td>
                    <%--<td bgcolor="Black"  class="ntextwhite" valign=top width=200px height="2600">
                       <table style="border:none">
                        <tr>
                            <td><img src= "Image/Bullet1.png" /></td>
                            <td colspan=2 class="ntextwhite" valign=top class="ntextwhiteLarge"> Extract Data</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><img src= "Image/Bullet2.png" /></td>
                            <td><a onclick='ChangeIFrameLocation("ExtractData/ExtracConsumerMasterData.aspx","28")'>  Consumer master data (raw data)</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><img src= "Image/Bullet2.png" /></td>
                            <td><a onclick='ChangeIFrameLocation("ExtractData/ExtractCampaignSpecificData.aspx?","26")'> Campaign specific data</a></td>
                        </tr>
                        <tr bgcolor=white height=1px><td colspan=3 height=1px></td></tr>
                        <tr>
                            <td><img src= "Image/Bullet1.png" /></td>
                            <td colspan=2 class="ntextwhite" valign=top class="ntextwhiteLarge">Upload Data</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><img src= "Image/Bullet2.png" /></td>
                            <td><a onclick='ChangeIFrameLocation("ExtractData/UploadFile.aspx","5")'> Call Center - Upload Data</a></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><img src= "Image/Bullet2.png" /></td>
                            <td><a onclick='ChangeIFrameLocation("ExtractData/UploadCSVFromIPad.aspx","27")'>Smart touch App - Data Upload (CSV)</a></td>
                        </tr>
                        <tr bgcolor=white height=1px><td colspan=3></td></tr>                         
                        <tr>
                            <td><img src= "Image/Bullet1.png" /></td>
                            <td colspan=2 class="ntextwhite" valign=top class="ntextwhiteLarge">Report</td>

                        </tr>                       
                        <tr>
                            <td></td>
                            <td><img src= "Image/Bullet2.png" /></td>
                            <td><a onclick='ChangeIFrameLocation("Reports/DashboardReport.aspx","29")'>Dashboard Report</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><img src= "Image/Bullet2.png" /></td>
                            <td><a onclick='ChangeIFrameLocation("Reports/CommunicationReport.aspx","29")'>Communication Report</td>
                        </tr>
                           <tr>
                               <td>
                                   &nbsp;</td>
                               <td colspan="2">
                                   Staff Report</td>
                           </tr>
                           <!--tr>
                               <td>
                                   &nbsp;</td>
                               <td>
                                   <img src="Image/Bullet2.png" /></td>
                               <td>
                                   <a onclick='ChangeIFrameLocation("Reports/Report_DataEntryPerformanceDetail.aspx","30")'>Data Entry Performance Detail</td>
                           </tr-->
                           <tr>
                               <td>
                                   &nbsp;</td>
                               <td>
                                   <img src="Image/Bullet2.png" /></td>
                               <td>
                                   <a onclick='ChangeIFrameLocation("Reports/Report_ComparingDataEntryPerformance.aspx","31")'>Comparing Data Entry Performance</td>
                           </tr>
                           <tr>
                               <td>
                                   &nbsp;</td>
                               <td>
                                   <img src="Image/Bullet2.png" /></td>
                               <td>
                                   <a onclick='ChangeIFrameLocation("Reports/Report_TotalCommissionbyOutlet.aspx","32")'>Total Commission by Outlet</td>
                           </tr>
                           <tr>
                               <td>
                                   &nbsp;</td>
                               <td>
                                   <img src="Image/Bullet2.png" /></td>
                               <td>
                                   <a onclick='ChangeIFrameLocation("Reports/Report_TotalCommissionbyDataEntry.aspx","33")'>Total Commission by Data Entry</td>
                           </tr>
                           <tr>
                               <td>
                                   &nbsp;</td>
                               <td>
                                   <img src="Image/Bullet2.png" /></td>
                               <td>
                                   <a onclick='ChangeIFrameLocation("Reports/Report_DailyDataEntryPerformance.aspx","33")'>Daily Data Entry Performance</td>
                           </tr>
                        <tr bgcolor=white height=1px><td colspan=3></td></tr>                        
                        <tr>
                            <td><img src= "Image/Bullet1.png" /></td>
                            <td colspan=2 class="ntextwhite" valign=top class="ntextwhiteLarge">System configuration</td>

                        </tr>
                        <tr>
                            <td></td>
                            <td><img src= "Image/Bullet2.png" /></td>
                            <td>Users</td>
                        </tr>
                        <tr>
                            <td></td><td></td>
                            <td><img src= "Image/Bullet3.png" /><a onclick='ChangeIFrameLocation("Security/UserSearch.aspx","6")'> User Manager</a></td>
                        </tr>
                        <tr>
                            <td></td><td></td>
                            <td><img src= "Image/Bullet3.png" /><a onclick='ChangeIFrameLocation("Security/UserGroupSearch.aspx","7")'> Groups</a></td>
                        </tr>
                        <tr>
                            <td></td><td></td>
                            <td><img src= "Image/Bullet3.png" /><a onclick='ChangeIFrameLocation("Security/UserAccess.aspx","8")'> Access Level</a></td>
                        </tr>               
                        <tr>
                            <td></td>
                            <td><img src= "Image/Bullet2.png" /></td>
                            <td><a  onclick='ChangeIFrameLocation("Master/OutletSearch.aspx","14")'>Outlet</a></td>
                        </tr>                                 
                        <tr>
                            <td></td>
                            <td><img src= "Image/Bullet2.png" /></td>
                            <td><a onclick='ChangeIFrameLocation("Master/SurveyTypeSearch.aspx","15")'>4A</a></td>
                        </tr>          
                        <tr>
                            <td></td>
                            <td><img src= "Image/Bullet2.png" /></td>
                            <td><a onclick='ChangeIFrameLocation("Master/ProvinceSearch.aspx","16")' >Province</a></td>
                        </tr>                     
                        <tr>
                            <td></td>
                            <td><img src= "Image/Bullet2.png" /></td>
                            <td><a onclick='ChangeIFrameLocation("Master/PlatformSearch.aspx","17")'>Platform</a></td>
                        </tr>                     
                        <tr>
                            <td></td>
                            <td><img src= "Image/Bullet2.png" /></td>
                            <td><a onclick='ChangeIFrameLocation("Master/OutboundConfigSearch.aspx","20")'>Outbound Config</a></td>
                        </tr>                            
                        <tr>
                            <td></td>
                            <td><img src= "Image/Bullet2.png" /></td>
                            <td><a onclick='ChangeIFrameLocation("Master/ConsumerSearch.aspx","24")'>Consumer</a></td>
                        </tr>                     
                        <tr>
                            <td></td>
                            <td><img src= "Image/Bullet2.png" /></td>
                            <td><a onclick='ChangeIFrameLocation("Master/OutboundEventBirthDaySearch.aspx","25")'>Birthday</a></td>
                        </tr>                     
                        <tr>
                            <td></td>
                            <td><img src= "Image/Bullet2.png" /></td>
                            <td><a onclick='ChangeIFrameLocation("Master/OutboundEventSearch.aspx","19")'>Event</a></td>
                        </tr>                                         
                       </table>
                    </td>--%>
                    <td>
                        <iframe id="iframeMain" name="iframeMain" frameborder="0" scrolling="auto" style="width: 100%; height: 2550px"></iframe>
                    </td>
                </tr>
                <tr style="display:none">
                    <td>
                        <asp:Button ID="btnBindMenu" runat="server" onclick="btnBindMenu_Click" />
                        <asp:TextBox ID="txtMenuID" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtLoc" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <div align="right">
                <asp:Label ID="lblVersion" runat="server" color="gray" align="right" Text="version 1.1.0.0"></asp:Label>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>      
    </form>
</body>
</html>