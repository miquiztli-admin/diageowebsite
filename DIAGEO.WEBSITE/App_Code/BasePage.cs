﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.OleDb;
using System.IO;
//using Microsoft.Office.Interop.Excel;
using System.Threading;
using System.Globalization;
using System.Reflection;
using DIAGEO.BLL;
using System.Text;
using System.Text.RegularExpressions;

public class BasePage : System.Web.UI.Page
{

    public BasePage()
    {
    }

    #region Variable

    #endregion

    #region Property

    public int RowPerPage
    {
        get { return int.Parse(ViewState["RowPerPage"].ToString()); }
        set { ViewState["RowPerPage"] = value; }
    }
    public string CallCenterID
    {
        get { return ConfigurationManager.AppSettings["CallCenter_ID"].ToString(); }
    }
    public string CallCenterDataID
    {
        get { return ConfigurationManager.AppSettings["CallCenter_DataID"].ToString(); }
    }
    public string CallCenterDataSubID
    {
        get { return ConfigurationManager.AppSettings["CallCenter_DataSubID"].ToString(); }
    }
    public string CallCenterDataStatus
    {
        get { return ConfigurationManager.AppSettings["CallCenter_DataStatus"].ToString(); }
    }
    public string CallCenterOutlet
    {
        get { return ConfigurationManager.AppSettings["CallCenter_Outlet"].ToString(); }
    }
    public string SeparateSplit
    {
        get { return ConfigurationSettings.AppSettings["Separate"]; }
    }
    public string MainSplit
    {
        get { return ConfigurationSettings.AppSettings["MainSplit"]; }
    }
    public string SubSplit
    {
        get { return ConfigurationSettings.AppSettings["SubSplit"]; }
    }

    #endregion

    #region Property Permission
    public bool CanView { get; set; }
    public bool CanAdd { get; set; }
    public bool CanEdit { get; set; }
    public bool CanDelete { get; set; }
    public bool CanExport { get; set; }
    public bool CanPrint { get; set; }
    public string PermissionView { get; set; }
    public string PermissionAdd { get; set; }
    public string PermissionEdit { get; set; }
    public string PermissionDelete { get; set; }
    public string PermissionExport { get; set; }
    public string PermissionPrint { get; set; }
    #endregion

    #region Paging

    protected int GetCurrentPageIndex(int startGroupPage, int quantityPageShow, string linkButtonText)
    {
        if (linkButtonText.Trim().ToUpper().Equals(">>"))
            return startGroupPage + quantityPageShow;

        if (linkButtonText.Trim().ToUpper().Equals("<<"))
            return startGroupPage - 1;

        return int.Parse(linkButtonText);
    }

    #endregion

    #region Excel

    public System.Data.DataTable ReadData(string filelocation, string sheetName, string additionalColumn)
    {
        OleDbCommand excelCommand;
        OleDbConnection excelConn;
        OleDbDataAdapter excelDataAdapter;
        string query = "select * from [" + sheetName + "$]";

        try
        {
            if (!string.IsNullOrEmpty(additionalColumn))
                query = query.Insert(query.IndexOf('*') + 1, "," + additionalColumn);
            OleDbConnection con = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filelocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"");
            //OleDbConnection con = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filelocation + ";Extended Properties=Excel 12.0 Xml;");
            OleDbDataAdapter da = new OleDbDataAdapter(query, con);
            System.Data.DataTable dt = new System.Data.DataTable();
            da.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception("ReadData :: " + ex.Message);
        }
    }

    public System.Data.DataTable ReadData(string filelocation, string sheetName)
    {
        return ReadData(filelocation, sheetName, "");
    }

    //public bool ExportDataTableToExcel(System.Data.DataTable dt, string filename, string pathfile)
    //{
    //    Range cell;
    //    Workbook oWB;
    //    Range oRange;
    //    Application oXL;
    //    Worksheet oSheet;
    //    System.Globalization.CultureInfo oldCulture;

    //    try
    //    {
    //        oldCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
    //        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US", false);

    //        string directory = pathfile;
    //        if (!Directory.Exists(directory))
    //            Directory.CreateDirectory(directory);



    //        oXL = new Application();
    //        oXL.Visible = false;
    //        oXL.DisplayAlerts = false;

    //        oWB = oXL.Workbooks.Add(Missing.Value);
    //        oSheet = (Worksheet)oWB.ActiveSheet;
    //        oSheet.Name = "Data";

    //        int rowCount = 1;
    //        foreach (DataRow dr in dt.Rows)
    //        {
    //            rowCount += 1;
    //            for (int i = 1; i < dt.Columns.Count + 1; i++)
    //            {
    //                if (rowCount == 2)
    //                {
    //                    oSheet.Cells[1, i] = dt.Columns[i - 1].ColumnName;
    //                }
    //                oSheet.Cells[rowCount, i] = dr[i - 1].ToString();

    //                cell = oSheet.Cells[rowCount, i] as Range;
    //                const string textIndicator = "@";
    //                cell.NumberFormat = textIndicator;
    //                if (dt.Columns[i - 1].ColumnName != "DATEOFBIRTH" && dt.Columns[i - 1].ColumnName != "REGISTERDATE" && dt.Columns[i - 1].ColumnName != "LASTVISITDATE")
    //                {
    //                    if (cell.Value2 != null)
    //                        cell.Value2 = cell.Value2.ToString();
    //                }
    //                else
    //                    cell.Value2 = Convert.ToDateTime(dr[i - 1].ToString()).ToString("dd/MM/yyyy");
    //            }
    //        }

    //        oRange = oSheet.get_Range(oSheet.Cells[1, 1], oSheet.Cells[rowCount, dt.Columns.Count]);
    //        oRange.EntireColumn.AutoFit();

    //        oSheet = null;
    //        oRange = null;

    //        oWB.SaveAs(directory + "\\" + filename, XlFileFormat.xlWorkbookNormal,
    //        Missing.Value, Missing.Value, Missing.Value, Missing.Value,
    //        XlSaveAsAccessMode.xlExclusive,
    //        Missing.Value, Missing.Value, Missing.Value,
    //        Missing.Value, Missing.Value);
    //        oWB.Close(Missing.Value, Missing.Value, Missing.Value);
    //        oWB = null;
    //        oXL.Quit();

    //        Thread.CurrentThread.CurrentCulture = oldCulture;
    //    }
    //    catch
    //    {
    //        throw;
    //    }
    //    finally
    //    {
    //        GC.WaitForPendingFinalizers();
    //        GC.Collect();
    //        GC.WaitForPendingFinalizers();
    //        GC.Collect();
    //    }

    //    return true;
    //}

    public static void ExportDataTableToExcel(DataSet source, string fileName)
    {

        foreach (DataRow item in source.Tables[0].Rows)
        {
            foreach (DataColumn column in source.Tables[0].Columns)
            {
                if (column.ColumnName.Trim() == "BRANDPREBAILEYS")
                {
                    if (item[column].ToString() == "T")
                        item[column] = "TRUE";
                    else
                        item[column] = "FALSE";
                }
            }
        }


        System.IO.StreamWriter excelDoc;
        excelDoc = new System.IO.StreamWriter(fileName);
        excelDoc.Write("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
        const string startExcelXML = "<xml version=\"1.0\"?>\r\n<?mso-application progid=\"Excel.Sheet\"?>\r\n<Workbook " +
              "xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"\r\n" +
              " xmlns:o=\"urn:schemas-microsoft-com:office:office\"\r\n " +
              " xmlns:x=\"urn:schemas-microsoft-com:office:excel\"\r\n " +
              " xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheetoffice:spreadsheet\"\r\n " +
              " xmlns:html=\"urn:http://www.w3.org/TR/REC-html40\">\r\n " +
              " <ExcelWorkbook xmlns=\"urn:schemas-microsoft-com:office:excel\">\r\n " +
              " <WindowHeight>11640</WindowHeight>\r\n " +
              " <WindowWidth>15480</WindowWidth>\r\n " +
              " <WindowTopX>0</WindowTopX>\r\n " +
              " <WindowTopY>75</WindowTopY>\r\n " +
              " <RefModeR1C1/>\r\n " +
              " <ProtectStructure>False</ProtectStructure>\r\n " +
              " <ProtectWindows>False</ProtectWindows>\r\n " +
              " </ExcelWorkbook>\r\n ";

        const string endExcelXML = "</Workbook>";

        int rowCount = 0;
        int sheetCount = 1;
        excelDoc.Write(startExcelXML);
        excelDoc.Write("<Worksheet ss:Name=\"ETRSTemplate" + sheetCount + "\">");
        excelDoc.Write("<Table ss:ExpandedColumnCount=\"256\" ss:ExpandedRowCount=\"1\" x:FullColumns=\"1\" x:FullRows=\"1\">");
        excelDoc.Write("");
        excelDoc.Write("<Row>");
        for (int x = 0; x < source.Tables[0].Columns.Count; x++)
        {
            excelDoc.Write("<Cell ss:StyleID=\"BoldColumn\"><Data ss:Type=\"String\">");
            excelDoc.Write(source.Tables[0].Columns[x].ColumnName + "</Data></Cell>");
            //excelDoc.Write();
        }
        excelDoc.Write("</Row>");
        foreach (DataRow x in source.Tables[0].Rows)
        {
            rowCount++;
            //if the number of rows is > 64000 create a new page to continue output
            if (rowCount == 64000)
            {
                rowCount = 0;
                sheetCount++;
                excelDoc.Write("</Table>");
                excelDoc.Write(" </Worksheet>");
                excelDoc.Write("<Worksheet ss:Name=\"Sheet" + sheetCount + "\">");
                excelDoc.Write("<Table>");
            }
            excelDoc.Write("<Row>"); //ID=" + rowCount + "
            for (int y = 0; y < source.Tables[0].Columns.Count; y++)
            {
                System.Type rowType;
                rowType = x[y].GetType();
                switch (rowType.ToString())
                {
                    case "System.String":
                        string XMLstring = x[y].ToString();
                        XMLstring = XMLstring.Trim();
                        XMLstring = XMLstring.Replace("&", "&");
                        XMLstring = XMLstring.Replace(">", ">");
                        XMLstring = XMLstring.Replace("<", "<");
                        excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\">" +
                                       "<Data ss:Type=\"String\">" + XMLstring + "</Data></Cell>");
                        //excelDoc.Write(XMLstring + "</Data></Cell>");
                        //excelDoc.Write("</Data></Cell>");
                        break;
                    case "System.DateTime":
                        //Excel has a specific Date Format of YYYY-MM-DD followed by  
                        //the letter 'T' then hh:mm:sss.lll Example 2005-01-31T24:01:21.000
                        //The Following Code puts the date stored in XMLDate 
                        //to the format above
                        DateTime XMLDate = (DateTime)x[y];
                        string XMLDatetoString = ""; //Excel Converted Date
                        XMLDatetoString = XMLDate.Year.ToString() +
                             "-" +
                             (XMLDate.Month < 10 ? "0" +
                             XMLDate.Month.ToString() : XMLDate.Month.ToString()) +
                             "-" +
                             (XMLDate.Day < 10 ? "0" +
                             XMLDate.Day.ToString() : XMLDate.Day.ToString()) +
                             "T" +
                             (XMLDate.Hour < 10 ? "0" +
                             XMLDate.Hour.ToString() : XMLDate.Hour.ToString()) +
                             ":" +
                             (XMLDate.Minute < 10 ? "0" +
                             XMLDate.Minute.ToString() : XMLDate.Minute.ToString()) +
                             ":" +
                             (XMLDate.Second < 10 ? "0" +
                             XMLDate.Second.ToString() : XMLDate.Second.ToString()) +
                             ".000";
                        excelDoc.Write("<Cell ss:StyleID=\"DateLiteral\">" +
                                     "<Data ss:Type=\"DateTime\">" + XMLDatetoString + "</Data></Cell>");
                        //excelDoc.Write(XMLDatetoString);
                        //excelDoc.Write("</Data></Cell>");
                        break;
                    case "System.Boolean":
                        excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\">" +
                                    "<Data ss:Type=\"String\">" + x[y].ToString() + "</Data></Cell>");
                        //excelDoc.Write(x[y].ToString()+"</Data></Cell>");
                        //excelDoc.Write("</Data></Cell>");
                        break;
                    case "System.Int16":
                    case "System.Int32":
                    case "System.Int64":
                    case "System.Byte":
                        excelDoc.Write("<Cell ss:StyleID=\"Integer\">" +
                                "<Data ss:Type=\"Number\">" + x[y].ToString() + "</Data></Cell>");
                        //excelDoc.Write(x[y].ToString() + "</Data></Cell>");
                        //excelDoc.Write("</Data></Cell>");
                        break;
                    case "System.Decimal":
                    case "System.Double":
                        excelDoc.Write("<Cell ss:StyleID=\"Decimal\">" +
                              "<Data ss:Type=\"Number\">" + x[y].ToString() + "</Data></Cell>");
                        //excelDoc.Write(x[y].ToString() + "</Data></Cell>");
                        //excelDoc.Write("</Data></Cell>");
                        break;
                    case "System.DBNull":
                        excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\">" +
                              "<Data ss:Type=\"String\">" + "</Data></Cell>");
                        //excelDoc.Write("" + "</Data></Cell>");
                        //excelDoc.Write("</Data></Cell>");
                        break;
                    default:
                        throw (new Exception(rowType.ToString() + " not handled."));
                }
            }
            excelDoc.Write("</Row>");
        }
        excelDoc.Write("</Table>");
        excelDoc.Write(" </Worksheet>");
        excelDoc.Write(endExcelXML);
        excelDoc.Close();
    }

    public static bool ExportDataTableToCSVTransaction(DataSet ds, string pathFile)
    {
        try
        {
            string directory = pathFile;
            StringBuilder sb = new StringBuilder();

            foreach (DataTable dt in ds.Tables)
            {
                sb = new StringBuilder();
                foreach (DataRow row in dt.Rows)
                {
                    foreach (DataColumn column in dt.Columns)
                    {
                        sb.Append(row[column.ColumnName].ToString() + ',');
                    }
                    sb.Remove(sb.Length - 1, 1);
                    sb.Append(Environment.NewLine);
                }

                File.AppendAllText(directory + "\\" + "ExtractData.csv", sb.ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
        }

        return true;
    }

    public void CopyBackupAndDeleteExcelFile(string DirectoryPath, string FileName, bool BackupFile, bool DeleteFile)
    {
        FileInfo fileInfo = new FileInfo(DirectoryPath + FileName);
        try
        {
            if (fileInfo.Exists)
            {
                if (BackupFile)
                    fileInfo.CopyTo(DirectoryPath + "Backup" + "\\" + FileName);
                if (DeleteFile)
                    fileInfo.Delete();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("DeleteExcelFile :: " + ex.Message);
        }
    }

    #endregion

    #region Message

    protected void JsClientCustom(string key, string message)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), key, message, true);
    }
    protected void JsClientAlert(string key, string message)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), key, string.Format("<script>alert('{0}');</script>", message), false);
    }

    protected void JsClientAlertWithRedirect(string msgalert, string strUrl)
    {
        string javaScript = string.Format("<script language=JavaScript>alert('{0}'); window.location.href = '{1}';</script>", msgalert, strUrl);
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "basejsrdr", javaScript, false);
    }

    protected void JsClientAlertWithCloseWindow(string msgalert)
    {
        string javaScript = string.Format("<script language=JavaScript>alert('{0}'); window.close();</script>", msgalert);
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "basejscls", javaScript, false);
    }

    protected void JsClientAlertWithFocus(string message, Control ctrl)
    {
        try
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", string.Format("<script>alert('{0}');</script>", message), false);
            ScriptManager Instance = ScriptManager.GetCurrent(this);
            if (Instance != null)// exception if not ScriptManager Instance
            {
                Instance.SetFocus(ctrl);
            }
            else
            {
                ctrl.Focus();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    #endregion

    #region CheckPermission
    public void CheckPermission(int UserId, int MenuId)
    {
        UserBLL userBll = new UserBLL();
        UserActivityLogBLL userActivityLogBll = new UserActivityLogBLL();
        System.Data.DataTable dtUser = new System.Data.DataTable();
        try
        {
            dtUser = userBll.CheckAuthentication(UserId, MenuId);
            if (dtUser.Rows.Count > 0)
            {
                PermissionView = dtUser.Rows[0]["accessView"].ToString();
                PermissionAdd = dtUser.Rows[0]["accessCreate"].ToString();
                PermissionEdit = dtUser.Rows[0]["accessModify"].ToString();
                PermissionDelete = dtUser.Rows[0]["accessDelete"].ToString();
                PermissionExport = dtUser.Rows[0]["accessExport"].ToString();
                PermissionPrint = dtUser.Rows[0]["accessPrint"].ToString();

                CanView = PermissionView == "T" ? true : false;
                CanAdd = PermissionAdd == "T" ? true : false;
                CanEdit = PermissionEdit == "T" ? true : false;
                CanDelete = PermissionDelete == "T" ? true : false;
                CanExport = PermissionExport == "T" ? true : false;
                CanPrint = PermissionPrint == "T" ? true : false;
            }
            else
            {
                CanView = false;
                CanAdd = false;
                CanEdit = false;
                CanDelete = false;
                CanExport = false;
                CanPrint = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BLLBase.CheckPermission :: " + ex.Message);
        }
    }
    #endregion

    public DateTime? ConvertDatetimeFromDatepicker(string dateString)
    {
        try
        {
            if (dateString == "")
                return null;

            DateTime date = new DateTime(int.Parse(dateString.Split('/')[2].ToString()), int.Parse(dateString.Split('/')[1].ToString()), int.Parse(dateString.Split('/')[0].ToString()));
            if (dateString == "")
                return null;
            else
                return date;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public bool CheckSecsionNull()
    {
        if (Session["UserID"] == null)
        {
            if (Request.UrlReferrer == null)
            {
                Response.Redirect(ConfigurationManager.AppSettings["PathSite"].ToString() + "/Login.aspx");
                return false;
            }
            else
            {
                string script = "'http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Login.aspx';";
                JsClientCustom("redirect", "alert('Session Expire'); parent.window.location.href = " + script);
                return false;
            }
        }
        else
        {
            return true;
        }
    }

    public static string md5(string sPassword)
    {
        try
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.UTF8.GetBytes(sPassword);
            bs = x.ComputeHash(bs);
            System.Text.StringBuilder s = new System.Text.StringBuilder();
            foreach (byte b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            return s.ToString();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public bool isEmail(string inputEmail)
    {
        if (inputEmail != "")
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputEmail))
                return (true);
            else
                return (false);
        }
        else
            return (false);
    }
}
