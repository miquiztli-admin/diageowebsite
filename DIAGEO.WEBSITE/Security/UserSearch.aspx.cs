﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Security_UserSearch : BasePage
{

    #region Property

    public string UserName { get { return ViewState["UserName"].ToString(); } set { ViewState["UserName"] = value; } }

    public string Email { get { return ViewState["Email"].ToString(); } set { ViewState["Email"] = value; } }

    public string Active { get { return ViewState["Active"].ToString(); } set { ViewState["Active"] = value; } }

    public string GroupID { get { return ViewState["GroupID"].ToString(); } set { ViewState["GroupID"] = value; } }

    #endregion

    #region Method

    public void BindUser(int pageNo)
    {
        UserBLL userBLL;
        DataTable dtSearch;
        int count;

        try
        {
            userBLL = new UserBLL();

            if (pageNo == 1)
                dtSearch = userBLL.Search(UserName, "", Email, GroupID, Active, 0, RowPerPage, "", 0);
            else
                dtSearch = userBLL.Search(UserName, "", Email, GroupID, Active, (pageNo - 1) * RowPerPage, RowPerPage, "", 0);

            count = int.Parse(userBLL.Search(UserName, "", Email, GroupID, Active, 0, RowPerPage, "", 1).Rows[0][0].ToString());

            pgSearch.RowPerPage = RowPerPage;
            pgSearch.CheckStringButton = pageNo.ToString();
            pgSearch.RowCount = count;
            pgSearch.AllowPagingOnDataGrid = false;
            pgSearch.DataSource = dtSearch;
            pgSearch.DataGridBind = dgSearch;
            pgSearch.DataBind();
            tableNavigation.Visible = true;
        }
        catch (Exception ex)
        {
            throw new Exception("BindUser :: " + ex.Message);
        }
    }

    public void BindUserGroup()
    {
        UserGroupBLL userGroupBLL;
        DataTable dtUserGroup;

        try
        {
            userGroupBLL = new UserGroupBLL();
            dtUserGroup = userGroupBLL.SelectAllByActiveStatus("T");
            ddlGroup.DataTextField = "name";
            ddlGroup.DataValueField = "groupId";
            ddlGroup.DataSource = dtUserGroup;
            ddlGroup.DataBind();
            ddlGroup.Items.Insert(0, new ListItem("เลือกกลุ่ม", ""));
        }
        catch (Exception ex)
        {
            throw new Exception("BindUserGroup :: " + ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {
                RowPerPage = int.Parse(ConfigurationManager.AppSettings["RowPerPage"].ToString());
                BindUserGroup();
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserSearch.Page_Load :: " + ex.Message);
        }
    }

    protected void dgSearch_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {                
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserSearch.dgSearch_ItemDataBound :: " + ex.Message);
        }
    }

    protected void dgSearch_ItemCommand(object source, DataGridCommandEventArgs e)
    {

        try 
	    {
            
            if (CanEdit)
            {
                if (e.CommandName == "Edit")
                {
                    Session["OldUserName"] = null;
                    Session["OldUserName"] = e.Item.Cells[0].Text;

                    string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Security/User.aspx?Mode=Edit&UserID=" + e.Item.Cells[5].Text + "&Email=" + e.Item.Cells[2].Text + "&Date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:300px; center:yes; scroll:yes'); document.getElementById('btnSearch').click();";
                    JsClientCustom("SavePopUp", window);

                    //string url = "http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Security/User.aspx?Mode=Edit&UserID=" + e.Item.Cells[5].Text + "&UserName=" + e.Item.Cells[0].Text + "&Email=" + e.Item.Cells[2].Text + "&Date=" + DateTime.Now.ToString();
                    //string window = "popupFullscreen('" + url + "');";
                    //JsClientCustom("SavePopUp", window);
                }
            }
            else
            {
                JsClientCustom("UserSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
            }
	    }
	    catch (Exception ex)
	    {
            JsClientAlert("ErrorMessage", "UserSearch.dgSearch_ItemCommand :: " + ex.Message);
	    }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            
            if (CanView)
            {
                UserName = txtUserName.Text;
                Email = txtEmail.Text;
                Active = rdoActive.SelectedValue;
                GroupID = ddlGroup.SelectedValue;
                BindUser(1);

            }
            else
            {
                JsClientCustom("UserSearch", "alert('ไม่มีสิทธิ์ค้นหาข้อมูล');window.close();");
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "UserUnblockUI", "unblockUI();", true);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserSearch.btnSearch_Click :: " + ex.Message);
        }
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        try
        {
            
            if (CanAdd)
            {
                string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Security/User.aspx?Mode=Insert&UserName=&Email=&Date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:300px; center:yes; scroll:yes'); document.getElementById('btnSearch').click();";
                JsClientCustom("SavePopUp", window);

                //string url = "http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Security/User.aspx?Mode=Insert&UserName=&Email=&Date=" + DateTime.Now.ToString();
                //string window = "popupFullscreen('" + url + "');";
                //JsClientCustom("SavePopUp", window);
            }
            else
            {
                JsClientCustom("UserSearch", "alert('ไม่มีสิทธิ์เพิ่มข้อมูล');window.close();");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserSearch.btnNew_Click :: " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            txtUserName.Text = "";
            txtEmail.Text = "";
            ddlGroup.SelectedIndex = 0;
            rdoActive.SelectedIndex = 0;

            UserName = "";
            Email = "";
            GroupID = "";
            Active = "";
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserSearch.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion

    #region Paging

    protected void pgSearch_GoClick(object sender, EventArgs e)
    {
        try
        {
            UserName = txtUserName.Text;
            Email = txtEmail.Text;
            Active = rdoActive.SelectedValue;

            string[] strField = Request.Form.GetValues("pgSearch$txtPage");
            int intPage = int.Parse(strField[0]);
            Session["currentPage"] = intPage;
            BindUser(intPage);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserSearch.pgSearch_GoClick :: " + ex.Message);
        }
    }

    protected void pgSearch_PageClick(object sender, EventArgs e)
    {
        LinkButton lb = sender as LinkButton;

        try
        {
            UserName = txtUserName.Text;
            Email = txtEmail.Text;
            Active = rdoActive.SelectedValue;

            int intPage = GetCurrentPageIndex(pgSearch.StartGroupPage, pgSearch.QuantityPageShow, lb.Text);
            Session["currentPage"] = intPage;
            BindUser(intPage);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserSearch.pgSearch_PageClick :: " + ex.Message);
        }
    }

    protected void pgSearch_RowPerPageSelectIndexChanged(Object sender, EventArgs e)
    {
        try
        {
            UserName = txtUserName.Text;
            Email = txtEmail.Text;
            Active = rdoActive.SelectedValue;

            DropDownList ddl = sender as DropDownList;
            pgSearch.RowPerPage = int.Parse(ddl.SelectedValue);
            RowPerPage = int.Parse(ddl.SelectedValue);
            BindUser(1);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserSearch.pgSearch_RowPerPageSelectIndexChanged :: " + ex.Message);
        }
    }

    #endregion        
    
}
