﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Security_User : BasePage
{
    #region Property

    public string OldPassword 
    { 
        get 
        { 
            return ViewState["OldPassword"].ToString(); 
        }
        set { ViewState["OldPassword"] = value; } 
    }

    #endregion

    #region Method

    public void BindUserGroup()
    {
        UserGroupBLL userGroupBLL;
        DataTable dtUserGroup;

        try
        {
            userGroupBLL = new UserGroupBLL();
            dtUserGroup = userGroupBLL.SelectAllByActiveStatus("T");
            ddlGroup.DataTextField = "name";
            ddlGroup.DataValueField = "groupId";
            ddlGroup.DataSource = dtUserGroup;
            ddlGroup.DataBind();
            ddlGroup.Items.Insert(0, new ListItem("เลือกกลุ่ม", ""));
        }
        catch (Exception ex)
        {
            throw new Exception("BindUserGroup :: " + ex.Message);
        }
    }

    public void BindData()
    {
        UserBLL userBLL;
        DataTable dtUser;
        User user;

        try
        {
            user = new User();
            userBLL = new UserBLL();
            dtUser = userBLL.SelectOne(int.Parse(Request.QueryString["UserId"].ToString()));
            if (dtUser.Rows.Count != 0)
            {
                if (Request.QueryString["Mode"].ToString() == "Insert")
                {
                    txtUserName.Text = dtUser.Rows[0]["username"].ToString();
                    txtPassword.Text = dtUser.Rows[0]["password"].ToString();
                    txtEmail.Text = dtUser.Rows[0]["email"].ToString();
                    ddlGroup.SelectedValue = dtUser.Rows[0]["userGroupId"].ToString();
                    rdoActive.SelectedValue = dtUser.Rows[0]["active"].ToString();
                }
                else
                {
                    txtUserName.ReadOnly = true;
                    txtUserName.Enabled = false;
                    txtUserName.BackColor = System.Drawing.Color.Silver;

                    txtUserName.Text = dtUser.Rows[0]["username"].ToString();
                    txtFullName.Text = dtUser.Rows[0]["fullname"].ToString();
                    OldPassword = dtUser.Rows[0]["password"].ToString();

                    user.NewPassword = dtUser.Rows[0]["password"].ToString();
                    txtEmail.Text = dtUser.Rows[0]["email"].ToString();
                    ddlGroup.SelectedValue = dtUser.Rows[0]["userGroupId"].ToString();
                    rdoActive.SelectedValue = dtUser.Rows[0]["active"].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindData :: " + ex.Message);
        }
    }

    public bool ValidateSave()
    {
        UserBLL userBLL;
        DataTable dtUser;

        try
        {
            if (txtUserName.Text == "")
            {
                JsClientAlert("Username", "โปรดกรอก User Name");
                return false;
            }

            if (Request.QueryString["Mode"].ToString() == "Insert")
            {
                if (txtPassword.Text == "")
                {
                    JsClientAlert("Password", "โปรดกรอก Password");
                    return false;
                }
            }

            if (txtFullName.Text.Trim() == "")
            {
                JsClientAlert("FullName", "โปรดกรอก FullName");
                return false;
            }

            if (ddlGroup.SelectedIndex == 0)
            {
                JsClientAlert("Group", "โปรดเลือกกลุ่ม");
                return false;
            }

            if (txtEmail.Text == "")
            {
                JsClientAlert("Email", "โปรดกรอก Email");
                return false;
            }

            if (Request.QueryString["Mode"].ToString() == "Edit")
            {
                userBLL = new UserBLL();

                if (txtUserName.Text != Session["OldUserName"].ToString())
                {
                    dtUser = userBLL.SelectOneBYUserNameAndPassword(txtUserName.Text, "", "");
                    if (dtUser.Rows.Count != 0)
                    {
                        JsClientAlert("UsernameHasUse", "User Name นี้มีผู้ใช้เเล้ว");
                        return false;
                    }
                }

                //if (txtEmail.Text != Request.QueryString["Email"].ToString())
                //{
                //    dtUser = userBLL.SelectOneBYUserNameAndPassword("", "", txtEmail.Text);
                //    if (dtUser.Rows.Count != 0)
                //    {
                //        JsClientAlert("EmailHasUse", "Email นี้มีผู้ใช้เเล้ว");
                //        return false;
                //    }
                //}

                if (txtPassword.Text != null && txtPassword.Text != "null" && txtConfirmPassword.Text != null && txtConfirmPassword.Text != "null")
                {
                    if (txtPassword.Text != txtConfirmPassword.Text)
                    {
                        JsClientAlert("ConfirmPassword", "Password เเละ Confirm Password ต้องเหมือนกัน");
                        return false;
                    }
                }
            }
            else
            {
                userBLL = new UserBLL();

                dtUser = userBLL.SelectOneBYUserNameAndPassword(txtUserName.Text, "", "");
                if (dtUser.Rows.Count != 0)
                {
                    JsClientAlert("UsernameHasUse", "User Name นี้มีผู้ใช้เเล้ว");
                    return false;
                }

                if (txtPassword.Text != txtConfirmPassword.Text)
                {
                    JsClientAlert("ConfirmPassword", "Password เเละ Confirm Password ต้องเหมือนกัน");
                    return false;
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateSave :: " + ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {
                BindUserGroup();
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {
                    //txtPassword.ReadOnly = true;
                    BindData();
                }
                else
                {                   
                    //txtUserName.ReadOnly = false;
                    txtUserName.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "User.Page_Load :: " + ex.Message);
        }
    }

    protected void btnResetPassWords_Click(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();
            user.NewPassword = ConfigurationManager.AppSettings["ResetPass"].ToString();
            JsClientCustom("SaveSuccess", "alert('ตั้งค่ารหัสผ่านเริ่มต้น');");

        }
        catch (Exception ex)
        {

            JsClientAlert("ErrorMessage", "User.btnSave_Click :: " + ex.Message);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        UserBLL userBLL;
        User user;
        UserActivityLogBLL userActivityLogBll;

        try
        {
            userBLL = new UserBLL();
            user = new User();
            userActivityLogBll = new UserActivityLogBLL();

            if (ValidateSave())
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                { 
                    if (CanEdit)
                    {
                        if (txtPassword.Text != null && txtPassword.Text != "" && txtConfirmPassword.Text != null && txtConfirmPassword.Text != "")
                        {
                            if (userBLL.UpdateSearch(txtUserName.Text, txtFullName.Text, md5(txtPassword.Text), txtEmail.Text, ddlGroup.SelectedValue, rdoActive.SelectedValue, Request.QueryString["UserID"].ToString(), user.UserID))
                            {
                                userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "User_Edit");
                                JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                            }
                        }
                        else
                        {
                            if (userBLL.UpdateSearch(txtUserName.Text, txtFullName.Text, OldPassword, txtEmail.Text, ddlGroup.SelectedValue, rdoActive.SelectedValue, Request.QueryString["UserID"].ToString(), user.UserID))
                            {
                                userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "User_Edit");
                                JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                            }
                        }
                    }
                    else
                    {
                        JsClientCustom("UserSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
                    }
                }
                else
                {

                    if (CanAdd)
                    {

                        if (userBLL.Insert(txtUserName.Text, txtFullName.Text, md5(txtPassword.Text), txtEmail.Text, ddlGroup.SelectedValue, rdoActive.SelectedValue, user.UserID))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "User_Add");
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                        }
                    }
                    else
                    {
                        JsClientCustom("UserSearch", "alert('ไม่มีสิทธิ์เพิ่มข้อมูล');window.close();");
                    }

                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "User.btnSave_Click :: " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            JsClientCustom("CloseWindow", "window.close();");
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "User.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion
}
