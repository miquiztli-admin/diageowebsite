﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserGroup.aspx.cs" Inherits="Security_UserGroup" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>UserGroup</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    <base target="_self"/>
</head>
<body style="background-color:Silver">
    <form id="FrmUser" runat="server">
    <div style="background-color:Silver">
        <fieldset>
        <legend class="ntextblack">UserGroup</legend>
        <br />
        <table align="center" style="background-color:Silver">
            <tr>
                <td align="right"><asp:Label ID="lblGroupName" CssClass="ntextblack" runat="server" Text="Group Name : "></asp:Label></td>
                <td align="left"><asp:TextBox ID="txtGroupName" ReadOnly="true" Enable="false" 
                        CssClass="ntextblackdata" runat="server" MaxLength="80"></asp:TextBox></td>                
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lbliActive" CssClass="ntextblack" runat="server" Text="Active : "></asp:Label></td>
                <td align="left">
                    <asp:RadioButtonList ID="rdoActive" CssClass="ntextblackdata" runat="server" 
                        RepeatDirection="Horizontal" TabIndex="1">
                        <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>                        
                    </asp:RadioButtonList>
                </td>                
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnSave" CssClass="ntextblack" runat="server" Text="บันทึก" 
                        onclick="btnSave_Click" TabIndex="2" />&nbsp;
                    <asp:Button ID="btnCancel" CssClass="ntextblack" runat="server" Text="ยกเลิก" 
                        onclick="btnCancel_Click" TabIndex="3" />                    
                </td>
            </tr>          
        </table>
        <br />
        </fieldset>
    </div>
    </form>
</body>
</html>

