﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="User.aspx.cs" Inherits="Security_User" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>User</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <base target="_self"/>
</head>
<body style="background-color:Silver">
    <form id="FrmUser" runat="server">
    <div style="background-color:Silver">
        <fieldset>
        <legend class="ntextblack">User</legend>
        <br />
        <table align="center" style="background-color:Silver">
            <tr>
                <td align="right"><asp:Label ID="lblUserName" CssClass="ntextblack" runat="server" Text="User Name : "></asp:Label>
                <font color="red"> * </font>
                </td>
                <td align="left"><asp:TextBox ID="txtUserName" CssClass="ntextblackdata" 
                        runat="server" MaxLength="70"></asp:TextBox></td>                
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lblFullName" CssClass="ntextblack" runat="server" Text="Full Name : "></asp:Label>
                <font color="red"> * </font>
                </td>
                <td align="left"><asp:TextBox ID="txtFullName" CssClass="ntextblackdata" 
                        runat="server" MaxLength="70"></asp:TextBox></td>                
            </tr>            
            <tr>
                <td align="right"><asp:Label ID="lblPassword" CssClass="ntextblack" runat="server" Text="Password : "></asp:Label></td>
                <td align="left">
                    <asp:TextBox ID="txtPassword" CssClass="ntextblackdata" 
                        runat="server" MaxLength="20" TextMode="Password" ></asp:TextBox>&nbsp;
                    </td>                
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lblConfirmPassword" CssClass="ntextblack" runat="server" Text="Confirm Password : "></asp:Label></td>
                <td align="left"><asp:TextBox ID="txtConfirmPassword" CssClass="ntextblackdata" 
                        runat="server" MaxLength="20" TextMode="Password"></asp:TextBox>&nbsp;</td>                
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lblEmail" CssClass="ntextblack" runat="server" Text="Email : "></asp:Label>
                <font color="red"> * </font>                
                </td>
                <td align="left"><asp:TextBox ID="txtEmail" CssClass="ntextblackdata" runat="server" MaxLength="100"></asp:TextBox>&nbsp;</td>                
            </tr>             
            <tr>
                <td align="right"><asp:Label ID="lblGroup" CssClass="ntextblack" runat="server" Text="User Group : "></asp:Label>
                <font color="red"> * </font>
                </td>
                <td align="left"><asp:DropDownList ID="ddlGroup" runat="server" Width="150px"></asp:DropDownList></td>                
            </tr>           
            <tr>
                <td align="right"><asp:Label ID="lbliActive" CssClass="ntextblack" runat="server" Text="Active : "></asp:Label></td>
                <td align="left">
                    <asp:RadioButtonList ID="rdoActive" CssClass="ntextblackdata" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>                        
                    </asp:RadioButtonList>
                </td>                
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnSave" CssClass="ntextblack" runat="server" Text="บันทึก" 
                        onclick="btnSave_Click" />&nbsp;
                    <asp:Button ID="btnCancel" CssClass="ntextblack" runat="server" Text="ยกเลิก" 
                        onclick="btnCancel_Click" />                    
                </td>
            </tr>          
        </table>
        <br />
        </fieldset>
    </div>
    </form>
</body>
</html>
