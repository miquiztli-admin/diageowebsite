﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Security_UserGroup : BasePage
{
    #region Method

    public void BindData()
    {
        UserGroupBLL userGroupBLL;
        DataTable dtUserGroup;

        try
        {
            userGroupBLL = new UserGroupBLL();
            dtUserGroup = userGroupBLL.SelectOne(int.Parse(Request.QueryString["UserGroupID"].ToString()));
            if (dtUserGroup.Rows.Count != 0)
            {
                txtGroupName.Text = dtUserGroup.Rows[0]["name"].ToString();
                rdoActive.SelectedValue = dtUserGroup.Rows[0]["active"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindData :: " + ex.Message);
        }
    }

    public bool ValidateSave(string mode)
    {
        UserGroupBLL userGroupBLL;
        DataTable dtUserGroup;
        string CheckUserGroup;
        DataRow Row; 

        try
        {
            if (txtGroupName.Text == "")
            {
                JsClientAlert("GroupName", "โปรดกรอก Group Name");
                return false;
            }
            userGroupBLL = new UserGroupBLL();
            if (mode == "Edit")
            {
                //if (txtGroupName.Text != Request.QueryString["name"].ToString())
                if (txtGroupName.Text != Session["OldGroupName"].ToString())
                {
                    dtUserGroup = userGroupBLL.SelectOne(int.Parse(Request.QueryString["UserGroupID"].ToString()));

                    Row = dtUserGroup.Rows[0];

                    CheckUserGroup = Row["name"].ToString();
                    if (txtGroupName.Text != CheckUserGroup)
                    {
                        dtUserGroup = null;
                        dtUserGroup = userGroupBLL.SelectOneBYGroupName(txtGroupName.Text);

                        if (dtUserGroup.Rows.Count != 0)
                        {
                            JsClientAlert("UsernameHasUse", "Group Name นี้มีผู้ใช้เเล้ว");
                            return false;
                        }
                    }
                }
            }
            else
            {
                dtUserGroup = userGroupBLL.SelectOneBYGroupName(txtGroupName.Text);
                if (dtUserGroup.Rows.Count != 0)
                {
                    JsClientAlert("UsernameHasUse", "Group Name นี้มีผู้ใช้เเล้ว");
                    return false;
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateSave :: " + ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {
                    BindData();
                }
                else
                {
                    txtGroupName.ReadOnly = false;
                    txtGroupName.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroup.PageLoad :: " + ex.Message);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        UserGroupBLL userGroupBLL;
        User user;
        UserActivityLogBLL userActivityLogBll;

        try
        {
            userGroupBLL = new UserGroupBLL();
            user = new User();
            userActivityLogBll = new UserActivityLogBLL();

            if (ValidateSave(Request.QueryString["Mode"].ToString()))
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {

                    if (CanEdit)
                    {
                        if (userGroupBLL.UpdateSearch(Request.QueryString["UserGroupID"].ToString(), txtGroupName.Text, rdoActive.SelectedValue, user.UserID))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "UserGroup_Edit");
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                        }
                    }
                    else
                    {
                        JsClientCustom("UserGroupSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
                    }
                }
                else
                {

                    if (CanAdd)
                    {

                        if (userGroupBLL.Insert(txtGroupName.Text, rdoActive.SelectedValue, user.UserID))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "UserGroup_Add");
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                        }
                    }
                    else
                    {
                        JsClientCustom("UserGroupSearch", "alert('ไม่มีสิทธิ์เพิ่มข้อมูล');window.close();");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroup.btnSave_Click :: " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            JsClientCustom("WindowClose", "window.close();");
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroup.btnCancel_Click :: " + ex.Message);
        }
    }

#endregion
}
