﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using System.Drawing;
using DIAGEO.COMMON;

public partial class Security_UserAccess : BasePage
{

    #region Property

    public string GroupID { get { return ViewState["GroupID"].ToString(); } set { ViewState["GroupID"] = value; } }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {              
                //RowPerPage = int.Parse(ConfigurationManager.AppSettings["RowPerPage"].ToString());
                BindUserGroup();
                dgSearch.Visible = false;
                //pgSearch.Visible = false;
                btnReset.Visible = false;
                btnSave.Visible = false;
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "alert('UserAccess.Page_Load :: " + ex.Message + "');");
        }
    }

    protected void dgSearch_ItemCreated(object sender, DataGridItemEventArgs e)
    {
        
    }

    protected void dgSearch_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (ddlGroup.SelectedValue != "")
            {
                dgSearch.Visible = true;
                //pgSearch.Visible = true;
                btnReset.Visible = true;
                btnSave.Visible = true;

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    if (e.Item.Cells[9] != null)
                    {
                        if (Convert.ToBoolean(e.Item.Cells[9].Text) == true)
                        {
                            e.Item.Cells[11].FindControl("chkView").Visible = false;
                            e.Item.Cells[12].FindControl("chkCreate").Visible = false;
                            e.Item.Cells[13].FindControl("chkModify").Visible = false;
                            e.Item.Cells[14].FindControl("chkDelete").Visible = false;
                            e.Item.Cells[15].FindControl("chkExport").Visible = false;
                            e.Item.Cells[16].FindControl("chkPrint").Visible = false;
                            e.Item.BackColor = Color.Silver;
                        }
                        else
                        {
                            //e.Item.Cells[9].Text = "&nbsp;&nbsp;";
                            e.Item.BackColor = Color.White;
                            CheckBox chbV = (CheckBox)(e.Item.FindControl("chkView"));
                            CheckBox chbC = (CheckBox)(e.Item.FindControl("chkCreate"));
                            CheckBox chbM = (CheckBox)(e.Item.FindControl("chkModify"));
                            CheckBox chbD = (CheckBox)(e.Item.FindControl("chkDelete"));
                            CheckBox chbE = (CheckBox)(e.Item.FindControl("chkExport"));
                            CheckBox chbP = (CheckBox)(e.Item.FindControl("chkPrint"));
                            e.Item.Cells[2].Text = "&nbsp;&nbsp;&nbsp;&nbsp;" + e.Item.Cells[2].Text.ToString();

                            if (e.Item.Cells[3].Text == "T")
                            {
                                chbV.Checked = true;
                            }

                            if (e.Item.Cells[4].Text == "T")
                            {
                                chbC.Checked = true;
                            }

                            if (e.Item.Cells[5].Text == "T")
                            {
                                chbM.Checked = true;
                            }

                            if (e.Item.Cells[6].Text == "T")
                            {
                                chbD.Checked = true;
                            }

                            if (e.Item.Cells[7].Text == "T")
                            {
                                chbE.Checked = true;
                            }

                            if (e.Item.Cells[8].Text == "T")
                            {
                                chbP.Checked = true;
                            }
                        }
                    }
                }
            }
            else
            {
                dgSearch.Visible = false;
                //pgSearch.Visible = false;
                btnReset.Visible = false;
                btnSave.Visible = false;
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "alert('UserAccess.dgSearch_ItemDataBound :: " + ex.Message + "');");
        }
    }

    protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            if (CanView)
            {

                GroupID = ddlGroup.SelectedValue;
                BindUserAccess(1);

            }
            else
            {
                JsClientCustom("UserAccess", "alert('ไม่มีสิทธิ์ค้นหาข้อมูล');window.close();");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserAccess.ddlGroup_SelectedIndexChanged :: " + ex.Message);
        }
    }

    protected void ddlGroup_TextChanged(object sender, EventArgs e)
    {




        try
        {
            
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "alert('UserAccess.ddlGroup_TextChanged :: " + ex.Message + "');");
        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        try
        {

            if (CanView)
            {
            GroupID = ddlGroup.SelectedValue;
            BindUserAccess(1);
            }
            else
            {
                JsClientCustom("UserAccess", "alert('ไม่มีสิทธิ์ค้นหาข้อมูล');window.close();");
            }

        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserAccess.btnReset_Click :: " + ex.Message);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string MenuID, GroupID, CView, CCreate, CModify, CDelete, CExport, CPrint;
        CheckBox CanViews, CanCreates, CanModifys, CanDeletes, CanExport, CanPrint;
        DataTable dt = new DataTable();
        UserAccessBLL userAccessBll;
        User user;
        UserActivityLogBLL userActivityLogBll;

        try
        {
            user = new User();
            userActivityLogBll = new UserActivityLogBLL();

            if (CanEdit)
            {
                userAccessBll = new UserAccessBLL();
                dt.Columns.Add(new DataColumn("MenuID", typeof(string)));
                dt.Columns.Add(new DataColumn("GroupID", typeof(string)));
                dt.Columns.Add(new DataColumn("CanView", typeof(string)));
                dt.Columns.Add(new DataColumn("CanCreate", typeof(string)));
                dt.Columns.Add(new DataColumn("CanModify", typeof(string)));
                dt.Columns.Add(new DataColumn("CanDelete", typeof(string)));
                dt.Columns.Add(new DataColumn("CanExport", typeof(string)));
                dt.Columns.Add(new DataColumn("CanPrint", typeof(string)));

                foreach (DataGridItem row in dgSearch.Items)
                {
                    if (row.Cells[9].Text == "True")
                    {
                        GroupID = row.Cells[0].Text;
                        MenuID = row.Cells[1].Text;

                        CView = "T";
                        CCreate = "T";
                        CModify = "T";
                        CDelete = "T";
                        CExport = "T";
                        CPrint = "T";

                        dt.Rows.Add(new object[] { MenuID, GroupID, CView, CCreate, CModify, CDelete, CExport, CPrint });
                    }
                    else
                    {
                        GroupID = row.Cells[0].Text;
                        MenuID = row.Cells[1].Text;
                        CanViews = (CheckBox)row.FindControl("chkView");
                        CanCreates = (CheckBox)row.FindControl("chkCreate");
                        CanModifys = (CheckBox)row.FindControl("chkModify");
                        CanDeletes = (CheckBox)row.FindControl("chkDelete");
                        CanExport = (CheckBox)row.FindControl("chkExport");
                        CanPrint = (CheckBox)row.FindControl("chkPrint");
                        CView = CanViews.Checked.ToString();
                        CCreate = CanCreates.Checked.ToString();
                        CModify = CanModifys.Checked.ToString();
                        CDelete = CanDeletes.Checked.ToString();
                        CExport = CanExport.Checked.ToString();
                        CPrint = CanPrint.Checked.ToString();


                        if ((row.Cells[3].Text == "T" & CView == "False") || (row.Cells[3].Text == "F" & CView == "True") || (row.Cells[4].Text == "T" & CCreate == "False") || (row.Cells[4].Text == "F" & CCreate == "True") || (row.Cells[5].Text == "T" & CModify == "False") || (row.Cells[5].Text == "F" & CModify == "True") || (row.Cells[6].Text == "T" & CDelete == "False") || (row.Cells[6].Text == "F" & CDelete == "True") || (row.Cells[7].Text == "T" & CExport == "False") || (row.Cells[7].Text == "F" & CExport == "True") || (row.Cells[8].Text == "T" & CPrint == "False") || (row.Cells[8].Text == "F" & CPrint == "True") || (row.Cells[3].Text == "&nbsp;" & CView == "False") || (row.Cells[3].Text == "&nbsp;" & CView == "True") || (row.Cells[4].Text == "&nbsp;" & CCreate == "False") || (row.Cells[4].Text == "&nbsp;" & CCreate == "True") || (row.Cells[5].Text == "&nbsp;" & CModify == "False") || (row.Cells[5].Text == "&nbsp;" & CModify == "True") || (row.Cells[6].Text == "&nbsp;" & CDelete == "False") || (row.Cells[6].Text == "&nbsp;" & CDelete == "True") || (row.Cells[7].Text == "&nbsp;" & CExport == "False") || (row.Cells[7].Text == "&nbsp;" & CExport == "True") || (row.Cells[8].Text == "&nbsp;" & CPrint == "False") || (row.Cells[8].Text == "&nbsp;" & CPrint == "True"))
                        {
                            //methodupdate
                            if (CView == "True")
                            {
                                CView = "T";
                            }
                            else
                            {
                                CView = "F";
                            }
                            if (CCreate == "True")
                            {
                                CCreate = "T";
                            }
                            else
                            {
                                CCreate = "F";
                            }
                            if (CModify == "True")
                            {
                                CModify = "T";
                            }
                            else
                            {
                                CModify = "F";
                            }
                            if (CDelete == "True")
                            {
                                CDelete = "T";
                            }
                            else
                            {
                                CDelete = "F";
                            }
                            if (CExport == "True")
                            {
                                CExport = "T";
                            }
                            else
                            {
                                CExport = "F";
                            }
                            if (CPrint == "True")
                            {
                                CPrint = "T";
                            }
                            else
                            {
                                CPrint = "F";
                            }

                            dt.Rows.Add(new object[] { MenuID, GroupID, CView, CCreate, CModify, CDelete, CExport, CPrint });

                        }
                    }
                }
                if(dt.Rows.Count != 0)
                {
                    if (userAccessBll.Update(dt, user.UserID.ToString(), ddlGroup.SelectedValue))
                    {
                        userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "UserAccess_Edit");
                        JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                    }
                }
                else
                {
                    JsClientAlert("NoDataChange", "ไม่พบข้อมูลเปลี่ยนแปลง");
                }
                GroupID = ddlGroup.SelectedValue;
                BindUserAccess(1);
                }
            else
            {
                JsClientCustom("UserAccess", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
            }

        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserAccess.btnReset_Click :: " + ex.Message);
        }
        finally
        {
            dt.Clear();
        }

    }

    #endregion

    #region Method

    public DataTable SetParent(DataTable dt)
    {
        int t, t1, t2;
        DataTable TempDtP;
        DataTable TempDtC;
        DataTable TempDtT;

        try
        {

            TempDtP = dt.Clone();
            TempDtC = dt.Clone();
            TempDtT = dt.Clone();
            DataRow Row;
            DataRow Row1;
            DataRow Row2;

            t = int.Parse(dt.Rows.Count.ToString());

            for (int i = 0; i < t; i++)
            {
                Row = dt.Rows[i];

                if (Row["IsParent"].ToString() == "True")
                {

                    TempDtP.ImportRow(dt.Rows[i]);
                }
                else
                {

                    TempDtC.ImportRow(dt.Rows[i]);
                }

            }
            t1 = int.Parse(TempDtP.Rows.Count.ToString());
            t2 = int.Parse(TempDtC.Rows.Count.ToString());
            for (int i = 0; i < t1; i++)
            {
                Row1 = TempDtP.Rows[i];

                TempDtT.ImportRow(TempDtP.Rows[i]);

                for (int j = 0; j < t2; j++)
                {
                    Row2 = TempDtC.Rows[j];
                    if (int.Parse(Row1["MenuID"].ToString()) == int.Parse(Row2["ParentID"].ToString()))
                    {
                        TempDtT.ImportRow(TempDtC.Rows[j]);
                    }

                }
            }
            return TempDtT;
        }
        catch (Exception ex)
        {
            throw new Exception("SetParent :: " + ex.Message);
        }
    }

    public void BindUserGroup()
    {
        UserGroupBLL userGroupBLL;
        DataTable dtUserGroup;

        try
        {
            userGroupBLL = new UserGroupBLL();
            dtUserGroup = userGroupBLL.SelectAllByActiveStatus("T");
            ddlGroup.DataTextField = "name";
            ddlGroup.DataValueField = "groupId";
            ddlGroup.DataSource = dtUserGroup;
            ddlGroup.DataBind();
            ddlGroup.Items.Insert(0, new ListItem("เลือกกลุ่ม", ""));
        }
        catch (Exception ex)
        {
            throw new Exception("UserSearch.BindUserGroup :: " + ex.Message);
        }
    }

    public void BindUserAccess(int pageNo)
    {
        UserAccessBLL userAccessBll;
        DataTable dtUserAccessSearch;
        DataTable dtUserAccessSearchSetParent;
        //int count;

        try
        {
            
            userAccessBll = new UserAccessBLL();

            //if (pageNo == 1)
            //{
                dtUserAccessSearch = userAccessBll.Search(GroupID, 0, /*RowPerPage*/1000, "", 0);
                dtUserAccessSearchSetParent = SetParent(dtUserAccessSearch);
            //}

            /*++ Buff 20131109 Change to no page limit (table is parent-child style, complex to set row limit, waste time) */
            dgSearch.DataSource = dtUserAccessSearchSetParent;
            dgSearch.DataBind();
            /*else
            {
                dtUserAccessSearch = userAccessBll.Search(GroupID, (pageNo - 1) * RowPerPage, RowPerPage, "", 0);
                dtUserAccessSearchSetParent = SetParent(dtUserAccessSearch);
            }

            count = int.Parse(userAccessBll.Search(GroupID, 0, RowPerPage, "", 1).Rows[0][0].ToString());

            pgSearch.RowPerPage = RowPerPage;
            pgSearch.CheckStringButton = pageNo.ToString();
            pgSearch.RowCount = count;
            pgSearch.AllowPagingOnDataGrid = false;
            pgSearch.DataSource = dtUserAccessSearchSetParent;
            pgSearch.DataGridBind = dgSearch;
            pgSearch.DataBind();
            tableNavigation.Visible = true;
            */
            /*-- Buff 20131109 Change to no page limit (table is parent-child style, complex to set row limit, waste time) */
        }
        catch (Exception ex)
        {
            throw new Exception("UserSearch.BindUser :: " + ex.Message);
        }
    }

    #endregion
/*
    #region Paging

    protected void pgSearch_GoClick(object sender, EventArgs e)
    {
        try
        {

            string[] strField = Request.Form.GetValues("pgSearch$txtPage");
            int intPage = int.Parse(strField[0]);
            Session["currentPage"] = intPage;
            BindUserAccess(intPage);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "alert('UserSearch.pgSearch_GoClick :: " + ex.Message + "');");
        }
    }

    protected void pgSearch_PageClick(object sender, EventArgs e)
    {
        LinkButton lb = sender as LinkButton;

        try
        {

            int intPage = GetCurrentPageIndex(pgSearch.StartGroupPage, pgSearch.QuantityPageShow, lb.Text);
            Session["currentPage"] = intPage;
            BindUserAccess(intPage);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "alert('UserSearch.pgSearch_PageClick :: " + ex.Message + "');");
        }
    }

    protected void pgSearch_RowPerPageSelectIndexChanged(Object sender, EventArgs e)
    {
        try
        {

            DropDownList ddl = sender as DropDownList;
            pgSearch.RowPerPage = int.Parse(ddl.SelectedValue);
            RowPerPage = int.Parse(ddl.SelectedValue);
            BindUserAccess(1);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "alert('UserSearch.pgSearch_RowPerPageSelectIndexChanged :: " + ex.Message + "');");
        }
    }

    #endregion 
*/
}
