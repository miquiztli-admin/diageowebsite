﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserAccess.aspx.cs" Inherits="Security_UserAccess" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="ITOS.Utility.CustomControl" Namespace="ITOS.Utility.CustomControl" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>User Search</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="FrmUserSearch" runat="server">
    <asp:ToolkitScriptManager ID="scriptManager" EnableScriptGlobalization="true" EnableScriptLocalization="true" runat="server"></asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="upHome" runat="server">
    <ContentTemplate>    
    <div>
        <fieldset>
        <legend class="ntextblack">AccessGroup</legend>
        <table align="center">
            <tr>
                <td align="right"><asp:Label ID="lblGroup" CssClass="ntextblack" runat="server" Text="UserAccess : "></asp:Label></td>
                <td align="left">
                    <asp:DropDownList ID="ddlGroup" Width="150px" runat="server" 
                        ontextchanged="ddlGroup_TextChanged" AutoPostBack="True" onselectedindexchanged="ddlGroup_SelectedIndexChanged" 
                ></asp:DropDownList>
                </td>                
            </tr>                             
        </table>
        <table align="center" style="width:900px">        
           <tr>
                <td align="right" style="width:900px">
                    <asp:Button ID="btnReset" Width="100px" CssClass="ntextblack" runat="server" 
                        Text="Reset" onclick="btnReset_Click"/>
                    <asp:Button ID="btnSave" Width="100px" CssClass="ntextblack" runat="server" 
                        Text="Save" onclick="btnSave_Click" />    
                </td>
                 
           </tr>
           <tr>
                <td align="center">
                    <asp:DataGrid ID="dgSearch" Width="900px" HeaderStyle-CssClass="gridheaderrowstyle" 
                        runat="server" AutoGenerateColumns="False" 
                        ItemStyle-CssClass="gridrowstyle" 
                        AlternatingItemStyle-CssClass="gridalternativerowstyle" 
                        onitemcreated="dgSearch_ItemCreated" onitemdatabound="dgSearch_ItemDataBound">
                        <AlternatingItemStyle CssClass="gridalternativerowstyle" />
                        <ItemStyle CssClass="gridrowstyle" />
                        <Columns>
                            <asp:BoundColumn DataField="GroupId" HeaderText="GroupId" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="MenuId" HeaderText="MenuID" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="MenuName" HeaderText="Menu Name" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CanView" HeaderText="View" Visible="False">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CanCreate" HeaderText="Create" Visible="False">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CanModify" HeaderText="Modify" Visible="False">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CanDelete" HeaderText="Delete" Visible="False">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CanExport" HeaderText="Export" Visible="False">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CanPrint" HeaderText="Print" Visible="False">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="IsParent" HeaderText="IsParent" Visible="False">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="ParentID" HeaderText="ParentID" Visible="False">
                            </asp:BoundColumn>
                            <asp:TemplateColumn  HeaderText="View" >
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkView" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  HeaderText="Add" >
                                 <ItemTemplate>
                                    <asp:CheckBox ID="chkCreate" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  HeaderText="Edit" >
                                 <ItemTemplate>
                                    <asp:CheckBox ID="chkModify" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  HeaderText="Delete" >
                                 <ItemTemplate>
                                    <asp:CheckBox ID="chkDelete" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  HeaderText="Export" >
                                 <ItemTemplate>
                                    <asp:CheckBox ID="chkExport" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn  HeaderText="Print" >
                                 <ItemTemplate>
                                    <asp:CheckBox ID="chkPrint" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <HeaderStyle CssClass="gridheaderrowstyle" />
                    </asp:DataGrid>
                </td>
            </tr>               
        </table>
        <%--<table id="tableNavigation" visible="false" runat="server" class="ntextblack" align="center">
            <tr><td><cc1:MultiPageNavigation id="pgSearch" runat="server" HaveTextBox="true" CssClass="ntextblackdata" NavigatorAlign="right" OnGoClick="pgSearch_GoClick" OnPageClick="pgSearch_PageClick" StringNextPage=">>" StringPreviousPage="<<" OnRowPerPageSelectIndexChanged="pgSearch_RowPerPageSelectIndexChanged"></cc1:MultiPageNavigation> </td></tr>
        </table>--%>
        </fieldset>
    </div>
    </ContentTemplate>
    <Triggers>
    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="click" />
    </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>

