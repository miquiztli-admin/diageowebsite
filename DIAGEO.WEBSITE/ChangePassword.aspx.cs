﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class ChangePassword : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnOk_Click(object sender, EventArgs e)
    {
        try
        {
            UserBLL userBLL;
            DataTable dtUser = new DataTable();
            User user;
            user = new User();
            userBLL = new UserBLL();

            if (txtOldPassword.Text == "")
            {
                JsClientAlert("noOld", "Please insert Old Password.");
            }
            else if (txtNewPassword.Text == "")
            {
                JsClientAlert("noNew", "Please insert New Password.");
            }
            else if (txtRenewPassword.Text == "")
            {
                JsClientAlert("noReNew", "Please insert ReNew Password.");
            }
            else if (txtNewPassword.Text.Length < 6)
            {
                JsClientAlert("noLength", "Please insert Password more than 6 digit.");
            }
            else if (txtNewPassword.Text != txtRenewPassword.Text)
            {
                JsClientAlert("noSame", "RePassword is not the same.");
            }
            else
            {
                dtUser = userBLL.SelectOneBYUserNameAndPassword("",md5(txtOldPassword.Text), "");
                if (dtUser.Rows.Count != 0)
                {
                    userBLL.UpdateNewPassword(md5(txtOldPassword.Text),md5(txtNewPassword.Text),user.UserID);
                    JsClientAlert("CorrectPass", "Change Password Successful.");
                }
                else
                {
                    JsClientAlert("WrongPass", "OldPassword is not correct.");
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}
