﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class Reports_Report : BasePage
{
    #region Property

    public string PYear
    {
        get
        {
            return ViewState["StartDateReportConsumerRecruitmentSplitByZoneAndOutlet"].ToString();
        }
        set { ViewState["StartDateReportConsumerRecruitmentSplitByZoneAndOutlet"] = value; }
    }

    public ReportDocument Repport
    {
        get
        {
            return (CrystalDecisions.CrystalReports.Engine.ReportDocument)Session["ReportConsumerRecruitmentSplitByZoneAndOutlet"];
        }
        set { Session["ReportConsumerRecruitmentSplitByZoneAndOutlet"] = value; }
    }

    #endregion



    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                BindShow();
            }
        }
        catch (Exception ex)
        {
            
        }
    }

    protected void btnViewReport_Click(object sender, EventArgs e)
    {

        try
        {
            
                if (ValidateData())
                {
                    ViewReport();
                }
            

        }
        catch (Exception ex)
        {
           
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {

            PYear = "";
            txtYear.Text = "";
        }
        catch (Exception ex)
        {
            
        }
    }

    #endregion

    #region Method

    private void BindShow()
    {

        try
        {

            ddlShow.Items.Insert(0, new ListItem("Recruitment & Activation ST", "RecruitmentST.rpt"));
            ddlShow.Items.Insert(1, new ListItem("Database ST", "DatabaseST.rpt"));
            ddlShow.Items.Insert(2, new ListItem("Brand Preference ST", "BrandPreferrence1ST.rpt"));
            ddlShow.Items.Insert(3, new ListItem("Brand Preference - Detail ST", "BrandPreferrence2ST.rpt"));
            ddlShow.Items.Insert(4, new ListItem("Recruitment & Activation HK", "RecruitmentHK.rpt"));
            ddlShow.Items.Insert(5, new ListItem("Database HK", "DatabaseHK.rpt"));
            ddlShow.Items.Insert(6, new ListItem("Brand Preference HK", "BrandPreferrence1HK.rpt"));
            ddlShow.Items.Insert(7, new ListItem("Brand Preference - Detail HK", "BrandPreferrence2HK.rpt"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindShow :: " + ex.Message);
        }
    }

    public bool ValidateData()
    {

        try
        {
            if (txtYear.Text == "")
            {
                
                return false;
            }

            return true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void ViewReport()
    {

        try
        {

            ReportDocument cryRpt = new ReportDocument();
            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            Tables CrTables;

            cryRpt.Load(Server.MapPath("~/Reports/ReportFiles/" + ddlShow.SelectedValue));

            crConnectionInfo.ServerName = ConfigurationManager.AppSettings["CrystalSource"].ToString();
            crConnectionInfo.DatabaseName = ConfigurationManager.AppSettings["Crystalcatalog"].ToString();
            crConnectionInfo.UserID = ConfigurationManager.AppSettings["CrystalUser"].ToString();
            crConnectionInfo.Password = ConfigurationManager.AppSettings["CrystalPWD"].ToString();

            PYear = txtYear.Text;

            cryRpt.SetParameterValue("@YYYY", int.Parse(PYear));

            CrTables = cryRpt.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }

            Repport = cryRpt;
            string window = "var mywindow = window.open('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Reports/ViewReport.aspx?Mode=Insert&Id=ReportConsumerRecruitmentSplitByZoneAndOutlet&Date=" + DateTime.Now.ToString() + "'); ";
            JsClientCustom("ViewReport", window);

        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
    }

    #endregion



}
