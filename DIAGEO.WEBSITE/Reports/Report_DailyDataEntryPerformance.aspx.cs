﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;

public partial class Reports_Report_DailyDataEntryPerformance : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.lbErrorMessage.InnerText = "";
        this.lbErrorMessage.Visible = false;
        if (!Page.IsPostBack)
        {
            try
            {
                DataTable _dtStaffs = dtListStaffFromSmartTouchDB();
                this.lbStaff.DataSource = _dtStaffs;
                this.lbStaff.DataValueField = "Staff_ID";
                this.lbStaff.DataTextField = "Staff_Name";
                this.lbStaff.DataBind();
                this.lbStaff.Items.Insert(0, new ListItem("All", "0"));
                this.lbStaff.SelectedIndex = 0;

                DataTable _dtOutlet = dtListQutletFromSmartTouchDB();
                this.lbOutlet.DataSource = _dtOutlet;
                this.lbOutlet.DataValueField = "Outlet_ID";
                this.lbOutlet.DataTextField = "Outlet_Name";
                this.lbOutlet.DataBind();
                this.lbOutlet.Items.Insert(0, new ListItem("All", "0"));
                this.lbOutlet.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                this.lbErrorMessage.InnerText = ex.Message;
                this.lbErrorMessage.Visible = true;
            }
        }
    }
    protected void btnViewReport_Click(object sender, EventArgs e)
    {
        try
        {
            string Url = ConfigurationManager.AppSettings["ReportServer"].ToString();
            string ReportName = "?/Diageo.DW.Report_Edge/";
            string Report = "DailyDataentryPerformance";
            if (txtTransactionDateFrom.Text == "")
            {
                JsClientAlert("Period", "Please insert StartDate.");
                return;
            }
            else if (txtTransactionDateTo.Text == "")
            {
                JsClientAlert("Period", "Please insert EndDate.");
                return;
            }
            else if (DateTime.ParseExact(txtTransactionDateTo.Text, "dd/MM/yyyy", null) < DateTime.ParseExact(txtTransactionDateFrom.Text, "dd/MM/yyyy", null))
            {
                JsClientAlert("Period", "Please insert EndDate greater than StartDate.");
                return;
            }

            DateTime dtFrom = DateTime.ParseExact(txtTransactionDateFrom.Text, "dd/MM/yyyy", null);
            DateTime dtTo = DateTime.ParseExact(txtTransactionDateTo.Text, "dd/MM/yyyy", null);

            string strStaff = "";
            for (int i = 0; i < lbStaff.Items.Count; i++)
            {
                if (lbStaff.Items[i].Selected)
                {
                    if (strStaff != "")
                        strStaff += "," + lbStaff.Items[i].Value;
                    else
                        strStaff = lbStaff.Items[i].Value;
                }
            }
            if (strStaff == "") strStaff = "0";
            
            string strOutlet = "";
            for (int i = 0; i < lbOutlet.Items.Count; i++)
            {
                if (lbOutlet.Items[i].Selected)
                {
                    if (strOutlet != "")
                        strOutlet += "," + lbOutlet.Items[i].Value;
                    else
                        strOutlet = lbOutlet.Items[i].Value;
                }
            }
            if (strOutlet == "") strOutlet = "0";

            string Parameter = "Startdate=" + HttpUtility.UrlEncode(dtFrom.ToString("MM/dd/yyyy")) + "&Enddate=" + HttpUtility.UrlEncode(dtTo.ToString("MM/dd/yyyy"));
            Parameter += "&StaffIDs=" + HttpUtility.UrlEncode(strStaff);
            Parameter += "&OutletIDs=" + HttpUtility.UrlEncode(strOutlet);
            Parameter += "&SortBy=" + HttpUtility.UrlEncode(this.ddlSortBy.SelectedValue);
            Parameter += "&OrderBy=" + HttpUtility.UrlEncode(this.ddlOrderBy.SelectedValue);

            string FullPath = Url + ReportName + Report + "&rs:Command=Render&rc:Parameters=false&rc:Zoom=88&" + Parameter;

            ClientScript.RegisterStartupScript("".GetType(), "onload", "<script>self.frames['iframeReport'].location.href = '" + FullPath + "' ;</script>");
        }
        catch (Exception ex)
        {
            throw new Exception("btnViewReport_Click ::" + ex.Message);
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            string Url = ConfigurationManager.AppSettings["ReportServer"].ToString();
            string ReportName = "?/Diageo.DW.Report_Edge/";
            string Report = "DailyDataentryPerformance";
            if (txtTransactionDateFrom.Text == "")
            {
                JsClientAlert("Period", "Please insert StartDate.");
                return;
            }
            else if (txtTransactionDateTo.Text == "")
            {
                JsClientAlert("Period", "Please insert EndDate.");
                return;
            }
            else if (DateTime.ParseExact(txtTransactionDateTo.Text, "dd/MM/yyyy", null) < DateTime.ParseExact(txtTransactionDateFrom.Text, "dd/MM/yyyy", null))
            {
                JsClientAlert("Period", "Please insert EndDate greater than StartDate.");
                return;
            }

            DateTime dtFrom = DateTime.ParseExact(txtTransactionDateFrom.Text, "dd/MM/yyyy", null);
            DateTime dtTo = DateTime.ParseExact(txtTransactionDateTo.Text, "dd/MM/yyyy", null);

            string strStaff = "0";
            for (int i = 0; i < lbStaff.Items.Count; i++)
            {
                if (lbStaff.Items[i].Selected)
                {
                    if (strStaff != "")
                        strStaff += "," + lbStaff.Items[i].Value;
                    else
                        strStaff = lbStaff.Items[i].Value;
                }
            }

            string strOutlet = "0";
            for (int i = 0; i < lbOutlet.Items.Count; i++)
            {
                if (lbOutlet.Items[i].Selected)
                {
                    if (strOutlet != "")
                        strOutlet += "," + lbOutlet.Items[i].Value;
                    else
                        strOutlet = lbOutlet.Items[i].Value;
                }
            }
            string Parameter = "Startdate=" + HttpUtility.UrlEncode(dtFrom.ToString("MM/dd/yyyy")) + "&Enddate=" + HttpUtility.UrlEncode(dtTo.ToString("MM/dd/yyyy"));
            Parameter += "&StaffIDs=" + HttpUtility.UrlEncode(strStaff);
            Parameter += "&OutletIDs=" + HttpUtility.UrlEncode(strOutlet);
            Parameter += "&SortBy=" + HttpUtility.UrlEncode(this.ddlSortBy.SelectedValue);
            Parameter += "&OrderBy=" + HttpUtility.UrlEncode(this.ddlOrderBy.SelectedValue);

            string FullPath = Url + ReportName + Report + "&rs:Command=Render&rc:Parameters=false&rs:Format=WORD&" + Parameter;
            ClientScript.RegisterStartupScript("".GetType(), "onload", "<script>self.frames['iframeReport'].location.href = '" + FullPath + "' ;</script>");
        }
        catch (Exception ex)
        {
            throw new Exception("btnExport_Click ::" + ex.Message);
        }
    }

    private DataTable dtListStaffFromSmartTouchDB()
    {
        SqlInt32 _errorCode = 0;
        SqlConnection _mainConnection = new SqlConnection();
        AppSettingsReader _configReader = new AppSettingsReader();
        _mainConnection.ConnectionString =
                    _configReader.GetValue("Main.ConnectionString", typeof(string)).ToString();

        SqlCommand cmdToExecute = new SqlCommand();
        cmdToExecute.CommandText = "Sp_SmartTouch_Staffs";
        cmdToExecute.CommandType = CommandType.StoredProcedure;
        DataTable toReturn = new DataTable("Staff");
        SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
        cmdToExecute.Connection = _mainConnection;

        try
        {
            cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

            _mainConnection.Open();
            adapter.Fill(toReturn);
            _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

            if (_errorCode != 0)
            {
                throw new Exception("Stored Procedure 'Sp_SmartTouch_Staffs' reported the ErrorCode: " + _errorCode);
            }
            return toReturn;
        }
        catch (Exception ex)
        {
            throw new Exception("Get staff data::Error occured.", ex);
        }
        finally
        {
            _mainConnection.Close();
            cmdToExecute.Dispose();
            adapter.Dispose();
        }
    }

    private DataTable dtListQutletFromSmartTouchDB()
    {
        SqlInt32 _errorCode = 0;
        SqlConnection _mainConnection = new SqlConnection();
        AppSettingsReader _configReader = new AppSettingsReader();
        _mainConnection.ConnectionString =
                    _configReader.GetValue("Main.ConnectionString", typeof(string)).ToString();

        SqlCommand cmdToExecute = new SqlCommand();
        cmdToExecute.CommandText = "Sp_SmartTouch_Outlets";
        cmdToExecute.CommandType = CommandType.StoredProcedure;
        DataTable toReturn = new DataTable("Outlet");
        SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
        cmdToExecute.Connection = _mainConnection;

        try
        {
            cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

            _mainConnection.Open();
            adapter.Fill(toReturn);
            _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

            if (_errorCode != 0)
            {
                throw new Exception("Stored Procedure 'Sp_SmartTouch_Outlets' reported the ErrorCode: " + _errorCode);
            }
            return toReturn;
        }
        catch (Exception ex)
        {
            throw new Exception("Get outlet data::Error occured.", ex);
        }
        finally
        {
            _mainConnection.Close();
            cmdToExecute.Dispose();
            adapter.Dispose();
        }
    }
}
