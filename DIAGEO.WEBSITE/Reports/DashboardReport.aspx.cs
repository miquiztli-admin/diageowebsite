﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class Reports_DashboardReport : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

#region "ITOS Procedures"
    //protected void btnViewReport_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        string Url = ConfigurationManager.AppSettings["ReportServer"].ToString();
    //        string ReportName = "?/Diageo.DW.Report_Edge/";
    //        string Report = "DashboardBrand_JWB";
    //        string Parameter = "0";
    //        string Condition = "";
    //        if (txtPeriod.Text != "")
    //        {
    //            Parameter = txtPeriod.Text;
    //            if (cbBlackLabel.Checked)
    //                Report = "DashboardBrand_JWB";
    //            else if (cbRedLabel.Checked)
    //                Report = "DashboardBrand_JWR";
    //            else if (cbSmirnoff.Checked)
    //                Report = "DashboardBrand_Smirnoff";
    //            else if (cbHipkingdom.Checked)
    //                Report = "DashboardBrand_HK";
    //            else if (cbSmartTouch.Checked)
    //                Report = "DashboardBrand_SmartTouch";
    //            else if (cbDBSegment.Checked)
    //                Report = "DashboardBrand_Segment";
    //            string FullPath = Url + ReportName + Report + "&rs:Command=Render&rc:Parameters=false&rc:Zoom=88&YYYY=" + Parameter;
    //            Page.RegisterStartupScript("str", "<script>self.frames['iframeReport'].location.href = '" + FullPath + "' ;</script>");
    //        }
    //        else
    //        {
    //            JsClientAlert("Period", "Please insert Period.");
    //        }
    //        //<window.location.href = '" + FullPath + "' target='iframeReport'/>");
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception("btnViewReport_Click ::"+ex.Message);
    //    }
    //}
    //protected void btnExport_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        string Url = ConfigurationManager.AppSettings["ReportServer"].ToString();
    //        string ReportName = "?/Diageo.DW.Report_Edge/";
    //        string Report = "DashboardBrand_JWB";
    //        string Parameter = "0";
    //        string Condition = "";
    //        if (txtPeriod.Text != "")
    //        {
    //            Parameter = txtPeriod.Text;
    //            if (cbBlackLabel.Checked)
    //                Report = "DashboardBrand_JWB";
    //            else if (cbRedLabel.Checked)
    //                Report = "DashboardBrand_JWR";
    //            else if (cbSmirnoff.Checked)
    //                Report = "DashboardBrand_Smirnoff";
    //            else if (cbHipkingdom.Checked)
    //                Report = "DashboardBrand_HK";
    //            else if (cbSmartTouch.Checked)
    //                Report = "DashboardBrand_SmartTouch";
    //            else if (cbDBSegment.Checked)
    //                Report = "DashboardBrand_Segment";
    //            string FullPath = Url + ReportName + Report + "&rs:Command=Render&rc:Parameters=false&rs:Format=WORD&YYYY=" + Parameter;
    //            Page.RegisterStartupScript("str", "<script>window.location.href = '" + FullPath + "' ;</script>");
    //        }
    //        else
    //        {
    //            JsClientAlert("Period", "Please insert Period.");
    //        }
    //        //<window.location.href = '" + FullPath + "' target='iframeReport'/>");
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception("btnViewReport_Click ::" + ex.Message);
    //    }
    //}
#endregion

    protected void btnViewReport_Click(object sender, EventArgs e)
    {
        try
        {
            string Url = ConfigurationManager.AppSettings["ReportServer"].ToString();
            string ReportName = "?/Diageo.DW.Report_Edge/DashboardBrand_Main";
            string Params = "";
            if (txtPeriod.Text.ToString().Trim() == "")
            {
                JsClientAlert("Period", "Please insert year period ( YYYY format and not greater than current year ).");
                return;
            }
            if ((int.Parse(txtPeriod.Text.ToString()) < 2000) || (int.Parse(txtPeriod.Text.ToString()) > (DateTime.Now.Year + 1)))
            {
                JsClientAlert("Period", "Please insert a valid year period ( YYYY format and not greater than current year ).");
                return;
            }
            if (cbBlackLabel.Checked) Params += "&JWBVisible=True";
            if (cbRedLabel.Checked) Params += "&JWRVisible=True";
            if (cbSmirnoff.Checked) Params += "&SmirnoffVisible=True";
            if (cbHipkingdom.Checked) Params += "&HIPKINGDOMVisible=True";
            if (cbSmartTouch.Checked) Params += "&SmartTouchVisible=True";
            if (cbDBSegment.Checked) Params += "&SegmentVisible=True";
            if (Params == "") {
                JsClientAlert("Reports", "Please select report(s).");
                return;            
            }

            Params += "&YYYY=" + txtPeriod.Text.ToString().Trim();
            string FullPath = Url + ReportName + "&rs:Command=Render&rc:Parameters=false&rc:Zoom=88" + Params;
            ClientScript.RegisterStartupScript("".GetType(), "str", "<script>self.frames['iframeReport'].location.href = '" + FullPath + "' ;</script>");
        }
        catch (Exception ex)
        {
            throw new Exception("btnViewReport_Click ::" + ex.Message);
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            string Url = ConfigurationManager.AppSettings["ReportServer"].ToString();
            string ReportName = "?/Diageo.DW.Report_Edge/DashboardBrand_Main";
            string Params = "";
            if (txtPeriod.Text.ToString().Trim() == "")
            {
                JsClientAlert("Period", "Please insert year period ( YYYY format and not greater than current year ).");
                return;
            }
            if ((int.Parse(txtPeriod.Text.ToString()) < 2000) || (int.Parse(txtPeriod.Text.ToString()) > (DateTime.Now.Year + 1)))
            {
                JsClientAlert("Period", "Please insert a valid year period ( YYYY format and not greater than current year ).");
                return;
            }
            if (cbBlackLabel.Checked) Params += "&JWBVisible=True";
            if (cbRedLabel.Checked) Params += "&JWRVisible=True";
            if (cbSmirnoff.Checked) Params += "&SmirnoffVisible=True";
            if (cbHipkingdom.Checked) Params += "&HIPKINGDOMVisible=True";
            if (cbSmartTouch.Checked) Params += "&SmartTouchVisible=True";
            if (cbDBSegment.Checked) Params += "&SegmentVisible=True";
            if (Params == "")
            {
                JsClientAlert("Reports", "Please select report(s).");
                return;
            }

            Params += "&YYYY=" + txtPeriod.Text.ToString().Trim();
            string FullPath = Url + ReportName + "&rs:Command=Render&rc:Parameters=false&rs:Format=WORD" + Params;
            ClientScript.RegisterStartupScript("".GetType(), "str", "<script>self.frames['iframeReport'].location.href = '" + FullPath + "' ;</script>"); 
        }
        catch (Exception ex)
        {
            throw new Exception("btnViewReport_Click ::" + ex.Message);
        }
    }

    protected void btnCancel1_Click(object sender, EventArgs e)
    {
        try
        {
            cbBlackLabel.Checked = false;
            cbDBSegment.Checked = false;
            cbHipkingdom.Checked = false;
            cbRedLabel.Checked = false;
            cbSmartTouch.Checked = false;
            cbSmirnoff.Checked = false;
            txtPeriod.Text = "";
            
        }
        catch (Exception ex)
        {
            throw new Exception("btnCancel1_Click ::" + ex.Message);
        }
    }
}
