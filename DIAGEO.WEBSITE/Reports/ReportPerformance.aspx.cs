﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class Reports_ReportPerformance : BasePage
{
    #region Property

    public string PYear
    {
        get
        {
            return ViewState["StartDateReportConsumerRecruitmentSplitByZoneAndOutlet"].ToString();
        }
        set { ViewState["StartDateReportConsumerRecruitmentSplitByZoneAndOutlet"] = value; }
    }

    public ReportDocument Repport
    {
        get
        {
            return (CrystalDecisions.CrystalReports.Engine.ReportDocument)Session["ReportConsumerRecruitmentSplitByZoneAndOutlet"];
        }
        set { Session["ReportConsumerRecruitmentSplitByZoneAndOutlet"] = value; }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                BindShow();
                BindOutlet();
                
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnViewReport_Click(object sender, EventArgs e)
    {
        try
        {
            if (ValidateData())
            {
                ViewReport();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {

        }
        catch (Exception ex)
        {

        }
    }

    #endregion

    #region Method

    private void BindShow()
    {

        try
        {
            ddlShow.Items.Insert(0, new ListItem("Performance EDM report", "Performance_EDM.rpt"));
            ddlShow.Items.Insert(1, new ListItem("Performance CallCenter report", "Performance_SMS.rpt"));
            ddlShow.Items.Insert(2, new ListItem("Performance Turn up report", "Performance_Turnup.rpt"));
            ddlShow.Items.Insert(3, new ListItem("Performance EDM By Age Report", "Performance_EDM_Age.rpt"));
            ddlShow.Items.Insert(4, new ListItem("Performance EDM By Brand Report", "Performance_EDM_Brand.rpt"));
            ddlShow.Items.Insert(5, new ListItem("Performance CallCenter By Age Report", "Performance_CallCenter_Age.rpt"));
            ddlShow.Items.Insert(6, new ListItem("Performance CallCenter By Brand Report", "Performance_CallCenter_Brand.rpt"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindShow :: " + ex.Message);
        }
    }

    private void BindOutlet()
    {
        DataTable dtOutlet;
        OutletBLL outletBLL = new OutletBLL();

        try
        {
            dtOutlet = outletBLL.SelectActiveOutlet("T");
            DataRow[] drOutlet = dtOutlet.Select(string.Format("name='{0}'"," "));
            foreach (DataRow item in drOutlet)
            {
                item.Delete();
            }

            ddlOutlet.DataTextField = "name";
            ddlOutlet.DataValueField = "outletId";
            ddlOutlet.DataSource = dtOutlet;
            ddlOutlet.DataBind();
            ddlOutlet.Items.Insert(0, new ListItem("All",""));
        }
        catch (Exception ex)
        {
            throw new Exception("BindShow :: " + ex.Message);
        }
    }

    public bool ValidateData()
    {
        try
        {
            return true;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void ViewReport()
    {

        try
        {

            ReportDocument cryRpt = new ReportDocument();
            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            Tables CrTables;
          
            cryRpt.Load(Server.MapPath("~/Reports/ReportFiles/" + ddlShow.SelectedValue));
            crConnectionInfo.ServerName = ConfigurationManager.AppSettings["CrystalSource"].ToString();
            crConnectionInfo.DatabaseName = ConfigurationManager.AppSettings["Crystalcatalog"].ToString();
            crConnectionInfo.UserID = ConfigurationManager.AppSettings["CrystalUser"].ToString();
            crConnectionInfo.Password = ConfigurationManager.AppSettings["CrystalPWD"].ToString();

            cryRpt.SetParameterValue("@StartDate", txtTransactionDateFrom.Text.Split('/')[2].ToString() + "/" + txtTransactionDateFrom.Text.Split('/')[1].ToString() + "/" + txtTransactionDateFrom.Text.Split('/')[0].ToString());
            cryRpt.SetParameterValue("@EndDate", txtTransactionDateTo.Text.Split('/')[2].ToString() + "/" + txtTransactionDateTo.Text.Split('/')[1].ToString() + "/" + txtTransactionDateTo.Text.Split('/')[0].ToString());
            cryRpt.SetParameterValue("@OutletID", ddlOutlet.SelectedValue);
            if (trDDLOutboundType.Visible == true)
                cryRpt.SetParameterValue("@EDMResult", ddlOutboundType.SelectedValue);

            CrTables = cryRpt.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }

            Repport = cryRpt;
            string window = "var mywindow = window.open('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Reports/ViewReport.aspx?Mode=Insert&Id=ReportConsumerRecruitmentSplitByZoneAndOutlet&Date=" + DateTime.Now.ToString() + "'); ";
            JsClientCustom("ViewReport", window);

        }
        catch (Exception ex)
        {

            throw new Exception(ex.Message);
        }
    }

    #endregion

    protected void ddlShow_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlShow.SelectedIndex == 5 || ddlShow.SelectedIndex == 6)
            {
                ddlOutboundType.Items.Clear();
                ddlOutboundType.Items.Insert(0, new ListItem("Sent", "Sent"));
                ddlOutboundType.Items.Insert(1, new ListItem("Success", "Success"));
                ddlOutboundType.Items.Insert(2, new ListItem("Fail", "Fail"));
                trDDLOutboundType.Visible = true;
            }

            if (ddlShow.SelectedIndex == 3 || ddlShow.SelectedIndex == 4)
            {
                ddlOutboundType.Items.Clear();
                ddlOutboundType.Items.Insert(0, new ListItem("Sent", "Sent"));
                ddlOutboundType.Items.Insert(1, new ListItem("Delivered", "Delivered"));
                ddlOutboundType.Items.Insert(2, new ListItem("Clicked", "Clicked"));
                ddlOutboundType.Items.Insert(3, new ListItem("Turnup", "Turnup"));
                trDDLOutboundType.Visible = true;
            }


            if (ddlShow.SelectedIndex == 0 || ddlShow.SelectedIndex == 1 || ddlShow.SelectedIndex == 2)
            {
                trDDLOutboundType.Visible = false;
            }
        }
        catch (Exception ex)
        {            
            throw new Exception(ex.Message);
        }
    }
}
