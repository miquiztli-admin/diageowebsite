﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;

public partial class ViewReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack) {
                if (Request.QueryString["id"] != null)
                {
                    if (Session[Request.QueryString["id"]] != null)
                    {
                        CrystalReportViewer1.ReportSource = (ReportDocument)Session[Request.QueryString["id"]];
                        CrystalReportViewer1.HasExportButton = true;
                        CrystalReportViewer1.HasCrystalLogo = false;
                        CrystalReportViewer1.HasDrillUpButton = false;
                        CrystalReportViewer1.HasGotoPageButton = false;
                        CrystalReportViewer1.HasPageNavigationButtons = true;
                        CrystalReportViewer1.HasPrintButton = true;
                        CrystalReportViewer1.HasRefreshButton = false;
                        CrystalReportViewer1.HasSearchButton = false;
                        CrystalReportViewer1.HasToggleGroupTreeButton = false;
                        CrystalReportViewer1.HasViewList = false;
                        CrystalReportViewer1.HasZoomFactorList = true;
                        CrystalReportViewer1.DataBind();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Page_Load :: " + ex.Message);
        }
    }

    protected void CrystalReportViewer1_Init(object sender, EventArgs e)
    {
        try
        {
            if (IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    if (Session[Request.QueryString["id"]] != null)
                    {
                        CrystalReportViewer1.ReportSource = (ReportDocument)Session[Request.QueryString["id"]];
                        CrystalReportViewer1.HasExportButton = true;
                        CrystalReportViewer1.HasCrystalLogo = false;
                        CrystalReportViewer1.HasDrillUpButton = false;
                        CrystalReportViewer1.HasGotoPageButton = false;
                        CrystalReportViewer1.HasPageNavigationButtons = false;
                        CrystalReportViewer1.HasPrintButton = true;
                        CrystalReportViewer1.HasRefreshButton = false;
                        CrystalReportViewer1.HasSearchButton = false;
                        CrystalReportViewer1.HasToggleGroupTreeButton = false;
                        CrystalReportViewer1.HasViewList = false;
                        CrystalReportViewer1.HasZoomFactorList = false;
                        CrystalReportViewer1.ReuseParameterValuesOnRefresh = true;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(" CrystalReportViewer1_Init :: " + ex.Message);
        }
    }

}
