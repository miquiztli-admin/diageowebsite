﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DashboardReport.aspx.cs" Inherits="Reports_DashboardReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
<script src="../Jquery/js_function.js" type="text/javascript"></script>
    <title></title>
    <style type="text/css">
        .style1
        {
            vertical-align:top;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="tbDashboard" runat="server" align="center" style="width: 100%">
        <tr>
            <td class="style1">
                <label class="ntextblack">Select Report: </label>
            </td>
            <td>
                <asp:CheckBox ID="cbBlackLabel" runat="server" CssClass="ntextblack" Text="Dashboard Brand – Johnnie Walker Black Label" /><br />
                <asp:CheckBox ID="cbRedLabel" runat="server" CssClass="ntextblack" Text="Dashboard Brand – Johnnie Walker Red Label" /><br />
                <asp:CheckBox ID="cbSmirnoff" runat="server" CssClass="ntextblack" Text="Dashboard Brand – Smirnoff" /><br />
                <asp:CheckBox ID="cbHipkingdom" runat="server" CssClass="ntextblack" Text="Dashboard HIPKINGDOM" /><br />
                <asp:CheckBox ID="cbSmartTouch" runat="server" CssClass="ntextblack" Text="Dashboard SmartTouch" /><br />
                <asp:CheckBox ID="cbDBSegment" runat="server" CssClass="ntextblack" Text="Dashboard Database Segment" />
                
            </td>
        </tr>
        <tr>
            <td class="style1">
                <label class="ntextblack">Year Period: </label>
            </td>
            <td>
                <asp:TextBox ID="txtPeriod" runat="server" OnKeyPress="validateNumKey()" 
                    MaxLength="4"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style1">
            </td>
            <td>
                <label class="ntextblack" id="lbErrorMessage" runat="server" visible="false">Error message display here if anyDashboard </label>
                </td>
        </tr>
        <tr>
            <td class="style1">
            </td>
            <td>
        <asp:Button ID="btnViewReport" CssClass="ntextblack"  runat="server" Text="View Report" 
                    onclick="btnViewReport_Click"  />
        <asp:Button ID="btnExport" CssClass="ntextblack"  runat="server" Text="Export Report" 
                    onclick="btnExport_Click"  />
        <asp:Button ID="btnCancel1" CssClass="ntextblack"  runat="server" Text="Cancel" 
                    onclick="btnCancel1_Click"  />
            </td>
        </tr>
        <tr>
            <td class="style1">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="style1" colspan="2">
                <iframe id="iframeReport" frameborder="0" name="iframeReport" scrolling="auto" 
                    style="width: 100%; height: 1000px"></iframe>
            </td>   
        </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
