﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using DIAGEO.BLL;
using System.Data;

public partial class Reports_Report_DataEntryPerformanceDetail : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            OutboundEventBLL obBll = new OutboundEventBLL();
            DataTable dtEvent = new DataTable();
            dtEvent = obBll.SelectActiveOutboundEventMaster();
            cbOutlet.DataSource = dtEvent;
            cbOutlet.DataTextField = "eventName";
            cbOutlet.DataValueField = "eventId";
            cbOutlet.DataBind();
        }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            string Url = ConfigurationManager.AppSettings["ReportServer"].ToString();
            string ReportName = "?/Diageo.DW.Report_Edge/";
            string Report = "DataEntryPerformance";
            if (txtTransactionDateFrom.Text == "")
            {
                JsClientAlert("Period", "Please insert StartDate.");
                return;
            }
            else if (txtTransactionDateTo.Text == "")
            {
                JsClientAlert("Period", "Please insert EndDate.");
                return;
            }
            else if (DateTime.ParseExact(txtTransactionDateTo.Text, "dd/MM/yyyy", null) < DateTime.ParseExact(txtTransactionDateFrom.Text, "dd/MM/yyyy", null))
            {
                JsClientAlert("Period", "Please insert EndDate greater than StartDate.");
                return;
            }
            else if (ddlTopic.SelectedValue == "")
            {
                JsClientAlert("Topic", "Please select Topic.");
                return;
            }

            DateTime dtFrom = DateTime.ParseExact(txtTransactionDateFrom.Text, "dd/MM/yyyy", null);
            DateTime dtTo = DateTime.ParseExact(txtTransactionDateTo.Text, "dd/MM/yyyy", null);

            string Parameter = "Startdate1=" + HttpUtility.UrlEncode(dtFrom.ToString("yyyy-MM-dd")) + "&Enddate1=" + HttpUtility.UrlEncode(dtTo.ToString("yyyy-MM-dd"));

            string strOutlet = "";
            if (ddlTopic.SelectedValue == '4'.ToString())
            {
                for (int i = 0; i < cbOutlet.Items.Count; i++)
                {
                    if (cbOutlet.Items[i].Selected)
                    {
                        if (cbOutlet.Items[0].Selected)
                        {
                            strOutlet = "0";
                            break;
                        }
                        else
                        {
                            if (strOutlet != "")
                                strOutlet += "," + cbOutlet.Items[i].Value;
                            else
                                strOutlet = cbOutlet.Items[i].Value;
                        }
                    }

                }
            }
            Parameter += "&Outlet=" + HttpUtility.UrlEncode(strOutlet);
            Parameter += "&Topic=" + ddlTopic.SelectedValue;

            string FullPath = Url + ReportName + Report + "&rs:Command=Render&rc:Parameters=false&rs:Format=WORD&" + Parameter;
            ClientScript.RegisterStartupScript("".GetType(), "str", "<script>self.frames['iframeReport'].location.href = '" + FullPath + "' ;</script>");
        }
        catch (Exception ex)
        {
            throw new Exception("btnExport_Click ::" + ex.Message);
        }
    }
    protected void btnViewReport_Click(object sender, EventArgs e)
    {
        try
        {
            string Url = ConfigurationManager.AppSettings["ReportServer"].ToString();
            string ReportName = "?/Diageo.DW.Report_Edge/";
            string Report = "DataEntryPerformance";
            if (txtTransactionDateFrom.Text == "")
            {
                JsClientAlert("Period", "Please insert StartDate.");
                return;
            }
            else if (txtTransactionDateTo.Text == "")
            {
                JsClientAlert("Period", "Please insert EndDate.");
                return;
            }
            else if (DateTime.ParseExact(txtTransactionDateTo.Text, "dd/MM/yyyy", null) < DateTime.ParseExact(txtTransactionDateFrom.Text, "dd/MM/yyyy", null))
            {
                JsClientAlert("Period", "Please insert EndDate greater than StartDate.");
                return;
            }
            else if (ddlTopic.SelectedValue == "")
            {
                JsClientAlert("Topic", "Please select Topic.");
                return;
            }

            DateTime dtFrom = DateTime.ParseExact(txtTransactionDateFrom.Text, "dd/MM/yyyy", null);
            DateTime dtTo = DateTime.ParseExact(txtTransactionDateTo.Text, "dd/MM/yyyy", null);

            string Parameter = "Startdate1=" + HttpUtility.UrlEncode(dtFrom.ToString("yyyy-MM-dd")) + "&Enddate1=" + HttpUtility.UrlEncode(dtTo.ToString("yyyy-MM-dd"));

            string strOutlet = "";
            if (ddlTopic.SelectedValue == '4'.ToString())
            {
                for (int i = 0; i < cbOutlet.Items.Count; i++)
                {
                    if (cbOutlet.Items[i].Selected)
                    {
                        if (cbOutlet.Items[0].Selected)
                        {
                            strOutlet = "0";
                            break;
                        }
                        else
                        {
                            if (strOutlet != "")
                                strOutlet += "," + cbOutlet.Items[i].Value;
                            else
                                strOutlet = cbOutlet.Items[i].Value;
                        }
                    }

                }
            }
            Parameter += "&Outlet=" + HttpUtility.UrlEncode(strOutlet);
            Parameter += "&Topic=" + ddlTopic.SelectedValue;

            string FullPath = Url + ReportName + Report + "&rs:Command=Render&rc:Parameters=false&rc:Zoom=88&" + Parameter;
            ClientScript.RegisterStartupScript("".GetType(), "str", "<script>self.frames['iframeReport'].location.href = '" + FullPath + "' ;</script>");
        }
        catch (Exception ex)
        {
            throw new Exception("btnViewReport_Click ::" + ex.Message);
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }
}
