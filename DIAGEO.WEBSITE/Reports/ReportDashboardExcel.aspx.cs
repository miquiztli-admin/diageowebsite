﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class Reports_ReportDashboardExcel : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void FileStreamDownload(string filePath, string newFileNameDownload, bool deleteFile)
    {      
        FileStream liveStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
        Byte[] buffer;

        try
        {
            buffer = new byte[liveStream.Length];
            liveStream.Read(buffer, 0, int.Parse(liveStream.Length.ToString()));
            liveStream.Close();

            HttpContext.Current.Response.Clear();

            Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Length", buffer.Length.ToString());
            Response.AddHeader("Content-Disposition", "attachment; filename=" + newFileNameDownload);
            Response.BinaryWrite(buffer);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            if (deleteFile)            
            File.Delete(filePath);
        }
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        try
        {
            FileStreamDownload(Server.MapPath("~//Reports//ReportFiles//Dashboard.xls"), "Dashboard.xls", false);
        }
        catch (Exception ex)
        {            
            throw new Exception(ex.Message);
        }
    }
}
