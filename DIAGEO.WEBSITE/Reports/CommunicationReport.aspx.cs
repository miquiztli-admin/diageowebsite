﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DIAGEO.BLL;
using System.Data;
using System.Configuration;

public partial class Reports_CommunicationReport : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            OutboundEventBLL obBll = new OutboundEventBLL();
            DataTable dtEvent = new DataTable();
            dtEvent = obBll.SelectActiveOutboundEventMaster();
            ddlEvent.DataSource = dtEvent;
            ddlEvent.DataTextField = "eventName";
            ddlEvent.DataValueField = "eventId";
            ddlEvent.DataBind();

            ddlEvent0.DataSource = dtEvent;
            ddlEvent0.DataTextField = "eventName";
            ddlEvent0.DataValueField = "eventId";
            ddlEvent0.DataBind();
        }
    }
    protected void btn_ViewReport_Click(object sender, EventArgs e)
    {
        try
        {
            string Url = ConfigurationManager.AppSettings["ReportServer"].ToString();
            string ReportName = "?/Diageo.DW.Report_Edge/";
            string Params = "";
            string FullPath = "";
            if (txtActivePeriodFrom.Text == "")
            {
                JsClientAlert("Period", "Please insert StartDate.");
                return;
            }
            else if (txtActivePeriodTo.Text == "")
            {
                JsClientAlert("Period", "Please insert EndDate.");
                return;
            }
            else if (DateTime.ParseExact(txtActivePeriodTo.Text, "yyyy-MM-dd", null) < DateTime.ParseExact(txtActivePeriodFrom.Text, "yyyy-MM-dd", null))
            {
                JsClientAlert("Period", "Please insert EndDate greater than StartDate.");
                return;
            }

            if (cbAllField.Checked == true)
            {
                if (txtComparePeriodFrom.Text == "")
                {
                    JsClientAlert("Compare Period", "Please insert StartDate.");
                    return;
                }
                else if (txtComparePeriodTo.Text == "")
                {
                    JsClientAlert("Compare Period", "Please insert EndDate.");
                    return;
                }
                else if (DateTime.ParseExact(txtComparePeriodTo.Text, "yyyy-MM-dd", null) < DateTime.ParseExact(txtComparePeriodFrom.Text, "yyyy-MM-dd", null))
                {
                    JsClientAlert("Compare Period", "Please insert EndDate greater than StartDate.");
                    return;
                }
                Params = "&Event=" + ddlEvent.SelectedValue.ToString() + "&DateFrom=" + txtActivePeriodFrom.Text + "&DateTo=" + txtActivePeriodTo.Text + "&Event1=" + ddlEvent0.SelectedValue.ToString() + "&DateFrom1=" + txtComparePeriodFrom.Text + "&DateTo1=" + txtComparePeriodTo.Text + "&EDM=1&SMS=1&CCenter=1";
            }
            else
            {
                Params = "&Event=" + ddlEvent.SelectedValue.ToString() + "&DateFrom=" + txtActivePeriodFrom.Text + "&DateTo=" + txtActivePeriodTo.Text + "&EDM=1&SMS=1&CCenter=1";
                /*FullPath = Url + ReportName + "CommunicationReport&rs:Command=Render&rc:Parameters=false&rc:Zoom=88&Event=" + ddlEvent.SelectedValue.ToString() + "&DateFrom=" + txtActivePeriodFrom.Text + "&DateTo=" + txtActivePeriodTo.Text + "&Event1:isnull=true&DateFrom1:isnull=true&DateTo1:isnull=true&EDM=1&SMS=1&CCenter=1";*/
            }

            if (ddlReport.SelectedValue == "2")
            {
                if (cbAllField.Checked == true)
                {
                    FullPath = Url + ReportName + "TurnupReportCompare&rs:Command=Render&rc:Parameters=false&rc:Zoom=88" + Params;
                }
                else
                {
                    FullPath = Url + ReportName + "TurnupReport&rs:Command=Render&rc:Parameters=false&rc:Zoom=88" + Params;
                }
            }
            else 
            {
                FullPath = Url + ReportName + "CommunicationReport&rs:Command=Render&rc:Parameters=false&rc:Zoom=88" + Params;
            }

            if (FullPath != "")
            {

                ClientScript.RegisterStartupScript("".GetType(), "str", "<script>self.frames['iframeReport'].location.href = '" + FullPath + "' ;</script>");
            }
            else
            {
                JsClientAlert("Report Type", "Please Select Report.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("btnViewReport_Click ::"+ex.Message);
        }
    }
}
