﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using AjaxControlToolkit;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Home : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try 
	    {
            lblVersion.Text = ConfigurationSettings.AppSettings["Version"];

            CheckSecsionNull();

            user = new User();

            if (!Page.IsPostBack)
            {
                lblUserName.Text = user.UserName;
            }

	    }
	    catch (Exception ex)
	    {
            JsClientAlert("ErrorMessage", "Home.Page_Load :: " + ex.Message);
	    }        
    }

    protected void mvMenu_Init(object sender, EventArgs e)
    {
        int index = 0;
        string header;
        string content = "";
        UserMenuBLL userMenuBLL;
        UserAccessBLL userAccessBLL;
        DataTable dtUserMenu;
        DataTable dtAccessMenu;
        DataRow[] drChildMenu;
        DataRow[] drAccessMenu;
        DataRow[] drAccessMenuParent;
        TemplateBuilder tmpHead;
        TemplateBuilder tmpContent;
        AccordionPane pane;
        DIAGEO.COMMON.User _user;

        try
        {
            if (CheckSecsionNull())
            {
                _user = new User();
                userMenuBLL = new UserMenuBLL();
                userAccessBLL = new UserAccessBLL();
                dtUserMenu = userMenuBLL.SelectAll();
                dtAccessMenu = userAccessBLL.SelectAll();
                drAccessMenu = dtAccessMenu.Select(string.Format("groupId='{0}'", _user.UserGroupID));

                foreach (DataRow item in dtUserMenu.Rows)
                {
                    header = "";
                    index = 0;

                    drAccessMenuParent = dtAccessMenu.Select(string.Format("menuId='{0}' AND groupId = '{1}'", item["menuId"].ToString(), _user.UserGroupID.ToString()));

                    if (Convert.ToBoolean(item["isParent"].ToString()) == true)
                    {
                        if (drAccessMenuParent.Count() != 0)
                        {
                            if (drAccessMenuParent[0]["accessView"].ToString() == "T")
                            {
                                header = "<table style='background-image:url(Image/headersilver.gif);width:100%;cursor:pointer;'><tr><td>";
                                header += "<img src='Image/arrow.gif'/><a id='" + item["name"].ToString() + "ID' class='ntextwhiteLarge' onmouseout='OnmouseOutMenuParent(this)' onmouseover='OnmouseOverMenu(this)'>" + item["name"].ToString() + "</a>";
                                header += "</td></tr></table>";
                            }

                            if (item["name"].ToString() == "Logout")
                            {
                                header = "<table style='background-image:url(Image/headersilver.gif);width:100%;cursor:pointer;'><tr><td>";
                                header += "<img src='Image/arrow.gif'/><a id='" + item["name"].ToString() + "ID' class='ntextwhiteLarge' onmouseout='OnmouseOutMenuParent(this)' onclick='Logout()' onmouseover='OnmouseOverMenu(this)'>" + item["name"].ToString() + "</a>";
                                header += "</td></tr></table>";
                            }
                        }
                    }

                    tmpHead = new TemplateBuilder();
                    tmpHead.AllowWhitespaceLiterals();
                    tmpHead.AppendLiteralString(header);

                    pane = new AccordionPane();
                    pane.ID = item["name"].ToString() + "PaneID";
                    pane.Header = tmpHead;
                   
                    drChildMenu = dtUserMenu.Select(string.Format("parentID='{0}'", item["menuID"].ToString()));

                    foreach (DataRow itemChild in drChildMenu)
                    {
                        foreach (DataRow itemAccess in drAccessMenu)
                        {
                            if (itemChild["menuId"].ToString() == itemAccess["menuId"].ToString())
                            {
                                if (itemAccess["accessView"].ToString() == "T")
                                {
                                    content += "<table style='width:100%;cursor:pointer;'><tr><td>";
                                    content += "&nbsp;&nbsp;&nbsp;&nbsp;<a id='" + itemChild["name"].ToString() + "ID' class='ntextwhite' onclick='ChangeIFrameLocation(\"" + itemChild["url"].ToString() + "\",\"" + itemChild["menuId"].ToString() + "\")' onmouseout='OnmouseOutMenu(this)' onmouseover='OnmouseOverMenu(this)'>" + "- " + itemChild["name"].ToString() + "</a>";
                                    content += "</td></tr></table>";
                                }

                                if (itemAccess["accessView"].ToString() == "T")
                                    index = index + 1;
                            }
                        }
                    }

                    tmpContent = new TemplateBuilder();
                    tmpContent.AllowWhitespaceLiterals();
                    tmpContent.AppendLiteralString(content);
                    pane.Content = tmpContent;

                    if (content.Length != 0 || item["name"].ToString() == "Logout")
                        MyAccordion.Panes.Add(pane);

                    content = "";
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Home.mvMenu_Init :: " + ex.Message);
        }        
    }

    public void BindSessionMenu(string menuID)
    {
        User user = new User();

        try
        {
            user.MenuID = menuID;
        }
        catch (Exception ex)
        {            
            throw new Exception(ex.Message);
        }
    }

    protected void btnBindMenu_Click(object sender, EventArgs e)
    {
        try
        {
            if (CheckSecsionNull())
            {
                BindSessionMenu(txtMenuID.Text);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "document.getElementById('iframeMain').src = '" + txtLoc.Text + "';", true);
            }       
        }
        catch (Exception ex)
        {            
            throw new Exception(ex.Message);
        }
    }
}
