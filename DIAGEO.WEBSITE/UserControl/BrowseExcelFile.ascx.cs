﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class UserControl_BrowseExcelFile : UserControl
{
    public DataTable ExcelFileDiageoVIP { get { return Session["ExcelFileDiageoVIP"] as DataTable; } set { Session["ExcelFileDiageoVIP"] = value; } }
    public DataTable ExcelFileVIPOutlet { get { return Session["ExcelFileVIPOutlet"] as DataTable; } set { Session["ExcelFileVIPOutlet"] = value; } }
    public DataTable ExcelFileBlockList { get { return Session["ExcelFileBlockList"] as DataTable; } set { Session["ExcelFileBlockList"] = value; } }

    public string PathFile { get { return Session["PathFile"].ToString(); } set { Session["PathFile"] = value; } }

    protected void Page_Load(object sender, EventArgs e)
    {

    }               

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            BasePage baseP = new BasePage();
            PathFile = "D:\\New fol  der (4)\\excelBlockList.xlsx";
            if (Request.QueryString["DiageoVIP"] != null)
            {
                ExcelFileDiageoVIP = baseP.ReadData(PathFile, "$Sheet1");
            }

            if (Request.QueryString["VIPOutlet"] != null)
            {
                ExcelFileVIPOutlet = baseP.ReadData(PathFile, "$Sheet1");
            }

            if (Request.QueryString["BlockList"] != null)
            {
                ExcelFileBlockList = baseP.ReadData(PathFile, "$Sheet1");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
}
