﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Master_ConsumerSearch : BasePage
{

    #region Property

    public string IdCard { get { return ViewState["IdCard"].ToString(); } set { ViewState["IdCard"] = value; } }

    public string FirstName { get { return ViewState["FirstName"].ToString(); } set { ViewState["FirstName"] = value; } }

    public string LastName { get { return ViewState["LastName"].ToString(); } set { ViewState["LastName"] = value; } }

    public string Email { get { return ViewState["Email"].ToString(); } set { ViewState["Email"] = value; } }

    public string Mobile { get { return ViewState["Mobile"].ToString(); } set { ViewState["Mobile"] = value; } }

    #endregion

    #region Method

    public void BindConsumer(int pageNo)
    {
        ConsumerBLL consumerBLL;
        DataTable dtSearch;
        int count;

        try
        {
            consumerBLL = new ConsumerBLL();

            if (pageNo == 1)
                dtSearch = consumerBLL.Search(IdCard, FirstName, LastName, Email, Mobile, 0, RowPerPage, "", 0);
            else
                dtSearch = consumerBLL.Search(IdCard, FirstName, LastName, Email, Mobile, (pageNo - 1) * RowPerPage, RowPerPage, "", 0);

            count = int.Parse(consumerBLL.Search(IdCard, FirstName, LastName, Email, Mobile, 0, RowPerPage, "", 1).Rows[0][0].ToString());

            pgSearch.RowPerPage = RowPerPage;
            pgSearch.CheckStringButton = pageNo.ToString();
            pgSearch.RowCount = count;
            pgSearch.AllowPagingOnDataGrid = false;
            pgSearch.DataSource = dtSearch;
            pgSearch.DataGridBind = dgSearch;
            pgSearch.DataBind();
            tableNavigation.Visible = true;
        }
        catch (Exception ex)
        {
            throw new Exception("BindConsumer :: " + ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {
                RowPerPage = int.Parse(ConfigurationManager.AppSettings["RowPerPage"].ToString());
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvinceSearch.Page_Load :: " + ex.Message);
        }
    }

    protected void dgSearch_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ConsumerSearch.dgSearch_ItemDataBound :: " + ex.Message);
        }
    }

    protected void dgSearch_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        try
        {

            if (CanEdit)
            {

                if (e.CommandName == "Edit")
                {
                    //Response.Redirect("Consumer.aspx?Mode=Edit&consumerId=" + e.Item.Cells[6].Text + "&idCard=" + e.Item.Cells[0].Text + "&tractionId=" + e.Item.Cells[7].Text + "&email=" + e.Item.Cells[3].Text + "&mobile=" + e.Item.Cells[4].Text + "&Date=" + DateTime.Now.ToString());
                    //string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/Consumer.aspx?Mode=Edit&consumerId=" + e.Item.Cells[6].Text + "&idCard=" + e.Item.Cells[0].Text + "&tractionId=" + e.Item.Cells[7].Text + "&email=" + e.Item.Cells[3].Text + "&mobile=" + e.Item.Cells[4].Text + "&Date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:500px; center:yes; scroll:yes'); document.getElementById('btnSearch').click();";
                    //JsClientCustom("SavePopUp", window);

                    string url = "http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/Consumer.aspx?Mode=Edit&consumerId=" + e.Item.Cells[6].Text + "&idCard=" + e.Item.Cells[0].Text + "&tractionId=" + e.Item.Cells[7].Text + "&email=" + e.Item.Cells[3].Text + "&mobile=" + e.Item.Cells[4].Text + "&Date=" + DateTime.Now.ToString();
                    string window = "popupFullscreen('" + url + "');document.getElementById('btnSearch').click();";
                    JsClientCustom("SavePopUp", window);
                }
            }
            else
            {
                JsClientCustom("ProvinceSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvinceSearch.dgSearch_ItemCommand :: " + ex.Message);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {

            if (CanView)
            {
                IdCard = txtIdCard.Text;
                FirstName = txtFristName.Text;
                LastName = txtLastName.Text;
                Email = txtEmail.Text;
                Mobile = txtMobile.Text;

                BindConsumer(1);
            }
            else
            {
                JsClientCustom("ProvinceSearch", "alert('ไม่มีสิทธิ์ค้นหาข้อมูล');window.close();");
            }
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "UserUnblockUI", "unblockUI();", true);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvinceSearch.btnSearch_Click :: " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            IdCard = txtIdCard.Text = "";
            FirstName = txtFristName.Text = "";
            LastName = txtLastName.Text = "";
            Email = txtEmail.Text = "";
            Mobile = txtMobile.Text = "";

            IdCard = "";
            FirstName = "";
            LastName = "";
            Email = "";
            Mobile = "";
        }
        catch (Exception ex)
        {
            JsClientCustom("ErrorMessage", "ProvinceSearch.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion

    #region Paging

    protected void pgSearch_GoClick(object sender, EventArgs e)
    {
        try
        {
            string[] strField = Request.Form.GetValues("pgSearch$txtPage");
            int intPage = int.Parse(strField[0]);
            Session["currentPage"] = intPage;
            BindConsumer(intPage);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvinceSearch.pgSearch_GoClick :: " + ex.Message);
        }
    }

    protected void pgSearch_PageClick(object sender, EventArgs e)
    {
        LinkButton lb = sender as LinkButton;

        try
        {      
            int intPage = GetCurrentPageIndex(pgSearch.StartGroupPage, pgSearch.QuantityPageShow, lb.Text);
            Session["currentPage"] = intPage;
            BindConsumer(intPage);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvinceSearch.pgSearch_PageClick :: " + ex.Message);
        }
    }

    protected void pgSearch_RowPerPageSelectIndexChanged(Object sender, EventArgs e)
    {
        try
        {
            DropDownList ddl = sender as DropDownList;
            pgSearch.RowPerPage = int.Parse(ddl.SelectedValue);
            RowPerPage = int.Parse(ddl.SelectedValue);
            BindConsumer(1);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvinceSearch.pgSearch_RowPerPageSelectIndexChanged :: " + ex.Message);
        }
    }

    #endregion

}
