﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Master_SurveyTypeSearch : BasePage
{

    #region Property

    public string Code { get { return ViewState["SurveyTypeCode"].ToString(); } set { ViewState["SurveyTypeCode"] = value; } }

    public string Name { get { return ViewState["SurveyTypeName"].ToString(); } set { ViewState["SurveyTypeName"] = value; } }

    public string Active { get { return ViewState["Active"].ToString(); } set { ViewState["Active"] = value; } }

    #endregion

    #region Method

    public void BindSurveyType(int pageNo)
    {
        SurveyTypeBLL surveyTypeBLL;
        DataTable dtSearch;
        int count;

        try
        {
            surveyTypeBLL = new SurveyTypeBLL();

            if (pageNo == 1)
                dtSearch = surveyTypeBLL.Search(Code, Name, Active, 0, RowPerPage, "", 0);
            else
                dtSearch = surveyTypeBLL.Search(Code, Name, Active, (pageNo - 1) * RowPerPage, RowPerPage, "", 0);

            count = int.Parse(surveyTypeBLL.Search(Code, Name, Active, 0, RowPerPage, "", 1).Rows[0][0].ToString());

            pgSearch.RowPerPage = RowPerPage;
            pgSearch.CheckStringButton = pageNo.ToString();
            pgSearch.RowCount = count;
            pgSearch.AllowPagingOnDataGrid = false;
            pgSearch.DataSource = dtSearch;
            pgSearch.DataGridBind = dgSearch;
            pgSearch.DataBind();
            tableNavigation.Visible = true;
        }
        catch (Exception ex)
        {
            throw new Exception("BindSurveyType :: " + ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }


            if (!Page.IsPostBack)
            {
                RowPerPage = int.Parse(ConfigurationManager.AppSettings["RowPerPage"].ToString());
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserSurveyTypeSearch.Page_Load :: " + ex.Message);
        }
    }

    protected void dgSearch_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "SurveyTypeSearch.dgSearch_ItemDataBound :: " + ex.Message);
        }
    }

    protected void dgSearch_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        try
        {
            
            if (CanEdit)
            {

                if (e.CommandName == "Edit")
                {
                    string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/SurveyType.aspx?Mode=Edit&SurveyTypeId=" + e.Item.Cells[4].Text + "&code=" + e.Item.Cells[0].Text + "&Date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:250px; center:yes; scroll:yes'); document.getElementById('btnSearch').click();";
                    JsClientCustom("SavePopUp", window);

                    //string url = "http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/SurveyType.aspx?Mode=Edit&SurveyTypeId=" + e.Item.Cells[4].Text + "&code=" + e.Item.Cells[0].Text + "&Date=" + DateTime.Now.ToString();
                    //string window = "popupFullscreen('" + url + "'); document.getElementById('btnSearch').click();";
                    //JsClientCustom("SavePopUp", window);
                }
            }
            else
            {
                JsClientCustom("SurveyTypeSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "SurveyTypeSearch.dgSearch_ItemCommand :: " + ex.Message);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            
            if (CanView)
            {
                Code = txtSurveyTypeCode.Text;
                Name = txtSurveyTypeName.Text;
                Active = rdoActive.SelectedValue;
                BindSurveyType(1);
            }
            else
            {
                JsClientCustom("SurveyTypeSearch", "alert('ไม่มีสิทธิ์ค้นหาข้อมูล');window.close();");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "SurveyTypeSearch.btnSearch_Click :: " + ex.Message);
        }
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        try
        {
            
            if (CanAdd)
            {

                string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/SurveyType.aspx?Mode=Insert&Code=&Name=&Date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:250px; center:yes; scroll:yes'); document.getElementById('btnSearch').click();";
                JsClientCustom("SavePopUp", window);

                //string url = "http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/SurveyType.aspx?Mode=Insert&Code=&Name=&Date=" + DateTime.Now.ToString();
                //string window = "popupFullscreen('" + url + "'); document.getElementById('btnSearch').click();";
                //JsClientCustom("SavePopUp", window);

            }
            else
            {
                JsClientCustom("SurveyTypeSearch", "alert('ไม่มีสิทธิ์เพิ่มข้อมูล');window.close();");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "SurveyTypeSearch.btnNew_Click :: " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            txtSurveyTypeCode.Text = "";
            txtSurveyTypeName.Text = "";
            rdoActive.SelectedIndex = 0;

            Code = "";
            Name = "";
            Active = "";
        }
        catch (Exception ex)
        {
            JsClientCustom("ErrorMessage", "SurveyTypeSearch.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion

    #region Paging

    protected void pgSearch_GoClick(object sender, EventArgs e)
    {
        try
        {
            //Code = txtSurveyTypeCode.Text;
            //Name = txtSurveyTypeName.Text;
            //Active = rdoActive.SelectedValue;

            string[] strField = Request.Form.GetValues("pgSearch$txtPage");
            int intPage = int.Parse(strField[0]);
            Session["currentPage"] = intPage;
            BindSurveyType(intPage);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "SurveyTypeSearch.pgSearch_GoClick :: " + ex.Message);
        }
    }

    protected void pgSearch_PageClick(object sender, EventArgs e)
    {
        LinkButton lb = sender as LinkButton;

        try
        {
            //Code = txtSurveyTypeCode.Text;
            //Name = txtSurveyTypeName.Text;
            //Active = rdoActive.SelectedValue;

            int intPage = GetCurrentPageIndex(pgSearch.StartGroupPage, pgSearch.QuantityPageShow, lb.Text);
            Session["currentPage"] = intPage;
            BindSurveyType(intPage);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "SurveyTypeSearch.pgSearch_PageClick :: " + ex.Message);
        }
    }

    protected void pgSearch_RowPerPageSelectIndexChanged(Object sender, EventArgs e)
    {
        try
        {
            //Code = txtSurveyTypeCode.Text;
            //Name = txtSurveyTypeName.Text;
            //Active = rdoActive.SelectedValue;

            DropDownList ddl = sender as DropDownList;
            pgSearch.RowPerPage = int.Parse(ddl.SelectedValue);
            RowPerPage = int.Parse(ddl.SelectedValue);
            BindSurveyType(1);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "SurveyTypeSearch.pgSearch_RowPerPageSelectIndexChanged :: " + ex.Message);
        }
    }

    #endregion

}
