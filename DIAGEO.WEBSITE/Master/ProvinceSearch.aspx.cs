﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Master_ProvinceSearch : BasePage
{

    #region Property

    public string Name { get { return ViewState["ProvinceName"].ToString(); } set { ViewState["ProvinceName"] = value; } }

    #endregion

    #region Method

    public void BindProvince(int pageNo)
    {
        ProvinceBLL provinceBLL;
        DataTable dtSearch;
        int count;

        try
        {
            provinceBLL = new ProvinceBLL();

            if (pageNo == 1)
                dtSearch = provinceBLL.Search(Name, 0, RowPerPage, "", 0);
            else
                dtSearch = provinceBLL.Search(Name, (pageNo - 1) * RowPerPage, RowPerPage, "", 0);

            count = int.Parse(provinceBLL.Search(Name, 0, RowPerPage, "", 1).Rows[0][0].ToString());

            pgSearch.RowPerPage = RowPerPage;
            pgSearch.CheckStringButton = pageNo.ToString();
            pgSearch.RowCount = count;
            pgSearch.AllowPagingOnDataGrid = false;
            pgSearch.DataSource = dtSearch;
            pgSearch.DataGridBind = dgSearch;
            pgSearch.DataBind();
            tableNavigation.Visible = true;
        }
        catch (Exception ex)
        {
            throw new Exception("BindProvince :: " + ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {
                RowPerPage = int.Parse(ConfigurationManager.AppSettings["RowPerPage"].ToString());
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvinceSearch.Page_Load :: " + ex.Message);
        }
    }

    protected void dgSearch_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvinceSearch.dgSearch_ItemDataBound :: " + ex.Message);
        }
    }

    protected void dgSearch_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        try
        {
            
            if (CanEdit)
            {

                if (e.CommandName == "Edit")
                {
                    Session["ProvinceName"] = null;
                    Session["ProvinceName"] = e.Item.Cells[0].Text;

                    string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/Province.aspx?Mode=Edit&ProvinceId=" + e.Item.Cells[2].Text + "&name=" + e.Item.Cells[0].Text + "&Date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:250px; center:yes; scroll:yes'); document.getElementById('btnSearch').click();";
                    JsClientCustom("SavePopUp", window);

                    //string url = "http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/Province.aspx?Mode=Edit&ProvinceId=" + e.Item.Cells[2].Text + "&name=" + e.Item.Cells[0].Text + "&Date=" + DateTime.Now.ToString();
                    //string window = "popupFullscreen('" + url + "'); document.getElementById('btnSearch').click();";
                    //JsClientCustom("SavePopUp", window);
                }
            }
            else
            {
                JsClientCustom("ProvinceSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvinceSearch.dgSearch_ItemCommand :: " + ex.Message);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            
            if (CanView)
            {
                Name = txtProvinceName.Text;
                BindProvince(1);
            }
            else
            {
                JsClientCustom("ProvinceSearch", "alert('ไม่มีสิทธิ์ค้นหาข้อมูล');window.close();");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvinceSearch.btnSearch_Click :: " + ex.Message);
        }
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        try
        {
            
            if (CanAdd)
            {
                string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/Province.aspx?Mode=Insert&Code=&Name=&Date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:250px; center:yes; scroll:yes'); document.getElementById('btnSearch').click();";
                JsClientCustom("SavePopUp", window);

                //string url = "http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/Province.aspx?Mode=Insert&Code=&Name=&Date=" + DateTime.Now.ToString();
                //string window = "popupFullscreen('" + url + "'); document.getElementById('btnSearch').click();";
                //JsClientCustom("SavePopUp", window);
            }
            else
            {
                JsClientCustom("ProvinceSearch", "alert('ไม่มีสิทธิ์เพิ่มข้อมูล');window.close();");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvinceSearch.btnNew_Click :: " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            txtProvinceName.Text = "";

            Name = "";
        }
        catch (Exception ex)
        {
            JsClientCustom("ErrorMessage", "ProvinceSearch.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion

    #region Paging

    protected void pgSearch_GoClick(object sender, EventArgs e)
    {
        try
        {
            //Name = txtProvinceName.Text;

            string[] strField = Request.Form.GetValues("pgSearch$txtPage");
            int intPage = int.Parse(strField[0]);
            Session["currentPage"] = intPage;
            BindProvince(intPage);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvinceSearch.pgSearch_GoClick :: " + ex.Message);
        }
    }

    protected void pgSearch_PageClick(object sender, EventArgs e)
    {
        LinkButton lb = sender as LinkButton;

        try
        {
            //Name = txtProvinceName.Text;

            int intPage = GetCurrentPageIndex(pgSearch.StartGroupPage, pgSearch.QuantityPageShow, lb.Text);
            Session["currentPage"] = intPage;
            BindProvince(intPage);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvinceSearch.pgSearch_PageClick :: " + ex.Message);
        }
    }

    protected void pgSearch_RowPerPageSelectIndexChanged(Object sender, EventArgs e)
    {
        try
        {
            //Name = txtProvinceName.Text;

            DropDownList ddl = sender as DropDownList;
            pgSearch.RowPerPage = int.Parse(ddl.SelectedValue);
            RowPerPage = int.Parse(ddl.SelectedValue);
            BindProvince(1);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "ProvinceSearch.pgSearch_RowPerPageSelectIndexChanged :: " + ex.Message);
        }
    }

    #endregion

}
