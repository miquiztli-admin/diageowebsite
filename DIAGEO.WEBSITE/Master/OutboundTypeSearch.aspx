﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OutboundTypeSearch.aspx.cs" Inherits="Master_OutboundTypeSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="ITOS.Utility.CustomControl" Namespace="ITOS.Utility.CustomControl"
    TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>OutboundType Search</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
</head>
<body>
    <form id="FrmUserSearch" runat="server">
    <asp:ToolkitScriptManager ID="scriptManager" EnablePartialRendering="true" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" runat="server">
    </asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="upHome" runat="server">
        <ContentTemplate>
            <div>
                <fieldset>
                    <legend class="ntextblack">OutboundType</legend>
                    <table align="center">
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblOutboundTypeName" CssClass="ntextblack" runat="server" Text="OutboundType Name : "></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtOutboundTypeName" CssClass="ntextblackdata" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lbliActive" CssClass="ntextblack" runat="server" Text="Active : "></asp:Label>
                            </td>
                            <td align="left">
                                <asp:RadioButtonList ID="rdoActive" CssClass="ntextblackdata" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                    <table align="center" style="width: 900px">
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnSearch" CssClass="ntextblack" runat="server" Text="Search" OnClick="btnSearch_Click" />&nbsp;
                                <asp:Button ID="btnCancel" CssClass="ntextblack" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 900px">
                                <asp:Button ID="btnNew" Width="100px" CssClass="ntextblack" runat="server" Text="New"
                                    OnClick="btnNew_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:DataGrid ID="dgSearch" Width="900px" HeaderStyle-CssClass="gridheaderrowstyle"
                                    runat="server" AutoGenerateColumns="false" OnItemDataBound="dgSearch_ItemDataBound"
                                    OnItemCommand="dgSearch_ItemCommand" ItemStyle-CssClass="gridrowstyle" AlternatingItemStyle-CssClass="gridalternativerowstyle">
                                    <Columns>
                                        <asp:BoundColumn DataField="name" HeaderText="outboundType Name" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ActiveWord" HeaderText="Active"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <asp:Button ID="btnEdit" Width="100px" runat="server" Text="Edit" CssClass="ntextblack"
                                                    CommandName="Edit" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="outboundTypeId" HeaderText="GroupId" Visible="false"></asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                    <table id="tableNavigation" visible="false" runat="server" class="ntextblack" align="center">
                        <tr>
                            <td>
                                <cc1:MultiPageNavigation ID="pgSearch" runat="server" HaveTextBox="true" CssClass="ntextblackdata"
                                    NavigatorAlign="right" OnGoClick="pgSearch_GoClick" OnPageClick="pgSearch_PageClick"
                                    StringNextPage=">>" StringPreviousPage="<<" OnRowPerPageSelectIndexChanged="pgSearch_RowPerPageSelectIndexChanged">
                                </cc1:MultiPageNavigation>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
