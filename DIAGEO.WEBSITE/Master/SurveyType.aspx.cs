﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Master_SurveyType : BasePage
{
    #region Method

    public bool ValidateSave(string mode)
    {
        SurveyTypeBLL surveyTypeBLL;
        DataTable dtSurveyType;
        string CheckSurveyTypeName;
        DataRow Row;

        try
        {
            if (txtSurveyTypeCode.Text == "")
            {
                JsClientAlert("SurveyTypeCode", "โปรดกรอก 4A Code");
                return false;
            }
            if (txtSurveyTypeName.Text == "")
            {
                JsClientAlert("SurveyTypeName", "โปรดกรอก 4A Name");
                return false;
            }
            surveyTypeBLL = new SurveyTypeBLL();
            if (mode == "Edit")
            {
                if (txtSurveyTypeCode.Text != Request.QueryString["code"].ToString())
                {
                    dtSurveyType = surveyTypeBLL.SelectOneBYSurveyTypeCodeOrName(txtSurveyTypeCode.Text, "");
                    if (dtSurveyType.Rows.Count != 0)
                    {
                        JsClientAlert("SurveyTypeCodeHasUse", "4A Code นี้มีผู้ใช้เเล้ว");
                        return false;
                    }
                }

                dtSurveyType = surveyTypeBLL.SelectOneBYSurveyTypeCodeOrName(Request.QueryString["code"].ToString(), "");

                Row = dtSurveyType.Rows[0];

                CheckSurveyTypeName = Row["name"].ToString();

                if (txtSurveyTypeName.Text != CheckSurveyTypeName)
                {
                    dtSurveyType = surveyTypeBLL.SelectOneBYSurveyTypeCodeOrName("", txtSurveyTypeName.Text);
                    if (dtSurveyType.Rows.Count != 0)
                    {
                        JsClientAlert("SurveyTypenameHasUse", "4A Name นี้มีผู้ใช้เเล้ว");
                        return false;
                    }
                }

            }
            else
            {
                dtSurveyType = surveyTypeBLL.SelectOneBYSurveyTypeCodeOrName(txtSurveyTypeCode.Text, "");
                if (dtSurveyType.Rows.Count != 0)
                {
                    JsClientAlert("SurveyTypecodeHasUse", "4A Code นี้มีผู้ใช้เเล้ว");
                    return false;
                }

                dtSurveyType = surveyTypeBLL.SelectOneBYSurveyTypeCodeOrName("", txtSurveyTypeName.Text);
                if (dtSurveyType.Rows.Count != 0)
                {
                    JsClientAlert("SurveyTypenameHasUse", "4A Name นี้มีผู้ใช้เเล้ว");
                    return false;
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateSave :: " + ex.Message);
        }
    }

    public void BindData()
    {
        SurveyTypeBLL surveyTypeBLL;
        DataTable dtSurveyType;

        try
        {
            surveyTypeBLL = new SurveyTypeBLL();
            dtSurveyType = surveyTypeBLL.SelectOne(int.Parse(Request.QueryString["SurveyTypeId"].ToString()));
            if (dtSurveyType.Rows.Count != 0)
            {
                txtSurveyTypeCode.Text = dtSurveyType.Rows[0]["code"].ToString();
                txtSurveyTypeName.Text = dtSurveyType.Rows[0]["name"].ToString();
                rdoActive.SelectedValue = dtSurveyType.Rows[0]["active"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindData :: " + ex.Message);
        }
    }


    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {
                    txtSurveyTypeCode.ReadOnly = true;
                    txtSurveyTypeName.ReadOnly = false;
                    txtSurveyTypeCode.Enabled = true;
                    txtSurveyTypeName.Enabled = true;

                    BindData();

                }
                else
                {
                    txtSurveyTypeCode.ReadOnly = false;
                    txtSurveyTypeName.ReadOnly = false;
                    txtSurveyTypeCode.Enabled = true;
                    txtSurveyTypeName.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "SurveyType.PageLoad :: " + ex.Message);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        SurveyTypeBLL surveyTypeBLL;
        User user;
        UserActivityLogBLL userActivityLogBll;

        try
        {
            surveyTypeBLL = new SurveyTypeBLL();
            user = new User();
            userActivityLogBll = new UserActivityLogBLL();

            if (ValidateSave(Request.QueryString["Mode"].ToString()))
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {

                    if (CanEdit)
                    {
                        if (surveyTypeBLL.UpdateSearch(Request.QueryString["SurveyTypeId"].ToString(), txtSurveyTypeCode.Text, txtSurveyTypeName.Text, rdoActive.SelectedValue))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "SurveyType_Edit");
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                        }
                    }
                    else
                    {
                        JsClientCustom("SurveyTypeSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
                    }
                }
                else
                {

                    if (CanAdd)
                    {

                        if (surveyTypeBLL.Insert(txtSurveyTypeCode.Text, txtSurveyTypeName.Text, rdoActive.SelectedValue))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "SurveyType_Add");
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                        }
                    }
                    else
                    {
                        JsClientCustom("SurveyTypeSearch", "alert('ไม่มีสิทธิ์เพิ่มข้อมูล');window.close();");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "SurveyType.btnSave_Click :: " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            JsClientCustom("WindowClose", "window.close();");
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "SurveyType.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion

}
