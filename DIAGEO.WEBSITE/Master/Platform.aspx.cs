﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Master_Platform : BasePage
{
    #region Method

    public bool ValidateSave(string mode)
    {
        PlatformBLL platformBLL;
        DataTable dtPlatform;

        try
        {
            if (txtPlatformCode.Text == "")
            {
                JsClientAlert("PlatformCode", "โปรดกรอก Platform Code");
                return false;
            }
            if (txtPlatformName.Text == "")
            {
                JsClientAlert("PlatformName", "โปรดกรอก Platform Name");
                return false;
            }
            platformBLL = new PlatformBLL();
            if (mode == "Edit")
            {
                if (txtPlatformCode.Text != Request.QueryString["code"].ToString())
                {
                    dtPlatform = platformBLL.SelectOneBYPlatformCodeOrPlatformName(txtPlatformCode.Text, "");
                    if (dtPlatform.Rows.Count != 0)
                    {
                        JsClientAlert("PlatformCodeHasUse", "Platform Code นี้มีผู้ใช้เเล้ว");
                        return false;
                    }
                }


            }
            else
            {
                dtPlatform = platformBLL.SelectOneBYPlatformCodeOrPlatformName(txtPlatformCode.Text, "");
                if (dtPlatform.Rows.Count != 0)
                {
                    JsClientAlert("PlatformcodeHasUse", "Platform Code นี้มีผู้ใช้เเล้ว");
                    return false;
                }

                dtPlatform = platformBLL.SelectOneBYPlatformCodeOrPlatformName("", txtPlatformName.Text);
                if (dtPlatform.Rows.Count != 0)
                {
                    JsClientAlert("PlatformnameHasUse", "Platform Name นี้มีผู้ใช้เเล้ว");
                    return false;
                }
            }

        return true;
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateSave :: " + ex.Message);
        }
    }

    public void BindData()
    {
        PlatformBLL platformBLL;
        DataTable dtPlatform;

        try
        {
            platformBLL = new PlatformBLL();
            dtPlatform = platformBLL.SelectOne(int.Parse(Request.QueryString["PlatformId"].ToString()));
            if (dtPlatform.Rows.Count != 0)
            {
                txtPlatformCode.Text = dtPlatform.Rows[0]["code"].ToString();
                txtPlatformName.Text = dtPlatform.Rows[0]["name"].ToString();
                rdoActive.SelectedValue = dtPlatform.Rows[0]["active"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindData :: " + ex.Message);
        }
    }


    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {
                    txtPlatformCode.ReadOnly = true;
                    txtPlatformName.ReadOnly = false;
                    txtPlatformCode.Enabled = true;
                    txtPlatformName.Enabled = true;
                    txtPlatformCode.BackColor = System.Drawing.Color.LightGray;
                    BindData();

                }
                else
                {
                    txtPlatformCode.ReadOnly = false;
                    txtPlatformName.ReadOnly = false;
                    txtPlatformCode.Enabled = true;
                    txtPlatformName.Enabled = true;
                    txtPlatformCode.BackColor = System.Drawing.Color.White;
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Outlet.PageLoad :: " + ex.Message);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        PlatformBLL platformBLL;
        User user;
        UserActivityLogBLL userActivityLogBll;

        try
        {
            platformBLL = new PlatformBLL();
            user = new User();
            userActivityLogBll = new UserActivityLogBLL();

            if (ValidateSave(Request.QueryString["Mode"].ToString()))
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {

                    if (CanEdit)
                    {
                        if (platformBLL.UpdateSearch(Request.QueryString["PlatformId"].ToString(), txtPlatformCode.Text, txtPlatformName.Text, rdoActive.SelectedValue, user.UserID))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "Platform_Edit");
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                        }
                    }
                    else
                    {
                        JsClientCustom("PlatformSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
                    }
                }
                else
                {

                    if (CanAdd)
                    {

                        if (platformBLL.Insert(txtPlatformCode.Text, txtPlatformName.Text, rdoActive.SelectedValue, user.UserID))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "Platform_Add");
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                        }
                    }
                    else
                    {
                        JsClientCustom("PlatformSearch", "alert('ไม่มีสิทธิ์เพิ่มข้อมูล');window.close();");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Outlet.btnSave_Click :: " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            JsClientCustom("WindowClose", "window.close();");
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroup.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion

}
