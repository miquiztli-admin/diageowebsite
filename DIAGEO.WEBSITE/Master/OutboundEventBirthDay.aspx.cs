﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Master_OutboundEventBirthDay : BasePage
{

    #region Property

    public int TabNumber
    {
        get { return (int)ViewState["TabNumber"]; }
        set { ViewState["TabNumber"] = value; }
    }

    #endregion

    #region Method

    public bool ValidateSave(string mode)
    {

        try
        {
            if (txtEventName.Text == "")
            {
                JsClientAlert("BirthdayConditionName", "โปรดกรอก Birthday Condition Name");
                return false;
            }
            //if (lblTextPlatform.Text == "")
            //{
            //    JsClientAlert("Platform", "โปรดเลือก Platform");
            //    return false;
            //}
            //if (lblTextOutlet.Text == "")
            //{
            //    JsClientAlert("OutletRegister", "โปรดเลือก OutletRegister");
            //    return false;
            //}
            if (txtStartAge.Text == "")
            {
                JsClientAlert("StartAge", "โปรดกรอก StartAge");
                return false;
            }
            if (txtEndAge.Text == "")
            {
                JsClientAlert("EndAge", "โปรดกรอก EndAge");
                return false;
            }
            if (int.Parse(txtStartAge.Text.ToString()) > int.Parse(txtEndAge.Text.ToString()))
            {
                JsClientAlert("Validate", "AgeTo can\\'t less than AgeFrom ");
                return false;
            }
            //if (lblTextProvice.Text == "")
            //{
            //    JsClientAlert("Province", "โปรดเลือก Province");
            //    return false;
            //}

            return true;
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateSave :: " + ex.Message);
        }
    }


    private void BindItemPlatform(ListBox listPlatform)
    {
        ArrayList ArrPlatformNAME;
        ArrayList ArrPlatformID;
        DataTable dtPlatform;

        try
        {
            ArrPlatformID = (ArrayList)Session["ArrPlatformID" + "0"];
            ArrPlatformNAME = (ArrayList)Session["ArrPlatformNAME" + "0"];

            dtPlatform = new DataTable();
            dtPlatform.Columns.Add(new DataColumn("Name", typeof(string)));
            dtPlatform.Columns.Add(new DataColumn("Value", typeof(string)));

            if (ArrPlatformNAME == null)
                ArrPlatformNAME = new ArrayList();

            if (ArrPlatformNAME.Count > 0)
            {
                int i=0;
                foreach (string temp in ArrPlatformNAME)
                {
                    dtPlatform.Rows.Add(new object[] { ArrPlatformNAME[i].ToString(), ArrPlatformID[i].ToString() });
                    i++;
                }

                listPlatform.DataTextField = "Name";
                listPlatform.DataValueField = "Value";
                listPlatform.DataSource = dtPlatform;
                listPlatform.DataBind();

            }

        }
        catch (Exception ex)
        {
            throw new Exception("BindItemPlatform :: " + ex.Message);
        }
    }

    private void BindItemOutlet(ListBox listOutletRegister)
    {
        ArrayList ArrOutletNAME;
        ArrayList ArrOutletID;
        DataTable dtOutlet;

        try
        {
            ArrOutletID = (ArrayList)Session["ArrOutletID" + "0"];
            ArrOutletNAME = (ArrayList)Session["ArrOutletNAME" + "0"];

            dtOutlet = new DataTable();
            dtOutlet.Columns.Add(new DataColumn("Name", typeof(string)));
            dtOutlet.Columns.Add(new DataColumn("Value", typeof(string)));

            if (ArrOutletNAME == null)
                ArrOutletNAME = new ArrayList();

            if (ArrOutletNAME.Count > 0)
            {
                int i = 0;
                foreach (string temp in ArrOutletNAME)
                {
                    dtOutlet.Rows.Add(new object[] { ArrOutletNAME[i].ToString(), ArrOutletID[i].ToString() });
                    i++;
                }

                listOutletRegister.DataTextField = "Name";
                listOutletRegister.DataValueField = "Value";
                listOutletRegister.DataSource = dtOutlet;
                listOutletRegister.DataBind();

            }

        }
        catch (Exception ex)
        {
            throw new Exception("BindItemPlatform :: " + ex.Message);
        }
    }

    private void BindItemProvince(ListBox listProvince)
    {
        ArrayList ArrProvinceNAME;
        ArrayList ArrProvinceID;
        DataTable dtProvince;

        try
        {
            ArrProvinceID = (ArrayList)Session["ArrProvinceID" + "0"];
            ArrProvinceNAME = (ArrayList)Session["ArrProvinceNAME" + "0"];

            dtProvince = new DataTable();
            dtProvince.Columns.Add(new DataColumn("Name", typeof(string)));
            dtProvince.Columns.Add(new DataColumn("Value", typeof(string)));

            if (ArrProvinceNAME == null)
                ArrProvinceNAME = new ArrayList();

            if (ArrProvinceNAME.Count > 0)
            {
                int i = 0;
                foreach (string temp in ArrProvinceNAME)
                {
                    dtProvince.Rows.Add(new object[] { ArrProvinceNAME[i].ToString(), ArrProvinceID[i].ToString() });
                    i++;
                }

                listProvince.DataTextField = "Name";
                listProvince.DataValueField = "Value";
                listProvince.DataSource = dtProvince;
                listProvince.DataBind();

            }

        }
        catch (Exception ex)
        {
            throw new Exception("BindItemPlatform :: " + ex.Message);
        }
    }


    private void BindItemPlatformL(Label lblPlatform)
    {
        ArrayList ArrPlatformName;
        int Count = 0;
        try
        {
            lblPlatform.Text = "";
            ArrPlatformName = (ArrayList)Session["ArrPlatformName" + "0"];

            if (ArrPlatformName == null)
                ArrPlatformName = new ArrayList();
            if (ArrPlatformName.Count > 0)
            {
                for (int i = 0; i < ArrPlatformName.Count; i++)
                {
                    Count++;
                    if ((Count % 3 == 0) && (Count != 0))
                        lblPlatform.Text += Environment.NewLine;

                    if (i != ArrPlatformName.Count - 1)
                        lblPlatform.Text += ArrPlatformName[i].ToString() + ", ";
                    else
                        lblPlatform.Text += ArrPlatformName[i].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindItemPlatform :: " + ex.Message);
        }
    }

    private void BindItemOutletL(Label lblOutlet)
    {
        ArrayList ArrOutletName;
        int Count = 0;
        try
        {
            lblOutlet.Text = "";
            ArrOutletName = (ArrayList)Session["ArrOutletName" + "0"];

            if (ArrOutletName == null)
                ArrOutletName = new ArrayList();
            if (ArrOutletName.Count > 0)
            {
                for (int i = 0; i < ArrOutletName.Count; i++)
                {
                    Count++;
                    if ((Count % 3 == 0) && (Count != 0))
                        lblOutlet.Text += Environment.NewLine;

                    if (i != ArrOutletName.Count - 1)
                        lblOutlet.Text += ArrOutletName[i].ToString() + ", ";
                    else
                        lblOutlet.Text += ArrOutletName[i].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindItemOutlet :: " + ex.Message);
        }
    }

    private void BindItemProvinceL(Label lblProvince)
    {
        ArrayList ArrProvinceName;
        int Count = 0;
        try
        {
            lblProvince.Text = "";
            ArrProvinceName = (ArrayList)Session["ArrProvinceName" + "0"];

            if (ArrProvinceName == null)
                ArrProvinceName = new ArrayList();
            if (ArrProvinceName.Count > 0)
            {
                for (int i = 0; i < ArrProvinceName.Count; i++)
                {
                    Count++;
                    if ((Count % 3 == 0) && (Count != 0))
                        lblProvince.Text += Environment.NewLine;

                    if (i != ArrProvinceName.Count - 1)
                        lblProvince.Text += ArrProvinceName[i].ToString() + ", ";
                    else
                        lblProvince.Text += ArrProvinceName[i].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindItemOutlet :: " + ex.Message);
        }
    }


    private void BindItemPlatform(DataTable dtPlatform, Label lblPlatform)
    {
        int Count = 0;
        DataRow Row;
        ArrayList ArrPlatformID = new ArrayList();
        ArrayList ArrPlatformNAME = new ArrayList();

        try
        {

            if (dtPlatform.Rows.Count != 0)
            {
                for (int i = 0; i < dtPlatform.Rows.Count; i++)
                {
                    Count++;
                    Row = dtPlatform.Rows[i];

                    ArrPlatformID.Add(Row["platformId"].ToString());
                    ArrPlatformNAME.Add(Row["name"].ToString());

                    if ((Count % 3 == 0) && (Count != 0))
                        lblPlatform.Text += Environment.NewLine;

                    if (i != dtPlatform.Rows.Count - 1)
                        lblPlatform.Text += Row["name"].ToString() + ", ";
                    else
                        lblPlatform.Text += Row["name"].ToString().ToString();
                }
                Session["ArrPlatformID0"] = ArrPlatformID;
                Session["ArrPlatformNAME0"] = ArrPlatformNAME;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindItemPlatform :: " + ex.Message);
        }
    }

    private void BindItemOutlet(DataTable dtOutlet, Label lblOutlet)
    {
        int Count = 0;
        DataRow Row;
        ArrayList ArrOutletID = new ArrayList();
        ArrayList ArrOutletNAME = new ArrayList();
        try
        {

            if (dtOutlet.Rows.Count != 0)
            {
                for (int i = 0; i < dtOutlet.Rows.Count; i++)
                {
                    Count++;
                    Row = dtOutlet.Rows[i];

                    ArrOutletID.Add(Row["outletId"].ToString());
                    ArrOutletNAME.Add(Row["name"].ToString());

                    if ((Count % 3 == 0) && (Count != 0))
                        lblOutlet.Text += Environment.NewLine;

                    if (i != dtOutlet.Rows.Count - 1)
                        lblOutlet.Text += Row["name"].ToString() + ", ";
                    else
                        lblOutlet.Text += Row["name"].ToString().ToString();
                }
                Session["ArrOutletID0"] = ArrOutletID;
                Session["ArrOutletNAME0"] = ArrOutletNAME;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindItemOutlet :: " + ex.Message);
        }
    }

    private void BindItemProvince(DataTable dtProvince, Label lblProvince)
    {
        int Count = 0;
        DataRow Row;
        ArrayList ArrProvinceID = new ArrayList();
        ArrayList ArrProvinceNAME = new ArrayList();
        try
        {

            if (dtProvince.Rows.Count != 0)
            {
                for (int i = 0; i < dtProvince.Rows.Count; i++)
                {
                    Count++;
                    Row = dtProvince.Rows[i];

                    ArrProvinceID.Add(Row["provinceId"].ToString());
                    ArrProvinceNAME.Add(Row["name"].ToString());

                    if ((Count % 3 == 0) && (Count != 0))
                        lblProvince.Text += Environment.NewLine;

                    if (i != dtProvince.Rows.Count - 1)
                        lblProvince.Text += Row["name"].ToString() + ", ";
                    else
                        lblProvince.Text += Row["name"].ToString().ToString();
                }
                Session["ArrProvinceID0"] = ArrProvinceID;
                Session["ArrProvinceNAME0"] = ArrProvinceNAME;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindItemOutlet :: " + ex.Message);
        }
    }


    private DataTable BindEventMaster()
    {
        DataTable dtEventMaster;
        string Yes;

        try 
	    {
            // Create File DataTable

		    dtEventMaster = new DataTable();
            dtEventMaster.Columns.Add(new DataColumn("EventName", typeof(string)));
            dtEventMaster.Columns.Add(new DataColumn("ExcludeBlockList", typeof(string)));

            //Exclude Block List

            if (chbYes.Checked)
            {
                Yes = "T";
            }
            else
            {
                Yes = "F";
            }

            //Add Data to DataTable

            dtEventMaster.Rows.Add(new object[] { txtEventName.Text, Yes });

            return dtEventMaster; 

	    }
	    catch (Exception ex)
	    {		
		    throw new Exception(ex.Message);
	    }

        

    }

    private DataTable BindEvent()
    {
        DataTable dtEvent;
        string Baileys, Benmore, JwBlack, JwGold, JwGreen, JwRed, Smirnoff, Absolut, Ballentine, 
            Blend, Chivas, Dewar, Pre100Pipers, Pre100Pipers8Y, Hennessy, BrandPreOther, BrandPreNotDrink,
            DiadeoOptIn, OutletOptIn, HardbounceSMS, HardbounceEDM, HardbounceCallCenter;
        
        try
        {
            #region CreateFileDataTable

            // Create File DataTable

            dtEvent = new DataTable();
            dtEvent.Columns.Add(new DataColumn("Name", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPreJwGold", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPreJwBlack", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPreSmirnoff", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPreJwRed", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPreJwGreen", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPreBenmore", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPreBaileys", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPreAbsolut", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPreBallentine", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPreBlend", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPreChivas", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPreDewar", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPre100Pipers", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPre100Pipers8Y", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPreHennessy", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPreOther", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("BrandPreNotDrink", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("Gender", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("ValidOptInDiageo", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("ValidOptInOutlet", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("StartAge", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("EndAge", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("EmailActive", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("MobileActive", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("IncludeSMSBounce", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("IncludeEDMBounce", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("IncludeCallcenterBounce", typeof(string)));
            dtEvent.Columns.Add(new DataColumn("Active", typeof(string)));

            #endregion

            #region AddValueCheckBox

            if (chbBaileys.Checked)
            {
                Baileys = "T";
            }
            else
            {
                Baileys = "F";
            }

            if (chbBenmore.Checked)
            {
                Benmore = "T";
            }
            else
            {
                Benmore = "F";
            }

            if (chbJwBlack.Checked)
            {
                JwBlack = "T";
            }
            else
            {
                JwBlack = "F";
            }

            if (chbJwGold.Checked)
            {
                JwGold = "T";
            }
            else
            {
                JwGold = "F";
            }

            if (chbJwGreen.Checked)
            {
                JwGreen = "T";
            }
            else
            {
                JwGreen = "F";
            }

            if (chbJwRed.Checked)
            {
                JwRed = "T";
            }
            else
            {
                JwRed = "F";
            }

            if (chbSmirnoff.Checked)
            {
                Smirnoff = "T";
            }
            else
            {
                Smirnoff = "F";
            }

            //Brand Other

            if (chbAbsolut.Checked)
            {
                Absolut = "T";
            }
            else
            {
                Absolut = "F";
            }

            if (chbBallentine.Checked)
            {
                Ballentine = "T";
            }
            else
            {
                Ballentine = "F";
            }

            if (chbBlend285.Checked)
            {
                Blend = "T";
            }
            else
            {
                Blend = "F";
            }

            if (chbChivas.Checked)
            {
                Chivas = "T";
            }
            else
            {
                Chivas = "F";
            }

            if (chbDewar.Checked)
            {
                Dewar = "T";
            }
            else
            {
                Dewar = "F";
            }

            if (chb100Pipers.Checked)
            {
                Pre100Pipers = "T";
            }
            else
            {
                Pre100Pipers = "F";
            }

            if (chb100Pipers8Y.Checked)
            {
                Pre100Pipers8Y = "T";
            }
            else
            {
                Pre100Pipers8Y = "F";
            }

            if (chbHennessy.Checked)
            {
                Hennessy = "T";
            }
            else
            {
                Hennessy = "F";
            }

            if (chbOther.Checked)
            {
                BrandPreOther = "T";
            }
            else
            {
                BrandPreOther = "F";
            }

            if (chbNotDrink.Checked)
            {
                BrandPreNotDrink = "T";
            }
            else
            {
                BrandPreNotDrink = "F";
            }

            // OptIn

            if (chbDiageoOptIn.Checked)
            {
                DiadeoOptIn = "T";
            }
            else
            {
                DiadeoOptIn = "F";
            }

            if (chbOutletOptIn.Checked)
            {
                OutletOptIn = "T";
            }
            else
            {
                OutletOptIn = "F";
            }

            //Include Bouce

            if (chbHardbounceSMS.Checked)
            {
                HardbounceSMS = "T";
            }
            else
            {
                HardbounceSMS = "F";
            }

            if (chbHardbounceEDM.Checked)
            {
                HardbounceEDM = "T";
            }
            else
            {
                HardbounceEDM = "F";
            }

            if (chbHardbounceCallCenter.Checked)
            {
                HardbounceCallCenter = "T";
            }
            else
            {
                HardbounceCallCenter = "F";
            }

            #endregion

            //Add Data to DataTable

            dtEvent.Rows.Add(new object[] { txtEventName.Text, JwGold, JwBlack, Smirnoff, JwRed, JwGreen, Benmore, 
                Baileys, Absolut, Ballentine, Blend, Chivas, Dewar, Pre100Pipers, Pre100Pipers8Y, Hennessy, BrandPreOther, 
                BrandPreNotDrink, rdoGender.SelectedValue, DiadeoOptIn, OutletOptIn, txtStartAge.Text, txtEndAge.Text, 
                rdoEmailActive.SelectedValue, rdoMobileActive.SelectedValue, HardbounceSMS, HardbounceEDM, HardbounceCallCenter, 
                rdoActive.SelectedValue } );

            return dtEvent;

        }
        catch (Exception ex)
        {
            
            throw new Exception(ex.Message);
        }

    }

    //private DataTable BindEventPlatform()
    //{
    //    DataTable dtEventPlatform;

    //    try
    //    {
    //        dtEventPlatform = new DataTable();
    //        dtEventPlatform.Columns.Add(new DataColumn("PlatformId", typeof(string)));

    //        foreach (ListItem item in listPlatform.Items)
    //        {
    //            dtEventPlatform.Rows.Add(new object[] { item.Value });
    //        }

    //        return dtEventPlatform;
    //    }
    //    catch (Exception ex)
    //    {
            
    //        throw new Exception(ex.Message);
    //    }

    //}

    //private DataTable BindEventOutlet()
    //{
    //    DataTable dtEventOutlet;

    //    try
    //    {
    //        dtEventOutlet = new DataTable();
    //        dtEventOutlet.Columns.Add(new DataColumn("OutletId", typeof(string)));

    //        foreach (ListItem item in listOutletRegister.Items)
    //        {
    //            dtEventOutlet.Rows.Add(new object[] { item.Value });
    //        }

    //        return dtEventOutlet;
    //    }
    //    catch (Exception ex)
    //    {

    //        throw new Exception(ex.Message);
    //    }

    //}

    //private DataTable BindEventProvince()
    //{
    //    DataTable dtEventProvince;

    //    try
    //    {
    //        dtEventProvince = new DataTable();
    //        dtEventProvince.Columns.Add(new DataColumn("ProvinceId", typeof(string)));

    //        foreach (ListItem item in listProvince.Items)
    //        {
    //            dtEventProvince.Rows.Add(new object[] { item.Value });
    //        }

    //        return dtEventProvince;
    //    }
    //    catch (Exception ex)
    //    {

    //        throw new Exception(ex.Message);
    //    }

    //}

    private DataTable GetDataEventPlatform(DataTable dt, string DataGroup, ArrayList ArrPlatformID)
    {
        DIAGEO.COMMON.User User = new DIAGEO.COMMON.User();
        DataRow dr;
        try
        {
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("PlatformID");
                dt.Columns.Add("DataGroup");
                dt.Columns.Add("CreateBy");
                dt.Columns.Add("CreateDate");
                dt.Columns.Add("UpdateBy");
                dt.Columns.Add("UpdateDate");
            }

            for (int i = 0; i < ArrPlatformID.Count; i++)
            {
                dr = dt.NewRow();
                dr["PlatformID"] = ArrPlatformID[i].ToString();
                dr["DataGroup"] = DataGroup;
                dr["CreateBy"] = User.UserID;
                dr["CreateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                dr["UpdateBy"] = User.UserID;
                dr["UpdateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                dt.Rows.Add(dr);
            }

            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception("GetDataEventPlatform :: " + ex.Message);
        }
    }

    private DataTable GetDataEventOutlet(DataTable dt, string DataGroup, ArrayList ArrOutletID)
    {
        DIAGEO.COMMON.User User = new DIAGEO.COMMON.User();
        DataRow dr;
        try
        {
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("OutletID");
                dt.Columns.Add("DataGroup");
                dt.Columns.Add("CreateBy");
                dt.Columns.Add("CreateDate");
                dt.Columns.Add("UpdateBy");
                dt.Columns.Add("UpdateDate");
            }

            for (int i = 0; i < ArrOutletID.Count; i++)
            {
                dr = dt.NewRow();
                dr["OutletID"] = ArrOutletID[i].ToString();
                dr["DataGroup"] = DataGroup;
                dr["CreateBy"] = User.UserID;
                dr["CreateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                dr["UpdateBy"] = User.UserID;
                dr["UpdateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                dt.Rows.Add(dr);
            }

            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception("GetDataEventOutlet :: " + ex.Message);
        }
    }

    private DataTable GetDataEventProvince(DataTable dt, string DataGroup, ArrayList ArrProvinceID)
    {
        DIAGEO.COMMON.User User = new DIAGEO.COMMON.User();
        DataRow dr;
        try
        {
            if (dt.Columns.Count == 0)
            {
                dt.Columns.Add("ProvinceID");
                dt.Columns.Add("DataGroup");
                dt.Columns.Add("CreateBy");
                dt.Columns.Add("CreateDate");
                dt.Columns.Add("UpdateBy");
                dt.Columns.Add("UpdateDate");
            }

            for (int i = 0; i < ArrProvinceID.Count; i++)
            {
                dr = dt.NewRow();
                dr["ProvinceID"] = ArrProvinceID[i].ToString();
                dr["DataGroup"] = DataGroup;
                dr["CreateBy"] = User.UserID;
                dr["CreateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                dr["UpdateBy"] = User.UserID;
                dr["UpdateDate"] = DateTime.Now.ToString("dd/MM/yyyy");
                dt.Rows.Add(dr);
            }

            return dt;
        }
        catch (Exception ex)
        {
            throw new Exception("GetDataEventProvince :: " + ex.Message);
        }
    }

    public void BindData()
    {
        OutBoundEventMasterBLL outBoundEventMasterBLL;
        OutboundEventBLL outboundEventBLL;
        EventPlatformBLL eventPlatformBLL;
        EventOutletBLL eventOutletBLL;
        EventProvinceBLL eventProvinceBLL;

        DataTable dtOutboundEventMaster, dtOutboundEvent, dtPlatform, dtOutlet, dtProvince;

        try
        {
            outBoundEventMasterBLL = new OutBoundEventMasterBLL();
            outboundEventBLL = new OutboundEventBLL();
            eventPlatformBLL = new EventPlatformBLL();
            eventOutletBLL = new EventOutletBLL();
            eventProvinceBLL = new EventProvinceBLL();
            //DataRow Row;

            dtOutboundEventMaster = outBoundEventMasterBLL.SelectAllByEventID(int.Parse(Request.QueryString["EventId"].ToString()));
            dtOutboundEvent = outboundEventBLL.SelectByEventID(int.Parse(Request.QueryString["EventId"].ToString()));
            dtPlatform = eventPlatformBLL.SelectByEventID(int.Parse(Request.QueryString["EventId"].ToString()));
            dtOutlet = eventOutletBLL.SelectByEventID(int.Parse(Request.QueryString["EventId"].ToString()));
            dtProvince = eventProvinceBLL.SelectByEventID(int.Parse(Request.QueryString["EventId"].ToString()));

            // BindData OutboundEventMaster

            if (dtOutboundEventMaster.Rows.Count != 0)
            {
                txtEventName.Text = dtOutboundEventMaster.Rows[0]["eventName"].ToString();

                if (dtOutboundEventMaster.Rows[0]["excludeBlockList"].ToString() == "T")
                {
                    chbYes.Checked = true;
                }
                else
                {
                    chbYes.Checked = false;
                }
            }

            #region BindDataOutboundEvent

            // BindData OutboundEvent

            if (dtOutboundEvent.Rows.Count != 0)
            {
                if (dtOutboundEvent.Rows[0]["brandPreBaileys"].ToString() == "T")
                {
                    chbBaileys.Checked = true;
                }
                else
                {
                    chbBaileys.Checked = false;
                }

                if (dtOutboundEvent.Rows[0]["brandPreBenmore"].ToString() == "T")
                {
                    chbBenmore.Checked = true;
                }
                else
                {
                    chbBenmore.Checked = false;
                }

                if (dtOutboundEvent.Rows[0]["brandPreJwBlack"].ToString() == "T")
                {
                    chbJwBlack.Checked = true;
                }
                else
                {
                    chbJwBlack.Checked = false;
                }

                if (dtOutboundEvent.Rows[0]["brandPreJwGold"].ToString() == "T")
                {
                    chbJwGold.Checked = true;
                }
                else
                {
                    chbJwGold.Checked = false;
                }

                if (dtOutboundEvent.Rows[0]["brandPreJwGreen"].ToString() == "T")
                {
                    chbJwGreen.Checked = true;
                }
                else
                {
                    chbJwGreen.Checked = false;
                }

                if (dtOutboundEvent.Rows[0]["brandPreJwRed"].ToString() == "T")
                {
                    chbJwRed.Checked = true;
                }
                else
                {
                    chbJwRed.Checked = false;
                }

                if (dtOutboundEvent.Rows[0]["brandPreSmirnoff"].ToString() == "T")
                {
                    chbSmirnoff.Checked = true;
                }
                else
                {
                    chbSmirnoff.Checked = false;
                }
                if (dtOutboundEvent.Rows[0]["brandPreAbsolut"].ToString() == "T")
                {
                    chbAbsolut.Checked = true;
                }
                else
                {
                    chbAbsolut.Checked = false;
                }

                if (dtOutboundEvent.Rows[0]["brandPreBallentine"].ToString() == "T")
                {
                    chbBallentine.Checked = true;
                }
                else
                {
                    chbBallentine.Checked = false;
                }

                if (dtOutboundEvent.Rows[0]["brandPreBlend"].ToString() == "T")
                {
                    chbBlend285.Checked = true;
                }
                else
                {
                    chbBlend285.Checked = false;
                }

                if (dtOutboundEvent.Rows[0]["brandPreChivas"].ToString() == "T")
                {
                    chbChivas.Checked = true;
                }
                else
                {
                    chbChivas.Checked = false;
                }

                if (dtOutboundEvent.Rows[0]["brandPreDewar"].ToString() == "T")
                {
                    chbDewar.Checked = true;
                }
                else
                {
                    chbDewar.Checked = false;
                }

                if (dtOutboundEvent.Rows[0]["brandPre100Pipers"].ToString() == "T")
                {
                    chb100Pipers.Checked = true;
                }
                else
                {
                    chb100Pipers.Checked = false;
                }

                if (dtOutboundEvent.Rows[0]["brandPre100Pipers8Y"].ToString() == "T")
                {
                    chb100Pipers8Y.Checked = true;
                }
                else
                {
                    chb100Pipers8Y.Checked = false;
                }
                if (dtOutboundEvent.Rows[0]["brandPreHennessy"].ToString() == "T")
                {
                    chbHennessy.Checked = true;
                }
                else
                {
                    chbHennessy.Checked = false;
                }
                if (dtOutboundEvent.Rows[0]["brandPreOther"].ToString() == "T")
                {
                    chbOther.Checked = true;
                }
                else
                {
                    chbOther.Checked = false;
                }
                if (dtOutboundEvent.Rows[0]["brandPreNotDrink"].ToString() == "T")
                {
                    chbNotDrink.Checked = true;
                }
                else
                {
                    chbNotDrink.Checked = false;
                }

                rdoGender.SelectedValue = dtOutboundEvent.Rows[0]["gender"].ToString();


                if (dtOutboundEvent.Rows[0]["validOptInDiageo"].ToString() == "T")
                {
                    chbDiageoOptIn.Checked = true;
                }
                else
                {
                    chbDiageoOptIn.Checked = false;
                }
                if (dtOutboundEvent.Rows[0]["validOptInOutlet"].ToString() == "T")
                {
                    chbOutletOptIn.Checked = true;
                }
                else
                {
                    chbOutletOptIn.Checked = false;
                }

                txtStartAge.Text = dtOutboundEvent.Rows[0]["startAge"].ToString();
                txtEndAge.Text = dtOutboundEvent.Rows[0]["endAge"].ToString();
                rdoEmailActive.SelectedValue = dtOutboundEvent.Rows[0]["emailActive"].ToString();
                rdoMobileActive.SelectedValue = dtOutboundEvent.Rows[0]["mobileActive"].ToString();

                if (dtOutboundEvent.Rows[0]["includeSMSBounce"].ToString() == "T")
                {
                    chbHardbounceSMS.Checked = true;
                }
                else
                {
                    chbHardbounceSMS.Checked = false;
                }
                if (dtOutboundEvent.Rows[0]["includeCallcenterBounce"].ToString() == "T")
                {
                    chbHardbounceCallCenter.Checked = true;
                }
                else
                {
                    chbHardbounceCallCenter.Checked = false;
                }
                if (dtOutboundEvent.Rows[0]["includeEDMBounce"].ToString() == "T")
                {
                    chbHardbounceEDM.Checked = true;
                }
                else
                {
                    chbHardbounceEDM.Checked = false;
                }

                rdoActive.SelectedValue = dtOutboundEvent.Rows[0]["active"].ToString();

            }

            #endregion

            #region Bind Event Old



            //Bind EventPlatform

            //if (dtPlatform.Rows.Count != 0)
            //{
            //    DataTable dtPlatformTemp = new DataTable();
            //    dtPlatformTemp.Columns.Add(new DataColumn("Name", typeof(string)));
            //    dtPlatformTemp.Columns.Add(new DataColumn("Value", typeof(string)));

            //    for (int i = 0; i < dtPlatform.Rows.Count; i++)
            //    {
            //        Row = dtPlatform.Rows[i];

            //        dtPlatformTemp.Rows.Add(new object[] { Row["name"].ToString(), Row["platformId"].ToString() });
            //    }

            //    listPlatform.DataTextField = "Name";
            //    listPlatform.DataValueField = "Value";
            //    listPlatform.DataSource = dtPlatformTemp;
            //    listPlatform.DataBind();
            //}

            //Bind EventOutlet

            //if (dtOutlet.Rows.Count != 0)
            //{
            //    DataTable dtOutletTemp = new DataTable();
            //    dtOutletTemp.Columns.Add(new DataColumn("Name", typeof(string)));
            //    dtOutletTemp.Columns.Add(new DataColumn("Value", typeof(string)));

            //    for (int i = 0; i < dtOutlet.Rows.Count; i++)
            //    {
            //        Row = dtOutlet.Rows[i];

            //        dtOutletTemp.Rows.Add(new object[] { Row["name"].ToString(), Row["outletId"].ToString() });
            //    }

            //    listOutletRegister.DataTextField = "Name";
            //    listOutletRegister.DataValueField = "Value";
            //    listOutletRegister.DataSource = dtOutletTemp;
            //    listOutletRegister.DataBind();
            //}

            //Bind EventProvince

            //if (dtProvince.Rows.Count != 0)
            //{
            //    DataTable dtProvinceTemp = new DataTable();
            //    dtProvinceTemp.Columns.Add(new DataColumn("Name", typeof(string)));
            //    dtProvinceTemp.Columns.Add(new DataColumn("Value", typeof(string)));

            //    for (int i = 0; i < dtProvince.Rows.Count; i++)
            //    {
            //        Row = dtProvince.Rows[i];

            //        dtProvinceTemp.Rows.Add(new object[] { Row["name"].ToString(), Row["provinceId"].ToString() });
            //    }

            //    listProvince.DataTextField = "Name";
            //    listProvince.DataValueField = "Value";
            //    listProvince.DataSource = dtProvinceTemp;
            //    listProvince.DataBind();
            //}

            #endregion

            BindItemPlatform(dtPlatform, lblTextPlatform);
            BindItemOutlet(dtOutlet, lblTextOutlet);
            BindItemProvince(dtProvince, lblTextProvice);

        }
        catch (Exception ex)
        {
            throw new Exception("BindData :: " + ex.Message);
        }
    }

    private void DeleteSesion()
    {
        Session["ArrPlatformID" + "0"] = null;
        Session["ArrPlatformNAME" + "0"] = null;
        Session["ArrOutletID" + "0"] = null;
        Session["ArrOutletNAME" + "0"] = null;
        Session["ArrProvinceID" + "0"] = null;
        Session["ArrProvinceNAME" + "0"] = null;
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {

                txtEventName.ReadOnly = false;
                txtStartAge.ReadOnly = false;
                txtEndAge.ReadOnly = false;
                txtEventName.Enabled = true;
                txtStartAge.Enabled = true;
                txtEndAge.Enabled = true;

                if (Request.QueryString["Mode"].ToString() == "Edit")
                {
                    txtEventName.ReadOnly = false;
                    txtStartAge.ReadOnly = false;
                    txtEndAge.ReadOnly = false;
                    txtEventName.Enabled = true;
                    txtStartAge.Enabled = true;
                    txtEndAge.Enabled = true;

                    BindData();

                }
                else
                {
                    txtEventName.ReadOnly = false;
                    txtStartAge.ReadOnly = false;
                    txtEndAge.ReadOnly = false;
                    txtEventName.Enabled = true;
                    txtStartAge.Enabled = true;
                    txtEndAge.Enabled = true;

                }

            }
            else
            {
                //DeleteSesion();
            }

        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Outlet.PageLoad :: " + ex.Message);
        }
    }

    protected void btnChoosePlatform_Click(object sender, EventArgs e)
    {
        try
        {
            string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/ExtractData/PlatFormPopup.aspx?tabNumber=0&date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:500px; center:yes; scroll:yes'); document.getElementById('btnRefresh').click();";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenPopup", window, true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnChooseOutletRegister_Click(object sender, EventArgs e)
    {
        try
        {
            string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/ExtractData/OutletPopup.aspx?tabNumber=0&date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:500px; center:yes; scroll:yes'); document.getElementById('btnRefresh').click();";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenPopup", window, true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnChooseProvince_Click(object sender, EventArgs e)
    {
        try
        {
            string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/ExtractData/ProvincePopup.aspx?tabNumber=0&date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:500px; center:yes; scroll:yes'); document.getElementById('btnRefresh').click();";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "OpenPopup", window, true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        OutboundEventBLL outboundEventBLL;
        DataTable dtEventMaster, dtEvent, dtPlatform, dtOutlet, dtProvince;
        User user;
        UserActivityLogBLL userActivityLogBll;



        try
        {

            DataTable dtEventPlatform = new DataTable("EventPlatform");
            DataTable dtEventOutlet = new DataTable("EventOutlet");
            DataTable dtEventProvince = new DataTable("EventProvince");
            ArrayList ArrPlatformID, ArrOutletID, ArrProvinceID;
            string DataGroup = "";
            outboundEventBLL = new OutboundEventBLL();
            user = new User();
            userActivityLogBll = new UserActivityLogBLL();

            if (ValidateSave(Request.QueryString["Mode"].ToString()))
            {
                DataGroup = "1";
                ArrPlatformID = ((ArrayList)Session["ArrPlatformID" + "0"] == null) ? new ArrayList() : (ArrayList)Session["ArrPlatformID" + "0"];
                ArrOutletID = ((ArrayList)Session["ArrOutletID" + "0"] == null) ? new ArrayList() : (ArrayList)Session["ArrOutletID" + "0"];
                ArrProvinceID = ((ArrayList)Session["ArrProvinceID" + "0"] == null) ? new ArrayList() : (ArrayList)Session["ArrProvinceID" + "0"];

                dtEventMaster = BindEventMaster();
                dtEvent = BindEvent();
                dtPlatform = GetDataEventPlatform(dtEventPlatform, DataGroup, ArrPlatformID);
                dtOutlet = GetDataEventOutlet(dtEventOutlet, DataGroup, ArrOutletID);
                dtProvince = GetDataEventProvince(dtEventProvince, DataGroup, ArrProvinceID);

                //dtPlatform = BindEventPlatform();
                //dtOutlet = BindEventOutlet();
                //dtProvince = BindEventProvince();

                if (Request.QueryString["Mode"].ToString() == "Edit")
                {

                    if (CanEdit)
                    {
                        outboundEventBLL.EventID = int.Parse(Request.QueryString["EventId"].ToString());
                        if (outboundEventBLL.UpDateOutboundEventBirthDay(dtEventMaster, dtEvent, dtPlatform, dtOutlet, dtProvince, user.UserID))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "OutboundEvent_Edit");
                            DeleteSesion();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "xxx", "unblockUI();", true);
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();window.opener.document.getElementById('btnSearch').click();");
                        }
                    }
                    else
                    {
                        DeleteSesion();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "xxx", "unblockUI();", true);
                        JsClientCustom("OutboundEventSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
                    }
                }
                else
                {

                    if (CanAdd)
                    {
                        if (outboundEventBLL.InsertOutboundEventBirthDay(dtEventMaster, dtEvent, dtPlatform, dtOutlet, dtProvince, user.UserID))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "OutboundEvent_Add");
                            DeleteSesion();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "xxx", "unblockUI();", true);
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();window.opener.document.getElementById('btnSearch').click();");
                        }
                    }
                    else
                    {
                        DeleteSesion();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "xxx", "unblockUI();", true);
                        JsClientCustom("OutboundEventSearch", "alert('ไม่มีสิทธิ์เพิ่มข้อมูล');window.close();");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            DeleteSesion();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "xxx", "unblockUI();", true);
            JsClientAlert("ErrorMessage", "Outlet.btnSave_Click :: " + ex.Message);
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            DeleteSesion();
            JsClientCustom("WindowClose", "window.close();");
        }
        catch (Exception ex)
        {
            DeleteSesion();
            JsClientAlert("ErrorMessage", "UserGroup.btnCancel_Click :: " + ex.Message);
        }
    }

    protected void btnRefresh_Click(object sender, EventArgs e)
    {

        try
        {
            BindItemPlatformL(lblTextPlatform);
            BindItemOutletL(lblTextOutlet);
            BindItemProvinceL(lblTextProvice);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroup.btnRefresh_Click :: " + ex.Message);
        }
    }

    #endregion
}
