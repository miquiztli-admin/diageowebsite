﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Master_OutboundType : BasePage
{
    #region Method

    public bool ValidateSave(string mode)
    {
        OutboundTypeBLL outboundTypeBLL;
        DataTable dtOutboundTypeBLL;

        try
        {
            if (txtOutboundTypeName.Text == "")
            {
                JsClientAlert("OutboundTypeName", "โปรดกรอก OutboundType Name");
                return false;
            }
            outboundTypeBLL = new OutboundTypeBLL();
            if (mode == "Edit")
            {
                if (txtOutboundTypeName.Text != Request.QueryString["name"].ToString())
                {
                    dtOutboundTypeBLL = outboundTypeBLL.SelectOneBYOutboundTypeName(txtOutboundTypeName.Text);
                    if (dtOutboundTypeBLL.Rows.Count != 0)
                    {
                        JsClientAlert("OutboundTypeNameHasUse", "OutboundType Name นี้มีผู้ใช้เเล้ว");
                        return false;
                    }
                }


            }
            else
            {
                dtOutboundTypeBLL = outboundTypeBLL.SelectOneBYOutboundTypeName(txtOutboundTypeName.Text);
                if (dtOutboundTypeBLL.Rows.Count != 0)
                {
                    JsClientAlert("OutboundTypeNameHasUse", "OutboundType Name นี้มีผู้ใช้เเล้ว");
                    return false;
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateSave :: " + ex.Message);
        }
    }

    public void BindData()
    {
        OutboundTypeBLL outboundTypeBLL;
        DataTable dtOutboundTypeBLL;

        try
        {
            outboundTypeBLL = new OutboundTypeBLL();
            dtOutboundTypeBLL = outboundTypeBLL.SelectOne(int.Parse(Request.QueryString["OutboundTypeId"].ToString()));
            if (dtOutboundTypeBLL.Rows.Count != 0)
            {
                txtOutboundTypeName.Text = dtOutboundTypeBLL.Rows[0]["name"].ToString();
                ddlOutboundTypeFormat.SelectedValue = dtOutboundTypeBLL.Rows[0]["format"].ToString();
                rdoActive.SelectedValue = dtOutboundTypeBLL.Rows[0]["active"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindData :: " + ex.Message);
        }
    }


    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {
                    txtOutboundTypeName.ReadOnly = false;
                    txtOutboundTypeName.Enabled = true;

                    BindData();

                }
                else
                {
                    txtOutboundTypeName.ReadOnly = false;
                    txtOutboundTypeName.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutboundType.PageLoad :: " + ex.Message);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        OutboundTypeBLL outboundTypeBLL;
        User user;
        UserActivityLogBLL userActivityLogBll;


        try
        {
            outboundTypeBLL = new OutboundTypeBLL();
            user = new User();
            userActivityLogBll = new UserActivityLogBLL();

            if (ValidateSave(Request.QueryString["Mode"].ToString()))
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {

                    if (CanEdit)
                    {
                        if (outboundTypeBLL.UpdateSearch(Request.QueryString["OutboundTypeId"].ToString(), txtOutboundTypeName.Text, ddlOutboundTypeFormat.SelectedValue, rdoActive.SelectedValue, user.UserID))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "OutboundType_Edit");
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                        }
                    }
                    else
                    {                      
                            JsClientCustom("OutboundTypeSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");          
                    }
                }
                else
                {

                        if (CanAdd)
                        {

                            if (outboundTypeBLL.Insert(txtOutboundTypeName.Text, ddlOutboundTypeFormat.SelectedValue, rdoActive.SelectedValue, user.UserID))
                            {
                                userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "OutboundType_Add");
                                JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                            }
                        }
                        else
                        {
                            JsClientCustom("OutboundTypeSearch", "alert('ไม่มีสิทธิ์เพิ่มข้อมูล');window.close();");
                        }
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutboundType.btnSave_Click :: " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            JsClientCustom("WindowClose", "window.close();");
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutboundType.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion

}
