﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ConsumerSearch.aspx.cs" Inherits="Master_ConsumerSearch" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="ITOS.Utility.CustomControl" Namespace="ITOS.Utility.CustomControl"
    TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Consumer Search</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../Style/block.css" rel="stylesheet" type="text/css" />
    <link href="../Style/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    <script type="text/javascript">  
      function  blockUI()
      {
        $.blockUI({message: '<h1><img src="../Image/Load.gif" /></h1>'});
      }
    </script>
     
    <script type="text/javascript">  
      function  unblockUI()
      {
        $.unblockUI();
      }
    </script>
    
</head>
<body >
    <form id="FrmUserSearch" runat="server" onmousemove="SetProgressPosition();">
    <asp:ToolkitScriptManager ID="scriptManager" EnablePartialRendering="true" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" runat="server">
    </asp:ToolkitScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="100" DynamicLayout="true">
        <ProgressTemplate>
            <div class="ntextblackOverley" id="divProgress">
                &nbsp; Please wait... &nbsp;
                <br />
                &nbsp;
                <asp:Image GenerateEmptyAlternateText="true" ID="Image1" runat="server" ImageUrl="~/Image/Load.gif"
                    Width="40px" Height="40px" Style="margin-top: 7px;" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="upHome" runat="server">
        <ContentTemplate>
            <div>
                <fieldset>
                    <legend class="ntextblack">Consumer</legend>
                    <table align="center">
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblIdCard" CssClass="ntextblack" runat="server" Text="National ID : "></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtIdCard" CssClass="ntextblackdata" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblFristName" CssClass="ntextblack" runat="server" Text="Frist Name : "></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtFristName" CssClass="ntextblackdata" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblLastName" CssClass="ntextblack" runat="server" Text="Last Name : "></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtLastName" CssClass="ntextblackdata" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblEmail" CssClass="ntextblack" runat="server" Text="E-mail : "></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtEmail" CssClass="ntextblackdata" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="lblMobile" CssClass="ntextblack" runat="server" Text="Mobile No : "></asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtMobile" CssClass="ntextblackdata" 
                                    OnKeyPress="validateNumKey()" runat="server" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <table align="center" style="width: 900px">
                        <tr>
                            <td align="center">
                                <asp:Button ID="btnSearch" CssClass="ntextblack" runat="server" Text="Search" OnClick="btnSearch_Click"/>&nbsp;
                                <asp:Button ID="btnCancel" CssClass="ntextblack" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:DataGrid ID="dgSearch" Width="900px" HeaderStyle-CssClass="gridheaderrowstyle"
                                    runat="server" AutoGenerateColumns="false" OnItemDataBound="dgSearch_ItemDataBound"
                                    OnItemCommand="dgSearch_ItemCommand" ItemStyle-CssClass="gridrowstyle" AlternatingItemStyle-CssClass="gridalternativerowstyle">
                                    <Columns>
                                        <asp:BoundColumn DataField="idCard" HeaderText="National ID" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="firstName" HeaderText="Frist Name" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="lastName" HeaderText="Last Name" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="email" HeaderText="E-mail" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="mobile" HeaderText="Mobile No" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <ItemTemplate>
                                                <asp:Button ID="btnEdit" Width="100px" runat="server" Text="Edit" CssClass="ntextblack"
                                                    CommandName="Edit" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="consumerId" HeaderText="ConsumerId" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="tractionId" HeaderText="TractionId" Visible="false"></asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                    <table id="tableNavigation" visible="false" runat="server" class="ntextblack" align="center">
                        <tr>
                            <td>
                                <cc1:MultiPageNavigation ID="pgSearch" runat="server" HaveTextBox="true" CssClass="ntextblackdata"
                                    NavigatorAlign="right" OnGoClick="pgSearch_GoClick" OnPageClick="pgSearch_PageClick"
                                    StringNextPage=">>" StringPreviousPage="<<" OnRowPerPageSelectIndexChanged="pgSearch_RowPerPageSelectIndexChanged">
                                </cc1:MultiPageNavigation>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>