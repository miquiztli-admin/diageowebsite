﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Master_OutboundConfigSearch : BasePage
{

    #region Property

    //public int Platform { get { return int.Parse(ViewState["PlatformId"].ToString()); } set { ViewState["PlatformId"] = value; } }

    //public int Outlet { get { return int.Parse(ViewState["OutletId"].ToString()); } set { ViewState["OutletId"] = value; } }

    public int OutboundEvent { get { return int.Parse(ViewState["OutboundEventId"].ToString()); } set { ViewState["OutboundEventId"] = value; } }

    public int OutboundType { get { return int.Parse(ViewState["OutboundTypeId"].ToString()); } set { ViewState["OutboundTypeId"] = value; } }

    public string Active { get { return ViewState["Active"].ToString(); } set { ViewState["Active"] = value; } }

    #endregion

    #region Method

    //public void BindPlatform()
    //{
    //    PlatformBLL platformBLL;
    //    DataTable dtPlatform;

    //    try
    //    {
    //        platformBLL = new PlatformBLL();
    //        dtPlatform = platformBLL.SelectActivePlatform("T");
    //        ddlPlatform.DataTextField = "name";
    //        ddlPlatform.DataValueField = "platformId";
    //        ddlPlatform.DataSource = dtPlatform;
    //        ddlPlatform.DataBind();
    //        ddlPlatform.Items.Insert(0, new ListItem("เลือก Platform", "0"));
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception("BindPlatform :: " + ex.Message);
    //    }
    //}

    //public void BindOutlet()
    //{
    //    OutletBLL outletBLL;
    //    DataTable dtOutlet;

    //    try
    //    {
    //        outletBLL = new OutletBLL();
    //        dtOutlet = outletBLL.SelectActiveOutlet("T");
    //        ddlOutlet.DataTextField = "name";
    //        ddlOutlet.DataValueField = "outletId";
    //        ddlOutlet.DataSource = dtOutlet;
    //        ddlOutlet.DataBind();
    //        ddlOutlet.Items.Insert(0, new ListItem("เลือก Outlet", "0"));
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception("BindOutlet :: " + ex.Message);
    //    }
    //}

    public void BindOutboundEvent()
    {
        OutboundEventBLL outboundEventBLL;
        DataTable dtOutboundEvent;

        try
        {
            outboundEventBLL = new OutboundEventBLL();
            dtOutboundEvent = outboundEventBLL.SelectActiveOutboundEventMaster();
            ddlOutboundEvent.DataTextField = "eventName";
            ddlOutboundEvent.DataValueField = "eventId";
            ddlOutboundEvent.DataSource = dtOutboundEvent;
            ddlOutboundEvent.DataBind();
            ddlOutboundEvent.Items.Insert(0, new ListItem("เลือก Event", "0"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindOutboundEvent :: " + ex.Message);
        }
    }

    public void BindOutboundType()
    {
        OutboundTypeBLL outboundTypeBLL;
        DataTable dtOutboundType;

        try
        {
            outboundTypeBLL = new OutboundTypeBLL();
            dtOutboundType = outboundTypeBLL.SelectActiveOutboundType("T");
            ddlOutboundType.DataTextField = "name";
            ddlOutboundType.DataValueField = "outboundTypeId";
            ddlOutboundType.DataSource = dtOutboundType;
            ddlOutboundType.DataBind();
            ddlOutboundType.Items.Insert(0, new ListItem("เลือก OutboundType", "0"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindOutboundType :: " + ex.Message);
        }
    }

    public void BindOutboundConfig(int pageNo)
    {
        OutboundConfigBLL outboundConfigBLL;
        DataTable dtSearch;
        int count;

        try
        {
            outboundConfigBLL = new OutboundConfigBLL();

            if (pageNo == 1)
                dtSearch = outboundConfigBLL.Search(0, 0, OutboundEvent, OutboundType, Active, 0, RowPerPage, "", 0);
            else
                dtSearch = outboundConfigBLL.Search(0, 0, OutboundEvent, OutboundType, Active, (pageNo - 1) * RowPerPage, RowPerPage, "", 0);

            count = int.Parse(outboundConfigBLL.Search(0, 0, OutboundEvent, OutboundType, Active, 0, RowPerPage, "", 1).Rows[0][0].ToString());

            pgSearch.RowPerPage = RowPerPage;
            pgSearch.CheckStringButton = pageNo.ToString();
            pgSearch.RowCount = count;
            pgSearch.AllowPagingOnDataGrid = false;
            pgSearch.DataSource = dtSearch;
            pgSearch.DataGridBind = dgSearch;
            pgSearch.DataBind();
            tableNavigation.Visible = true;
        }
        catch (Exception ex)
        {
            throw new Exception("BindOutboundConfig :: " + ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {
                RowPerPage = int.Parse(ConfigurationManager.AppSettings["RowPerPage"].ToString());
                //BindPlatform();
                //BindOutlet();
                BindOutboundEvent();
                BindOutboundType();
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutboundConfigSearch.Page_Load :: " + ex.Message);
        }
    }

    protected void dgSearch_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutboundConfigSearch.dgSearch_ItemDataBound :: " + ex.Message);
        }
    }

    protected void dgSearch_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        try
        {
            
            if (CanEdit)
            {

                if (e.CommandName == "Edit")
                {
                    Session["OutboundEventInOutboundConfig"] = e.Item.Cells[7].Text;
                    Session["OutboundTypeInOutboundConfig"] = e.Item.Cells[8].Text;
                    string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/OutboundConfig.aspx?Mode=Edit&outboundConfigId=" + e.Item.Cells[6].Text + "&Date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:400px; center:yes; scroll:yes'); document.getElementById('btnSearch').click();";
                    JsClientCustom("SavePopUp", window);

                    //string url = "http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/OutboundConfig.aspx?Mode=Edit&outboundConfigId=" + e.Item.Cells[6].Text + "&Date=" + DateTime.Now.ToString();
                    //string window = "popupFullscreen('" + url + "');document.getElementById('btnSearch').click();";
                    //JsClientCustom("SavePopUp", window);
                }
            }
            else
            {
                JsClientCustom("OutboundConfigSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutboundConfigSearch.dgSearch_ItemCommand :: " + ex.Message);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            
            if (CanView)
            {

                //Platform = int.Parse(ddlPlatform.SelectedValue);
                //Outlet = int.Parse(ddlOutlet.SelectedValue);
                OutboundEvent = int.Parse(ddlOutboundEvent.SelectedValue);
                OutboundType = int.Parse(ddlOutboundType.SelectedValue);
                Active = rdoActive.SelectedValue;
                BindOutboundConfig(1);
            }
            else
            {
                JsClientCustom("OutboundConfigSearch", "alert('ไม่มีสิทธิ์ค้นหาข้อมูล');window.close();");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutboundConfigSearch.btnSearch_Click :: " + ex.Message);
        }
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        try
        {
            
            if (CanAdd)
            {
                string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/OutboundConfig.aspx?Mode=Insert&Code=&Name=&Date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:400px; center:yes; scroll:yes'); document.getElementById('btnSearch').click();";
                JsClientCustom("SavePopUp", window);

                //string url = "http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/OutboundConfig.aspx?Mode=Insert&Code=&Name=&Date=" + DateTime.Now.ToString();
                //string window = "popupFullscreen('" + url + "'); document.getElementById('btnSearch').click();";
                //JsClientCustom("SavePopUp", window);
            }
            else
            {
                JsClientCustom("OutboundConfigSearch", "alert('ไม่มีสิทธิ์เพิ่มข้อมูล');window.close();");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutboundConfigSearch.btnNew_Click :: " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            //ddlPlatform.SelectedIndex = 0;
            //ddlOutlet.SelectedIndex = 0;
            ddlOutboundEvent.SelectedIndex = 0;
            ddlOutboundType.SelectedIndex = 0;
            rdoActive.SelectedIndex = 0;

            //Platform = 0;
            //Outlet = 0;
            OutboundEvent = 0;
            OutboundType = 0;
            Active = "";
        }
        catch (Exception ex)
        {
            JsClientCustom("ErrorMessage", "UserGroupSearch.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion

    #region Paging

    protected void pgSearch_GoClick(object sender, EventArgs e)
    {
        try
        {
            //Platform = int.Parse(ddlPlatform.SelectedValue);
            //Outlet = int.Parse(ddlOutlet.SelectedValue);
            //OutboundEvent = int.Parse(ddlOutboundEvent.SelectedValue);
            //OutboundType = int.Parse(ddlOutboundType.SelectedValue);
            //Active = rdoActive.SelectedValue;

            string[] strField = Request.Form.GetValues("pgSearch$txtPage");
            int intPage = int.Parse(strField[0]);
            Session["currentPage"] = intPage;
            BindOutboundConfig(intPage);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutboundConfigSearch.pgSearch_GoClick :: " + ex.Message);
        }
    }

    protected void pgSearch_PageClick(object sender, EventArgs e)
    {
        LinkButton lb = sender as LinkButton;

        try
        {
            //Platform = int.Parse(ddlPlatform.SelectedValue);
            //Outlet = int.Parse(ddlOutlet.SelectedValue);
            //OutboundEvent = int.Parse(ddlOutboundEvent.SelectedValue);
            //OutboundType = int.Parse(ddlOutboundType.SelectedValue);
            //Active = rdoActive.SelectedValue;

            int intPage = GetCurrentPageIndex(pgSearch.StartGroupPage, pgSearch.QuantityPageShow, lb.Text);
            Session["currentPage"] = intPage;
            BindOutboundConfig(intPage);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutboundConfigSearch.pgSearch_PageClick :: " + ex.Message);
        }
    }

    protected void pgSearch_RowPerPageSelectIndexChanged(Object sender, EventArgs e)
    {
        try
        {
            //Platform = int.Parse(ddlPlatform.SelectedValue);
            //Outlet = int.Parse(ddlOutlet.SelectedValue);
            //OutboundEvent = int.Parse(ddlOutboundEvent.SelectedValue);
            //OutboundType = int.Parse(ddlOutboundType.SelectedValue);
            //Active = rdoActive.SelectedValue;

            DropDownList ddl = sender as DropDownList;
            pgSearch.RowPerPage = int.Parse(ddl.SelectedValue);
            RowPerPage = int.Parse(ddl.SelectedValue);
            BindOutboundConfig(1);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutboundConfigSearch.pgSearch_RowPerPageSelectIndexChanged :: " + ex.Message);
        }
    }

    #endregion

}
