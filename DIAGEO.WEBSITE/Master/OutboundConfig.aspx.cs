﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;
using ITOS.Utility.Formatter;
using System.Globalization;

public partial class Master_OutboundConfig : BasePage
{
    #region Method

    private DateTime MyDateConversion(string dateAsString)
    {
        return System.DateTime.ParseExact(dateAsString, "dd/MM/yyyy", System.Globalization.CultureInfo.CurrentCulture);
    } 

    public bool ValidateSave(string mode)
    {
        OutboundConfigBLL outboundConfigBLL;
        DataTable dtSearch;
        DateTime SDate, EDate;

        try
        {
            //if (int.Parse(ddlPlatform.SelectedValue) == 0)
            //{
            //    JsClientAlert("Platform", "โปรดเลือก Platform");
            //    return false;
            //}
            //if (int.Parse(ddlOutlet.SelectedValue) == 0)
            //{
            //    JsClientAlert("Outlet", "โปรดเลือก Outlet");
            //    return false;
            //}
            if (int.Parse(ddlOutboundEvent.SelectedValue) == 0)
            {
                JsClientAlert("Event", "โปรดเลือก Event");
                return false;
            }
            if (int.Parse(ddlOutboundType.SelectedValue) == 0)
            {
                JsClientAlert("OutboundType", "โปรดเลือก OutboundType");
                return false;
            }
            if (txtQtyOfDay.Text == "")
            {
                JsClientAlert("QtyOfDay", "โปรดกรอก QtyOfDay");
                return false;
            }
            if (txtStartDate.Text == "")
            {
                JsClientAlert("StartDate", "โปรดกรอก StartDate");
                return false;
            }
            else
            {
                DateTime TempStartDate;
                if (!(DateTime.TryParseExact(txtStartDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TempStartDate)))
                {
                    JsClientAlert("StartDate", " Format StartDate Incorrect ");
                    return false;
                }
            }
            if (txtEndDate.Text == "")
            {
                JsClientAlert("EndDate", "โปรดกรอก EndDate");
                return false;
            }
            else
            {
                DateTime TempStartDate;
                if (!(DateTime.TryParseExact(txtEndDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out TempStartDate)))
                {
                    JsClientAlert("StartDate", " Format EndDate Incorrect ");
                    return false;
                }
            }

            SDate = MyDateConversion(txtStartDate.Text);
            EDate = MyDateConversion(txtEndDate.Text);

            //if (DateTime.Now.Date > SDate)
            //{
            //    JsClientAlert("StartDate", "โปรดกรอก StartDate ให้มากกว่าวันปัจจุบัน");
            //    return false;
            //}

            //if (DateTime.Now.Date > EDate)
            //{
            //    JsClientAlert("EndDate", "โปรดกรอก EndDate ให้มากกว่าวันปัจจุบัน");
            //    return false;
            //} 

            if (SDate > EDate)
            {
                JsClientAlert("EndDate", "โปรดกรอก EndDate ให้มากกว่า StartDate");
                return false;
            }

            if (mode == "Edit")
            {
                if ((ddlOutboundEvent.SelectedValue == Session["OutboundEventInOutboundConfig"].ToString()) && (ddlOutboundType.SelectedValue == Session["OutboundTypeInOutboundConfig"].ToString()))
                {                  
                    return true;
                }
                else
                {
                    outboundConfigBLL = new OutboundConfigBLL();

                    dtSearch = outboundConfigBLL.Search(0, 0, int.Parse(ddlOutboundEvent.SelectedValue), int.Parse(ddlOutboundType.SelectedValue), " ", 0, 5, "", 0);

                    if (dtSearch.Rows.Count > 0)
                    {
                        JsClientAlert("OutboundConfigduplicate", "เงื่อนไข OutboundEvent และ OutboundType ซ้ำ ");
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                    
                }

            }
            else
            {
                outboundConfigBLL = new OutboundConfigBLL();

                dtSearch = outboundConfigBLL.Search(0, 0, int.Parse(ddlOutboundEvent.SelectedValue), int.Parse(ddlOutboundType.SelectedValue), " ", 0, 5, "", 0);

                if (dtSearch.Rows.Count > 0)
                {
                    JsClientAlert("OutboundConfigduplicate", "เงื่อนไข OutboundEvent และ OutboundType ซ้ำ ");
                    return false;
                }
                else
                {
                    return true;
                }

            }
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateSave :: " + ex.Message);
        }
    }

    public void BindData()
    {
        OutboundConfigBLL outboundConfigBLL;
        DataTable dtOutlet;

        try
        {
            outboundConfigBLL = new OutboundConfigBLL();
            dtOutlet = outboundConfigBLL.SelectOne(int.Parse(Request.QueryString["outboundConfigId"].ToString()));
            if (dtOutlet.Rows.Count != 0)
            {
                //ddlPlatform.SelectedValue = dtOutlet.Rows[0]["platformId"].ToString();
                //ddlOutlet.SelectedValue = dtOutlet.Rows[0]["outletId"].ToString();
                ddlOutboundEvent.SelectedValue = dtOutlet.Rows[0]["eventId"].ToString();
                ddlOutboundType.SelectedValue = dtOutlet.Rows[0]["outboundTypeId"].ToString();
                txtQtyOfDay.Text = dtOutlet.Rows[0]["qtyOfDay"].ToString();
                txtStartDate.Text = DateTime.Parse(dtOutlet.Rows[0]["startDate"].ToString()).ToString("dd/MM/yyyy");
                txtEndDate.Text = DateTime.Parse(dtOutlet.Rows[0]["endDate"].ToString()).ToString("dd/MM/yyyy");
                rdoActive.SelectedValue = dtOutlet.Rows[0]["active"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindData :: " + ex.Message);
        }
    }

    //public void BindPlatform()
    //{
    //    PlatformBLL platformBLL;
    //    DataTable dtPlatform;

    //    try
    //    {
    //        platformBLL = new PlatformBLL();
    //        dtPlatform = platformBLL.SelectActivePlatform("T");
    //        ddlPlatform.DataTextField = "name";
    //        ddlPlatform.DataValueField = "platformId";
    //        ddlPlatform.DataSource = dtPlatform;
    //        ddlPlatform.DataBind();
    //        ddlPlatform.Items.Insert(0, new ListItem("เลือก Platform", "0"));
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception("BindPlatform :: " + ex.Message);
    //    }
    //}

    //public void BindOutlet()
    //{
    //    OutletBLL outletBLL;
    //    DataTable dtOutlet;

    //    try
    //    {
    //        outletBLL = new OutletBLL();
    //        dtOutlet = outletBLL.SelectActiveOutlet("T");
    //        ddlOutlet.DataTextField = "name";
    //        ddlOutlet.DataValueField = "outletId";
    //        ddlOutlet.DataSource = dtOutlet;
    //        ddlOutlet.DataBind();
    //        ddlOutlet.Items.Insert(0, new ListItem("เลือก Outlet", "0"));
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception("BindOutlet :: " + ex.Message);
    //    }
    //}

    public void BindOutboundEvent()
    {
        OutboundEventBLL outboundEventBLL;
        DataTable dtOutboundEvent;

        try
        {
            outboundEventBLL = new OutboundEventBLL();
            dtOutboundEvent = outboundEventBLL.SelectActiveOutboundEventMaster();
            ddlOutboundEvent.DataTextField = "eventName";
            ddlOutboundEvent.DataValueField = "eventId";
            ddlOutboundEvent.DataSource = dtOutboundEvent;
            ddlOutboundEvent.DataBind();
            ddlOutboundEvent.Items.Insert(0, new ListItem("เลือก Event", "0"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindOutboundEvent :: " + ex.Message);
        }
    }

    public void BindOutboundType()
    {
        OutboundTypeBLL outboundTypeBLL;
        DataTable dtOutboundType;

        try
        {
            outboundTypeBLL = new OutboundTypeBLL();
            dtOutboundType = outboundTypeBLL.SelectActiveOutboundType("T");
            ddlOutboundType.DataTextField = "name";
            ddlOutboundType.DataValueField = "outboundTypeId";
            ddlOutboundType.DataSource = dtOutboundType;
            ddlOutboundType.DataBind();
            ddlOutboundType.Items.Insert(0, new ListItem("เลือก OutboundType", "0"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindOutboundType :: " + ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {
                    //BindPlatform();
                    //BindOutlet();
                    BindOutboundEvent();
                    BindOutboundType();
                    BindData();
                }
                else
                {
                    //BindPlatform();
                    //BindOutlet();
                    BindOutboundEvent();
                    BindOutboundType();
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutboundConfig.PageLoad :: " + ex.Message);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        OutboundConfigBLL outboundConfigBLL;
        User user;
        UserActivityLogBLL userActivityLogBll;

        try
        {
            outboundConfigBLL = new OutboundConfigBLL();
            user = new User();
            userActivityLogBll = new UserActivityLogBLL();

            if (ValidateSave(Request.QueryString["Mode"].ToString()))
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {
            
                    if (CanEdit)
                    {

                        DateTimeFormatter dateFormater = new DateTimeFormatter();
                        if (outboundConfigBLL.UpdateSearch(Request.QueryString["outboundConfigId"].ToString(), "0", "0", ddlOutboundEvent.SelectedValue, ddlOutboundType.SelectedValue, txtQtyOfDay.Text, dateFormater.Parse(txtStartDate.Text.ToString()), dateFormater.Parse(txtEndDate.Text.ToString()), rdoActive.SelectedValue, user.UserID))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "OutboundConfig_Edit");
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                        }

                    }
                    else
                    {
                        JsClientCustom("OutboundConfigSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
                    }
                }
                else
                {

                    if (CanAdd)
                    {
                        DateTimeFormatter dateFormater = new DateTimeFormatter();
                        if (outboundConfigBLL.Insert("0", "0", ddlOutboundEvent.SelectedValue, ddlOutboundType.SelectedValue, txtQtyOfDay.Text, dateFormater.Parse(txtStartDate.Text.ToString()), dateFormater.Parse(txtEndDate.Text.ToString()), rdoActive.SelectedValue, user.UserID))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "OutboundConfig_Add");
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                        }
                    }
                    else
                    {
                        JsClientCustom("OutboundConfigSearch", "alert('ไม่มีสิทธิ์เพิ่มข้อมูล');window.close();");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Outlet.btnSave_Click :: " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            JsClientCustom("WindowClose", "window.close();");
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroup.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion

}
