﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Master_NewOutboundConfig : BasePage
{
    #region Method

    public bool ValidateSave(string mode)
    {
        if (int.Parse(ddlPlatform.SelectedValue) == 0)
        {
            JsClientAlert("Platform", "โปรดเลือก Platform");
            return false;
        }
        if (int.Parse(ddlOutboundEvent.SelectedValue) == 0)
        {
            JsClientAlert("Event", "โปรดเลือก Event");
            return false;
        }
        if (int.Parse(ddlOutboundType.SelectedValue) == 0)
        {
            JsClientAlert("OutboundType", "โปรดเลือก OutboundType");
            return false;
        }
        if (txtQtyOfDay.Text == "")
        {
            JsClientAlert("QtyOfDay", "โปรดกรอก QtyOfDay");
            return false;
        }
        if (txtStartDate.Text == "")
        {
            JsClientAlert("StartDate", "โปรดกรอก StartDate");
            return false;
        }
        if (txtEndDate.Text == "")
        {
            JsClientAlert("EndDate", "โปรดกรอก EndDate");
            return false;
        }

        if (mode == "Edit")
        {
            return true;
        }
        return true;
    }

    public void BindData()
    {
        OutboundConfigBLL outboundConfigBLL;
        DataTable dtOutlet;

        try
        {
            outboundConfigBLL = new OutboundConfigBLL();
            dtOutlet = outboundConfigBLL.SelectOne(int.Parse(Request.QueryString["outboundConfigId"].ToString()));
            if (dtOutlet.Rows.Count != 0)
            {
                ddlPlatform.SelectedValue = dtOutlet.Rows[0]["platformId"].ToString();
                ddlOutboundEvent.SelectedValue = dtOutlet.Rows[0]["eventId"].ToString();
                ddlOutboundType.SelectedValue = dtOutlet.Rows[0]["outboundTypeId"].ToString();
                txtQtyOfDay.Text = dtOutlet.Rows[0]["qtyOfDay"].ToString();
                txtStartDate.Text = DateTime.Parse(dtOutlet.Rows[0]["startDate"].ToString()).ToString("dd/MM/yyyy");
                txtEndDate.Text = DateTime.Parse(dtOutlet.Rows[0]["endDate"].ToString()).ToString("dd/MM/yyyy");
                rdoActive.SelectedValue = dtOutlet.Rows[0]["active"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindData :: " + ex.Message);
        }
    }

    public void BindPlatform()
    {
        PlatformBLL platformBLL;
        DataTable dtPlatform;

        try
        {
            platformBLL = new PlatformBLL();
            dtPlatform = platformBLL.SelectActivePlatform("T");
            ddlPlatform.DataTextField = "name";
            ddlPlatform.DataValueField = "platformId";
            ddlPlatform.DataSource = dtPlatform;
            ddlPlatform.DataBind();
            ddlPlatform.Items.Insert(0, new ListItem("เลือก Platform", "0"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindPlatform :: " + ex.Message);
        }
    }

    public void BindOutboundEvent()
    {
        OutboundEventBLL outboundEventBLL;
        DataTable dtOutboundEvent;

        try
        {
            outboundEventBLL = new OutboundEventBLL();
            dtOutboundEvent = outboundEventBLL.SelectActiveOutboundEventMaster();
            ddlOutboundEvent.DataTextField = "eventName";
            ddlOutboundEvent.DataValueField = "eventId";
            ddlOutboundEvent.DataSource = dtOutboundEvent;
            ddlOutboundEvent.DataBind();
            ddlOutboundEvent.Items.Insert(0, new ListItem("เลือก Event", "0"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindOutboundEvent :: " + ex.Message);
        }
    }

    public void BindOutboundType()
    {
        OutboundTypeBLL outboundTypeBLL;
        DataTable dtOutboundType;

        try
        {
            outboundTypeBLL = new OutboundTypeBLL();
            dtOutboundType = outboundTypeBLL.SelectActiveOutboundType("T");
            ddlOutboundType.DataTextField = "name";
            ddlOutboundType.DataValueField = "outboundTypeId";
            ddlOutboundType.DataSource = dtOutboundType;
            ddlOutboundType.DataBind();
            ddlOutboundType.Items.Insert(0, new ListItem("เลือก OutboundType", "0"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindOutboundType :: " + ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {
                    BindPlatform();
                    BindOutboundEvent();
                    BindOutboundType();
                    BindData();
                }
                else
                {
                    BindPlatform();
                    BindOutboundEvent();
                    BindOutboundType();
                }
            }

        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Outlet.PageLoad :: " + ex.Message);
        }
    }

    //protected void btnChooseOutletRegister_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + "/DIAGEO.WEBSITE/Master/OutletPopup.aspx?ComeFrom=Register&OutletCode=','mywindow','dialogWidth:1000px; dialogHeight:550px; center:yes; scroll:yes'); document.getElementById('btnRefresh').click();";
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "", window, true);
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }
    //}

    protected void btnSave_Click(object sender, EventArgs e)
    {
        OutboundConfigBLL outboundConfigBLL;
        DIAGEO.COMMON.User user;

        try
        {
            outboundConfigBLL = new OutboundConfigBLL();
            user = new DIAGEO.COMMON.User();

            //foreach (ListItem item in listOutletRegister.Items)
            //{
            //outboundConfigBLL.Insert(ddlPlatform.SelectedValue, "", ddlOutboundEvent.SelectedValue, ddlOutboundType.SelectedValue, txtQtyOfDay.Text, DateTime.ParseExact(txtStartDate.Text, "yyyy/MM/dd", null), DateTime.ParseExact(txtEndDate.Text, "yyyy/MM/dd", null), rdoActive.SelectedValue, user.UserID);
            //}
                JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");

        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Outlet.btnSave_Click :: " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            JsClientCustom("WindowClose", "window.close();");
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroup.btnCancel_Click :: " + ex.Message);
        }
    }

    //protected void btnRefresh_Click(object sender, EventArgs e)
    //{
    //    Outlet outletData;

    //    try
    //    {
    //        outletData = new Outlet();

    //        if (outletData.OutletData != null)
    //        {
    //            if (outletData.OutletData != null)
    //            {
    //                listOutletRegister.DataTextField = "Name";
    //                listOutletRegister.DataValueField = "Value";
    //                listOutletRegister.DataSource = outletData.OutletData;
    //                listOutletRegister.DataBind();
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception(ex.Message);
    //    }

    //}


    #endregion
}
