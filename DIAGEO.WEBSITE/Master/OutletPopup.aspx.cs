﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Master_OutletPopup : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                BindOutlet();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void BindOutlet()
    {
        DataTable dtOutlet;
        OutletBLL outletBLL;

        try
        {
            outletBLL = new OutletBLL();
            dtOutlet = outletBLL.SelectActiveOutlet("T");
            gvOutlet.DataSource = dtOutlet;
            gvOutlet.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnSelect_Click(object sender, EventArgs e)
    {
        string outlet = "";
        CheckBox chkOutlet;
        OutboundEventBD outboundEventBD;
        DataTable dtOutboundEventBD;
        DataRow drOutletRegisterName;

        try
        {

            outboundEventBD = new OutboundEventBD();
            dtOutboundEventBD = new DataTable();
            dtOutboundEventBD.Columns.Add(new DataColumn("Name"));
            dtOutboundEventBD.Columns.Add(new DataColumn("Value"));
            for (int i = 0; i <= gvOutlet.Rows.Count - 1; i++)
            {
                chkOutlet = gvOutlet.Rows[i].FindControl("chkOutlet") as CheckBox;
                if (chkOutlet.Checked == true)
                {
                    outlet += gvOutlet.DataKeys[i][0].ToString() + ",";
                    drOutletRegisterName = dtOutboundEventBD.NewRow();
                    drOutletRegisterName[0] = gvOutlet.Rows[i].Cells[1].Text;
                    drOutletRegisterName[1] = gvOutlet.DataKeys[i][0].ToString();
                    dtOutboundEventBD.Rows.Add(drOutletRegisterName);
                }
            }

            if (Request.QueryString["ComeFrom"].ToString() == "Register")
                outboundEventBD.OutletDataForBD = dtOutboundEventBD;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "window.close();", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void HeaderLevelCheckBox_CheckedChanged(object sender, EventArgs e)
    {

        foreach (GridViewRow dr in gvOutlet.Rows)
        {

            CheckBox chk = (CheckBox)dr.Cells[0].FindControl("chkOutlet");

            chk.Checked = ((CheckBox)sender).Checked;
        }  
    }

}