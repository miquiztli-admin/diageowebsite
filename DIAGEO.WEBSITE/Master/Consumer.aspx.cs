﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;
using ITOS.Utility.Formatter;
using WebServiceEditConsumer;
//using WebservicceTest;

public partial class Master_Consumer : BasePage
{
    #region Method

    private bool ValidateSave(string mode)
    {
        ConsumerBLL consumerBLL;
        DataTable dtConsumer;

        try
        {

            consumerBLL = new ConsumerBLL();

            //if (txtIdCard.Text == "")
            //{
            //    JsClientAlert("National ID", "โปรดกรอก National_ID");
            //    return false;
            //}
            if (txtFirstName.Text == "")
            {
                JsClientAlert("FirstName", "โปรดกรอก FirstName");
                return false;
            }
            if (txtLastName.Text == "")
            {
                JsClientAlert("LastName", "โปรดกรอก LastName");
                return false;
            }
            if (txtMobile.Text == "")
            {
                JsClientAlert("Mobile", "โปรดกรอก Mobile");
                return false;
            }
            if (txtEmail.Text == "")
            {
                JsClientAlert("Email", "โปรดกรอก E-mail");
                return false;
            }
            if (txtDateOfBirth.Text == "")
            {
                JsClientAlert("DateOfBirth", "โปรดกรอก DateOfBirth");
                return false;
            }

            if (mode == "Edit")
            {

                //if (txtIdCard.Text != Request.QueryString["idCard"].ToString())
                //{
                //    dtConsumer = consumerBLL.SelectOneBYIdCardEmailAndMobile(txtIdCard.Text, "", "");
                //    if (dtConsumer.Rows.Count != 0)
                //    {
                //        JsClientAlert("UsernameHasUse", "National ID นี้มีผู้ใช้เเล้ว");
                //        return false;
                //    }
                //}

                if (txtEmail.Text != Request.QueryString["email"].ToString())
                {
                    dtConsumer = consumerBLL.SelectOneBYIdCardEmailAndMobile("", txtEmail.Text, "");
                    if (dtConsumer.Rows.Count != 0)
                    {
                        JsClientAlert("UsernameHasUse", "E-mail นี้มีผู้ใช้เเล้ว");
                        return false;
                    }
                }

                if (txtMobile.Text != Request.QueryString["mobile"].ToString())
                {
                    dtConsumer = consumerBLL.SelectOneBYIdCardEmailAndMobile("", "", txtMobile.Text);
                    if (dtConsumer.Rows.Count != 0)
                    {
                        JsClientAlert("UsernameHasUse", "Mobile No นี้มีผู้ใช้เเล้ว");
                        return false;
                    }
                }

                return true;

            }
            else
            {
                //dtConsumer = consumerBLL.SelectOneBYIdCardEmailAndMobile(txtIdCard.Text, "", "");
                //if (dtConsumer.Rows.Count != 0)
                //{
                //    JsClientAlert("UsernameHasUse", "National ID นี้มีผู้ใช้เเล้ว");
                //    return false;
                //}

                dtConsumer = consumerBLL.SelectOneBYIdCardEmailAndMobile("", txtEmail.Text, "");
                if (dtConsumer.Rows.Count != 0)
                {
                    JsClientAlert("UsernameHasUse", "E-mail นี้มีผู้ใช้เเล้ว");
                    return false;
                }

                dtConsumer = consumerBLL.SelectOneBYIdCardEmailAndMobile("", "", txtMobile.Text);
                if (dtConsumer.Rows.Count != 0)
                {
                    JsClientAlert("UsernameHasUse", "Mobile No นี้มีผู้ใช้เเล้ว");
                    return false;
                }


                return true;

            }
        }
        catch (Exception ex)
        {

            throw new Exception("Consumer::ValidateSave" + ex.Message);
        }
    }

    private string BindDataConsumer()
    {
        string DataConsumer, MemberCode, DateOfBirth;
        int _year, _month, _date;
        PlatformMemberBLL platformMemberBLL;
        try
        {
            platformMemberBLL = new PlatformMemberBLL();
            MemberCode = platformMemberBLL.SelectOneByConsumerId(int.Parse(Request.QueryString["consumerId"].ToString()));

            _year = Convert.ToInt32(txtDateOfBirth.Text.Split('/')[2].ToString());
            _month = Convert.ToInt32(txtDateOfBirth.Text.Split('/')[1].ToString());
            _date = Convert.ToInt32(txtDateOfBirth.Text.Split('/')[0].ToString());

            DateOfBirth = _date.ToString() + "/" + _month.ToString() + "/" + _year.ToString();

            //Consumer
            DataConsumer = "FIRSTNAME" + SeparateSplit + txtFirstName.Text.ToString() + MainSplit + "LASTNAME" + SeparateSplit + txtLastName.Text.ToString()
            + MainSplit + "EMAIL" + SeparateSplit + txtEmail.Text.ToString() + MainSplit + "MOBILE" + SeparateSplit + txtMobile.Text.ToString()
            + MainSplit + "FACEBOOKID" + SeparateSplit + txtEmail.Text.ToString() + MainSplit + "MEMBERCODE" + SeparateSplit + "1"
            + MainSplit + "DATEOFBIRTH" + SeparateSplit + DateOfBirth + MainSplit + "GENDER" + SeparateSplit + rdoGender.SelectedValue.ToString();

            if (txtHomeAddress.Text != null && txtHomeAddress.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "IDCARD" + SeparateSplit + txtIdCard.Text.ToString();
            }

            if (txtHomeAddress.Text != null && txtHomeAddress.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "HOMEADDRESS" + SeparateSplit + txtHomeAddress.Text.ToString();
            }
            if (txtHomeMoo.Text != null && txtHomeMoo.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "HOMEMOO" + SeparateSplit + txtHomeMoo.Text.ToString();
            }

            if (txtHomeVillage.Text != null && txtHomeVillage.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "HOMEVILLAGE" + SeparateSplit + txtHomeVillage.Text.ToString();
            }
            if (txtHomeVillage.Text != null && txtHomeVillage.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "HOMESOI" + SeparateSplit + txtHomeSoi.Text.ToString();
            }
            if (txtHomeVillage.Text != null && txtHomeVillage.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "HOMEROAD" + SeparateSplit + txtWorkRoad.Text.ToString();
            }
            if (txtHomeVillage.Text != null && txtHomeVillage.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "HOMESUBDISTRICT" + SeparateSplit + txtHomeSubDistrict.Text.ToString();
            }
            if (txtHomeVillage.Text != null && txtHomeVillage.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "HOMEDISTRICT" + SeparateSplit + txtHomeDistrict.Text.ToString();
            }
            if (txtHomeVillage.Text != null && txtHomeVillage.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "HOMEPROVINCE" + SeparateSplit + ddlHomeProvince.SelectedValue.ToString();
            }
            if (txtHomeVillage.Text != null && txtHomeVillage.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "WORKADDRESS" + SeparateSplit + txtWorkAddress.Text.ToString();
            }
            if (txtHomeVillage.Text != null && txtHomeVillage.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "WORKMOO" + SeparateSplit + txtWorkMoo.Text.ToString();
            }
            if (txtHomeVillage.Text != null && txtHomeVillage.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "WORKVILLAGE" + SeparateSplit + txtWorkVillage.Text.ToString();
            }
            if (txtHomeVillage.Text != null && txtHomeVillage.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "WORKSOI" + SeparateSplit + txtWorkSoi.Text.ToString();
            }
            if (txtHomeVillage.Text != null && txtHomeVillage.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "WORKROAD" + SeparateSplit + txtWorkRoad.Text.ToString();
            }
            if (txtHomeVillage.Text != null && txtHomeVillage.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "WORKSUBDISTRICT" + SeparateSplit + txtWorkSubDistrict.Text.ToString();
            }
            if (txtHomeVillage.Text != null && txtHomeVillage.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "WORKDISTRICT" + SeparateSplit + txtWorkDistrict.Text.ToString();
            }
            if (txtHomeVillage.Text != null && txtHomeVillage.Text.ToString() != "")
            {
                DataConsumer = DataConsumer + MainSplit + "WORKPROVINCE" + SeparateSplit + ddlWorkProvince.SelectedValue.ToString();
            }

            DataConsumer = DataConsumer + MainSplit + "MEMBERCODE" + SeparateSplit + MemberCode;

            DataConsumer = DataConsumer + MainSplit + "SUBSCRIBECALLCENTER_DG" + SeparateSplit + rdoDGCallCenterActive.SelectedValue + MainSplit + "SUBSCRIBEEMAIL_DG" + SeparateSplit + rdoDGEmailActive.SelectedValue
            + MainSplit + "SUBSCRIBESMS_DG" + SeparateSplit + rdoDGSMSActive.SelectedValue + MainSplit + "SUBSCRIBECALLCENTER_OUTLET" + SeparateSplit + rdoOutletCallCenterActive.SelectedValue
            + MainSplit + "SUBSCRIBEEMAIL_OUTLET" + SeparateSplit + rdoOutletEmailActive.SelectedValue + MainSplit + "SUBSCRIBESMS_OUTLET" + SeparateSplit + rdoOutletSMSActive.SelectedValue;

            return DataConsumer;
        }
        catch (Exception ex)
        {
            
            throw new Exception(ex.Message);
        }
        
    }

    private string BindDataSubScribe()
    {
        string DataSubScribe;
        try
        {
            // SubScribe
            DataSubScribe = "SUBSCRIBECALLCENTERDG" + SeparateSplit + rdoDGCallCenterActive.SelectedValue + SubSplit + "SUBSCRIBEEMAILDG" + SeparateSplit + rdoDGEmailActive.SelectedValue
            + SubSplit + "SUBSCRIBESMSDG" + SeparateSplit + rdoDGSMSActive.SelectedValue + SubSplit + "SUBSCRIBECALLCENTEROUTLET" + SeparateSplit + rdoOutletCallCenterActive.SelectedValue
            + SubSplit + "SUBSCRIBEEMAILOUTLET" + SeparateSplit + rdoOutletEmailActive.SelectedValue + SubSplit + "SUBSCRIBESMSOUTLET" + SeparateSplit + rdoOutletSMSActive.SelectedValue;
            return DataSubScribe;
        }
        catch (Exception ex)
        {
            
            throw new Exception(ex.Message);
        }
        
    }

    private void BindData()
    {
        ConsumerBLL consumerBLL;
        DataTable dtConsumer;

        try
        {
            consumerBLL = new ConsumerBLL();
            dtConsumer = consumerBLL.SelectOne(int.Parse(Request.QueryString["consumerId"].ToString()));
            if (dtConsumer.Rows.Count != 0)
            {

                txtIdCard.Text = dtConsumer.Rows[0]["idCard"].ToString();
                txtFirstName.Text = dtConsumer.Rows[0]["firstName"].ToString();
                txtLastName.Text = dtConsumer.Rows[0]["lastName"].ToString();
                txtEmail.Text = dtConsumer.Rows[0]["email"].ToString();
                ViewState["OldEmail"] = dtConsumer.Rows[0]["email"].ToString();
                txtMobile.Text = dtConsumer.Rows[0]["mobile"].ToString();

                if (dtConsumer.Rows[0]["dateOfBirth"].ToString() != "" && dtConsumer.Rows[0]["dateOfBirth"] != null)
                {
                txtDateOfBirth.Text = DateTime.Parse(dtConsumer.Rows[0]["dateOfBirth"].ToString()).ToString("dd/MM/yyyy");
                }

                rdoGender.SelectedValue = dtConsumer.Rows[0]["gender"].ToString();
                txtHomeAddress.Text = dtConsumer.Rows[0]["homeAddress"].ToString();
                txtHomeMoo.Text = dtConsumer.Rows[0]["homeMoo"].ToString();
                txtHomeVillage.Text = dtConsumer.Rows[0]["homeVillage"].ToString();
                txtHomeSoi.Text = dtConsumer.Rows[0]["homeSoi"].ToString();
                txtHomeRoad.Text = dtConsumer.Rows[0]["homeRoad"].ToString();
                txtHomeSubDistrict.Text = dtConsumer.Rows[0]["homeSubDistrict"].ToString();
                txtHomeDistrict.Text = dtConsumer.Rows[0]["homeDistrict"].ToString();
                ddlHomeProvince.SelectedValue = dtConsumer.Rows[0]["homeProvince"].ToString();
                txtWorkAddress.Text = dtConsumer.Rows[0]["workAddress"].ToString();
                txtWorkMoo.Text = dtConsumer.Rows[0]["workMoo"].ToString();
                txtWorkVillage.Text = dtConsumer.Rows[0]["workVillage"].ToString();
                txtWorkSoi.Text = dtConsumer.Rows[0]["workSoi"].ToString();
                txtWorkRoad.Text = dtConsumer.Rows[0]["workRoad"].ToString();
                txtWorkSubDistrict.Text = dtConsumer.Rows[0]["workSubDistrict"].ToString();
                txtWorkDistrict.Text = dtConsumer.Rows[0]["workDistrict"].ToString();
                ddlWorkProvince.SelectedValue = dtConsumer.Rows[0]["workProvince"].ToString();
                rdoDGCallCenterActive.SelectedValue = dtConsumer.Rows[0]["subScribeCallCenter_DG"].ToString();
                rdoDGEmailActive.SelectedValue = dtConsumer.Rows[0]["subScribeEmail_DG"].ToString();
                rdoDGSMSActive.SelectedValue = dtConsumer.Rows[0]["subScribeSMS_DG"].ToString();
                rdoOutletCallCenterActive.SelectedValue = dtConsumer.Rows[0]["subScribeCallCenter_Outlet"].ToString();
                rdoOutletEmailActive.SelectedValue = dtConsumer.Rows[0]["subScribeEmail_Outlet"].ToString();
                rdoOutletSMSActive.SelectedValue = dtConsumer.Rows[0]["subScribeSMS_Outlet"].ToString();

            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindData :: " + ex.Message);
        }
    }

    private void CallWebservice()
    {
        ConsumerBLL consumerBll;
        User user;
        UserActivityLogBLL userActivityLogBll;
        ConsumerMgt EditConsumerToTracTion;
        //ConsumerBLL consumerBLL;
        //DataTable dtConsumer;
        string User, Pass, PlatForm, MatchKey, MatchVale, DataConsumer, MsErrConsumer, DataSubScribe;

        try
        {
            consumerBll = new ConsumerBLL();
            user = new User();
            userActivityLogBll = new UserActivityLogBLL();

            EditConsumerToTracTion = new ConsumerMgt();
            //consumerBLL = new ConsumerBLL();

            if (ValidateSave(Request.QueryString["Mode"].ToString()))
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {

                    if (CanEdit)
                    {

                        #region OldUpdateconsumer
                        //DateTimeFormatter dateFormater = new DateTimeFormatter();

                        //if (consumerBll.UpdateConsumer(int.Parse(Request.QueryString["consumerId"]), txtIdCard.Text, txtFirstName.Text, txtLastName.Text,
                        //txtEmail.Text, txtMobile.Text, dateFormater.Parse(txtDateOfBirth.Text.ToString()), rdoGender.SelectedValue, txtHomeAddress.Text, txtHomeMoo.Text, txtHomeVillage.Text,
                        //txtHomeSoi.Text, txtHomeRoad.Text, txtHomeSubDistrict.Text, txtHomeDistrict.Text, ddlHomeProvince.SelectedValue, txtWorkAddress.Text,
                        //txtWorkMoo.Text, txtWorkVillage.Text, txtWorkSoi.Text, txtWorkRoad.Text, txtWorkSubDistrict.Text, txtWorkDistrict.Text,
                        //ddlWorkProvince.SelectedValue, user.UserID))

                        //EditToTracTion(int.Parse(user.UserID.ToString()), int.Parse(Request.QueryString["consumerId"]));
                        //userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "Consumer_Edit");
                        #endregion

                        //dtConsumer = consumerBLL.SelectOne(int.Parse(Request.QueryString["consumerId"]));

                        User = Session["UserName"].ToString();
                        Pass = Session["PasswordNonEncode"].ToString();

                        PlatForm = "ST";
                        MatchKey = "C";
                        MatchVale = Request.QueryString["tractionId"].ToString();
                        DataConsumer = BindDataConsumer();
                        DataSubScribe = BindDataSubScribe();

                        //CallWebservice 

                        if (MatchVale == " ")
                        {
                            JsClientCustom("Error", "alert('ไม่มี tractionId ไม่สามารถเปลี่ยนแปลงข้อมูลได้');window.close();");
                        }
                        else
                        {

                            MsErrConsumer = EditConsumerToTracTion.RegisterConsumer(User, Pass, PlatForm, "", MatchKey, MatchVale, DataConsumer);

                            //MsErrConsumer = MsErrConsumer + EditConsumerToTracTion.SubScribeConsumer(User, Pass, PlatForm, MatchKey, MatchVale, DataSubScribe);

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "xxx", "unblockUI();", true);

                            if (MsErrConsumer.ToString().Length > 40)
                            {
                                JsClientCustom("Error", "alert('ไม่สามารถบันทึกข้อมูลได้');window.close();");
                            }
                            else
                            {
                                JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ " + MsErrConsumer + "');window.close();");
                            }
                        }


                    }
                    else
                    {
                        JsClientCustom("OutboundConfigSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
                    }
                }
            }

        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "xxx", "unblockUI();", true);           
            throw new Exception(ex.Message);
        }
    }

    //public void EditToTracTion(int userID, int consumerId)
    //{
    //    ConsumerMgt EditConsumerToTracTion;
    //    ConsumerBLL consumerBLL;
    //    UserBLL userBLL;
    //    DataTable dtConsumer,dtUser;
    //    string User, Pass, PlatForm, MatchKey, MatchVale, Data;

    //    try
    //    {
    //        EditConsumerToTracTion = new ConsumerMgt();
    //        consumerBLL = new ConsumerBLL();
    //        userBLL = new UserBLL();

    //        dtConsumer = consumerBLL.SelectOne(consumerId);
    //        dtUser = userBLL.SelectOne(userID);

    //        if ((dtConsumer.Rows.Count != 0) && (dtUser.Rows.Count != 0))
    //        {
    //            User = dtUser.Rows[0]["userName"].ToString();
    //            Pass = dtUser.Rows[0]["password"].ToString();

    //            PlatForm = dtConsumer.Rows[0]["registerPlatformId"].ToString();
    //            MatchKey = "E";
    //            MatchVale = ViewState["OldEmail"].ToString();
    //            Data = "IDCARD" + SeparateSplit + dtConsumer.Rows[0]["idCard"].ToString() + MainSplit + "FIRSTNAME" + SeparateSplit + dtConsumer.Rows[0]["firstName"].ToString()
    //            + MainSplit + "EMAIL" + SeparateSplit + dtConsumer.Rows[0]["email"].ToString() + MainSplit + "MOBILE" + SeparateSplit + dtConsumer.Rows[0]["mobile"].ToString()
    //            + MainSplit + "DATEOFBIRTH" + SeparateSplit + DateTime.Parse(dtConsumer.Rows[0]["dateOfBirth"].ToString()).ToString("dd/MM/yyyy") + MainSplit + "GENDER" + SeparateSplit + dtConsumer.Rows[0]["gender"].ToString()
    //            + MainSplit + "HOMEADDRESS" + SeparateSplit + dtConsumer.Rows[0]["homeAddress"].ToString() + MainSplit + "HOMEMOO" + SeparateSplit + dtConsumer.Rows[0]["homeMoo"].ToString()
    //            + MainSplit + "HOMEVILLAGE" + SeparateSplit + dtConsumer.Rows[0]["homeVillage"].ToString() + MainSplit + "HOMESOI" + SeparateSplit + dtConsumer.Rows[0]["homeSoi"].ToString()
    //            + MainSplit + "HOMEROAD" + SeparateSplit + dtConsumer.Rows[0]["homeRoad"].ToString() + MainSplit + "HOMESUBDISTRICT" + SeparateSplit + dtConsumer.Rows[0]["homeSubDistrict"].ToString()
    //            + MainSplit + "HOMEDISTRICT" + SeparateSplit + dtConsumer.Rows[0]["homeDistrict"].ToString() + MainSplit + "HOMEPROVINCE" + SeparateSplit + dtConsumer.Rows[0]["homeProvince"].ToString()
    //            + MainSplit + "WORKADDRESS" + SeparateSplit + dtConsumer.Rows[0]["workAddress"].ToString() + MainSplit + "WORKMOO" + SeparateSplit + dtConsumer.Rows[0]["workMoo"].ToString()
    //            + MainSplit + "WORKVILLAGE" + SeparateSplit + dtConsumer.Rows[0]["workVillage"].ToString() + MainSplit + "WORKSOI" + SeparateSplit + dtConsumer.Rows[0]["workSoi"].ToString()
    //            + MainSplit + "WORKROAD" + SeparateSplit + dtConsumer.Rows[0]["workRoad"].ToString() + MainSplit + "WORKSUBDISTRICT" + SeparateSplit + dtConsumer.Rows[0]["workSubDistrict"].ToString()
    //            + MainSplit + "WORKDISTRICT" + SeparateSplit + dtConsumer.Rows[0]["workDistrict"].ToString() + MainSplit + "WORKPROVINCE" + SeparateSplit + dtConsumer.Rows[0]["workProvince"].ToString();
    //            EditConsumerToTracTion.RegisterConsumer(User, Pass, PlatForm, MatchKey, MatchVale, Data);

    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception("EditToTracTion :: " + ex.Message);
    //    }
    //}


    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {
                    BindData();
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Outlet.PageLoad :: " + ex.Message);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        try
        {
            CallWebservice();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "xxx", "unblockUI();", true);
            JsClientAlert("ErrorMessage", "Outlet.btnSave_Click :: " + ex.Message);
            //Response.Redirect("ConsumerSearch.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            //Response.Redirect("ConsumerSearch.aspx");
            JsClientCustom("WindowClose", "window.close();");
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Consumer.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion

}
