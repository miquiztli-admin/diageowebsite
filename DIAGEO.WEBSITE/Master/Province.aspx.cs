﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Master_Province : BasePage
{

    #region Method

    public bool ValidateSave(string mode)
    {
        ProvinceBLL provinceBLL;
        DataTable dtProvince;

        try
        {
            if (txtProvinceName.Text == "")
            {
                JsClientAlert("ProvinceName", "โปรดกรอก Province Name");
                return false;
            }
            provinceBLL = new ProvinceBLL();
            if (mode == "Edit")
            {
                //if (txtProvinceName.Text != Request.QueryString["name"].ToString())
                if (txtProvinceName.Text != Session["ProvinceName"].ToString())
                {
                    dtProvince = provinceBLL.SelectOneBYOutletName(txtProvinceName.Text);
                    if (dtProvince.Rows.Count != 0)
                    {
                        JsClientAlert("ProvinceNameHasUse", "Province Name นี้มีผู้ใช้เเล้ว");
                        return false;
                    }
                }

            }
            else
            {
                dtProvince = provinceBLL.SelectOneBYOutletName(txtProvinceName.Text);
                if (dtProvince.Rows.Count != 0)
                {
                    JsClientAlert("ProvinceNameHasUse", "Province Name นี้มีผู้ใช้เเล้ว");
                    return false;
                }

            }

            return true;
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateSave :: " + ex.Message);
        }
    }

    public void BindData()
    {
        ProvinceBLL provinceBLL;
        DataTable dtProvince;

        try
        {
            provinceBLL = new ProvinceBLL();
            dtProvince = provinceBLL.SelectOne(int.Parse(Request.QueryString["ProvinceId"].ToString()));
            if (dtProvince.Rows.Count != 0)
            {
                txtProvinceName.Text = dtProvince.Rows[0]["name"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindData :: " + ex.Message);
        }
    }


    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }


            if (!Page.IsPostBack)
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {

                    txtProvinceName.ReadOnly = false;
                    txtProvinceName.Enabled = true;

                    BindData();

                }
                else
                {

                    txtProvinceName.ReadOnly = false;
                    txtProvinceName.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Province.PageLoad :: " + ex.Message);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        ProvinceBLL provinceBLL;
        User user;
        UserActivityLogBLL userActivityLogBll;

        try
        {
            provinceBLL = new ProvinceBLL();
            user = new User();
            userActivityLogBll = new UserActivityLogBLL();

            if (ValidateSave(Request.QueryString["Mode"].ToString()))
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {

                    if (CanEdit)
                    {
                        if (provinceBLL.UpdateSearch(Request.QueryString["ProvinceId"].ToString(), txtProvinceName.Text))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "Province_Edit");
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                        }
                    }
                    else
                    {
                        JsClientCustom("ProvinceSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
                    }
                }
                else
                {

                    if (CanAdd)
                    {

                        if (provinceBLL.Insert(txtProvinceName.Text))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "Province_Add");
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                        }
                    }
                    else
                    {
                        JsClientCustom("ProvinceSearch", "alert('ไม่มีสิทธิ์เพิ่มข้อมูล');window.close();");
                    }       
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Province.btnSave_Click :: " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            JsClientCustom("WindowClose", "window.close();");
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Province.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion

}
