﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Master_OutletSearch : BasePage
{

    #region Property

    public string Code { get { return ViewState["OutletCode"].ToString(); } set { ViewState["OutletCode"] = value; } }

    public string Name { get { return ViewState["OutletName"].ToString(); } set { ViewState["OutletName"] = value; } }

    public string Zone { get { return ViewState["OutletZone"].ToString(); } set { ViewState["OutletZone"] = value; } }

    public string Address { get { return ViewState["OutletAddress"].ToString(); } set { ViewState["OutletAddress"] = value; } }

    public string Tel { get { return ViewState["OutletTel"].ToString(); } set { ViewState["OutletTel"] = value; } }

    public string Active { get { return ViewState["OutletActive"].ToString(); } set { ViewState["OutletActive"] = value; } }

    #endregion

    #region Method

    public void BindOutlet(int pageNo)
    {
        OutletBLL outletBLL;
        DataTable dtSearch;
        int count;

        try
        {
            outletBLL = new OutletBLL();

            if (pageNo == 1)
                dtSearch = outletBLL.Search(Code, Name, Zone, Address, Tel, Active, 0, RowPerPage, "", 0);
            else
                dtSearch = outletBLL.Search(Code, Name, Zone, Address, Tel, Active, (pageNo - 1) * RowPerPage, RowPerPage, "", 0);

            count = int.Parse(outletBLL.Search(Code, Name, Zone, Address, Tel, Active, 0, RowPerPage, "", 1).Rows[0][0].ToString());

            pgSearch.RowPerPage = RowPerPage;
            pgSearch.CheckStringButton = pageNo.ToString();
            pgSearch.RowCount = count;
            pgSearch.AllowPagingOnDataGrid = false;
            pgSearch.DataSource = dtSearch;
            pgSearch.DataGridBind = dgSearch;
            pgSearch.DataBind();
            tableNavigation.Visible = true;
        }
        catch (Exception ex)
        {
            throw new Exception("BindOutlet :: " + ex.Message);
        }
    }

    public void BindZone()
    {
        OutletZoneBLL outletZoneBLL;
        DataTable dtOutletZone;

        try
        {
            outletZoneBLL = new OutletZoneBLL();
            dtOutletZone = outletZoneBLL.SelectActiveOutletZone("T");
            ddlZone.DataTextField = "name";
            ddlZone.DataValueField = "zoneId";
            ddlZone.DataSource = dtOutletZone;
            ddlZone.DataBind();
            ddlZone.Items.Insert(0, new ListItem("เลือก Zone", "0"));
        }
        catch (Exception ex)
        {
            throw new Exception("BindZone :: " + ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {
                RowPerPage = int.Parse(ConfigurationManager.AppSettings["RowPerPage"].ToString());
                BindZone();
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroupSearch.Page_Load :: " + ex.Message);
        }
    }

    protected void dgSearch_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroupSearch.dgSearch_ItemDataBound :: " + ex.Message);
        }
    }

    protected void dgSearch_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        try
        {
            
            if (CanEdit)
            {
                if (e.CommandName == "Edit")
                {
                    Session["OutletcodeEdit"] = null;
                    Session["OutletcodeEdit"] = e.Item.Cells[0].Text;
                    string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/Outlet.aspx?Mode=Edit&OutletId=" + e.Item.Cells[7].Text + "&Date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:250px; center:yes; scroll:yes'); document.getElementById('btnSearch').click();";
                    JsClientCustom("SavePopUp", window);

                    //string url = "http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/Outlet.aspx?Mode=Edit&OutletId=" + e.Item.Cells[7].Text + "&code=" + e.Item.Cells[0].Text + "&Date=" + DateTime.Now.ToString();
                    //string window = "popupFullscreen('" + url + "'); document.getElementById('btnSearch').click();";
                    //JsClientCustom("SavePopUp", window);
                }
            }
            else
            {
                JsClientCustom("OutletSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "OutletSearch.dgSearch_ItemCommand :: " + ex.Message);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            
            if (CanView)
            {
                Code = txtOutletCode.Text;
                Name = txtOutletName.Text;
                Zone = ddlZone.SelectedValue;
                Address = txtAddress.Text;
                Tel = txtTel.Text;
                Active = rdoActive.SelectedValue;
                BindOutlet(1);
            }
            else
            {
                JsClientCustom("OutletSearch", "alert('ไม่มีสิทธิ์ค้นหาข้อมูล');window.close();");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroupSearch.btnSearch_Click :: " + ex.Message);
        }
    }

    protected void btnNew_Click(object sender, EventArgs e)
    {
        try
        {
            
            if (CanAdd)
            {
                string window = "var mywindow = window.showModalDialog('http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/Outlet.aspx?Mode=Insert&Code=&Name=&Date=" + DateTime.Now.ToString() + "','mywindow','dialogWidth:1000px; dialogHeight:250px; center:yes; scroll:yes'); document.getElementById('btnSearch').click();";
                JsClientCustom("SavePopUp", window);

                //string url = "http://" + Request.UrlReferrer.Authority + ConfigurationManager.AppSettings["PathSite"].ToString() + "/Master/Outlet.aspx?Mode=Insert&Code=&Name=&Date=" + DateTime.Now.ToString();
                //string window = "popupFullscreen('" + url + "'); document.getElementById('btnSearch').click();";
                //JsClientCustom("SavePopUp", window);
            }
            else
            {
                JsClientCustom("OutletSearch", "alert('ไม่มีสิทธิ์เพิ่มข้อมูล');window.close();");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroupSearch.btnNew_Click :: " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            txtOutletCode.Text = "";
            txtOutletName.Text = "";
            ddlZone.SelectedValue = "0";
            txtAddress.Text = "";
            txtTel.Text = "";
            rdoActive.SelectedIndex = 0;

            Code = "";
            Name = "";
            Zone = "0";
            Address = "";
            Tel = "";
            Active = "";
        }
        catch (Exception ex)
        {
            JsClientCustom("ErrorMessage", "UserGroupSearch.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion

    #region Paging

    protected void pgSearch_GoClick(object sender, EventArgs e)
    {
        try
        {
            //Code = txtOutletCode.Text;
            //Name = txtOutletName.Text;
            //Active = rdoActive.SelectedValue;

            string[] strField = Request.Form.GetValues("pgSearch$txtPage");
            int intPage = int.Parse(strField[0]);
            Session["currentPage"] = intPage;
            BindOutlet(intPage);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroupSearch.pgSearch_GoClick :: " + ex.Message);
        }
    }

    protected void pgSearch_PageClick(object sender, EventArgs e)
    {
        LinkButton lb = sender as LinkButton;

        try
        {
            //Code = txtOutletCode.Text;
            //Name = txtOutletName.Text;
            //Active = rdoActive.SelectedValue;

            int intPage = GetCurrentPageIndex(pgSearch.StartGroupPage, pgSearch.QuantityPageShow, lb.Text);
            Session["currentPage"] = intPage;
            BindOutlet(intPage);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroupSearch.pgSearch_PageClick :: " + ex.Message);
        }
    }

    protected void pgSearch_RowPerPageSelectIndexChanged(Object sender, EventArgs e)
    {
        try
        {
            //Code = txtOutletCode.Text;
            //Name = txtOutletName.Text;
            //Active = rdoActive.SelectedValue;

            DropDownList ddl = sender as DropDownList;
            pgSearch.RowPerPage = int.Parse(ddl.SelectedValue);
            RowPerPage = int.Parse(ddl.SelectedValue);
            BindOutlet(1);
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroupSearch.pgSearch_RowPerPageSelectIndexChanged :: " + ex.Message);
        }
    }

    #endregion        

}
