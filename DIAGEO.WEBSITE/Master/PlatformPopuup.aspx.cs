﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Master_PlatformPopuup : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsPostBack)
            {
                BindPlatform();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void BindPlatform()
    {
        DataTable dtPlatform;
        PlatformBLL platformBLL;

        try
        {
            platformBLL = new PlatformBLL();
            dtPlatform = platformBLL.SelectActivePlatform("T");
            gvPlatform.DataSource = dtPlatform;
            gvPlatform.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnSelect_Click(object sender, EventArgs e)
    {
        string outlet = "";
        CheckBox chkOutlet;
        OutboundEventBD outboundEventBD;
        DataTable dtOutboundEventBD;
        DataRow drPlatform;

        try
        {

            outboundEventBD = new OutboundEventBD();
            dtOutboundEventBD = new DataTable();
            dtOutboundEventBD.Columns.Add(new DataColumn("Name"));
            dtOutboundEventBD.Columns.Add(new DataColumn("Value"));
            for (int i = 0; i <= gvPlatform.Rows.Count - 1; i++)
            {
                chkOutlet = gvPlatform.Rows[i].FindControl("chkPlatform") as CheckBox;
                if (chkOutlet.Checked == true)
                {
                    outlet += gvPlatform.DataKeys[i][0].ToString() + ",";
                    drPlatform = dtOutboundEventBD.NewRow();
                    drPlatform[0] = gvPlatform.Rows[i].Cells[1].Text;
                    drPlatform[1] = gvPlatform.DataKeys[i][0].ToString();
                    dtOutboundEventBD.Rows.Add(drPlatform);
                }
            }

            if (Request.QueryString["ComeFrom"].ToString() == "Register")
                outboundEventBD.PlatformDataForBD = dtOutboundEventBD;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "window.close();", true);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void HeaderLevelCheckBox_CheckedChanged(object sender, EventArgs e)
    {

        foreach (GridViewRow dr in gvPlatform.Rows)
        {

            CheckBox chk = (CheckBox)dr.Cells[0].FindControl("chkPlatform");

            chk.Checked = ((CheckBox)sender).Checked;
        }
    }

}
