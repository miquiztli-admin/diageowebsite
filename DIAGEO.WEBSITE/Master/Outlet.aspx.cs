﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Master_Outlet : BasePage
{
    #region Method

        public bool ValidateSave(string mode)
    {
        OutletBLL outletBLL;
        DataTable dtOutlet;

        try
        {
            if (txtOutletCode.Text == "")
            {
                JsClientAlert("OutletCode", "โปรดกรอก Outlet Code");
                return false;
            }
            if (txtOutletName.Text == "")
            {
                JsClientAlert("OutletName", "โปรดกรอก Outlet Name");
                return false;
            }
            if (int.Parse(ddlZone.SelectedValue) == 0)
            {
                JsClientAlert("Zone", "โปรดเลือก Zone");
                return false;
            }
            //if (txtAddress.Text == "")
            //{
            //    JsClientAlert("Address", "โปรดกรอก Address");
            //    return false;
            //}
            //if (txtTel.Text == "")
            //{
            //    JsClientAlert("Telephone", "โปรดกรอก Telephone");
            //    return false;
            //}
            if (txtTel.Text != "")
            {
                if (!ISCorrectMobileNum(txtTel.Text))
                {
                    JsClientAlert("Telephone", "หมายเลขโทรศัพท์ไม่ถูกต้อง");
                    return false;
                }
            }

            outletBLL = new OutletBLL();
            if (mode == "Edit")
            {
                if (txtOutletCode.Text != Session["OutletcodeEdit"].ToString())
                {
                    dtOutlet = outletBLL.SelectOneBYOutletName(txtOutletCode.Text, "");
                    if (dtOutlet.Rows.Count != 0)
                    {
                        JsClientAlert("OutletCodeHasUse", "Outlet Code นี้มีผู้ใช้เเล้ว");
                        return false;
                    }
                }

                //if (txtOutletName.Text != Request.QueryString["name"].ToString())
                //{
                //    dtOutlet = outletBLL.SelectOneBYOutletName("",txtOutletName.Text);
                //    if (dtOutlet.Rows.Count != 0)
                //    {
                //        JsClientAlert("OutletnameHasUse", "Outlet Name นี้มีผู้ใช้เเล้ว");
                //        return false;
                //    }
                //}

            }
            else
            {
                dtOutlet = null;
                dtOutlet = outletBLL.SelectOneBYOutletName(txtOutletCode.Text, "");
                if ((dtOutlet.Rows.Count > 0))
                {
                    JsClientAlert("OutletcodeHasUse", "Outlet Code นี้มีผู้ใช้เเล้ว");
                    return false;
                }
                dtOutlet = null;
                dtOutlet = outletBLL.SelectOneBYOutletName("",txtOutletName.Text);
                if ((dtOutlet.Rows.Count > 0))
                {
                    JsClientAlert("OutletnameHasUse", "Outlet Name นี้มีผู้ใช้เเล้ว");
                    return false;
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateSave :: " + ex.Message);
        }
    }

        public void BindData()
        {
            OutletBLL outletBLL;
            DataTable dtOutlet;

            try
            {
                outletBLL = new OutletBLL();
                dtOutlet = outletBLL.SelectOne(int.Parse(Request.QueryString["OutletId"].ToString()));
                if (dtOutlet.Rows.Count != 0)
                {
                    txtOutletCode.Text = dtOutlet.Rows[0]["code"].ToString();
                    txtOutletName.Text = dtOutlet.Rows[0]["name"].ToString();
                    ddlZone.SelectedValue = dtOutlet.Rows[0]["zoneId"].ToString();
                    txtAddress.Text = dtOutlet.Rows[0]["address"].ToString();
                    txtTel.Text = dtOutlet.Rows[0]["tel"].ToString();
                    rdoActive.SelectedValue = dtOutlet.Rows[0]["active"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("BindData :: " + ex.Message);
            }
        }

        public void BindZone()
        {
            OutletZoneBLL outletZoneBLL;
            DataTable dtOutletZone;

            try
            {
                outletZoneBLL = new OutletZoneBLL();
                dtOutletZone = outletZoneBLL.SelectActiveOutletZone("T");
                ddlZone.DataTextField = "name";
                ddlZone.DataValueField = "zoneId";
                ddlZone.DataSource = dtOutletZone;
                ddlZone.DataBind();
                ddlZone.Items.Insert(0, new ListItem("เลือก Zone", "0"));
            }
            catch (Exception ex)
            {
                throw new Exception("BindZone :: " + ex.Message);
            }
        }

        public bool ISCorrectMobileNum(string mobileNumForCheck)
        {
            //- Check format
            if (mobileNumForCheck.Length < 9)
                return false;
            if (mobileNumForCheck.Length > 10 && !mobileNumForCheck.Substring(0, 3).Equals("668"))
                return false;
            if (mobileNumForCheck.Length == 10 && !mobileNumForCheck.Substring(0, 2).Equals("08"))
                return false;
            if (mobileNumForCheck.Length == 9 && !mobileNumForCheck.Substring(0, 2).Equals("02"))
                return false;
            if (mobileNumForCheck.Length == 9 && !mobileNumForCheck.Substring(0, 1).Equals("0"))
                return false;
            return true;
        }


    #endregion

    #region Event

        protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {
                    txtOutletCode.ReadOnly = false;
                    txtOutletName.ReadOnly = false;
                    txtAddress.ReadOnly = false;
                    txtTel.ReadOnly = false;
                    txtOutletCode.Enabled = true;
                    txtOutletCode.Enabled = true;
                    txtOutletName.Enabled = true;
                    txtAddress.Enabled = true;
                    txtTel.Enabled = true;

                    BindZone();
                    BindData();
                    

                }
                else
                {
                    txtOutletCode.ReadOnly = false;
                    txtOutletName.ReadOnly = false;
                    txtAddress.ReadOnly = false;
                    txtTel.ReadOnly = false;
                    txtOutletCode.Enabled = true;
                    txtOutletName.Enabled = true;
                    txtAddress.Enabled = true;
                    txtTel.Enabled = true;

                    BindZone();
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Outlet.PageLoad :: " + ex.Message);
        }
    }

        protected void btnSave_Click(object sender, EventArgs e)
    {
        OutletBLL outletBLL;
        User user;
        UserActivityLogBLL userActivityLogBll;

        try
        {
            outletBLL = new OutletBLL();
            user = new DIAGEO.COMMON.User();
            userActivityLogBll = new UserActivityLogBLL();

            if (ValidateSave(Request.QueryString["Mode"].ToString()))
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {

                    if (CanEdit)
                    {
                        if (outletBLL.UpdateSearch(Request.QueryString["OutletId"].ToString(), txtOutletCode.Text, txtOutletName.Text, int.Parse(ddlZone.SelectedValue.ToString()), txtAddress.Text, txtTel.Text, rdoActive.SelectedValue, user.UserID))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "Outlet_Edit");
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                        }
                    }
                    else
                    {
                        JsClientCustom("OutletSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
                    }
                }
                else
                {

                    if (CanAdd)
                    {

                        if (outletBLL.Insert(txtOutletCode.Text, txtOutletName.Text, int.Parse(ddlZone.SelectedValue.ToString()), txtAddress.Text, txtTel.Text, rdoActive.SelectedValue, user.UserID))
                        {
                            userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "Outlet_Edit");
                            JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                        }
                    }
                    else
                    {
                        JsClientCustom("OutletSearch", "alert('ไม่มีสิทธิ์เพิ่มข้อมูล');window.close();");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Outlet.btnSave_Click :: " + ex.Message);
        }
    }

        protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            JsClientCustom("WindowClose", "window.close();");
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroup.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion
    
}
