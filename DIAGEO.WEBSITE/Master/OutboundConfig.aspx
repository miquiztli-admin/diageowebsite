﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OutboundConfig.aspx.cs" Inherits="Master_OutboundConfig" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>OutboundConfig</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../Style/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <base target="_self"/>
    <script type="text/javascript">  
    $(function(){   
    var dateBefore=null;   
    $("#txtStartDate").datepicker({   
        dateFormat: 'dd/mm/yy',   
        showOn: 'button',      
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],    
        monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],   
        changeMonth: true,   
        changeYear: true ,   
        beforeShow:function(){   
            if($(this).val()!=""){   
                var arrayDate=$(this).val().split("/");        
                arrayDate[2]=parseInt(arrayDate[2]);   
                $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);   
            }   
            setTimeout(function(){   
                $.each($(".ui-datepicker-year option"),function(j,k){   
                    var textYear=parseInt($(".ui-datepicker-year option").eq(j).val());   
                    $(".ui-datepicker-year option").eq(j).text(textYear);   
                });                
            },50);   
  
        },   
        onChangeMonthYear: function(){   
            setTimeout(function(){   
                $.each($(".ui-datepicker-year option"),function(j,k){   
                    var textYear=parseInt($(".ui-datepicker-year option").eq(j).val());   
                    $(".ui-datepicker-year option").eq(j).text(textYear);   
                });                
            },50);         
        },   
        onClose:function(){   
            if($(this).val()!="" && $(this).val()==dateBefore){            
                var arrayDate=dateBefore.split("/");   
                arrayDate[2]=parseInt(arrayDate[2]);   
                $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);       
            }          
        },   
        onSelect: function(dateText, inst){    
            dateBefore=$(this).val();   
            var arrayDate=dateText.split("/");   
            arrayDate[2]=parseInt(arrayDate[2]);   
            $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);   
        }   
  
        });   
       
    });   
    </script>
    <script type="text/javascript">  
    $(function(){   
    var dateBefore=null;   
    $("#txtEndDate").datepicker({   
        dateFormat: 'dd/mm/yy',   
        showOn: 'button',      
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],    
        monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],   
        changeMonth: true,   
        changeYear: true ,   
        beforeShow:function(){   
            if($(this).val()!=""){   
                var arrayDate=$(this).val().split("/");        
                arrayDate[2]=parseInt(arrayDate[2]);   
                $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);   
            }   
            setTimeout(function(){   
                $.each($(".ui-datepicker-year option"),function(j,k){   
                    var textYear=parseInt($(".ui-datepicker-year option").eq(j).val());   
                    $(".ui-datepicker-year option").eq(j).text(textYear);   
                });                
            },50);   
  
        },   
        onChangeMonthYear: function(){   
            setTimeout(function(){   
                $.each($(".ui-datepicker-year option"),function(j,k){   
                    var textYear=parseInt($(".ui-datepicker-year option").eq(j).val());   
                    $(".ui-datepicker-year option").eq(j).text(textYear);   
                });                
            },50);         
        },   
        onClose:function(){   
            if($(this).val()!="" && $(this).val()==dateBefore){            
                var arrayDate=dateBefore.split("/");   
                arrayDate[2]=parseInt(arrayDate[2]);   
                $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);       
            }          
        },   
        onSelect: function(dateText, inst){    
            dateBefore=$(this).val();   
            var arrayDate=dateText.split("/");   
            arrayDate[2]=parseInt(arrayDate[2]);   
            $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);   
        }   
  
        });   
       
    });   
    </script>

</head>
<body style="background-color:Silver">
    <form id="FrmUser" runat="server">
    <div style="background-color:Silver">
        <fieldset>
        <legend class="ntextblack">OutboundConfig</legend>
        <br />
            <table align="center" style="background-color: Silver">
                <%--<tr>
                    <td align="right">
                        <asp:Label ID="lblPlatform" CssClass="ntextblack" runat="server" Text="Platform NAME : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlPlatform" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                </tr>--%>
                <%--<tr>
                    <td align="right">
                        <asp:Label ID="lblOutlet" CssClass="ntextblack" runat="server" Text="Outlet Name : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlOutlet" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                </tr>--%>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblOutboundEvent" CssClass="ntextblack" runat="server" Text="Event NAME : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlOutboundEvent" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblOutboundType" CssClass="ntextblack" runat="server" Text="OutboundType Name : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlOutboundType" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label1" CssClass="ntextblack" runat="server" Text="Qty Of Day : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtQtyOfDay" CssClass="ntextblackdata" runat="server" 
                            Onkeypress = "Javascript:return validateNumKey();" MaxLength="2"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label2" CssClass="ntextblack" runat="server" Text="Start Date : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtStartDate" CssClass="ntextblackdata" runat="server" 
                            MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label3" CssClass="ntextblack" runat="server" Text="End Date : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtEndDate" CssClass="ntextblackdata" runat="server" 
                            MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lbliActive" CssClass="ntextblack" runat="server" Text="Active : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:RadioButtonList ID="rdoActive" CssClass="ntextblackdata" runat="server" RepeatDirection="Horizontal"
                            TabIndex="1">
                            <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="btnSave" CssClass="ntextblack" runat="server" Text="บันทึก" OnClientClick="return true; alert(txtStartDate.Text + '_' + txtEndDate.Text);" OnClick="btnSave_Click"
                            TabIndex="2" />&nbsp;
                        <asp:Button ID="btnCancel" CssClass="ntextblack" runat="server" Text="ยกเลิก" OnClick="btnCancel_Click"
                            TabIndex="3" />
                    </td>
                </tr>
            </table>
        <br />
        </fieldset>
    </div>
    </form>
</body>
</html>
