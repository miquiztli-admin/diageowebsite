﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;

public partial class Master_OutboundEvent : BasePage
{
    #region Method

    public bool ValidateSave(string mode)
    {
        OutboundEventBLL outboundEventBLL;
        DataTable dtOutboundEvent;

        try
        {
            if (txtEventName.Text == "")
            {
                JsClientAlert("OutletCode", "โปรดกรอก Outlet Code");
                return false;
            }
            if (txtStartAge.Text == "")
            {
                JsClientAlert("StartAge", "โปรดกรอก StartAge");
                return false;
            }
            if (txtEndAge.Text == "")
            {
                JsClientAlert("EndAge", "โปรดกรอก EndAge");
                return false;
            }
            outboundEventBLL = new OutboundEventBLL();
            if (mode == "Edit")
            {
                if (txtEventName.Text != Request.QueryString["name"].ToString())
                {
                    dtOutboundEvent = outboundEventBLL.SelectOneBYOutboundEventName(txtEventName.Text);
                    if (dtOutboundEvent.Rows.Count != 0)
                    {
                        JsClientAlert("EventNameHasUse", "Event name นี้มีผู้ใช้เเล้ว");
                        return false;
                    }
                }

            }
            else
            {
                dtOutboundEvent = outboundEventBLL.SelectOneBYOutboundEventName(txtEventName.Text);
                if (dtOutboundEvent.Rows.Count != 0)
                {
                    JsClientAlert("EventNameHasUse", "Event name นี้มีผู้ใช้เเล้ว");
                    return false;
                }
            }

            return true;
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateSave :: " + ex.Message);
        }
    }

    public void BindData()
    {
        OutboundEventBLL outboundEventBLL;
        DataTable dtOutboundEvent;

        try
        {
            outboundEventBLL = new OutboundEventBLL();
            dtOutboundEvent = outboundEventBLL.SelectOne(int.Parse(Request.QueryString["EventId"].ToString()));
            if (dtOutboundEvent.Rows.Count != 0)
            {
                txtEventName.Text = dtOutboundEvent.Rows[0]["name"].ToString();
                txtStartAge.Text = dtOutboundEvent.Rows[0]["startAge"].ToString();
                txtEndAge.Text = dtOutboundEvent.Rows[0]["endAge"].ToString();
                

                rdoActive.SelectedValue = dtOutboundEvent.Rows[0]["active"].ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("BindData :: " + ex.Message);
        }
    }


    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        User user;

        try
        {
            user = new User();

            if (CheckSecsionNull())
            {
                CheckPermission(int.Parse(user.UserID), int.Parse(user.MenuID));
            }

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {
                    txtEventName.ReadOnly = false;
                    txtStartAge.ReadOnly = false;
                    txtEndAge.ReadOnly = false;
                    txtEventName.Enabled = true;
                    txtStartAge.Enabled = true;
                    txtEndAge.Enabled = true;

                    BindData();

                }
                else
                {
                    txtEventName.ReadOnly = false;
                    txtStartAge.ReadOnly = false;
                    txtEndAge.ReadOnly = false;
                    txtEventName.Enabled = true;
                    txtStartAge.Enabled = true;
                    txtEndAge.Enabled = true;

                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Outlet.PageLoad :: " + ex.Message);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        OutboundEventBLL outboundEventBLL;    
        User user;
        UserActivityLogBLL userActivityLogBll;

        try
        {
            outboundEventBLL = new OutboundEventBLL();
            user = new User();
            userActivityLogBll = new UserActivityLogBLL();

          
            if (ValidateSave(Request.QueryString["Mode"].ToString()))
            {
                if (Request.QueryString["Mode"].ToString() == "Edit")
                {

                    //if (CanEdit)
                    //{
                    //    if (outboundEventBLL.UpdateSearch(Request.QueryString["EventId"].ToString(), txtEventName.Text, txtStartAge.Text, txtEndAge.Text, Baileys, Benmore, JwBlack, JwGold, JwGreen, JwRed, Smirnoff, rdoActive.SelectedValue, user.UserID))
                    //    {
                    //        userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "OutboundEvent_Edit");
                    //        JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                    //    }
                    //}
                    //else
                    //{
                    //    JsClientCustom("OutboundEventSearch", "alert('ไม่มีสิทธิ์แก้ไขข้อมูล');window.close();");
                    //}
                }
                else
                {
                    if (CanAdd)
                    {
                        //if (outboundEventBLL.Insert(txtEventName.Text, txtStartAge.Text, txtEndAge.Text, 
                        //    chk100Pipers.Checked, chk100Pipers8yrs.Checked, chkAbsolute.Checked, chkBaileys.Checked,
                        //    chkBallentine.Checked, chkBenmore.Checked, chkBlend.Checked, chkChivas.Checked, chkDewar.Checked, 
                        //    chkBenmoreJwBlack.Checked, chkJwGold.Checked, chkJwGreen.Checked, chkJwRed.Checked, null, null, chkSmirnoff.Checked, rdoActive.SelectedValue, user.UserID)
                        //{

                        //    string brandPreBaileys, string brandPreBallentine,
                        //    string brandPreBenmore, string brandPreBlend, string brandPreChivas,
                        //    string brandPreDewar, string brandPreJwBlack, string brandPreJwGold, string brandPreJwGreen, string brandPreJwRed,
                        //    string brandPreNotdrink, string brandPreOthers, string brandPreSmirnoff, string active, string userIDLogin


                        //    userActivityLogBll.Insert(int.Parse(user.UserID.ToString()), int.Parse(user.UserGroupID.ToString()), int.Parse(user.MenuID.ToString()), "OutboundEvent_Add");
                        //    JsClientCustom("SaveSuccess", "alert('บันทึกสำเร็จ');window.close();");
                        //}
                    }
                    else
                    {
                        JsClientCustom("OutboundEventSearch", "alert('ไม่มีสิทธิ์เพิ่มข้อมูล');window.close();");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Outlet.btnSave_Click :: " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            JsClientCustom("WindowClose", "window.close();");
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "UserGroup.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion

}
