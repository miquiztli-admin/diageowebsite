﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BlockListScreen.aspx.cs" Inherits="Master_BlockListScreen" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>User</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../Style/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <base target="_self"/>
   

</head>
<body style="background-color:Silver">
    <form id="form1" runat="server">
            <table align="center" style="background-color: Silver">
                <tr>
                    <td align="right">
                        <asp:Label ID="lblMobileNo" CssClass="ntextblack" runat="server" 
                            Text="Mobile Nunber"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtMobileNo" CssClass="ntextblackdata" runat="server" 
                            Width="134px" onKeyPress="validateNumKey()" MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblEmail" CssClass="ntextblack" runat="server" 
                            Text="Email"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtEmail" CssClass="ntextblackdata" runat="server" 
                            Width="222px" MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblNationID" CssClass="ntextblack" runat="server" 
                            Text="National ID"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtNationID" CssClass="ntextblackdata" runat="server" 
                            onKeyPress="validateNumKey()" MaxLength="13" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label1" CssClass="ntextblack" runat="server" Text="First Name"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtFirstName" CssClass="ntextblackdata" runat="server" 
                            Width="169px" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label2" CssClass="ntextblack" runat="server" Text="Last Name"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtLastName" CssClass="ntextblackdata" runat="server" 
                            Width="167px" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label3" CssClass="ntextblack" runat="server" Text="Block Reason"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtBlockReason" CssClass="ntextblackdata" runat="server" 
                            Height="61px" TextMode="MultiLine" Width="219px" MaxLength="90"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="btnSave" CssClass="ntextblack" runat="server" Text="บันทึก" OnClientClick="return confirm('Do you want to save these data?');"
                            TabIndex="2" onclick="btnSave_Click" />&nbsp;
                        <asp:Button ID="btnCancel" CssClass="ntextblack" runat="server" Text="ยกเลิก" OnClick="btnCancel_Click"
                            TabIndex="3" />
                    </td>
                </tr>
            </table>
        </form>
</body>
</html>
