﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;
using ITOS.Utility.Formatter;
using System.Text.RegularExpressions;
public partial class Master_BlockListScreen : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        User UserName = new User();
        EventBlockListBLL eventBlockListBLL = new EventBlockListBLL();
        try
        {
            if (isEmail(txtEmail.Text))
            {
                bool statusSave = eventBlockListBLL.InsertEventBlockList(txtMobileNo.Text, txtEmail.Text, txtNationID.Text, txtFirstName.Text, txtLastName.Text, txtBlockReason.Text, UserName.UserName);
                if (statusSave)
                    JsClientAlert("SuccessSaveBlock", "Save Successful.");
                else
                    JsClientAlert("ErrorSaveBlock", "Error Save Data.");
            }
            else
            {
                JsClientAlert("ErrorEmail", "Please insert correct email.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {

    }

   
}
