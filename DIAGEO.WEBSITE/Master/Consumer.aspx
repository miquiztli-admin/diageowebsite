﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Consumer.aspx.cs" Inherits="Master_Consumer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Consumer</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../Style/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="../Style/block.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery.blockUI.js" type="text/javascript"></script>
    <base target="_self"/>
    <script type="text/javascript">  
    $(function(){   
    var dateBefore=null;   
    $("#txtDateOfBirth").datepicker({   
        dateFormat: 'dd/mm/yy',   
        showOn: 'button',      
        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],    
        monthNamesShort: ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'],   
        changeMonth: true,   
        changeYear: true ,   
        beforeShow:function(){   
            if($(this).val()!=""){   
                var arrayDate=$(this).val().split("/");        
                arrayDate[2]=parseInt(arrayDate[2]);   
                $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);   
            }   
            setTimeout(function(){   
                $.each($(".ui-datepicker-year option"),function(j,k){   
                    var textYear=parseInt($(".ui-datepicker-year option").eq(j).val());   
                    $(".ui-datepicker-year option").eq(j).text(textYear);   
                });                
            },50);   
  
        },   
        onChangeMonthYear: function(){   
            setTimeout(function(){   
                $.each($(".ui-datepicker-year option"),function(j,k){   
                    var textYear=parseInt($(".ui-datepicker-year option").eq(j).val());   
                    $(".ui-datepicker-year option").eq(j).text(textYear);   
                });                
            },50);         
        },   
        onClose:function(){   
            if($(this).val()!="" && $(this).val()==dateBefore){            
                var arrayDate=dateBefore.split("/");   
                arrayDate[2]=parseInt(arrayDate[2]);   
                $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);       
            }          
        },   
        onSelect: function(dateText, inst){    
            dateBefore=$(this).val();   
            var arrayDate=dateText.split("/");   
            arrayDate[2]=parseInt(arrayDate[2]);   
            $(this).val(arrayDate[0]+"/"+arrayDate[1]+"/"+arrayDate[2]);   
        }   
  
        });   
       
    });   
    </script>
    
    <script type="text/javascript">  
      function  blockUI()
      {
        $.blockUI({message: '<h1><img src="../Image/Load.gif" /></h1>'});
      }
    </script>
     
    <script type="text/javascript">  
      function  unblockUI()
      {
        $.unblockUI();
      }
    </script>
    

</head>
<body style="background-color:Silver">
    <form id="FrmUser" runat="server">
    <div style="background-color:Silver">
        <fieldset>
        <legend class="ntextblack">Consumer</legend>
        <br />
            <table align="center" style="background-color: Silver">
                <tr>
                    <td align="right">
                        <asp:Label ID="lblFirstName" CssClass="ntextblack" runat="server" Text="First Name : "></asp:Label>
                        <font color="red"> * </font>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtFirstName" CssClass="ntextblackdata" runat="server" 
                            MaxLength="50" TabIndex="2"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="lblLastName" CssClass="ntextblack" runat="server" Text="Last Name : "></asp:Label>
                        <font color="red"> * </font>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtLastName" CssClass="ntextblackdata" runat="server" 
                            MaxLength="50" TabIndex="3"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblEmail" CssClass="ntextblack" runat="server" Text="E-mail : "></asp:Label>
                        <font color="red"> * </font>
                    </td>
                    <td align="left" style="margin-left: 40px">
                        <asp:TextBox ID="txtEmail" CssClass="ntextblackdata" runat="server" 
                            MaxLength="50" TabIndex="4"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="lblMobile" CssClass="ntextblack" runat="server" Text="Mobile NO. : "></asp:Label>
                        <font color="red"> * </font>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtMobile" CssClass="ntextblackdata" runat="server" 
                            Onkeypress="Javascript:return validateNumKey();" MaxLength="10" 
                            TabIndex="5"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblDateOfBirth" CssClass="ntextblack" runat="server" Text="BirthDay : "></asp:Label>
                        <font color="red"> * </font>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtDateOfBirth" CssClass="ntextblackdata" runat="server" 
                            MaxLength="10" TabIndex="6"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="lblGender" runat="server" Text="Gender :" CssClass="ntextblack"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="rdoGender" runat="server" CssClass="ntextblack" 
                            RepeatDirection="Horizontal" TabIndex="7">
                            <asp:ListItem Text="Male" Value="MALE" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Female" Value="FEMALE"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="1">
                        <asp:Label ID="lblIdCard" CssClass="ntextblack" runat="server" Text="National ID : "></asp:Label>
                    </td>
                    <td align="left" colspan="3">
                        <asp:TextBox ID="txtIdCard" CssClass="ntextblackdata" runat="server" 
                            Onkeypress="Javascript:return validateNumKey();" CausesValidation="True" 
                            MaxLength="13" TabIndex="1"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblHomeAddress" CssClass="ntextblack" runat="server" Text="Home Address : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtHomeAddress" CssClass="ntextblackdata" runat="server" 
                            MaxLength="90" TabIndex="8"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="lblHomeMoo" CssClass="ntextblack" runat="server" Text="Home Moo : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtHomeMoo" CssClass="ntextblackdata" runat="server" 
                            MaxLength="50" TabIndex="9"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblHomeVillage" CssClass="ntextblack" runat="server" Text="Home Village : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtHomeVillage" CssClass="ntextblackdata" runat="server" 
                            MaxLength="50" TabIndex="10"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="lblHomeSoi" CssClass="ntextblack" runat="server" Text="Home Soi : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtHomeSoi" CssClass="ntextblackdata" runat="server" 
                            TabIndex="11"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblHomeRoad" CssClass="ntextblack" runat="server" Text="Home Road : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtHomeRoad" CssClass="ntextblackdata" runat="server" 
                            TabIndex="12"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="lblHomeSubDistrict" CssClass="ntextblack" runat="server" Text="Home SubDistrict : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtHomeSubDistrict" CssClass="ntextblackdata" runat="server" 
                            TabIndex="13"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblHomeDistrict" CssClass="ntextblack" runat="server" Text="Home District : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtHomeDistrict" CssClass="ntextblackdata" runat="server" 
                            MaxLength="50" TabIndex="14"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="lblHomeProvince" CssClass="ntextblack" runat="server" Text="Home Province : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlHomeProvince" CssClass="ntextblack" runat="server" 
                            TabIndex="15">
                            <asp:ListItem Text="Please Select" Value=""></asp:ListItem>
                            <asp:ListItem Text="เชียงใหม่" Value="เชียงใหม่"></asp:ListItem>
                            <asp:ListItem Text="เชียงราย" Value="เชียงราย"></asp:ListItem>
                            <asp:ListItem Text="เพชรบุรี" Value="เพชรบุรี"></asp:ListItem>
                            <asp:ListItem Text="เพชรบูรณ์" Value="เพชรบูรณ์"></asp:ListItem>
                            <asp:ListItem Text="เลย" Value="เลย"></asp:ListItem>
                            <asp:ListItem Text="แพร่" Value="แพร่"></asp:ListItem>
                            <asp:ListItem Text="แม่ฮ่องสอน" Value="แม่ฮ่องสอน"></asp:ListItem>
                            <asp:ListItem Text="กระบี่" Value="กระบี่"></asp:ListItem>
                            <asp:ListItem Text="กรุงเทพมหานคร" Value="กรุงเทพมหานคร"></asp:ListItem>
                            <asp:ListItem Text="กาญจนบุรี" Value="กาญจนบุรี"></asp:ListItem>
                            <asp:ListItem Text="กาฬสินธุ์" Value="กาฬสินธุ์"></asp:ListItem>
                            <asp:ListItem Text="กำแพงเพชร" Value="กำแพงเพชร"></asp:ListItem>
                            <asp:ListItem Text="ขอนแก่น" Value="ขอนแก่น"></asp:ListItem>
                            <asp:ListItem Text="จันทบุรี" Value="จันทบุรี"></asp:ListItem>
                            <asp:ListItem Text="ฉะเชิงเทรา" Value="ฉะเชิงเทรา"></asp:ListItem>
                            <asp:ListItem Text="ชลบุรี" Value="ชลบุรี"></asp:ListItem>
                            <asp:ListItem Text="ชัยนาท" Value="ชัยนาท"></asp:ListItem>
                            <asp:ListItem Text="ชัยภูมิ" Value="ชัยภูมิ"></asp:ListItem>
                            <asp:ListItem Text="ชุมพร" Value="ชุมพร"></asp:ListItem>
                            <asp:ListItem Text="ตรัง" Value="ตรัง"></asp:ListItem>
                            <asp:ListItem Text="ตราด" Value="ตราด"></asp:ListItem>
                            <asp:ListItem Text="ตาก" Value="ตาก"></asp:ListItem>
                            <asp:ListItem Text="นครนายก" Value="นครนายก"></asp:ListItem>
                            <asp:ListItem Text="นครปฐม" Value="นครปฐม"></asp:ListItem>
                            <asp:ListItem Text="นครพนม" Value="นครพนม"></asp:ListItem>
                            <asp:ListItem Text="นครราชสีมา" Value="นครราชสีมา"></asp:ListItem>
                            <asp:ListItem Text="นครศรีธรรมราช" Value="นครศรีธรรมราช"></asp:ListItem>
                            <asp:ListItem Text="นครสวรรค์" Value="นครสวรรค์"></asp:ListItem>
                            <asp:ListItem Text="นนทบุรี" Value="นนทบุรี"></asp:ListItem>
                            <asp:ListItem Text="นราธิวาส" Value="นราธิวาส"></asp:ListItem>
                            <asp:ListItem Text="น่าน" Value="น่าน"></asp:ListItem>
                            <asp:ListItem Text="บึงกาฬ" Value="บึงกาฬ"></asp:ListItem>
                            <asp:ListItem Text="บุรีรัมย์" Value="บุรีรัมย์"></asp:ListItem>
                            <asp:ListItem Text="ปทุมธานี" Value="ปทุมธานี"></asp:ListItem>
                            <asp:ListItem Text="ประจวบคีรีขันธ์" Value="ประจวบคีรีขันธ์"></asp:ListItem>
                            <asp:ListItem Text="ปราจีนบุรี" Value="ปราจีนบุรี"></asp:ListItem>
                            <asp:ListItem Text="ปัตตานี" Value="ปัตตานี"></asp:ListItem>
                            <asp:ListItem Text="พระนครศรีอยุธยา" Value="พระนครศรีอยุธยา"></asp:ListItem>
                            <asp:ListItem Text="พะเยา" Value="พะเยา"></asp:ListItem>
                            <asp:ListItem Text="พังงา" Value="พังงา"></asp:ListItem>
                            <asp:ListItem Text="พัทลุง" Value="พัทลุง"></asp:ListItem>
                            <asp:ListItem Text="พิจิตร" Value="พิจิตร"></asp:ListItem>
                            <asp:ListItem Text="พิษณุโลก" Value="พิษณุโลก"></asp:ListItem>
                            <asp:ListItem Text="ภูเก็ต" Value="ภูเก็ต"></asp:ListItem>
                            <asp:ListItem Text="มหาสารคาม" Value="มหาสารคาม"></asp:ListItem>
                            <asp:ListItem Text="มุกดาหาร" Value="มุกดาหาร"></asp:ListItem>
                            <asp:ListItem Text="ยโสธร" Value="ยโสธร"></asp:ListItem>
                            <asp:ListItem Text="ยะลา" Value="ยะลา"></asp:ListItem>
                            <asp:ListItem Text="ร้อยเอ็ด" Value="ร้อยเอ็ด"></asp:ListItem>
                            <asp:ListItem Text="ระนอง" Value="ระนอง"></asp:ListItem>
                            <asp:ListItem Text="ระยอง" Value="ระยอง"></asp:ListItem>
                            <asp:ListItem Text="ราชบุรี" Value="ราชบุรี"></asp:ListItem>
                            <asp:ListItem Text="ลพบุรี" Value="ลพบุรี"></asp:ListItem>
                            <asp:ListItem Text="ลำปาง" Value="ลำปาง"></asp:ListItem>
                            <asp:ListItem Text="ลำพูน" Value="ลำพูน"></asp:ListItem>
                            <asp:ListItem Text="ศรีสะเกษ" Value="ศรีสะเกษ"></asp:ListItem>
                            <asp:ListItem Text="สกลนคร" Value="สกลนคร"></asp:ListItem>
                            <asp:ListItem Text="สงขลา" Value="สงขลา"></asp:ListItem>
                            <asp:ListItem Text="สตูล" Value="สตูล"></asp:ListItem>
                            <asp:ListItem Text="สมุทรปราการ" Value="สมุทรปราการ"></asp:ListItem>
                            <asp:ListItem Text="สมุทรสงคราม" Value="สมุทรสงคราม"></asp:ListItem>
                            <asp:ListItem Text="สมุทรสาคร" Value="สมุทรสาคร"></asp:ListItem>
                            <asp:ListItem Text="สระแก้ว" Value="สระแก้ว"></asp:ListItem>
                            <asp:ListItem Text="สระบุรี" Value="สระบุรี"></asp:ListItem>
                            <asp:ListItem Text="สิงห์บุรี" Value="สิงห์บุรี"></asp:ListItem>
                            <asp:ListItem Text="สุโขทัย" Value="สุโขทัย"></asp:ListItem>
                            <asp:ListItem Text="สุพรรณบุรี" Value="สุพรรณบุรี"></asp:ListItem>
                            <asp:ListItem Text="สุราษฎร์ธานี" Value="สุราษฎร์ธานี"></asp:ListItem>
                            <asp:ListItem Text="สุรินทร์" Value="สุรินทร์"></asp:ListItem>
                            <asp:ListItem Text="หนองคาย" Value="หนองคาย"></asp:ListItem>
                            <asp:ListItem Text="หนองบัวลำภู" Value="หนองบัวลำภู"></asp:ListItem>
                            <asp:ListItem Text="อ่างทอง" Value="อ่างทอง"></asp:ListItem>
                            <asp:ListItem Text="อำนาจเจริญ" Value="อำนาจเจริญ"></asp:ListItem>
                            <asp:ListItem Text="อุดรธานี" Value="อุดรธานี"></asp:ListItem>
                            <asp:ListItem Text="อุตรดิตถ์" Value="อุตรดิตถ์"></asp:ListItem>
                            <asp:ListItem Text="อุทัยธานี" Value="อุทัยธานี"></asp:ListItem>
                            <asp:ListItem Text="อุบลราชธานี" Value="อุบลราชธานี"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblWorkAddress" CssClass="ntextblack" runat="server" Text="Work Address : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtWorkAddress" CssClass="ntextblackdata" runat="server" 
                            TabIndex="16"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="lblWorkMoo" CssClass="ntextblack" runat="server" Text="Work Moo : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtWorkMoo" CssClass="ntextblackdata" runat="server" 
                            TabIndex="17"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblWorkVillage" CssClass="ntextblack" runat="server" Text="Work Village : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtWorkVillage" CssClass="ntextblackdata" runat="server" 
                            MaxLength="50" TabIndex="18"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="lblWorkSoi" CssClass="ntextblack" runat="server" Text="Work Soi : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtWorkSoi" CssClass="ntextblackdata" runat="server" 
                            MaxLength="50" TabIndex="19"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblWorkRoad" CssClass="ntextblack" runat="server" Text="Work Road : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtWorkRoad" CssClass="ntextblackdata" runat="server" 
                            TabIndex="20"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="lblWorkSubDistrict" CssClass="ntextblack" runat="server" Text="Work SubDistrict : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtWorkSubDistrict" CssClass="ntextblackdata" runat="server" 
                            TabIndex="21"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblWorkDistrict" CssClass="ntextblack" runat="server" Text="Work District : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtWorkDistrict" CssClass="ntextblackdata" runat="server" 
                            TabIndex="22"></asp:TextBox>
                    </td>
                    <td align="right">
                        <asp:Label ID="lblWorkProvince" CssClass="ntextblack" runat="server" Text="Work Province : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlWorkProvince" CssClass="ntextblack" runat="server" 
                            TabIndex="23">
                            <asp:ListItem Text="Please Select" Value=""></asp:ListItem>
                            <asp:ListItem Text="เชียงใหม่" Value="เชียงใหม่"></asp:ListItem>
                            <asp:ListItem Text="เชียงราย" Value="เชียงราย"></asp:ListItem>
                            <asp:ListItem Text="เพชรบุรี" Value="เพชรบุรี"></asp:ListItem>
                            <asp:ListItem Text="เพชรบูรณ์" Value="เพชรบูรณ์"></asp:ListItem>
                            <asp:ListItem Text="เลย" Value="เลย"></asp:ListItem>
                            <asp:ListItem Text="แพร่" Value="แพร่"></asp:ListItem>
                            <asp:ListItem Text="แม่ฮ่องสอน" Value="แม่ฮ่องสอน"></asp:ListItem>
                            <asp:ListItem Text="กระบี่" Value="กระบี่"></asp:ListItem>
                            <asp:ListItem Text="กรุงเทพมหานคร" Value="กรุงเทพมหานคร"></asp:ListItem>
                            <asp:ListItem Text="กาญจนบุรี" Value="กาญจนบุรี"></asp:ListItem>
                            <asp:ListItem Text="กาฬสินธุ์" Value="กาฬสินธุ์"></asp:ListItem>
                            <asp:ListItem Text="กำแพงเพชร" Value="กำแพงเพชร"></asp:ListItem>
                            <asp:ListItem Text="ขอนแก่น" Value="ขอนแก่น"></asp:ListItem>
                            <asp:ListItem Text="จันทบุรี" Value="จันทบุรี"></asp:ListItem>
                            <asp:ListItem Text="ฉะเชิงเทรา" Value="ฉะเชิงเทรา"></asp:ListItem>
                            <asp:ListItem Text="ชลบุรี" Value="ชลบุรี"></asp:ListItem>
                            <asp:ListItem Text="ชัยนาท" Value="ชัยนาท"></asp:ListItem>
                            <asp:ListItem Text="ชัยภูมิ" Value="ชัยภูมิ"></asp:ListItem>
                            <asp:ListItem Text="ชุมพร" Value="ชุมพร"></asp:ListItem>
                            <asp:ListItem Text="ตรัง" Value="ตรัง"></asp:ListItem>
                            <asp:ListItem Text="ตราด" Value="ตราด"></asp:ListItem>
                            <asp:ListItem Text="ตาก" Value="ตาก"></asp:ListItem>
                            <asp:ListItem Text="นครนายก" Value="นครนายก"></asp:ListItem>
                            <asp:ListItem Text="นครปฐม" Value="นครปฐม"></asp:ListItem>
                            <asp:ListItem Text="นครพนม" Value="นครพนม"></asp:ListItem>
                            <asp:ListItem Text="นครราชสีมา" Value="นครราชสีมา"></asp:ListItem>
                            <asp:ListItem Text="นครศรีธรรมราช" Value="นครศรีธรรมราช"></asp:ListItem>
                            <asp:ListItem Text="นครสวรรค์" Value="นครสวรรค์"></asp:ListItem>
                            <asp:ListItem Text="นนทบุรี" Value="นนทบุรี"></asp:ListItem>
                            <asp:ListItem Text="นราธิวาส" Value="นราธิวาส"></asp:ListItem>
                            <asp:ListItem Text="น่าน" Value="น่าน"></asp:ListItem>
                            <asp:ListItem Text="บึงกาฬ" Value="บึงกาฬ"></asp:ListItem>
                            <asp:ListItem Text="บุรีรัมย์" Value="บุรีรัมย์"></asp:ListItem>
                            <asp:ListItem Text="ปทุมธานี" Value="ปทุมธานี"></asp:ListItem>
                            <asp:ListItem Text="ประจวบคีรีขันธ์" Value="ประจวบคีรีขันธ์"></asp:ListItem>
                            <asp:ListItem Text="ปราจีนบุรี" Value="ปราจีนบุรี"></asp:ListItem>
                            <asp:ListItem Text="ปัตตานี" Value="ปัตตานี"></asp:ListItem>
                            <asp:ListItem Text="พระนครศรีอยุธยา" Value="พระนครศรีอยุธยา"></asp:ListItem>
                            <asp:ListItem Text="พะเยา" Value="พะเยา"></asp:ListItem>
                            <asp:ListItem Text="พังงา" Value="พังงา"></asp:ListItem>
                            <asp:ListItem Text="พัทลุง" Value="พัทลุง"></asp:ListItem>
                            <asp:ListItem Text="พิจิตร" Value="พิจิตร"></asp:ListItem>
                            <asp:ListItem Text="พิษณุโลก" Value="พิษณุโลก"></asp:ListItem>
                            <asp:ListItem Text="ภูเก็ต" Value="ภูเก็ต"></asp:ListItem>
                            <asp:ListItem Text="มหาสารคาม" Value="มหาสารคาม"></asp:ListItem>
                            <asp:ListItem Text="มุกดาหาร" Value="มุกดาหาร"></asp:ListItem>
                            <asp:ListItem Text="ยโสธร" Value="ยโสธร"></asp:ListItem>
                            <asp:ListItem Text="ยะลา" Value="ยะลา"></asp:ListItem>
                            <asp:ListItem Text="ร้อยเอ็ด" Value="ร้อยเอ็ด"></asp:ListItem>
                            <asp:ListItem Text="ระนอง" Value="ระนอง"></asp:ListItem>
                            <asp:ListItem Text="ระยอง" Value="ระยอง"></asp:ListItem>
                            <asp:ListItem Text="ราชบุรี" Value="ราชบุรี"></asp:ListItem>
                            <asp:ListItem Text="ลพบุรี" Value="ลพบุรี"></asp:ListItem>
                            <asp:ListItem Text="ลำปาง" Value="ลำปาง"></asp:ListItem>
                            <asp:ListItem Text="ลำพูน" Value="ลำพูน"></asp:ListItem>
                            <asp:ListItem Text="ศรีสะเกษ" Value="ศรีสะเกษ"></asp:ListItem>
                            <asp:ListItem Text="สกลนคร" Value="สกลนคร"></asp:ListItem>
                            <asp:ListItem Text="สงขลา" Value="สงขลา"></asp:ListItem>
                            <asp:ListItem Text="สตูล" Value="สตูล"></asp:ListItem>
                            <asp:ListItem Text="สมุทรปราการ" Value="สมุทรปราการ"></asp:ListItem>
                            <asp:ListItem Text="สมุทรสงคราม" Value="สมุทรสงคราม"></asp:ListItem>
                            <asp:ListItem Text="สมุทรสาคร" Value="สมุทรสาคร"></asp:ListItem>
                            <asp:ListItem Text="สระแก้ว" Value="สระแก้ว"></asp:ListItem>
                            <asp:ListItem Text="สระบุรี" Value="สระบุรี"></asp:ListItem>
                            <asp:ListItem Text="สิงห์บุรี" Value="สิงห์บุรี"></asp:ListItem>
                            <asp:ListItem Text="สุโขทัย" Value="สุโขทัย"></asp:ListItem>
                            <asp:ListItem Text="สุพรรณบุรี" Value="สุพรรณบุรี"></asp:ListItem>
                            <asp:ListItem Text="สุราษฎร์ธานี" Value="สุราษฎร์ธานี"></asp:ListItem>
                            <asp:ListItem Text="สุรินทร์" Value="สุรินทร์"></asp:ListItem>
                            <asp:ListItem Text="หนองคาย" Value="หนองคาย"></asp:ListItem>
                            <asp:ListItem Text="หนองบัวลำภู" Value="หนองบัวลำภู"></asp:ListItem>
                            <asp:ListItem Text="อ่างทอง" Value="อ่างทอง"></asp:ListItem>
                            <asp:ListItem Text="อำนาจเจริญ" Value="อำนาจเจริญ"></asp:ListItem>
                            <asp:ListItem Text="อุดรธานี" Value="อุดรธานี"></asp:ListItem>
                            <asp:ListItem Text="อุตรดิตถ์" Value="อุตรดิตถ์"></asp:ListItem>
                            <asp:ListItem Text="อุทัยธานี" Value="อุทัยธานี"></asp:ListItem>
                            <asp:ListItem Text="อุบลราชธานี" Value="อุบลราชธานี"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label4" CssClass="ntextblack" runat="server" Text="DIAGEO SubScribeCallCenter : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:RadioButtonList ID="rdoDGCallCenterActive" CssClass="ntextblackdata" 
                            runat="server" RepeatDirection="Horizontal"
                            TabIndex="24">
                            <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td align="right">
                        <asp:Label ID="Label5" CssClass="ntextblack" runat="server" Text="DIAGEO SubScribeEmail : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:RadioButtonList ID="rdoDGEmailActive" CssClass="ntextblackdata" 
                            runat="server" RepeatDirection="Horizontal"
                            TabIndex="25">
                            <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label6" CssClass="ntextblack" runat="server" Text="DIAGEO SubScribeSMS : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:RadioButtonList ID="rdoDGSMSActive" CssClass="ntextblackdata" 
                            runat="server" RepeatDirection="Horizontal"
                            TabIndex="26">
                            <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td align="right">
                        <asp:Label ID="Label1" CssClass="ntextblack" runat="server" Text="Outlet SubScribeCallCenter : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:RadioButtonList ID="rdoOutletCallCenterActive" CssClass="ntextblackdata" 
                            runat="server" RepeatDirection="Horizontal"
                            TabIndex="27">
                            <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label2" CssClass="ntextblack" runat="server" Text="Outlet SubScribeEmail : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:RadioButtonList ID="rdoOutletEmailActive" CssClass="ntextblackdata" 
                            runat="server" RepeatDirection="Horizontal"
                            TabIndex="28">
                            <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td align="right">
                        <asp:Label ID="Label3" CssClass="ntextblack" runat="server" Text="Outlet SubScribeSMS : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:RadioButtonList ID="rdoOutletSMSActive" CssClass="ntextblackdata" 
                            runat="server" RepeatDirection="Horizontal"
                            TabIndex="29">
                            <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">
                        <asp:Button ID="btnSave" CssClass="ntextblack" runat="server" Text="บันทึก" OnClientClick="blockUI();"
                            OnClick="btnSave_Click" TabIndex="30" />&nbsp;
                        <asp:Button ID="btnCancel" CssClass="ntextblack" runat="server" Text="ยกเลิก" OnClick="btnCancel_Click"
                            TabIndex="31" />
                    </td>
                </tr>
            </table>
        <br />
        </fieldset>
    </div>
    </form>
</body>
</html>
