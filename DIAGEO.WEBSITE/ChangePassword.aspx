﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <title></title>
    <style type="text/css">

        .label
        {
        	text-align:right;
        	width:165px;
        	padding-right:10px;
        }
    .buttons                { border:1px solid #000000; background-color:#FFFFFF; font-size:11px;cursor:pointer;}
        .style1
        {
            font-family: "Century Gothic";
            font-weight: bold;
            width: 207px;
        }
        .ntextblack
        {
            font-family: "Century Gothic";
            font-weight: 700;
        }
        .style2
        {
            font-family: "Century Gothic";
            font-weight: 700;
            width: 207px;
        }
        .style3
        {
            width: 207px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
            <table border="0" width="450px" align="center">
                <colgroup>
                    <col />
                    <col style="width:150px" />
                </colgroup>
                <tr align="left">
                    <td colspan="2" valign="middle">
                        <b>
                        <a style="font-family:Century Gothic; font-size:large;  color:Red; padding-left:10px;">Change Password</a></b>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="style1">
                        Old Password:
                    </td >
                    <td align="left" valign="middle" style="height:28px;">
                        <asp:TextBox ID="txtOldPassword" runat="server" Width="145px" MaxLength="14" 
                            TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="style2">
                        New Password:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtNewPassword" runat="server" Width="145px" MaxLength="14" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="style2">
                        Re-Type New Password:
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtRenewPassword" runat="server" Width="145px" MaxLength="14" TextMode="Password"></asp:TextBox>
                    </td>
                    
                </tr>
                <tr>
                    <td class="style3"></td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="right" class="style3">
                    </td>
                    <td align="right">                   
                        <asp:Button ID="btnOk" runat="server" Text="Ok" onclick="btnOk_Click" />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                    </td>
                </tr>
            </table>
    
    </div>
    </form>
</body>
</html>
