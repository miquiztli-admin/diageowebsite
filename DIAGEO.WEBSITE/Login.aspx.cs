﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DIAGEO.BLL;
using DIAGEO.COMMON;


public partial class Security_Login : BasePage
{
    #region Method

    public bool ValidateLogin()
    {
        try
        {
            if (txtUserName.Text == "")
            {
                JsClientAlert("Username", "โปรดกรอก User Name");
                return false;
            }

            if (txtPassword.Text == "")
            {
                JsClientAlert("Password", "โปรดกรอก Password");
                return false;
            }

            return true;
        }
        catch (Exception ex)
        {
            throw new Exception("ValidateLogin :: " + ex.Message);
        }
    }

    #endregion

    #region Event

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblVersion.Text = ConfigurationSettings.AppSettings["Version"];
            if (!IsPostBack)
            {
                Session.Clear();
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Login.Page_Load" + ex.Message);
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        UserBLL userBLL;
        DataTable dtUser = new DataTable();
        DIAGEO.COMMON.User user;

        userBLL = new UserBLL();
        user = new DIAGEO.COMMON.User();

        try
        {

            if (ValidateLogin())
            {
                dtUser = userBLL.SelectOneBYUserNameAndPassword(txtUserName.Text, "", "");
                if (dtUser.Rows.Count != 0)
                {
                    dtUser = userBLL.SelectOneBYUserNameAndPassword(txtUserName.Text, md5(txtPassword.Text), "");
                    if (dtUser.Rows.Count != 0)
                    {
                        if (dtUser.Rows[0]["active"].ToString() == "F")
                        {
                            JsClientAlert("NotPermission", "User นี้ยังไม่ได้รับการอนุมัติให้ใช้งาน");
                            return;
                        }

                        user.UserID = dtUser.Rows[0]["userId"].ToString();
                        user.UserName = dtUser.Rows[0]["username"].ToString();
                        user.Password = dtUser.Rows[0]["password"].ToString();
                        user.PasswordNonEncode = txtPassword.Text.ToString();
                        user.Email = dtUser.Rows[0]["email"].ToString();
                        user.UserGroupID = dtUser.Rows[0]["UserGroupId"].ToString();
                    }
                    else
                    {
                        JsClientAlert("WrongPassword", "User หรือ Password ไม่ถูกต้อง");
                        return;
                    }
                }

                else
                {
                    JsClientAlert("NotFoundUser", "User หรือ Password ไม่ถูกต้อง");
                    return;
                }

                Response.Redirect("~/Home.aspx");
            }
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Login.btnLogin.Click :: " + ex.Message);
        }
        finally
        {
            user.Dispose();
            dtUser.Dispose();
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            txtUserName.Text = "";
            txtPassword.Text = "";
        }
        catch (Exception ex)
        {
            JsClientAlert("ErrorMessage", "Login.btnCancel_Click :: " + ex.Message);
        }
    }

    #endregion
}
