﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using System.Data;

namespace DIAGEO.BLL
{
    public class CallCenterBounceMappingBLL
    {
        private CallCenterBounceMappingDAL dalCallCenter;

        public DataTable SelectAll()
        {
            dalCallCenter = new CallCenterBounceMappingDAL();
            try
            {
                return dalCallCenter.SelectAll();
            }
            catch (Exception ex)
            {
                throw new Exception("CallCenterBounceMappingBLL.SelectAll :: " + ex.Message);
            }
        }

        public DataTable GetBounceStatus(string Code)
        {
            try
            {
                dalCallCenter = new CallCenterBounceMappingDAL();
                return dalCallCenter.GetBounceStatus(Code);
            }
            catch (Exception ex)
            {
                throw new Exception("CallCenterBounceMappingBLL.GetBounceStatus :: " + ex.Message);
            }
        }
    }
}
