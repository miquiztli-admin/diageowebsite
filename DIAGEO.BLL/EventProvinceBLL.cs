﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using DIAGEO.DAL.GDAL;
using System.Data;

namespace DIAGEO.BLL
{
    public class EventProvinceBLL
    {
        public DataTable SelectByEventID(int eventID)
        {
            EventProvinceDAL eventProvinceDAL;

            try
            {
                eventProvinceDAL = new EventProvinceDAL();
                return eventProvinceDAL.SelectBYEventID(eventID);
            }
            catch (Exception ex)
            {
                
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectByEventIDAndDataGroup(int eventID, int dataGroup)
        {
            EventProvinceDAL eventProvinceDAL;

            try
            {
                eventProvinceDAL = new EventProvinceDAL();
                return eventProvinceDAL.SelectByEventIDAndDataGroup(eventID, dataGroup);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
