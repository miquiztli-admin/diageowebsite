﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DIAGEO.DAL;
using DIAGEO.DAL.GDAL;
using DIAGEO.COMMON;

namespace DIAGEO.BLL
{
    public class UserBLL 
    {

        public DataTable CheckAuthentication(int UserId, int MenuId)
        {
            UserDAL userDal;

            try
            {
                userDal = new UserDAL();

                return userDal.CheckAuthentication(UserId,MenuId);
            }
            catch (Exception ex)
            {
                throw new Exception("UserBLL.CheckAuthentication :: " + ex.Message);
            }
        } 

        public DataTable SelectAll()
        {
            UserDAL userDAL;

            try
            {
                userDAL = new UserDAL();
                return userDAL.SelectAll();
            }
            catch (Exception ex)
            {                
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectOne(int userID)
        {
            UserDAL userDAL;

            try
            {
                userDAL = new UserDAL();
                userDAL.UserId = userID;
                return userDAL.SelectOne();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        } 

        public DataTable SelectOneBYUserNameAndPassword(string userName, string password, string email)
        {
            UserDAL userDAL;
            DataTable dtUser;

            try
            {
                userDAL = new UserDAL();
                dtUser = userDAL.SelectOneBYUserNameAndPassword(userName, password, email);
                return dtUser;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable Search(string userName, string password, string email,
        string userGroupId, string active, int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            UserDAL userDAL;
            DataTable dtUser;

            try
            {
                userDAL = new UserDAL();
                dtUser = userDAL.Search(userName, password, email,
                         userGroupId, active, startRowIndex, maximumRows, sortExpression, isCount);

                return dtUser;
            }
            catch (Exception ex)
            {                
                throw new Exception(ex.Message);
            }
        }

        public bool Insert(string userName, string fullname, string password, string email, string userGroupId, string active, string userIDLogin)
        {
            UserDAL userDAL;
            
            try
            {
                userDAL = new UserDAL();             
                userDAL.Username = userName;
                userDAL.Fullname = fullname;
                userDAL.Password = password;
                userDAL.Email = email;
                userDAL.Active = active;
                userDAL.UserGroupId = int.Parse(userGroupId);
                userDAL.CreateBy = userIDLogin;
                userDAL.CreateDate = DateTime.Now;
                userDAL.UpdateBy = userIDLogin;
                userDAL.UpdateDate = DateTime.Now;
                userDAL.Insert();
                return true;
            }
            catch (Exception ex)
            {                
                throw new Exception(ex.Message);
            }
        }

        public bool UpdateSearch(string userName, string fullName, string password, string email, string userGroupId, string active, string userID, string userIDLogin)
        {
            UserDAL userDAL;
            
            try
            {
                userDAL = new UserDAL();
                userDAL.UserId = int.Parse(userID);
                userDAL.Username = userName;
                userDAL.Password = password;
                userDAL.Email = email;
                userDAL.UserGroupId = int.Parse(userGroupId);
                userDAL.Active = active;
                userDAL.Fullname = fullName;
                userDAL.UpdateSearch(userIDLogin);
                return true;
            }
            catch (Exception ex)
            {                
                throw new Exception(ex.Message);
            }
        }


        public bool UpdateNewPassword(string OldPass, string NewPass, string UserID)
        {
            UserDAL userDAL;
            
            try
            {
                userDAL = new UserDAL();
                userDAL.UpdateNewPassword(OldPass, NewPass, UserID);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
