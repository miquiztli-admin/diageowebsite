﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DIAGEO.DAL;
using DIAGEO.DAL.GDAL;

namespace DIAGEO.BLL
{
    public class OutboundEventBLL
    {
        public int EventID { get; set; }

        public DataTable SelectActiveOutboundEventMaster()
        {
            OutboundEventDAL outboundEventDAL;

            try
            {
                outboundEventDAL = new OutboundEventDAL();
                return outboundEventDAL.SelectOutboundEventMaster();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectOne()
        {
            OutboundEventDAL outboundEventDAL;

            try
            {
                outboundEventDAL = new OutboundEventDAL();
                return outboundEventDAL.SelectOne();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public DataTable SelectOne(int eventId)
        {
            OutboundEventDAL outboundEventDAL;

            try
            {
                outboundEventDAL = new OutboundEventDAL();
                outboundEventDAL.EventId = eventId;
                return outboundEventDAL.SelectOne();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectOneBYOutboundEventName(string eventName)
        {
            OutboundEventDAL outboundEventDAL;

            try
            {
                outboundEventDAL = new OutboundEventDAL();
                return outboundEventDAL.SelectOneBYOutboundEventName(eventName);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectByEventID(int eventID)
        {
            OutboundEventDAL outboundEventDAL;

            try
            {
                outboundEventDAL = new OutboundEventDAL();
                return outboundEventDAL.SelectBYEventID(eventID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public DataTable Search(string eventName, int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            OutboundEventDAL outboundEventDAL;
            DataTable dtOutlet;

            try
            {
                outboundEventDAL = new OutboundEventDAL();
                dtOutlet = outboundEventDAL.Search(eventName, startRowIndex, maximumRows, sortExpression, isCount);

                return dtOutlet;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SearchEventBirthDay(string eventName, string active, int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            OutboundEventDAL outboundEventDAL;
            DataTable dtOutlet;

            try
            {
                outboundEventDAL = new OutboundEventDAL();
                dtOutlet = outboundEventDAL.SearchEventBirthDay(eventName, active, startRowIndex, maximumRows, sortExpression, isCount);

                return dtOutlet;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool UpdateSearch(string eventId, string eventName, string startAge, string endAge, string brandPreBaileys,
        string brandPreBenmore, string brandPreJwBlack, string brandPreJwGold, string brandPreJwGreen, string brandPreJwRed,
        string brandPreSmirnoff, string active, string userIDLogin)
        {
            OutboundEventDAL outboundEventDAL;

            try
            {
                outboundEventDAL = new OutboundEventDAL();
                outboundEventDAL.EventId = int.Parse(eventId);
                outboundEventDAL.Name = eventName;
                outboundEventDAL.StartAge = int.Parse(startAge);
                outboundEventDAL.EndAge = int.Parse(endAge);
                outboundEventDAL.BrandPreBaileys = brandPreBaileys;
                outboundEventDAL.BrandPreBenmore = brandPreBenmore;
                outboundEventDAL.BrandPreJwBlack = brandPreJwBlack;
                outboundEventDAL.BrandPreJwGold = brandPreJwGold;
                outboundEventDAL.BrandPreJwGreen = brandPreJwGreen;
                outboundEventDAL.BrandPreJwRed = brandPreJwRed;
                outboundEventDAL.BrandPreSmirnoff = brandPreSmirnoff;
                outboundEventDAL.Active = active;
                outboundEventDAL.UpdateDate = DateTime.Now;
                outboundEventDAL.UpdateSearch(userIDLogin);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Insert(string eventName, string startAge, string endAge,
        string brandPre100Pipers, string brandPre100Pipers8Y, string brandPreAbsolut,
        string brandPreBaileys, string brandPreBallentine,
        string brandPreBenmore, string brandPreBlend, string brandPreChivas,
        string brandPreDewar, string brandPreJwBlack, string brandPreJwGold, string brandPreJwGreen, string brandPreJwRed,
        string brandPreNotdrink, string brandPreOthers, string brandPreSmirnoff, string active, string userIDLogin)
        {
            OutboundEventDAL outboundEventDAL;

            try
            {
                outboundEventDAL = new OutboundEventDAL();
                outboundEventDAL.Name = eventName;
                outboundEventDAL.StartAge = int.Parse(startAge);
                outboundEventDAL.EndAge = int.Parse(endAge);
                outboundEventDAL.BrandPre100Pipers = brandPre100Pipers;
                outboundEventDAL.BrandPre100Pipers8Y = brandPre100Pipers8Y;
                outboundEventDAL.BrandPreAbsolut = brandPreAbsolut;
                outboundEventDAL.BrandPreBaileys = brandPreBaileys;
                outboundEventDAL.BrandPreBallentine = brandPreBallentine;
                outboundEventDAL.BrandPreBenmore = brandPreBenmore;
                outboundEventDAL.BrandPreBlend = brandPreBlend;
                outboundEventDAL.BrandPreChivas = brandPreChivas;
                outboundEventDAL.BrandPreDewar = brandPreDewar;
                outboundEventDAL.BrandPreJwBlack = brandPreJwBlack;
                outboundEventDAL.BrandPreJwGold = brandPreJwGold;
                outboundEventDAL.BrandPreJwGreen = brandPreJwGreen;
                outboundEventDAL.BrandPreJwRed = brandPreJwRed;
                outboundEventDAL.BrandPreNotDrink = brandPreNotdrink;
                outboundEventDAL.BrandPreOther = brandPreOthers;
                outboundEventDAL.BrandPreSmirnoff = brandPreSmirnoff;
                outboundEventDAL.Active = active;
                outboundEventDAL.CreateBy = userIDLogin;
                outboundEventDAL.CreateDate = DateTime.Now;
                outboundEventDAL.UpdateBy = userIDLogin;
                outboundEventDAL.UpdateDate = DateTime.Now;
                outboundEventDAL.Insert();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SearchConsumerDataGroupTOGlobalTemp(int eventId,
        string senderName, int platformId, string brandPreferenceCondition,
        string gender, int ageFrom, int ageTo, int monthOFBirthDate,
        int provinceId, int lastTimeOpenEmail, int receiveEmail, string emailActive,
        string mobileActive, decimal openEmailRate, string dataGroupName)
        {
            OutboundEventDAL outboundEventDAL;

            try
            {
                outboundEventDAL = new OutboundEventDAL();
                return outboundEventDAL.SearchConsumerDataGroupTOGlobalTemp(eventId, senderName, platformId, brandPreferenceCondition, gender, ageFrom, ageTo, monthOFBirthDate, provinceId, lastTimeOpenEmail, receiveEmail, emailActive, mobileActive, openEmailRate, dataGroupName);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool InsertOutboundEvent(DataTable dtOutboundEventMaster, DataTable dtOutboundEvent, DataTable dtEventPlatform, DataTable dtEventOutlet,
                                        DataTable dtEventProvince, DataTable dtEventVisitCondition, string Mode, DataTable dtEventDiageoVip/*by O_Lek*/, DataTable dtEventOutletVip/*by O_Lek*/)
        {
            OutboundEventMasterDAL dalOutboundEventMaster = new OutboundEventMasterDAL();
            OutboundEventDAL dalOutboundEvent = new OutboundEventDAL();
            EventPlatformDAL dalEventPlatform = new EventPlatformDAL();
            EventOutletDAL dalEventOutlet = new EventOutletDAL();
            EventProvinceDAL dalEventProvince = new EventProvinceDAL();
            EventVisitConditionDAL dalEventVisitCondition = new EventVisitConditionDAL();
            /*by O_Lek*/
            EventDiageoVipDAL dalEventDiageoVip = new EventDiageoVipDAL();
            EventOutletVipDAL dalEventOutletVip = new EventOutletVipDAL();
            /*by O_Lek*/
            ConnectionProvider conProv = new ConnectionProvider();

            string TransactionName = "InsertOutboundEvent";
            DataRow dr;
            try
            {
                conProv.OpenConnection();
                conProv.BeginTransaction(TransactionName);

                dalOutboundEventMaster.MainConnectionProvider = conProv;
                //--1 Event for 1 Insert OutboundEventMaster
                dr = dtOutboundEventMaster.Rows[0];
                dalOutboundEventMaster.EventName = dr["EventName"].ToString();
                dalOutboundEventMaster.EventDateFrom = DateTime.Parse(dr["EventDateFrom"].ToString());
                dalOutboundEventMaster.EventDateTo = DateTime.Parse(dr["EventDateTo"].ToString());
                dalOutboundEventMaster.ExcludeBlockList = dr["ExcludeBlockList"].ToString();

                if (Mode.ToLower().Equals("add"))
                {
                    dalOutboundEventMaster.CreateBy = dr["CreateBy"].ToString();
                    dalOutboundEventMaster.CreateDate = DateTime.Parse(dr["CreateDate"].ToString());
                }
                else
                {
                    dalOutboundEventMaster.UpdateBy = dr["UpdateBy"].ToString();
                    dalOutboundEventMaster.UpdateDate = DateTime.Parse(dr["UpdateDate"].ToString());
                }

                dalOutboundEventMaster.Flag = "Event";

                dalOutboundEventMaster.Insert();
                EventID = int.Parse(dalOutboundEventMaster.EventId.ToString());
                //--End of OutboundEventMaster

                dalOutboundEvent.MainConnectionProvider = conProv;
                for (int i = 0; i < dtOutboundEvent.Rows.Count; i++)
                {
                    dr = dtOutboundEvent.Rows[i];
                    dalOutboundEvent.EventId = EventID;
                    dalOutboundEvent.DataGroup = dr["DataGroup"].ToString();
                    dalOutboundEvent.Name = dr["EventName"].ToString();
                    dalOutboundEvent.SenderName = dr["SenderName"].ToString();
                    dalOutboundEvent.BrandId = int.Parse(dr["BrandID"].ToString());
                    dalOutboundEvent.BrandPre100Pipers = dr["brandPre100Pipers"].ToString();
                    dalOutboundEvent.BrandPre100Pipers8Y = dr["brandPre100Pipers8Y"].ToString();
                    dalOutboundEvent.BrandPreAbsolut = dr["brandPreAbsolut"].ToString();
                    dalOutboundEvent.BrandPreBaileys = dr["brandPreBaileys"].ToString();
                    dalOutboundEvent.BrandPreBallentine = dr["brandPreBallentine"].ToString();
                    dalOutboundEvent.BrandPreBenmore = dr["brandPreBenmore"].ToString();
                    dalOutboundEvent.BrandPreBlend = dr["brandPreBlend"].ToString();
                    dalOutboundEvent.BrandPreChivas = dr["brandPreChivas"].ToString();
                    dalOutboundEvent.BrandPreDewar = dr["brandPreDewar"].ToString();
                    dalOutboundEvent.BrandPreHennessy = dr["brandPreHennessy"].ToString();
                    dalOutboundEvent.BrandPreJwBlack = dr["brandPreJwBlack"].ToString();
                    dalOutboundEvent.BrandPreJwGold = dr["brandPreJwGold"].ToString();
                    dalOutboundEvent.BrandPreJwGreen = dr["brandPreJwGreen"].ToString();
                    dalOutboundEvent.BrandPreJwRed = dr["brandPreJwRed"].ToString();
                    dalOutboundEvent.BrandPreSmirnoff = dr["brandPreSmirnoff"].ToString();
                    dalOutboundEvent.BrandPreOther = dr["brandPreOther"].ToString();
                    dalOutboundEvent.BrandPreNotDrink = dr["brandPreNotdrink"].ToString();
                    dalOutboundEvent.Gender = dr["Gender"].ToString();
                    dalOutboundEvent.ValidOptInDiageo = dr["ValidDiageo"].ToString();
                    dalOutboundEvent.ValidOptInOutlet = dr["ValidOutlet"].ToString();

                    if (dr["AgeFrom"].ToString() != "")
                        dalOutboundEvent.StartAge = int.Parse(dr["AgeFrom"].ToString());
                    if (dr["AgeTo"].ToString() != "")
                        dalOutboundEvent.EndAge = int.Parse(dr["AgeTo"].ToString());

                    dalOutboundEvent.MonthJan = dr["MonthJan"].ToString();
                    dalOutboundEvent.MonthFeb = dr["MonthFeb"].ToString();
                    dalOutboundEvent.MonthMar = dr["MonthMar"].ToString();
                    dalOutboundEvent.MonthApr = dr["MonthApr"].ToString();
                    dalOutboundEvent.MonthMay = dr["MonthMay"].ToString();
                    dalOutboundEvent.MonthJun = dr["MonthJun"].ToString();
                    dalOutboundEvent.MonthJul = dr["MonthJul"].ToString();
                    dalOutboundEvent.MonthAug = dr["MonthAug"].ToString();
                    dalOutboundEvent.MonthSep = dr["MonthSep"].ToString();
                    dalOutboundEvent.MonthOct = dr["MonthOct"].ToString();
                    dalOutboundEvent.MonthNov = dr["MonthNov"].ToString();
                    dalOutboundEvent.MonthDec = dr["MonthDec"].ToString();

                    if (dr["OpenEmail"].ToString() != "")
                        dalOutboundEvent.OpenEmailInLastTime = int.Parse(dr["OpenEmail"].ToString());
                    if (dr["ReceiveEmail"].ToString() != "")
                        dalOutboundEvent.ReceiveEmailInLessTime = int.Parse(dr["ReceiveEmail"].ToString());

                    dalOutboundEvent.EmailActive = dr["EmailActive"].ToString();
                    dalOutboundEvent.MobileActive = dr["MobileActive"].ToString();

                    if (dr["EmailRate"].ToString() != "")
                        dalOutboundEvent.EmailRate = decimal.Parse(dr["EmailRate"].ToString());

                    dalOutboundEvent.IncludeCallcenterBounce = dr["IncludeCallCenter"].ToString();
                    dalOutboundEvent.IncludeEDMBounce = dr["IncludeEDM"].ToString();
                    dalOutboundEvent.IncludeSMSBounce = dr["IncludeSMS"].ToString();

                    if (Mode.ToLower().Equals("add"))
                    {
                        dalOutboundEvent.CreateBy = dr["CreateBy"].ToString();
                        dalOutboundEvent.CreateDate = DateTime.Parse(dr["CreateDate"].ToString());
                    }
                    else
                    {
                        dalOutboundEvent.UpdateBy = dr["UpdateBy"].ToString();
                        dalOutboundEvent.UpdateDate = DateTime.Parse(dr["UpdateDate"].ToString());
                    }

                    dalOutboundEvent.Callcenterflag = "1";
                    dalOutboundEvent.Edmflag = "1";
                    dalOutboundEvent.Smsflag = "1";
                    dalOutboundEvent.Bdayflag = "F";
                    dalOutboundEvent.Active = "T";

                    dalOutboundEvent.Insert();
                }

                dalEventPlatform.MainConnectionProvider = conProv;
                for (int i = 0; i < dtEventPlatform.Rows.Count; i++)
                {
                    dr = dtEventPlatform.Rows[i];
                    dalEventPlatform.PlatformId = int.Parse(dr["PlatformID"].ToString());
                    dalEventPlatform.EventId = EventID;
                    dalEventPlatform.Datagroup = dr["DataGroup"].ToString();

                    if (Mode.ToLower().Equals("add"))
                    {
                        dalEventPlatform.CreateBy = dr["CreateBy"].ToString();
                        dalEventPlatform.CreateDate = DateTime.Parse(dr["CreateDate"].ToString());
                    }
                    else
                    {
                        dalEventPlatform.UpdateBy = dr["UpdateBy"].ToString();
                        dalEventPlatform.UpdateDate = DateTime.Parse(dr["UpdateDate"].ToString());
                    }

                    dalEventPlatform.Insert();
                }

                dalEventOutlet.MainConnectionProvider = conProv;
                for (int i = 0; i < dtEventOutlet.Rows.Count; i++)
                {
                    dr = dtEventOutlet.Rows[i];
                    dalEventOutlet.OutletId = int.Parse(dr["OutletID"].ToString());
                    dalEventOutlet.EventId = EventID;
                    dalEventOutlet.Datagroup = dr["DataGroup"].ToString();
                    if (Mode.ToLower().Equals("add"))
                    {
                        dalEventOutlet.CreateBy = dr["CreateBy"].ToString();
                        dalEventOutlet.CreateDate = DateTime.Parse(dr["CreateDate"].ToString());
                    }
                    else
                    {
                        dalEventOutlet.UpdateBy = dr["UpdateBy"].ToString();
                        dalEventOutlet.UpdateDate = DateTime.Parse(dr["UpdateDate"].ToString());
                    }

                    dalEventOutlet.Insert();
                }

                dalEventProvince.MainConnectionProvider = conProv;
                for (int i = 0; i < dtEventProvince.Rows.Count; i++)
                {
                    dr = dtEventProvince.Rows[i];
                    dalEventProvince.ProvinceId = int.Parse(dr["ProvinceID"].ToString());
                    dalEventProvince.EventId = EventID;
                    dalEventProvince.Datagroup = dr["DataGroup"].ToString();
                    if (Mode.ToLower().Equals("add"))
                    {
                        dalEventProvince.CreateBy = dr["CreateBy"].ToString();
                        dalEventProvince.CreateDate = DateTime.Parse(dr["CreateDate"].ToString());
                    }
                    else
                    {
                        dalEventProvince.UpdateBy = dr["UpdateBy"].ToString();
                        dalEventProvince.UpdateDate = DateTime.Parse(dr["UpdateDate"].ToString());
                    }

                    dalEventProvince.Insert();
                }

                dalEventVisitCondition.MainConnectionProvider = conProv;
                for (int i = 0; i < dtEventVisitCondition.Rows.Count; i++)
                {
                    dr = dtEventVisitCondition.Rows[i];
                    dalEventVisitCondition.EventId = EventID;
                    dalEventVisitCondition.DataGroup = dr["DataGroup"].ToString();
                    dalEventVisitCondition.TypeId = int.Parse(dr["TypeID"].ToString());
                    dalEventVisitCondition.OutletId = int.Parse(dr["OutletID"].ToString());

                    if (dr["DateFrom"].ToString() != "")
                        dalEventVisitCondition.DateFrom = DateTime.Parse(dr["DateFrom"].ToString());
                    if (dr["DateTo"].ToString() != "")
                        dalEventVisitCondition.DateTo = DateTime.Parse(dr["DateTo"].ToString());
                    if (dr["LastActivity"].ToString() != "")
                        dalEventVisitCondition.LastActivityNotOver = int.Parse(dr["LastActivity"].ToString());
                    if (dr["Visit"].ToString() != "")
                        dalEventVisitCondition.VisitFrequency = int.Parse(dr["Visit"].ToString());

                    if (Mode.ToLower().Equals("add"))
                    {
                        dalEventVisitCondition.CreateBy = dr["CreateBy"].ToString();
                        dalEventVisitCondition.CreateDate = DateTime.Parse(dr["CreateDate"].ToString());
                    }
                    else
                    {
                        dalEventVisitCondition.UpdateBy = dr["UpdateBy"].ToString();
                        dalEventVisitCondition.UpdateDate = DateTime.Parse(dr["UpdateDate"].ToString());
                    }

                    dalEventVisitCondition.Insert();
                }
                /*by O_Lek*/
                
                User userVip= new User();

                if (dtEventDiageoVip != null)
                {
                    if (dtEventDiageoVip.Rows.Count > 0)
                    {
                        dalEventDiageoVip.MainConnectionProvider = conProv;
                        foreach (DataRow drDiageoVip in dtEventDiageoVip.Rows)
                        {
                            dalEventDiageoVip.EventId = EventID;
                            dalEventDiageoVip.FirstName = drDiageoVip["First Name"].ToString();
                            dalEventDiageoVip.LastName = drDiageoVip["Last Name"].ToString();
                            dalEventDiageoVip.Mobile = drDiageoVip["MOBILE NUMBER"].ToString();
                            dalEventDiageoVip.Email = drDiageoVip["EMAIL"].ToString();
                            dalEventDiageoVip.CreateBy = userVip.Username;
                            DataTable dtCheck = dalEventDiageoVip.SearchFirstAndLastName();
                            if (dtCheck.Rows.Count == 0)
                            {
                                dalEventDiageoVip.InsertDiageoVipDAL();
                            }
                        }
                    }
                }
                if (dtEventOutletVip != null)
                {
                    if (dtEventOutletVip.Rows.Count > 0)
                    {
                        dalEventOutletVip.MainConnectionProvider = conProv;
                        foreach (DataRow drEventOutletVip in dtEventOutletVip.Rows)
                        {
                            dalEventOutletVip.EventId = EventID;
                            dalEventOutletVip.FirstName = drEventOutletVip["First Name"].ToString();
                            dalEventOutletVip.LastName = drEventOutletVip["Last Name"].ToString();
                            dalEventOutletVip.Mobile = drEventOutletVip["MOBILE NUMBER"].ToString();
                            dalEventOutletVip.Email = drEventOutletVip["EMAIL"].ToString();
                            dalEventOutletVip.CreateBy = userVip.Username;
                            DataTable dtCheck = dalEventOutletVip.SearchFirstAndLastName();
                            if (dtCheck.Rows.Count == 0)
                            {
                                dalEventOutletVip.InsertOutletVipDAL();
                            }
                        }
                    }
                }
                /*by O_Lek*/
                conProv.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                conProv.RollbackTransaction(TransactionName);
                throw new Exception("InsertOutboundEvent :: " + ex.Message);
            }
            finally
            {
                conProv.CloseConnection(false);
                conProv.Dispose();
            }
        }

        public bool UpdateOutboundEvent(DataTable dtOutboundEventMaster, DataTable dtOutboundEvent, DataTable dtEventPlatform, DataTable dtEventOutlet,
                                        DataTable dtEventProvince, DataTable dtEventVisitCondition, string Mode, DataTable dtEventDiageoVip/*by O_Lek*/, DataTable dtEventOutletVip/*by O_Lek*/)
        {
            OutboundEventMasterDAL dalOutboundEventMaster = new OutboundEventMasterDAL();
            OutboundEventDAL dalOutboundEvent = new OutboundEventDAL();
            EventPlatformDAL dalEventPlatform = new EventPlatformDAL();
            EventOutletDAL dalEventOutlet = new EventOutletDAL();
            EventProvinceDAL dalEventProvince = new EventProvinceDAL();
            EventVisitConditionDAL dalEventVisitCondition = new EventVisitConditionDAL();
            /*by O_Lek*/
            EventDiageoVipDAL dalEventDiageoVip = new EventDiageoVipDAL();
            EventOutletVipDAL dalEventOutletVip = new EventOutletVipDAL();
            /*by O_Lek*/
            ConnectionProvider conProv = new ConnectionProvider();

            string TransactionName = "UpdateOutboundEvent";
            DataRow dr;
            try
            {
                conProv.OpenConnection();
                conProv.BeginTransaction(TransactionName);

                dalOutboundEventMaster.MainConnectionProvider = conProv;
                //--1 Event for 1 Insert OutboundEventMaster
                dr = dtOutboundEventMaster.Rows[0];
                dalOutboundEventMaster.EventName = dr["EventName"].ToString();
                dalOutboundEventMaster.EventDateFrom = DateTime.Parse(dr["EventDateFrom"].ToString());
                dalOutboundEventMaster.EventDateTo = DateTime.Parse(dr["EventDateTo"].ToString());
                dalOutboundEventMaster.ExcludeBlockList = dr["ExcludeBlockList"].ToString();
                dalOutboundEventMaster.UpdateBy = dr["UpdateBy"].ToString();
                dalOutboundEventMaster.UpdateDate = DateTime.Parse(dr["UpdateDate"].ToString());
                dalOutboundEventMaster.Flag = "Event";
                dalOutboundEventMaster.Update();
                //--End of OutboundEventMaster

                dalOutboundEvent.MainConnectionProvider = conProv;

                dalOutboundEvent.EventId = EventID;
                dalOutboundEvent.DeleteByEventId();

                for (int i = 0; i < dtOutboundEvent.Rows.Count; i++)
                {
                    dr = dtOutboundEvent.Rows[i];
                    dalOutboundEvent.EventId = EventID;
                    dalOutboundEvent.DataGroup = dr["DataGroup"].ToString();
                    dalOutboundEvent.Name = dr["EventName"].ToString();
                    dalOutboundEvent.SenderName = dr["SenderName"].ToString();

                    if (dr["OpenEmail"].ToString() != "")
                        dalOutboundEvent.BrandId = int.Parse(dr["BrandID"].ToString());

                    dalOutboundEvent.BrandPre100Pipers = dr["brandPre100Pipers"].ToString();
                    dalOutboundEvent.BrandPre100Pipers8Y = dr["brandPre100Pipers8Y"].ToString();
                    dalOutboundEvent.BrandPreAbsolut = dr["brandPreAbsolut"].ToString();
                    dalOutboundEvent.BrandPreBaileys = dr["brandPreBaileys"].ToString();
                    dalOutboundEvent.BrandPreBallentine = dr["brandPreBallentine"].ToString();
                    dalOutboundEvent.BrandPreBenmore = dr["brandPreBenmore"].ToString();
                    dalOutboundEvent.BrandPreBlend = dr["brandPreBlend"].ToString();
                    dalOutboundEvent.BrandPreChivas = dr["brandPreChivas"].ToString();
                    dalOutboundEvent.BrandPreDewar = dr["brandPreDewar"].ToString();
                    dalOutboundEvent.BrandPreHennessy = dr["brandPreHennessy"].ToString();
                    dalOutboundEvent.BrandPreJwBlack = dr["brandPreJwBlack"].ToString();
                    dalOutboundEvent.BrandPreJwGold = dr["brandPreJwGold"].ToString();
                    dalOutboundEvent.BrandPreJwGreen = dr["brandPreJwGreen"].ToString();
                    dalOutboundEvent.BrandPreJwRed = dr["brandPreJwRed"].ToString();
                    dalOutboundEvent.BrandPreSmirnoff = dr["brandPreSmirnoff"].ToString();
                    dalOutboundEvent.BrandPreOther = dr["brandPreOther"].ToString();
                    dalOutboundEvent.BrandPreNotDrink = dr["brandPreNotdrink"].ToString();
                    dalOutboundEvent.Gender = dr["Gender"].ToString();
                    dalOutboundEvent.ValidOptInDiageo = dr["ValidDiageo"].ToString();
                    dalOutboundEvent.ValidOptInOutlet = dr["ValidOutlet"].ToString();

                    if (dr["AgeFrom"].ToString() != "")
                        dalOutboundEvent.StartAge = int.Parse(dr["AgeFrom"].ToString());
                    if (dr["AgeTo"].ToString() != "")
                        dalOutboundEvent.EndAge = int.Parse(dr["AgeTo"].ToString());

                    dalOutboundEvent.MonthJan = dr["MonthJan"].ToString();
                    dalOutboundEvent.MonthFeb = dr["MonthFeb"].ToString();
                    dalOutboundEvent.MonthMar = dr["MonthMar"].ToString();
                    dalOutboundEvent.MonthApr = dr["MonthApr"].ToString();
                    dalOutboundEvent.MonthMay = dr["MonthMay"].ToString();
                    dalOutboundEvent.MonthJun = dr["MonthJun"].ToString();
                    dalOutboundEvent.MonthJul = dr["MonthJul"].ToString();
                    dalOutboundEvent.MonthAug = dr["MonthAug"].ToString();
                    dalOutboundEvent.MonthSep = dr["MonthSep"].ToString();
                    dalOutboundEvent.MonthOct = dr["MonthOct"].ToString();
                    dalOutboundEvent.MonthNov = dr["MonthNov"].ToString();
                    dalOutboundEvent.MonthDec = dr["MonthDec"].ToString();

                    if (dr["OpenEmail"].ToString() != "")
                        dalOutboundEvent.OpenEmailInLastTime = int.Parse(dr["OpenEmail"].ToString());
                    if (dr["ReceiveEmail"].ToString() != "")
                        dalOutboundEvent.ReceiveEmailInLessTime = int.Parse(dr["ReceiveEmail"].ToString());

                    dalOutboundEvent.EmailActive = dr["EmailActive"].ToString();
                    dalOutboundEvent.MobileActive = dr["MobileActive"].ToString();

                    if (dr["EmailRate"].ToString() != "")
                        dalOutboundEvent.EmailRate = decimal.Parse(dr["EmailRate"].ToString());

                    dalOutboundEvent.IncludeCallcenterBounce = dr["IncludeCallCenter"].ToString();
                    dalOutboundEvent.IncludeEDMBounce = dr["IncludeEDM"].ToString();
                    dalOutboundEvent.IncludeSMSBounce = dr["IncludeSMS"].ToString();
                    dalOutboundEvent.Callcenterflag = "1";
                    dalOutboundEvent.Edmflag = "1";
                    dalOutboundEvent.Smsflag = "1";
                    dalOutboundEvent.Bdayflag = "F";
                    dalOutboundEvent.Active = "T";
                    dalOutboundEvent.UpdateBy = dr["UpdateBy"].ToString();
                    dalOutboundEvent.UpdateDate = DateTime.Parse(dr["UpdateDate"].ToString());
                    dalOutboundEvent.Insert();
                }

                dalEventPlatform.MainConnectionProvider = conProv;


                dalEventPlatform.EventId = EventID;
                dalEventPlatform.DeleteByEventId();

                for (int i = 0; i < dtEventPlatform.Rows.Count; i++)
                {
                    dr = dtEventPlatform.Rows[i];
                    dalEventPlatform.PlatformId = int.Parse(dr["PlatformID"].ToString());
                    dalEventPlatform.EventId = EventID;
                    dalEventPlatform.Datagroup = dr["DataGroup"].ToString();

                    if (Mode.ToLower().Equals("add"))
                    {
                        dalEventPlatform.CreateBy = dr["CreateBy"].ToString();
                        dalEventPlatform.CreateDate = DateTime.Parse(dr["CreateDate"].ToString());
                    }
                    else
                    {
                        dalEventPlatform.UpdateBy = dr["UpdateBy"].ToString();
                        dalEventPlatform.UpdateDate = DateTime.Parse(dr["UpdateDate"].ToString());
                    }

                    dalEventPlatform.Insert();
                }

                dalEventOutlet.MainConnectionProvider = conProv;

                dalEventOutlet.EventId = EventID;
                dalEventOutlet.DeleteByEventId();

                for (int i = 0; i < dtEventOutlet.Rows.Count; i++)
                {
                    dr = dtEventOutlet.Rows[i];
                    dalEventOutlet.OutletId = int.Parse(dr["OutletID"].ToString());
                    dalEventOutlet.EventId = EventID;
                    dalEventOutlet.Datagroup = dr["DataGroup"].ToString();
                    if (Mode.ToLower().Equals("add"))
                    {
                        dalEventOutlet.CreateBy = dr["CreateBy"].ToString();
                        dalEventOutlet.CreateDate = DateTime.Parse(dr["CreateDate"].ToString());
                    }
                    else
                    {
                        dalEventOutlet.UpdateBy = dr["UpdateBy"].ToString();
                        dalEventOutlet.UpdateDate = DateTime.Parse(dr["UpdateDate"].ToString());
                    }

                    dalEventOutlet.Insert();
                }

                dalEventProvince.MainConnectionProvider = conProv;

                dalEventProvince.EventId = EventID;
                dalEventProvince.DeleteByEventId();

                for (int i = 0; i < dtEventProvince.Rows.Count; i++)
                {
                    dr = dtEventProvince.Rows[i];
                    dalEventProvince.ProvinceId = int.Parse(dr["ProvinceID"].ToString());
                    dalEventProvince.EventId = EventID;
                    dalEventProvince.Datagroup = dr["DataGroup"].ToString();
                    if (Mode.ToLower().Equals("add"))
                    {
                        dalEventProvince.CreateBy = dr["CreateBy"].ToString();
                        dalEventProvince.CreateDate = DateTime.Parse(dr["CreateDate"].ToString());
                    }
                    else
                    {
                        dalEventProvince.UpdateBy = dr["UpdateBy"].ToString();
                        dalEventProvince.UpdateDate = DateTime.Parse(dr["UpdateDate"].ToString());
                    }

                    dalEventProvince.Insert();
                }

                dalEventVisitCondition.MainConnectionProvider = conProv;

                dalEventVisitCondition.EventId = EventID;
                dalEventVisitCondition.DeleteByEventId();

                for (int i = 0; i < dtEventVisitCondition.Rows.Count; i++)
                {
                    dr = dtEventVisitCondition.Rows[i];
                    dalEventVisitCondition.EventId = EventID;
                    dalEventVisitCondition.DataGroup = dr["DataGroup"].ToString();
                    dalEventVisitCondition.TypeId = int.Parse(dr["TypeID"].ToString());
                    dalEventVisitCondition.OutletId = int.Parse(dr["OutletID"].ToString());

                    if (dr["DateFrom"].ToString() != "")
                        dalEventVisitCondition.DateFrom = DateTime.Parse(dr["DateFrom"].ToString());
                    if (dr["DateTo"].ToString() != "")
                        dalEventVisitCondition.DateTo = DateTime.Parse(dr["DateTo"].ToString());
                    if (dr["LastActivity"].ToString() != "")
                        dalEventVisitCondition.LastActivityNotOver = int.Parse(dr["LastActivity"].ToString());
                    if (dr["Visit"].ToString() != "")
                        dalEventVisitCondition.VisitFrequency = int.Parse(dr["Visit"].ToString());

                    if (Mode.ToLower().Equals("add"))
                    {
                        dalEventVisitCondition.CreateBy = dr["CreateBy"].ToString();
                        dalEventVisitCondition.CreateDate = DateTime.Parse(dr["CreateDate"].ToString());
                    }
                    else
                    {
                        dalEventVisitCondition.UpdateBy = dr["UpdateBy"].ToString();
                        dalEventVisitCondition.UpdateDate = DateTime.Parse(dr["UpdateDate"].ToString());
                    }

                    dalEventVisitCondition.Insert();
                }
                /*by O_Lek*/

                User userVip = new User();

                if (dtEventDiageoVip != null)
                {
                    if (dtEventDiageoVip.Rows.Count > 0)
                    {
                        dalEventDiageoVip.MainConnectionProvider = conProv;

                        dalEventDiageoVip.EventId = EventID;
                        dalEventDiageoVip.DeleteByEventId();

                        foreach (DataRow drDiageoVip in dtEventDiageoVip.Rows)
                        {
                            dalEventDiageoVip.EventId = EventID;
                            dalEventDiageoVip.FirstName = drDiageoVip["First Name"].ToString();
                            dalEventDiageoVip.LastName = drDiageoVip["Last Name"].ToString();
                            dalEventDiageoVip.Mobile = drDiageoVip["MOBILE NUMBER"].ToString();
                            dalEventDiageoVip.Email = drDiageoVip["EMAIL"].ToString();
                            dalEventDiageoVip.CreateBy = userVip.Username;
                            DataTable dtCheck = dalEventDiageoVip.SearchFirstAndLastName();
                            if (dtCheck.Rows.Count == 0)
                            {
                                dalEventDiageoVip.InsertDiageoVipDAL();
                            }
                        }
                    }
                }
                if (dtEventOutletVip != null)
                {
                    if (dtEventOutletVip.Rows.Count > 0)
                    {
                        dalEventOutletVip.MainConnectionProvider = conProv;

                        dalEventOutletVip.EventId = EventID;
                        dalEventOutletVip.DeleteByEventId();

                        foreach (DataRow drEventOutletVip in dtEventOutletVip.Rows)
                        {
                            dalEventOutletVip.EventId = EventID;
                            dalEventOutletVip.FirstName = drEventOutletVip["First Name"].ToString();
                            dalEventOutletVip.LastName = drEventOutletVip["Last Name"].ToString();
                            dalEventOutletVip.Mobile = drEventOutletVip["MOBILE NUMBER"].ToString();
                            dalEventOutletVip.Email = drEventOutletVip["EMAIL"].ToString();
                            dalEventOutletVip.CreateBy = userVip.Username;
                            DataTable dtCheck = dalEventOutletVip.SearchFirstAndLastName();
                            if (dtCheck.Rows.Count == 0)
                            {
                                dalEventOutletVip.InsertOutletVipDAL();
                            }
                        }
                    }
                }
                /*by O_Lek*/
                conProv.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                conProv.RollbackTransaction(TransactionName);
                throw new Exception("InsertOutboundEvent :: " + ex.Message);
            }
            finally
            {
                conProv.CloseConnection(false);
                conProv.Dispose();
            }
        }

        public bool InsertOutboundEventBirthDay(DataTable dtEventMaster, DataTable dtEvent, DataTable dtPlatform, DataTable dtOutlet, DataTable dtProvince, string userIDLogin)
        {
            OutboundEventMasterDAL outboundEventMasterDAL;
            OutboundEventDAL outboundEventDAL;
            EventPlatformDAL eventPlatformDAL;
            EventOutletDAL eventOutletDAL;
            EventProvinceDAL eventProvinceDAL;

            ConnectionProvider conp = new ConnectionProvider();

            DataRow Row;

            try
            {
                outboundEventMasterDAL = new OutboundEventMasterDAL();
                outboundEventDAL = new OutboundEventDAL();
                eventPlatformDAL = new EventPlatformDAL();
                eventOutletDAL = new EventOutletDAL();
                eventProvinceDAL = new EventProvinceDAL();

                conp.OpenConnection();
                conp.BeginTransaction("OutboundEventBirthDay");

                outboundEventMasterDAL.MainConnectionProvider = conp;
                outboundEventDAL.MainConnectionProvider = conp;
                eventPlatformDAL.MainConnectionProvider = conp;
                eventOutletDAL.MainConnectionProvider = conp;
                eventProvinceDAL.MainConnectionProvider = conp;

                for (int i = 0; i < dtEventMaster.Rows.Count; i++)
                {
                    Row = dtEventMaster.Rows[i];
                    outboundEventMasterDAL.EventName = Row["EventName"].ToString();
                    outboundEventMasterDAL.ExcludeBlockList = Row["ExcludeBlockList"].ToString();
                    outboundEventMasterDAL.CreateDate = DateTime.Now;
                    outboundEventMasterDAL.CreateBy = userIDLogin;
                    outboundEventMasterDAL.UpdateDate = DateTime.Now;
                    outboundEventMasterDAL.UpdateBy = userIDLogin;
                    outboundEventMasterDAL.Flag = "BDAY";
                    outboundEventMasterDAL.InsertOutboundEventMaster();
                    EventID = int.Parse(outboundEventMasterDAL.EventId.ToString());
                }

                for (int i = 0; i < dtEvent.Rows.Count; i++)
                {
                    Row = dtEvent.Rows[i];
                    outboundEventDAL.EventId = EventID;
                    outboundEventDAL.DataGroup = "1";
                    outboundEventDAL.Name = Row["Name"].ToString();
                    outboundEventDAL.BrandPreJwGold = Row["BrandPreJwGold"].ToString();
                    outboundEventDAL.BrandPreJwBlack = Row["BrandPreJwBlack"].ToString();
                    outboundEventDAL.BrandPreSmirnoff = Row["BrandPreSmirnoff"].ToString();
                    outboundEventDAL.BrandPreJwRed = Row["BrandPreJwRed"].ToString();
                    outboundEventDAL.BrandPreJwGreen = Row["BrandPreJwGreen"].ToString();
                    outboundEventDAL.BrandPreBenmore = Row["BrandPreBenmore"].ToString();
                    outboundEventDAL.BrandPreBaileys = Row["BrandPreBaileys"].ToString();
                    outboundEventDAL.BrandPreAbsolut = Row["BrandPreAbsolut"].ToString();
                    outboundEventDAL.BrandPreBallentine = Row["BrandPreBallentine"].ToString();
                    outboundEventDAL.BrandPreBlend = Row["BrandPreBlend"].ToString();
                    outboundEventDAL.BrandPreChivas = Row["BrandPreChivas"].ToString();
                    outboundEventDAL.BrandPreDewar = Row["BrandPreDewar"].ToString();
                    outboundEventDAL.BrandPre100Pipers = Row["BrandPre100Pipers"].ToString();
                    outboundEventDAL.BrandPre100Pipers8Y = Row["BrandPre100Pipers8Y"].ToString();
                    outboundEventDAL.BrandPreHennessy = Row["BrandPreHennessy"].ToString();
                    outboundEventDAL.BrandPreOther = Row["BrandPreOther"].ToString();
                    outboundEventDAL.BrandPreNotDrink = Row["BrandPreNotDrink"].ToString();
                    outboundEventDAL.Gender = Row["Gender"].ToString();
                    outboundEventDAL.ValidOptInDiageo = Row["ValidOptInDiageo"].ToString();
                    outboundEventDAL.ValidOptInOutlet = Row["ValidOptInOutlet"].ToString();
                    outboundEventDAL.StartAge = int.Parse(Row["StartAge"].ToString());
                    outboundEventDAL.EndAge = int.Parse(Row["EndAge"].ToString());
                    outboundEventDAL.EmailActive = Row["EmailActive"].ToString();
                    outboundEventDAL.MobileActive = Row["MobileActive"].ToString();
                    outboundEventDAL.Active = Row["Active"].ToString();
                    outboundEventDAL.IncludeSMSBounce = Row["IncludeSMSBounce"].ToString();
                    outboundEventDAL.IncludeEDMBounce = Row["IncludeEDMBounce"].ToString();
                    outboundEventDAL.IncludeCallcenterBounce = Row["IncludeCallcenterBounce"].ToString();
                    outboundEventDAL.Smsflag = "1";
                    outboundEventDAL.Edmflag = "1";
                    outboundEventDAL.Callcenterflag = "1";
                    outboundEventDAL.CreateDate = DateTime.Now;
                    outboundEventDAL.CreateBy = userIDLogin;
                    outboundEventDAL.UpdateDate = DateTime.Now;
                    outboundEventDAL.UpdateBy = userIDLogin;
                    outboundEventDAL.Bdayflag = "T";
                    outboundEventDAL.InsertOutboundEvent();
                }

                for (int i = 0; i < dtPlatform.Rows.Count; i++)
                {
                    Row = dtPlatform.Rows[i];
                    eventPlatformDAL.PlatformId = int.Parse(Row["PlatformId"].ToString());
                    eventPlatformDAL.EventId = EventID;
                    eventPlatformDAL.Datagroup = "1";
                    eventPlatformDAL.CreateDate = DateTime.Now;
                    eventPlatformDAL.CreateBy = userIDLogin;
                    eventPlatformDAL.UpdateDate = DateTime.Now;
                    eventPlatformDAL.UpdateBy = userIDLogin;
                    eventPlatformDAL.Flag = "BDAY";
                    eventPlatformDAL.Insert();
                }

                for (int i = 0; i < dtOutlet.Rows.Count; i++)
                {
                    Row = dtOutlet.Rows[i];
                    eventOutletDAL.OutletId = int.Parse(Row["OutletId"].ToString());
                    eventOutletDAL.EventId = EventID;
                    eventOutletDAL.Datagroup = "1";
                    eventOutletDAL.CreateDate = DateTime.Now;
                    eventOutletDAL.CreateBy = userIDLogin;
                    eventOutletDAL.UpdateDate = DateTime.Now;
                    eventOutletDAL.UpdateBy = userIDLogin;
                    eventOutletDAL.Flag = "BDAY";
                    eventOutletDAL.Insert();
                }

                for (int i = 0; i < dtProvince.Rows.Count; i++)
                {
                    Row = dtProvince.Rows[i];
                    eventProvinceDAL.ProvinceId = int.Parse(Row["ProvinceId"].ToString());
                    eventProvinceDAL.EventId = EventID;
                    eventProvinceDAL.Datagroup = "1";
                    eventProvinceDAL.CreateDate = DateTime.Now;
                    eventProvinceDAL.CreateBy = userIDLogin;
                    eventProvinceDAL.UpdateDate = DateTime.Now;
                    eventProvinceDAL.UpdateBy = userIDLogin;
                    eventProvinceDAL.Flag = "BDAY";
                    eventProvinceDAL.Insert();
                }

                conp.CommitTransaction();

                return true;
            }
            catch (Exception ex)
            {
                conp.RollbackTransaction("OutboundEventBirthDay");
                throw new Exception(ex.Message);
            }
            finally
            {
                conp.CloseConnection(false);
                conp.Dispose();
            }
        }

        public bool UpDateOutboundEventBirthDay(DataTable dtEventMaster, DataTable dtEvent, DataTable dtPlatform, DataTable dtOutlet, DataTable dtProvince, string userIDLogin)
        {
            OutboundEventMasterDAL outboundEventMasterDAL;
            OutboundEventDAL outboundEventDAL;
            EventPlatformDAL eventPlatformDAL;
            EventOutletDAL eventOutletDAL;
            EventProvinceDAL eventProvinceDAL;

            ConnectionProvider conp = new ConnectionProvider();

            DataRow Row;

            try
            {
                outboundEventMasterDAL = new OutboundEventMasterDAL();
                outboundEventDAL = new OutboundEventDAL();
                eventPlatformDAL = new EventPlatformDAL();
                eventOutletDAL = new EventOutletDAL();
                eventProvinceDAL = new EventProvinceDAL();

                conp.OpenConnection();
                conp.BeginTransaction("OutboundEventBirthDay");

                outboundEventMasterDAL.MainConnectionProvider = conp;
                outboundEventDAL.MainConnectionProvider = conp;
                eventPlatformDAL.MainConnectionProvider = conp;
                eventOutletDAL.MainConnectionProvider = conp;
                eventProvinceDAL.MainConnectionProvider = conp;

                //Table OutboundEventMaster

                for (int i = 0; i < dtEventMaster.Rows.Count; i++)
                {
                    Row = dtEventMaster.Rows[i];
                    outboundEventMasterDAL.EventId = EventID;
                    outboundEventMasterDAL.EventName = Row["EventName"].ToString();
                    outboundEventMasterDAL.ExcludeBlockList = Row["ExcludeBlockList"].ToString();
                    outboundEventMasterDAL.CreateDate = DateTime.Now;
                    outboundEventMasterDAL.CreateBy = userIDLogin;
                    outboundEventMasterDAL.UpdateDate = DateTime.Now;
                    outboundEventMasterDAL.UpdateBy = userIDLogin;
                    outboundEventMasterDAL.UpdateOutboundEventMaster();
                }

                //Table OutboundEvent

                for (int i = 0; i < dtEvent.Rows.Count; i++)
                {
                    Row = dtEvent.Rows[i];
                    outboundEventDAL.EventId = EventID;
                    outboundEventDAL.DataGroup = "1";
                    outboundEventDAL.Name = Row["Name"].ToString();
                    outboundEventDAL.BrandPreJwGold = Row["BrandPreJwGold"].ToString();
                    outboundEventDAL.BrandPreJwBlack = Row["BrandPreJwBlack"].ToString();
                    outboundEventDAL.BrandPreSmirnoff = Row["BrandPreSmirnoff"].ToString();
                    outboundEventDAL.BrandPreJwRed = Row["BrandPreJwRed"].ToString();
                    outboundEventDAL.BrandPreJwGreen = Row["BrandPreJwGreen"].ToString();
                    outboundEventDAL.BrandPreBenmore = Row["BrandPreBenmore"].ToString();
                    outboundEventDAL.BrandPreBaileys = Row["BrandPreBaileys"].ToString();
                    outboundEventDAL.BrandPreAbsolut = Row["BrandPreAbsolut"].ToString();
                    outboundEventDAL.BrandPreBallentine = Row["BrandPreBallentine"].ToString();
                    outboundEventDAL.BrandPreBlend = Row["BrandPreBlend"].ToString();
                    outboundEventDAL.BrandPreChivas = Row["BrandPreChivas"].ToString();
                    outboundEventDAL.BrandPreDewar = Row["BrandPreDewar"].ToString();
                    outboundEventDAL.BrandPre100Pipers = Row["BrandPre100Pipers"].ToString();
                    outboundEventDAL.BrandPre100Pipers8Y = Row["BrandPre100Pipers8Y"].ToString();
                    outboundEventDAL.BrandPreHennessy = Row["BrandPreHennessy"].ToString();
                    outboundEventDAL.BrandPreOther = Row["BrandPreOther"].ToString();
                    outboundEventDAL.BrandPreNotDrink = Row["BrandPreNotDrink"].ToString();
                    outboundEventDAL.Gender = Row["Gender"].ToString();
                    outboundEventDAL.ValidOptInDiageo = Row["ValidOptInDiageo"].ToString();
                    outboundEventDAL.ValidOptInOutlet = Row["ValidOptInOutlet"].ToString();
                    outboundEventDAL.StartAge = int.Parse(Row["StartAge"].ToString());
                    outboundEventDAL.EndAge = int.Parse(Row["EndAge"].ToString());
                    outboundEventDAL.EmailActive = Row["EmailActive"].ToString();
                    outboundEventDAL.MobileActive = Row["MobileActive"].ToString();
                    outboundEventDAL.Active = Row["Active"].ToString();
                    outboundEventDAL.IncludeSMSBounce = Row["IncludeSMSBounce"].ToString();
                    outboundEventDAL.IncludeEDMBounce = Row["IncludeEDMBounce"].ToString();
                    outboundEventDAL.IncludeCallcenterBounce = Row["IncludeCallcenterBounce"].ToString();
                    outboundEventDAL.Smsflag = "1";
                    outboundEventDAL.Edmflag = "1";
                    outboundEventDAL.Callcenterflag = "1";
                    outboundEventDAL.CreateDate = DateTime.Now;
                    outboundEventDAL.CreateBy = userIDLogin;
                    outboundEventDAL.UpdateDate = DateTime.Now;
                    outboundEventDAL.UpdateBy = userIDLogin;
                    outboundEventDAL.Bdayflag = "T";
                    outboundEventDAL.UpdateOutboundEvent();
                }

                //Table EventPlatform

                eventPlatformDAL.EventId = EventID;
                eventPlatformDAL.DeleteByEventId();

                for (int i = 0; i < dtPlatform.Rows.Count; i++)
                {
                    Row = dtPlatform.Rows[i];

                    eventPlatformDAL.PlatformId = int.Parse(Row["PlatformId"].ToString());
                    eventPlatformDAL.EventId = EventID;
                    eventPlatformDAL.Datagroup = "1";
                    eventPlatformDAL.CreateDate = DateTime.Now;
                    eventPlatformDAL.CreateBy = userIDLogin;
                    eventPlatformDAL.UpdateDate = DateTime.Now;
                    eventPlatformDAL.UpdateBy = userIDLogin;
                    eventPlatformDAL.Flag = "BDAY";
                    eventPlatformDAL.Insert();
                }

                //Table EventOutlet

                eventOutletDAL.EventId = EventID;
                eventOutletDAL.DeleteByEventId();

                for (int i = 0; i < dtOutlet.Rows.Count; i++)
                {
                    Row = dtOutlet.Rows[i];

                    eventOutletDAL.OutletId = int.Parse(Row["OutletId"].ToString());
                    eventOutletDAL.EventId = EventID;
                    eventOutletDAL.Datagroup = "1";
                    eventOutletDAL.CreateDate = DateTime.Now;
                    eventOutletDAL.CreateBy = userIDLogin;
                    eventOutletDAL.UpdateDate = DateTime.Now;
                    eventOutletDAL.UpdateBy = userIDLogin;
                    eventOutletDAL.Flag = "BDAY";
                    eventOutletDAL.Insert();
                }

                //Table EventProvince

                eventProvinceDAL.EventId = EventID;
                eventProvinceDAL.DeleteByEventId();

                for (int i = 0; i < dtProvince.Rows.Count; i++)
                {
                    Row = dtProvince.Rows[i];

                    eventProvinceDAL.ProvinceId = int.Parse(Row["ProvinceId"].ToString());
                    eventProvinceDAL.EventId = EventID;
                    eventProvinceDAL.Datagroup = "1";
                    eventProvinceDAL.CreateDate = DateTime.Now;
                    eventProvinceDAL.CreateBy = userIDLogin;
                    eventProvinceDAL.UpdateDate = DateTime.Now;
                    eventProvinceDAL.UpdateBy = userIDLogin;
                    eventProvinceDAL.Flag = "BDAY";
                    eventProvinceDAL.Insert();
                }

                conp.CommitTransaction();

                return true;
            }
            catch (Exception ex)
            {
                conp.RollbackTransaction("OutboundEventBirthDay");
                throw new Exception(ex.Message);
            }
            finally
            {
                conp.CloseConnection(false);
                conp.Dispose();
            }
        }

        public bool InsertOutboundEventExtractData(DataTable dtOutboundEventMaster, DataTable dtOutboundEvent, DataTable dtEventPlatform, DataTable dtEventOutlet,
        DataTable dtEventProvince, DataTable dtEventVisitCondition, string Mode, DataTable dtEventDiageoVip/*by O_Lek*/, DataTable dtEventOutletVip/*by O_Lek*/,
        ref int eventId)
        {
            OutboundEventMasterExtractDataDAL dalOutboundEventMaster = new OutboundEventMasterExtractDataDAL();
            OutboundEventExtractDataDAL dalOutboundEvent = new OutboundEventExtractDataDAL();
            EventPlatformExtractDataDAL dalEventPlatform = new EventPlatformExtractDataDAL();
            EventOutletExtractDataDAL dalEventOutlet = new EventOutletExtractDataDAL();
            EventProvinceExtractDataDAL dalEventProvince = new EventProvinceExtractDataDAL();
            EventVisitConditionExtractDataDAL dalEventVisitCondition = new EventVisitConditionExtractDataDAL();
            /*by O_Lek*/
            EventDiageoVipExtractDataDAL dalEventDiageoVipExtractData = new EventDiageoVipExtractDataDAL();
            EventOutletVipExtractDataDAL dalEventOutletVipExtractData = new EventOutletVipExtractDataDAL();
            /*by O_Lek*/

            ConnectionProvider conProv = new ConnectionProvider();

            string TransactionName = "InsertOutboundEvent";
            DataRow dr;
            try
            {
                conProv.OpenConnection();
                conProv.BeginTransaction(TransactionName);

                dalOutboundEventMaster.MainConnectionProvider = conProv;
                //--1 Event for 1 Insert OutboundEventMaster
                dr = dtOutboundEventMaster.Rows[0];
                dalOutboundEventMaster.EventName = dr["EventName"].ToString();
                dalOutboundEventMaster.EventDateFrom = DateTime.Parse(dr["EventDateFrom"].ToString());
                dalOutboundEventMaster.EventDateTo = DateTime.Parse(dr["EventDateTo"].ToString());
                dalOutboundEventMaster.ExcludeBlockList = dr["ExcludeBlockList"].ToString();

                if (Mode.ToLower().Equals("add"))
                {
                    dalOutboundEventMaster.CreateBy = dr["CreateBy"].ToString();
                    dalOutboundEventMaster.CreateDate = DateTime.Now;
                }
                else
                {
                    dalOutboundEventMaster.UpdateBy = dr["UpdateBy"].ToString();
                    dalOutboundEventMaster.UpdateDate = DateTime.Now;
                }

                dalOutboundEventMaster.Flag = "Event";

                dalOutboundEventMaster.Insert();
                EventID = int.Parse(dalOutboundEventMaster.EventId.ToString());
                eventId = EventID;
                //--End of OutboundEventMaster

                dalOutboundEvent.MainConnectionProvider = conProv;
                for (int i = 0; i < dtOutboundEvent.Rows.Count; i++)
                {
                    dr = dtOutboundEvent.Rows[i];
                    dalOutboundEvent.EventId = EventID;
                    dalOutboundEvent.DataGroup = dr["DataGroup"].ToString();
                    dalOutboundEvent.Name = dr["EventName"].ToString();
                    dalOutboundEvent.SenderName = dr["SenderName"].ToString();
                    dalOutboundEvent.BrandId = int.Parse(dr["BrandID"].ToString());
                    dalOutboundEvent.BrandPre100Pipers = dr["brandPre100Pipers"].ToString();
                    dalOutboundEvent.BrandPre100Pipers8Y = dr["brandPre100Pipers8Y"].ToString();
                    dalOutboundEvent.BrandPreAbsolut = dr["brandPreAbsolut"].ToString();
                    dalOutboundEvent.BrandPreBaileys = dr["brandPreBaileys"].ToString();
                    dalOutboundEvent.BrandPreBallentine = dr["brandPreBallentine"].ToString();
                    dalOutboundEvent.BrandPreBenmore = dr["brandPreBenmore"].ToString();
                    dalOutboundEvent.BrandPreBlend = dr["brandPreBlend"].ToString();
                    dalOutboundEvent.BrandPreChivas = dr["brandPreChivas"].ToString();
                    dalOutboundEvent.BrandPreDewar = dr["brandPreDewar"].ToString();
                    dalOutboundEvent.BrandPreHennessy = dr["brandPreHennessy"].ToString();
                    dalOutboundEvent.BrandPreJwBlack = dr["brandPreJwBlack"].ToString();
                    dalOutboundEvent.BrandPreJwGold = dr["brandPreJwGold"].ToString();
                    dalOutboundEvent.BrandPreJwGreen = dr["brandPreJwGreen"].ToString();
                    dalOutboundEvent.BrandPreJwRed = dr["brandPreJwRed"].ToString();
                    dalOutboundEvent.BrandPreSmirnoff = dr["brandPreSmirnoff"].ToString();
                    dalOutboundEvent.BrandPreOther = dr["brandPreOther"].ToString();
                    dalOutboundEvent.BrandPreNotDrink = dr["brandPreNotdrink"].ToString();
                    dalOutboundEvent.Gender = dr["Gender"].ToString();
                    dalOutboundEvent.ValidOptInDiageo = dr["ValidDiageo"].ToString();
                    dalOutboundEvent.ValidOptInOutlet = dr["ValidOutlet"].ToString();

                    if (dr["AgeFrom"].ToString() != "")
                        dalOutboundEvent.StartAge = int.Parse(dr["AgeFrom"].ToString());
                    if (dr["AgeTo"].ToString() != "")
                        dalOutboundEvent.EndAge = int.Parse(dr["AgeTo"].ToString());

                    dalOutboundEvent.MonthJan = dr["MonthJan"].ToString();
                    dalOutboundEvent.MonthFeb = dr["MonthFeb"].ToString();
                    dalOutboundEvent.MonthMar = dr["MonthMar"].ToString();
                    dalOutboundEvent.MonthApr = dr["MonthApr"].ToString();
                    dalOutboundEvent.MonthMay = dr["MonthMay"].ToString();
                    dalOutboundEvent.MonthJun = dr["MonthJun"].ToString();
                    dalOutboundEvent.MonthJul = dr["MonthJul"].ToString();
                    dalOutboundEvent.MonthAug = dr["MonthAug"].ToString();
                    dalOutboundEvent.MonthSep = dr["MonthSep"].ToString();
                    dalOutboundEvent.MonthOct = dr["MonthOct"].ToString();
                    dalOutboundEvent.MonthNov = dr["MonthNov"].ToString();
                    dalOutboundEvent.MonthDec = dr["MonthDec"].ToString();

                    if (dr["OpenEmail"].ToString() != "")
                        dalOutboundEvent.OpenEmailInLastTime = int.Parse(dr["OpenEmail"].ToString());
                    if (dr["ReceiveEmail"].ToString() != "")
                        dalOutboundEvent.ReceiveEmailInLessTime = int.Parse(dr["ReceiveEmail"].ToString());

                    dalOutboundEvent.EmailActive = dr["EmailActive"].ToString();
                    dalOutboundEvent.MobileActive = dr["MobileActive"].ToString();

                    if (dr["EmailRate"].ToString() != "")
                        dalOutboundEvent.EmailRate = decimal.Parse(dr["EmailRate"].ToString());

                    dalOutboundEvent.IncludeCallcenterBounce = dr["IncludeCallCenter"].ToString();
                    dalOutboundEvent.IncludeEDMBounce = dr["IncludeEDM"].ToString();
                    dalOutboundEvent.IncludeSMSBounce = dr["IncludeSMS"].ToString();

                    if (Mode.ToLower().Equals("add"))
                    {
                        dalOutboundEvent.CreateBy = dr["CreateBy"].ToString();
                        dalOutboundEvent.CreateDate = DateTime.Now;
                    }
                    else
                    {
                        dalOutboundEvent.UpdateBy = dr["UpdateBy"].ToString();
                        dalOutboundEvent.UpdateDate = DateTime.Now;
                    }

                    dalOutboundEvent.Callcenterflag = "1";
                    dalOutboundEvent.Edmflag = "1";
                    dalOutboundEvent.Smsflag = "1";
                    dalOutboundEvent.Bdayflag = "F";
                    dalOutboundEvent.Active = "T";

                    dalOutboundEvent.Insert();
                }

                dalEventPlatform.MainConnectionProvider = conProv;
                for (int i = 0; i < dtEventPlatform.Rows.Count; i++)
                {
                    dr = dtEventPlatform.Rows[i];
                    dalEventPlatform.PlatformId = int.Parse(dr["PlatformID"].ToString());
                    dalEventPlatform.EventId = EventID;
                    dalEventPlatform.Datagroup = dr["DataGroup"].ToString();

                    if (Mode.ToLower().Equals("add"))
                    {
                        dalEventPlatform.CreateBy = dr["CreateBy"].ToString();
                        dalEventPlatform.CreateDate = DateTime.Now;
                    }
                    else
                    {
                        dalEventPlatform.UpdateBy = dr["UpdateBy"].ToString();
                        dalEventPlatform.UpdateDate = DateTime.Now;
                    }

                    dalEventPlatform.Insert();
                }

                dalEventOutlet.MainConnectionProvider = conProv;
                for (int i = 0; i < dtEventOutlet.Rows.Count; i++)
                {
                    dr = dtEventOutlet.Rows[i];
                    dalEventOutlet.OutletId = int.Parse(dr["OutletID"].ToString());
                    dalEventOutlet.EventId = EventID;
                    dalEventOutlet.Datagroup = dr["DataGroup"].ToString();
                    if (Mode.ToLower().Equals("add"))
                    {
                        dalEventOutlet.CreateBy = dr["CreateBy"].ToString();
                        dalEventOutlet.CreateDate = DateTime.Now;
                    }
                    else
                    {
                        dalEventOutlet.UpdateBy = dr["UpdateBy"].ToString();
                        dalEventOutlet.UpdateDate = DateTime.Now;
                    }

                    dalEventOutlet.Insert();
                }

                dalEventProvince.MainConnectionProvider = conProv;
                for (int i = 0; i < dtEventProvince.Rows.Count; i++)
                {
                    dr = dtEventProvince.Rows[i];
                    dalEventProvince.ProvinceId = int.Parse(dr["ProvinceID"].ToString());
                    dalEventProvince.EventId = EventID;
                    dalEventProvince.Datagroup = dr["DataGroup"].ToString();
                    if (Mode.ToLower().Equals("add"))
                    {
                        dalEventProvince.CreateBy = dr["CreateBy"].ToString();
                        dalEventProvince.CreateDate = DateTime.Now;
                    }
                    else
                    {
                        dalEventProvince.UpdateBy = dr["UpdateBy"].ToString();
                        dalEventProvince.UpdateDate = DateTime.Now;
                    }

                    dalEventProvince.Insert();
                }

                dalEventVisitCondition.MainConnectionProvider = conProv;
                for (int i = 0; i < dtEventVisitCondition.Rows.Count; i++)
                {
                    dr = dtEventVisitCondition.Rows[i];
                    dalEventVisitCondition.EventId = EventID;
                    dalEventVisitCondition.DataGroup = dr["DataGroup"].ToString();
                    dalEventVisitCondition.TypeId = int.Parse(dr["TypeID"].ToString());
                    dalEventVisitCondition.OutletId = int.Parse(dr["OutletID"].ToString());

                    if (dr["DateFrom"].ToString() != "")
                        dalEventVisitCondition.DateFrom = DateTime.Parse(dr["DateFrom"].ToString());
                    if (dr["DateTo"].ToString() != "")
                        dalEventVisitCondition.DateTo = DateTime.Parse(dr["DateTo"].ToString());
                    if (dr["LastActivity"].ToString() != "")
                        dalEventVisitCondition.LastActivityNotOver = int.Parse(dr["LastActivity"].ToString());
                    if (dr["Visit"].ToString() != "")
                        dalEventVisitCondition.VisitFrequency = int.Parse(dr["Visit"].ToString());

                    if (Mode.ToLower().Equals("add"))
                    {
                        dalEventVisitCondition.CreateBy = dr["CreateBy"].ToString();
                        dalEventVisitCondition.CreateDate = DateTime.Now;
                    }
                    else
                    {
                        dalEventVisitCondition.UpdateBy = dr["UpdateBy"].ToString();
                        dalEventVisitCondition.UpdateDate = DateTime.Now;
                    }

                    dalEventVisitCondition.Insert();
                }
                ///*by O_Lek*/

                User userVip = new User();

                if (dtEventDiageoVip != null)
                {
                    if (dtEventDiageoVip.Rows.Count > 0)
                    {
                        dalEventDiageoVipExtractData.MainConnectionProvider = conProv;
                        foreach (DataRow drDiageoVip in dtEventDiageoVip.Rows)
                        {
                            dalEventDiageoVipExtractData.EventId = EventID;
                            dalEventDiageoVipExtractData.FirstName = drDiageoVip["First Name"].ToString();
                            dalEventDiageoVipExtractData.LastName = drDiageoVip["Last Name"].ToString();
                            dalEventDiageoVipExtractData.Mobile = drDiageoVip["MOBILE NUMBER"].ToString();
                            dalEventDiageoVipExtractData.Email = drDiageoVip["EMAIL"].ToString();
                            dalEventDiageoVipExtractData.CreateBy = userVip.Username;
                            DataTable dtCheck = dalEventDiageoVipExtractData.SearchFirstAndLastName();
                            if (dtCheck.Rows.Count == 0)
                            {
                                dalEventDiageoVipExtractData.Insert();
                            }
                        }
                    }
                }
                if (dtEventOutletVip != null)
                {
                    if (dtEventOutletVip.Rows.Count > 0)
                    {
                        dalEventOutletVipExtractData.MainConnectionProvider = conProv;
                        foreach (DataRow drEventOutletVip in dtEventOutletVip.Rows)
                        {
                            dalEventOutletVipExtractData.EventId = EventID;
                            dalEventOutletVipExtractData.FirstName = drEventOutletVip["First Name"].ToString();
                            dalEventOutletVipExtractData.LastName = drEventOutletVip["Last Name"].ToString();
                            dalEventOutletVipExtractData.Mobile = drEventOutletVip["MOBILE NUMBER"].ToString();
                            dalEventOutletVipExtractData.Email = drEventOutletVip["EMAIL"].ToString();
                            dalEventOutletVipExtractData.CreateBy = userVip.Username;
                            DataTable dtCheck = dalEventOutletVipExtractData.SearchFirstAndLastName();
                            if (dtCheck.Rows.Count == 0)
                            {
                                dalEventOutletVipExtractData.Insert();
                            }
                        }
                    }
                }
                /*by O_Lek*/
                conProv.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                conProv.RollbackTransaction(TransactionName);
                throw new Exception("InsertOutboundEvent :: " + ex.Message);
            }
            finally
            {
                conProv.CloseConnection(false);
                conProv.Dispose();
            }
        }

        public bool SaveDataExtractCampaign(DataTable dtOutboundEventMaster, DataTable dtOutboundEvent, DataTable dtEventVisitCondition, ref int eventId)
        {
            OutboundEventMasterExtractDataDAL dalOutboundEventMaster = new OutboundEventMasterExtractDataDAL();
            OutboundEventExtractDataDAL dalOutboundEvent = new OutboundEventExtractDataDAL();
            EventVisitConditionExtractDataDAL dalEventVisitCondition = new EventVisitConditionExtractDataDAL();

            OutboundEventDAL DAL_OutboundEvent = new OutboundEventDAL();
            EventVisitConditionDAL DAL_EventVisitCondition = new EventVisitConditionDAL();

            ConnectionProvider conProv = new ConnectionProvider();

            string TransactionName = "SaveDataExtractCampaign";
            DataRow dr;


            try
            {
                conProv.OpenConnection();
                conProv.BeginTransaction(TransactionName);

                dalOutboundEventMaster.MainConnectionProvider = conProv;

                dr = dtOutboundEventMaster.Rows[0];
                dalOutboundEventMaster.EventName = dr["EventName"].ToString();
                dalOutboundEventMaster.EventDateFrom = DateTime.Parse(dr["EventDateFrom"].ToString());
                dalOutboundEventMaster.EventDateTo = DateTime.Parse(dr["EventDateTo"].ToString());
                dalOutboundEventMaster.ExcludeBlockList = dr["ExcludeBlockList"].ToString();

                dalOutboundEventMaster.Flag = dr["flag"].ToString();

                dalOutboundEventMaster.IncludeDiageoVIP = dr["IncludeDiageoVIP"].ToString();
                dalOutboundEventMaster.IncludeOutletVIP = dr["IncludeOutletVIP"].ToString();
                dalOutboundEventMaster.IncludeOutletVIPList = dr["IncludeOutletVIPList"].ToString();
                dalOutboundEventMaster.ExcludeOtherOutletVIP = dr["ExcludeOtherOutletVIP"].ToString();
                dalOutboundEventMaster.RecordPriority = dr["RecordPriority"].ToString();
                if (dr["CustomerMaxRepeat"].ToString() != "")
                    dalOutboundEventMaster.CustomerMaxRepeat = int.Parse(dr["CustomerMaxRepeat"].ToString());

                if (int.Parse(dr["eventId"].ToString()) <= 0)
                {
                    dalOutboundEventMaster.CreateBy = dr["CreateBy"].ToString();
                    dalOutboundEventMaster.CreateDate = DateTime.Now;

                    dalOutboundEventMaster.Insert();
                }
                else
                {
                    dalOutboundEventMaster.UpdateBy = dr["UpdateBy"].ToString();
                    dalOutboundEventMaster.UpdateDate = DateTime.Now;

                    dalOutboundEventMaster.Update();
                }

                //dalOutboundEventMaster.Flag = "Event";
                
                EventID = int.Parse(dalOutboundEventMaster.EventId.ToString());
                eventId = EventID;
                //--End of OutboundEventMaster

                //--Start of Delete OutboundEvent
                DAL_OutboundEvent.MainConnectionProvider = conProv;
                DAL_OutboundEvent.EventId = EventID;
                DAL_OutboundEvent.DeleteByEventId();
                //--End of Delete OutboundEvent

                //--Start of OutboundEvent
                dalOutboundEvent.MainConnectionProvider = conProv;
                for (int i = 0; i < dtOutboundEvent.Rows.Count; i++)
                {
                    dr = dtOutboundEvent.Rows[i];
                    dalOutboundEvent.EventId = EventID;
                    dalOutboundEvent.DataGroup = dr["DataGroup"].ToString();
                    dalOutboundEvent.Name = dr["name"].ToString();
                    dalOutboundEvent.SenderName = dr["senderName"].ToString();
                    //dalOutboundEvent.BrandId = int.Parse(dr["BrandID"].ToString());

                    dalOutboundEvent.BrandPreJwGold = dr["brandPreJwGold"].ToString();
                    dalOutboundEvent.BrandPreJwBlack = dr["brandPreJwBlack"].ToString();
                    dalOutboundEvent.BrandPreSmirnoff = dr["brandPreSmirnoff"].ToString();
                    dalOutboundEvent.BrandPreJwRed = dr["brandPreJwRed"].ToString();
                    dalOutboundEvent.BrandPreJwGreen = dr["brandPreJwGreen"].ToString();
                    dalOutboundEvent.BrandPreBenmore = dr["brandPreBenmore"].ToString();
                    dalOutboundEvent.BrandPreBaileys = dr["brandPreBaileys"].ToString();
                    dalOutboundEvent.BrandPreAbsolut = dr["brandPreAbsolut"].ToString();
                    dalOutboundEvent.BrandPreBallentine = dr["brandPreBallentine"].ToString();
                    dalOutboundEvent.BrandPreBlend = dr["brandPreBlend"].ToString();
                    dalOutboundEvent.BrandPreChivas = dr["brandPreChivas"].ToString();
                    dalOutboundEvent.BrandPreDewar = dr["brandPreDewar"].ToString();
                    dalOutboundEvent.BrandPre100Pipers = dr["brandPre100Pipers"].ToString();
                    dalOutboundEvent.BrandPre100Pipers8Y = dr["brandPre100Pipers8Y"].ToString();
                    dalOutboundEvent.BrandPreHennessy = dr["brandPreHennessy"].ToString();
                    dalOutboundEvent.BrandPreOther = dr["brandPreOther"].ToString();
                    dalOutboundEvent.BrandPreNotDrink = dr["brandPreNotdrink"].ToString();
                    dalOutboundEvent.Gender = dr["gender"].ToString();
                    dalOutboundEvent.ValidOptInDiageo = dr["validOptInDiageo"].ToString();
                    dalOutboundEvent.ValidOptInOutlet = dr["validOptInOutlet"].ToString();
                    if (dr["startAge"].ToString() != "")
                        dalOutboundEvent.StartAge = int.Parse(dr["startAge"].ToString());
                    if (dr["endAge"].ToString() != "")
                        dalOutboundEvent.EndAge = int.Parse(dr["endAge"].ToString());

                    dalOutboundEvent.IncludeCallcenterBounce = dr["includeCallcenterBounce"].ToString();
                    dalOutboundEvent.IncludeEDMBounce = dr["includeEDMBounce"].ToString();
                    dalOutboundEvent.IncludeSMSBounce = dr["includeSMSBounce"].ToString();

                    dalOutboundEvent.PlatformIDList = dr["PlatformIDList"].ToString();
                    dalOutboundEvent.OutletIDList = dr["OutletIDList"].ToString();
                    dalOutboundEvent.ProvinceList = dr["ProvinceList"].ToString();

                    if (dr["ActivePeriodDateFrom"].ToString() != "")
                        dalOutboundEvent.ActivePeriodDateFrom = DateTime.Parse(dr["ActivePeriodDateFrom"].ToString());
                    if (dr["ActivePeriodDateTo"].ToString() != "")
                        dalOutboundEvent.ActivePeriodDateTo = DateTime.Parse(dr["ActivePeriodDateTo"].ToString());

                    dalOutboundEvent.Adorer = dr["Adorer"].ToString();
                    dalOutboundEvent.Adopter = dr["Adopter"].ToString();
                    dalOutboundEvent.Acceptor = dr["Acceptor"].ToString();
                    dalOutboundEvent.Available = dr["Available"].ToString();
                    dalOutboundEvent.Rejector = dr["Rejecter"].ToString();

                    dalOutboundEvent.EDMValid = dr["EDMValid"].ToString();
                    dalOutboundEvent.SMSValid = dr["SMSValid"].ToString();
                    dalOutboundEvent.CallCenterValid = dr["CallCenterValid"].ToString();

                    if (dr["BDRangeFrom"].ToString() != "")
                        dalOutboundEvent.BDRangeFrom = DateTime.Parse(dr["BDRangeFrom"].ToString());
                    if (dr["BDRangeTo"].ToString() != "")
                        dalOutboundEvent.BDRangeTo = DateTime.Parse(dr["BDRangeTo"].ToString());

                    dalOutboundEvent.Active = dr["active"].ToString();
                    dalOutboundEvent.CreateBy = dr["CreateBy"].ToString();
                    dalOutboundEvent.CreateDate = DateTime.Now;

                    //dalOutboundEvent.MonthJan = dr["MonthJan"].ToString();
                    //dalOutboundEvent.MonthFeb = dr["MonthFeb"].ToString();
                    //dalOutboundEvent.MonthMar = dr["MonthMar"].ToString();
                    //dalOutboundEvent.MonthApr = dr["MonthApr"].ToString();
                    //dalOutboundEvent.MonthMay = dr["MonthMay"].ToString();
                    //dalOutboundEvent.MonthJun = dr["MonthJun"].ToString();
                    //dalOutboundEvent.MonthJul = dr["MonthJul"].ToString();
                    //dalOutboundEvent.MonthAug = dr["MonthAug"].ToString();
                    //dalOutboundEvent.MonthSep = dr["MonthSep"].ToString();
                    //dalOutboundEvent.MonthOct = dr["MonthOct"].ToString();
                    //dalOutboundEvent.MonthNov = dr["MonthNov"].ToString();
                    //dalOutboundEvent.MonthDec = dr["MonthDec"].ToString();

                    //if (dr["OpenEmail"].ToString() != "")
                    //    dalOutboundEvent.OpenEmailInLastTime = int.Parse(dr["OpenEmail"].ToString());
                    //if (dr["ReceiveEmail"].ToString() != "")
                    //    dalOutboundEvent.ReceiveEmailInLessTime = int.Parse(dr["ReceiveEmail"].ToString());

                    //dalOutboundEvent.EmailActive = dr["EmailActive"].ToString();
                    //dalOutboundEvent.MobileActive = dr["MobileActive"].ToString();

                    //if (dr["EmailRate"].ToString() != "")
                    //    dalOutboundEvent.EmailRate = decimal.Parse(dr["EmailRate"].ToString());

                    

                    //dalOutboundEvent.Callcenterflag = "1";
                    //dalOutboundEvent.Edmflag = "1";
                    //dalOutboundEvent.Smsflag = "1";
                    //dalOutboundEvent.Bdayflag = "F";
                    //dalOutboundEvent.Active = "T";

                    dalOutboundEvent.Insert();
                }
                //--End of OutboundEvent


                //--Start of Delete EventVisitCondition
                DAL_EventVisitCondition.MainConnectionProvider = conProv;
                DAL_EventVisitCondition.EventId = EventID;
                DAL_EventVisitCondition.DeleteByEventId();
                //--End of Delete EventVisitCondition

                //--Start of EventVisitCondition
                dalEventVisitCondition.MainConnectionProvider = conProv;
                for (int i = 0; i < dtEventVisitCondition.Rows.Count; i++)
                {
                    dr = dtEventVisitCondition.Rows[i];
                    dalEventVisitCondition.EventId = EventID;
                    dalEventVisitCondition.DataGroup = dr["DataGroup"].ToString();
                    dalEventVisitCondition.TypeId = int.Parse(dr["typeId"].ToString());
                    dalEventVisitCondition.TypeIDDetail = int.Parse(dr["TypeIDDetail"].ToString());
                    //dalEventVisitCondition.OutletId = int.Parse(dr["OutletID"].ToString());

                    if (dr["dateFrom"].ToString() != "")
                        dalEventVisitCondition.DateFrom = DateTime.Parse(dr["dateFrom"].ToString());
                    if (dr["dateTo"].ToString() != "")
                        dalEventVisitCondition.DateTo = DateTime.Parse(dr["dateTo"].ToString());
                    //if (dr["LastActivity"].ToString() != "")
                    //    dalEventVisitCondition.LastActivityNotOver = int.Parse(dr["LastActivity"].ToString());
                    if (dr["visitFrequency"].ToString() != "")
                        dalEventVisitCondition.VisitFrequency = int.Parse(dr["visitFrequency"].ToString());

                    dalEventVisitCondition.CreateBy = dr["CreateBy"].ToString();
                    dalEventVisitCondition.CreateDate = DateTime.Now;

                    dalEventVisitCondition.Insert();
                }
                //--End of EventVisitCondition

                conProv.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                conProv.RollbackTransaction(TransactionName);
                throw new Exception("SaveDataExtractCampaign :: " + ex.Message);
            }
            finally
            {
                conProv.CloseConnection(false);
                conProv.Dispose();
            }
        }
    }
}