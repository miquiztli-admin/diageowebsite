﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using DIAGEO.DAL.GDAL;
using System.Collections;
using System.Data;

namespace DIAGEO.BLL
{
    public class OutboundBLL
    {
        private OutboundDAL dalOutbound;

        public bool UpdateResultData(ArrayList ConsumerID, ArrayList ResultData, ArrayList ResultCode, ArrayList Code, ArrayList Outlet, string UserID)
        {
            ConsumerBLL bllConsumer = new ConsumerBLL();
            ConnectionProvider conProv = new ConnectionProvider();
            dalOutbound = new OutboundDAL();
            DateTime Date = DateTime.Now;
            DataTable dtConsumer = new DataTable("Consumer");

            string TransactionName = "UpdateResultData";

            try
            {
                conProv.OpenConnection();
                conProv.BeginTransaction(TransactionName);
                
                for (int i = 0; i < ConsumerID.Count; i++)
                {
                    dalOutbound.MainConnectionProvider = conProv;

                    dtConsumer = CheckConsumer(ConsumerID[i].ToString());
                    if (dtConsumer.Rows.Count > 0)
                    {
                        dalOutbound.UpdateResultData(int.Parse(ConsumerID[i].ToString()), ResultData[i].ToString(), ResultCode[i].ToString(), Outlet[i].ToString(), UserID, Date);
                        bllConsumer.UpdateOutboundCallCenter(int.Parse(ConsumerID[i].ToString()), Code[i].ToString(), UserID, Date, conProv);
                    }
                }

                conProv.CommitTransaction();
                return true;
            }
            catch (Exception ex)
            {
                conProv.RollbackTransaction(TransactionName);
                throw new Exception("OutboundBLL.UpdateResultData :: " + ex.Message);
            }
            finally
            {
                conProv.CloseConnection(false);
                conProv.Dispose();
            }
        }
        // ทำเรื่องเช็ค softbound hardbound
        public DataTable CheckConsumer(string ID)
        {
            try
            {
                dalOutbound = new OutboundDAL();
                return dalOutbound.GetOutboundDataAndConsumerData(int.Parse(ID));
            }
            catch (Exception ex)
            {
                throw new Exception("OutboundBLL.GetOutboundDataAndConsumerData :: " + ex.Message);
            }
        }
    }
}
