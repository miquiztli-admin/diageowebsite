﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using System.Data;

namespace DIAGEO.BLL
{
    public class OutletBLL
    {

        public DataTable SelectOne(int outletId)
        {
            OutletDAL outletDAL;

            try
            {
                outletDAL = new OutletDAL();
                outletDAL.OutletId = outletId;
                return outletDAL.SelectOne();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectActiveOutlet(string active)
        {
            OutletDAL outletDAL;

            try
            {
                outletDAL = new OutletDAL();
                outletDAL.Active = active;
                return outletDAL.SelectActiveOutlet();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        } 
        
        public DataTable SelectOneBYOutletName(string outletCode, string outletName)
        {
            OutletDAL outletDAL;

            try
            {
                outletDAL = new OutletDAL();
                return outletDAL.SelectOneBYOutletName(outletCode,outletName);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable Search(string outletCode, string outletName, string zoneId, string address, string tel, string active, int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            OutletDAL outletDAL;
            DataTable dtOutlet;

            try
            {
                outletDAL = new OutletDAL();
                dtOutlet = outletDAL.Search(outletCode, outletName, int.Parse(zoneId), address, tel, active, startRowIndex, maximumRows, sortExpression, isCount);

                return dtOutlet;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool UpdateSearch(string outletId, string outletCode, string outletName, int zoneId, string address,string tel, string active, string userIDLogin)
        {
            OutletDAL outletDAL;

            try
            {
                outletDAL = new OutletDAL();
                outletDAL.OutletId = int.Parse(outletId);
                outletDAL.Code = outletCode;
                outletDAL.Name = outletName;
                outletDAL.ZoneId = zoneId;
                outletDAL.Address = address;
                outletDAL.Tel = tel;
                outletDAL.Active = active;
                outletDAL.UpdateDate = DateTime.Now;
                outletDAL.UpdateSearch(userIDLogin);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Insert(string outletCode, string outletName, int zoneId, string address, string tel, string active, string userIDLogin)
        {
            OutletDAL outletDAL;

            try
            {
                outletDAL = new OutletDAL();
                outletDAL.Code = outletCode;
                outletDAL.Name = outletName;
                outletDAL.ZoneId = zoneId;
                outletDAL.Address = address;
                outletDAL.Tel = tel;
                outletDAL.Active = active;
                outletDAL.CreateBy = userIDLogin;
                outletDAL.CreateDate = DateTime.Now;
                outletDAL.UpdateBy = userIDLogin;
                outletDAL.UpdateDate = DateTime.Now;
                outletDAL.Insert();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectActiveOutletOrderByName(string active)
        {
            OutletDAL dalOutlet = new OutletDAL();
            try
            {
                return dalOutlet.SelectActiveOutletOrderByName(active);
            }
            catch (Exception ex)
            {
                throw new Exception("OutletBLL.SelectActiveOutletOrderByName :: " + ex.Message);
            }
        } 
    }
}
