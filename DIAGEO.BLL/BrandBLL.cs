﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using System.Data;

namespace DIAGEO.BLL
{
    public class BrandBLL
    {
        private BrandDAL dalBrand;

        public DataTable SelectActiveBrandOrderByName(string Active)
        {
            try
            {
                dalBrand = new BrandDAL();
                return dalBrand.SelectActiveBrandOrderByName(Active);
            }
            catch (Exception ex)
            {
                throw new Exception("BrandBLL.SelectActiveBrandOrderByName :: " + ex.Message);
            }
        }
    }
}
