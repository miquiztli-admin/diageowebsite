﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using System.Data;

namespace DIAGEO.BLL
{
    public class ProvinceBLL
    {
        public DataTable SelectAll()
        {
            ProvinceDAL dalProvince = new ProvinceDAL();
            try
            {
                return dalProvince.SelectAll();
            }
            catch (Exception ex)
            {
                throw new Exception("ProvinceBLL.SelectAll :: " + ex.Message);
            }
        }

        public DataTable SelectAllAndOrderByName()
        {
            ProvinceDAL dalProvince = new ProvinceDAL();
            try
            {
                return dalProvince.SelectAllAndOrderByName();
            }
            catch (Exception ex)
            {
                throw new Exception("ProvinceBLL.SelectAllAndOrderByName :: " + ex.Message);
            }
        }

        public DataTable SelectOne(int provinceId)
        {
            ProvinceDAL provinceDAL;

            try
            {
                provinceDAL = new ProvinceDAL();
                provinceDAL.ProvinceId = provinceId;
                return provinceDAL.SelectOne();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectOneBYOutletName(string provinceName)
        {
            ProvinceDAL provinceDAL;

            try
            {
                provinceDAL = new ProvinceDAL();
                return provinceDAL.SelectOneBYProvinceName(provinceName);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable Search(string provinceName, int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            ProvinceDAL provinceDAL;

            try
            {
                provinceDAL = new ProvinceDAL();
                return provinceDAL.Search(provinceName, startRowIndex, maximumRows, sortExpression, isCount);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool UpdateSearch(string provinceId, string provinceName)
        {
            ProvinceDAL provinceDAL;

            try
            {
                provinceDAL = new ProvinceDAL();
                provinceDAL.ProvinceId = int.Parse(provinceId);
                provinceDAL.Name = provinceName;
                provinceDAL.UpdateSearch();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Insert(string provinceName)
        {
            ProvinceDAL provinceDAL;

            try
            {
                provinceDAL = new ProvinceDAL();
                provinceDAL.Name = provinceName;
                provinceDAL.Insert();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
