﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using System.Data;

namespace DIAGEO.BLL
{
    public class OutboundConfigBLL
    {

        public DataTable SelectOne(int outboundConfigId)
        {
            OutboundConfigDAL outboundConfigDAL;

            try
            {
                outboundConfigDAL = new OutboundConfigDAL();
                outboundConfigDAL.OutboundConfigId = outboundConfigId;
                return outboundConfigDAL.SelectOne();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable Search(int platformId, int outletId, int eventId, int outboundTypeId, string active, int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            OutboundConfigDAL outboundConfigDAL;

            try
            {
                outboundConfigDAL = new OutboundConfigDAL();

                return outboundConfigDAL.Search(platformId, outletId, eventId, outboundTypeId, active, startRowIndex, maximumRows, sortExpression, isCount);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool UpdateSearch(string outboundConfigId, string platformId, string outletId, string eventId, string outboundTypeId, string qtyOfDay, DateTime startDate, DateTime endDate, string active, string userIDLogin)
        {
            OutboundConfigDAL outboundConfigDAL;

            try
            {
                outboundConfigDAL = new OutboundConfigDAL();
                outboundConfigDAL.OutboundConfigId = int.Parse(outboundConfigId);
                outboundConfigDAL.PlatformId = int.Parse(platformId);
                outboundConfigDAL.OutletId = int.Parse(outletId);
                outboundConfigDAL.EventId = int.Parse(eventId);
                outboundConfigDAL.OutboundTypeId = int.Parse(outboundTypeId);
                outboundConfigDAL.QtyOfDay = int.Parse(qtyOfDay);
                outboundConfigDAL.StartDate = startDate;
                outboundConfigDAL.EndDate = endDate;
                outboundConfigDAL.Active = active;
                outboundConfigDAL.UpdateDate = DateTime.Now;
                outboundConfigDAL.UpdateSearch(userIDLogin);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Insert(string platformId, string outletId, string eventId, string outboundTypeId, string qtyOfDay, DateTime startDate, DateTime endDate, string active, string userIDLogin)
        {
            OutboundConfigDAL outboundConfigDAL;

            try
            {
                outboundConfigDAL = new OutboundConfigDAL();
                outboundConfigDAL.PlatformId = int.Parse(platformId);
                outboundConfigDAL.OutletId = int.Parse(outletId);
                outboundConfigDAL.EventId = int.Parse(eventId);
                outboundConfigDAL.OutboundTypeId = int.Parse(outboundTypeId);
                outboundConfigDAL.QtyOfDay = int.Parse(qtyOfDay);
                outboundConfigDAL.StartDate = startDate;
                outboundConfigDAL.EndDate = endDate;
                outboundConfigDAL.Active = active;
                outboundConfigDAL.CreateBy = userIDLogin;
                outboundConfigDAL.CreateDate = DateTime.Now;
                outboundConfigDAL.UpdateBy = userIDLogin;
                outboundConfigDAL.UpdateDate = DateTime.Now;
                outboundConfigDAL.Insert();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
