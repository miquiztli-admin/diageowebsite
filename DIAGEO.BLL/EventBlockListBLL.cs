﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using DIAGEO.DAL.GDAL;
using System.Data;

namespace DIAGEO.BLL
{
    public class EventBlockListBLL
    {
        public bool InsertEventBlockList(string MobileName, string Email , string NationID, string Fname, string Lname , string BlockReason , string UserName)
        {
            EventBlockListDAL eventBlockListDAL;

            try
            {
                eventBlockListDAL = new EventBlockListDAL();
                return eventBlockListDAL.InsertEventBlockList(int.Parse(MobileName), Email, int.Parse(NationID), Fname, Lname, BlockReason, UserName);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
