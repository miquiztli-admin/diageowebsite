﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using System.Data;

namespace DIAGEO.BLL
{
    public class EventVisitConditionBLL
    {
        public DataTable SelectByEventIDAndDataGroup(int eventID, int dataGroup)
        {
            EventVisitConditionDAL eventVisitConditionDAL;

            try
            {
                eventVisitConditionDAL = new EventVisitConditionDAL();
                return eventVisitConditionDAL.SelectByEventIDAndDataGroup(eventID, dataGroup);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectByEachTypeID(int eventID, int dataGroup, int typeId)
        {
            EventVisitConditionDAL eventVisitConditionDAL;

            try
            {
                eventVisitConditionDAL = new EventVisitConditionDAL();
                return eventVisitConditionDAL.SelectByEachTypeId(eventID, dataGroup,typeId);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
