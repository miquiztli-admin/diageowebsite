﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using DIAGEO.DAL.GDAL;
using System.Collections;
using System.Data;
using System.Globalization;

namespace DIAGEO.BLL
{
    public class ConsumerBLL
    {

        private ConsumerDAL dalConsumer;

        public void UpdateOutboundCallCenter(int OutboundID, string ResultCode, string UserID, DateTime Date, ConnectionProvider conProv)
        {
            dalConsumer = new ConsumerDAL();
            try
            {
                dalConsumer.MainConnectionProvider = conProv;
                dalConsumer.UpdateOutboundCallCenter(OutboundID, ResultCode, UserID, Date);
            }
            catch (Exception ex)
            {
                throw new Exception("ConsumerBLL.UpdateOutboundCallCenter :: " + ex.Message);
            }
        }

        public DataTable ExtractData(string PlatformId, bool BrandPreBaileys, bool BrandPreBenmore, bool BrandPreJwBlack,
        bool BrandPreJwGold, bool BrandPreJwGreen, bool BrandPreJwRed, bool BrandPreSmirnoff, bool BrandPreNonAbsolute,
        bool BrandPreNonBallentine, bool BrandPreNonBlend, bool BrandPreNonChivas, bool BrandPreNonDewar, bool BrandPreNon100Pipers,
        bool BrandPreNon100Pipers8yrs, string Survey4aBaileys, string Survey4aBenmore, string Survey4aJwBlack, string Survey4aJwGold,
        string Survey4aJwGreen, string Survey4aJwRed, string Survey4aSmirnoff, string SurveyP4wNonAbsolute, string SurveyP4wNonBallentine,
        string SurveyP4wNonBlend, string SurveyP4wNonChivas, string SurveyP4wNonDewar, string SurveyP4wNon100Pipers, string SurveyP4wNon100Pipers8yrs,
        string Gender, string GenderFrom, string GenderTo, DateTime? DateOfBirthFrom, DateTime? DateOfBirthTo, string Province, string RegisterOutlet,
        DateTime? RegisterDateFrom, DateTime? RegisterDateTo,
        string LastVisitOutlet, DateTime? LastVisitDateFrom, DateTime? LastVisitDateTo, bool ActiveEmail, bool ActiveMobile, bool ActiveCallCenter, string SubScribePlatformID,
        int IsCount, int StartRowIndex, int MaximumRowIndex)
        {
            ConsumerDAL consumerDAL = new ConsumerDAL();

            try
            {
                return consumerDAL.ExtractData(PlatformId, BrandPreBaileys, BrandPreBenmore, BrandPreJwBlack, BrandPreJwGold, BrandPreJwGreen, BrandPreJwRed,
                                        BrandPreSmirnoff, BrandPreNonAbsolute, BrandPreNonBallentine, BrandPreNonBlend, BrandPreNonChivas, BrandPreNonDewar,
                                        BrandPreNon100Pipers, BrandPreNon100Pipers8yrs,
                                        Survey4aBaileys, Survey4aBenmore, Survey4aJwBlack, Survey4aJwGold, Survey4aJwGreen, Survey4aJwRed, Survey4aSmirnoff,
                                        SurveyP4wNonAbsolute, SurveyP4wNonBallentine, SurveyP4wNonBlend, SurveyP4wNonChivas, SurveyP4wNonDewar, SurveyP4wNon100Pipers,
                                        SurveyP4wNon100Pipers8yrs, Gender, GenderFrom, GenderTo, DateOfBirthFrom, DateOfBirthTo,
                                        Province, RegisterOutlet, RegisterDateFrom, RegisterDateTo, LastVisitOutlet, LastVisitDateFrom, LastVisitDateTo,
                                        ActiveEmail, ActiveMobile, ActiveCallCenter, SubScribePlatformID, IsCount, StartRowIndex, MaximumRowIndex);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool UpdateConsumer(int consumerId, string idCard, string firstName, string lastName, string email, string mobile, DateTime dateOfBirth,
        string gender, string homeAddess, string homeMoo, string homeVillage, string homeSoi, string homeRoad, string homeSubDistrict,
        string homeDistrict, string homeProvince, string workAddess, string workMoo, string workVillage, string workSoi, string workRoad, string workSubDistrict,
        string workDistrict, string workProvince, string updateBy)
        {
            dalConsumer = new ConsumerDAL();

            try
            {
                dalConsumer.ConsumerId = consumerId;
                dalConsumer.IdCard = idCard;
                dalConsumer.FirstName = firstName;
                dalConsumer.LastName = lastName;
                dalConsumer.Email = email;
                dalConsumer.Mobile = mobile;
                dalConsumer.DateOfBirth = dateOfBirth;
                dalConsumer.Gender = gender;
                dalConsumer.HomeAddress = homeAddess;
                dalConsumer.HomeMoo = homeMoo;
                dalConsumer.HomeVillage = homeVillage;
                dalConsumer.HomeSoi = homeSoi;
                dalConsumer.HomeRoad = homeRoad;
                dalConsumer.HomeSubDistrict = homeSubDistrict;
                dalConsumer.HomeDistrict = homeDistrict;
                dalConsumer.HomeProvince = homeProvince;
                dalConsumer.WorkAddress = workAddess;
                dalConsumer.WorkMoo = workMoo;
                dalConsumer.WorkVillage = workVillage;
                dalConsumer.WorkSoi = workSoi;
                dalConsumer.WorkRoad = workRoad;
                dalConsumer.WorkSubDistrict = workSubDistrict;
                dalConsumer.WorkDistrict = workDistrict;
                dalConsumer.WorkProvince = workProvince;
                dalConsumer.UpdateDate = DateTime.Now;
                dalConsumer.UpdateBy = updateBy;
                dalConsumer.UpdateConsumer();
                return true;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        public DataTable Search(string consumerIdCard, string consumerFirstName, string consumerLastName, string consumerEmail, string consumerMobile,
        int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            ConsumerDAL consumerDAL;

            try
            {
                consumerDAL = new ConsumerDAL();
                return consumerDAL.Search(consumerIdCard, consumerFirstName, consumerLastName, consumerEmail, consumerMobile, startRowIndex, maximumRows, sortExpression, isCount);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectOne(int consumerId)
        {
            ConsumerDAL consumerDAL;

            try
            {
                consumerDAL = new ConsumerDAL();
                consumerDAL.ConsumerId = consumerId;
                return consumerDAL.SelectOne();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectOneBYIdCardEmailAndMobile(string idCard, string email, string moblie)
        {
            ConsumerDAL consumerDAL;
            DataTable dtConsumer;

            try
            {
                consumerDAL = new ConsumerDAL();
                dtConsumer = consumerDAL.SelectOneBYIdCardEmailAndMobile(idCard, email, moblie);
                return dtConsumer;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool InserOrUpdateConsumerByIpad(DataTable dtConsumerIpad, string UserId)
        {
            ConsumerDAL consumerDAL;
            OutletDAL outletDAL;
            BrandMemberDAL brandMemberDAL;
            BrandDAL brandDAL;

            ConnectionProvider conp = new ConnectionProvider();

            try
            {
                DataTable dtConsumer, dtBranMember, dtOutlet, dtBrand;
                DataRow dr, drManipulate;
                DateTime DateOfBirthday, DateRegisTer;

                consumerDAL = new ConsumerDAL();
                outletDAL = new OutletDAL();
                brandMemberDAL = new BrandMemberDAL();
                brandDAL = new BrandDAL();


                conp.OpenConnection();
                conp.BeginTransaction("InserOrUpdateConsumerByIpad");

                brandMemberDAL.MainConnectionProvider = conp;
                consumerDAL.MainConnectionProvider = conp;

                if (dtConsumerIpad.Rows.Count < 0)
                {
                    return false;
                }

                for (int i = 0; i < dtConsumerIpad.Rows.Count; i++)
                {
                    dr = dtConsumerIpad.Rows[i];

                    string OutletNameForInsert = "";
                    string BrandNameForInsert = "";
                    string BrandIdForInsert = "";
                    int OutletIdForInsert;

                    DateTime.TryParseExact(dr["Birthday"].ToString(), "M/d/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateOfBirthday);

                    DateTime.TryParseExact(dr["RegisterDate"].ToString(), "M/d/yyyy hh:mm:ss tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateRegisTer);

                    OutletNameForInsert = dr["OutletName"].ToString();
                    BrandNameForInsert = dr["BarndName"].ToString();

                    outletDAL.Name = OutletNameForInsert;
                    dtOutlet = outletDAL.SelectByOutletName();
                    if (!(dtOutlet.Rows.Count > 0))
                        return false;
                    OutletIdForInsert = int.Parse(outletDAL.OutletId.ToString());


                    brandDAL.Brand_Name = BrandNameForInsert;
                    dtBrand = brandDAL.SelectByBrandName();
                    if (!(dtBrand.Rows.Count > 0))
                        return false;
                    BrandIdForInsert = brandDAL.Brand_ID.ToString();

                    dtConsumer = consumerDAL.ManipulateConsumer(dr["EMAIL"].ToString(), dr["MOBILENUMBER"].ToString(), DateOfBirthday, dr["FirstName"].ToString(), dr["LastName"].ToString());

                    if (dtConsumer.Rows.Count > 0)
                    {
                        int score;
                        int countrowupdate = 0;
                        for (int j = 0; j < dtConsumer.Rows.Count; j++)
                        {
                            drManipulate = dtConsumer.Rows[j];
                            score = int.Parse(drManipulate["Score"].ToString());
                            if (score >= 51)
                            {
                                //--UpdateConsumer
                                consumerDAL.ConsumerId = int.Parse(drManipulate["CONSUMERID"].ToString());
                                consumerDAL.FirstName = dr["FirstName"].ToString();
                                consumerDAL.LastName = dr["LastName"].ToString();
                                consumerDAL.DateOfBirth = DateOfBirthday;
                                consumerDAL.Email = dr["EMAIL"].ToString();
                                consumerDAL.Mobile = dr["MOBILENUMBER"].ToString();

                                if (dr["BrandPreference"].ToString() == "100 PIPERS")
                                {
                                    consumerDAL.BrandPre100Pipers = "T";
                                    consumerDAL.Survey4a100Pipers = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "")
                                {
                                    consumerDAL.BrandPre100Pipers8Y = "T";
                                    consumerDAL.Survey4a100Pipers8Y = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "ABSOLUTE VODKA")
                                {
                                    consumerDAL.BrandPreAbsolut = "T";
                                    consumerDAL.Survey4aAbsolut = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "")
                                {
                                    consumerDAL.BrandPreBaileys = "T";
                                    consumerDAL.Survey4aBaileys = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "")
                                {
                                    consumerDAL.BrandPreBallentine = "T";
                                    consumerDAL.Survey4aBallentine = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "")
                                {
                                    consumerDAL.BrandPreBenmore = "T";
                                    consumerDAL.Survey4aBenmore = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "")
                                {
                                    consumerDAL.BrandPreBlend = "T";
                                    consumerDAL.Survey4aBlend = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "CHIVAS")
                                {
                                    consumerDAL.BrandPreChivas = "T";
                                    consumerDAL.Survey4aChivas = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "")
                                {
                                    consumerDAL.BrandPreDewar = "T";
                                    consumerDAL.Survey4aDewar = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "HENNESSY")
                                {
                                    consumerDAL.BrandPreHennessy = "T";
                                    consumerDAL.Survey4aHennessy = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "JW BLACK")
                                {
                                    consumerDAL.BrandPreJwBlack = "T";
                                    consumerDAL.Survey4aJwBlack = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "JW GOLD")
                                {
                                    consumerDAL.BrandPreJwGold = "T";
                                    consumerDAL.Survey4aJwGold = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "")
                                {
                                    consumerDAL.BrandPreJwGreen = "T";
                                    consumerDAL.Survey4aJwGreen = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "JW RED")
                                {
                                    consumerDAL.BrandPreJwRed = "T";
                                    consumerDAL.Survey4aJwRed = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "SMIRNOFF")
                                {
                                    consumerDAL.BrandPreSmirnoff = "T";
                                    consumerDAL.Survey4aSmirnoff = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "")
                                {
                                    consumerDAL.BrandPreNotdrink = "T";
                                    consumerDAL.Survey4aNotdrink = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "")
                                {
                                    consumerDAL.BrandPreOther = "T";
                                    consumerDAL.Survey4aOthers = dr["4Aresponse"].ToString();
                                }
                                if (dr["BrandPreference"].ToString() == "")
                                {
                                    consumerDAL.BrandPreText = "T";
                                    consumerDAL.Survey4aText = dr["4Aresponse"].ToString();
                                }

                                consumerDAL.OptInDG = dr["OptIn"].ToString();
                                consumerDAL.OptInOutlet = dr["OptIn2"].ToString();
                                consumerDAL.LastVisitPlatformId = 2;
                                consumerDAL.LastVisitOutletId = OutletIdForInsert;
                                consumerDAL.LastVisitDate = DateRegisTer;
                                consumerDAL.TransactionDate = DateRegisTer;
                                consumerDAL.UpdateDate = DateTime.Now;
                                consumerDAL.UpdateBy = UserId;

                                consumerDAL.UpdateConsumerByIpad();

                                //--selectBrandMemberByConsumerIdAndBrandId
                                brandMemberDAL.ConsumerId = int.Parse(drManipulate["CONSUMERID"].ToString());
                                brandMemberDAL.BrandId = BrandIdForInsert;
                                dtBranMember = brandMemberDAL.SelectByConsumerIdAndBrandId();
                                //--Have UpdateBrandMember
                                if (dtBranMember.Rows.Count > 0)
                                {
                                    brandMemberDAL.ConsumerId = int.Parse(drManipulate["CONSUMERID"].ToString());
                                    brandMemberDAL.BrandId = BrandIdForInsert;
                                    brandMemberDAL.LastVisitDate = DateRegisTer;
                                    brandMemberDAL.UpdateDate = DateTime.Now;
                                    brandMemberDAL.UpdateBy = UserId;

                                    brandMemberDAL.UpdateBrandMemberByConsumerIdAndBrandId();

                                }
                                //--Non InsertBrandMember
                                else
                                {
                                    brandMemberDAL.ConsumerId = int.Parse(drManipulate["CONSUMERID"].ToString());
                                    brandMemberDAL.BrandId = BrandIdForInsert;
                                    brandMemberDAL.RegisterOutletId = OutletIdForInsert;
                                    brandMemberDAL.RegisterDate = DateRegisTer;
                                    brandMemberDAL.LastVisitDate = DateRegisTer;
                                    brandMemberDAL.CreateDate = DateTime.Now;
                                    brandMemberDAL.CreateBy = UserId;

                                    brandMemberDAL.InsertBrandMemberByIpad();

                                }
                                countrowupdate += 1;
                            }
                            else
                            {
                                if (countrowupdate == 0 && dtConsumer.Rows.Count == j + 1)
                                {
                                    //--InsertConsumer
                                    consumerDAL.FirstName = dr["FirstName"].ToString();
                                    consumerDAL.LastName = dr["LastName"].ToString();
                                    consumerDAL.DateOfBirth = DateOfBirthday;
                                    consumerDAL.Email = dr["EMAIL"].ToString();
                                    consumerDAL.Mobile = dr["MOBILENUMBER"].ToString();

                                    if (dr["BrandPreference"].ToString() == "100 PIPERS")
                                    {
                                        consumerDAL.BrandPre100Pipers = "T";
                                        consumerDAL.Survey4a100Pipers = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "")
                                    {
                                        consumerDAL.BrandPre100Pipers8Y = "T";
                                        consumerDAL.Survey4a100Pipers8Y = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "ABSOLUTE VODKA")
                                    {
                                        consumerDAL.BrandPreAbsolut = "T";
                                        consumerDAL.Survey4aAbsolut = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "")
                                    {
                                        consumerDAL.BrandPreBaileys = "T";
                                        consumerDAL.Survey4aBaileys = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "")
                                    {
                                        consumerDAL.BrandPreBallentine = "T";
                                        consumerDAL.Survey4aBallentine = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "")
                                    {
                                        consumerDAL.BrandPreBenmore = "T";
                                        consumerDAL.Survey4aBenmore = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "")
                                    {
                                        consumerDAL.BrandPreBlend = "T";
                                        consumerDAL.Survey4aBlend = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "CHIVAS")
                                    {
                                        consumerDAL.BrandPreChivas = "T";
                                        consumerDAL.Survey4aChivas = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "")
                                    {
                                        consumerDAL.BrandPreDewar = "T";
                                        consumerDAL.Survey4aDewar = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "HENNESSY")
                                    {
                                        consumerDAL.BrandPreHennessy = "T";
                                        consumerDAL.Survey4aHennessy = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "JW BLACK")
                                    {
                                        consumerDAL.BrandPreJwBlack = "T";
                                        consumerDAL.Survey4aJwBlack = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "JW GOLD")
                                    {
                                        consumerDAL.BrandPreJwGold = "T";
                                        consumerDAL.Survey4aJwGold = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "")
                                    {
                                        consumerDAL.BrandPreJwGreen = "T";
                                        consumerDAL.Survey4aJwGreen = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "JW RED")
                                    {
                                        consumerDAL.BrandPreJwRed = "T";
                                        consumerDAL.Survey4aJwRed = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "SMIRNOFF")
                                    {
                                        consumerDAL.BrandPreSmirnoff = "T";
                                        consumerDAL.Survey4aSmirnoff = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "")
                                    {
                                        consumerDAL.BrandPreNotdrink = "T";
                                        consumerDAL.Survey4aNotdrink = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "")
                                    {
                                        consumerDAL.BrandPreOther = "T";
                                        consumerDAL.Survey4aOthers = dr["4Aresponse"].ToString();
                                    }
                                    if (dr["BrandPreference"].ToString() == "")
                                    {
                                        consumerDAL.BrandPreText = "T";
                                        consumerDAL.Survey4aText = dr["4Aresponse"].ToString();
                                    }

                                    consumerDAL.OptInDG = dr["OptIn"].ToString();
                                    consumerDAL.OptInOutlet = dr["OptIn2"].ToString();
                                    consumerDAL.RegisterPlatformId = 2;
                                    consumerDAL.RegisterOutletId = OutletIdForInsert;
                                    consumerDAL.RegisterDate = DateRegisTer;
                                    consumerDAL.LastVisitPlatformId = 2;
                                    consumerDAL.LastVisitOutletId = OutletIdForInsert;
                                    consumerDAL.LastVisitDate = DateRegisTer;
                                    consumerDAL.TransactionDate = DateRegisTer;
                                    consumerDAL.CreateDate = DateTime.Now;
                                    consumerDAL.CreateBy = UserId;

                                    consumerDAL.InsertConsumerByIpad();

                                    //--InsertBrandMember

                                    brandMemberDAL.ConsumerId = consumerDAL.ConsumerId;
                                    brandMemberDAL.BrandId = BrandIdForInsert;
                                    brandMemberDAL.RegisterOutletId = OutletIdForInsert;
                                    brandMemberDAL.RegisterDate = DateRegisTer;
                                    brandMemberDAL.LastVisitDate = DateRegisTer;
                                    brandMemberDAL.CreateDate = DateTime.Now;
                                    brandMemberDAL.CreateBy = UserId;

                                    brandMemberDAL.InsertBrandMemberByIpad();

                                }
                            }


                        }
                    }
                    else
                    {
                        //--InsertConsumer
                        //--InsertBrandMember
                    }



                }
                conp.CommitTransaction();

                return true;
            }
            catch (Exception ex)
            {
                conp.RollbackTransaction("InserOrUpdateConsumerByIpad");
                throw new Exception(" ConsumerBLL::InserOrUpdateConsumerByIpad " + ex.Message);
            }
            finally
            {
                conp.CloseConnection(false);
                conp.Dispose();
            }
        }

        public DataTable SelectColumnVExtract()
        {
            ConsumerDAL consumerDAL;

            try
            {
                consumerDAL = new ConsumerDAL();
                return consumerDAL.SelectColumnVExtract();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataTable SelectConsumerData()
        {
            ConsumerDAL consumerDAL;

            try
            {
                consumerDAL = new ConsumerDAL();
                return consumerDAL.SelectConsumerData();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
