﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using DIAGEO.DAL.GDAL;
using System.Data;

namespace DIAGEO.BLL
{
    public class OutBoundEventMasterBLL
    {

        public DataTable SelectAllByEventID(int eventID)
        {
            OutboundEventMasterDAL outboundEventMasterDAL;

            try
            {
                outboundEventMasterDAL = new OutboundEventMasterDAL();
                outboundEventMasterDAL.EventId = eventID;
                return outboundEventMasterDAL.SelectOne();
            }
            catch (Exception ex)
            {
                
                throw new Exception(ex.Message);
            }
        }
    }
}
