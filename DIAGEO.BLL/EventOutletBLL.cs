﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using DIAGEO.DAL.GDAL;
using System.Data;

namespace DIAGEO.BLL
{
    public class EventOutletBLL
    {
        public DataTable SelectByEventID(int eventID)
        {
            EventOutletDAL eventOutletDAL;

            try
            {
                eventOutletDAL = new EventOutletDAL();
                return eventOutletDAL.SelectBYEventID(eventID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectByEventIDAndDataGroup(int eventID, int dataGroup)
        {
            EventOutletDAL eventOutletDAL;

            try
            {
                eventOutletDAL = new EventOutletDAL();
                return eventOutletDAL.SelectByEventIDAndDataGroup(eventID, dataGroup);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
