﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using DIAGEO.DAL.GDAL;
using System.Data;

namespace DIAGEO.BLL
{
    public class EventPlatformBLL
    {
        public DataTable SelectByEventID(int eventID)
        {
            EventPlatformDAL eventPlatformDAL;

            try
            {
                eventPlatformDAL = new EventPlatformDAL();
                return eventPlatformDAL.SelectBYEventID(eventID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectByEventIDAndDataGroup(int eventID, int dataGroup)
        {
            EventPlatformDAL eventPlatformDAL;

            try
            {
                eventPlatformDAL = new EventPlatformDAL();
                return eventPlatformDAL.SelectByEventIDAndDataGroup(eventID, dataGroup);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
    }
}
