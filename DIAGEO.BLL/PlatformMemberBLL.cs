﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DIAGEO.DAL;

namespace DIAGEO.BLL
{
    public class PlatformMemberBLL
    {
        public string SelectOneByConsumerId(int consumerId)
        {
            PlatformMemberDAL platformMemberDAL;

            try
            {
                platformMemberDAL = new PlatformMemberDAL();
                platformMemberDAL.ConsumerId = consumerId;
                platformMemberDAL.SelectOneByConsumerId();
                return platformMemberDAL.MemberCode.ToString();
            }
            catch (Exception ex)
            {

                throw new Exception("PlatformMemberBLL::SelectOneByConsumerId::Error occured." + ex.Message);
            }
        }
    }
}
