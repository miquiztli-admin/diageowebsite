﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DIAGEO.DAL;
using DIAGEO.DAL.GDAL;
using DIAGEO.COMMON;

namespace DIAGEO.BLL
{
    public class OutletZoneBLL
    {
        public DataTable SelectActiveOutletZone(string active)
        {
            OutletZoneDAL outletZoneDAL;

            try
            {
                outletZoneDAL = new OutletZoneDAL();
                return outletZoneDAL.SelectActiveOutletZone(active);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
