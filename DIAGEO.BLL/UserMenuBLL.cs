﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DIAGEO.DAL;
using DIAGEO.DAL.GDAL;

namespace DIAGEO.BLL
{
    public class UserMenuBLL
    {
        public DataTable SelectAll()
        {
            UserMenuDAL userMenuDAL;

            try
            {
                userMenuDAL = new UserMenuDAL();
                return userMenuDAL.SelectAll();
            }
            catch (Exception ex)
            {                
                throw new Exception(ex.Message);
            }

        }

        public DataTable SelectAllMenu(int groupId)
        {
            UserMenuDAL userMenuDAL;

            try
            {
                userMenuDAL = new UserMenuDAL();
                return userMenuDAL.SelectAllMenu(groupId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
