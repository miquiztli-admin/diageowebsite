﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using System.Data;

namespace DIAGEO.BLL
{
    public class SurveyTypeBLL 
    {
        public DataTable SelectOne(int surveyTypeId)
        {
            SurveyTypeDAL surveyTypeDAL;

            try
            {
                surveyTypeDAL = new SurveyTypeDAL();
                surveyTypeDAL.SurveyTypeId = surveyTypeId;
                return surveyTypeDAL.SelectOne();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectOneBYSurveyTypeCodeOrName(string surveyTypeCode, string surveyTypeName)
        {
            SurveyTypeDAL surveyTypeDAL;

            try
            {
                surveyTypeDAL = new SurveyTypeDAL();
                return surveyTypeDAL.SelectOneBYSurveyTypeCodeOrName(surveyTypeCode, surveyTypeName);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable Search(string surveyTypeCode, string surveyTypeName, string active, int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            SurveyTypeDAL surveyTypeDAL;

            try
            {
                surveyTypeDAL = new SurveyTypeDAL();
                return surveyTypeDAL.Search(surveyTypeCode, surveyTypeName, active, startRowIndex, maximumRows, sortExpression, isCount);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool UpdateSearch(string surveyTypeId, string surveyTypeCode, string surveyTypeName, string active)
        {
            SurveyTypeDAL surveyTypeDAL;

            try
            {
                surveyTypeDAL = new SurveyTypeDAL();
                surveyTypeDAL.SurveyTypeId = int.Parse(surveyTypeId);
                surveyTypeDAL.Code = surveyTypeCode;
                surveyTypeDAL.Name = surveyTypeName;
                surveyTypeDAL.Active = active;
                surveyTypeDAL.UpdateSearch();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Insert(string surveyTypeCode, string surveyTypeName, string active)
        {
            SurveyTypeDAL surveyTypeDAL;

            try
            {
                surveyTypeDAL = new SurveyTypeDAL();
                surveyTypeDAL.Code = surveyTypeCode;
                surveyTypeDAL.Name = surveyTypeName;
                surveyTypeDAL.Active = active;
                surveyTypeDAL.Flag = "T";
                surveyTypeDAL.Insert();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }       

    }
}
