﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using System.Data;

namespace DIAGEO.BLL
{
    public class OutboundTypeBLL
    {
        public DataTable SelectActiveOutboundType(string active)
        {
            OutboundTypeDAL outboundTypeDAL;

            try
            {
                outboundTypeDAL = new OutboundTypeDAL();
                outboundTypeDAL.Active = active;
                return outboundTypeDAL.SelectActiveOutboundType();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        } 

        public DataTable SelectOne(int outboundTypeId)
        {
            OutboundTypeDAL outboundTypeDAL;

            try
            {
                outboundTypeDAL = new OutboundTypeDAL();
                outboundTypeDAL.OutboundTypeId = outboundTypeId;
                return outboundTypeDAL.SelectOne();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectOneBYOutboundTypeName(string outboundTypeName)
        {
            OutboundTypeDAL outboundTypeDAL;

            try
            {
                outboundTypeDAL = new OutboundTypeDAL();
                return outboundTypeDAL.SelectOneBYOutboundTypeName(outboundTypeName);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable Search(string outboundTypeName, string active, int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            OutboundTypeDAL outboundTypeDAL;

            try
            {
                outboundTypeDAL = new OutboundTypeDAL();

                return outboundTypeDAL.Search(outboundTypeName, active, startRowIndex, maximumRows, sortExpression, isCount);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool UpdateSearch(string outboundTypeId, string outboundTypeName, string format, string active, string userIDLogin)
        {
            OutboundTypeDAL outboundTypeDAL;

            try
            {
                outboundTypeDAL = new OutboundTypeDAL();
                outboundTypeDAL.OutboundTypeId = int.Parse(outboundTypeId);
                outboundTypeDAL.Name = outboundTypeName;
                outboundTypeDAL.Format = format;
                outboundTypeDAL.Active = active;
                outboundTypeDAL.UpdateDate = DateTime.Now;
                outboundTypeDAL.UpdateSearch(userIDLogin);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Insert(string outboundTypeName, string format, string active, string userIDLogin)
        {
            OutboundTypeDAL outboundTypeDAL;

            try
            {
                outboundTypeDAL = new OutboundTypeDAL();
                outboundTypeDAL.Name = outboundTypeName;
                outboundTypeDAL.Format = format;
                outboundTypeDAL.Active = active;
                outboundTypeDAL.CreateBy = userIDLogin;
                outboundTypeDAL.CreateDate = DateTime.Now;
                outboundTypeDAL.UpdateBy = userIDLogin;
                outboundTypeDAL.UpdateDate = DateTime.Now;
                outboundTypeDAL.Insert();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    }
}
