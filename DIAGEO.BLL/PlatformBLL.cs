﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DIAGEO.DAL;
using DIAGEO.DAL.GDAL;
using DIAGEO.COMMON;

namespace DIAGEO.BLL
{
    public class PlatformBLL  
    {
        public DataTable SelectActivePlatform(string active)
        {
            PlatformDAL platformDAL;

            try
            {
                platformDAL = new PlatformDAL();
                return platformDAL.SelectActivePlatform(active);
            }
            catch (Exception ex)
            {                
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectOne(int platformId)
        {
            PlatformDAL platformDAL;

            try
            {
                platformDAL = new PlatformDAL();
                platformDAL.PlatformId = platformId;
                return platformDAL.SelectOne();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectOneBYPlatformCodeOrPlatformName(string platformCode, string platformName)
        {
            PlatformDAL platformDAL;

            try
            {
                platformDAL = new PlatformDAL();
                return platformDAL.SelectOneBYPlatformCodeOrPlatformName(platformCode, platformName);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable Search(string platformCode, string platformName, string active, int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            PlatformDAL platformDAL;

            try
            {
                platformDAL = new PlatformDAL();

                return platformDAL.Search(platformCode, platformName, active, startRowIndex, maximumRows, sortExpression, isCount);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool UpdateSearch(string platformId, string platformCode, string platformName, string active, string userIDLogin)
        {
            PlatformDAL platformDAL;

            try
            {
                string TempDateTime;
                DateTime TempDate = DateTime.Now;

                TempDateTime = TempDate.ToString();
                TempDateTime = TempDateTime.Substring(0, 10);

                platformDAL = new PlatformDAL();
                platformDAL.PlatformId = int.Parse(platformId);
                platformDAL.Code = platformCode;
                platformDAL.Name = platformName;
                platformDAL.Active = active;
                platformDAL.UpdateDate = TempDateTime;
                platformDAL.UpdateSearch(userIDLogin);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Insert(string platformCode, string platformName, string active, string userIDLogin)
        {
            PlatformDAL platformDAL;

            try
            {
                string TempDateTime;
                DateTime TempDate = DateTime.Now;

                TempDateTime = TempDate.ToString();
                TempDateTime = TempDateTime.Substring(0,10);

                platformDAL = new PlatformDAL();
                platformDAL.Code = platformCode;
                platformDAL.Name = platformName;
                platformDAL.Active = active;
                platformDAL.CreateBy = userIDLogin;
                platformDAL.CreateDate = DateTime.Now;
                platformDAL.UpdateBy = userIDLogin;
                platformDAL.UpdateDate = TempDateTime;
                platformDAL.Insert();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectActivePlatformOrderByName(string active)
        {
            try
            {
                PlatformDAL dalPlatform = new PlatformDAL();
                return dalPlatform.SelectActivePlatformOrderByName(active);
            }
            catch (Exception ex)
            {
                throw new Exception("SelectActivePlatformOrderByName :: " + ex.Message);
            }
        }
    }
}
