﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using DIAGEO.DAL.GDAL;
using DIAGEO.COMMON;

namespace DIAGEO.BLL
{
    public class UserGroupBLL
    {

        public DataTable SelectAll()
        {
            UserGroupDAL userGroupDAL;

            try
            {
                userGroupDAL = new UserGroupDAL();
                return userGroupDAL.SelectAll();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectOne(int groupID)
        {
            UserGroupDAL userGroupDAL;

            try
            {
                userGroupDAL = new UserGroupDAL();
                userGroupDAL.GroupId = groupID;
                return userGroupDAL.SelectOne();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectOneBYGroupName(string nameGroup)
        {
            UserGroupDAL userGroupDAL;

            try
            {
                userGroupDAL = new UserGroupDAL();
                return userGroupDAL.SelectOneBYGroupName(nameGroup);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        } 

        public DataTable SelectAllByActiveStatus(string active)
        {
            UserGroupDAL userGroupDAL;

            try
            {
                userGroupDAL = new UserGroupDAL();
                return userGroupDAL.SelectAllByActiveStatus(active);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable Search(string name, string active, int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            UserGroupDAL userGroupDAL;
            DataTable dtUserGroup;

            try
            {
                userGroupDAL = new UserGroupDAL();
                dtUserGroup = userGroupDAL.Search(name, active, startRowIndex, maximumRows, sortExpression, isCount);

                return dtUserGroup;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Insert(string name, string active, string userIDLogin)
        {
            UserGroupDAL userGroupDAL;

            try
            {
                userGroupDAL = new UserGroupDAL();
                userGroupDAL.Name = name;
                userGroupDAL.Active = active;
                userGroupDAL.CreateBy = userIDLogin;
                userGroupDAL.CreateDate = DateTime.Now;
                userGroupDAL.UpdateBy = userIDLogin;
                userGroupDAL.UpdateDate = DateTime.Now;
                userGroupDAL.Insert();
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool UpdateSearch(string groupID, string name, string active, string userIDLogin)
        {
            UserGroupDAL userGroupDAL;

            try
            {
                userGroupDAL = new UserGroupDAL();
                userGroupDAL.GroupId = int.Parse(groupID);
                userGroupDAL.Name = name;
                userGroupDAL.Active = active;
                userGroupDAL.UpdateSearch(userIDLogin);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }        

    }
}
