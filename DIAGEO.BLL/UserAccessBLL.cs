﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DIAGEO.DAL;

namespace DIAGEO.BLL
{
    public class UserAccessBLL
    {
        public DataTable SelectAll()
        {
            UserAccessDAL userAccessDAL;

            try
            {
                userAccessDAL = new UserAccessDAL();
                return userAccessDAL.SelectAll();
            }
            catch (Exception ex)
            {                
                throw new Exception(ex.Message);
            }

        }

        public DataTable Search(string userGroupId, int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            UserAccessDAL userAccessDAL;
            DataTable dtUserAccess;

            try
            {
                userAccessDAL = new UserAccessDAL();
                dtUserAccess = userAccessDAL.Search(userGroupId, startRowIndex, maximumRows, sortExpression, isCount);

                return dtUserAccess;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool Update(DataTable dt, string userIDLogin, string gvGroupID)
        {
            UserAccessDAL userAccessDAL;
            DIAGEO.DAL.GDAL.ConnectionProvider conp = new DIAGEO.DAL.GDAL.ConnectionProvider();
            DataRow Row;

            try
            {
                userAccessDAL = new UserAccessDAL();
                userAccessDAL.MainConnectionProvider = conp;
                conp.OpenConnection();
                conp.BeginTransaction("UserAccess");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Row = dt.Rows[i];

                    if (Row["GroupID"].ToString() == "&nbsp;")
                    {
                        userAccessDAL.MenuId = int.Parse(Row["MenuID"].ToString());
                        userAccessDAL.GroupId = int.Parse(gvGroupID);
                        userAccessDAL.AccessView = Row["CanView"].ToString();
                        userAccessDAL.AccessCreate = Row["CanCreate"].ToString();
                        userAccessDAL.AccessModify = Row["CanModify"].ToString();
                        userAccessDAL.AccessDelete = Row["CanDelete"].ToString();
                        userAccessDAL.AccessExport = Row["CanExport"].ToString();
                        userAccessDAL.AccessPrint = Row["CanPrint"].ToString();
                        userAccessDAL.Active = "T";
                        userAccessDAL.CreateDate = DateTime.Now;
                        userAccessDAL.CreateBy = userIDLogin;
                        userAccessDAL.UpdateDate = DateTime.Now;
                        userAccessDAL.UpdateBy = userIDLogin;

                        userAccessDAL.Insert();
                    }
                    else
                    {
                        userAccessDAL.MenuId = int.Parse(Row["MenuID"].ToString());
                        userAccessDAL.GroupId = int.Parse(Row["GroupID"].ToString());
                        userAccessDAL.AccessView = Row["CanView"].ToString();
                        userAccessDAL.AccessCreate = Row["CanCreate"].ToString();
                        userAccessDAL.AccessModify = Row["CanModify"].ToString();
                        userAccessDAL.AccessDelete = Row["CanDelete"].ToString();
                        userAccessDAL.AccessExport = Row["CanExport"].ToString();
                        userAccessDAL.AccessPrint = Row["CanPrint"].ToString();
                        userAccessDAL.Active = "T";
                        userAccessDAL.CreateDate = DateTime.Now;
                        userAccessDAL.CreateBy = userIDLogin;
                        userAccessDAL.UpdateDate = DateTime.Now;
                        userAccessDAL.UpdateBy = userIDLogin;

                        userAccessDAL.UpdateUserAccess();
                    }
                }
                
                conp.CommitTransaction();

                return true;
            }
            catch (Exception ex)
            {
                conp.RollbackTransaction("UserAccess");
                throw new Exception(ex.Message);
            }
            finally 
            {
                conp.CloseConnection(false);
                conp.Dispose();
            }
        }

        public DataTable SelectAllMenu(int groupId)
        {
            UserAccessDAL userAccessDAL;

            try
            {
                userAccessDAL = new UserAccessDAL();
                return userAccessDAL.SelectAllMenu(groupId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

    }
}
