﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL;
using DIAGEO.DAL.GDAL;

namespace DIAGEO.BLL
{
    public class UserActivityLogBLL
    {

        public void Insert(int UserID, int UserGroupID, int MenuID, string FuncName)
        {

            UserActivityLogDAL userActivityLogDAL;

            try
            {

                userActivityLogDAL = new UserActivityLogDAL();

                Insert(UserID, UserGroupID, MenuID, FuncName, null);
            }
            catch (Exception ex)
            {
                throw new Exception("ActivityLogBLL.Insert.Overload1 :: " + ex.Message);
            }
        }

        public void Insert(int UserID, int UserGroupID, int MenuID, string FuncName, ConnectionProvider conProv)
        {
            ConnectionProvider conBase = new ConnectionProvider();
            UserActivityLogDAL userActivityLogDAL;

            try
            {

                userActivityLogDAL = new UserActivityLogDAL();

                if (conProv != null)
                {
                    userActivityLogDAL.MainConnectionProvider = conProv;

                    userActivityLogDAL.UserId = UserID;
                    userActivityLogDAL.UserGroupId = UserGroupID;
                    userActivityLogDAL.MenuId = MenuID;
                    userActivityLogDAL.ComputerName = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                    userActivityLogDAL.DateTime = DateTime.Now;
                    userActivityLogDAL.Description = "Use " + FuncName;
                    userActivityLogDAL.Flag = "";

                    userActivityLogDAL.Insert();
                }
                else
                {
                    conBase.OpenConnection();
                    conBase.BeginTransaction("Insert");
                    userActivityLogDAL.MainConnectionProvider = conBase;

                    userActivityLogDAL.UserId = UserID;
                    userActivityLogDAL.UserGroupId = UserGroupID;
                    userActivityLogDAL.MenuId = MenuID;
                    userActivityLogDAL.ComputerName = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                    userActivityLogDAL.DateTime = DateTime.Now;
                    userActivityLogDAL.Description = "Use " + FuncName;
                    userActivityLogDAL.Flag = "";

                    userActivityLogDAL.Insert();
                    conBase.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                if (conProv == null)
                    conBase.RollbackTransaction("Insert");
                throw new Exception("ActivityLogBLL.Insert.Overload2 :: " + ex.Message);
            }
            finally
            {
                if (conProv == null)
                    conBase.CloseConnection(false);
                conBase.Dispose();
            }
        }

    }
}
