///////////////////////////////////////////////////////////////////////////
// Description: Data Access class for the table 'UserGroup'
// Generated by LLBLGen v1.21.2003.712 Final on: Wednesday, April 11, 2012, 4:44:44 PM
// Because the Base Class already implements IDispose, this class doesn't.
///////////////////////////////////////////////////////////////////////////
using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;

namespace DIAGEO.DAL.GDAL
{
	/// <summary>
	/// Purpose: Data Access class for the table 'UserGroup'.
	/// </summary>
	public class UserGroup : DBInteractionBase
	{
		#region Class Member Declarations
			private SqlString		_active;
			private SqlDateTime		_createDate, _updateDate;
			private SqlInt32		_groupId;
			private SqlString		_updateBy, _flag, _createBy, _name;
		#endregion


		/// <summary>
		/// Purpose: Class constructor.
		/// </summary>
		public UserGroup()
		{
			// Nothing for now.
		}


		/// <summary>
		/// Purpose: Insert method. This method will insert one new row into the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>Name. May be SqlString.Null</LI>
		///		 <LI>Active. May be SqlString.Null</LI>
		///		 <LI>CreateDate. May be SqlDateTime.Null</LI>
		///		 <LI>CreateBy. May be SqlString.Null</LI>
		///		 <LI>UpdateDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdateBy. May be SqlString.Null</LI>
		///		 <LI>Flag. May be SqlString.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>GroupId</LI>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Insert()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[Sp_G_UserGroup_Insert]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@name", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _name));
				cmdToExecute.Parameters.Add(new SqlParameter("@active", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _active));
				cmdToExecute.Parameters.Add(new SqlParameter("@createDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@createBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@updateDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updateDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@updateBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updateBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@flag", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _flag));
				cmdToExecute.Parameters.Add(new SqlParameter("@groupId", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _groupId));
				cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_groupId = Convert.ToInt32(cmdToExecute.Parameters["@groupId"].Value.ToString());
				_errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'Sp_G_UserGroup_Insert' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
				throw new Exception("UserGroup::Insert::Error occured." + ex.Message );
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Update method. This method will Update one existing row in the database.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>GroupId</LI>
		///		 <LI>Name. May be SqlString.Null</LI>
		///		 <LI>Active. May be SqlString.Null</LI>
		///		 <LI>CreateDate. May be SqlDateTime.Null</LI>
		///		 <LI>CreateBy. May be SqlString.Null</LI>
		///		 <LI>UpdateDate. May be SqlDateTime.Null</LI>
		///		 <LI>UpdateBy. May be SqlString.Null</LI>
		///		 <LI>Flag. May be SqlString.Null</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Update()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[Sp_G_UserGroup_Update]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@groupId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _groupId));
				cmdToExecute.Parameters.Add(new SqlParameter("@name", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _name));
				cmdToExecute.Parameters.Add(new SqlParameter("@active", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _active));
				cmdToExecute.Parameters.Add(new SqlParameter("@createDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@createBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _createBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@updateDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updateDate));
				cmdToExecute.Parameters.Add(new SqlParameter("@updateBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _updateBy));
				cmdToExecute.Parameters.Add(new SqlParameter("@flag", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _flag));
				cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'Sp_G_UserGroup_Update' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("UserGroup::Update::Error occured." + ex.Message);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Delete method. This method will Delete one existing row in the database, based on the Primary Key.
		/// </summary>
		/// <returns>True if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>GroupId</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override bool Delete()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[Sp_G_UserGroup_Delete]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@groupId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _groupId));
				cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				_rowsAffected = cmdToExecute.ExecuteNonQuery();
				_errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'Sp_G_UserGroup_Delete' reported the ErrorCode: " + _errorCode);
				}

				return true;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("UserGroup::Delete::Error occured." + ex.Message);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
			}
		}


		/// <summary>
		/// Purpose: Select method. This method will Select one existing row from the database, based on the Primary Key.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties needed for this method: 
		/// <UL>
		///		 <LI>GroupId</LI>
		/// </UL>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		///		 <LI>GroupId</LI>
		///		 <LI>Name</LI>
		///		 <LI>Active</LI>
		///		 <LI>CreateDate</LI>
		///		 <LI>CreateBy</LI>
		///		 <LI>UpdateDate</LI>
		///		 <LI>UpdateBy</LI>
		///		 <LI>Flag</LI>
		/// </UL>
		/// Will fill all properties corresponding with a field in the table with the value of the row selected.
		/// </remarks>
		public override DataTable SelectOne()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[Sp_G_UserGroup_SelectOne]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("UserGroup");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@groupId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, _groupId));
				cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'Sp_G_UserGroup_SelectOne' reported the ErrorCode: " + _errorCode);
				}

				if(toReturn.Rows.Count > 0)
				{
					_groupId = (Int32)toReturn.Rows[0]["groupId"];
					_name = toReturn.Rows[0]["name"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["name"];
					_active = toReturn.Rows[0]["active"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["active"];
					_createDate = toReturn.Rows[0]["createDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["createDate"];
					_createBy = toReturn.Rows[0]["createBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["createBy"];
					_updateDate = toReturn.Rows[0]["updateDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["updateDate"];
					_updateBy = toReturn.Rows[0]["updateBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["updateBy"];
					_flag = toReturn.Rows[0]["flag"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["flag"];
				}
				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("UserGroup::SelectOne::Error occured." + ex.Message);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		/// <summary>
		/// Purpose: SelectAll method. This method will Select all rows from the table.
		/// </summary>
		/// <returns>DataTable object if succeeded, otherwise an Exception is thrown. </returns>
		/// <remarks>
		/// Properties set after a succesful call of this method: 
		/// <UL>
		///		 <LI>ErrorCode</LI>
		/// </UL>
		/// </remarks>
		public override DataTable SelectAll()
		{
			SqlCommand	cmdToExecute = new SqlCommand();
			cmdToExecute.CommandText = "dbo.[Sp_G_UserGroup_SelectAll]";
			cmdToExecute.CommandType = CommandType.StoredProcedure;
			DataTable toReturn = new DataTable("UserGroup");
			SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

			// Use base class' connection object
			cmdToExecute.Connection = _mainConnection;

			try
			{
				cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

				if(_mainConnectionIsCreatedLocal)
				{
					// Open connection.
					_mainConnection.Open();
				}
				else
				{
					if(_mainConnectionProvider.IsTransactionPending)
					{
						cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
					}
				}

				// Execute query.
				adapter.Fill(toReturn);
				_errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

				if(_errorCode != (int)LLBLError.AllOk)
				{
					// Throw error.
					throw new Exception("Stored Procedure 'Sp_G_UserGroup_SelectAll' reported the ErrorCode: " + _errorCode);
				}

				return toReturn;
			}
			catch(Exception ex)
			{
				// some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("UserGroup::SelectAll::Error occured." + ex.Message);
			}
			finally
			{
				if(_mainConnectionIsCreatedLocal)
				{
					// Close connection.
					_mainConnection.Close();
				}
				cmdToExecute.Dispose();
				adapter.Dispose();
			}
		}


		#region Class Property Declarations
		public SqlInt32 GroupId
		{
			get
			{
				return _groupId;
			}
			set
			{
				SqlInt32 groupIdTmp = (SqlInt32)value;
				if(groupIdTmp.IsNull)
				{
					throw new ArgumentOutOfRangeException("GroupId", "GroupId can't be NULL");
				}
				_groupId = value;
			}
		}


		public SqlString Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}


		public SqlString Active
		{
			get
			{
				return _active;
			}
			set
			{
				_active = value;
			}
		}


		public SqlDateTime CreateDate
		{
			get
			{
				return _createDate;
			}
			set
			{
				_createDate = value;
			}
		}


		public SqlString CreateBy
		{
			get
			{
				return _createBy;
			}
			set
			{
				_createBy = value;
			}
		}


		public SqlDateTime UpdateDate
		{
			get
			{
				return _updateDate;
			}
			set
			{
				_updateDate = value;
			}
		}


		public SqlString UpdateBy
		{
			get
			{
				return _updateBy;
			}
			set
			{
				_updateBy = value;
			}
		}


		public SqlString Flag
		{
			get
			{
				return _flag;
			}
			set
			{
				_flag = value;
			}
		}
		#endregion
	}
}
