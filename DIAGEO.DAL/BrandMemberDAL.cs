﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL.GDAL;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Data;

namespace DIAGEO.DAL
{
    public class BrandMemberDAL : BrandMember
    {
        public DataTable SelectByConsumerIdAndBrandId()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[Sp_BrandMember_SelectByConsumerIdAndBrandId]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("BrandMember");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@consumerId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, ConsumerId));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandId", SqlDbType.VarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandId));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'Sp_G_BrandMember_SelectOne' reported the ErrorCode: " + _errorCode);
                }

                if (toReturn.Rows.Count > 0)
                {
                    MemberId = (Int32)toReturn.Rows[0]["memberId"];
                    ConsumerId = toReturn.Rows[0]["consumerId"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["consumerId"];
                    BrandId = toReturn.Rows[0]["brandId"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["brandId"];
                    MemberCode = toReturn.Rows[0]["memberCode"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["memberCode"];
                    RegisterOutletId = toReturn.Rows[0]["registerOutletId"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["registerOutletId"];
                    RegisterDate = toReturn.Rows[0]["registerDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["registerDate"];
                    LastVisitDate = toReturn.Rows[0]["lastVisitDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["lastVisitDate"];
                    CreateDate = toReturn.Rows[0]["createDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["createDate"];
                    CreateBy = toReturn.Rows[0]["createBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["createBy"];
                    UpdateDate = toReturn.Rows[0]["updateDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["updateDate"];
                    UpdateBy = toReturn.Rows[0]["updateBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["updateBy"];
                    Flag = toReturn.Rows[0]["flag"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["flag"];
                }
                return toReturn;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("BrandMember::SelectOne::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

        public bool InsertBrandMemberByIpad()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[Sp_BrandMember_InsertBrandMemberByIpad]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@consumerId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, ConsumerId));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandId", SqlDbType.VarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandId));
                cmdToExecute.Parameters.Add(new SqlParameter("@memberCode", SqlDbType.VarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, MemberCode));
                cmdToExecute.Parameters.Add(new SqlParameter("@registerOutletId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, RegisterOutletId));
                cmdToExecute.Parameters.Add(new SqlParameter("@registerDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, RegisterDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@lastVisitDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, LastVisitDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@createDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, CreateDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@createBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, CreateBy));
                cmdToExecute.Parameters.Add(new SqlParameter("@flag", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Flag));
                cmdToExecute.Parameters.Add(new SqlParameter("@memberId", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, MemberId));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                //_rowsAffected = cmdToExecute.ExecuteNonQuery();
                //MemberId = Convert.ToInt32(cmdToExecute.Parameters["@memberId"].Value.ToString());
                //_errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                MemberId = Convert.ToInt32(cmdToExecute.ExecuteScalar().ToString());
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'Sp_BrandMember_InsertBrandMemberByIpad' reported the ErrorCode: " + _errorCode);
                }

                return true;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("BrandMemberDAL::InsertBrandMemberByIpad::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }

        public bool UpdateBrandMemberByConsumerIdAndBrandId()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[Sp_BrandMember_UpdateBrandMemberByConsumerIdAndBrandId]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@consumerId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, ConsumerId));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandId", SqlDbType.VarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandId));
                //cmdToExecute.Parameters.Add(new SqlParameter("@memberCode", SqlDbType.VarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, MemberCode));
                cmdToExecute.Parameters.Add(new SqlParameter("@lastVisitDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, LastVisitDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@updateDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, UpdateDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@updateBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, UpdateBy));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                _rowsAffected = cmdToExecute.ExecuteNonQuery();
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'Sp_BrandMember_UpdateBrandMemberByConsumerIdAndBrandId' reported the ErrorCode: " + _errorCode);
                }

                return true;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("BrandMemberDAL::UpdateBrandMemberByConsumerIdAndBrandId::Error occured.", ex);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }

    }
}
