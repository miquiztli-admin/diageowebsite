using System;
using System.Data;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using DIAGEO.DAL.GDAL;

namespace DIAGEO.DAL
{
    public class OutboundEventDAL : OutboundEvent
    {
        public DataTable SelectOutboundEventMaster()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "Sp_OutboundEventMaster_SelectOutboundEventMaster";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("OutboundEventMaster");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_OutboundEventMaster_SelectOutboundEventMaster' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("OutboundEventDAL::SelectOutboundEventMaster::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

        public DataTable SelectOneBYOutboundEventName(string name)
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "Sp_OutboundEvent_SelectOneBYOutboundEventName";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("OutboundEvent");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@EventName", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, name));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_OutboundEvent_SelectOneBYOutboundEventName' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("OutboundEvent::SelectOneBYOutboundEventName::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

        public DataTable SelectBYEventID(int eventID)
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "Sp_OutboundEvent_SelectByEventID";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("OutboundEvent");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@eventId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, eventID));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_OutboundEvent_SelectByEventID' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("OutboundEventDAL::SelectByEventID::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }
        
        public DataTable Search(string eventName, int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "Sp_OutboundEvent_Search";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("OutboundEvent");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@EventName", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, eventName));
                cmdToExecute.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, startRowIndex));
                cmdToExecute.Parameters.Add(new SqlParameter("@MaximumRows", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, maximumRows));
                cmdToExecute.Parameters.Add(new SqlParameter("@SortExpression", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, sortExpression));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));
                cmdToExecute.Parameters.Add(new SqlParameter("@ISCount", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, isCount));

                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_OutboundEvent_Search' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("OutboundEvent::Search::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

        public DataTable SearchEventBirthDay(string eventName, string active, int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "Sp_OutboundEventBirthDay_Search";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("OutboundEventBirthDay");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@EventName", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, eventName));
                cmdToExecute.Parameters.Add(new SqlParameter("@Active", SqlDbType.Char, 1, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, active));
                cmdToExecute.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, startRowIndex));
                cmdToExecute.Parameters.Add(new SqlParameter("@MaximumRows", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, maximumRows));
                cmdToExecute.Parameters.Add(new SqlParameter("@SortExpression", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, sortExpression));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));
                cmdToExecute.Parameters.Add(new SqlParameter("@ISCount", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, isCount));

                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_OutboundEventBirthDay_Search' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("OutboundEvent::SearchEventBirthDay::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

        public DataTable SearchConsumerDataGroupTOGlobalTemp(int eventId,
	    string senderName, int platformId, string brandPreferenceCondition,
	    string gender, int ageFrom, int ageTo, int monthOFBirthDate,
	    int provinceId, int lastTimeOpenEmail, int receiveEmail, string emailActive,
	    string mobileActive, decimal openEmailRate, string dataGroupName)
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "Sp_Consumer_SearchConsumerDataGroupTOGlobalTemp";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("SearchConsumerDataGroupTOGlobalTemp");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@EventId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, eventId));
                cmdToExecute.Parameters.Add(new SqlParameter("@SenderName", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, senderName));
                cmdToExecute.Parameters.Add(new SqlParameter("@PlatformId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, platformId));
                cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreferenceCondition", SqlDbType.NVarChar, 4000, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, brandPreferenceCondition));
                cmdToExecute.Parameters.Add(new SqlParameter("@Gender", SqlDbType.VarChar, 10, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, gender));
                cmdToExecute.Parameters.Add(new SqlParameter("@AgeFrom", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, ageFrom));
                cmdToExecute.Parameters.Add(new SqlParameter("@AgeTo", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, ageTo));
                cmdToExecute.Parameters.Add(new SqlParameter("@MonthOFBirthDate", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, monthOFBirthDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@ProvinceId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, provinceId));                
                cmdToExecute.Parameters.Add(new SqlParameter("@LastTimeOpenEmail", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, lastTimeOpenEmail));                
                cmdToExecute.Parameters.Add(new SqlParameter("@ReceiveEmail", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, receiveEmail));                
                cmdToExecute.Parameters.Add(new SqlParameter("@EmailActive", SqlDbType.Char, 1, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, emailActive));                
                cmdToExecute.Parameters.Add(new SqlParameter("@MobileActive", SqlDbType.Char, 1, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, mobileActive));                
                cmdToExecute.Parameters.Add(new SqlParameter("@OpenEmailRate", SqlDbType.Decimal, 10, ParameterDirection.Input, false, 10, 2, "", DataRowVersion.Proposed, mobileActive));
                cmdToExecute.Parameters.Add(new SqlParameter("@DataGroupName", SqlDbType.NVarChar, 4000, ParameterDirection.Input, false, 10, 2, "", DataRowVersion.Proposed, dataGroupName));                
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));
               
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_Consumer_SearchConsumerDataGroupTOGlobalTemp' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("OutboundEvent::SearchConsumerDataGroupTOGlobalTemp::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

        //public DataTable SearchEventSmarttouch(string type,
        //string outletID, DateTime dateFrom, DateTime dateTo,
        //string lastActivityNotOver, string visitFrequency)
        //{
        //    SqlCommand cmdToExecute = new SqlCommand();
        //    cmdToExecute.CommandText = "Sp_Consumer_SearchConsumerDataGroupTOGlobalTemp";
        //    cmdToExecute.CommandType = CommandType.StoredProcedure;
        //    DataTable toReturn = new DataTable("SearchConsumerDataGroupTOGlobalTemp");
        //    SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
        //    cmdToExecute.Connection = _mainConnection;

        //    try
        //    {
        //        cmdToExecute.Parameters.Add(new SqlParameter("@EventId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, eventId));
        //        cmdToExecute.Parameters.Add(new SqlParameter("@SenderName", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, senderName));
        //        cmdToExecute.Parameters.Add(new SqlParameter("@PlatformId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, platformId));
        //        cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreferenceCondition", SqlDbType.NVarChar, 4000, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, brandPreferenceCondition));
        //        cmdToExecute.Parameters.Add(new SqlParameter("@Gender", SqlDbType.VarChar, 10, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, gender));
        //        cmdToExecute.Parameters.Add(new SqlParameter("@AgeFrom", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, ageFrom));
        //        cmdToExecute.Parameters.Add(new SqlParameter("@AgeTo", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, ageTo));
        //        cmdToExecute.Parameters.Add(new SqlParameter("@MonthOFBirthDate", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, monthOFBirthDate));
        //        cmdToExecute.Parameters.Add(new SqlParameter("@ProvinceId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, provinceId));
        //        cmdToExecute.Parameters.Add(new SqlParameter("@LastTimeOpenEmail", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, lastTimeOpenEmail));
        //        cmdToExecute.Parameters.Add(new SqlParameter("@ReceiveEmail", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, receiveEmail));
        //        cmdToExecute.Parameters.Add(new SqlParameter("@EmailActive", SqlDbType.Char, 1, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, emailActive));
        //        cmdToExecute.Parameters.Add(new SqlParameter("@MobileActive", SqlDbType.Char, 1, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, mobileActive));
        //        cmdToExecute.Parameters.Add(new SqlParameter("@OpenEmailRate", SqlDbType.Decimal, 10, ParameterDirection.Input, false, 10, 2, "", DataRowVersion.Proposed, mobileActive));
        //        cmdToExecute.Parameters.Add(new SqlParameter("@DataGroupName", SqlDbType.NVarChar, 4000, ParameterDirection.Input, false, 10, 2, "", DataRowVersion.Proposed, dataGroupName));
        //        cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

        //        if (_mainConnectionIsCreatedLocal)
        //        {
        //            _mainConnection.Open();
        //        }
        //        else
        //        {
        //            if (_mainConnectionProvider.IsTransactionPending)
        //            {
        //                cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
        //            }
        //        }

        //        adapter.Fill(toReturn);
        //        _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

        //        if (_errorCode != (int)LLBLError.AllOk)
        //        {
        //            throw new Exception("Stored Procedure 'Sp_Consumer_SearchConsumerDataGroupTOGlobalTemp' reported the ErrorCode: " + _errorCode);
        //        }

        //        return toReturn;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("OutboundEvent::SearchConsumerDataGroupTOGlobalTemp::Error occured." + ex.Message);
        //    }
        //    finally
        //    {
        //        if (_mainConnectionIsCreatedLocal)
        //        {
        //            _mainConnection.Close();
        //        }
        //        cmdToExecute.Dispose();
        //        adapter.Dispose();
        //    }
        //}

        public bool UpdateSearch(string userIdLogin)
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "Sp_OutboundEvent_UpdateSearch";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@eventId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, EventId));
                cmdToExecute.Parameters.Add(new SqlParameter("@name", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Name));
                cmdToExecute.Parameters.Add(new SqlParameter("@active", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Active));
                cmdToExecute.Parameters.Add(new SqlParameter("@updateBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, userIdLogin));
                cmdToExecute.Parameters.Add(new SqlParameter("@startAge", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, StartAge));
                cmdToExecute.Parameters.Add(new SqlParameter("@endAge", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, EndAge));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBaileys", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBaileys));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBenmore", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBenmore));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwBlack", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwBlack));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwGold", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwGold));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwGreen", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwGreen));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwRed", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwRed));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreSmirnoff", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreSmirnoff));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                _rowsAffected = cmdToExecute.ExecuteNonQuery();
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_OutboundEvent_UpdateSearch' reported the ErrorCode: " + _errorCode);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("OutboundEvent::UpdateSearch::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }

        public bool InsertOutboundEvent()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[Sp_OutboundEvent_Insert]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@eventId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, EventId));
                cmdToExecute.Parameters.Add(new SqlParameter("@dataGroup", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, DataGroup));
                cmdToExecute.Parameters.Add(new SqlParameter("@name", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Name));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwGold", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwGold));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwBlack", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwBlack));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreSmirnoff", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreSmirnoff));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwRed", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwRed));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwGreen", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwGreen));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBenmore", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBenmore));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBaileys", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBaileys));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreAbsolut", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreAbsolut));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBallentine", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBallentine));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBlend", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBlend));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreChivas", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreChivas));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreDewar", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreDewar));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPre100Pipers", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPre100Pipers));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPre100Pipers8Y", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPre100Pipers8Y));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreHennessy", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreHennessy));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreOther", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreOther));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreNotDrink", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreNotDrink));
                cmdToExecute.Parameters.Add(new SqlParameter("@gender", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Gender));
                cmdToExecute.Parameters.Add(new SqlParameter("@validOptInDiageo", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidOptInDiageo));
                cmdToExecute.Parameters.Add(new SqlParameter("@validOptInOutlet", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidOptInOutlet));
                cmdToExecute.Parameters.Add(new SqlParameter("@startAge", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, StartAge));
                cmdToExecute.Parameters.Add(new SqlParameter("@endAge", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, EndAge));
                cmdToExecute.Parameters.Add(new SqlParameter("@emailActive", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, EmailActive));
                cmdToExecute.Parameters.Add(new SqlParameter("@mobileActive", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, MobileActive));
                cmdToExecute.Parameters.Add(new SqlParameter("@active", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Active));
                cmdToExecute.Parameters.Add(new SqlParameter("@includeSMSBounce", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, IncludeSMSBounce));
                cmdToExecute.Parameters.Add(new SqlParameter("@includeEDMBounce", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, IncludeEDMBounce));
                cmdToExecute.Parameters.Add(new SqlParameter("@includeCallcenterBounce", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, IncludeCallcenterBounce));
                cmdToExecute.Parameters.Add(new SqlParameter("@smsflag", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Smsflag));
                cmdToExecute.Parameters.Add(new SqlParameter("@edmflag", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Edmflag));
                cmdToExecute.Parameters.Add(new SqlParameter("@callcenterflag", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Callcenterflag));
                cmdToExecute.Parameters.Add(new SqlParameter("@createDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, CreateDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@createBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, CreateBy));
                cmdToExecute.Parameters.Add(new SqlParameter("@updateDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, UpdateDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@updateBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, UpdateBy));
                cmdToExecute.Parameters.Add(new SqlParameter("@bdayflag", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Bdayflag));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                _rowsAffected = cmdToExecute.ExecuteNonQuery();
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'Sp_OutboundEvent_Insert' reported the ErrorCode: " + _errorCode);
                }

                return true;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("OutboundEvent::Insert::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }

        public bool UpdateOutboundEvent()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[Sp_OutboundEvent_Update]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@eventId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, EventId));
                cmdToExecute.Parameters.Add(new SqlParameter("@dataGroup", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, DataGroup));
                cmdToExecute.Parameters.Add(new SqlParameter("@name", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Name));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwGold", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwGold));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwBlack", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwBlack));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreSmirnoff", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreSmirnoff));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwRed", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwRed));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwGreen", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwGreen));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBenmore", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBenmore));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBaileys", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBaileys));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreAbsolut", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreAbsolut));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBallentine", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBallentine));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBlend", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBlend));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreChivas", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreChivas));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreDewar", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreDewar));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPre100Pipers", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPre100Pipers));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPre100Pipers8Y", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPre100Pipers8Y));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreHennessy", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreHennessy));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreOther", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreOther));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreNotDrink", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreNotDrink));
                cmdToExecute.Parameters.Add(new SqlParameter("@gender", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Gender));
                cmdToExecute.Parameters.Add(new SqlParameter("@validOptInDiageo", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidOptInDiageo));
                cmdToExecute.Parameters.Add(new SqlParameter("@validOptInOutlet", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidOptInOutlet));
                cmdToExecute.Parameters.Add(new SqlParameter("@startAge", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, StartAge));
                cmdToExecute.Parameters.Add(new SqlParameter("@endAge", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, EndAge));
                cmdToExecute.Parameters.Add(new SqlParameter("@emailActive", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, EmailActive));
                cmdToExecute.Parameters.Add(new SqlParameter("@mobileActive", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, MobileActive));
                cmdToExecute.Parameters.Add(new SqlParameter("@active", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Active));
                cmdToExecute.Parameters.Add(new SqlParameter("@includeSMSBounce", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, IncludeSMSBounce));
                cmdToExecute.Parameters.Add(new SqlParameter("@includeEDMBounce", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, IncludeEDMBounce));
                cmdToExecute.Parameters.Add(new SqlParameter("@includeCallcenterBounce", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, IncludeCallcenterBounce));
                cmdToExecute.Parameters.Add(new SqlParameter("@smsflag", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Smsflag));
                cmdToExecute.Parameters.Add(new SqlParameter("@edmflag", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Edmflag));
                cmdToExecute.Parameters.Add(new SqlParameter("@callcenterflag", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Callcenterflag));
                cmdToExecute.Parameters.Add(new SqlParameter("@createDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, CreateDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@createBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, CreateBy));
                cmdToExecute.Parameters.Add(new SqlParameter("@updateDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, UpdateDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@updateBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, UpdateBy));
                cmdToExecute.Parameters.Add(new SqlParameter("@bdayflag", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Bdayflag));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                _rowsAffected = cmdToExecute.ExecuteNonQuery();
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'Sp_G_OutboundEvent_Update' reported the ErrorCode: " + _errorCode);
                }

                return true;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("OutboundEvent::Update::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }

        public bool DeleteByEventId()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[Sp_OutboundEvent_DeleteByEventId]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@eventId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, EventId));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                _rowsAffected = cmdToExecute.ExecuteNonQuery();
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'Sp_OutboundEvent_DeleteByEventId' reported the ErrorCode: " + _errorCode);
                }

                return true;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("OutboundEvent::DeleteByEventId::Error occured :: " + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }

    }
}
