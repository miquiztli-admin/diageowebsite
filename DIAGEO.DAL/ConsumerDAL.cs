﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL.GDAL;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlTypes;

namespace DIAGEO.DAL
{
    public class ConsumerDAL : Consumer
    {
        #region Class Property Declarations

        public String OptInDG { get; set; }

        public String OptInOutlet { get; set; }

        #endregion

        public bool UpdateOutboundCallCenter(int OutboundID, string ResultCode, string UpdateBy, DateTime UpdateDate)
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[Sp_Consumer_UpdateOutboundCallCenter]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@OutboundID", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, OutboundID));
                cmdToExecute.Parameters.Add(new SqlParameter("@ResultCode", SqlDbType.VarChar, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ResultCode));
                cmdToExecute.Parameters.Add(new SqlParameter("@UpdateBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, UpdateBy));
                cmdToExecute.Parameters.Add(new SqlParameter("@UpdateDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, UpdateDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                _rowsAffected = cmdToExecute.ExecuteNonQuery();
                _errorCode = (SqlInt32)cmdToExecute.Parameters["@ErrorCode"].Value;

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'Sp_Consumer_UpdateOutboundCallCenter' reported the ErrorCode: " + _errorCode);
                }

                return true;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("Consumer::UpdateOutboundCallCenter::Error occured::" + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }

        public DataTable ExtractData(string PlatformId, bool BrandPreBaileys, bool BrandPreBenmore, bool BrandPreJwBlack,
        bool BrandPreJwGold, bool BrandPreJwGreen, bool BrandPreJwRed, bool BrandPreSmirnoff, bool BrandPreNonAbsolute,
        bool BrandPreNonBallentine, bool BrandPreNonBlend, bool BrandPreNonChivas, bool BrandPreNonDewar, bool BrandPreNon100Pipers,
        bool BrandPreNon100Pipers8yrs, string Survey4aBaileys, string Survey4aBenmore, string Survey4aJwBlack, string Survey4aJwGold,
        string Survey4aJwGreen, string Survey4aJwRed, string Survey4aSmirnoff, string SurveyP4wNonAbsolute, string SurveyP4wNonBallentine,
        string SurveyP4wNonBlend, string SurveyP4wNonChivas, string SurveyP4wNonDewar, string SurveyP4wNon100Pipers, string SurveyP4wNon100Pipers8yrs,
        string Gender, string GenderFrom, string GenderTo,
        DateTime? DateOfBirthFrom, DateTime? DateOfBirthTo, string Province, string RegisterOutlet, DateTime? RegisterDateFrom, DateTime? RegisterDateTo,
        string LastVisitOutlet, DateTime? LastVisitDateFrom, DateTime? LastVisitDateTo,
        bool ActiveEmail, bool ActiveMobile, bool ActiveCallCenter, string SubScribePlatformID, int IsCount, int StartRowIndex, int MaximumRowIndex)
        {
            SqlCommand cmdToExecute = new SqlCommand();
            DataTable toReturn = new DataTable("ExtractData");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
            cmdToExecute.CommandText = "Sp_Consumer_ExtractData";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            cmdToExecute.Connection = _mainConnection;

            try
            {

                cmdToExecute.Parameters.Add(new SqlParameter("@PlatformId", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, PlatformId));
                cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreBaileys", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(BrandPreBaileys)));
                cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreBenmore", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(BrandPreBenmore)));
                cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreJwBlack", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(BrandPreJwBlack)));
                cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreJwGold", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(BrandPreJwGold)));
                cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreJwGreen", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(BrandPreJwGreen)));
                cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreJwRed", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(BrandPreJwRed)));
                cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreSmirnoff", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(BrandPreSmirnoff)));

                cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreNonAbsolute", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(BrandPreNonAbsolute)));
                cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreNonBallentine", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(BrandPreNonBallentine)));
                cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreNonBlend", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(BrandPreNonBlend)));
                cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreNonChivas", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(BrandPreNonChivas)));
                cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreNonDewar", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(BrandPreNonDewar)));
                cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreNon100Pipers", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(BrandPreNon100Pipers)));
                cmdToExecute.Parameters.Add(new SqlParameter("@BrandPreNon100Pipers8yrs", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(BrandPreNon100Pipers8yrs)));

                cmdToExecute.Parameters.Add(new SqlParameter("@Survey4aBaileys", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aBaileys));
                cmdToExecute.Parameters.Add(new SqlParameter("@Survey4aBenmore", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aBenmore));
                cmdToExecute.Parameters.Add(new SqlParameter("@Survey4aJwBlack", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aJwBlack));
                cmdToExecute.Parameters.Add(new SqlParameter("@Survey4aJwGold", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aJwGold));
                cmdToExecute.Parameters.Add(new SqlParameter("@Survey4aJwGreen", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aJwGreen));
                cmdToExecute.Parameters.Add(new SqlParameter("@Survey4aJwRed", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aJwRed));
                cmdToExecute.Parameters.Add(new SqlParameter("@Survey4aSmirnoff", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aSmirnoff));

                cmdToExecute.Parameters.Add(new SqlParameter("@SurveyP4wNonAbsolute", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, SurveyP4wNonAbsolute));
                cmdToExecute.Parameters.Add(new SqlParameter("@SurveyP4wNonBallentine", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, SurveyP4wNonBallentine));
                cmdToExecute.Parameters.Add(new SqlParameter("@SurveyP4wNonBlend", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, SurveyP4wNonBlend));
                cmdToExecute.Parameters.Add(new SqlParameter("@SurveyP4wNonChivas", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, SurveyP4wNonChivas));
                cmdToExecute.Parameters.Add(new SqlParameter("@SurveyP4wNonDewar", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, SurveyP4wNonDewar));
                cmdToExecute.Parameters.Add(new SqlParameter("@SurveyP4wNon100Pipers", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, SurveyP4wNon100Pipers));
                cmdToExecute.Parameters.Add(new SqlParameter("@SurveyP4wNon100Pipers8yrs", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, SurveyP4wNon100Pipers8yrs));

                cmdToExecute.Parameters.Add(new SqlParameter("@Gender", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Gender));
                cmdToExecute.Parameters.Add(new SqlParameter("@GenderFrom", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, GenderFrom));
                cmdToExecute.Parameters.Add(new SqlParameter("@GenderTo", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, GenderTo));

                if (DateOfBirthFrom == null)
                    cmdToExecute.Parameters.Add(new SqlParameter("@DateOfBirthFrom", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, DBNull.Value));
                else
                    cmdToExecute.Parameters.Add(new SqlParameter("@DateOfBirthFrom", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, DateOfBirthFrom));

                if (DateOfBirthTo == null)
                    cmdToExecute.Parameters.Add(new SqlParameter("@DateOfBirthTo", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, DBNull.Value));
                else
                    cmdToExecute.Parameters.Add(new SqlParameter("@DateOfBirthTo", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, DateOfBirthTo));

                cmdToExecute.Parameters.Add(new SqlParameter("@Province", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Province));
                cmdToExecute.Parameters.Add(new SqlParameter("@RegisterOutlet", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, RegisterOutlet));

                if (RegisterDateFrom == null)
                    cmdToExecute.Parameters.Add(new SqlParameter("@RegisterDateFrom", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, DBNull.Value));
                else
                    cmdToExecute.Parameters.Add(new SqlParameter("@RegisterDateFrom", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, RegisterDateFrom));

                if (RegisterDateTo == null)
                    cmdToExecute.Parameters.Add(new SqlParameter("@RegisterDateTo", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, DBNull.Value));
                else
                    cmdToExecute.Parameters.Add(new SqlParameter("@RegisterDateTo", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, RegisterDateTo));

                cmdToExecute.Parameters.Add(new SqlParameter("@LastVisitOutlet", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, LastVisitOutlet));

                if (LastVisitDateFrom == null)
                    cmdToExecute.Parameters.Add(new SqlParameter("@LastVisitDateFrom", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, DBNull.Value));
                else
                    cmdToExecute.Parameters.Add(new SqlParameter("@LastVisitDateFrom", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, LastVisitDateFrom));

                if (LastVisitDateTo == null)
                    cmdToExecute.Parameters.Add(new SqlParameter("@LastVisitDateTo", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, DBNull.Value));
                else
                    cmdToExecute.Parameters.Add(new SqlParameter("@LastVisitDateTo", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, LastVisitDateTo));

                cmdToExecute.Parameters.Add(new SqlParameter("@ActiveEmail", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(ActiveEmail)));
                cmdToExecute.Parameters.Add(new SqlParameter("@ActiveMobile", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(ActiveMobile)));
                cmdToExecute.Parameters.Add(new SqlParameter("@ActiveCallCenter", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ValidateFalseString(ActiveCallCenter)));
                cmdToExecute.Parameters.Add(new SqlParameter("@SubScribePlatformID", SqlDbType.NVarChar, 4000, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, SubScribePlatformID));
                cmdToExecute.Parameters.Add(new SqlParameter("@ISCount", SqlDbType.Int, 4, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, IsCount));

                cmdToExecute.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int, 4, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, StartRowIndex));
                cmdToExecute.Parameters.Add(new SqlParameter("@MaximumRowsIndex", SqlDbType.Int, 4, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, MaximumRowIndex));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_Consumer_ExtractData' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("Consumer::ExtractData::Error occured::" + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }

        public bool UpdateConsumer()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[Sp_Consumer_UpdateConsumer]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@consumerId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, ConsumerId));
                cmdToExecute.Parameters.Add(new SqlParameter("@idCard", SqlDbType.VarChar, 13, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, IdCard));
                cmdToExecute.Parameters.Add(new SqlParameter("@firstName", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, FirstName));
                cmdToExecute.Parameters.Add(new SqlParameter("@lastName", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, LastName));
                cmdToExecute.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Email));
                cmdToExecute.Parameters.Add(new SqlParameter("@mobile", SqlDbType.VarChar, 11, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Mobile));
                cmdToExecute.Parameters.Add(new SqlParameter("@dateOfBirth", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, DateOfBirth));
                cmdToExecute.Parameters.Add(new SqlParameter("@gender", SqlDbType.VarChar, 10, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Gender));
                cmdToExecute.Parameters.Add(new SqlParameter("@homeAddress", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, HomeAddress));
                cmdToExecute.Parameters.Add(new SqlParameter("@homeMoo", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, HomeMoo));
                cmdToExecute.Parameters.Add(new SqlParameter("@homeVillage", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, HomeVillage));
                cmdToExecute.Parameters.Add(new SqlParameter("@homeSoi", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, HomeSoi));
                cmdToExecute.Parameters.Add(new SqlParameter("@homeRoad", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, HomeRoad));
                cmdToExecute.Parameters.Add(new SqlParameter("@homeSubDistrict", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, HomeSubDistrict));
                cmdToExecute.Parameters.Add(new SqlParameter("@homeDistrict", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, HomeDistrict));
                cmdToExecute.Parameters.Add(new SqlParameter("@homeProvince", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, HomeProvince));
                cmdToExecute.Parameters.Add(new SqlParameter("@workAddress", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, WorkAddress));
                cmdToExecute.Parameters.Add(new SqlParameter("@workMoo", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, WorkMoo));
                cmdToExecute.Parameters.Add(new SqlParameter("@workVillage", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, WorkVillage));
                cmdToExecute.Parameters.Add(new SqlParameter("@workSoi", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, WorkSoi));
                cmdToExecute.Parameters.Add(new SqlParameter("@workRoad", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, WorkRoad));
                cmdToExecute.Parameters.Add(new SqlParameter("@workSubDistrict", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, WorkSubDistrict));
                cmdToExecute.Parameters.Add(new SqlParameter("@workDistrict", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, WorkDistrict));
                cmdToExecute.Parameters.Add(new SqlParameter("@workProvince", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, WorkProvince));
                cmdToExecute.Parameters.Add(new SqlParameter("@updateDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, UpdateDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@updateBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, UpdateBy));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                _rowsAffected = cmdToExecute.ExecuteNonQuery();
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'Sp_Consumer_UpdateConsumer' reported the ErrorCode: " + _errorCode);
                }

                return true;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("ConsumerDAL::UpdateConsumer::Error occured::" + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }

        public bool UpdateConsumerByIpad()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[Sp_Consumer_UpdateConsumerByIpad]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@consumerId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, ConsumerId));
                cmdToExecute.Parameters.Add(new SqlParameter("@firstName", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, FirstName));
                cmdToExecute.Parameters.Add(new SqlParameter("@lastName", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, LastName));
                cmdToExecute.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Email));
                cmdToExecute.Parameters.Add(new SqlParameter("@mobile", SqlDbType.VarChar, 11, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Mobile));
                cmdToExecute.Parameters.Add(new SqlParameter("@dateOfBirth", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, DateOfBirth));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPre100Pipers", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPre100Pipers));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPre100Pipers8Y", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPre100Pipers8Y));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreAbsolut", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreAbsolut));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBaileys", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBaileys));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBallentine", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBallentine));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBenmore", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBenmore));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBlend", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBlend));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreChivas", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreChivas));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreDewar", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreDewar));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreHennessy", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreHennessy));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwBlack", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwBlack));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwGold", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwGold));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwGreen", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwGreen));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwRed", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwRed));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreSmirnoff", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreSmirnoff));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreNotdrink", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreNotdrink));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreText", SqlDbType.VarChar, 500, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreText));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreOther", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreOther));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aBaileys", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aBaileys));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aBenmore", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aBenmore));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aJwBlack", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aJwBlack));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aJwGold", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aJwGold));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aJwGreen", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aJwGreen));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aJwRed", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aJwRed));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aSmirnoff", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aSmirnoff));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aAbsolut", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aAbsolut));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aBallentine", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aBallentine));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aBlend", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aBlend));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aChivas", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aChivas));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aDewar", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aDewar));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aHennessy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aHennessy));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4a100Pipers", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4a100Pipers));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4a100Pipers8Y", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4a100Pipers8Y));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aOthers", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aOthers));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aNotdrink", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aNotdrink));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aText", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aText));
                cmdToExecute.Parameters.Add(new SqlParameter("@optInDG", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, OptInDG));
                cmdToExecute.Parameters.Add(new SqlParameter("@optInOutlet", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, OptInOutlet));
                cmdToExecute.Parameters.Add(new SqlParameter("@lastVisitPlatformId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, LastVisitPlatformId));
                cmdToExecute.Parameters.Add(new SqlParameter("@lastVisitOutletId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, LastVisitOutletId));
                cmdToExecute.Parameters.Add(new SqlParameter("@lastVisitDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, LastVisitDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@activeConsumer", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ActiveConsumer));
                cmdToExecute.Parameters.Add(new SqlParameter("@activeDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ActiveDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@transactionDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, TransactionDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@updateDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, UpdateDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@updateBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, UpdateBy));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                _rowsAffected = cmdToExecute.ExecuteNonQuery();
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'Sp_Consumer_UpdateConsumerByIpad' reported the ErrorCode: " + _errorCode);
                }

                return true;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("ConsumerDAL::UpdateConsumerByIpad::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }

        public bool InsertConsumerByIpad()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[Sp_Consumer_InsertConsumerByIpad]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@firstName", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, FirstName));
                cmdToExecute.Parameters.Add(new SqlParameter("@lastName", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, LastName));
                cmdToExecute.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Email));
                cmdToExecute.Parameters.Add(new SqlParameter("@mobile", SqlDbType.VarChar, 11, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Mobile));
                cmdToExecute.Parameters.Add(new SqlParameter("@dateOfBirth", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, DateOfBirth));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPre100Pipers", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPre100Pipers));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPre100Pipers8Y", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPre100Pipers8Y));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreAbsolut", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreAbsolut));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBaileys", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBaileys));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBallentine", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBallentine));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBenmore", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBenmore));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreBlend", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreBlend));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreChivas", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreChivas));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreDewar", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreDewar));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreHennessy", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreHennessy));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwBlack", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwBlack));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwGold", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwGold));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwGreen", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwGreen));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreJwRed", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreJwRed));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreSmirnoff", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreSmirnoff));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreNotdrink", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreNotdrink));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreText", SqlDbType.VarChar, 500, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreText));
                cmdToExecute.Parameters.Add(new SqlParameter("@brandPreOther", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, BrandPreOther));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aBaileys", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aBaileys));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aBenmore", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aBenmore));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aJwBlack", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aJwBlack));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aJwGold", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aJwGold));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aJwGreen", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aJwGreen));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aJwRed", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aJwRed));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aSmirnoff", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aSmirnoff));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aAbsolut", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aAbsolut));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aBallentine", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aBallentine));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aBlend", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aBlend));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aChivas", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aChivas));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aDewar", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aDewar));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aHennessy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aHennessy));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4a100Pipers", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4a100Pipers));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4a100Pipers8Y", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4a100Pipers8Y));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aOthers", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aOthers));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aNotdrink", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aNotdrink));
                cmdToExecute.Parameters.Add(new SqlParameter("@survey4aText", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Survey4aText));
                cmdToExecute.Parameters.Add(new SqlParameter("@optInDG", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, OptInDG));
                cmdToExecute.Parameters.Add(new SqlParameter("@optInOutlet", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, OptInOutlet));
                cmdToExecute.Parameters.Add(new SqlParameter("@registerPlatformId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, RegisterPlatformId));
                cmdToExecute.Parameters.Add(new SqlParameter("@registerOutletId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, RegisterOutletId));
                cmdToExecute.Parameters.Add(new SqlParameter("@registerDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, RegisterDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@lastVisitPlatformId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, LastVisitPlatformId));
                cmdToExecute.Parameters.Add(new SqlParameter("@lastVisitOutletId", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, LastVisitOutletId));
                cmdToExecute.Parameters.Add(new SqlParameter("@lastVisitDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, LastVisitDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@activeConsumer", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ActiveConsumer));
                cmdToExecute.Parameters.Add(new SqlParameter("@activeDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, ActiveDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@transactionDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, TransactionDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@createDate", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, CreateDate));
                cmdToExecute.Parameters.Add(new SqlParameter("@createBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, CreateBy));
                cmdToExecute.Parameters.Add(new SqlParameter("@consumerId", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, ConsumerId));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                //_rowsAffected = cmdToExecute.ExecuteNonQuery();
                //ConsumerId = Convert.ToInt32(cmdToExecute.Parameters["@consumerId"].Value.ToString());
                //_errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                ConsumerId = Convert.ToInt32(cmdToExecute.ExecuteScalar().ToString());
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'Sp_Consumer_InsertConsumerByIpad' reported the ErrorCode: " + _errorCode);
                }

                return true;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("ConsumerDAL::InsertConsumerByIpad::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }


        public DataTable Search(string consumerIdCard, string consumerFirstName, string consumerLastName, string consumerEmail, string consumerMobile,
        int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "Sp_Consumer_Search";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("Consumer");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@ConsumerIdCard", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, consumerIdCard));
                cmdToExecute.Parameters.Add(new SqlParameter("@ConsumerFirstName", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, consumerFirstName));
                cmdToExecute.Parameters.Add(new SqlParameter("@ConsumerLastName", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, consumerLastName));
                cmdToExecute.Parameters.Add(new SqlParameter("@ConsumerEmail", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, consumerEmail));
                cmdToExecute.Parameters.Add(new SqlParameter("@ConsumerMobile", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, consumerMobile));
                cmdToExecute.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, startRowIndex));
                cmdToExecute.Parameters.Add(new SqlParameter("@MaximumRows", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, maximumRows));
                cmdToExecute.Parameters.Add(new SqlParameter("@SortExpression", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, sortExpression));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));
                cmdToExecute.Parameters.Add(new SqlParameter("@ISCount", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, isCount));

                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_Consumer_Search' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("ConsumerDAL::Search::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

        public string ValidateFalseString(bool sign)
        {
            try
            {
                if (sign == true)
                    return "T";
                else
                    return "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public DataTable SelectOneBYIdCardEmailAndMobile(string idCard, string email, string mobile)
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "Sp_Consumer_SelectOneBYIdCardEmailAndMobile";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("User");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@IdCard", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, idCard));
                cmdToExecute.Parameters.Add(new SqlParameter("@Email", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, email));
                cmdToExecute.Parameters.Add(new SqlParameter("@Mobile", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, mobile));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_User_SelectOneBYUserNameAndPassword' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("User::SelectOneBYIdCardEmailAndMobile::Error occured.", ex);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

        public DataTable ManipulateConsumer(String _email_val, String _mobile_val, DateTime _dateofBirth_val, String _firstName_val, String _lastName_val)
        {

            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[SP_Manipulate_ConsumerByIpad]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("Manipulate");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@Email", SqlDbType.VarChar, 50, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _email_val));
                cmdToExecute.Parameters.Add(new SqlParameter("@Mobile", SqlDbType.VarChar, 11, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _mobile_val));
                cmdToExecute.Parameters.Add(new SqlParameter("@DateofBirth", SqlDbType.DateTime, 8, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, _dateofBirth_val));
                cmdToExecute.Parameters.Add(new SqlParameter("@FirstName", SqlDbType.VarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _firstName_val));
                cmdToExecute.Parameters.Add(new SqlParameter("@LastName", SqlDbType.VarChar, 50, ParameterDirection.Input, false, 0, 0, "", DataRowVersion.Proposed, _lastName_val));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));
                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                adapter.Fill(toReturn);
                return toReturn;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("ConsumerDAL::Manipulate:: " + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }
        public DataTable SelectColumnVExtract()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "Sp_ExtractConsumer_SelectOne";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("User");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_ExtractConsumer_SelectOne' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("User::SelectColumnVExtract::Error occured.", ex);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

        public DataTable SelectConsumerData()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "Sp_ExtractConsumer_SelectData";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("User");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_ExtractConsumer_SelectData' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("User::SelectConsumerData::Error occured.", ex);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }
    }
}
