﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL.GDAL;
using System.Data.SqlClient;
using System.Data;
using System.Data.SqlTypes;

namespace DIAGEO.DAL
{
    public class EventVisitConditionDAL : EventVisitCondition
    { 
        public DataTable SelectByEventIDAndDataGroup(int eventID, int dataGroup)
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[Sp_EventVisitCondition_SelectByEventIDAndDataGroup]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("EventVisitCondition");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@eventId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, eventID));
                cmdToExecute.Parameters.Add(new SqlParameter("@datagroup", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, dataGroup));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'Sp_G_EventVisitCondition_SelectOne' reported the ErrorCode: " + _errorCode);
                }

                //if (toReturn.Rows.Count > 0)
                //{
                //    ConditionId = (Int32)toReturn.Rows[0]["conditionId"];
                //    EventId = toReturn.Rows[0]["eventId"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["eventId"];
                //    DataGroup = toReturn.Rows[0]["dataGroup"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["dataGroup"];
                //    TypeId = toReturn.Rows[0]["typeId"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["typeId"];
                //    OutletId = toReturn.Rows[0]["outletId"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["outletId"];
                //    DateFrom = toReturn.Rows[0]["dateFrom"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["dateFrom"];
                //    DateTo = toReturn.Rows[0]["dateTo"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["dateTo"];
                //    LastActivityNotOver = toReturn.Rows[0]["lastActivityNotOver"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["lastActivityNotOver"];
                //    VisitFrequency = toReturn.Rows[0]["visitFrequency"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["visitFrequency"];
                //    CreateDate = toReturn.Rows[0]["createDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["createDate"];
                //    CreateBy = toReturn.Rows[0]["createBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["createBy"];
                //    UpdateDate = toReturn.Rows[0]["updateDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["updateDate"];
                //    UpdateBy = toReturn.Rows[0]["updateBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["updateBy"];
                //}
                return toReturn;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("EventVisitCondition::SelectOne::Error occured::" + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

        public bool DeleteByEventId()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[Sp_EventVisitCondition_DeleteByEventId]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@eventId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, EventId));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                _rowsAffected = cmdToExecute.ExecuteNonQuery();
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'Sp_EventVisitCondition_DeleteByEventId' reported the ErrorCode: " + _errorCode);
                }

                return true;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("EventVisitConditionDAL::DeleteByEventId::Error occured :: " + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }

        public DataTable SelectByEachTypeId(int eventID, int dataGroup,int typeId)
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[Sp_EventVisitCondition_SelectByEachTypeId]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("EventVisitCondition");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@eventId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, eventID));
                cmdToExecute.Parameters.Add(new SqlParameter("@datagroup", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, dataGroup));
                cmdToExecute.Parameters.Add(new SqlParameter("@typeId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, typeId));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'Sp_EventVisitCondition_SelectByEachTypeId' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("EventVisitCondition::SelectByEachTypeId::Error occured::" + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }
    }
}
