﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DIAGEO.DAL.GDAL;
using System.Data.SqlClient;
using System.Data;

namespace DIAGEO.DAL
{
    public class OutboundTypeDAL : OutboundType
    {
        public DataTable SelectActiveOutboundType()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "Sp_OutboundType_SelectActiveOutboundType";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("OutboundType");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@Active", SqlDbType.Char, 1, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, Active));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_OutboundType_SelectActiveOutboundType' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("OutboundTypeDAL::SelectActiveOutboundType::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }
        
        public DataTable SelectOneBYOutboundTypeName(string name)
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "Sp_OutboundType_SelectOneBYOutboundTypeName";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("OutboundType");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@outboundTypeName", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, name));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_OutboundType_SelectOneBYOutboundTypeName' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("OutboundType::SelectOneBYOutboundTypeName::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

        public DataTable Search(string outboundTypeName, string active, int startRowIndex, int maximumRows, string sortExpression, int isCount)
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "Sp_OutboundType_Search";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("OutboundType");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@OutboundTypeName", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, outboundTypeName));
                cmdToExecute.Parameters.Add(new SqlParameter("@Active", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, active));
                cmdToExecute.Parameters.Add(new SqlParameter("@StartRowIndex", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, startRowIndex));
                cmdToExecute.Parameters.Add(new SqlParameter("@MaximumRows", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, maximumRows));
                cmdToExecute.Parameters.Add(new SqlParameter("@SortExpression", SqlDbType.VarChar, 100, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, sortExpression));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));
                cmdToExecute.Parameters.Add(new SqlParameter("@ISCount", SqlDbType.Int, 4, ParameterDirection.Input, true, 10, 0, "", DataRowVersion.Proposed, isCount));

                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_OutboundType_Search' reported the ErrorCode: " + _errorCode);
                }

                return toReturn;
            }
            catch (Exception ex)
            {
                throw new Exception("OutboundType::Search::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

        public bool UpdateSearch(string userIdLogin)
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "Sp_OutboundType_UpdateSearch";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@outboundTypeId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, OutboundTypeId));
                cmdToExecute.Parameters.Add(new SqlParameter("@name", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Name));
                cmdToExecute.Parameters.Add(new SqlParameter("@active", SqlDbType.Char, 1, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Active));
                cmdToExecute.Parameters.Add(new SqlParameter("@updateBy", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, userIdLogin));
                cmdToExecute.Parameters.Add(new SqlParameter("@format", SqlDbType.VarChar, 100, ParameterDirection.Input, true, 0, 0, "", DataRowVersion.Proposed, Format));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                _rowsAffected = cmdToExecute.ExecuteNonQuery();
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    throw new Exception("Stored Procedure 'Sp_OutboundType_UpdateSearch' reported the ErrorCode: " + _errorCode);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("OutboundType::UpdateSearch::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
            }
        }


    }
}
