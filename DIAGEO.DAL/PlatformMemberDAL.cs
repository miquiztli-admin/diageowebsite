﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using DIAGEO.DAL.GDAL;
using System.Data.SqlTypes;

namespace DIAGEO.DAL
{
    public class PlatformMemberDAL : PlatformMember
    {
        public DataTable SelectOneByConsumerId()
        {
            SqlCommand cmdToExecute = new SqlCommand();
            cmdToExecute.CommandText = "dbo.[Sp_PlatformMember_SelectOneByConsumerId]";
            cmdToExecute.CommandType = CommandType.StoredProcedure;
            DataTable toReturn = new DataTable("PlatformMember");
            SqlDataAdapter adapter = new SqlDataAdapter(cmdToExecute);

            // Use base class' connection object
            cmdToExecute.Connection = _mainConnection;

            try
            {
                cmdToExecute.Parameters.Add(new SqlParameter("@consumerId", SqlDbType.Int, 4, ParameterDirection.Input, false, 10, 0, "", DataRowVersion.Proposed, ConsumerId));
                cmdToExecute.Parameters.Add(new SqlParameter("@ErrorCode", SqlDbType.Int, 4, ParameterDirection.Output, true, 10, 0, "", DataRowVersion.Proposed, _errorCode));

                if (_mainConnectionIsCreatedLocal)
                {
                    // Open connection.
                    _mainConnection.Open();
                }
                else
                {
                    if (_mainConnectionProvider.IsTransactionPending)
                    {
                        cmdToExecute.Transaction = _mainConnectionProvider.CurrentTransaction;
                    }
                }

                // Execute query.
                adapter.Fill(toReturn);
                _errorCode = Convert.ToInt32(cmdToExecute.Parameters["@ErrorCode"].Value.ToString());

                if (_errorCode != (int)LLBLError.AllOk)
                {
                    // Throw error.
                    throw new Exception("Stored Procedure 'Sp_G_PlatformMember_SelectOne' reported the ErrorCode: " + _errorCode);
                }

                if (toReturn.Rows.Count > 0)
                {
                    MemberId = (Int32)toReturn.Rows[0]["memberId"];
                    ConsumerId = toReturn.Rows[0]["consumerId"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["consumerId"];
                    PlatformId = toReturn.Rows[0]["platformId"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["platformId"];
                    MemberCode = toReturn.Rows[0]["memberCode"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["memberCode"];
                    RegisterOutletId = toReturn.Rows[0]["registerOutletId"] == System.DBNull.Value ? SqlInt32.Null : (Int32)toReturn.Rows[0]["registerOutletId"];
                    RegisterDate = toReturn.Rows[0]["registerDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["registerDate"];
                    LastVisitDate = toReturn.Rows[0]["lastVisitDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["lastVisitDate"];
                    CreateDate = toReturn.Rows[0]["createDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["createDate"];
                    CreateBy = toReturn.Rows[0]["createBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["createBy"];
                    UpdateDate = toReturn.Rows[0]["updateDate"] == System.DBNull.Value ? SqlDateTime.Null : (DateTime)toReturn.Rows[0]["updateDate"];
                    UpdateBy = toReturn.Rows[0]["updateBy"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["updateBy"];
                    Flag = toReturn.Rows[0]["flag"] == System.DBNull.Value ? SqlString.Null : (string)toReturn.Rows[0]["flag"];
                }
                return toReturn;
            }
            catch (Exception ex)
            {
                // some error occured. Bubble it to caller and encapsulate Exception object
                throw new Exception("PlatformMemberDAL::SelectOneByConsumerId::Error occured." + ex.Message);
            }
            finally
            {
                if (_mainConnectionIsCreatedLocal)
                {
                    // Close connection.
                    _mainConnection.Close();
                }
                cmdToExecute.Dispose();
                adapter.Dispose();
            }
        }

    }
}
