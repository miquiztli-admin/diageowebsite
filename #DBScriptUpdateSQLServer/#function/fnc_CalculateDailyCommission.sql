USE [DiageoDB_Edge]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.fnc_CalculateDailyCommission') IS NOT NULL
    DROP FUNCTION fnc_CalculateDailyCommission;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <19/05/2014>
-- Description:	<Use to calculate commission in stored procedure instead of report code for easy mantain>
CREATE FUNCTION [dbo].[fnc_CalculateDailyCommission]
(	
	@MonthlyEngagement INT,
	@MonthlyTraffic FLOAT,
	@MonthlyNew INT,
	@DailyEngage INT,
	@DailyNew INT
)
RETURNS FLOAT
AS
BEGIN
	DECLARE @flMonthlyNew FLOAT, @flMonthlyEngagement FLOAT
	DECLARE @flDailyNew FLOAT, @flDailyEngagement FLOAT
	DECLARE @flMonthlyEngagePerTraffic FLOAT, @flMonthlyNewPerEngage FLOAT
	DECLARE @Step1 FLOAT, @Step2 FLOAT

	SET @flMonthlyNew = CONVERT(FLOAT, @MonthlyNew)
	SET @flMonthlyEngagement = CONVERT(FLOAT, @MonthlyEngagement)
	SET @flMonthlyEngagePerTraffic = 0
	IF @MonthlyTraffic != 0 SET @flMonthlyEngagePerTraffic = @flMonthlyEngagement/CONVERT(FLOAT, @MonthlyTraffic)
	SET @flMonthlyNewPerEngage = 0
	IF @flMonthlyEngagement != 0 SET @flMonthlyNewPerEngage = @flMonthlyNew/@flMonthlyEngagement
	SET @flDailyNew = CONVERT(FLOAT, @DailyNew)
	SET @flDailyEngagement = CONVERT(FLOAT, @DailyEngage)
	
	IF @flMonthlyEngagePerTraffic < 0.2
		SET @Step1 = 0
	ELSE IF @flMonthlyEngagePerTraffic < 0.25
		SET @Step1 = @flDailyEngagement * 0.5
	ELSE -- More than 25%
		SET @Step1 = @flDailyEngagement
		
	IF @flMonthlyNewPerEngage < 0.2
		IF @flMonthlyEngagePerTraffic < 0.2
			SET @Step2 = 0
		ELSE
			SET @Step2 = @flDailyNew
	ELSE
		SET @Step2 = @flDailyNew * 2

	RETURN @Step1 + @Step2
END
