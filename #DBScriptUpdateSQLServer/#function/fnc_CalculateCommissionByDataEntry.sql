USE [DiageoDB_Edge]
GO
/****** Object:  UserDefinedFunction [dbo].[fnc_CalculateCommissionByDataEntry]    Script Date: 04/24/2014 12:25:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.fnc_CalculateCommissionByDataEntry') IS NOT NULL
    DROP FUNCTION fnc_CalculateCommissionByDataEntry;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <12/12/2013>
-- Description:	<Use to calculate commission in stored procedure instead of report code for easy mantain>
CREATE FUNCTION [dbo].[fnc_CalculateCommissionByDataEntry]
(	
	@Engagement INT,
	@Traffic FLOAT,
	@New INT
)
RETURNS FLOAT
AS
BEGIN
	DECLARE @flNew FLOAT, @flEngagement FLOAT
	DECLARE @flEngagePerTraffic FLOAT, @flNewPerEngage FLOAT
	DECLARE @Step1 FLOAT, @Step2 FLOAT

	SET @flNew = CONVERT(FLOAT, @New)
	SET @flEngagement = CONVERT(FLOAT, @Engagement)
	SET @flEngagePerTraffic = 0
	IF @Traffic != 0 SET @flEngagePerTraffic = @flEngagement/CONVERT(FLOAT, @Traffic)
	SET @flNewPerEngage = 0
	IF @flEngagement != 0 SET @flNewPerEngage = @flNew/@flEngagement
	
	IF @flEngagePerTraffic < 0.2
		SET @Step1 = 0
	ELSE IF @flEngagePerTraffic < 0.25
		SET @Step1 = @flEngagement * 0.5
	ELSE -- More than 25%
		SET @Step1 = @flEngagement
		
	IF @flNewPerEngage < 0.2
		IF @flEngagePerTraffic < 0.2
			SET @Step2 = 0
		ELSE
			SET @Step2 = @flNew
	ELSE
		SET @Step2 = @flNew * 2

	RETURN @Step1 + @Step2
END
