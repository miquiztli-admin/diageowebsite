USE [DiageoDBTest]
GO

/****** Object:  Table [dbo].[tbl_m_rpt_survey_type]    Script Date: 09/28/2013 10:53:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'dbo.tbl_m_rpt_survey_type', N'U') IS NOT NULL
    DROP TABLE dbo.tbl_m_rpt_survey_type;
GO

CREATE TABLE [dbo].[tbl_m_rpt_survey_type](
	[disp_name] [varchar](10) NULL,
	[survey_val] [int] NULL
) ON [PRIMARY]

GO

INSERT INTO [dbo].[tbl_m_rpt_survey_type](disp_name, survey_val) 
VALUES ('Adorer', 1),
	('Aopter', 2),
	('Available', 3),
	('Accept', 4),
	('Reject', 5)

GO