USE [DiageoDBTest]
GO

/****** Object:  Table [dbo].[tbl_m_rpt_survey_type]    Script Date: 09/28/2013 10:53:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'dbo.tbl_m_rpt_dashb_templ_graph', N'U') IS NOT NULL
    DROP TABLE dbo.tbl_m_rpt_dashb_templ_graph;
GO

CREATE TABLE [dbo].[tbl_m_rpt_dashb_templ_graph](
	[period] [varchar](10) NULL,
	[num] [int] NULL
) ON [PRIMARY]

GO

INSERT INTO [dbo].[tbl_m_rpt_dashb_templ_graph](period, num) 
VALUES ('Q1', 1),
	('Q2', 2),
	('Q3', 3),
	('Q4', 4),
	('YTD', 5),
	('Target', 6)

GO