USE [DiageoDBTest]
GO

/****** Object:  Table [dbo].[tbl_m_rpt_quarter_month]    Script Date: 09/28/2013 10:53:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'dbo.tbl_m_rpt_quarter_month', N'U') IS NOT NULL
    DROP TABLE dbo.tbl_m_rpt_quarter_month;
GO

CREATE TABLE [dbo].[tbl_m_rpt_quarter_month](
	[quarter] [int] NULL,
	[year_shift] [int] NULL,
	[month_num] [int] NULL
) ON [PRIMARY]

GO

INSERT INTO [dbo].[tbl_m_rpt_quarter_month](quarter, year_shift, month_num) 
VALUES (1, -1, 7),
	(1, -1, 8),
	(1, -1, 9),
	(2, -1, 10),
	(2, -1, 11),
	(2, -1, 12),
	(3, 0, 1),
	(3, 0, 2),
	(3, 0, 3),
	(4, 0, 4),
	(4, 0, 5),
	(4, 0, 6)
	
GO