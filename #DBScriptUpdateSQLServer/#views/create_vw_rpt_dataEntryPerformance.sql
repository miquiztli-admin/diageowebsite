USE [DiageoDB_Edge]
GO
IF OBJECT_ID (N'dbo.vw_rpt_dataEntryPerformance', N'VIEW') IS NOT NULL
    DROP VIEW vw_rpt_dataEntryPerformance;
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_rpt_dataEntryPerformance]
AS
	SELECT s.Staff_Code, s.Staff_Name, o.Outlet_ID, o.Outlet_Name, r.RepeatTransactionDate,
	(ISNULL(t.Traffic_Amount, 0) / ISNULL(daily.NoPG, 1)) AS Traffic_Amount,
	ISNULL(r.AllType, 0) AS Engagement,
	ISNULL(r.NewType, 0) AS NewType,
	ISNULL(r.RepeatType, 0) AS RepeatType
	FROM (
		SELECT Repeat_Outlet, Repeat_CreateBy, RepeatTransactionDate,
		SUM(Case when UPPER(Repeat_Type) = 'R' then 1 else 0 end) as RepeatType,
		SUM(Case when UPPER(Repeat_Type) = 'N' then 1 else 0 end) as NewType,
		SUM(Case when Repeat_Type <> '' then 1 else 0 end) as AllType
		FROM 
		(
			SELECT Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, 
			CONVERT(date, Repeat_TransactionDate) as RepeatTransactionDate
			FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
			GROUP BY Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, Repeat_Consumer, CONVERT(date, Repeat_TransactionDate)
		) R1
		GROUP BY Repeat_Outlet, Repeat_CreateBy, RepeatTransactionDate
	) r
		LEFT JOIN (
			SELECT Repeat_Outlet, RepeatTransactionDate,
			COUNT(DISTINCT(Repeat_CreateBy)) AS NoPG
			FROM 
			(
				SELECT Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, 
				CONVERT(date, Repeat_TransactionDate) as RepeatTransactionDate
				FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
				GROUP BY Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, Repeat_Consumer, CONVERT(date, Repeat_TransactionDate)
			) R1
			GROUP BY Repeat_Outlet, RepeatTransactionDate
		) daily ON (daily.Repeat_Outlet = r.Repeat_Outlet 
			AND r.RepeatTransactionDate = daily.RepeatTransactionDate)
		LEFT JOIN SmartTouchDB..ST_mOutlet o ON r.Repeat_Outlet = o.Outlet_ID
		LEFT JOIN SmartTouchDB..ST_mStaff s ON r.Repeat_CreateBy = s.Staff_ID
		LEFT JOIN SmartTouchDB..ST_tsTraffic t ON (t.Traffic_Outlet = r.Repeat_Outlet 
			AND r.RepeatTransactionDate = CONVERT(date, t.Traffic_Date))
	WHERE s.Staff_Code IS NOT NULL

GO
