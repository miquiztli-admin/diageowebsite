USE [DiageoDBTest]
GO
IF OBJECT_ID (N'dbo.vw_rpt_outboundCommunication', N'VIEW') IS NOT NULL
    DROP VIEW vw_rpt_outboundCommunication;
GO
/****** Object:  View [dbo].[vw_rpt_outboundCommunication]    Script Date: 09/28/2013 15:57:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_rpt_outboundCommunication]
AS

	SELECT e.eventId, e.Name AS Campaign, o.consumerId, o.senddate,
	CASE WHEN o.outboundTypeId = 1 THEN 1 ELSE NULL END EDM,
	CASE WHEN o.outboundTypeId = 2 THEN 1 ELSE NULL END SMS,
	CASE WHEN o.outboundTypeId = 3 THEN 1 ELSE NULL END CCenter, 
	CASE
		WHEN o.outboundTypeId = 1 THEN o.consumerId
		ELSE NULL
	END AS EDMSent,
	CASE
		WHEN o.outboundTypeId = 1 AND LOWER(result) = 'success' THEN o.consumerId
		ELSE NULL
	END AS EDMDeliver,
	CASE
		WHEN o.outboundTypeId = 1 AND LOWER(result) = 'success' AND LOWER(openStatus) = 'open' THEN o.consumerId
		ELSE NULL
	END AS EDMOpen,
	CASE
		WHEN o.outboundTypeId = 1 AND LOWER(openStatus) = 'open' AND clickLink = 'Y' THEN o.consumerId
		ELSE NULL
	END AS EDMClickLink,
	CASE
		WHEN o.outboundTypeId = 2 THEN o.consumerId
		ELSE NULL
	END AS SMSSent,
	CASE
		WHEN o.outboundTypeId = 2 AND LOWER(result) = 'success' THEN o.consumerId
		ELSE NULL
	END AS SMSDeliver,
	CASE
		WHEN o.outboundTypeId = 2 AND LOWER(result) <> 'success' THEN o.consumerId
		ELSE NULL
	END AS SMSFail,
	CASE
		WHEN o.outboundTypeId = 3 THEN o.consumerId
		ELSE NULL
	END AS CCenterSent,
	CASE
		WHEN o.outboundTypeId = 3 AND LOWER(o.openStatus) = '0100' /* Success */ THEN o.consumerId
		ELSE NULL
	END AS CCenterSuccess,
	CASE
		WHEN o.outboundTypeId = 3 AND LOWER(o.openStatus) <> '0100' AND cc.Code IS NOT NULL THEN o.consumerId
		ELSE NULL
	END AS CCenterFail
	FROM DiageoDB..OutboundEvent AS e 
		INNER JOIN DiageoDB..Outbound AS o ON e.eventId = o.eventId
		LEFT OUTER JOIN DiageoDB..CallCenterBounceMapping cc ON o.openStatus = cc.Code

GO
