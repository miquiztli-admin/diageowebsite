USE [DiageoDBTest]
GO
IF OBJECT_ID (N'dbo.vw_rpt_outboundEvent_by_quarter', N'VIEW') IS NOT NULL
    DROP VIEW vw_rpt_outboundEvent_by_quarter;
GO
/****** Object:  View [dbo].[vw_rpt_outboundEvent_by_quarter]    Script Date: 09/28/2013 15:57:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_rpt_outboundEvent_by_quarter]
AS
	/*++ Buff 20131012 Backup my old script */
	/*
	--1.
	SELECT YEAR(c.createDate) + (-1 * qm.year_shift) AS quarterYear, qm.quarter AS quarterNum,
	c.outboundTypeId, c.startDate, c.endDate, 
	em.*
	FROM dbo.tbl_m_rpt_quarter_month AS qm 
		INNER JOIN DiageoDB..outboundconfig c ON qm.month_num = MONTH(c.startDate)
		INNER JOIN DiageoDB..OutboundEventMaster em ON c.eventId = em.eventId	
	*/
	/*-- Buff 20131012 Backup my old script */
	
	/*
	Remark
	this case may cause eventId overlap between 2 quarter (eventId apear in 2 quarter by senddate in 
	outbound table between border months, like 30 Sep - 2 Oct, etc.)
	
	Should we query eventId with min(senddate) on required criteria before map with quarter?
	eg.

	SELECT e.eventId, MIN(o.senddate), e.brandPreJwBlack, e.brandPreJwRed, e.brandPreSmirnoff
	FROM DiageoDB..OutboundEvent AS e
		INNER JOIN DiageoDB..Outbound AS o ON e.eventId = o.eventId
	WHERE e.brandPreJwBlack = 'T' 
	OR e.brandPreJwRed = 'T'
	OR e.brandPreSmirnoff = 'T'
	GROUP BY e.eventId, e.brandPreJwBlack, e.brandPreJwRed, e.brandPreSmirnoff

	** Noted that this only each eventID happen only once **
	*/ 
	SELECT YEAR(o.senddate) + (-1 * qm.year_shift) AS quarterYear, qm.quarter AS quarterNum,
	o.consumerId, o.outboundTypeId, o.openStatus, o.result, o.clickLink, 
	CASE
		WHEN o.outboundTypeId = 1 THEN o.consumerId
		ELSE NULL
	END AS EDM,
	CASE
		WHEN o.outboundTypeId = 1 AND LOWER(result) = 'success' THEN o.consumerId
		ELSE NULL
	END AS EDMDeliver,
	CASE
		WHEN o.outboundTypeId = 1 AND LOWER(result) = 'success' AND LOWER(openStatus) = 'open' THEN o.consumerId
		ELSE NULL
	END AS EDMOpen,
	CASE
		WHEN o.outboundTypeId = 1 AND LOWER(openStatus) = 'open' AND clickLink = 'Y' THEN o.consumerId
		ELSE NULL
	END AS EDMClickLink,
	CASE
		WHEN o.outboundTypeId = 2 THEN o.consumerId
		ELSE NULL
	END AS SMS,
	CASE
		WHEN o.outboundTypeId = 2 AND  LOWER(result) = 'success' THEN o.consumerId
		ELSE NULL
	END AS SMSDeliver,
	CASE
		WHEN o.outboundTypeId = 3 THEN o.consumerId
		ELSE NULL
	END AS CC,
	CASE
		WHEN (
			e.brandPreJwBlack ='F' AND e.brandPreJwRed ='F' and e.brandPreJwGold ='F' 
			and e.brandPreSmirnoff='F' and brandPreBenmore = 'F' and e.brandPreHennessy ='F' 
			and brandPreBaileys= 'F' and brandPreJwGreen ='F'
			) THEN 1
		ELSE 0
	END AS isHK, 
	e.*
	FROM DiageoDB..OutboundEvent AS e 
		INNER JOIN DiageoDB..Outbound AS o ON e.eventId = o.eventId
		INNER JOIN dbo.tbl_m_rpt_quarter_month AS qm ON qm.month_num = MONTH(o.senddate) 

GO
