USE [DiageoDBTest]
GO
IF OBJECT_ID (N'dbo.vw_rpt_dashb_hk_by_quarter', N'VIEW') IS NOT NULL
    DROP VIEW vw_rpt_dashb_hk_by_quarter;
GO
/****** Object:  View [dbo].[vw_rpt_dashb_hk_by_quarter]    Script Date: 09/28/2013 15:57:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_rpt_dashb_hk_by_quarter]
AS
SELECT
	YEAR(c.createDate) + (-1 * qcm.year_shift) AS cre_quarterYear, qcm.quarter AS cre_quarterNum, 
	YEAR(c.updateDate) + (-1 * qum.year_shift) AS upd_quarterYear, qum.quarter AS upd_quarterNum, 
	c.consumerId,
	CASE
		WHEN (c.subScribeEmail_DG = 'T'
			OR c.subScribeSMS_DG = 'T' 
			OR c.subScribeCallCenter_DG = 'T') THEN c.consumerId
		ELSE NULL
	END AS subscribe,
	CASE
		WHEN c.subScribeEmail_DG != 'T' THEN c.consumerId
		ELSE NULL
	END AS unsubscribe_email,
	CASE
		WHEN (c.subScribeEmail_DG != 'T' OR c.blockEmail ='T') THEN c.consumerId
		ELSE NULL
	END AS invalid_email,
	CASE
		WHEN (c.blockMobile ='T' 
			OR c.subScribeSMS_DG != 'T' 
			OR c.subScribeCallCenter_DG != 'T') THEN c.consumerId
		ELSE NULL
	END AS invalid_mobile,
	CASE
		WHEN (
			c.brandPreJwBlack = 'T' 
			OR c.brandPreJwRed ='T' 
			OR c.brandPreJwGold = 'T' 
			OR c.brandPreSmirnoff ='T' 
			OR c.brandPreBenmore = 'T' 
			OR c.brandPreHennessy ='T'
			OR ISNULL(c.survey4aJwBlack, '') != ''
			OR ISNULL(c.survey4aJwRed, '') != ''
			OR ISNULL(c.survey4aJwGold, '') != ''
			OR ISNULL(c.survey4aSmirnoff, '') != ''
			OR ISNULL(c.survey4aBenmore, '') != ''
			OR ISNULL(c.survey4aHennessy, '') != ''
			) THEN 1
		ELSE 0
	END AS isHK,
	CASE
		WHEN (
			c.survey4aJwBlack = '1'
			OR c.survey4aJwRed = '1'
			OR c.survey4aJwGold = '1'
			OR c.survey4aSmirnoff = '1'
			OR c.survey4aBenmore = '1'
			OR c.survey4aHennessy = '1') THEN consumerId
		ELSE NULL
	END AS HK_Adorer,
	CASE
		WHEN (
			c.survey4aJwBlack = '2'
			OR c.survey4aJwRed = '2'
			OR c.survey4aJwGold = '2'
			OR c.survey4aSmirnoff = '2'
			OR c.survey4aBenmore = '2'
			OR c.survey4aHennessy = '2') THEN consumerId
		ELSE NULL
	END AS HK_Adopter,
	CASE
		WHEN (
			c.survey4aJwBlack = '3'
			OR c.survey4aJwRed = '3'
			OR c.survey4aJwGold = '3'
			OR c.survey4aSmirnoff = '3'
			OR c.survey4aBenmore = '3'
			OR c.survey4aHennessy = '3') THEN consumerId
		ELSE NULL
	END AS HK_Accepter,
	CASE
		WHEN (
			c.survey4aJwBlack = '4'
			OR c.survey4aJwRed = '4'
			OR c.survey4aJwGold = '4'
			OR c.survey4aSmirnoff = '4'
			OR c.survey4aBenmore = '4'
			OR c.survey4aHennessy = '4') THEN consumerId
		ELSE NULL
	END AS HK_Available,
	CASE
		WHEN (
			c.survey4aJwBlack = '5'
			OR c.survey4aJwRed = '5'
			OR c.survey4aJwGold = '5'
			OR c.survey4aSmirnoff = '5'
			OR c.survey4aBenmore = '5'
			OR c.survey4aHennessy = '5') THEN consumerId
		ELSE NULL
	END AS HK_Rejecter,
	CASE
		WHEN (
			ISNULL(c.survey4aJwBlack, '') NOT IN ('1', '2', '3', '4', '5')
			AND ISNULL(c.survey4aJwRed, '') NOT IN ('1', '2', '3', '4', '5')
			AND ISNULL(c.survey4aJwGold, '') NOT IN ('1', '2', '3', '4', '5')
			AND ISNULL(c.survey4aSmirnoff, '') NOT IN ('1', '2', '3', '4', '5')
			AND ISNULL(c.survey4aBenmore, '') NOT IN ('1', '2', '3', '4', '5')
			AND ISNULL(c.survey4aHennessy, '') NOT IN ('1', '2', '3', '4', '5')) THEN consumerId
		ELSE NULL
	END AS HK_NA, -- use with condition isHK = 1 to get correct only HK_NA, otherwise it will sum with all others no HK


	CASE ISNUMERIC(c.survey4aJwBlack) WHEN 1 THEN c.survey4aJwBlack ELSE NULL END AS survey4aJwBlack,
	CASE ISNUMERIC(c.survey4aJwRed) WHEN 1 THEN c.survey4aJwRed ELSE NULL END AS survey4aJwRed,
	CASE ISNUMERIC(c.survey4aJwGold) WHEN 1 THEN c.survey4aJwGold ELSE NULL END AS survey4aJwGold,
	CASE ISNUMERIC(c.survey4aSmirnoff) WHEN 1 THEN c.survey4aSmirnoff ELSE NULL END AS survey4aSmirnoff,
	CASE ISNUMERIC(c.survey4aBenmore) WHEN 1 THEN c.survey4aBenmore ELSE NULL END AS survey4aBenmore,
	CASE ISNUMERIC(c.survey4aHennessy) WHEN 1 THEN c.survey4aHennessy ELSE NULL END AS survey4aHennessy,
	CASE ISNUMERIC(c.survey4aBaileys) WHEN 1 THEN c.survey4aBaileys ELSE NULL END AS survey4aBaileys,
	CASE ISNUMERIC(c.survey4aJwGreen) WHEN 1 THEN c.survey4aJwGreen ELSE NULL END AS survey4aJwGreen,
	CASE ISNUMERIC(c.survey4aAbsolut) WHEN 1 THEN c.survey4aAbsolut ELSE NULL END AS survey4aAbsolut,
	CASE ISNUMERIC(c.survey4aBallentine) WHEN 1 THEN c.survey4aBallentine ELSE NULL END AS survey4aBallentine,
	CASE ISNUMERIC(c.survey4aBlend) WHEN 1 THEN c.survey4aBlend ELSE NULL END AS survey4aBlend,
	CASE ISNUMERIC(c.survey4aChivas) WHEN 1 THEN c.survey4aChivas ELSE NULL END AS survey4aChivas,
	CASE ISNUMERIC(c.survey4aDewar) WHEN 1 THEN c.survey4aDewar ELSE NULL END AS survey4aDewar,
	CASE ISNUMERIC(c.survey4a100Pipers) WHEN 1 THEN c.survey4a100Pipers ELSE NULL END AS survey4a100Pipers,
	CASE ISNUMERIC(c.survey4a100Pipers8Y) WHEN 1 THEN c.survey4a100Pipers8Y ELSE NULL END AS survey4a100Pipers8Y,
	CASE ISNUMERIC(c.survey4aOthers) WHEN 1 THEN c.survey4aOthers ELSE NULL END AS survey4aOthers,
	CASE ISNUMERIC(c.survey4aNotdrink) WHEN 1 THEN c.survey4aNotdrink ELSE NULL END AS survey4aNotdrink,
	CASE ISNUMERIC(c.survey4aText) WHEN 1 THEN c.survey4aText ELSE NULL END AS survey4aText,
	c.surveyP4wBaileys, c.surveyP4wBenmore, 
      c.surveyP4wJwBlack, c.surveyP4wJwGold, c.surveyP4wJwGreen, c.surveyP4wJwRed, c.surveyP4wSmirnoff, c.surveyP4wNonAbsolute, c.surveyP4wNonBallentine, 
      c.surveyP4wNonBlend, c.surveyP4wNonChivas, c.surveyP4wNonDewar, c.surveyP4wNonHennessy, c.surveyP4wNon100Pipers, c.surveyP4wNon100Pipers8yrs, 
      c.surveyP4wNonOthers, c.surveyP4wNonNotdrink, c.surveyP4wNonText,
	  CASE
		WHEN (
			brandPreJwBlack = 'T' 
			OR brandPreJwRed ='T' 
			OR brandPreJwGold = 'T' 
			OR brandPreSmirnoff ='T' 
			OR brandPreBenmore = 'T' 
			OR brandPreHennessy ='T'
			OR survey4aJwBlack is not null 
			OR survey4aJwRed is not null 
			OR survey4aJwGold is not null 
			OR survey4aSmirnoff is not NULL
			OR survey4aBenmore is not null 
			OR survey4aHennessy is not NULL
			) THEN consumerId
		ELSE NULL
	  END AS diageoBrandPre,
	  CASE 
		WHEN brandPreJwBlack = 'T' OR survey4aJwBlack IS NOT NULL THEN c.consumerId
		ELSE NULL
	  END AS brandPreJwBlack,
	  CASE 
		WHEN brandPreJwRed = 'T' OR survey4aJwRed IS NOT NULL THEN c.consumerId
		ELSE NULL
	  END AS brandPreJwRed,
	  CASE 
		WHEN brandPreSmirnoff = 'T' OR survey4aSmirnoff IS NOT NULL THEN c.consumerId
		ELSE NULL
	  END AS brandPreSmirnoff,
	  CASE 
		WHEN (
			brandPreHennessy = 'T' OR survey4aHennessy IS NOT NULL 
			OR brandPreBenmore = 'T' OR survey4aBenmore IS NOT NULL
			OR brandPreJwGold = 'T' OR survey4aJwGold IS NOT null 
			OR brandPreJwGreen = 'T' OR survey4aJwGreen IS NOT NULL
			) THEN consumerId
		ELSE NULL
	  END AS diageoBrandPreOthers,
	  CASE 
		WHEN (
			brandPreOther = 'T' OR survey4aOthers IS NOT NULL 
			OR brandPreNotdrink = 'T' OR survey4aNotdrink IS NOT NULL
			) THEN c.consumerId
		ELSE NULL
	  END AS brandPreOthers,
	  CASE
	    WHEN (
			brandPre100Pipers = 'T' 
			OR brandPre100Pipers8Y = 'T' 
			OR brandPreAbsolut = 'T' 
			OR brandPreBaileys = 'T' 
			OR brandPreBallentine = 'T' 
			OR brandPreBlend = 'T' 
			OR brandPreChivas = 'T' 
			OR brandPreDewar = 'T' 
			OR survey4a100Pipers IS NOT NULL 
			OR survey4a100Pipers8Y IS NOT NULL 
			OR survey4aAbsolut IS NOT NULL 
			OR survey4aBaileys IS NOT NULL 
			OR survey4aBallentine IS NOT NULL 
			OR survey4aBlend IS NOT NULL 
			OR survey4aChivas IS NOT NULL 
			OR survey4aDewar IS NOT NULL
			) THEN c.consumerId
	    ELSE NULL
	  END AS brandPreCompetitors,
	  CASE 
		WHEN brandPreJwGreen = 'T' OR survey4aJwGreen IS NOT NULL THEN c.consumerId
		ELSE NULL
	  END AS brandPreJwGreen,
	  CASE 
		WHEN brandPreJwGold = 'T' OR survey4aJwGold IS NOT NULL THEN c.consumerId
		ELSE NULL
	  END AS brandPreJwGold,
	  CASE 
		WHEN brandPreBaileys = 'T' OR survey4aBaileys IS NOT NULL THEN c.consumerId
		ELSE NULL
	  END AS brandPreBaileys,
	  CASE 
		WHEN brandPreBallentine = 'T' OR survey4aBallentine IS NOT NULL THEN c.consumerId
		ELSE NULL
	  END AS brandPreBallentine,
	  CASE 
		WHEN brandPreBenmore = 'T' OR survey4aBenmore IS NOT NULL THEN c.consumerId
		ELSE NULL
	  END AS brandPreBenmore,
	  CASE 
		WHEN brandPreAbsolut = 'T' OR survey4aAbsolut IS NOT NULL THEN c.consumerId
		ELSE NULL
	  END AS brandPreAbsolut,
	  CASE 
		WHEN brandPreBlend = 'T' OR survey4aBlend IS NOT NULL THEN c.consumerId
		ELSE NULL
	  END AS brandPreBlend,
	  CASE 
		WHEN brandPreChivas = 'T' OR survey4aChivas IS NOT NULL THEN c.consumerId
		ELSE NULL
	  END AS brandPreChivas,
	  CASE 
		WHEN brandPreDewar = 'T' OR survey4aDewar IS NOT NULL THEN c.consumerId
		ELSE NULL
	  END AS brandPreDewar,
	  CASE 
		WHEN brandPreHennessy = 'T' OR survey4aHennessy IS NOT NULL THEN c.consumerId
		ELSE NULL
	  END AS brandPreHennessy,
	  CASE 
		WHEN brandPre100Pipers = 'T' OR survey4a100Pipers IS NOT NULL THEN c.consumerId
		ELSE NULL
	  END AS brandPre100Pipers,
	  CASE 
		WHEN brandPre100Pipers8Y = 'T' OR survey4a100Pipers8Y IS NOT NULL THEN c.consumerId
		ELSE NULL
	  END AS brandPre100Pipers8Y,
-- For Dashboard DB
	-- BOTH
	CASE 
		WHEN (
				(ISNULL(c.email, '') != '' AND ISNULL(c.blockEmail, 'F') = 'F') 
				AND (
					(ISNULL(c.mobile, '') != '') 
					AND ((ISNULL(c.blockCallCenter, 'F') = 'F' OR ISNULL(c.blockMobile, 'F') = 'F'))
				)
			) THEN consumerId
		ELSE NULL
	END AS both,
	-- EMAIL ONLY
	CASE
		WHEN (
				(ISNULL(c.email, '') != '' AND ISNULL(c.blockEmail, 'F') = 'F')
				AND (
					(ISNULL(c.blockCallCenter, 'F') = 'T' AND ISNULL(c.blockMobile, 'F') = 'T') OR ISNULL(c.mobile, '') = ''
				)
			) THEN consumerId
		ELSE NULL
	END AS emailOnly,
	-- MOBILE ONLY
	CASE
		WHEN (
				(
					ISNULL(c.mobile, '') != '' 
					AND ((ISNULL(c.blockCallCenter, 'F') = 'F' OR ISNULL(c.blockMobile, 'F') = 'F'))
				)
				AND (ISNULL(c.blockEmail, 'F') = 'T' OR ISNULL(c.email, '') = '')
			) THEN consumerId
		ELSE NULL
	END as mobileOnly,
	-- NO ALL
	CASE
		WHEN (
				(ISNULL(c.email, '') = '' OR ISNULL(c.blockEmail, 'F') = 'T') 
				AND (ISNULL(c.mobile, '') = '' OR (ISNULL(c.blockCallCenter, 'F') = 'T' AND ISNULL(c.blockMobile, 'F') = 'T'))
			) THEN consumerId
		ELSE NULL
	END AS no_all
	
FROM DiageoDB..Consumer AS c
	LEFT OUTER JOIN dbo.tbl_m_rpt_quarter_month AS qcm ON MONTH(c.createDate) = qcm.month_num 
	LEFT OUTER JOIN dbo.tbl_m_rpt_quarter_month AS qum ON MONTH(c.updateDate) = qum.month_num 
WHERE c.registerPlatformId = '0' 
OR c.registerPlatformId = '1' 

GO


