USE [DiageoDBTest]
GO
IF OBJECT_ID (N'dbo.vw_rpt_turnup_sent', N'VIEW') IS NOT NULL
    DROP VIEW vw_rpt_turnup_sent;
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_rpt_turnup_sent]
AS
	SELECT c.gender, c.dateOfBirth, o.consumerId, o.eventId, o.senddate, 
	CASE WHEN o.outboundTypeId = 1 THEN 1 ELSE NULL END EDM,
	CASE WHEN o.outboundTypeId = 2 THEN 1 ELSE NULL END SMS,
	CASE WHEN o.outboundTypeId = 3 THEN 1 ELSE NULL END CCenter, 
	CASE 
		WHEN c.brandPreJwGold = 'T' THEN 'JWG'
		WHEN c.brandPreJwBlack = 'T' THEN 'JWB'
		WHEN c.brandPreSmirnoff = 'T' THEN 'SMI'
		WHEN c.brandPreJwRed = 'T' THEN 'JWR'
		WHEN c.brandPreBenmore = 'T' THEN 'BEN'
		ELSE NULL
	END AS Brand,
	CASE 
/*++ Inferno 20131216 Change Priority of fall through case followed by user requirements */
/*		WHEN survey4aJwGold = '1'
			OR survey4aJwBlack = '1'
			OR survey4aSmirnoff = '1'
			OR survey4aJwRed = '1'
			OR survey4aBenmore = '1' THEN 1
		WHEN survey4aJwGold = '2'
			OR survey4aJwBlack = '2'
			OR survey4aSmirnoff = '2'
			OR survey4aJwRed = '2'
			OR survey4aBenmore = '2' THEN 2
/*++ Inferno 20131212 Change requrement 3 = Acceptor/ 4 = Available */
		WHEN survey4aJwGold = '3'
			OR survey4aJwBlack = '3'
			OR survey4aSmirnoff = '3'
			OR survey4aJwRed = '3'
			OR survey4aBenmore = '3' THEN 4
		WHEN survey4aJwGold = '4'
			OR survey4aJwBlack = '4'
			OR survey4aSmirnoff = '4'
			OR survey4aJwRed = '4'
			OR survey4aBenmore = '4' THEN 3
/*-- Inferno 20131212 Change requrement 3 = Acceptor/ 4 = Available */
		WHEN survey4aJwGold = '5'
			OR survey4aJwBlack = '5'
			OR survey4aSmirnoff = '5'
			OR survey4aJwRed = '5'
			OR survey4aBenmore = '5' THEN 5
		ELSE NULL
*/
		WHEN ISNULL(survey4aJwGold, '') IN ('1', '2', '3', '4', '5') THEN survey4aJwGold
		WHEN ISNULL(survey4aJwBlack, '') IN ('1', '2', '3', '4', '5') THEN survey4aJwBlack
		WHEN ISNULL(survey4aSmirnoff, '') IN ('1', '2', '3', '4', '5') THEN survey4aSmirnoff
		WHEN ISNULL(survey4aJwRed, '') IN ('1', '2', '3', '4', '5') THEN survey4aJwRed
		WHEN ISNULL(survey4aBenmore, '') IN ('1', '2', '3', '4', '5') THEN survey4aBenmore
		ELSE NULL
/*-- Inferno 20131216 Change Priority of fall through case followed by user requirements */
	END AS survey4a		
	FROM DiageoDB.dbo.Consumer c
		INNER JOIN DiageoDB.dbo.Outbound o on o.consumerId = c.consumerId

GO
