USE [DiageoDBTest]
GO
IF OBJECT_ID (N'dbo.vw_rpt_dashb_segment_by_quarter', N'VIEW') IS NOT NULL
    DROP VIEW vw_rpt_dashb_segment_by_quarter;
GO
/****** Object:  View [dbo].[vw_rpt_dashb_segment_by_quarter]    Script Date: 09/28/2013 15:57:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_rpt_dashb_segment_by_quarter]
AS
SELECT
	YEAR(c.updateDate) + (-1 * qum.year_shift) AS upd_quarterYear, qum.quarter AS upd_quarterNum, 
	c.consumerId,
/*++ Inferno 20131212 Change requrement 3 = Acceptor/ 4 = Available */
	CASE ISNUMERIC(c.survey4aBenmore) 
		WHEN 1 THEN 
			CASE 
				WHEN c.survey4aBenmore = 3 THEN 4
				WHEN c.survey4aBenmore = 4 THEN 3
				ELSE c.survey4aBenmore
			END
		ELSE NULL 
	END AS survey4aBenmore,
	CASE ISNUMERIC(c.survey4aJwBlack) 
		WHEN 1 THEN 
			CASE 
				WHEN c.survey4aJwBlack = 3 THEN 4
				WHEN c.survey4aJwBlack = 4 THEN 3
				ELSE c.survey4aJwBlack
			END
		ELSE NULL 
	END AS survey4aJwBlack,
	CASE ISNUMERIC(c.survey4aJwGold) 
		WHEN 1 THEN 
			CASE 
				WHEN c.survey4aJwGold = 3 THEN 4
				WHEN c.survey4aJwGold = 4 THEN 3
				ELSE c.survey4aJwGold
			END
		ELSE NULL 
	END AS survey4aJwGold,
	CASE ISNUMERIC(c.survey4aJwGreen) 
		WHEN 1 THEN 
			CASE 
				WHEN c.survey4aJwGreen = 3 THEN 4
				WHEN c.survey4aJwGreen = 4 THEN 3
				ELSE c.survey4aJwGreen
			END
		ELSE NULL 
	END AS survey4aJwGreen,
	CASE ISNUMERIC(c.survey4aJwRed) 
		WHEN 1 THEN 
			CASE 
				WHEN c.survey4aJwRed = 3 THEN 4
				WHEN c.survey4aJwRed = 4 THEN 3
				ELSE c.survey4aJwRed
			END
		ELSE NULL 
	END AS survey4aJwRed,
	CASE ISNUMERIC(c.survey4aSmirnoff) 
		WHEN 1 THEN 
			CASE 
				WHEN c.survey4aSmirnoff = 3 THEN 4
				WHEN c.survey4aSmirnoff = 4 THEN 3
				ELSE c.survey4aSmirnoff
			END
		ELSE NULL 
	END AS survey4aSmirnoff,
/*-- Inferno 20131212 Change requrement 3 = Acceptor/ 4 = Available */
	c.brandPreBenmore, c.brandPreJwBlack, c.brandPreJwGold, c.brandPreJwGreen, c.brandPreJwRed, c.brandPreSmirnoff,
	CASE
		WHEN c.brandPreJwGold = 'T' THEN 1
		WHEN c.brandPreJwBlack = 'T' THEN 2
		WHEN c.brandPreSmirnoff = 'T' THEN 3
		WHEN c.brandPreJwRed = 'T' THEN 4
		WHEN c.brandPreBenmore = 'T' THEN 5
		ELSE NULL
	END AS comsumerLevelBrand,
/*++ Inferno 20131212 Change requrement 3 = Acceptor/ 4 = Available */
	CASE
		CASE
			WHEN c.brandPreJwGold = 'T' THEN ISNULL(survey4aJwGold, '')
			WHEN c.brandPreJwBlack = 'T' THEN ISNULL(survey4aJwBlack, '')
			WHEN c.brandPreSmirnoff = 'T' THEN ISNULL(survey4aSmirnoff, '')
			WHEN c.brandPreJwRed = 'T' THEN ISNULL(survey4aJwRed, '')
			WHEN c.brandPreBenmore = 'T' THEN ISNULL(survey4aBenmore, '')
			ELSE NULL
		END
		/*WHEN 1 THEN '1'
		WHEN 2 THEN '2'
		WHEN 3 THEN '4'
		WHEN 4 THEN '3'
		WHEN 5 THEN '5'*/
		WHEN '1' THEN '1'
		WHEN '2' THEN '2'
		WHEN '3' THEN '4'
		WHEN '4' THEN '3'
		WHEN '5' THEN '5'
		ELSE NULL
	END AS comsumerLevel4A,
/*-- Inferno 20131212 Change requrement 3 = Acceptor/ 4 = Available */
	-- BOTH
	CASE 
		WHEN (
				(ISNULL(c.email, '') != '' AND ISNULL(c.blockEmail, 'F') = 'F') 
				AND (
					(ISNULL(c.mobile, '') != '') 
					AND ((ISNULL(c.blockCallCenter, 'F') = 'F' OR ISNULL(c.blockMobile, 'F') = 'F'))
				)
			) THEN consumerId
		ELSE NULL
	END AS both,
	-- EMAIL ONLY
	CASE
		WHEN (
				(ISNULL(c.email, '') != '' AND ISNULL(c.blockEmail, 'F') = 'F')
				AND (
					(ISNULL(c.blockCallCenter, 'F') = 'T' AND ISNULL(c.blockMobile, 'F') = 'T') OR ISNULL(c.mobile, '') = ''
				)
			) THEN consumerId
		ELSE NULL
	END AS emailOnly,
	-- MOBILE ONLY
	CASE
		WHEN (
				(
					ISNULL(c.mobile, '') != '' 
					AND ((ISNULL(c.blockCallCenter, 'F') = 'F' OR ISNULL(c.blockMobile, 'F') = 'F'))
				)
				AND (ISNULL(c.blockEmail, 'F') = 'T' OR ISNULL(c.email, '') = '')
			) THEN consumerId
		ELSE NULL
	END as mobileOnly,
	-- NO ALL
	CASE
		WHEN (
				(ISNULL(c.email, '') = '' OR ISNULL(c.blockEmail, 'F') = 'T') 
				AND (ISNULL(c.mobile, '') = '' OR (ISNULL(c.blockCallCenter, 'F') = 'T' AND ISNULL(c.blockMobile, 'F') = 'T'))
			) THEN consumerId
		ELSE NULL
	END AS no_all
	FROM DiageoDB..Consumer AS c
	INNER JOIN dbo.tbl_m_rpt_quarter_month AS qum ON MONTH(c.updateDate) = qum.month_num 

GO


