USE [DiageoDBTest]
GO
IF OBJECT_ID (N'dbo.vw_rpt_dashboardConsumer_by_quarter', N'VIEW') IS NOT NULL
    DROP VIEW vw_rpt_dashboardConsumer_by_quarter;
GO
/****** 
	Object:  View [dbo].[vw_rpt_dashboardConsumer_by_quarter]
    Script Date: 09/28/2013 15:57:50 
******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_rpt_dashboardConsumer_by_quarter]
AS
	SELECT c.consumerId, c.updatedate,
	CASE ISNUMERIC(c.survey4aJwBlack)
/*++ Inferno 20131212 Change requrement 3 = Acceptor/ 4 = Available */
		--WHEN 1 THEN c.survey4aJwBlack
		WHEN 1 THEN 
			CASE 
				WHEN c.survey4aJwBlack = 3 THEN 4
				WHEN c.survey4aJwBlack = 4 THEN 3
				ELSE c.survey4aJwBlack
			END
/*-- Inferno 20131212 Change requrement 3 = Acceptor/ 4 = Available */
		ELSE NULL
	END AS survey4aJwBlack, 
	CASE ISNUMERIC(c.survey4aJwRed)
		WHEN 1 THEN 
			CASE 
				WHEN c.survey4aJwRed = 3 THEN 4
				WHEN c.survey4aJwRed = 4 THEN 3
				ELSE c.survey4aJwRed
			END
		ELSE NULL
	END AS survey4aJwRed, 
	CASE ISNUMERIC(c.survey4aSmirnoff)
		WHEN 1 THEN 
			CASE 
				WHEN c.survey4aSmirnoff = 3 THEN 4
				WHEN c.survey4aSmirnoff = 4 THEN 3
				ELSE c.survey4aSmirnoff
			END
		ELSE NULL
	END AS survey4aSmirnoff,
	c.brandPreJwBlack, c.brandPreJwRed, c.brandPreSmirnoff,
	YEAR(c.createDate) + (-1 * qcm.year_shift) AS cre_quarterYear, qcm.quarter AS cre_quarterNum,
	YEAR(c.updateDate) + (-1 * qum.year_shift) AS upd_quarterYear, qum.quarter AS upd_quarterNum,
	CASE
		WHEN (c.subScribeEmail_DG = 'T'
			OR c.subScribeSMS_DG = 'T' 
			OR c.subScribeCallCenter_DG = 'T') THEN c.consumerId
		ELSE NULL
	END AS subscribe,
	CASE
		WHEN c.subScribeEmail_DG != 'T' THEN c.consumerId
		ELSE NULL
	END AS unsubscribe_email,
	CASE
		WHEN (c.subScribeEmail_DG != 'T' OR c.blockEmail ='T') THEN c.consumerId
		ELSE NULL
	END AS invalid_email,
	CASE
		WHEN (c.blockMobile ='T' 
			OR c.subScribeSMS_DG != 'T' 
			OR c.subScribeCallCenter_DG != 'T') THEN c.consumerId
		ELSE NULL
	END AS invalid_mobile,
-- For Dashboard DB
	-- BOTH
	CASE 
		WHEN (
				(ISNULL(c.email, '') != '' AND ISNULL(c.blockEmail, 'F') = 'F') 
				AND (
					(ISNULL(c.mobile, '') != '') 
					AND ((ISNULL(c.blockCallCenter, 'F') = 'F' OR ISNULL(c.blockMobile, 'F') = 'F'))
				)
			) THEN consumerId
		ELSE NULL
	END AS both,
	-- EMAIL ONLY
	CASE
		WHEN (
				(ISNULL(c.email, '') != '' AND ISNULL(c.blockEmail, 'F') = 'F')
				AND (
					(ISNULL(c.blockCallCenter, 'F') = 'T' AND ISNULL(c.blockMobile, 'F') = 'T') OR ISNULL(c.mobile, '') = ''
				)
			) THEN consumerId
		ELSE NULL
	END AS emailOnly,
	-- MOBILE ONLY
	CASE
		WHEN (
				(
					ISNULL(c.mobile, '') != '' 
					AND ((ISNULL(c.blockCallCenter, 'F') = 'F' OR ISNULL(c.blockMobile, 'F') = 'F'))
				)
				AND (ISNULL(c.blockEmail, 'F') = 'T' OR ISNULL(c.email, '') = '')
			) THEN consumerId
		ELSE NULL
	END as mobileOnly,
	-- NO ALL
	CASE
		WHEN (
				(ISNULL(c.email, '') = '' OR ISNULL(c.blockEmail, 'F') = 'T') 
				AND (ISNULL(c.mobile, '') = '' OR (ISNULL(c.blockCallCenter, 'F') = 'T' AND ISNULL(c.blockMobile, 'F') = 'T'))
			) THEN consumerId
		ELSE NULL
	END AS no_all
	
	FROM DiageoDB..Consumer AS c 
        --INNER JOIN DiageoDB..PlatformMember AS p ON c.consumerId = p.consumerId
		LEFT OUTER JOIN dbo.tbl_m_rpt_quarter_month AS qcm ON MONTH(c.createDate) = qcm.month_num 
		LEFT OUTER JOIN dbo.tbl_m_rpt_quarter_month AS qum ON MONTH(c.updateDate) = qum.month_num 
GO