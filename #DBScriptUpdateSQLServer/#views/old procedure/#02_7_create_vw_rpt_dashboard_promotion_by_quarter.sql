USE [DiageoDBTest]
GO
IF OBJECT_ID (N'dbo.vw_rpt_dashboard_promotion_by_quarter', N'VIEW') IS NOT NULL
    DROP VIEW vw_rpt_dashboard_promotion_by_quarter;
GO
/****** Object:  View [dbo].[vw_rpt_dashboard_promotion_by_quarter]    Script Date: 09/28/2013 15:57:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_rpt_dashboard_promotion_by_quarter]
AS
SELECT  
			redeem_consumer,
			YEAR(rd.Redeem_TransactionDate) + (-1 * qrm.year_shift) AS quarterYear,
			qrm.month_num AS quarterNum
FROM         SmartTouchDB..ST_tsRedeem rd
				INNER JOIN SmartTouchDB..ST_tsConsumer c ON rd.Redeem_Consumer = c.Con_ID
				LEFT OUTER JOIN dbo.tbl_m_rpt_quarter_month AS qrm ON MONTH(rd.Redeem_TransactionDate) = qrm.month_num

GO


