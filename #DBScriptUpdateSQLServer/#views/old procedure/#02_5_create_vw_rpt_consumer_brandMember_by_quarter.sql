USE [DiageoDBTest]
GO
IF OBJECT_ID (N'dbo.vw_rpt_consumer_brandMember_by_quarter', N'VIEW') IS NOT NULL
    DROP VIEW vw_rpt_consumer_brandMember_by_quarter;
GO
/****** Object:  View [dbo].[vw_rpt_consumer_brandMember_by_quarter]    Script Date: 09/28/2013 15:57:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].vw_rpt_consumer_brandMember_by_quarter
AS
SELECT  
                      c.consumerId, b.brandId, 
                      /*c.brandPre100Pipers, c.brandPre100Pipers8Y, c.brandPreAbsolut, c.brandPreBaileys, c.brandPreBallentine, c.brandPreBenmore, c.brandPreBlend, 
                      c.brandPreChivas, c.brandPreDewar, c.brandPreHennessy, c.brandPreJwBlack, c.brandPreJwGold, c.brandPreJwGreen, c.brandPreJwRed, c.brandPreSmirnoff, 
                      c.brandPreNotdrink, c.brandPreText, c.brandPreOther, */
                      c.survey4aBaileys, c.survey4aBenmore, c.survey4aJwBlack, c.survey4aJwGold, c.survey4aJwGreen, 
                      c.survey4aJwRed, c.survey4aSmirnoff, c.survey4aAbsolut, c.survey4aBallentine, c.survey4aBlend, c.survey4aChivas, c.survey4aDewar, c.survey4aHennessy, 
                      c.survey4a100Pipers, c.survey4a100Pipers8Y, c.survey4aOthers, c.survey4aNotdrink, c.survey4aText, c.surveyP4wBaileys, c.surveyP4wBenmore, 
                      c.surveyP4wJwBlack, c.surveyP4wJwGold, c.surveyP4wJwGreen, c.surveyP4wJwRed, c.surveyP4wSmirnoff, c.surveyP4wNonAbsolute, c.surveyP4wNonBallentine, 
                      c.surveyP4wNonBlend, c.surveyP4wNonChivas, c.surveyP4wNonDewar, c.surveyP4wNonHennessy, c.surveyP4wNon100Pipers, c.surveyP4wNon100Pipers8yrs, 
                      c.surveyP4wNonOthers, c.surveyP4wNonNotdrink, c.surveyP4wNonText,
                      /*CASE
						WHEN (c.subScribeCallCenter_DG = 'T' OR c.subScribeCallCenter_Outlet='T' 
							OR c.subScribeEmail_DG='T' OR c.subScribeEmail_Outlet='T') THEN c.consumerId 
						ELSE NULL
					  END AS subscribe,
					  CASE
					    WHEN (c.subScribeEmail_DG='F') THEN c.consumerId
					    ELSE NULL
					  END AS unsubscribe_email,
					  CASE 
						WHEN (c.activeEmail='F') THEN c.consumerId
						ELSE NULL
					  END AS invalid_email,
					  CASE
					    WHEN (c.activeCallCenter='F' OR c.blockCallCenter='T') THEN c.consumerId
					    ELSE NULL
					  END AS invalid_mobile,
					  CASE
					    WHEN (activeMobile = 'F' OR blockMobile = 'T') THEN c.consumerId
					    ELSE NULL
					  END AS invalid_block_mobile,*/
					  /*CASE
					    WHEN (
							(c.activeCallCenter='T' AND c.blockCallCenter='F') 
							OR (c.activeEmail='T' AND c.blockEmail='F')
							) THEN c.consumerId
						ELSE NULL
					  END AS dashboard_segment,*/
					  /*CASE 
						WHEN brandPreJwBlack = 'T' THEN c.consumerId
						ELSE NULL
					  END AS brandPreJwBlack,
					  CASE 
						WHEN brandPreJwRed = 'T' THEN c.consumerId
						ELSE NULL
					  END AS brandPreJwRed,
					  CASE 
						WHEN brandPreJwGreen = 'T' THEN c.consumerId
						ELSE NULL
					  END AS brandPreJwGreen,
					  CASE 
						WHEN brandPreJwGold = 'T' THEN c.consumerId
						ELSE NULL
					  END AS brandPreJwGold,
					  CASE 
						WHEN brandPreSmirnoff = 'T' THEN c.consumerId
						ELSE NULL
					  END AS brandPreSmirnoff,
					  CASE 
						WHEN brandPreBaileys = 'T' THEN c.consumerId
						ELSE NULL
					  END AS brandPreBaileys,
					  CASE 
						WHEN brandPreBallentine = 'T' THEN c.consumerId
						ELSE NULL
					  END AS brandPreBallentine,
					  CASE 
						WHEN brandPreBenmore = 'T' THEN c.consumerId
						ELSE NULL
					  END AS brandPreBenmore,
					  CASE 
						WHEN brandPreAbsolut = 'T' THEN c.consumerId
						ELSE NULL
					  END AS brandPreAbsolut,
					  CASE 
						WHEN brandPreBlend = 'T' THEN c.consumerId
						ELSE NULL
					  END AS brandPreBlend,
					  CASE 
						WHEN brandPreChivas = 'T' THEN c.consumerId
						ELSE NULL
					  END AS brandPreChivas,
					  CASE 
						WHEN brandPreDewar = 'T' THEN c.consumerId
						ELSE NULL
					  END AS brandPreDewar,
					  CASE 
						WHEN brandPreHennessy = 'T' THEN c.consumerId
						ELSE NULL
					  END AS brandPreHennessy,
					  CASE 
						WHEN brandPre100Pipers = 'T' THEN c.consumerId
						ELSE NULL
					  END AS brandPre100Pipers,
					  CASE 
						WHEN brandPre100Pipers8Y = 'T' THEN c.consumerId
						ELSE NULL
					  END AS brandPre100Pipers8Y,
					  CASE 
						WHEN (brandPreOther = 'T' /*AND LEN(brandPreText) > 0*/) THEN c.consumerId
						ELSE NULL
					  END AS brandPreOther,
					  CASE
					    WHEN (brandPreAbsolut = 'T' OR brandPreBallentine = 'T' AND brandPreBlend = 'T' 
							OR brandPreChivas = 'T' OR brandPreDewar = 'T' OR brandPreHennessy = 'T' 
							OR brandPre100Pipers = 'T' OR brandPre100Pipers8Y = 'T' ) THEN c.consumerId
					    ELSE NULL
					  END AS brandPreCompetitors,*/
			          YEAR(c.registerDate) + (-1 * qm.year_shift) AS quarterYear, 
                      qm.quarter AS quarterNum, 
					  CASE 
					    WHEN (
							(c.activeEmail = 'T' OR c.blockemail = 'F' OR c.email IS NOT NULL) 
							OR (c.activeMobile = 'T' OR c.blockMobile = 'F' OR c.mobile IS NOT NULL)
							--OR (c.activeCallCenter = 'T' AND c.blockCallCenter = 'F')
					      ) THEN c.consumerId 
						ELSE NULL
					  END AS contactable, 
                      CASE 
						WHEN (
						    (c.activeEmail = 'F' OR c.blockEmail = 'T' OR c.email IS NULL)
							AND (c.activeMobile = 'T' OR c.blockMobile = 'F' OR mobile IS NOT NULL)
							/*AND (c.activeCallCenter = 'T' AND c.blockCallCenter = 'F')*/
						  ) THEN c.consumerId  
					    ELSE NULL 
					  END AS mobileOnly, 
                      CASE 
						WHEN (
						    (c.activeEmail = 'T' OR c.blockEmail = 'F' OR email IS NOT NULL)
							AND (c.activeMobile = 'F' OR c.blockMobile = 'T' OR mobile IS NULL)
							/*AND (c.activeCallCenter = 'F' AND c.blockCallCenter = 'T')*/	
						  ) THEN c.consumerId 
						ELSE NULL
					  END AS emailOnly, 
					  CASE 
					    WHEN (
						    c.activeEmail = 'T' OR c.blockEmail = 'F' OR email IS NOT NULL
							AND c.activeMobile = 'T' OR c.blockMobile = 'F' OR mobile IS NOT NULL
							/*AND c.activeCallCenter = 'F' AND c.blockCallCenter = 'T' AND mobile IS NOT NULL*/	
						  ) THEN c.consumerId  
						ELSE NULL
					  END AS mobileEmail
FROM         DiageoDB..Consumer AS c 
                INNER JOIN DiageoDB..BrandMember b on c.consumerId = b.consumerId
				INNER JOIN dbo.tbl_m_rpt_quarter_month AS qm ON MONTH(c.registerDate) = qm.month_num 
GO


