USE [DiageoDB]
GO
/****** Object:  StoredProcedure [dbo].[Sp_OutboundConfig_Search]    Script Date: 11/09/2013 02:25:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Sp_OutboundConfig_Search]
	@platformId INT,
	@outletId INT,
	@eventId INT,
	@outboundTypeId INT,
	@active char(1),
	@StartRowIndex INT,
	@MaximumRows INT,
	@SortExpression VARCHAR(100),
	@ISCount INT,
	@ErrorCode INT OUTPUT
AS
BEGIN

	DECLARE @Sql AS NVARCHAR(MAX)
	DECLARE @Where AS NVARCHAR(MAX)
	
	SET @Sql = ''
	SET @Where = ' WHERE 1=1 '
	
	IF(@SortExpression IS NULL OR @SortExpression = '')
	BEGIN
			SET @SortExpression = 'outboundConfigId' 	
	END
	
	IF(@platformId <> 0)
	BEGIN
			SET @Where += ' AND [OutboundConfig].platformId = ' + Convert(VARCHAR,@platformId)
	END
	
	IF(@outletId <> 0)
	BEGIN
			SET @Where += ' AND [OutboundConfig].outletId = ' + Convert(VARCHAR,@outletId)
	END
	
	IF(@eventId <> 0)
	BEGIN
			SET @Where += ' AND [OutboundConfig].eventId = ' + Convert(VARCHAR,@eventId)
	END
	
	IF(@outboundTypeId <> 0)
	BEGIN
			SET @Where += ' AND [OutboundConfig].outboundTypeId = ' + Convert(VARCHAR,@outboundTypeId)
	END
	
	IF(@Active IS NOT NULL AND @Active <> '')
	BEGIN
			SET @Where += ' AND [OutboundConfig].active = ''' + @Active + ''''
	END	
	
	IF(@ISCount = 0)
	BEGIN
	
			SET @Sql = '
						SELECT
									[OutboundConfig].*,
									[Platform].name AS PlatformName,
									[Outlet].name AS OutletName,
									[OutboundEventMaster].EventName AS OutboundEventName,
									[OutboundType].name AS OutboundTypeName,
									CASE
										WHEN [OutboundConfig].active = ''T'' THEN ''True''
										WHEN [OutboundConfig].active = ''F'' THEN ''False''
									END AS ActiveWord
						FROM		[OutboundConfig] 
						Left join [Platform] on [Platform].platformId = [OutboundConfig].platformId
						Left join [Outlet] on [Outlet].outletId = [OutboundConfig].outletId
						Left join [OutboundEventMaster] on [OutboundEventMaster].eventId = [OutboundConfig].eventId
						Left join [OutboundType] on [OutboundType].outboundTypeId = [OutboundConfig].outboundTypeId
						' + @Where
						
			SET @Sql = '
						SELECT *
						FROM ( SELECT *,ROW_NUMBER() OVER (ORDER BY ' + @SortExpression + ') AS RowRank '
						 + ' From ( ' + @Sql + ' ) AS MSAGENTMASTER '
						 + ' ) AS MSAGENTMASTERWITHROWNUMBER
						WHERE     RowRank > ' + CONVERT(nvarchar(10), @StartRowIndex) + 
							' AND RowRank <= (' + CONVERT(nvarchar(10), @StartRowIndex) + ' + ' 
							+ CONVERT(nvarchar(10), @MaximumRows) + ')'
													
	END
	ELSE
	BEGIN
	
			SET @Sql = '
						SELECT
								COUNT(*)
						FROM	[OutboundConfig] ' + @Where
							
	END
	
	PRINT @Sql
	EXECUTE(@Sql)
	SELECT @ErrorCode=@@ERROR

END
