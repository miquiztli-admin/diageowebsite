USE [DiageoDB]
GO
select * into UserMenu_BuffBKUP20131109 from usermenu
GO
truncate table UserMenu
GO
/****** Object:  Table [dbo].[UserMenu]    Script Date: 11/09/2013 19:26:24 ******/
SET IDENTITY_INSERT [dbo].[UserMenu] ON
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (1, 1, N'System Configuration', NULL, CAST(0x0000A03100000000 AS DateTime), N'admin', CAST(0x0000A03100000000 AS DateTime), N'admin', NULL, NULL)
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (2, 1, N'Users', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (3, 1, N'Upload Data', NULL, CAST(0x0000A03100000000 AS DateTime), N'admin', CAST(0x0000A03100000000 AS DateTime), N'admin', NULL, NULL)
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (4, 1, N'Extract Data', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (5, 1, N'Report', NULL, CAST(0x0000A03100000000 AS DateTime), N'admin', CAST(0x0000A03100000000 AS DateTime), N'admin', NULL, NULL)
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (6, 1, N'Logout', N'Login.aspx', CAST(0x0000A03100000000 AS DateTime), N'admin', CAST(0x0000A03100000000 AS DateTime), N'admin', NULL, NULL)
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (7, 0, N'User Account', N'Security/UserSearch.aspx', CAST(0x0000A03100000000 AS DateTime), N'admin', CAST(0x0000A03100000000 AS DateTime), N'admin', NULL, N'2')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (8, 0, N'User Group', N'Security/UserGroupSearch.aspx', CAST(0x0000A03100000000 AS DateTime), N'admin', CAST(0x0000A03100000000 AS DateTime), N'admin', NULL, N'2')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (9, 0, N'Access Group', N'Security/UserAccess.aspx', CAST(0x0000A03100000000 AS DateTime), N'admin', CAST(0x0000A03100000000 AS DateTime), N'admin', NULL, N'2')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (10, 0, N'Outlet', N'Master/OutletSearch.aspx', CAST(0x0000A03D00000000 AS DateTime), N'admin', CAST(0x0000A03D00000000 AS DateTime), N'admin', NULL, N'1')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (11, 0, N'4A', N'Master/SurveyTypeSearch.aspx', CAST(0x0000A03E00000000 AS DateTime), N'admin', CAST(0x0000A03E00000000 AS DateTime), N'admin', NULL, N'1')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (12, 0, N'Province', N'Master/ProvinceSearch.aspx', CAST(0x0000A03E00000000 AS DateTime), N'admin', CAST(0x0000A03E00000000 AS DateTime), N'admin', NULL, N'1')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (14, 0, N'Platform', N'Master/PlatformSearch.aspx', CAST(0x0000A03E00000000 AS DateTime), N'admin', CAST(0x0000A03E00000000 AS DateTime), N'admin', NULL, N'1')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (15, 0, N'OutboundConfig', N'Master/OutboundConfigSearch.aspx', CAST(0x0000A04000000000 AS DateTime), N'admin', CAST(0x0000A04000000000 AS DateTime), N'admin', NULL, N'1')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (16, 0, N'Consumer', N'Master/ConsumerSearch.aspx', CAST(0x0000A06200000000 AS DateTime), N'1', CAST(0x0000A06200000000 AS DateTime), N'1', NULL, N'1')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (17, 0, N'Birthday', N'Master/OutboundEventBirthDaySearch.aspx', CAST(0x0000A06C00000000 AS DateTime), N'1', CAST(0x0000A06C00000000 AS DateTime), N'1', NULL, N'1')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (18, 0, N'Event', N'Master/OutboundEventSearch.aspx', CAST(0x0000A06C00000000 AS DateTime), N'1', CAST(0x0000A06C00000000 AS DateTime), N'1', NULL, N'1')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (19, 0, N'Smart touch App - Data Upload (CSV)', N'ExtractData/UploadCSVFromIPad.aspx', CAST(0x0000A09100000000 AS DateTime), N'1', CAST(0x0000A09100000000 AS DateTime), N'1', NULL, N'3')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (20, 0, N'Call Center - Upload Data', N'ExtractData/UploadFile.aspx', CAST(0x0000A03100000000 AS DateTime), N'admin', CAST(0x0000A03100000000 AS DateTime), N'admin', NULL, N'3')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (21, 0, N'Extract Consumer', N'ExtractData/ExtractionConsumersData.aspx?mode=add&FromPage=Extract', CAST(0x0000A03100000000 AS DateTime), N'admin', CAST(0x0000A03100000000 AS DateTime), N'admin', NULL, N'4')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (22, 0, N'Consumer master data (raw data)', N'ExtractData/ExtracConsumerMasterData.aspx', NULL, NULL, NULL, NULL, NULL, N'4')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (23, 0, N'Campaign specific data', N'ExtractData/ExtractCampaignSpecificData.aspx', NULL, NULL, NULL, NULL, NULL, N'4')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (24, 0, N'Communication Report', N'Reports/CommunicationReport.aspx', NULL, NULL, NULL, NULL, NULL, N'5')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (25, 0, N'Dashboard Report', N'Reports/DashboardReport.aspx', CAST(0x0000A09100000000 AS DateTime), N'1', CAST(0x0000A09100000000 AS DateTime), N'1', NULL, N'5')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (26, 0, N'Performance Report', N'Reports/ReportPerformance.aspx', CAST(0x0000A09100000000 AS DateTime), N'1', CAST(0x0000A09100000000 AS DateTime), N'1', NULL, N'5')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (27, 0, N'Data Entry Performance Detail', N'Reports/Report_DataEntryPerformanceDetail.aspx', CAST(0x0000A22D00000000 AS DateTime), N'1', CAST(0x0000A22C00000000 AS DateTime), N'1', NULL, N'5')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (28, 0, N'Comparing Data Entry Performance', N'Reports/Report_ComparingDataEntryPerformancel.aspx', CAST(0x0000A22D00000000 AS DateTime), N'1', CAST(0x0000A22C00000000 AS DateTime), N'1', NULL, N'5')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (29, 0, N'Total Commission by Outlet', N'Reports/Report_TotalCommissionbyOutlet.aspx', CAST(0x0000A22D00000000 AS DateTime), N'1', CAST(0x0000A22C00000000 AS DateTime), N'1', NULL, N'5')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (30, 0, N'Total Commission by Data Entry', N'Reports/Report_TotalCommissionbyDataEntry.aspx', CAST(0x0000A22D00000000 AS DateTime), N'1', CAST(0x0000A22C00000000 AS DateTime), N'1', NULL, N'5')
INSERT [dbo].[UserMenu] ([menuId], [isParent], [name], [url], [createDate], [createBy], [updateDate], [updateBy], [flag], [parentID]) VALUES (31, 0, N'Daily Data Entry Performance', N'Reports/Report_DailyDataEntryPerformance.aspx', CAST(0x0000A22D00000000 AS DateTime), N'1', CAST(0x0000A22C00000000 AS DateTime), N'1', NULL, N'5')
SET IDENTITY_INSERT [dbo].[UserMenu] OFF

-- Delete all unused menu
delete 
from UserAccess
where groupid = 1
and menuid not in (select menuid from usermenu)

-- Add all new menu
insert into UserAccess
select menuid, 1, 'T', 'T', 'T', 'T', 'T', null, 'Buff', null, null, null, 'T', 'T'
from usermenu
where menuid not in (select menuid from UserAccess where groupid = 1)


select * from UserAccess where groupid = 1

select * from usermenu