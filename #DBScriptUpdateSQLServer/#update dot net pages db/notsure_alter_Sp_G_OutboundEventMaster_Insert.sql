USE [DiageoDB]
GO
/****** Object:  StoredProcedure [dbo].[Sp_G_OutboundEventMaster_Insert]    Script Date: 11/09/2013 15:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'OutboundEventMaster'
-- Gets: @eventName varchar(100)
-- Gets: @eventDateFrom datetime
-- Gets: @eventDateTo datetime
-- Gets: @excludeBlockList char(1)
-- Gets: @createDate datetime
-- Gets: @createBy varchar(100)
-- Gets: @updateDate datetime
-- Gets: @updateBy varchar(100)
-- Gets: @flag varchar(100)
-- Returns: @eventId int
-- Returns: @ErrorCode int
---------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[Sp_G_OutboundEventMaster_Insert]
	@eventName varchar(100),
	@eventDateFrom datetime,
	@eventDateTo datetime,
	@excludeBlockList char(1),
	@createDate datetime,
	@createBy varchar(100),
	@updateDate datetime,
	@updateBy varchar(100),
	@flag varchar(100),
	@IncludeDiageoVIP Varchar(1),
	@IncludeOutletVIP Varchar(1),
	@IncludeOutletVIPList Varchar(1),
	@ExcludeOtherOutletVIP Varchar(1),
	@RecordPriority Varchar(1),
	@CustomerMaxRepeat Int,
	@eventId int OUTPUT,
	@ErrorCode int OUTPUT
AS
-- INSERT a new row in the table.
INSERT [dbo].[OutboundEventMaster]
(
	[eventName],
	[eventDateFrom],
	[eventDateTo],
	[excludeBlockList],
	[createDate],
	[createBy],
	[updateDate],
	[updateBy],
	[flag]
)
VALUES
(
	@eventName,
	@eventDateFrom,
	@eventDateTo,
	@excludeBlockList,
	@createDate,
	@createBy,
	@updateDate,
	@updateBy,
	@flag
)
-- Get the Error Code for the statement just executed.
SELECT @ErrorCode=@@ERROR
-- Get the IDENTITY value for the row just inserted.
SELECT @eventId=SCOPE_IDENTITY()
