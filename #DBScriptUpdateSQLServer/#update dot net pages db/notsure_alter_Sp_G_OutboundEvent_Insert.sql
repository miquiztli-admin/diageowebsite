USE [DiageoDB]
GO
/****** Object:  StoredProcedure [dbo].[Sp_G_OutboundEvent_Insert]    Script Date: 11/09/2013 19:03:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------
-- Stored procedure that will insert 1 row in the table 'OutboundEvent'
-- Gets: @eventId int
-- Gets: @dataGroup varchar(100)
-- Gets: @name varchar(100)
-- Gets: @senderName varchar(100)
-- Gets: @brandId int
-- Gets: @brandPreJwGold char(1)
-- Gets: @brandPreJwBlack char(1)
-- Gets: @brandPreSmirnoff char(1)
-- Gets: @brandPreJwRed char(1)
-- Gets: @brandPreJwGreen char(1)
-- Gets: @brandPreBenmore char(1)
-- Gets: @brandPreBaileys char(1)
-- Gets: @brandPreAbsolut char(1)
-- Gets: @brandPreBallentine char(1)
-- Gets: @brandPreBlend char(1)
-- Gets: @brandPreChivas char(1)
-- Gets: @brandPreDewar char(1)
-- Gets: @brandPre100Pipers char(1)
-- Gets: @brandPre100Pipers8Y char(1)
-- Gets: @brandPreHennessy char(1)
-- Gets: @brandPreOther char(1)
-- Gets: @brandPreNotDrink char(1)
-- Gets: @gender varchar(100)
-- Gets: @validOptInDiageo char(1)
-- Gets: @validOptInOutlet char(1)
-- Gets: @startAge int
-- Gets: @endAge int
-- Gets: @monthJan char(1)
-- Gets: @monthFeb char(1)
-- Gets: @monthMar char(1)
-- Gets: @monthApr char(1)
-- Gets: @monthMay char(1)
-- Gets: @monthJun char(1)
-- Gets: @monthJul char(1)
-- Gets: @monthAug char(1)
-- Gets: @monthSep char(1)
-- Gets: @monthOct char(1)
-- Gets: @monthNov char(1)
-- Gets: @monthDec char(1)
-- Gets: @openEmailInLastTime int
-- Gets: @receiveEmailInLessTime int
-- Gets: @emailActive char(1)
-- Gets: @mobileActive char(1)
-- Gets: @active char(1)
-- Gets: @emailRate decimal(18, 0)
-- Gets: @includeSMSBounce char(1)
-- Gets: @includeEDMBounce char(1)
-- Gets: @includeCallcenterBounce char(1)
-- Gets: @createDate datetime
-- Gets: @createBy varchar(100)
-- Gets: @updateDate datetime
-- Gets: @updateBy varchar(100)
-- Gets: @smsflag varchar(100)
-- Gets: @edmflag varchar(100)
-- Gets: @callcenterflag varchar(100)
-- Gets: @bdayflag varchar(100)
-- Gets: @startDate datetime
-- Gets: @endDate datetime
-- Returns: @ErrorCode int
---------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[Sp_G_OutboundEvent_Insert]
	@eventId int,
	@dataGroup varchar(100),
	@name varchar(100),
	@senderName varchar(100),
	@brandId int,
	@brandPreJwGold char(1),
	@brandPreJwBlack char(1),
	@brandPreSmirnoff char(1),
	@brandPreJwRed char(1),
	@brandPreJwGreen char(1),
	@brandPreBenmore char(1),
	@brandPreBaileys char(1),
	@brandPreAbsolut char(1),
	@brandPreBallentine char(1),
	@brandPreBlend char(1),
	@brandPreChivas char(1),
	@brandPreDewar char(1),
	@brandPre100Pipers char(1),
	@brandPre100Pipers8Y char(1),
	@brandPreHennessy char(1),
	@brandPreOther char(1),
	@brandPreNotDrink char(1),
	@gender varchar(100),
	@validOptInDiageo char(1),
	@validOptInOutlet char(1),
	@startAge int,
	@endAge int,
	@monthJan char(1),
	@monthFeb char(1),
	@monthMar char(1),
	@monthApr char(1),
	@monthMay char(1),
	@monthJun char(1),
	@monthJul char(1),
	@monthAug char(1),
	@monthSep char(1),
	@monthOct char(1),
	@monthNov char(1),
	@monthDec char(1),
	@openEmailInLastTime int,
	@receiveEmailInLessTime int,
	@emailActive char(1),
	@mobileActive char(1),
	@active char(1),
	@emailRate decimal(18, 0),
	@includeSMSBounce char(1),
	@includeEDMBounce char(1),
	@includeCallcenterBounce char(1),
	@createDate datetime,
	@createBy varchar(100),
	@updateDate datetime,
	@updateBy varchar(100),
	@smsflag varchar(100),
	@edmflag varchar(100),
	@callcenterflag varchar(100),
	@bdayflag varchar(100),
	@startDate datetime,
	@endDate datetime,
	@PlatformIDList Varchar(400),  -- Start Buff Added to match with .net code calling
	@OutletIDList Varchar(100),
	@ProvinceList Varchar(1000),
	@ActivePeriodDateFrom datetime,
	@ActivePeriodDateTo datetime,
	@Adorer Varchar(1),
	@Adopter Varchar(1),
	@Acceptor Varchar(1),
	@Available Varchar(1),
	@Rejector Varchar(1),
	@EDMValid Varchar(1),
	@SMSValid Varchar(1),
	@CallCenterValid Varchar(1),
	@BDRangeFrom datetime,
	@BDRangeTo datetime, -- End Buff Added
	@ErrorCode int OUTPUT
AS
-- INSERT a new row in the table.
INSERT [dbo].[OutboundEvent]
(
	[eventId],
	[dataGroup],
	[name],
	[senderName],
	[brandId],
	[brandPreJwGold],
	[brandPreJwBlack],
	[brandPreSmirnoff],
	[brandPreJwRed],
	[brandPreJwGreen],
	[brandPreBenmore],
	[brandPreBaileys],
	[brandPreAbsolut],
	[brandPreBallentine],
	[brandPreBlend],
	[brandPreChivas],
	[brandPreDewar],
	[brandPre100Pipers],
	[brandPre100Pipers8Y],
	[brandPreHennessy],
	[brandPreOther],
	[brandPreNotDrink],
	[gender],
	[validOptInDiageo],
	[validOptInOutlet],
	[startAge],
	[endAge],
	[monthJan],
	[monthFeb],
	[monthMar],
	[monthApr],
	[monthMay],
	[monthJun],
	[monthJul],
	[monthAug],
	[monthSep],
	[monthOct],
	[monthNov],
	[monthDec],
	[openEmailInLastTime],
	[receiveEmailInLessTime],
	[emailActive],
	[mobileActive],
	[active],
	[emailRate],
	[includeSMSBounce],
	[includeEDMBounce],
	[includeCallcenterBounce],
	[createDate],
	[createBy],
	[updateDate],
	[updateBy],
	[smsflag],
	[edmflag],
	[callcenterflag],
	[bdayflag],
	[startDate],
	[endDate]
)
VALUES
(
	@eventId,
	@dataGroup,
	@name,
	@senderName,
	@brandId,
	@brandPreJwGold,
	@brandPreJwBlack,
	@brandPreSmirnoff,
	@brandPreJwRed,
	@brandPreJwGreen,
	@brandPreBenmore,
	@brandPreBaileys,
	@brandPreAbsolut,
	@brandPreBallentine,
	@brandPreBlend,
	@brandPreChivas,
	@brandPreDewar,
	@brandPre100Pipers,
	@brandPre100Pipers8Y,
	@brandPreHennessy,
	@brandPreOther,
	@brandPreNotDrink,
	@gender,
	@validOptInDiageo,
	@validOptInOutlet,
	@startAge,
	@endAge,
	@monthJan,
	@monthFeb,
	@monthMar,
	@monthApr,
	@monthMay,
	@monthJun,
	@monthJul,
	@monthAug,
	@monthSep,
	@monthOct,
	@monthNov,
	@monthDec,
	@openEmailInLastTime,
	@receiveEmailInLessTime,
	@emailActive,
	@mobileActive,
	@active,
	@emailRate,
	@includeSMSBounce,
	@includeEDMBounce,
	@includeCallcenterBounce,
	@createDate,
	@createBy,
	@updateDate,
	@updateBy,
	@smsflag,
	@edmflag,
	@callcenterflag,
	@bdayflag,
	@startDate,
	@endDate
)
-- Get the Error Code for the statement just executed.
SELECT @ErrorCode=@@ERROR
