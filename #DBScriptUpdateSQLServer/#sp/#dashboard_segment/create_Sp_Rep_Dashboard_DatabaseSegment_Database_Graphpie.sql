USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_DatabaseSegment_Database_Graphpie]    Script Date: 10/02/2013 14:01:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_DatabaseSegment_Database_Graphpie', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_DatabaseSegment_Database_Graphpie;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <02/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_DatabaseSegment_Database_Graphpie](@YYYY as integer)
AS
BEGIN

	Declare @EmailBrandYearNew bigint, @MobileBrandYearNew bigint, @BothBrandYearNew bigint

	DECLARE @ResultTable TABLE (name varchar(20),value bigint)

	SELECT
	@BothBrandYearNew = COUNT(brandPreJwGold) + COUNT(brandPreJwBlack) + COUNT(brandPreSmirnoff) + COUNT(brandPreJwRed) + COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear <= @YYYY
	AND both IS NOT NULL
	
	SELECT
	@MobileBrandYearNew = COUNT(brandPreJwGold) + COUNT(brandPreJwBlack) + COUNT(brandPreSmirnoff) + COUNT(brandPreJwRed) + COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear <= @YYYY
	AND mobileOnly IS NOT NULL

	SELECT
	@EmailBrandYearNew = COUNT(brandPreJwGold) + COUNT(brandPreJwBlack) + COUNT(brandPreSmirnoff) + COUNT(brandPreJwRed) + COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear <= @YYYY
	AND emailOnly IS NOT NULL
	
	INSERT INTO @ResultTable
	VALUES ('Email Only',@EmailBrandYearNew)

	INSERT INTO @ResultTable
	VALUES ('Mobile Only',@MobileBrandYearNew)

	INSERT INTO @ResultTable
	VALUES ('Both',@BothBrandYearNew)

	SELECT * FROM @ResultTable

END
