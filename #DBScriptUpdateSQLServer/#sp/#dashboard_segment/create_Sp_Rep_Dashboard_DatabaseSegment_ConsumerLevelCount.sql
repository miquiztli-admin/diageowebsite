USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_DatabaseSegment_ConsumerLevelCount]    Script Date: 10/02/2013 11:20:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_DatabaseSegment_ConsumerLevelCount', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_DatabaseSegment_ConsumerLevelCount;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <02/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_DatabaseSegment_ConsumerLevelCount](@YYYY as integer)
AS
BEGIN

	declare @GoldAdorer bigint = 0,@GoldAdopter bigint = 0,@GoldAcceptor bigint = 0,@GoldAvailable bigint = 0,@GoldRejector bigint = 0,@GoldUnknown bigint = 0
	declare @BlackAdorer bigint = 0,@BlackAdopter bigint = 0,@BlackAcceptor bigint = 0,@BlackAvailable bigint = 0,@BlackRejector bigint = 0,@BlackUnknown bigint = 0
	declare @SMAdorer bigint = 0,@SMAdopter bigint = 0,@SMAcceptor bigint = 0,@SMAvailable bigint = 0,@SMRejector bigint = 0,@SMUnknown bigint = 0
	declare @RedAdorer bigint = 0,@RedAdopter bigint = 0,@RedAcceptor bigint = 0,@RedAvailable bigint = 0,@RedRejector bigint = 0,@RedUnknown bigint = 0
	declare @BMAdorer bigint = 0,@BMAdopter bigint = 0,@BMAcceptor bigint = 0,@BMAvailable bigint = 0,@BMRejector bigint = 0,@BMUnknown bigint = 0

	DECLARE @cCursor CURSOR
	DECLARE @tmpConsumerBrand int, @tmpConsumer4A VARCHAR(1), @tmpValue bigint

	DECLARE @ResultTable TABLE (
		 GoldAdorer bigint,GoldAdopter bigint,GoldAcceptor bigint,GoldAvailable bigint,GoldRejector bigint,GoldUnknown bigint,
		 BlackAdorer bigint,BlackAdopter bigint,BlackAcceptor bigint,BlackAvailable bigint,BlackRejector bigint,BlackUnknown bigint,
		 SMAdorer bigint,SMAdopter bigint,SMAcceptor bigint,SMAvailable bigint,SMRejector bigint,SMUnknown bigint,
		 RedAdorer bigint,RedAdopter bigint,RedAcceptor bigint,RedAvailable bigint,RedRejector bigint,RedUnknown bigint,
		 BMAdorer bigint,BMAdopter bigint,BMAcceptor bigint,BMAvailable bigint,BMRejector bigint,BMUnknown bigint
	)


	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT comsumerLevelBrand, comsumerLevel4A, ISNULL(COUNT(DISTINCT(consumerId)), 0)
		FROM vw_rpt_dashb_segment_by_quarter
		WHERE upd_QuarterYear <= @YYYY
		AND comsumerLevelBrand IS NOT NULL
		GROUP BY comsumerLevelBrand, comsumerLevel4A
		
	OPEN @cCursor
	FETCH NEXT FROM @cCursor INTO @tmpConsumerBrand, @tmpConsumer4A, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpConsumerBrand = 1
		BEGIN
			IF @tmpConsumer4A = '1'
				SET @GoldAdorer = @tmpValue
			ELSE IF @tmpConsumer4A = '2'
				SET @GoldAdopter = @tmpValue
			ELSE IF @tmpConsumer4A = '3'
				SET @GoldAvailable = @tmpValue
			ELSE IF @tmpConsumer4A = '4'
				SET @GoldAcceptor = @tmpValue
			ELSE IF @tmpConsumer4A = '5'
				SET @GoldRejector = @tmpValue
			ELSE -- Case have more than 6 type
				SET @GoldUnknown = ISNULL(@GoldUnknown, 0) + @tmpValue
		END
		ELSE IF @tmpConsumerBrand = 2
		BEGIN
			IF @tmpConsumer4A = '1'
				SET @BlackAdorer = @tmpValue
			ELSE IF @tmpConsumer4A = '2'
				SET @BlackAdopter = @tmpValue
			ELSE IF @tmpConsumer4A = '3'
				SET @BlackAvailable = @tmpValue
			ELSE IF @tmpConsumer4A = '4'
				SET @BlackAcceptor = @tmpValue
			ELSE IF @tmpConsumer4A = '5'
				SET @BlackRejector = @tmpValue
			ELSE -- Case have more than 6 type
				SET @BlackUnknown = ISNULL(@BlackUnknown, 0) + @tmpValue
		END
		ELSE IF @tmpConsumerBrand = 3
		BEGIN
			IF @tmpConsumer4A = '1'
				SET @SMAdorer = @tmpValue
			ELSE IF @tmpConsumer4A = '2'
				SET @SMAdopter = @tmpValue
			ELSE IF @tmpConsumer4A = '3'
				SET @SMAvailable = @tmpValue
			ELSE IF @tmpConsumer4A = '4'
				SET @SMAcceptor = @tmpValue
			ELSE IF @tmpConsumer4A = '5'
				SET @SMRejector = @tmpValue
			ELSE -- Case have more than 6 type
				SET @SMUnknown = ISNULL(@SMUnknown, 0) + @tmpValue
		END
		ELSE IF @tmpConsumerBrand = 4
		BEGIN
			IF @tmpConsumer4A = '1'
				SET @RedAdorer = @tmpValue
			ELSE IF @tmpConsumer4A = '2'
				SET @RedAdopter = @tmpValue
			ELSE IF @tmpConsumer4A = '3'
				SET @RedAvailable = @tmpValue
			ELSE IF @tmpConsumer4A = '4'
				SET @RedAcceptor = @tmpValue
			ELSE IF @tmpConsumer4A = '5'
				SET @RedRejector = @tmpValue
			ELSE -- Case have more than 6 type
				SET @RedUnknown = ISNULL(@RedUnknown, 0) + @tmpValue
		END
		ELSE IF @tmpConsumerBrand = 5
		BEGIN
			IF @tmpConsumer4A = '1'
				SET @BMAdorer = @tmpValue
			ELSE IF @tmpConsumer4A = '2'
				SET @BMAdopter = @tmpValue
			ELSE IF @tmpConsumer4A = '3'
				SET @BMAvailable = @tmpValue
			ELSE IF @tmpConsumer4A = '4'
				SET @BMAcceptor = @tmpValue
			ELSE IF @tmpConsumer4A = '5'
				SET @BMRejector = @tmpValue
			ELSE -- Case have more than 6 type
				SET @BMUnknown = ISNULL(@BMUnknown, 0) + @tmpValue
		END

		FETCH NEXT FROM @cCursor INTO @tmpConsumerBrand, @tmpConsumer4A, @tmpValue
	END
	CLOSE @cCursor

	INSERT INTO @ResultTable
	VALUES (
		@GoldAdorer ,@GoldAdopter ,@GoldAcceptor ,@GoldAvailable ,@GoldRejector ,@GoldUnknown ,
		@BlackAdorer ,@BlackAdopter ,@BlackAcceptor ,@BlackAvailable ,@BlackRejector ,@BlackUnknown ,
		@SMAdorer ,@SMAdopter ,@SMAcceptor ,@SMAvailable ,@SMRejector ,@SMUnknown ,
		@RedAdorer ,@RedAdopter ,@RedAcceptor ,@RedAvailable ,@RedRejector ,@RedUnknown ,
		@BMAdorer ,@BMAdopter ,@BMAcceptor ,@BMAvailable ,@BMRejector ,@BMUnknown 
	)

	SELECT * FROM @ResultTable

END
