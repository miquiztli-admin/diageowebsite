USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_DatabaseSegment_ContactableDatabaseStatus_Graph]    Script Date: 10/02/2013 11:50:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_DatabaseSegment_ContactableDatabaseStatus_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_DatabaseSegment_ContactableDatabaseStatus_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <02/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_DatabaseSegment_ContactableDatabaseStatus_Graph](@YYYY as integer)
AS
BEGIN

	Declare @GoldHVC bigint, @GoldNONHVC bigint,@GoldNO4A bigint,@GoldTarget bigint
	Declare @BlackHVC bigint, @BlackNONHVC bigint,@BlackNO4A bigint,@BlackTarget bigint
	Declare @SMHVC bigint, @SMNONHVC bigint,@SMNO4A bigint,@SMTarget bigint
	Declare @RedHVC bigint, @RedNONHVC bigint,@RedNO4A bigint,@RedTarget bigint
	Declare @BMHVC bigint, @BMNONHVC bigint,@BMNO4A bigint,@BMTarget bigint

	DECLARE @ResultTable TABLE (
	 Brand varchar(20),HVC bigint,NONHVC bigint,NO4A bigint,
	 BrandTarget bigint
	)

	SELECT 
	@GoldHVC = ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwGold = 'T' AND (survey4aJwGold = 1 OR survey4aJwGold = 2) THEN consumerId
			ELSE NULL
		END
	)), 0) ,
	@GoldNONHVC = ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwGold = 'T' AND (survey4aJwGold = 3 OR survey4aJwGold = 4 OR survey4aJwGold = 5) THEN consumerId
			ELSE NULL
		END
	)), 0) ,
	@GoldNO4A = ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwGold = 'T' AND ISNULL(survey4aJwGold, '') = '' THEN consumerId
			ELSE NULL
		END
	)), 0) ,
	@BlackHVC = ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwBlack = 'T' AND (survey4aJwBlack = 1 OR survey4aJwBlack = 2) THEN consumerId
			ELSE NULL
		END
	)), 0) ,
	@BlackNONHVC = ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwBlack = 'T' AND (survey4aJwBlack = 3 OR survey4aJwBlack = 4 OR survey4aJwBlack = 5) THEN consumerId
			ELSE NULL
		END
	)), 0) ,
	@BlackNO4A = ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwBlack = 'T' AND ISNULL(survey4aJwBlack, '') = '' THEN consumerId
			ELSE NULL
		END
	)), 0) ,
	@SMHVC = ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreSmirnoff = 'T' AND (survey4aSmirnoff = 1 OR survey4aSmirnoff = 2) THEN consumerId
			ELSE NULL
		END
	)), 0) ,
	@SMNONHVC = ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreSmirnoff = 'T' AND (survey4aSmirnoff = 3 OR survey4aSmirnoff = 4 OR survey4aSmirnoff = 5) THEN consumerId
			ELSE NULL
		END
	)), 0) ,
	@SMNO4A = ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreSmirnoff = 'T' AND ISNULL(survey4aSmirnoff, '') = '' THEN consumerId
			ELSE NULL
		END
	)), 0) ,
	@RedHVC = ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwRed = 'T' AND (survey4aJwRed = 1 OR survey4aJwRed = 2) THEN consumerId
			ELSE NULL
		END
	)), 0) ,
	@RedNONHVC = ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwRed = 'T' AND (survey4aJwRed = 3 OR survey4aJwRed = 4 OR survey4aJwRed = 5) THEN consumerId
			ELSE NULL
		END
	)), 0) ,
	@RedNO4A = ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwRed = 'T' AND ISNULL(survey4aJwRed, '') = '' THEN consumerId
			ELSE NULL
		END
	)), 0) ,
	@BMHVC = ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreBenmore = 'T' AND (survey4aBenmore = '1' OR survey4aBenmore = '2') THEN consumerId
			ELSE NULL
		END
	)), 0) ,
	@BMNONHVC = ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreBenmore = 'T' AND (survey4aBenmore = '3' OR survey4aBenmore = '4' OR survey4aBenmore = '5') THEN consumerId
			ELSE NULL
		END
	)), 0) ,
	@BMNO4A = ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreBenmore = 'T' AND ISNULL(survey4aBenmore, '') = '' THEN consumerId
			ELSE NULL
		END
	)), 0)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_quarterYear <= @YYYY
	AND no_all IS NULL

	--target
	SET @GoldTarget=0
	SET @BlackTarget = 0
	SET @SMTarget=0
	SET @RedTarget=0
	SET @BMTarget=0

	--insert JWGL
	INSERT INTO @ResultTable
	values('JWGL',@GoldHVC,@GoldNONHVC,@GoldNO4A,@GoldTarget)

	--insert JWBL
	INSERT INTO @ResultTable
	values('JWBL',@BlackHVC,@BlackNONHVC,@BlackNO4A,@BlackTarget)

	--insert SM
	INSERT INTO @ResultTable
	values('SM',@SMHVC,@SMNONHVC,@SMNO4A,@SMTarget)

	--insert JWRL
	INSERT INTO @ResultTable
	values('JWRL',@RedHVC,@RedNONHVC,@RedNO4A,@RedTarget)

	--insert BM
	INSERT INTO @ResultTable
	values('BM',@BMHVC,@BMNONHVC,@BMNO4A,@BMTarget)

	SELECT * FROM @ResultTable

END
