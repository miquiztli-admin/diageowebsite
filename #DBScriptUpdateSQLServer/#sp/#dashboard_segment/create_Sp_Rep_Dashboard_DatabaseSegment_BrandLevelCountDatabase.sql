USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_DatabaseSegment_BrandLevelCountDatabase]    Script Date: 10/02/2013 11:16:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_DatabaseSegment_BrandLevelCountDatabase', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_DatabaseSegment_BrandLevelCountDatabase;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <02/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_DatabaseSegment_BrandLevelCountDatabase](@YYYY as integer)
AS
BEGIN

	declare @GoldAdorer bigint = 0,@GoldAdopter bigint = 0,@GoldAcceptor bigint = 0,@GoldAvailable bigint = 0,@GoldRejector bigint = 0,@GoldUnknown bigint = 0
	declare @BlackAdorer bigint = 0,@BlackAdopter bigint = 0,@BlackAcceptor bigint = 0,@BlackAvailable bigint = 0,@BlackRejector bigint = 0,@BlackUnknown bigint = 0
	declare @SMAdorer bigint = 0,@SMAdopter bigint = 0,@SMAcceptor bigint = 0,@SMAvailable bigint = 0,@SMRejector bigint = 0,@SMUnknown bigint = 0
	declare @RedAdorer bigint = 0,@RedAdopter bigint = 0,@RedAcceptor bigint = 0,@RedAvailable bigint = 0,@RedRejector bigint = 0,@RedUnknown bigint = 0
	declare @BMAdorer bigint = 0,@BMAdopter bigint = 0,@BMAcceptor bigint = 0,@BMAvailable bigint = 0,@BMRejector bigint = 0,@BMUnknown bigint = 0

	DECLARE @cCursor CURSOR
	DECLARE @tmpFilter VARCHAR(1), @tmpValue bigint

	DECLARE @ResultTable TABLE (
		 GoldAdorer bigint,GoldAdopter bigint,GoldAcceptor bigint,GoldAvailable bigint,GoldRejector bigint,GoldUnknown bigint,
		 BlackAdorer bigint,BlackAdopter bigint,BlackAcceptor bigint,BlackAvailable bigint,BlackRejector bigint,BlackUnknown bigint,
		 SMAdorer bigint,SMAdopter bigint,SMAcceptor bigint,SMAvailable bigint,SMRejector bigint,SMUnknown bigint,
		 RedAdorer bigint,RedAdopter bigint,RedAcceptor bigint,RedAvailable bigint,RedRejector bigint,RedUnknown bigint,
		 BMAdorer bigint,BMAdopter bigint,BMAcceptor bigint,BMAvailable bigint,BMRejector bigint,BMUnknown bigint
	)

--Gold
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT ISNULL(survey4aJwGold, ''), COUNT(DISTINCT(consumerId))
		FROM vw_rpt_dashb_segment_by_quarter
		WHERE upd_quarterYear <= @YYYY
		AND brandPreJwGold = 'T'
		GROUP BY ISNULL(survey4aJwGold, '')
		ORDER BY ISNULL(survey4aJwGold, '') ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = '1'
			SET @GoldAdorer = @tmpValue
		ELSE IF @tmpFilter = '2'
			SET @GoldAdopter = @tmpValue
		ELSE IF @tmpFilter = '3'
			SET @GoldAvailable = @tmpValue
		ELSE IF @tmpFilter = '4'
			SET @GoldAcceptor = @tmpValue
		ELSE IF @tmpFilter = '5'
			SET @GoldRejector = @tmpValue
		ELSE -- Case have more than 6 type
			SET @GoldUnknown = ISNULL(@BlackUnknown, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	CLOSE @cCursor
	
--Black
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT ISNULL(survey4aJwBlack, ''), COUNT(DISTINCT(consumerId))
		FROM vw_rpt_dashb_segment_by_quarter
		WHERE upd_quarterYear <= @YYYY
		AND brandPreJwBlack = 'T'
		GROUP BY ISNULL(survey4aJwBlack, '')
		ORDER BY ISNULL(survey4aJwBlack, '') ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = '1'
			SET @BlackAdorer = @tmpValue
		ELSE IF @tmpFilter = '2'
			SET @BlackAdopter = @tmpValue
		ELSE IF @tmpFilter = '3'
			SET @BlackAvailable = @tmpValue
		ELSE IF @tmpFilter = '4'
			SET @BlackAcceptor = @tmpValue
		ELSE IF @tmpFilter = '5'
			SET @BlackRejector = @tmpValue
		ELSE -- Case have more than 6 type
			SET @BlackUnknown = ISNULL(@BlackUnknown, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	
	CLOSE @cCursor

--SM
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT ISNULL(survey4aSmirnoff, ''), COUNT(DISTINCT(consumerId))
		FROM vw_rpt_dashb_segment_by_quarter
		WHERE upd_quarterYear <= @YYYY
		AND brandPreSmirnoff = 'T'
		GROUP BY ISNULL(survey4aSmirnoff, '')
		ORDER BY ISNULL(survey4aSmirnoff, '') ASC

	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = '1'
			SET @SMAdorer = @tmpValue
		ELSE IF @tmpFilter = '2'
			SET @SMAdopter = @tmpValue
		ELSE IF @tmpFilter = '3'
			SET @SMAvailable = @tmpValue
		ELSE IF @tmpFilter = '4'
			SET @SMAcceptor = @tmpValue
		ELSE IF @tmpFilter = '5'
			SET @SMRejector = @tmpValue
		ELSE -- Case have more than 6 type
			SET @SMUnknown = ISNULL(@SMUnknown, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	CLOSE @cCursor

--RED
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT ISNULL(survey4aJwRed, ''), COUNT(DISTINCT(consumerId))
		FROM vw_rpt_dashb_segment_by_quarter
		WHERE upd_quarterYear <= @YYYY
		AND brandPreJwRed = 'T'
		GROUP BY ISNULL(survey4aJwRed, '')
		ORDER BY ISNULL(survey4aJwRed, '') ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = '1'
			SET @RedAdorer = @tmpValue
		ELSE IF @tmpFilter = '2'
			SET @RedAdopter = @tmpValue
		ELSE IF @tmpFilter = '3'
			SET @RedAvailable = @tmpValue
		ELSE IF @tmpFilter = '4'
			SET @RedAcceptor = @tmpValue
		ELSE IF @tmpFilter = '5'
			SET @RedRejector = @tmpValue
		ELSE -- Case have more than 6 type
			SET @RedUnknown = ISNULL(@RedUnknown, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	CLOSE @cCursor

--Benmore
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT ISNULL(survey4aBenmore, ''), COUNT(DISTINCT(consumerId))
		FROM vw_rpt_dashb_segment_by_quarter
		WHERE upd_quarterYear <= @YYYY
		AND brandPreBenmore = 'T'
		GROUP BY ISNULL(survey4aBenmore, '')
		ORDER BY ISNULL(survey4aBenmore, '') ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = '1'
			SET @BMAdorer = @tmpValue
		ELSE IF @tmpFilter = '2'
			SET @BMAdopter = @tmpValue
		ELSE IF @tmpFilter = '3'
			SET @BMAvailable = @tmpValue
		ELSE IF @tmpFilter = '4'
			SET @BMAcceptor = @tmpValue
		ELSE IF @tmpFilter = '5'
			SET @BMRejector = @tmpValue
		ELSE -- Case have more than 6 type
			SET @BMUnknown = ISNULL(@BMUnknown, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	CLOSE @cCursor

	INSERT INTO @ResultTable 
	VALUES (
	 @GoldAdorer ,@GoldAdopter ,@GoldAcceptor ,@GoldAvailable ,@GoldRejector ,@GoldUnknown ,
	 @BlackAdorer ,@BlackAdopter ,@BlackAcceptor ,@BlackAvailable ,@BlackRejector ,@BlackUnknown ,
	 @SMAdorer ,@SMAdopter ,@SMAcceptor ,@SMAvailable ,@SMRejector ,@SMUnknown ,
	 @RedAdorer ,@RedAdopter ,@RedAcceptor ,@RedAvailable ,@RedRejector ,@RedUnknown ,
	 @BMAdorer ,@BMAdopter ,@BMAcceptor ,@BMAvailable ,@BMRejector ,@BMUnknown 
	)

	SELECT * FROM @ResultTable

END
