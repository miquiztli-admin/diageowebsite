USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_DatabaseSegment_ContactableDatabaseStatus]    Script Date: 10/02/2013 11:37:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_DatabaseSegment_ContactableDatabaseStatus', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_DatabaseSegment_ContactableDatabaseStatus;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <02/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_DatabaseSegment_ContactableDatabaseStatus](@YYYY as integer)
AS
BEGIN
	/*Declare @GoldHVC bigint, @GoldNONHVC bigint,@GoldNO4A bigint,@GoldTarget bigint
	Declare @BlackHVC bigint, @BlackNONHVC bigint,@BlackNO4A bigint,@BlackTarget bigint
	Declare @SMHVC bigint, @SMNONHVC bigint,@SMNO4A bigint,@SMTarget bigint
	Declare @RedHVC bigint, @RedNONHVC bigint,@RedNO4A bigint,@RedTarget bigint
	Declare @BMHVC bigint, @BMNONHVC bigint,@BMNO4A bigint,@BMTarget bigint

	DECLARE @ResultTable TABLE (
	 GoldHVC bigint, GoldNONHVC bigint, GoldNO4A bigint, GoldTarget bigint,
	 BlackHVC bigint, BlackNONHVC bigint,BlackNO4A bigint,BlackTarget bigint,
	 SMHVC bigint, SMNONHVC bigint,SMNO4A bigint,SMTarget bigint,
	 RedHVC bigint, RedNONHVC bigint,RedNO4A bigint,RedTarget bigint,
	 BMHVC bigint, BMNONHVC bigint,BMNO4A bigint,BMTarget bigint
	)
	INSERT INTO @ResultTable
	*/
	SELECT 
	ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwGold = 'T' AND (survey4aJwGold = 1 OR survey4aJwGold = 2) THEN consumerId
			ELSE NULL
		END
	)), 0) AS GoldHVC,
	ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwGold = 'T' AND (survey4aJwGold = 3 OR survey4aJwGold = 4 OR survey4aJwGold = 5) THEN consumerId
			ELSE NULL
		END
	)), 0) AS GoldNONHVC,
	ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwGold = 'T' AND ISNULL(survey4aJwGold, '') = '' THEN consumerId
			ELSE NULL
		END
	)), 0) AS GoldNO4A,
	0 AS GoldTarget,
	ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwBlack = 'T' AND (survey4aJwBlack = 1 OR survey4aJwBlack = 2) THEN consumerId
			ELSE NULL
		END
	)), 0) AS BlackHVC,
	ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwBlack = 'T' AND (survey4aJwBlack = 3 OR survey4aJwBlack = 4 OR survey4aJwBlack = 5) THEN consumerId
			ELSE NULL
		END
	)), 0) AS BlackNONHVC,
	ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwBlack = 'T' AND ISNULL(survey4aJwBlack, '') = '' THEN consumerId
			ELSE NULL
		END
	)), 0) AS BlackNO4A,
	0 AS BlackTarget,
	ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreSmirnoff = 'T' AND (survey4aSmirnoff = 1 OR survey4aSmirnoff = 2) THEN consumerId
			ELSE NULL
		END
	)), 0) AS SMHVC,
	ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreSmirnoff = 'T' AND (survey4aSmirnoff = 3 OR survey4aSmirnoff = 4 OR survey4aSmirnoff = 5) THEN consumerId
			ELSE NULL
		END
	)), 0) AS SMNONHVC,
	ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreSmirnoff = 'T' AND ISNULL(survey4aSmirnoff, '') = '' THEN consumerId
			ELSE NULL
		END
	)), 0) AS SMNO4A,
	0 AS SMTarget,
	ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwRed = 'T' AND (survey4aJwRed = 1 OR survey4aJwRed = 2) THEN consumerId
			ELSE NULL
		END
	)), 0) AS RedHVC,
	ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwRed = 'T' AND (survey4aJwRed = 3 OR survey4aJwRed = 4 OR survey4aJwRed = 5) THEN consumerId
			ELSE NULL
		END
	)), 0) AS RedNONHVC,
	ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreJwRed = 'T' AND ISNULL(survey4aJwRed, '') = '' THEN consumerId
			ELSE NULL
		END
	)), 0) AS RedNO4A,
	0 AS RedTarget,
	ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreBenmore = 'T' AND (survey4aBenmore = '1' OR survey4aBenmore = '2') THEN consumerId
			ELSE NULL
		END
	)), 0) AS BMHVC,
	ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreBenmore = 'T' AND (survey4aBenmore = '3' OR survey4aBenmore = '4' OR survey4aBenmore = '5') THEN consumerId
			ELSE NULL
		END
	)), 0) AS BMNONHVC,
	ISNULL(COUNT(DISTINCT(
		CASE
			WHEN brandPreBenmore = 'T' AND ISNULL(survey4aBenmore, '') = '' THEN consumerId
			ELSE NULL
		END
	)), 0) AS BMNO4A,
	0 AS BMTarget
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_quarterYear <= @YYYY
	AND no_all IS NULL

	--SELECT * from @ResultTable

END
