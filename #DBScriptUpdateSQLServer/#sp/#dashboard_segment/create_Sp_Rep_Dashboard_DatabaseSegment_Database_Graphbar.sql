USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_DatabaseSegment_Database_Graphbar]    Script Date: 10/02/2013 13:25:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_DatabaseSegment_Database_Graphbar', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_DatabaseSegment_Database_Graphbar;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <02/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_DatabaseSegment_Database_Graphbar](@YYYY as integer)
AS
BEGIN

	Declare @GoldBrandYearOld bigint, @GoldBrandYearNew bigint
	Declare @BlackBrandYearOld bigint, @BlackBrandYearNew bigint
	Declare @SMBrandYearOld bigint, @SMBrandYearNew bigint
	Declare @RedBrandYearOld bigint, @RedBrandYearNew bigint
	Declare @BMBrandYearOld bigint, @BMBrandYearNew bigint

	DECLARE @ResultTable TABLE (
	 period varchar(20), GoldBrand bigint, BlackBrand bigint, SMBrand bigint, RedBrand bigint, BMBrand bigint
	)

	SELECT
	@GoldBrandYearOld = COUNT(brandPreJwGold),
	@BlackBrandYearOld = COUNT(brandPreJwBlack),
	@SMBrandYearOld = COUNT(brandPreSmirnoff),
	@RedBrandYearOld = COUNT(brandPreJwRed),
	@BMBrandYearOld = COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear < @YYYY
	AND no_all IS NULL
	
	SELECT
	@GoldBrandYearNew = COUNT(brandPreJwGold),
	@BlackBrandYearNew = COUNT(brandPreJwBlack),
	@SMBrandYearNew = COUNT(brandPreSmirnoff),
	@RedBrandYearNew = COUNT(brandPreJwRed),
	@BMBrandYearNew = COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear = @YYYY
	AND no_all IS NULL

	SET @GoldBrandYearNew = ISNULL(@GoldBrandYearOld, 0) + ISNULL(@GoldBrandYearNew, 0)
	SET @BlackBrandYearNew = ISNULL(@BlackBrandYearOld, 0) + ISNULL(@BlackBrandYearNew, 0)
	SET @SMBrandYearNew = ISNULL(@SMBrandYearOld, 0) + ISNULL(@SMBrandYearNew, 0)
	SET @RedBrandYearNew = ISNULL(@RedBrandYearOld, 0) + ISNULL(@RedBrandYearNew, 0)
	SET @BMBrandYearNew = ISNULL(@BMBrandYearOld, 0) + ISNULL(@BMBrandYearNew, 0)

	--insert F_old
	INSERT INTO @ResultTable
	VALUES ('F'+CONVERT(varchar,(@YYYY%100)-1),@GoldBrandYearOld,@BlackBrandYearOld,@SMBrandYearOld,@RedBrandYearOld,@BMBrandYearOld)

	--insert F_new
	INSERT INTO @ResultTable
	VALUES ('F'+CONVERT(varchar,(@YYYY%100)),@GoldBrandYearNew,@BlackBrandYearNew,@SMBrandYearNew,@RedBrandYearNew,@BMBrandYearNew)

	--insert Target
	INSERT INTO @ResultTable
	VALUES ('TARGET',0,0,0,0,0)

	SELECT * FROM @ResultTable

END
