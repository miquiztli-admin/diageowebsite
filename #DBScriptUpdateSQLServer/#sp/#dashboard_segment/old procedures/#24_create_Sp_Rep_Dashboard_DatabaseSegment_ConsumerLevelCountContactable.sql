USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_DatabaseSegment_ConsumerLevelCountContactable_org]    Script Date: 10/02/2013 11:23:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_DatabaseSegment_ConsumerLevelCountContactable', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_DatabaseSegment_ConsumerLevelCountContactable;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <02/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_DatabaseSegment_ConsumerLevelCountContactable](@YYYY as integer)
AS
BEGIN

declare @BlackAdorer bigint,@BlackAdopter bigint,@BlackAcceptor bigint,@BlackAvailable bigint,@BlackRejector bigint,@BlackUnknown bigint
declare @SMAdorer bigint,@SMAdopter bigint,@SMAcceptor bigint,@SMAvailable bigint,@SMRejector bigint,@SMUnknown bigint
declare @RedAdorer bigint,@RedAdopter bigint,@RedAcceptor bigint,@RedAvailable bigint,@RedRejector bigint,@RedUnknown bigint
declare @BAAdorer bigint,@BAAdopter bigint,@BAAcceptor bigint,@BAAvailable bigint,@BARejector bigint,@BAUnknown bigint
declare @BMAdorer bigint,@BMAdopter bigint,@BMAcceptor bigint,@BMAvailable bigint,@BMRejector bigint,@BMUnknown bigint

	DECLARE @cCursor CURSOR
	DECLARE @tmpFilter int, @tmpValue bigint

--Create Temp Table
Begin
Create Table #ResultTable (  
 BlackAdorer bigint,BlackAdopter bigint,BlackAcceptor bigint,BlackAvailable bigint,BlackRejector bigint,BlackUnknown bigint,
 SMAdorer bigint,SMAdopter bigint,SMAcceptor bigint,SMAvailable bigint,SMRejector bigint,SMUnknown bigint,
 RedAdorer bigint,RedAdopter bigint,RedAcceptor bigint,RedAvailable bigint,RedRejector bigint,RedUnknown bigint,
 BAAdorer bigint,BAAdopter bigint,BAAcceptor bigint,BAAvailable bigint,BARejector bigint,BAUnknown bigint,
 BMAdorer bigint,BMAdopter bigint,BMAcceptor bigint,BMAvailable bigint,BMRejector bigint,BMUnknown bigint
)
End

--Black
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT survey4aJwBlack, COUNT(DISTINCT(consumerId))
		FROM vw_rpt_consumer_register_by_quarter
		WHERE quarterYear = @YYYY
		AND contactable IS NOT NULL
		AND survey4aJwBlack IS NOT NULL 
		AND LEN(survey4aJwBlack) > 0
		GROUP BY survey4aJwBlack
		ORDER BY survey4aJwBlack ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = 1
			SET @BlackAdorer = @tmpValue
		ELSE IF @tmpFilter = 2
			SET @BlackAdopter = @tmpValue
		ELSE IF @tmpFilter = 3
			SET @BlackAvailable = @tmpValue
		ELSE IF @tmpFilter = 4
			SET @BlackAcceptor = @tmpValue
		ELSE IF @tmpFilter = 5
			SET @BlackRejector = @tmpValue
		ELSE -- Case have more than 6 type
			SET @BlackUnknown = ISNULL(@BlackUnknown, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	
	CLOSE @cCursor

--SM
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT survey4aSmirnoff, COUNT(DISTINCT(consumerId))
		FROM vw_rpt_consumer_register_by_quarter
		WHERE quarterYear = @YYYY
		AND contactable IS NOT NULL
		AND survey4aSmirnoff IS NOT NULL 
		AND LEN(survey4aSmirnoff) > 0
		GROUP BY survey4aSmirnoff
		ORDER BY survey4aSmirnoff ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = 1
			SET @SMAdorer = @tmpValue
		ELSE IF @tmpFilter = 2
			SET @SMAdopter = @tmpValue
		ELSE IF @tmpFilter = 3
			SET @SMAvailable = @tmpValue
		ELSE IF @tmpFilter = 4
			SET @SMAcceptor = @tmpValue
		ELSE IF @tmpFilter = 5
			SET @SMRejector = @tmpValue
		ELSE -- Case have more than 6 type
			SET @SMUnknown = ISNULL(@SMUnknown, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	
	CLOSE @cCursor

--RED
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT survey4aJwRed, COUNT(DISTINCT(consumerId))
		FROM vw_rpt_consumer_register_by_quarter
		WHERE quarterYear = @YYYY
		AND contactable IS NOT NULL
		AND survey4aJwRed IS NOT NULL 
		AND LEN(survey4aJwRed) > 0
		GROUP BY survey4aJwRed
		ORDER BY survey4aJwRed ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = 1
			SET @RedAdorer = @tmpValue
		ELSE IF @tmpFilter = 2
			SET @RedAdopter = @tmpValue
		ELSE IF @tmpFilter = 3
			SET @RedAvailable = @tmpValue
		ELSE IF @tmpFilter = 4
			SET @RedAcceptor = @tmpValue
		ELSE IF @tmpFilter = 5
			SET @RedRejector = @tmpValue
		ELSE -- Case have more than 6 type
			SET @RedUnknown = ISNULL(@RedUnknown, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	
	CLOSE @cCursor

--BA
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT survey4aBaileys, COUNT(DISTINCT(consumerId))
		FROM vw_rpt_consumer_register_by_quarter
		WHERE quarterYear = @YYYY
		AND contactable IS NOT NULL
		AND survey4aBaileys IS NOT NULL 
		AND LEN(survey4aBaileys) > 0
		GROUP BY survey4aBaileys
		ORDER BY survey4aBaileys ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = 1
			SET @BAAdorer = @tmpValue
		ELSE IF @tmpFilter = 2
			SET @BAAdopter = @tmpValue
		ELSE IF @tmpFilter = 3
			SET @BAAvailable = @tmpValue
		ELSE IF @tmpFilter = 4
			SET @BAAcceptor = @tmpValue
		ELSE IF @tmpFilter = 5
			SET @BARejector = @tmpValue
		ELSE -- Case have more than 6 type
			SET @BAUnknown = ISNULL(@BAUnknown, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	
	CLOSE @cCursor

--Benmore
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT survey4aBenmore, COUNT(DISTINCT(consumerId))
		FROM vw_rpt_consumer_register_by_quarter
		WHERE quarterYear = @YYYY
		AND contactable IS NOT NULL
		AND survey4aBenmore IS NOT NULL 
		AND LEN(survey4aBenmore) > 0
		GROUP BY survey4aBenmore
		ORDER BY survey4aBenmore ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = 1
			SET @BMAdorer = @tmpValue
		ELSE IF @tmpFilter = 2
			SET @BMAdopter = @tmpValue
		ELSE IF @tmpFilter = 3
			SET @BMAvailable = @tmpValue
		ELSE IF @tmpFilter = 4
			SET @BMAcceptor = @tmpValue
		ELSE IF @tmpFilter = 5
			SET @BMRejector = @tmpValue
		ELSE -- Case have more than 6 type
			SET @BMUnknown = ISNULL(@BMUnknown, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	
	CLOSE @cCursor

	insert into #ResultTable
	values(
	 @BlackAdorer ,@BlackAdopter ,@BlackAcceptor ,@BlackAvailable ,@BlackRejector ,@BlackUnknown ,
	 @SMAdorer ,@SMAdopter ,@SMAcceptor ,@SMAvailable ,@SMRejector ,@SMUnknown ,
	 @RedAdorer ,@RedAdopter ,@RedAcceptor ,@RedAvailable ,@RedRejector ,@RedUnknown ,
	 @BAAdorer ,@BAAdopter ,@BAAcceptor ,@BAAvailable ,@BARejector ,@BAUnknown ,
	 @BMAdorer ,@BMAdopter ,@BMAcceptor ,@BMAvailable ,@BMRejector ,@BMUnknown 
	)

	select * from #ResultTable

	Drop Table #ResultTable


END
