USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_DatabaseSegment_Database_Graphbar]    Script Date: 10/02/2013 13:25:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_DatabaseSegment_Database_Graphbar', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_DatabaseSegment_Database_Graphbar;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <02/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_DatabaseSegment_Database_Graphbar](@YYYY as integer)
AS
BEGIN

Declare @BlackBrandYearOld bigint, @BlackBrandYearNew bigint,@BlackConsumerYearOld bigint,@BlackConsumerYearNew bigint
Declare @SMBrandYearOld bigint, @SMBrandYearNew bigint,@SMConsumerYearOld bigint,@SMConsumerYearNew bigint
Declare @RedBrandYearOld bigint, @RedBrandYearNew bigint,@RedConsumerYearOld bigint,@RedConsumerYearNew bigint
Declare @BABrandYearOld bigint, @BABrandYearNew bigint,@BAConsumerYearOld bigint,@BAConsumerYearNew bigint
Declare @BMBrandYearOld bigint, @BMBrandYearNew bigint,@BMConsumerYearOld bigint,@BMConsumerYearNew bigint


--Create Temp Table
Begin
Create Table #ResultTable (  
 period varchar(20),BlackBrand bigint,SMBrand bigint,RedBrand bigint,
 BABrand bigint, BMBrand bigint
							)
End
	SELECT
	@BlackConsumerYearOld = COUNT(DISTINCT(CASE WHEN survey4aJwBlack IS NOT NULL THEN consumerId ELSE NULL END)),
	@SMConsumerYearOld = COUNT(DISTINCT(CASE WHEN survey4aSmirnoff IS NOT NULL THEN consumerId ELSE NULL END)),
	@RedConsumerYearOld = COUNT(DISTINCT(CASE WHEN survey4aJwRed IS NOT NULL THEN consumerId ELSE NULL END)),
	@BAConsumerYearOld = COUNT(DISTINCT(CASE WHEN survey4aBaileys IS NOT NULL THEN consumerId ELSE NULL END)),
	@BMConsumerYearOld = COUNT(DISTINCT(CASE WHEN survey4aBenmore IS NOT NULL THEN consumerId ELSE NULL END))
	FROM vw_rpt_segment_consumer_by_quarter
	WHERE createQuarterYear = @YYYY - 1
	
	SELECT
	@BlackBrandYearOld = COUNT(DISTINCT(CASE WHEN survey4aJwBlack IS NOT NULL THEN consumerId ELSE NULL END)),
	@SMBrandYearOld = COUNT(DISTINCT(CASE WHEN survey4aSmirnoff IS NOT NULL THEN consumerId ELSE NULL END)),
	@RedBrandYearOld = COUNT(DISTINCT(CASE WHEN survey4aJwRed IS NOT NULL THEN consumerId ELSE NULL END)),
	@BABrandYearOld = COUNT(DISTINCT(CASE WHEN survey4aBaileys IS NOT NULL THEN consumerId ELSE NULL END)),
	@BMBrandYearOld = COUNT(DISTINCT(CASE WHEN survey4aBenmore IS NOT NULL THEN consumerId ELSE NULL END))
	FROM vw_rpt_segment_consumer_by_quarter
	WHERE createQuarterYear = @YYYY - 1
	AND brand IS NOT NULL	

	SELECT
	@BlackConsumerYearNew = COUNT(DISTINCT(CASE WHEN survey4aJwBlack IS NOT NULL THEN consumerId ELSE NULL END)),
	@SMConsumerYearNew = COUNT(DISTINCT(CASE WHEN survey4aSmirnoff IS NOT NULL THEN consumerId ELSE NULL END)),
	@RedConsumerYearNew = COUNT(DISTINCT(CASE WHEN survey4aJwRed IS NOT NULL THEN consumerId ELSE NULL END)),
	@BAConsumerYearNew = COUNT(DISTINCT(CASE WHEN survey4aBaileys IS NOT NULL THEN consumerId ELSE NULL END)),
	@BMConsumerYearNew = COUNT(DISTINCT(CASE WHEN survey4aBenmore IS NOT NULL THEN consumerId ELSE NULL END))
	FROM vw_rpt_segment_consumer_by_quarter
	WHERE createQuarterYear = @YYYY
	
	SELECT
	@BlackBrandYearNew = COUNT(DISTINCT(CASE WHEN survey4aJwBlack IS NOT NULL THEN consumerId ELSE NULL END)),
	@SMBrandYearNew = COUNT(DISTINCT(CASE WHEN survey4aSmirnoff IS NOT NULL THEN consumerId ELSE NULL END)),
	@RedBrandYearNew = COUNT(DISTINCT(CASE WHEN survey4aJwRed IS NOT NULL THEN consumerId ELSE NULL END)),
	@BABrandYearNew = COUNT(DISTINCT(CASE WHEN survey4aBaileys IS NOT NULL THEN consumerId ELSE NULL END)),
	@BMBrandYearNew = COUNT(DISTINCT(CASE WHEN survey4aBenmore IS NOT NULL THEN consumerId ELSE NULL END))
	FROM vw_rpt_segment_consumer_by_quarter
	WHERE createQuarterYear = @YYYY
	AND brand IS NOT NULL

	--insert F_old
	insert into #ResultTable
	values('F'+CONVERT(varchar,(@YYYY%100)-1),@BlackBrandYearOld,@SMBrandYearOld,@RedBrandYearOld,@BABrandYearOld,@BMBrandYearOld)

	--insert F_new
	insert into #ResultTable
	values('F'+CONVERT(varchar,(@YYYY%100)),@BlackBrandYearNew,@SMBrandYearNew,@RedBrandYearNew,@BABrandYearNew,@BMBrandYearNew)

	--insert Target
	insert into #ResultTable
	values('TARGET',0,0,0,0,0)

	select * from #ResultTable

	Drop Table #ResultTable

END
