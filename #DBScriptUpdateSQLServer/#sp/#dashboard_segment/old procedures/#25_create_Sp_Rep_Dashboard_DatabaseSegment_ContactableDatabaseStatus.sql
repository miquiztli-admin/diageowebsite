USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_DatabaseSegment_ContactableDatabaseStatus]    Script Date: 10/02/2013 11:37:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_DatabaseSegment_ContactableDatabaseStatus', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_DatabaseSegment_ContactableDatabaseStatus;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <02/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_DatabaseSegment_ContactableDatabaseStatus](@YYYY as integer)
AS
BEGIN

Declare @BlackHVC bigint, @BlackNONHVC bigint,@BlackNO4A bigint,@BlackTarget bigint
Declare @SMHVC bigint, @SMNONHVC bigint,@SMNO4A bigint,@SMTarget bigint
Declare @RedHVC bigint, @RedNONHVC bigint,@RedNO4A bigint,@RedTarget bigint
Declare @BAHVC bigint, @BANONHVC bigint,@BANO4A bigint,@BATarget bigint
Declare @BMHVC bigint, @BMNONHVC bigint,@BMNO4A bigint,@BMTarget bigint

	DECLARE @cCursor CURSOR
	DECLARE @tmpFilter int, @tmpValue bigint

--Create Temp Table
Begin
Create Table #ResultTable (  
 BlackHVC bigint, BlackNONHVC bigint,BlackNO4A bigint,BlackTarget bigint,
 SMHVC bigint, SMNONHVC bigint,SMNO4A bigint,SMTarget bigint,
 RedHVC bigint, RedNONHVC bigint,RedNO4A bigint,RedTarget bigint,
 BAHVC bigint, BANONHVC bigint,BANO4A bigint,BATarget bigint,
 BMHVC bigint, BMNONHVC bigint,BMNO4A bigint,BMTarget bigint,
)
End

--Black
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT survey4aJwBlack, COUNT(DISTINCT(consumerId))
		FROM vw_rpt_consumer_register_by_quarter
		WHERE quarterYear = @YYYY
		AND survey4aJwBlack IS NOT NULL 
		AND LEN(survey4aJwBlack) > 0 --(survey4aJwBlack <> '')
		GROUP BY survey4aJwBlack
		ORDER BY survey4aJwBlack ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = 1 OR @tmpFilter = 2
			SET @BlackHVC = ISNULL(@BlackHVC, 0) + @tmpValue
		ELSE IF @tmpFilter = 3 OR @tmpFilter = 4 OR @tmpFilter = 5
			SET @BlackNONHVC = ISNULL(@BlackNONHVC, 0) + @tmpValue
		ELSE -- Case have more than 6 type
			SET @BlackNO4A = ISNULL(@BlackNO4A, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	
	CLOSE @cCursor

--SM
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT survey4aSmirnoff, COUNT(DISTINCT(consumerId))
		FROM vw_rpt_consumer_register_by_quarter
		WHERE quarterYear = @YYYY
		AND survey4aSmirnoff IS NOT NULL 
		AND LEN(survey4aSmirnoff) > 0 --(survey4aJwBlack <> '')
		GROUP BY survey4aSmirnoff
		ORDER BY survey4aSmirnoff ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = 1 OR @tmpFilter = 2
			SET @SMHVC = ISNULL(@SMHVC, 0) + @tmpValue
		ELSE IF @tmpFilter = 3 OR @tmpFilter = 4 OR @tmpFilter = 5
			SET @SMNONHVC = ISNULL(@SMNONHVC, 0) + @tmpValue
		ELSE -- Case have more than 6 type
			SET @SMNO4A = ISNULL(@SMNO4A, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	
	CLOSE @cCursor

--RED
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT survey4aJwRed, COUNT(DISTINCT(consumerId))
		FROM vw_rpt_consumer_register_by_quarter
		WHERE quarterYear = @YYYY
		AND survey4aJwRed IS NOT NULL 
		AND LEN(survey4aJwRed) > 0 --(survey4aJwBlack <> '')
		GROUP BY survey4aJwRed
		ORDER BY survey4aJwRed ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = 1 OR @tmpFilter = 2
			SET @RedHVC = ISNULL(@RedHVC, 0) + @tmpValue
		ELSE IF @tmpFilter = 3 OR @tmpFilter = 4 OR @tmpFilter = 5
			SET @RedNONHVC = ISNULL(@RedNONHVC, 0) + @tmpValue
		ELSE -- Case have more than 6 type
			SET @RedNO4A = ISNULL(@RedNO4A, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	
	CLOSE @cCursor

--BA
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT survey4aBaileys, COUNT(DISTINCT(consumerId))
		FROM vw_rpt_consumer_register_by_quarter
		WHERE quarterYear = @YYYY
		AND survey4aBaileys IS NOT NULL 
		AND LEN(survey4aBaileys) > 0 --(survey4aJwBlack <> '')
		GROUP BY survey4aBaileys
		ORDER BY survey4aBaileys ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = 1 OR @tmpFilter = 2
			SET @BAHVC = ISNULL(@BAHVC, 0) + @tmpValue
		ELSE IF @tmpFilter = 3 OR @tmpFilter = 4 OR @tmpFilter = 5
			SET @BANONHVC = ISNULL(@BANONHVC, 0) + @tmpValue
		ELSE -- Case have more than 6 type
			SET @BANO4A = ISNULL(@BANO4A, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	
	CLOSE @cCursor

--Benmore
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT survey4aBenmore, COUNT(DISTINCT(consumerId))
		FROM vw_rpt_consumer_register_by_quarter
		WHERE quarterYear = @YYYY
		AND survey4aBenmore IS NOT NULL 
		AND LEN(survey4aBenmore) > 0
		GROUP BY survey4aBenmore
		ORDER BY survey4aBenmore ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = 1 OR @tmpFilter = 2
			SET @BMHVC = ISNULL(@BMHVC, 0) + @tmpValue
		ELSE IF @tmpFilter = 3 OR @tmpFilter = 4 OR @tmpFilter = 5
			SET @BMNONHVC = ISNULL(@BMNONHVC, 0) + @tmpValue
		ELSE -- Case have more than 6 type
			SET @BMNO4A = ISNULL(@BMNO4A, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	
	CLOSE @cCursor

	--target
	begin
		set @BlackTarget = 0
		set @SMTarget=0
		set @RedTarget=0
		set @BATarget=0
		set @BMTarget=0
	end


	insert into #ResultTable
	values(
	 @BlackHVC , @BlackNONHVC ,@BlackNO4A ,@BlackTarget ,
	 @SMHVC , @SMNONHVC ,@SMNO4A ,@SMTarget ,
	 @RedHVC , @RedNONHVC ,@RedNO4A ,@RedTarget ,
	 @BAHVC , @BANONHVC ,@BANO4A ,@BATarget ,
	 @BMHVC , @BMNONHVC ,@BMNO4A ,@BMTarget 
	)

	select * from #ResultTable

	Drop Table #ResultTable

END
