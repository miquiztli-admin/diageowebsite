USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_DatabaseSegment_Database]    Script Date: 10/02/2013 12:05:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_DatabaseSegment_Database', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_DatabaseSegment_Database;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <02/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_DatabaseSegment_Database](@YYYY as integer)
AS
BEGIN

Declare @DBBrandYearOld bigint,@DBBrandYearNew bigint,@DBConsumerYearOld bigint,@DBConsumerYearNew bigint
Declare @ContractBrandYearOld bigint, @ContractBrandYearNew bigint,@ContractConsumerYearOld bigint,@ContractConsumerYearNew bigint
Declare @EmailBrandYearOld bigint, @EmailBrandYearNew bigint,@EmailConsumerYearOld bigint,@EmailConsumerYearNew bigint
Declare @MobileBrandYearOld bigint,@MobileBrandYearNew bigint,@MobileConsumerYearOld bigint,@MobileConsumerYearNew bigint
Declare @BothBrandYearOld bigint, @BothBrandYearNew bigint,@BothConsumerYearOld bigint,@BothConsumerYearNew bigint
Declare @BlackBrandYearOld bigint, @BlackBrandYearNew bigint,@BlackConsumerYearOld bigint,@BlackConsumerYearNew bigint
Declare @SMBrandYearOld bigint, @SMBrandYearNew bigint,@SMConsumerYearOld bigint,@SMConsumerYearNew bigint
Declare @RedBrandYearOld bigint, @RedBrandYearNew bigint,@RedConsumerYearOld bigint,@RedConsumerYearNew bigint
Declare @BABrandYearOld bigint, @BABrandYearNew bigint,@BAConsumerYearOld bigint,@BAConsumerYearNew bigint
Declare @BMBrandYearOld bigint, @BMBrandYearNew bigint,@BMConsumerYearOld bigint,@BMConsumerYearNew bigint


--Create Temp Table
Begin
Create Table #ResultTable (  
 DBBrandYearOld bigint,DBBrandYearNew bigint,DBConsumerYearOld bigint,DBConsumerYearNew bigint,
 ContractBrandYearOld bigint, ContractBrandYearNew bigint,ContractConsumerYearOld bigint,ContractConsumerYearNew bigint,
 EmailBrandYearOld bigint, EmailBrandYearNew bigint,EmailConsumerYearOld bigint,EmailConsumerYearNew bigint,
 MobileBrandYearOld bigint,MobileBrandYearNew bigint,MobileConsumerYearOld bigint,MobileConsumerYearNew bigint,
 BothBrandYearOld bigint, BothBrandYearNew bigint,BothConsumerYearOld bigint,BothConsumerYearNew bigint,
 BlackBrandYearOld bigint, BlackBrandYearNew bigint,BlackConsumerYearOld bigint,BlackConsumerYearNew bigint,
 SMBrandYearOld bigint, SMBrandYearNew bigint,SMConsumerYearOld bigint,SMConsumerYearNew bigint,
 RedBrandYearOld bigint, RedBrandYearNew bigint,RedConsumerYearOld bigint,RedConsumerYearNew bigint,
 BABrandYearOld bigint, BABrandYearNew bigint,BAConsumerYearOld bigint,BAConsumerYearNew bigint,
 BMBrandYearOld bigint, BMBrandYearNew bigint,BMConsumerYearOld bigint,BMConsumerYearNew bigint,

							)
End

	SELECT
	@DBConsumerYearOld = COUNT(DISTINCT(consumerId)),
	@ContractConsumerYearOld =  COUNT(DISTINCT(contactable)),
	@EmailConsumerYearOld = COUNT(DISTINCT(emailOnly)),
	@MobileConsumerYearOld = COUNT(DISTINCT(mobileOnly)),
	@BothConsumerYearOld = COUNT(DISTINCT(mobileEmail)),
	@BlackConsumerYearOld = COUNT(DISTINCT(CASE WHEN survey4aJwBlack IS NOT NULL THEN consumerId ELSE NULL END)),
	@SMConsumerYearOld = COUNT(DISTINCT(CASE WHEN survey4aSmirnoff IS NOT NULL THEN consumerId ELSE NULL END)),
	@RedConsumerYearOld = COUNT(DISTINCT(CASE WHEN survey4aJwRed IS NOT NULL THEN consumerId ELSE NULL END)),
	@BAConsumerYearOld = COUNT(DISTINCT(CASE WHEN survey4aBaileys IS NOT NULL THEN consumerId ELSE NULL END)),
	@BMConsumerYearOld = COUNT(DISTINCT(CASE WHEN survey4aBenmore IS NOT NULL THEN consumerId ELSE NULL END))
	FROM vw_rpt_segment_consumer_by_quarter
	WHERE createQuarterYear = @YYYY - 1
	
	SELECT
	@DBBrandYearOld = COUNT(DISTINCT(brand)),
	@ContractBrandYearOld =  COUNT(DISTINCT(contactable)),
	@EmailBrandYearOld = COUNT(DISTINCT(emailOnly)),
	@MobileBrandYearOld = COUNT(DISTINCT(mobileOnly)),
	@BothBrandYearOld = COUNT(DISTINCT(mobileEmail)),
	@BlackBrandYearOld = COUNT(DISTINCT(CASE WHEN survey4aJwBlack IS NOT NULL THEN consumerId ELSE NULL END)),
	@SMBrandYearOld = COUNT(DISTINCT(CASE WHEN survey4aSmirnoff IS NOT NULL THEN consumerId ELSE NULL END)),
	@RedBrandYearOld = COUNT(DISTINCT(CASE WHEN survey4aJwRed IS NOT NULL THEN consumerId ELSE NULL END)),
	@BABrandYearOld = COUNT(DISTINCT(CASE WHEN survey4aBaileys IS NOT NULL THEN consumerId ELSE NULL END)),
	@BMBrandYearOld = COUNT(DISTINCT(CASE WHEN survey4aBenmore IS NOT NULL THEN consumerId ELSE NULL END))
	FROM vw_rpt_segment_consumer_by_quarter
	WHERE createQuarterYear = @YYYY - 1
	AND brand IS NOT NULL	

	SELECT
	@DBConsumerYearNew = COUNT(DISTINCT(consumerId)),
	@ContractConsumerYearNew =  COUNT(DISTINCT(contactable)),
	@EmailConsumerYearNew = COUNT(DISTINCT(emailOnly)),
	@MobileConsumerYearNew = COUNT(DISTINCT(mobileOnly)),
	@BothConsumerYearNew = COUNT(DISTINCT(mobileEmail)),
	@BlackConsumerYearNew = COUNT(DISTINCT(CASE WHEN survey4aJwBlack IS NOT NULL THEN consumerId ELSE NULL END)),
	@SMConsumerYearNew = COUNT(DISTINCT(CASE WHEN survey4aSmirnoff IS NOT NULL THEN consumerId ELSE NULL END)),
	@RedConsumerYearNew = COUNT(DISTINCT(CASE WHEN survey4aJwRed IS NOT NULL THEN consumerId ELSE NULL END)),
	@BAConsumerYearNew = COUNT(DISTINCT(CASE WHEN survey4aBaileys IS NOT NULL THEN consumerId ELSE NULL END)),
	@BMConsumerYearNew = COUNT(DISTINCT(CASE WHEN survey4aBenmore IS NOT NULL THEN consumerId ELSE NULL END))
	FROM vw_rpt_segment_consumer_by_quarter
	WHERE createQuarterYear = @YYYY
	
	SELECT
	@DBBrandYearNew = COUNT(DISTINCT(brand)),
	@ContractBrandYearNew =  COUNT(DISTINCT(contactable)),
	@EmailBrandYearNew = COUNT(DISTINCT(emailOnly)),
	@MobileBrandYearNew = COUNT(DISTINCT(mobileOnly)),
	@BothBrandYearNew = COUNT(DISTINCT(mobileEmail)),
	@BlackBrandYearNew = COUNT(DISTINCT(CASE WHEN survey4aJwBlack IS NOT NULL THEN consumerId ELSE NULL END)),
	@SMBrandYearNew = COUNT(DISTINCT(CASE WHEN survey4aSmirnoff IS NOT NULL THEN consumerId ELSE NULL END)),
	@RedBrandYearNew = COUNT(DISTINCT(CASE WHEN survey4aJwRed IS NOT NULL THEN consumerId ELSE NULL END)),
	@BABrandYearNew = COUNT(DISTINCT(CASE WHEN survey4aBaileys IS NOT NULL THEN consumerId ELSE NULL END)),
	@BMBrandYearNew = COUNT(DISTINCT(CASE WHEN survey4aBenmore IS NOT NULL THEN consumerId ELSE NULL END))
	FROM vw_rpt_segment_consumer_by_quarter
	WHERE createQuarterYear = @YYYY
	AND brand IS NOT NULL

	insert into #ResultTable
	values(
	 @DBBrandYearOld ,@DBBrandYearNew ,@DBConsumerYearOld ,@DBConsumerYearNew ,
	 @ContractBrandYearOld , @ContractBrandYearNew ,@ContractConsumerYearOld ,@ContractConsumerYearNew ,
	 @EmailBrandYearOld , @EmailBrandYearNew ,@EmailConsumerYearOld ,@EmailConsumerYearNew ,
	 @MobileBrandYearOld ,@MobileBrandYearNew ,@MobileConsumerYearOld ,@MobileConsumerYearNew ,
	 @BothBrandYearOld , @BothBrandYearNew ,@BothConsumerYearOld ,@BothConsumerYearNew ,
	 @BlackBrandYearOld , @BlackBrandYearNew ,@BlackConsumerYearOld ,@BlackConsumerYearNew ,
	 @SMBrandYearOld , @SMBrandYearNew ,@SMConsumerYearOld ,@SMConsumerYearNew ,
	 @RedBrandYearOld , @RedBrandYearNew ,@RedConsumerYearOld ,@RedConsumerYearNew ,
	 @BABrandYearOld , @BABrandYearNew ,@BAConsumerYearOld ,@BAConsumerYearNew ,
	 @BMBrandYearOld , @BMBrandYearNew ,@BMConsumerYearOld ,@BMConsumerYearNew 
	)

	select * from #ResultTable

	Drop Table #ResultTable

END
