USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_DatabaseSegment_Database_Graphpie]    Script Date: 10/02/2013 14:01:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_DatabaseSegment_Database_Graphpie', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_DatabaseSegment_Database_Graphpie;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <02/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_DatabaseSegment_Database_Graphpie](@YYYY as integer)
AS
BEGIN

Declare @EmailBrandYearNew bigint, @MobileBrandYearNew bigint, @BothBrandYearNew bigint

--Create Temp Table
Begin
Create Table #ResultTable (  
 name varchar(20),value bigint
							)
End
	SELECT
	@EmailBrandYearNew = COUNT(DISTINCT(emailOnly)),
	@MobileBrandYearNew = COUNT(DISTINCT(mobileOnly)),
	@BothBrandYearNew = COUNT(DISTINCT(mobileEmail))
	FROM vw_rpt_segment_consumer_by_quarter
	WHERE createQuarterYear = @YYYY
	AND brand IS NOT NULL
	
	insert into #ResultTable
	values('Email Only',@EmailBrandYearNew)

	--insert F_new
	insert into #ResultTable
	values('Mobile Only',@MobileBrandYearNew)

	--insert Target
	insert into #ResultTable
	values('Both',@BothBrandYearNew)

	select * from #ResultTable

	Drop Table #ResultTable

END
