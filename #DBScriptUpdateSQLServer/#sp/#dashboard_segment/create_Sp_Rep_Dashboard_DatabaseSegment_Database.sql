USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_DatabaseSegment_Database]    Script Date: 10/02/2013 12:05:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_DatabaseSegment_Database', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_DatabaseSegment_Database;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <02/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_DatabaseSegment_Database](@YYYY as integer)
AS
BEGIN

	Declare @DBBrandYearOld bigint = 0,@DBBrandYearNew bigint = 0,@DBConsumerYearOld bigint = 0,@DBConsumerYearNew bigint = 0
	Declare @ContractBrandYearOld bigint = 0, @ContractBrandYearNew bigint = 0,@ContractConsumerYearOld bigint = 0,@ContractConsumerYearNew bigint = 0
	Declare @EmailBrandYearOld bigint = 0, @EmailBrandYearNew bigint = 0,@EmailConsumerYearOld bigint = 0,@EmailConsumerYearNew bigint = 0
	Declare @MobileBrandYearOld bigint = 0,@MobileBrandYearNew bigint = 0,@MobileConsumerYearOld bigint = 0,@MobileConsumerYearNew bigint = 0
	Declare @BothBrandYearOld bigint = 0, @BothBrandYearNew bigint = 0,@BothConsumerYearOld bigint = 0,@BothConsumerYearNew bigint = 0
	Declare @GoldBrandYearOld bigint = 0, @GoldBrandYearNew bigint = 0,@GoldConsumerYearOld bigint = 0,@GoldConsumerYearNew bigint = 0
	Declare @BlackBrandYearOld bigint = 0, @BlackBrandYearNew bigint = 0,@BlackConsumerYearOld bigint = 0,@BlackConsumerYearNew bigint = 0
	Declare @SMBrandYearOld bigint = 0, @SMBrandYearNew bigint = 0,@SMConsumerYearOld bigint = 0,@SMConsumerYearNew bigint = 0
	Declare @RedBrandYearOld bigint = 0, @RedBrandYearNew bigint = 0,@RedConsumerYearOld bigint = 0,@RedConsumerYearNew bigint = 0
	Declare @BMBrandYearOld bigint = 0, @BMBrandYearNew bigint = 0,@BMConsumerYearOld bigint = 0,@BMConsumerYearNew bigint = 0

	DECLARE @ResultTable TABLE (
		DBBrandYearOld bigint,DBBrandYearNew bigint,DBConsumerYearOld bigint,DBConsumerYearNew bigint,
		ContractBrandYearOld bigint, ContractBrandYearNew bigint,ContractConsumerYearOld bigint,ContractConsumerYearNew bigint,
		EmailBrandYearOld bigint, EmailBrandYearNew bigint,EmailConsumerYearOld bigint,EmailConsumerYearNew bigint,
		MobileBrandYearOld bigint,MobileBrandYearNew bigint,MobileConsumerYearOld bigint,MobileConsumerYearNew bigint,
		BothBrandYearOld bigint, BothBrandYearNew bigint,BothConsumerYearOld bigint,BothConsumerYearNew bigint,
		GoldBrandYearOld bigint, GoldBrandYearNew bigint,GoldConsumerYearOld bigint,GoldConsumerYearNew bigint,
		BlackBrandYearOld bigint, BlackBrandYearNew bigint,BlackConsumerYearOld bigint,BlackConsumerYearNew bigint,
		SMBrandYearOld bigint, SMBrandYearNew bigint,SMConsumerYearOld bigint,SMConsumerYearNew bigint,
		RedBrandYearOld bigint, RedBrandYearNew bigint,RedConsumerYearOld bigint,RedConsumerYearNew bigint,
		BMBrandYearOld bigint, BMBrandYearNew bigint,BMConsumerYearOld bigint,BMConsumerYearNew bigint
	)

	SELECT
	@DBBrandYearOld = COUNT(brandPreJwGold) + COUNT(brandPreJwBlack) + COUNT(brandPreSmirnoff) + COUNT(brandPreJwRed) + COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear < @YYYY

	SELECT
	@GoldBrandYearOld = COUNT(brandPreJwGold),
	@BlackBrandYearOld = COUNT(brandPreJwBlack),
	@SMBrandYearOld = COUNT(brandPreSmirnoff),
	@RedBrandYearOld = COUNT(brandPreJwRed),
	@BMBrandYearOld = COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear < @YYYY
	AND no_all IS NULL
	
	SELECT
	@BothBrandYearOld = COUNT(brandPreJwGold) + COUNT(brandPreJwBlack) + COUNT(brandPreSmirnoff) + COUNT(brandPreJwRed) + COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear < @YYYY
	AND both IS NOT NULL
	
	SELECT
	@MobileBrandYearOld = COUNT(brandPreJwGold) + COUNT(brandPreJwBlack) + COUNT(brandPreSmirnoff) + COUNT(brandPreJwRed) + COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear < @YYYY
	AND mobileOnly IS NOT NULL

	SELECT
	@EmailBrandYearOld = COUNT(brandPreJwGold) + COUNT(brandPreJwBlack) + COUNT(brandPreSmirnoff) + COUNT(brandPreJwRed) + COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear < @YYYY
	AND emailOnly IS NOT NULL

	SELECT
	@ContractBrandYearOld = COUNT(brandPreJwGold) + COUNT(brandPreJwBlack) + COUNT(brandPreSmirnoff) + COUNT(brandPreJwRed) + COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear < @YYYY
	AND no_all IS NULL

	SELECT
	@DBConsumerYearOld = COUNT(DISTINCT(consumerId))
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear < @YYYY
	AND comsumerLevelBrand IS NOT NULL

	SELECT
	@ContractConsumerYearOld =  COUNT(DISTINCT(CASE WHEN no_all IS NULL THEN consumerId ELSE NULL END)),
	@EmailConsumerYearOld = COUNT(DISTINCT(emailOnly)),
	@MobileConsumerYearOld = COUNT(DISTINCT(mobileOnly)),
	@BothConsumerYearOld = COUNT(DISTINCT(both)),
	@GoldConsumerYearOld = COUNT(DISTINCT(CASE WHEN comsumerLevelBrand = 1 THEN consumerId ELSE NULL END)),
	@BlackConsumerYearOld = COUNT(DISTINCT(CASE WHEN comsumerLevelBrand = 2 THEN consumerId ELSE NULL END)),
	@SMConsumerYearOld = COUNT(DISTINCT(CASE WHEN comsumerLevelBrand = 3 THEN consumerId ELSE NULL END)),
	@RedConsumerYearOld = COUNT(DISTINCT(CASE WHEN comsumerLevelBrand = 4 THEN consumerId ELSE NULL END)),
	@BMConsumerYearOld = COUNT(DISTINCT(CASE WHEN comsumerLevelBrand = 5 THEN consumerId ELSE NULL END))
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear < @YYYY
	AND comsumerLevelBrand IS NOT NULL
	AND no_all IS NULL
	
	SELECT
	@DBBrandYearNew = COUNT(brandPreJwGold) + COUNT(brandPreJwBlack) + COUNT(brandPreSmirnoff) + COUNT(brandPreJwRed) + COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear = @YYYY

	SELECT
	@GoldBrandYearNew = COUNT(brandPreJwGold),
	@BlackBrandYearNew = COUNT(brandPreJwBlack),
	@SMBrandYearNew = COUNT(brandPreSmirnoff),
	@RedBrandYearNew = COUNT(brandPreJwRed),
	@BMBrandYearNew = COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear = @YYYY
	AND no_all IS NULL
	
	SELECT
	@BothBrandYearNew = COUNT(brandPreJwGold) + COUNT(brandPreJwBlack) + COUNT(brandPreSmirnoff) + COUNT(brandPreJwRed) + COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear = @YYYY
	AND both IS NOT NULL
	
	SELECT
	@MobileBrandYearNew = COUNT(brandPreJwGold) + COUNT(brandPreJwBlack) + COUNT(brandPreSmirnoff) + COUNT(brandPreJwRed) + COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear = @YYYY
	AND mobileOnly IS NOT NULL

	SELECT
	@EmailBrandYearNew = COUNT(brandPreJwGold) + COUNT(brandPreJwBlack) + COUNT(brandPreSmirnoff) + COUNT(brandPreJwRed) + COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear = @YYYY
	AND emailOnly IS NOT NULL

	SELECT
	@ContractBrandYearNew = COUNT(brandPreJwGold) + COUNT(brandPreJwBlack) + COUNT(brandPreSmirnoff) + COUNT(brandPreJwRed) + COUNT(brandPreBenmore)
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear = @YYYY
	AND no_all IS NULL

	SELECT
	@DBConsumerYearNew = COUNT(DISTINCT(consumerId))
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear = @YYYY
	AND comsumerLevelBrand IS NOT NULL

	SELECT
	@ContractConsumerYearNew =  COUNT(DISTINCT(CASE WHEN no_all IS NULL THEN consumerId ELSE NULL END)),
	@EmailConsumerYearNew = COUNT(DISTINCT(emailOnly)),
	@MobileConsumerYearNew = COUNT(DISTINCT(mobileOnly)),
	@BothConsumerYearNew = COUNT(DISTINCT(both)),
	@GoldConsumerYearNew = COUNT(DISTINCT(CASE WHEN comsumerLevelBrand = 1 THEN consumerId ELSE NULL END)),
	@BlackConsumerYearNew = COUNT(DISTINCT(CASE WHEN comsumerLevelBrand = 2 THEN consumerId ELSE NULL END)),
	@SMConsumerYearNew = COUNT(DISTINCT(CASE WHEN comsumerLevelBrand = 3 THEN consumerId ELSE NULL END)),
	@RedConsumerYearNew = COUNT(DISTINCT(CASE WHEN comsumerLevelBrand = 4 THEN consumerId ELSE NULL END)),
	@BMConsumerYearNew = COUNT(DISTINCT(CASE WHEN comsumerLevelBrand = 5 THEN consumerId ELSE NULL END))
	FROM vw_rpt_dashb_segment_by_quarter
	WHERE upd_QuarterYear = @YYYY
	AND comsumerLevelBrand IS NOT NULL
	AND no_all IS NULL

	SET @DBBrandYearNew = ISNULL(@DBBrandYearOld, 0) + ISNULL(@DBBrandYearNew, 0)
	SET @GoldBrandYearNew = ISNULL(@GoldBrandYearOld, 0) + ISNULL(@GoldBrandYearNew, 0)
	SET @BlackBrandYearNew = ISNULL(@BlackBrandYearOld, 0) + ISNULL(@BlackBrandYearNew, 0)
	SET @SMBrandYearNew = ISNULL(@SMBrandYearOld, 0) + ISNULL(@SMBrandYearNew, 0)
	SET @RedBrandYearNew = ISNULL(@RedBrandYearOld, 0) + ISNULL(@RedBrandYearNew, 0)
	SET @BMBrandYearNew = ISNULL(@BMBrandYearOld, 0) + ISNULL(@BMBrandYearNew, 0)
	SET @BothBrandYearNew = ISNULL(@BothBrandYearOld, 0) + ISNULL(@BothBrandYearNew, 0)
	SET @MobileBrandYearNew = ISNULL(@MobileBrandYearOld, 0) + ISNULL(@MobileBrandYearNew, 0)
	SET @EmailBrandYearNew = ISNULL(@EmailBrandYearOld, 0) + ISNULL(@EmailBrandYearNew, 0)
	SET @ContractBrandYearNew = ISNULL(@ContractBrandYearOld, 0) + ISNULL(@ContractBrandYearNew, 0)
	SET @DBConsumerYearNew = ISNULL(@DBConsumerYearOld, 0) + ISNULL(@DBConsumerYearNew, 0)
	SET @ContractConsumerYearNew = ISNULL(@ContractConsumerYearOld, 0) + ISNULL(@ContractConsumerYearNew, 0)
	SET @EmailConsumerYearNew = ISNULL(@EmailConsumerYearOld, 0) + ISNULL(@EmailConsumerYearNew, 0)
	SET @MobileConsumerYearNew = ISNULL(@MobileConsumerYearOld, 0) + ISNULL(@MobileConsumerYearNew, 0)
	SET @BothConsumerYearNew = ISNULL(@BothConsumerYearOld, 0) + ISNULL(@BothConsumerYearNew, 0)
	SET @GoldConsumerYearNew = ISNULL(@GoldConsumerYearOld, 0) + ISNULL(@GoldConsumerYearNew, 0)
	SET @BlackConsumerYearNew = ISNULL(@BlackConsumerYearOld, 0) + ISNULL(@BlackConsumerYearNew, 0)
	SET @SMConsumerYearNew = ISNULL(@SMConsumerYearOld, 0) + ISNULL(@SMConsumerYearNew, 0)
	SET @RedConsumerYearNew = ISNULL(@RedConsumerYearOld, 0) + ISNULL(@RedConsumerYearNew, 0)
	SET @BMConsumerYearNew = ISNULL(@BMConsumerYearOld, 0) + ISNULL(@BMConsumerYearNew, 0)
		
	INSERT INTO @ResultTable
	VALUES(
	 @DBBrandYearOld ,@DBBrandYearNew ,@DBConsumerYearOld ,@DBConsumerYearNew ,
	 @ContractBrandYearOld , @ContractBrandYearNew ,@ContractConsumerYearOld ,@ContractConsumerYearNew ,
	 @EmailBrandYearOld , @EmailBrandYearNew ,@EmailConsumerYearOld ,@EmailConsumerYearNew ,
	 @MobileBrandYearOld ,@MobileBrandYearNew ,@MobileConsumerYearOld ,@MobileConsumerYearNew ,
	 @BothBrandYearOld , @BothBrandYearNew ,@BothConsumerYearOld ,@BothConsumerYearNew ,
	 @GoldBrandYearOld , @GoldBrandYearNew ,@GoldConsumerYearOld ,@GoldConsumerYearNew ,
	 @BlackBrandYearOld , @BlackBrandYearNew ,@BlackConsumerYearOld ,@BlackConsumerYearNew ,
	 @SMBrandYearOld , @SMBrandYearNew ,@SMConsumerYearOld ,@SMConsumerYearNew ,
	 @RedBrandYearOld , @RedBrandYearNew ,@RedConsumerYearOld ,@RedConsumerYearNew ,
	 @BMBrandYearOld , @BMBrandYearNew ,@BMConsumerYearOld ,@BMConsumerYearNew 
	)

	SELECT * FROM @ResultTable

END
