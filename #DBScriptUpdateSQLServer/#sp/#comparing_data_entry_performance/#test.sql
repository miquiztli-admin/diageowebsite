declare @Startdate1 varchar(10) = '03-01-2014'
declare @Enddate1 varchar(10) = '03-31-2014'

	SELECT s.Staff_Code, s.Staff_Name, o.Outlet_Name, main.ActualDate AS ActualDate,
	main.Engagement AS Engagement,
	CAST(MonthlyR.AllType AS FLOAT), CAST(MonthlyT.Traffic AS FLOAT), 
	CASE 
		WHEN MonthlyT.Traffic > 0 THEN
		(main.Engage_Traffic - (CAST(MonthlyR.AllType AS FLOAT) / CAST(MonthlyT.Traffic AS FLOAT) * 100)) 
		ELSE 0
	END AS Engage_Traffic,
	CASE 
		WHEN MonthlyR.AllType > 0 THEN
		(main.NewRecruit_Engage - (CAST(MonthlyR.NewType AS FLOAT) / CAST(MonthlyR.AllType AS FLOAT) * 100))
		ELSE 0
	END AS NewRecruit_Engage,
	CASE 
		WHEN MonthlyR.AllType > 0 THEN
		(main.Repeat_Engage	- (CAST(MonthlyR.RepeatType AS FLOAT) / CAST(MonthlyR.AllType AS FLOAT) * 100)) 
		ELSE 0
	END AS Repeat_Engage
	FROM (
		SELECT R.Repeat_Outlet, R.Repeat_CreateBy, COUNT(R.RepeatTransactionDate) AS ActualDate,
		ISNULL(SUM(R.AllType), 0) AS Engagement,
		CASE 
			WHEN SUM(A.AverageTraffic) > 0 THEN
			(CAST(ISNULL(SUM(R.AllType), 0) AS FLOAT) / CAST(SUM(A.AverageTraffic) AS FLOAT) * 100)
			ELSE 0
		END
		AS Engage_Traffic,
		CASE 
			WHEN SUM(R.AllType) > 0 THEN
			(CAST(ISNULL(SUM(R.NewType), 0) AS FLOAT) / CAST(SUM(R.AllType) AS FLOAT) * 100)
			ELSE 0
		END
		AS NewRecruit_Engage,
		CASE 
			WHEN SUM(R.AllType) > 0 THEN
			(CAST(ISNULL(SUM(R.RepeatType), 0) AS FLOAT) / CAST(SUM(R.AllType) AS FLOAT) * 100)
			ELSE 0
		END
		AS Repeat_Engage
		FROM (
			SELECT Repeat_Outlet, Repeat_CreateBy, RepeatTransactionDate,
			SUM(Case when UPPER(Repeat_Type) = 'R' then 1 else 0 end) as RepeatType,
			SUM(Case when UPPER(Repeat_Type) = 'N' then 1 else 0 end) as NewType,
			SUM(Case when Repeat_Type <> '' then 1 else 0 end) as AllType
			FROM 
			(
				SELECT Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, 
				CONVERT(date, Repeat_TransactionDate) as RepeatTransactionDate
				FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
				WHERE Convert(date, [Repeat_TransactionDate]) between convert(date, @Startdate1) and convert(date, @Enddate1)
				GROUP BY Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, Repeat_Consumer, CONVERT(date, Repeat_TransactionDate)
			) R1
			GROUP BY Repeat_Outlet, Repeat_CreateBy, RepeatTransactionDate
		) AS R
		INNER JOIN 	(
			SELECT R.Repeat_Outlet, R.RepeatTransactionDate, 
			CAST(Traffic.Traffic AS FLOAT) / CAST(R.NoPG AS FLOAT) AS AverageTraffic
			FROM (
				SELECT [Traffic_Outlet], SUM([Traffic_Amount]) AS Traffic, 
				CONVERT(DATE, [Traffic_Date]) AS TrafficDate
				FROM [SmartTouchDB].[dbo].[ST_tsTraffic]
				WHERE Convert(date, [Traffic_Date]) between convert(date, @Startdate1) and convert(date, @Enddate1)
				GROUP BY [Traffic_Outlet], CONVERT(DATE, [Traffic_Date])
			) AS Traffic
			INNER JOIN (
				SELECT Repeat_Outlet, RepeatTransactionDate,
				COUNT(DISTINCT(Repeat_CreateBy)) AS NoPG
				FROM 
				(
					SELECT Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, 
					CONVERT(date, Repeat_TransactionDate) as RepeatTransactionDate
					FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
					WHERE Convert(date, [Repeat_TransactionDate]) between convert(date, @Startdate1) and convert(date, @Enddate1)
					GROUP BY Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, Repeat_Consumer, CONVERT(date, Repeat_TransactionDate)
				) R1
				GROUP BY Repeat_Outlet, RepeatTransactionDate
			) R ON R.Repeat_Outlet = Traffic.Traffic_Outlet
				AND R.RepeatTransactionDate = Traffic.TrafficDate
		) AS A ON R.Repeat_Outlet = A.Repeat_Outlet
			AND R.RepeatTransactionDate = A.RepeatTransactionDate
		GROUP BY R.Repeat_Outlet, R.Repeat_CreateBy
	) AS main
	INNER JOIN SmartTouchDB.[dbo].ST_mOutlet o ON main.Repeat_Outlet = o.Outlet_ID
	INNER JOIN SmartTouchDB.[dbo].ST_mStaff s ON main.Repeat_CreateBy = s.Staff_ID
	INNER JOIN (
		SELECT
		SUM(Case when UPPER(Repeat_Type) = 'R' then 1 else 0 end) as RepeatType,
		SUM(Case when UPPER(Repeat_Type) = 'N' then 1 else 0 end) as NewType,
		SUM(Case when Repeat_Type <> '' then 1 else 0 end) as AllType
		FROM 
		(
			SELECT Repeat_Type, [Repeat_Outlet], Repeat_CreateBy
			FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
			WHERE Convert(date, [Repeat_TransactionDate]) between convert(date, @Startdate1) and convert(date, @Enddate1)
			GROUP BY Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, Repeat_Consumer, CONVERT(date, Repeat_TransactionDate)
		) R1
	) AS MonthlyR ON 1 = 1
	INNER JOIN (
		SELECT SUM([Traffic_Amount]) AS Traffic
		FROM [SmartTouchDB].[dbo].[ST_tsTraffic]
		WHERE Convert(date, [Traffic_Date]) between convert(date, @Startdate1) and convert(date, @Enddate1)
	) AS MonthlyT ON 1 = 1
	--WHERE R.Repeat_Outlet = 2 AND R.Repeat_CreateBy = 2
	ORDER BY s.Staff_Code, s.Staff_Name, o.Outlet_Name
