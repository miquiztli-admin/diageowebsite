USE [DiageoDB_Edge]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Comparing_Data_Entry_Performance]    Script Date: 05/19/2014 18:23:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <19/05/2014>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================

--DROP VIEW [vw_rpt_dataEntryPerformance]
--GO

ALTER PROCEDURE [dbo].[Sp_Rep_Comparing_Data_Entry_Performance](
	@Startdate1 varchar(max), @Enddate1 varchar(max), @Topic int, @Outlet varchar(max)--, @PG_Input int
)
AS
BEGIN
/*	SELECT main.Staff_Code, main.Staff_Name, main.Outlet_Name, COUNT(main.T_Date) AS ActualDate,
	ISNULL(SUM(main.Engagement), 0) AS Engagement,
	CASE ISNULL(AVG(main.TrafficAmount), 0) 
		WHEN 0 THEN 0 
		ELSE (ISNULL(AVG(main.Engagement), 0) / AVG(main.TrafficAmount)) - ISNULL(avgEntry.Engage_Traffic, 0)
	END AS Engage_Traffic,
	CASE ISNULL(AVG(main.Engagement), 0)
		WHEN 0 THEN 0 
		ELSE (ISNULL(AVG(main.NewRecruit), 0) / AVG(main.Engagement)) - ISNULL(avgEntry.NewRecruit_Engage, 0)
	END AS NewRecruit_Engage,
	CASE ISNULL(AVG(main.Engagement), 0) 
		WHEN 0 THEN 0 
		ELSE (ISNULL(AVG(main.type_Repeat), 0) / AVG(main.Engagement)) - ISNULL(avgEntry.Repeat_Engage, 0)
	END AS Repeat_Engage
	FROM (
		SELECT Staff_Code, Staff_Name, Outlet_Name, CAST(Repeat_TransactionDate AS DATE) AS T_Date, 
		CAST(COUNT(repeat_id) AS FLOAT) AS Engagement, --CAST(COUNT(engagement) AS FLOAT) AS Engagement
		CAST(COUNT(new_Recruit) AS FLOAT) AS NewRecruit, CAST(COUNT(type_Repeat) AS FLOAT) AS type_Repeat, 
		CAST(SUM(ISNULL(Traffic_Amount, 0)) AS FLOAT) AS TrafficAmount
		FROM vw_rpt_dataEntryPerformance
		WHERE Repeat_TransactionDate BETWEEN @Startdate1 AND @Enddate1
		AND (
			(@Topic = 2 AND DATEPART(DW, Repeat_TransactionDate) IN (1,2,3,4,5)) --Week Day
			OR (@Topic = 3 AND DATEPART(DW, Repeat_TransactionDate) IN (6,7)) -- Week End
			OR @Topic NOT IN (2,3)
		)
		GROUP BY Staff_Code, Staff_Name, Outlet_Name, CAST(Repeat_TransactionDate AS DATE)
	) AS main, (
		SELECT 
		CASE ISNULL(AVG(TrafficAmount), 0) WHEN 0 THEN 0 ELSE AVG(Engagement) / AVG(TrafficAmount) END AS Engage_Traffic,
		CASE ISNULL(AVG(Engagement), 0) WHEN 0 THEN 0 ELSE AVG(NewRecruit) / AVG(Engagement) END AS NewRecruit_Engage,
		CASE ISNULL(AVG(Engagement), 0) WHEN 0 THEN 0 ELSE AVG(type_Repeat) / AVG(Engagement) END AS Repeat_Engage
		-- AVG(Engagement), AVG(NewRecruit), AVG(type_Repeat), AVG(TrafficAmount)
		FROM (
			SELECT Staff_Code, Outlet_Name, CAST(Repeat_TransactionDate AS DATE) AS T_Date, 
			CAST(COUNT(repeat_id) AS FLOAT) AS Engagement, --CAST(COUNT(engagement) AS FLOAT) AS Engagement
			CAST(COUNT(new_Recruit) AS FLOAT) AS NewRecruit, CAST(COUNT(type_Repeat) AS FLOAT) AS type_Repeat, 
			CAST(SUM(ISNULL(Traffic_Amount, 0)) AS FLOAT) AS TrafficAmount
			FROM vw_rpt_dataEntryPerformance
			WHERE Repeat_TransactionDate BETWEEN @Startdate1 AND @Enddate1
			AND (
				(@Topic = 2 AND DATEPART(DW, Repeat_TransactionDate) IN (1,2,3,4,5)) --Week Day
				OR (@Topic = 3 AND DATEPART(DW, Repeat_TransactionDate) IN (6,7)) -- Week End
				OR @Topic NOT IN (2,3)
			)
			GROUP BY Staff_Code, Outlet_Name, CAST(Repeat_TransactionDate AS DATE)
		) AS a
	) AS avgEntry
	GROUP BY main.Staff_Code, main.Staff_Name, main.Outlet_Name, avgEntry.Engage_Traffic, avgEntry.NewRecruit_Engage, 
	avgEntry.Repeat_Engage
*/

	SELECT s.Staff_Code, s.Staff_Name, o.Outlet_Name, main.ActualDate AS ActualDate,
	main.Engagement AS Engagement,
	--CAST(MonthlyR.AllType AS FLOAT), CAST(MonthlyT.Traffic AS FLOAT), 
	CASE 
		WHEN MonthlyT.Traffic > 0 THEN
		(main.Engage_Traffic - (CAST(MonthlyR.AllType AS FLOAT) / CAST(MonthlyT.Traffic AS FLOAT) * 100)) / 100
		ELSE 0
	END AS Engage_Traffic,
	CASE 
		WHEN MonthlyR.AllType > 0 THEN
		(main.NewRecruit_Engage - (CAST(MonthlyR.NewType AS FLOAT) / CAST(MonthlyR.AllType AS FLOAT) * 100)) / 100
		ELSE 0
	END AS NewRecruit_Engage,
	CASE 
		WHEN MonthlyR.AllType > 0 THEN
		(main.Repeat_Engage	- (CAST(MonthlyR.RepeatType AS FLOAT) / CAST(MonthlyR.AllType AS FLOAT) * 100)) / 100
		ELSE 0
	END AS Repeat_Engage
	FROM (
		SELECT R.Repeat_Outlet, R.Repeat_CreateBy, COUNT(R.RepeatTransactionDate) AS ActualDate,
		ISNULL(SUM(R.AllType), 0) AS Engagement,
		CASE 
			WHEN SUM(A.AverageTraffic) > 0 THEN
			(CAST(ISNULL(SUM(R.AllType), 0) AS FLOAT) / CAST(SUM(A.AverageTraffic) AS FLOAT) * 100)
			ELSE 0
		END
		AS Engage_Traffic,
		CASE 
			WHEN SUM(R.AllType) > 0 THEN
			(CAST(ISNULL(SUM(R.NewType), 0) AS FLOAT) / CAST(SUM(R.AllType) AS FLOAT) * 100)
			ELSE 0
		END
		AS NewRecruit_Engage,
		CASE 
			WHEN SUM(R.AllType) > 0 THEN
			(CAST(ISNULL(SUM(R.RepeatType), 0) AS FLOAT) / CAST(SUM(R.AllType) AS FLOAT) * 100)
			ELSE 0
		END
		AS Repeat_Engage
		FROM (
			SELECT Repeat_Outlet, Repeat_CreateBy, RepeatTransactionDate,
			SUM(Case when UPPER(Repeat_Type) = 'R' then 1 else 0 end) as RepeatType,
			SUM(Case when UPPER(Repeat_Type) = 'N' then 1 else 0 end) as NewType,
			SUM(Case when Repeat_Type <> '' then 1 else 0 end) as AllType
			FROM 
			(
				SELECT Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, 
				CONVERT(date, Repeat_TransactionDate) as RepeatTransactionDate
				FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
				WHERE Convert(date, [Repeat_TransactionDate]) between convert(date, @Startdate1) and convert(date, @Enddate1)
				GROUP BY Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, Repeat_Consumer, CONVERT(date, Repeat_TransactionDate)
			) R1
			GROUP BY Repeat_Outlet, Repeat_CreateBy, RepeatTransactionDate
		) AS R
		INNER JOIN 	(
			SELECT R.Repeat_Outlet, R.RepeatTransactionDate, 
			CAST(Traffic.Traffic AS FLOAT) / CAST(R.NoPG AS FLOAT) AS AverageTraffic
			FROM (
				SELECT [Traffic_Outlet], SUM([Traffic_Amount]) AS Traffic, 
				CONVERT(DATE, [Traffic_Date]) AS TrafficDate
				FROM [SmartTouchDB].[dbo].[ST_tsTraffic]
				WHERE Convert(date, [Traffic_Date]) between convert(date, @Startdate1) and convert(date, @Enddate1)
				GROUP BY [Traffic_Outlet], CONVERT(DATE, [Traffic_Date])
			) AS Traffic
			INNER JOIN (
				SELECT Repeat_Outlet, RepeatTransactionDate,
				COUNT(DISTINCT(Repeat_CreateBy)) AS NoPG
				FROM 
				(
					SELECT Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, 
					CONVERT(date, Repeat_TransactionDate) as RepeatTransactionDate
					FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
					WHERE Convert(date, [Repeat_TransactionDate]) between convert(date, @Startdate1) and convert(date, @Enddate1)
					GROUP BY Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, Repeat_Consumer, CONVERT(date, Repeat_TransactionDate)
				) R1
				GROUP BY Repeat_Outlet, RepeatTransactionDate
			) R ON R.Repeat_Outlet = Traffic.Traffic_Outlet
				AND R.RepeatTransactionDate = Traffic.TrafficDate
		) AS A ON R.Repeat_Outlet = A.Repeat_Outlet
			AND R.RepeatTransactionDate = A.RepeatTransactionDate
		WHERE (
			(@Topic = 2 AND DATEPART(DW, R.RepeatTransactionDate) IN (1,2,3,4,5)) --Week Day
			OR (@Topic = 3 AND DATEPART(DW, R.RepeatTransactionDate) IN (6,7)) -- Week End
			OR @Topic NOT IN (2,3)
		)
		GROUP BY R.Repeat_Outlet, R.Repeat_CreateBy
	) AS main
	INNER JOIN SmartTouchDB.[dbo].ST_mOutlet o ON main.Repeat_Outlet = o.Outlet_ID
	INNER JOIN SmartTouchDB.[dbo].ST_mStaff s ON main.Repeat_CreateBy = s.Staff_ID
	INNER JOIN (
		SELECT
		SUM(Case when UPPER(Repeat_Type) = 'R' then 1 else 0 end) as RepeatType,
		SUM(Case when UPPER(Repeat_Type) = 'N' then 1 else 0 end) as NewType,
		SUM(Case when Repeat_Type <> '' then 1 else 0 end) as AllType
		FROM 
		(
			SELECT Repeat_Type, [Repeat_Outlet], Repeat_CreateBy
			FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
			WHERE Convert(date, [Repeat_TransactionDate]) between convert(date, @Startdate1) and convert(date, @Enddate1)
			GROUP BY Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, Repeat_Consumer, CONVERT(date, Repeat_TransactionDate)
		) R1
	) AS MonthlyR ON 1 = 1
	INNER JOIN (
		SELECT SUM([Traffic_Amount]) AS Traffic
		FROM [SmartTouchDB].[dbo].[ST_tsTraffic]
		WHERE Convert(date, [Traffic_Date]) between convert(date, @Startdate1) and convert(date, @Enddate1)
	) AS MonthlyT ON 1 = 1
	ORDER BY s.Staff_Code, s.Staff_Name, o.Outlet_Name

END
