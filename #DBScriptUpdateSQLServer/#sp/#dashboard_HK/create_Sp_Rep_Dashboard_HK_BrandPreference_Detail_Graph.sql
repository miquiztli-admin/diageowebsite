USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_HK_BrandPreference_Detail_Graph]    Script Date: 10/01/2013 21:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'Sp_Rep_Dashboard_HK_BrandPreference_Detail_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_HK_BrandPreference_Detail_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Update date: <27/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_HK_BrandPreference_Detail_Graph]
	@YYYY AS INT
AS
BEGIN

	DECLARE @BLACKYTD BIGINT,@SMYTD BIGINT,@REDYTD BIGINT,@DBYTD BIGINT,@CBYTD BIGINT,@OTYTD BIGINT,@NVYTD BIGINT
			
	---------------------------------------------------------------------------
	DECLARE @BLYTD BIGINT,@BMFCYTD BIGINT,@GOLDYTD BIGINT,@GREENYTD BIGINT,@ASYTD BIGINT,@BLTYTD BIGINT
	DECLARE @BL289YTD BIGINT,@CVYTD BIGINT,@DWYTD BIGINT,@HNYTD BIGINT,@100PIYTD BIGINT,@100P8YYTD BIGINT
	
	DECLARE @ResultTable TABLE (Name varchar(20),Summary bigint) 

	SELECT 
	@BLACKYTD = ISNULL(COUNT(DISTINCT(brandPreJwBlack)), 0),
	@SMYTD = ISNULL(COUNT(DISTINCT(brandPreSmirnoff)), 0), 
	@REDYTD = ISNULL(COUNT(DISTINCT(brandPreJwRed)), 0), 
	@DBYTD = ISNULL(COUNT(DISTINCT(diageoBrandPreOthers)), 0), 
	@CBYTD = ISNULL(COUNT(DISTINCT(brandPreCompetitors)), 0), 
	@OTYTD = ISNULL(COUNT(DISTINCT(brandPreOthers)), 0), 
	@BLYTD = ISNULL(COUNT(DISTINCT(brandPreBaileys)), 0), 
	@BMFCYTD = ISNULL(COUNT(DISTINCT(brandPreBenmore)), 0),
	@GOLDYTD = ISNULL(COUNT(DISTINCT(brandPreJwGold)), 0),
	@GREENYTD = ISNULL(COUNT(DISTINCT(brandPreJwGreen)), 0),
	@ASYTD = ISNULL(COUNT(DISTINCT(brandPreAbsolut)), 0),
	@BLTYTD = ISNULL(COUNT(DISTINCT(brandPreBallentine)), 0),
	@BL289YTD = ISNULL(COUNT(DISTINCT(brandPreBlend)), 0),
	@CVYTD = ISNULL(COUNT(DISTINCT(brandPreChivas)), 0),
	@DWYTD = ISNULL(COUNT(DISTINCT(brandPreDewar)), 0),
	@HNYTD = ISNULL(COUNT(DISTINCT(brandPreHennessy)), 0),
	@100PIYTD = ISNULL(COUNT(DISTINCT(brandPre100Pipers)), 0),
	@100P8YYTD = ISNULL(COUNT(DISTINCT(brandPre100Pipers8Y)), 0),
	@NVYTD = ISNULL(COUNT(DISTINCT(
		CASE WHEN (
			brandPreJwBlack IS NULL
			AND brandPreJwRed IS NULL
			AND brandPreSmirnoff IS NULL
			AND diageoBrandPreOthers IS NULL
			AND brandPreCompetitors IS NULL
			AND brandPreOthers IS NULL
			) THEN consumerId 
			ELSE NULL 
		END)), 0)
	FROM vw_rpt_dashb_hk_by_quarter
	WHERE upd_quarterYear <= @YYYY
	AND no_all IS NULL
		
	--insert 
	INSERT INTO @ResultTable
	VALUES ('JW Black Label',@BLACKYTD),
		('Smirnoff',@SMYTD),
		('JW Red Label',@REDYTD),
		('Baileys',@BLYTD),
		('Benmore Four Casks',@BMFCYTD),
		('JW Gold Label',@GOLDYTD),
		('JW Green Label',@GREENYTD),
		('Absolute',@ASYTD),
		('Ballentine’s',@BLTYTD),
		('Blend 289',@BL289YTD),
		('Chivas',@CVYTD),
		('Dewar’s',@DWYTD),
		('Hennesy',@HNYTD),
		('100 Pipers',@100PIYTD),
		('100 Pipers 8 Years',@100P8YYTD),
		('Other',@OTYTD),
		('No Value',@NVYTD)

	SELECT Name, REPLACE(CONVERT(VARCHAR, CAST(Summary AS MONEY), 1), '.00', '') AS Summary 
	FROM @ResultTable
	
END
