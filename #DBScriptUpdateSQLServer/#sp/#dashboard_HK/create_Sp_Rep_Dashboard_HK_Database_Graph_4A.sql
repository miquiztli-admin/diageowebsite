USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_HK_Database_Graph_4A]    Script Date: 09/29/2013 17:22:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_HK_Database_Graph_4A', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_HK_Database_Graph_4A;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Update date: <26/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_HK_Database_Graph_4A](@YYYY as integer)
AS
BEGIN

	SELECT m.disp_name as Name, 
	CASE m.survey_val
		WHEN 1 THEN Sum4A.Adorer
		WHEN 2 THEN Sum4A.Adopter
		WHEN 3 THEN Sum4A.Available
		WHEN 4 THEN Sum4A.Accepter
		WHEN 5 THEN Sum4A.Rejecter
	END AS Summary
	FROM tbl_m_rpt_survey_type m, (
		SELECT 
		ISNULL(COUNT(DISTINCT(HK_Adorer)), 0) AS Adorer,
		ISNULL(COUNT(DISTINCT(HK_Adopter)), 0) AS Adopter,
		ISNULL(COUNT(DISTINCT(HK_Available)), 0) AS Available,
		ISNULL(COUNT(DISTINCT(HK_Accepter)), 0) AS Accepter,
		ISNULL(COUNT(DISTINCT(HK_Rejecter)), 0) AS Rejecter
		FROM vw_rpt_dashb_hk_by_quarter
		WHERE upd_quarterYear <= @YYYY
		AND isHK = 1
		AND no_all IS NULL
	) Sum4A
	ORDER BY m.survey_val ASC
		
	/*
	SELECT m.disp_name as Name, cq.Summary 
	FROM tbl_m_rpt_survey_type m 
		LEFT OUTER JOIN
			(
				SELECT survey4aJwBlack, 
				ISNULL(COUNT(DISTINCT(consumerId)), 0) AS Summary
				FROM vw_rpt_dashboardConsumer_by_quarter
				WHERE upd_quarterYear <= @YYYY
				AND brandPreJwBlack ='T'
				AND no_all IS NULL
				GROUP BY survey4aJwBlack
			)  cq ON m.survey_val = cq.survey4aJwBlack
	ORDER BY cq.survey4aJwBlack ASC
	*/

END
