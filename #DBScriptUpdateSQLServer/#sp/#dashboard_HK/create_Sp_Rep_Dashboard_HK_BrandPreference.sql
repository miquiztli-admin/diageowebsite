USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_HK_BrandPreference]    Script Date: 10/01/2013 18:38:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_HK_BrandPreference', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_HK_BrandPreference;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Update date: <27/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_HK_BrandPreference](
	@YYYY AS INT
)
AS
BEGIN

	DECLARE @BLACKOLDYEAR BIGINT,@BLACKQ1 BIGINT,@BLACKQ2 BIGINT,@BLACKQ3 BIGINT,@BLACKQ4 BIGINT,@BLACKYTD BIGINT
	DECLARE @SMOLDYEAR BIGINT,@SMQ1 BIGINT,@SMQ2 BIGINT,@SMQ3 BIGINT,@SMQ4 BIGINT,@SMYTD BIGINT
	DECLARE @REDOLDYEAR BIGINT,@REDQ1 BIGINT,@REDQ2 BIGINT,@REDQ3 BIGINT,@REDQ4 BIGINT,@REDYTD BIGINT
	DECLARE @DBOLDYEAR BIGINT,@DBQ1 BIGINT,@DBQ2 BIGINT,@DBQ3 BIGINT,@DBQ4 BIGINT,@DBYTD BIGINT
	DECLARE @CBOLDYEAR BIGINT,@CBQ1 BIGINT,@CBQ2 BIGINT,@CBQ3 BIGINT,@CBQ4 BIGINT,@CBYTD BIGINT
	DECLARE @OTOLDYEAR BIGINT,@OTQ1 BIGINT,@OTQ2 BIGINT,@OTQ3 BIGINT,@OTQ4 BIGINT,@OTYTD BIGINT
	DECLARE @NVOLDYEAR BIGINT,@NVQ1 BIGINT,@NVQ2 BIGINT,@NVQ3 BIGINT,@NVQ4 BIGINT,@NVYTD BIGINT
			
	---------------------------------------------------------------------------
	DECLARE @BLOLDYEAR BIGINT,@BLQ1 BIGINT,@BLQ2 BIGINT,@BLQ3 BIGINT,@BLQ4 BIGINT,@BLYTD BIGINT
	DECLARE @BMFCOLDYEAR BIGINT,@BMFCQ1 BIGINT,@BMFCQ2 BIGINT,@BMFCQ3 BIGINT,@BMFCQ4 BIGINT,@BMFCYTD BIGINT
	DECLARE @GOLDOLDYEAR BIGINT,@GOLDQ1 BIGINT,@GOLDQ2 BIGINT,@GOLDQ3 BIGINT,@GOLDQ4 BIGINT,@GOLDYTD BIGINT
	DECLARE @GREENOLDYEAR BIGINT,@GREENQ1 BIGINT,@GREENQ2 BIGINT,@GREENQ3 BIGINT,@GREENQ4 BIGINT,@GREENYTD BIGINT
	DECLARE @ASOLDYEAR BIGINT,@ASQ1 BIGINT,@ASQ2 BIGINT,@ASQ3 BIGINT,@ASQ4 BIGINT,@ASYTD BIGINT
	DECLARE @BLTOLDYEAR BIGINT,@BLTQ1 BIGINT,@BLTQ2 BIGINT,@BLTQ3 BIGINT,@BLTQ4 BIGINT,@BLTYTD BIGINT
	DECLARE @BL289OLDYEAR BIGINT,@BL289Q1 BIGINT,@BL289Q2 BIGINT,@BL289Q3 BIGINT,@BL289Q4 BIGINT,@BL289YTD BIGINT
	DECLARE @CVOLDYEAR BIGINT,@CVQ1 BIGINT,@CVQ2 BIGINT,@CVQ3 BIGINT,@CVQ4 BIGINT,@CVYTD BIGINT
	DECLARE @DWOLDYEAR BIGINT,@DWQ1 BIGINT,@DWQ2 BIGINT,@DWQ3 BIGINT,@DWQ4 BIGINT,@DWYTD BIGINT
	DECLARE @HNOLDYEAR BIGINT,@HNQ1 BIGINT,@HNQ2 BIGINT,@HNQ3 BIGINT,@HNQ4 BIGINT,@HNYTD BIGINT
	DECLARE @100PIOLDYEAR BIGINT,@100PIQ1 BIGINT,@100PIQ2 BIGINT,@100PIQ3 BIGINT,@100PIQ4 BIGINT,@100PIYTD BIGINT
	DECLARE @100P8YOLDYEAR BIGINT,@100P8YQ1 BIGINT,@100P8YQ2 BIGINT,@100P8YQ3 BIGINT,@100P8YQ4 BIGINT,@100P8YYTD BIGINT
	DECLARE @cCursor CURSOR
	DECLARE @tmpQuarterNum int,@tmpVal1 bigint, @tmpVal2 bigint, @tmpVal3 bigint, @tmpVal4 bigint, @tmpVal5 bigint
	DECLARE @tmpVal6 bigint, @tmpVal7 bigint, @tmpVal8 bigint, @tmpVal9 bigint, @tmpVal10 bigint
	DECLARE @tmpVal11 bigint, @tmpVal12 bigint, @tmpVal13 bigint, @tmpVal14 bigint, @tmpVal15 bigint
	DECLARE @tmpVal16 bigint, @tmpVal17 bigint, @tmpVal18 bigint, @tmpVal19 bigint		
	DECLARE @ResultTable TABLE ( 	
		BLACKOLDYEAR BIGINT,BLACKQ1 BIGINT,BLACKQ2 BIGINT,BLACKQ3 BIGINT,BLACKQ4 BIGINT,BLACKYTD BIGINT,
		SMOLDYEAR BIGINT,SMQ1 BIGINT,SMQ2 BIGINT,SMQ3 BIGINT,SMQ4 BIGINT,SMYTD BIGINT,
		REDOLDYEAR BIGINT,REDQ1 BIGINT,REDQ2 BIGINT,REDQ3 BIGINT,REDQ4 BIGINT,REDYTD BIGINT,
		DBOLDYEAR BIGINT,DBQ1 BIGINT,DBQ2 BIGINT,DBQ3 BIGINT,DBQ4 BIGINT,DBYTD BIGINT,
		CBOLDYEAR BIGINT,CBQ1 BIGINT,CBQ2 BIGINT,CBQ3 BIGINT,CBQ4 BIGINT,CBYTD BIGINT,
		OTOLDYEAR BIGINT,OTQ1 BIGINT,OTQ2 BIGINT,OTQ3 BIGINT,OTQ4 BIGINT,OTYTD BIGINT,
		NVOLDYEAR BIGINT,NVQ1 BIGINT,NVQ2 BIGINT,NVQ3 BIGINT,NVQ4 BIGINT,NVYTD BIGINT,	
		BLOLDYEAR BIGINT,BLQ1 BIGINT,BLQ2 BIGINT,BLQ3 BIGINT,BLQ4 BIGINT,BLYTD BIGINT,
		BMFCOLDYEAR BIGINT,BMFCQ1 BIGINT,BMFCQ2 BIGINT,BMFCQ3 BIGINT,BMFCQ4 BIGINT,BMFCYTD BIGINT,
		GOLDOLDYEAR BIGINT,GOLDQ1 BIGINT,GOLDQ2 BIGINT,GOLDQ3 BIGINT,GOLDQ4 BIGINT,GOLDYTD BIGINT,
		GREENOLDYEAR BIGINT,GREENQ1 BIGINT,GREENQ2 BIGINT,GREENQ3 BIGINT,GREENQ4 BIGINT,GREENYTD BIGINT,
		ASOLDYEAR BIGINT,ASQ1 BIGINT,ASQ2 BIGINT,ASQ3 BIGINT,ASQ4 BIGINT,ASYTD BIGINT,
		BLTOLDYEAR BIGINT,BLTQ1 BIGINT,BLTQ2 BIGINT,BLTQ3 BIGINT,BLTQ4 BIGINT,BLTYTD BIGINT,
		BL289OLDYEAR BIGINT,BL289Q1 BIGINT,BL289Q2 BIGINT,BL289Q3 BIGINT,BL289Q4 BIGINT,BL289YTD BIGINT,
		CVOLDYEAR BIGINT,CVQ1 BIGINT,CVQ2 BIGINT,CVQ3 BIGINT,CVQ4 BIGINT,CVYTD BIGINT,
		DWOLDYEAR BIGINT,DWQ1 BIGINT,DWQ2 BIGINT,DWQ3 BIGINT,DWQ4 BIGINT,DWYTD BIGINT,
		HNOLDYEAR BIGINT,HNQ1 BIGINT,HNQ2 BIGINT,HNQ3 BIGINT,HNQ4 BIGINT,HNYTD BIGINT,
		ONEHUNDREADPIOLDYEAR BIGINT,ONEHUNDREADPIQ1 BIGINT,ONEHUNDREADPIQ2 BIGINT,ONEHUNDREADPIQ3 BIGINT,ONEHUNDREADPIQ4 BIGINT,ONEHUNDREADPIYTD BIGINT,
		ONEHUNDREADP8YOLDYEAR BIGINT,ONEHUNDREAD8YQ1 BIGINT,ONEHUNDREADP8YQ2 BIGINT,ONEHUNDREADP8YQ3 BIGINT,ONEHUNDREADP8YQ4 BIGINT,ONEHUNDREADP8YYTD BIGINT								 							
	)

	SELECT 
	@BLACKOLDYEAR = COUNT(DISTINCT(brandPreJwBlack)),
	@SMOLDYEAR = COUNT(DISTINCT(brandPreSmirnoff)), 
	@REDOLDYEAR = COUNT(DISTINCT(brandPreJwRed)), 
	@DBOLDYEAR = COUNT(DISTINCT(diageoBrandPreOthers)), 
	@CBOLDYEAR = COUNT(DISTINCT(brandPreCompetitors)), 
	@OTOLDYEAR = COUNT(DISTINCT(brandPreOthers)), 
	@BLOLDYEAR = COUNT(DISTINCT(brandPreBaileys)), 
	@BMFCOLDYEAR = COUNT(DISTINCT(brandPreBenmore)),
	@GOLDOLDYEAR = COUNT(DISTINCT(brandPreJwGold)),
	@GREENOLDYEAR = COUNT(DISTINCT(brandPreJwGreen)),
	@ASOLDYEAR = COUNT(DISTINCT(brandPreAbsolut)),
	@BLTOLDYEAR = COUNT(DISTINCT(brandPreBallentine)),
	@BL289OLDYEAR = COUNT(DISTINCT(brandPreBlend)),
	@CVOLDYEAR = COUNT(DISTINCT(brandPreChivas)),
	@DWOLDYEAR = COUNT(DISTINCT(brandPreDewar)),
	@HNOLDYEAR = COUNT(DISTINCT(brandPreHennessy)),
	@100PIOLDYEAR = COUNT(DISTINCT(brandPre100Pipers)),
	@100P8YOLDYEAR = COUNT(DISTINCT(brandPre100Pipers8Y)),
	@NVOLDYEAR = COUNT(DISTINCT(
		CASE WHEN (
				diageoBrandPre IS NULL
				AND diageoBrandPreOthers IS NULL
				AND brandPreCompetitors IS NULL
				AND brandPreOthers IS NULL
			) THEN consumerId 
			ELSE NULL 
		END))
	FROM vw_rpt_dashb_hk_by_quarter
	WHERE upd_quarterYear < @YYYY
	AND no_all IS NULL
	
	SET @cCursor = CURSOR FAST_FORWARD
	FOR	
		SELECT upd_quarterNum, 
		ISNULL(COUNT(DISTINCT(brandPreJwBlack)), 0),
		ISNULL(COUNT(DISTINCT(brandPreSmirnoff)), 0), 
		ISNULL(COUNT(DISTINCT(brandPreJwRed)), 0), 
		ISNULL(COUNT(DISTINCT(diageoBrandPreOthers)), 0), 
		ISNULL(COUNT(DISTINCT(brandPreCompetitors)), 0), 
		ISNULL(COUNT(DISTINCT(brandPreOthers)), 0), 
		ISNULL(COUNT(DISTINCT(brandPreBaileys)), 0), 
		ISNULL(COUNT(DISTINCT(brandPreBenmore)), 0),
		ISNULL(COUNT(DISTINCT(brandPreJwGold)), 0),
		ISNULL(COUNT(DISTINCT(brandPreJwGreen)), 0),
		ISNULL(COUNT(DISTINCT(brandPreAbsolut)), 0),
		ISNULL(COUNT(DISTINCT(brandPreBallentine)), 0),
		ISNULL(COUNT(DISTINCT(brandPreBlend)), 0),
		ISNULL(COUNT(DISTINCT(brandPreChivas)), 0),
		ISNULL(COUNT(DISTINCT(brandPreDewar)), 0),
		ISNULL(COUNT(DISTINCT(brandPreHennessy)), 0),
		ISNULL(COUNT(DISTINCT(brandPre100Pipers)), 0),
		ISNULL(COUNT(DISTINCT(brandPre100Pipers8Y)), 0),
		ISNULL(COUNT(DISTINCT(
			CASE 
				WHEN (
					diageoBrandPre IS NULL
					AND diageoBrandPreOthers IS NULL
					AND brandPreCompetitors IS NULL
					AND brandPreOthers IS NULL
					)THEN consumerID 
				ELSE NULL 
			END)), 0)
		FROM vw_rpt_dashb_hk_by_quarter
		WHERE upd_quarterYear = @YYYY
		AND no_all IS NULL
		GROUP BY upd_quarterNum
		ORDER BY upd_quarterNum

	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpVal1, @tmpVal2, @tmpVal3, @tmpVal4, @tmpVal5, @tmpVal6, @tmpVal7, @tmpVal8, @tmpVal9, 
		@tmpVal10, @tmpVal11, @tmpVal12, @tmpVal13, @tmpVal14, @tmpVal15, @tmpVal16, @tmpVal17, @tmpVal18, @tmpVal19
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
		BEGIN
			SET @BLACKQ1 = @tmpVal1
			SET @SMQ1 = @tmpVal2
			SET @REDQ1 = @tmpVal3
			SET @DBQ1 = @tmpVal4
			SET @CBQ1 = @tmpVal5
			SET @OTQ1 = @tmpVal6
			SET @BLQ1 = @tmpVal7
			SET @BMFCQ1 = @tmpVal8
			SET @GOLDQ1 = @tmpVal9
			SET @GREENQ1 = @tmpVal10
			SET @ASQ1 = @tmpVal11
			SET @BLTQ1 = @tmpVal12
			SET @BL289Q1 = @tmpVal13
			SET @CVQ1 = @tmpVal14
			SET @DWQ1 = @tmpVal15
			SET @HNQ1 = @tmpVal16
			SET @100PIQ1 = @tmpVal17
			SET @100P8YQ1 = @tmpVal18
			SET @NVQ1 = @tmpVal19
		END
		ELSE IF (@tmpQuarterNum = 2)
		BEGIN
			SET @BLACKQ2 = @tmpVal1
			SET @SMQ2 = @tmpVal2
			SET @REDQ2 = @tmpVal3
			SET @DBQ2 = @tmpVal4
			SET @CBQ2 = @tmpVal5
			SET @OTQ2 = @tmpVal6
			SET @BLQ2 = @tmpVal7
			SET @BMFCQ2 = @tmpVal8
			SET @GOLDQ2 = @tmpVal9
			SET @GREENQ2 = @tmpVal10
			SET @ASQ2 = @tmpVal11
			SET @BLTQ2 = @tmpVal12
			SET @BL289Q2 = @tmpVal13
			SET @CVQ2 = @tmpVal14
			SET @DWQ2 = @tmpVal15
			SET @HNQ2 = @tmpVal16
			SET @100PIQ2 = @tmpVal17
			SET @100P8YQ2 = @tmpVal18
			SET @NVQ2 = @tmpVal19
		END
		ELSE IF (@tmpQuarterNum = 3)
		BEGIN
			SET @BLACKQ3 = @tmpVal1
			SET @SMQ3 = @tmpVal2
			SET @REDQ3 = @tmpVal3
			SET @DBQ3 = @tmpVal4
			SET @CBQ3 = @tmpVal5
			SET @OTQ3 = @tmpVal6
			SET @BLQ3 = @tmpVal7
			SET @BMFCQ3 = @tmpVal8
			SET @GOLDQ3 = @tmpVal9
			SET @GREENQ3 = @tmpVal10
			SET @ASQ3 = @tmpVal11
			SET @BLTQ3 = @tmpVal12
			SET @BL289Q3 = @tmpVal13
			SET @CVQ3 = @tmpVal14
			SET @DWQ3 = @tmpVal15
			SET @HNQ3 = @tmpVal16
			SET @100PIQ3 = @tmpVal17
			SET @100P8YQ3 = @tmpVal18
			SET @NVQ3 = @tmpVal19
		END
		ELSE IF (@tmpQuarterNum = 4)
		BEGIN
			SET @BLACKQ4 = @tmpVal1
			SET @SMQ4 = @tmpVal2
			SET @REDQ4 = @tmpVal3
			SET @DBQ4 = @tmpVal4
			SET @CBQ4 = @tmpVal5
			SET @OTQ4 = @tmpVal6
			SET @BLQ4 = @tmpVal7
			SET @BMFCQ4 = @tmpVal8
			SET @GOLDQ4 = @tmpVal9
			SET @GREENQ4 = @tmpVal10
			SET @ASQ4 = @tmpVal11
			SET @BLTQ4 = @tmpVal12
			SET @BL289Q4 = @tmpVal13
			SET @CVQ4 = @tmpVal14
			SET @DWQ4 = @tmpVal15
			SET @HNQ4 = @tmpVal16
			SET @100PIQ4 = @tmpVal17
			SET @100P8YQ4 = @tmpVal18
			SET @NVQ4 = @tmpVal19
		END
	
		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpVal1, @tmpVal2, @tmpVal3, @tmpVal4, @tmpVal5, @tmpVal6, @tmpVal7, @tmpVal8, @tmpVal9, 
			@tmpVal10, @tmpVal11, @tmpVal12, @tmpVal13, @tmpVal14, @tmpVal15, @tmpVal16, @tmpVal17, @tmpVal18, @tmpVal19
	END
	CLOSE @cCursor
	
	SET @BLACKQ1 = ISNULL(@BLACKOLDYEAR, 0) + ISNULL(@BLACKQ1, 0)
	SET @BLACKQ2 = ISNULL(@BLACKQ1, 0) + ISNULL(@BLACKQ2, 0)
	SET @BLACKQ3 = ISNULL(@BLACKQ2, 0) + ISNULL(@BLACKQ3, 0)
	SET @BLACKQ4 = ISNULL(@BLACKQ3, 0) + ISNULL(@BLACKQ4, 0)
	SET @BLACKYTD = ISNULL(@BLACKQ4, 0)
 
	SET @SMQ1 = ISNULL(@SMOLDYEAR, 0) + ISNULL(@SMQ1, 0)
	SET @SMQ2 = ISNULL(@SMQ1, 0) + ISNULL(@SMQ2, 0)
	SET @SMQ3 = ISNULL(@SMQ2, 0) + ISNULL(@SMQ3, 0)
	SET @SMQ4 = ISNULL(@SMQ3, 0) + ISNULL(@SMQ4, 0)
	SET @SMYTD = ISNULL(@SMQ4, 0)
 
	SET @REDQ1 = ISNULL(@REDOLDYEAR, 0) + ISNULL(@REDQ1, 0)
	SET @REDQ2 = ISNULL(@REDQ1, 0) + ISNULL(@REDQ2, 0)
	SET @REDQ3 = ISNULL(@REDQ2, 0) + ISNULL(@REDQ3, 0)
	SET @REDQ4 = ISNULL(@REDQ3, 0) + ISNULL(@REDQ4, 0)
	SET @REDYTD = ISNULL(@REDQ4, 0)
 
	SET @DBQ1 = ISNULL(@DBOLDYEAR, 0) + ISNULL(@DBQ1, 0)
	SET @DBQ2 = ISNULL(@DBQ1, 0) + ISNULL(@DBQ2, 0)
	SET @DBQ3 = ISNULL(@DBQ2, 0) + ISNULL(@DBQ3, 0)
	SET @DBQ4 = ISNULL(@DBQ3, 0) + ISNULL(@DBQ4, 0)
	SET @DBYTD = ISNULL(@DBQ4, 0)
 
	SET @CBQ1 = ISNULL(@CBOLDYEAR, 0) + ISNULL(@CBQ1, 0)
	SET @CBQ2 = ISNULL(@CBQ1, 0) + ISNULL(@CBQ2, 0)
	SET @CBQ3 = ISNULL(@CBQ2, 0) + ISNULL(@CBQ3, 0)
	SET @CBQ4 = ISNULL(@CBQ3, 0) + ISNULL(@CBQ4, 0)
	SET @CBYTD = ISNULL(@CBQ4, 0)
 
	SET @OTQ1 = ISNULL(@OTOLDYEAR, 0) + ISNULL(@OTQ1, 0)
	SET @OTQ2 = ISNULL(@OTQ1, 0) + ISNULL(@OTQ2, 0)
	SET @OTQ3 = ISNULL(@OTQ2, 0) + ISNULL(@OTQ3, 0)
	SET @OTQ4 = ISNULL(@OTQ3, 0) + ISNULL(@OTQ4, 0)
	SET @OTYTD = ISNULL(@OTQ4, 0)
 
	SET @NVQ1 = ISNULL(@NVOLDYEAR, 0) + ISNULL(@NVQ1, 0)
	SET @NVQ2 = ISNULL(@NVQ1, 0) + ISNULL(@NVQ2, 0)
	SET @NVQ3 = ISNULL(@NVQ2, 0) + ISNULL(@NVQ3, 0)
	SET @NVQ4 = ISNULL(@NVQ3, 0) + ISNULL(@NVQ4, 0)
	SET @NVYTD = ISNULL(@NVQ4, 0)
 
	SET @BLQ1 = ISNULL(@BLOLDYEAR, 0) + ISNULL(@BLQ1, 0)
	SET @BLQ2 = ISNULL(@BLQ1, 0) + ISNULL(@BLQ2, 0)
	SET @BLQ3 = ISNULL(@BLQ2, 0) + ISNULL(@BLQ3, 0)
	SET @BLQ4 = ISNULL(@BLQ3, 0) + ISNULL(@BLQ4, 0)
	SET @BLYTD = ISNULL(@BLQ4, 0)
 
	SET @BMFCQ1 = ISNULL(@BMFCOLDYEAR, 0) + ISNULL(@BMFCQ1, 0)
	SET @BMFCQ2 = ISNULL(@BMFCQ1, 0) + ISNULL(@BMFCQ2, 0)
	SET @BMFCQ3 = ISNULL(@BMFCQ2, 0) + ISNULL(@BMFCQ3, 0)
	SET @BMFCQ4 = ISNULL(@BMFCQ3, 0) + ISNULL(@BMFCQ4, 0)
	SET @BMFCYTD = ISNULL(@BMFCQ4, 0)
 
	SET @GOLDQ1 = ISNULL(@GOLDOLDYEAR, 0) + ISNULL(@GOLDQ1, 0)
	SET @GOLDQ2 = ISNULL(@GOLDQ1, 0) + ISNULL(@GOLDQ2, 0)
	SET @GOLDQ3 = ISNULL(@GOLDQ2, 0) + ISNULL(@GOLDQ3, 0)
	SET @GOLDQ4 = ISNULL(@GOLDQ3, 0) + ISNULL(@GOLDQ4, 0)
	SET @GOLDYTD = ISNULL(@GOLDQ4, 0)
 
	SET @GREENQ1 = ISNULL(@GREENOLDYEAR, 0) + ISNULL(@GREENQ1, 0)
	SET @GREENQ2 = ISNULL(@GREENQ1, 0) + ISNULL(@GREENQ2, 0)
	SET @GREENQ3 = ISNULL(@GREENQ2, 0) + ISNULL(@GREENQ3, 0)
	SET @GREENQ4 = ISNULL(@GREENQ3, 0) + ISNULL(@GREENQ4, 0)
	SET @GREENYTD = ISNULL(@GREENQ4, 0)
 
	SET @ASQ1 = ISNULL(@ASOLDYEAR, 0) + ISNULL(@ASQ1, 0)
	SET @ASQ2 = ISNULL(@ASQ1, 0) + ISNULL(@ASQ2, 0)
	SET @ASQ3 = ISNULL(@ASQ2, 0) + ISNULL(@ASQ3, 0)
	SET @ASQ4 = ISNULL(@ASQ3, 0) + ISNULL(@ASQ4, 0)
	SET @ASYTD = ISNULL(@ASQ4, 0)
 
	SET @BLTQ1 = ISNULL(@BLTOLDYEAR, 0) + ISNULL(@BLTQ1, 0)
	SET @BLTQ2 = ISNULL(@BLTQ1, 0) + ISNULL(@BLTQ2, 0)
	SET @BLTQ3 = ISNULL(@BLTQ2, 0) + ISNULL(@BLTQ3, 0)
	SET @BLTQ4 = ISNULL(@BLTQ3, 0) + ISNULL(@BLTQ4, 0)
	SET @BLTYTD = ISNULL(@BLTQ4, 0)
 
	SET @BL289Q1 = ISNULL(@BL289OLDYEAR, 0) + ISNULL(@BL289Q1, 0)
	SET @BL289Q2 = ISNULL(@BL289Q1, 0) + ISNULL(@BL289Q2, 0)
	SET @BL289Q3 = ISNULL(@BL289Q2, 0) + ISNULL(@BL289Q3, 0)
	SET @BL289Q4 = ISNULL(@BL289Q3, 0) + ISNULL(@BL289Q4, 0)
	SET @BL289YTD = ISNULL(@BL289Q4, 0)
 
	SET @CVQ1 = ISNULL(@CVOLDYEAR, 0) + ISNULL(@CVQ1, 0)
	SET @CVQ2 = ISNULL(@CVQ1, 0) + ISNULL(@CVQ2, 0)
	SET @CVQ3 = ISNULL(@CVQ2, 0) + ISNULL(@CVQ3, 0)
	SET @CVQ4 = ISNULL(@CVQ3, 0) + ISNULL(@CVQ4, 0)
	SET @CVYTD = ISNULL(@CVQ4, 0)
 
	SET @DWQ1 = ISNULL(@DWOLDYEAR, 0) + ISNULL(@DWQ1, 0)
	SET @DWQ2 = ISNULL(@DWQ1, 0) + ISNULL(@DWQ2, 0)
	SET @DWQ3 = ISNULL(@DWQ2, 0) + ISNULL(@DWQ3, 0)
	SET @DWQ4 = ISNULL(@DWQ3, 0) + ISNULL(@DWQ4, 0)
	SET @DWYTD = ISNULL(@DWQ4, 0)
 
	SET @HNQ1 = ISNULL(@HNOLDYEAR, 0) + ISNULL(@HNQ1, 0)
	SET @HNQ2 = ISNULL(@HNQ1, 0) + ISNULL(@HNQ2, 0)
	SET @HNQ3 = ISNULL(@HNQ2, 0) + ISNULL(@HNQ3, 0)
	SET @HNQ4 = ISNULL(@HNQ3, 0) + ISNULL(@HNQ4, 0)
	SET @HNYTD = ISNULL(@HNQ4, 0)
 
	SET @100PIQ1 = ISNULL(@100PIOLDYEAR, 0) + ISNULL(@100PIQ1, 0)
	SET @100PIQ2 = ISNULL(@100PIQ1, 0) + ISNULL(@100PIQ2, 0)
	SET @100PIQ3 = ISNULL(@100PIQ2, 0) + ISNULL(@100PIQ3, 0)
	SET @100PIQ4 = ISNULL(@100PIQ3, 0) + ISNULL(@100PIQ4, 0)
	SET @100PIYTD = ISNULL(@100PIQ4, 0)
 
	SET @100P8YQ1 = ISNULL(@100P8YOLDYEAR, 0) + ISNULL(@100P8YQ1, 0)
	SET @100P8YQ2 = ISNULL(@100P8YQ1, 0) + ISNULL(@100P8YQ2, 0)
	SET @100P8YQ3 = ISNULL(@100P8YQ2, 0) + ISNULL(@100P8YQ3, 0)
	SET @100P8YQ4 = ISNULL(@100P8YQ3, 0) + ISNULL(@100P8YQ4, 0)
	SET @100P8YYTD = ISNULL(@100P8YQ4, 0)

	INSERT INTO @ResultTable
	VALUES(
		@BLACKOLDYEAR,@BLACKQ1 ,@BLACKQ2 ,@BLACKQ3 ,@BLACKQ4 ,@BLACKYTD ,
		@SMOLDYEAR ,@SMQ1 ,@SMQ2 ,@SMQ3 ,@SMQ4 ,@SMYTD ,
		@REDOLDYEAR ,@REDQ1 ,@REDQ2 ,@REDQ3 ,@REDQ4 ,@REDYTD ,
		@DBOLDYEAR ,@DBQ1 ,@DBQ2 ,@DBQ3 ,@DBQ4 ,@DBYTD ,
		@CBOLDYEAR ,@CBQ1 ,@CBQ2 ,@CBQ3 ,@CBQ4 ,@CBYTD ,
		@OTOLDYEAR ,@OTQ1 ,@OTQ2 ,@OTQ3 ,@OTQ4 ,@OTYTD ,
		@NVOLDYEAR ,@NVQ1 ,@NVQ2 ,@NVQ3 ,@NVQ4 ,@NVYTD ,	
		@BLOLDYEAR ,@BLQ1 ,@BLQ2 ,@BLQ3 ,@BLQ4 ,@BLYTD ,
		@BMFCOLDYEAR ,@BMFCQ1 ,@BMFCQ2 , @BMFCQ3 ,@BMFCQ4 ,@BMFCYTD ,
		@GOLDOLDYEAR ,@GOLDQ1 ,@GOLDQ2 ,@GOLDQ3 ,@GOLDQ4 ,@GOLDYTD ,
		@GREENOLDYEAR ,@GREENQ1 ,@GREENQ2 ,@GREENQ3 ,@GREENQ4 ,@GREENYTD ,
		@ASOLDYEAR ,@ASQ1 ,@ASQ2 ,@ASQ3 ,@ASQ4 ,@ASYTD ,
		@BLTOLDYEAR ,@BLTQ1 ,@BLTQ2 ,@BLTQ3 ,@BLTQ4 ,@BLTYTD ,
		@BL289OLDYEAR ,@BL289Q1 ,@BL289Q2 ,@BL289Q3 ,@BL289Q4 ,@BL289YTD ,
		@CVOLDYEAR ,@CVQ1 ,@CVQ2 ,@CVQ3 ,@CVQ4 ,@CVYTD ,
		@DWOLDYEAR ,@DWQ1 ,@DWQ2 ,@DWQ3 ,@DWQ4 ,@DWYTD ,
		@HNOLDYEAR ,@HNQ1 ,@HNQ2 ,@HNQ3 ,@HNQ4 ,@HNYTD ,
		@100PIOLDYEAR ,@100PIQ1 ,@100PIQ2 ,@100PIQ3 ,@100PIQ4 ,@100PIYTD ,
		@100P8YOLDYEAR ,@100P8YQ1 ,@100P8YQ2 ,@100P8YQ3 ,@100P8YQ4 ,@100P8YYTD 								 							
	)

	SELECT replace(convert(varchar,cast(BLACKOLDYEAR AS money ),1),'.00','') AS BLACKOLDYEAR ,replace(convert(varchar,cast(BLACKQ1 AS money ),1),'.00','') AS BLACKQ1 ,replace(convert(varchar,cast(BLACKQ2 AS money ),1),'.00','') AS BLACKQ2 ,replace(convert(varchar,cast(BLACKQ3 AS money ),1),'.00','') AS BLACKQ3 ,replace(convert(varchar,cast(BLACKQ4 AS money ),1),'.00','') AS BLACKQ4 ,replace(convert(varchar,cast(BLACKYTD AS money ),1),'.00','') AS BLACKYTD ,
	 replace(convert(varchar,cast(SMOLDYEAR AS money ),1),'.00','') AS SMOLDYEAR ,replace(convert(varchar,cast(SMQ1 AS money ),1),'.00','') AS SMQ1 ,replace(convert(varchar,cast(SMQ2 AS money ),1),'.00','') AS SMQ2 ,replace(convert(varchar,cast(SMQ3 AS money ),1),'.00','') AS SMQ3 ,replace(convert(varchar,cast(SMQ4 AS money ),1),'.00','') AS SMQ4 ,replace(convert(varchar,cast(SMYTD AS money ),1),'.00','') AS SMYTD ,
	 replace(convert(varchar,cast(REDOLDYEAR AS money ),1),'.00','') AS REDOLDYEAR ,replace(convert(varchar,cast(REDQ1 AS money ),1),'.00','') AS REDQ1 ,replace(convert(varchar,cast(REDQ2 AS money ),1),'.00','') AS REDQ2 ,replace(convert(varchar,cast(REDQ3 AS money ),1),'.00','') AS REDQ3 ,replace(convert(varchar,cast(REDQ4 AS money ),1),'.00','') AS REDQ4 ,replace(convert(varchar,cast(REDYTD AS money ),1),'.00','') AS REDYTD ,
	 replace(convert(varchar,cast(DBOLDYEAR AS money ),1),'.00','') AS DBOLDYEAR ,replace(convert(varchar,cast(DBQ1 AS money ),1),'.00','') AS DBQ1 ,replace(convert(varchar,cast(DBQ2 AS money ),1),'.00','') AS DBQ2 ,replace(convert(varchar,cast(DBQ3 AS money ),1),'.00','') AS DBQ3 ,replace(convert(varchar,cast(DBQ4 AS money ),1),'.00','') AS DBQ4 ,replace(convert(varchar,cast(DBYTD AS money ),1),'.00','') AS DBYTD ,
	 replace(convert(varchar,cast(CBOLDYEAR AS money ),1),'.00','') AS CBOLDYEAR ,replace(convert(varchar,cast(CBQ1 AS money ),1),'.00','') AS CBQ1 ,replace(convert(varchar,cast(CBQ2 AS money ),1),'.00','') AS CBQ2 ,replace(convert(varchar,cast(CBQ3 AS money ),1),'.00','') AS CBQ3 ,replace(convert(varchar,cast(CBQ4 AS money ),1),'.00','') AS CBQ4 ,replace(convert(varchar,cast(CBYTD AS money ),1),'.00','') AS CBYTD ,
	 replace(convert(varchar,cast(OTOLDYEAR AS money ),1),'.00','') AS OTOLDYEAR ,replace(convert(varchar,cast(OTQ1 AS money ),1),'.00','') AS OTQ1 ,replace(convert(varchar,cast(OTQ2 AS money ),1),'.00','') AS OTQ2 ,replace(convert(varchar,cast(OTQ3 AS money ),1),'.00','') AS OTQ3 ,replace(convert(varchar,cast(OTQ4 AS money ),1),'.00','') AS OTQ4 ,replace(convert(varchar,cast(OTYTD AS money ),1),'.00','') AS OTYTD ,
	 replace(convert(varchar,cast(NVOLDYEAR AS money ),1),'.00','') AS NVOLDYEAR ,replace(convert(varchar,cast(NVQ1 AS money ),1),'.00','') AS NVQ1 ,replace(convert(varchar,cast(NVQ2 AS money ),1),'.00','') AS NVQ2 ,replace(convert(varchar,cast(NVQ3 AS money ),1),'.00','') AS NVQ3 ,replace(convert(varchar,cast(NVQ4 AS money ),1),'.00','') AS NVQ4 ,replace(convert(varchar,cast(NVYTD AS money ),1),'.00','') AS NVYTD ,	
	 replace(convert(varchar,cast(BLOLDYEAR AS money ),1),'.00','') AS BLOLDYEAR ,replace(convert(varchar,cast(BLQ1 AS money ),1),'.00','') AS BLQ1 ,replace(convert(varchar,cast(BLQ2 AS money ),1),'.00','') AS BLQ2 ,replace(convert(varchar,cast(BLQ3 AS money ),1),'.00','') AS BLQ3 ,replace(convert(varchar,cast(BLQ4 AS money ),1),'.00','') AS BLQ4 ,replace(convert(varchar,cast(BLYTD AS money ),1),'.00','') AS BLYTD ,
	 replace(convert(varchar,cast(BMFCOLDYEAR AS money ),1),'.00','') AS BMFCOLDYEAR ,replace(convert(varchar,cast(BMFCQ1 AS money ),1),'.00','') AS BMFCQ1 ,replace(convert(varchar,cast(BMFCQ2 AS money ),1),'.00','') AS BMFCQ2 ,replace(convert(varchar,cast(BMFCQ3 AS money ),1),'.00','') AS BMFCQ3 ,replace(convert(varchar,cast(BMFCQ4 AS money ),1),'.00','') AS BMFCQ4 ,replace(convert(varchar,cast(BMFCYTD AS money ),1),'.00','') AS BMFCYTD ,
	 replace(convert(varchar,cast(GOLDOLDYEAR AS money ),1),'.00','') AS GOLDOLDYEAR ,replace(convert(varchar,cast(GOLDQ1 AS money ),1),'.00','') AS GOLDQ1 ,replace(convert(varchar,cast(GOLDQ2 AS money ),1),'.00','') AS GOLDQ2 ,replace(convert(varchar,cast(GOLDQ3 AS money ),1),'.00','') AS GOLDQ3 ,replace(convert(varchar,cast(GOLDQ4 AS money ),1),'.00','') AS GOLDQ4 ,replace(convert(varchar,cast(GOLDYTD AS money ),1),'.00','') AS GOLDYTD ,
	 replace(convert(varchar,cast(GREENOLDYEAR AS money ),1),'.00','') AS GREENOLDYEAR ,replace(convert(varchar,cast(GREENQ1 AS money ),1),'.00','') AS GREENQ1 ,replace(convert(varchar,cast(GREENQ2 AS money ),1),'.00','') AS GREENQ2 ,replace(convert(varchar,cast(GREENQ3 AS money ),1),'.00','') AS GREENQ3 ,replace(convert(varchar,cast(GREENQ4 AS money ),1),'.00','') AS GREENQ4 ,replace(convert(varchar,cast(GREENYTD AS money ),1),'.00','') AS GREENYTD ,
	 replace(convert(varchar,cast(ASOLDYEAR AS money ),1),'.00','') AS ASOLDYEAR ,replace(convert(varchar,cast(ASQ1 AS money ),1),'.00','') AS ASQ1 ,replace(convert(varchar,cast(ASQ2 AS money ),1),'.00','') AS ASQ2 ,replace(convert(varchar,cast(ASQ3 AS money ),1),'.00','') AS ASQ3 ,replace(convert(varchar,cast(ASQ4 AS money ),1),'.00','') AS ASQ4 ,replace(convert(varchar,cast(ASYTD AS money ),1),'.00','') AS ASYTD ,
	 replace(convert(varchar,cast(BLTOLDYEAR AS money ),1),'.00','') AS BLTOLDYEAR ,replace(convert(varchar,cast(BLTQ1 AS money ),1),'.00','') AS BLTQ1 ,replace(convert(varchar,cast(BLTQ2 AS money ),1),'.00','') AS BLTQ2 ,replace(convert(varchar,cast(BLTQ3 AS money ),1),'.00','') AS BLTQ3 ,replace(convert(varchar,cast(BLTQ4 AS money ),1),'.00','') AS BLTQ4 ,replace(convert(varchar,cast(BLTYTD AS money ),1),'.00','') AS BLTYTD ,
	 replace(convert(varchar,cast(BL289OLDYEAR AS money ),1),'.00','') AS BL289OLDYEAR ,replace(convert(varchar,cast(BL289Q1 AS money ),1),'.00','') AS BL289Q1 ,replace(convert(varchar,cast(BL289Q2 AS money ),1),'.00','') AS BL289Q2 ,replace(convert(varchar,cast(BL289Q3 AS money ),1),'.00','') AS BL289Q3 ,replace(convert(varchar,cast(BL289Q4 AS money ),1),'.00','') AS BL289Q4 ,replace(convert(varchar,cast(BL289YTD AS money ),1),'.00','') AS BL289YTD ,
	 replace(convert(varchar,cast(CVOLDYEAR AS money ),1),'.00','') AS CVOLDYEAR ,replace(convert(varchar,cast(CVQ1 AS money ),1),'.00','') AS CVQ1 ,replace(convert(varchar,cast(CVQ2 AS money ),1),'.00','') AS CVQ2 ,replace(convert(varchar,cast(CVQ3 AS money ),1),'.00','') AS CVQ3 ,replace(convert(varchar,cast(CVQ4 AS money ),1),'.00','') AS CVQ4 ,replace(convert(varchar,cast(CVYTD AS money ),1),'.00','') AS CVYTD ,
	 replace(convert(varchar,cast(DWOLDYEAR AS money ),1),'.00','') AS DWOLDYEAR ,replace(convert(varchar,cast(DWQ1 AS money ),1),'.00','') AS DWQ1 ,replace(convert(varchar,cast(DWQ2 AS money ),1),'.00','') AS DWQ2 ,replace(convert(varchar,cast(DWQ3 AS money ),1),'.00','') AS DWQ3 ,replace(convert(varchar,cast(DWQ4 AS money ),1),'.00','') AS DWQ4 ,replace(convert(varchar,cast(DWYTD AS money ),1),'.00','') AS DWYTD ,
	 replace(convert(varchar,cast(HNOLDYEAR AS money ),1),'.00','') AS HNOLDYEAR ,replace(convert(varchar,cast(HNQ1 AS money ),1),'.00','') AS HNQ1 ,replace(convert(varchar,cast(HNQ2 AS money ),1),'.00','') AS HNQ2 ,replace(convert(varchar,cast(HNQ3 AS money ),1),'.00','') AS HNQ3 ,replace(convert(varchar,cast(HNQ4 AS money ),1),'.00','') AS HNQ4 ,replace(convert(varchar,cast(HNYTD AS money ),1),'.00','') AS HNYTD ,
	 replace(convert(varchar,cast(ONEHUNDREADPIOLDYEAR AS money ),1),'.00','') AS ONEHUNDREADPIOLDYEAR ,replace(convert(varchar,cast(ONEHUNDREADPIQ1 AS money ),1),'.00','') AS ONEHUNDREADPIQ1 ,replace(convert(varchar,cast(ONEHUNDREADPIQ2 AS money ),1),'.00','') AS ONEHUNDREADPIQ2 ,replace(convert(varchar,cast(ONEHUNDREADPIQ3 AS money ),1),'.00','') AS ONEHUNDREADPIQ3 ,replace(convert(varchar,cast(ONEHUNDREADPIQ4 AS money ),1),'.00','') AS ONEHUNDREADPIQ4 ,replace(convert(varchar,cast(ONEHUNDREADPIYTD AS money ),1),'.00','') AS ONEHUNDREADPIYTD ,
	 replace(convert(varchar,cast(ONEHUNDREADP8YOLDYEAR AS money ),1),'.00','') AS ONEHUNDREADP8YOLDYEAR ,replace(convert(varchar,cast(ONEHUNDREAD8YQ1 AS money ),1),'.00','') AS ONEHUNDREAD8YQ1 ,replace(convert(varchar,cast(ONEHUNDREADP8YQ2 AS money ),1),'.00','') AS ONEHUNDREADP8YQ2 ,replace(convert(varchar,cast(ONEHUNDREADP8YQ3 AS money ),1),'.00','') AS ONEHUNDREADP8YQ3 ,replace(convert(varchar,cast(ONEHUNDREADP8YQ4 AS money ),1),'.00','') AS ONEHUNDREADP8YQ4 ,replace(convert(varchar,cast(ONEHUNDREADP8YYTD AS money ),1),'.00','') AS ONEHUNDREADP8YYTD  
	 FROM @ResultTable
	
END
