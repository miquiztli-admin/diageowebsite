USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_HK_Recruit]    Script Date: 10/01/2013 16:56:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_HK_Recruit', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_HK_Recruit;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Update date: <26/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_HK_Recruit](@YYYY as integer)
AS
BEGIN

	Declare @newQ1 bigint = 0,@newQ2 bigint = 0,@newQ3 bigint = 0,@newQ4 bigint = 0
	Declare @subscribeBY bigint = 0,@subscribeQ1 bigint = 0,@subscribeQ2 bigint = 0,@subscribeQ3 bigint = 0,@subscribeQ4 bigint = 0
	Declare @4AResponsBY bigint = 0,@4AResponsQ1 bigint = 0,@4AResponsQ2 bigint = 0,@4AResponsQ3 bigint = 0,@4AResponsQ4 bigint = 0
	Declare @PromotionBY bigint = 0,@PromotionQ1 bigint = 0,@PromotionQ2 bigint = 0,@PromotionQ3 bigint = 0,@PromotionQ4 bigint = 0
	Declare @UnEmailBY bigint = 0,@UnEmailQ1 bigint = 0,@UnEmailQ2 bigint = 0,@UnEmailQ3 bigint = 0,@UnEmailQ4 bigint = 0
	Declare @InvalidEmailBY bigint = 0,@InvalidEmailQ1 bigint = 0,@InvalidEmailQ2 bigint = 0,@InvalidEmailQ3 bigint = 0,@InvalidEmailQ4 bigint = 0
	Declare @InvalidMobileBY bigint = 0,@InvalidMobileQ1 bigint = 0,@InvalidMobileQ2 bigint = 0,@InvalidMobileQ3 bigint = 0,@InvalidMobileQ4 bigint = 0

	DECLARE @tmpQuarterNum int, @tmpVal1 bigint, @tmpVal2 bigint, @tmpVal3 bigint, @tmpVal4 bigint, @tmpVal5 bigint
	DECLARE @cCursor CURSOR
	DECLARE @tmpFilter int, @tmpValue bigint
	
	DECLARE @_Q1 INT, @_Q2 INT, @_Q3 INT, @_Q4 INT
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarter,
		CASE
			WHEN MIN(DATEADD(MONTH, month_num - 1, CONVERT(DATE, '01-01-' + CAST(@YYYY + year_shift AS CHAR(4))))) > GETDATE() THEN 0
			ELSE 1
		END
		FROM tbl_m_rpt_quarter_month t
		GROUP BY t.quarter
	
	OPEN @cCursor
	FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpFilter = 1)
			SET @_Q1 = @tmpValue
		ELSE IF (@tmpFilter = 2)
			SET @_Q2 = @tmpValue
		ELSE IF (@tmpFilter = 3)
			SET @_Q3 = @tmpValue
		ELSE IF (@tmpFilter = 4)
			SET @_Q4 = @tmpValue
		
		FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue
	END
	CLOSE @cCursor
	
	SELECT 
	@newQ1 = ISNULL(COUNT(DISTINCT(CASE WHEN cre_quarterNum = 1 THEN consumerId ELSE NULL END)), 0),
	@newQ2 = ISNULL(COUNT(DISTINCT(CASE WHEN cre_quarterNum = 2 THEN consumerId ELSE NULL END)), 0),
	@newQ3 = ISNULL(COUNT(DISTINCT(CASE WHEN cre_quarterNum = 3 THEN consumerId ELSE NULL END)), 0),
	@newQ4 = ISNULL(COUNT(DISTINCT(CASE WHEN cre_quarterNum = 4 THEN consumerId ELSE NULL END)), 0)
	FROM vw_rpt_dashb_hk_by_quarter
	WHERE cre_quarterYear = @YYYY

	SELECT
	@subscribeBY = ISNULL(COUNT(DISTINCT(subscribe)), 0),
	@UnEmailBY = ISNULL(COUNT(DISTINCT(unsubscribe_email)), 0),
	@InvalidEmailBY = ISNULL(COUNT(DISTINCT(invalid_email)), 0),
	@InvalidMobileBY = ISNULL(COUNT(DISTINCT(invalid_mobile)), 0),
	@4AResponsBY = ISNULL(COUNT(DISTINCT(CASE WHEN isHK = 1 THEN consumerId ELSE NULL END)), 0) -- 4A Response
	FROM vw_rpt_dashb_hk_by_quarter
	WHERE upd_quarterYear < @YYYY

	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT upd_quarterNum, 
		ISNULL(COUNT(DISTINCT(subscribe)), 0),
		ISNULL(COUNT(DISTINCT(unsubscribe_email)), 0),
		ISNULL(COUNT(DISTINCT(invalid_email)), 0),
		ISNULL(COUNT(DISTINCT(invalid_mobile)), 0),
		ISNULL(COUNT(DISTINCT(CASE WHEN isHK = 1 THEN consumerId ELSE NULL END)), 0) -- 4A Response
		FROM vw_rpt_dashb_hk_by_quarter
		WHERE upd_quarterYear = @YYYY
		GROUP BY upd_quarterNum
		ORDER BY upd_quarterNum ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpVal1, @tmpVal2, @tmpVal3, @tmpVal4, @tmpVal5
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
		BEGIN
			SET @subscribeQ1 = @tmpVal1
			SET @UnEmailQ1 = @tmpVal2
			SET @InvalidEmailQ1 = @tmpVal3
			SET @InvalidMobileQ1 = @tmpVal4
			SET @4AResponsQ1 = @tmpVal5
		END
		ELSE IF (@tmpQuarterNum = 2)
		BEGIN
			SET @subscribeQ2 = @tmpVal1
			SET @UnEmailQ2 = @tmpVal2
			SET @InvalidEmailQ2 = @tmpVal3
			SET @InvalidMobileQ2 = @tmpVal4
			SET @4AResponsQ2 = @tmpVal5
		END
		ELSE IF (@tmpQuarterNum = 3)
		BEGIN
			SET @subscribeQ3 = @tmpVal1
			SET @UnEmailQ3 = @tmpVal2
			SET @InvalidEmailQ3 = @tmpVal3
			SET @InvalidMobileQ3 = @tmpVal4
			SET @4AResponsQ3 = @tmpVal5
		END
		ELSE IF (@tmpQuarterNum = 4)
		BEGIN
			SET @subscribeQ4 = @tmpVal1
			SET @UnEmailQ4 = @tmpVal2
			SET @InvalidEmailQ4 = @tmpVal3
			SET @InvalidMobileQ4 = @tmpVal4
			SET @4AResponsQ4 = @tmpVal5
		END
		
		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpVal1, @tmpVal2, @tmpVal3, @tmpVal4, @tmpVal5
	END
	
	CLOSE @cCursor
	
--************* Not confirmed *********************
--Promotion Participation
	SELECT 
	@PromotionBY = COUNT(DISTINCT(redeem_consumer))
	FROM vw_rpt_dashboard_promotion_by_quarter
	WHERE quarterYear < @YYYY

	SELECT 
	@PromotionQ1 = ISNULL(COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN redeem_consumer ELSE NULL END)), 0),
	@PromotionQ2 = ISNULL(COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN redeem_consumer ELSE NULL END)), 0),
	@PromotionQ3 = ISNULL(COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN redeem_consumer ELSE NULL END)), 0),
	@PromotionQ4 = ISNULL(COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN redeem_consumer ELSE NULL END)), 0)
	FROM vw_rpt_dashboard_promotion_by_quarter
	WHERE quarterYear = @YYYY
	GROUP BY quarterNum
	ORDER BY quarterNum ASC
--************* Not confirmed *********************

--SUM value
	SET @subscribeQ1 = ISNULL(@subscribeBY, 0) + ISNULL(@subscribeQ1, 0)
	SET @subscribeQ2 = ISNULL(@subscribeQ1, 0) + ISNULL(@subscribeQ2, 0)
	SET @subscribeQ3 = ISNULL(@subscribeQ2, 0) + ISNULL(@subscribeQ3, 0)
	SET @subscribeQ4 = ISNULL(@subscribeQ3, 0) + ISNULL(@subscribeQ4, 0)

	SET @UnEmailQ1 = ISNULL(@UnEmailBY, 0) + ISNULL(@UnEmailQ1, 0)
	SET @UnEmailQ2 = ISNULL(@UnEmailQ1, 0) + ISNULL(@UnEmailQ2, 0)
	SET @UnEmailQ3 = ISNULL(@UnEmailQ2, 0) + ISNULL(@UnEmailQ3, 0)
	SET @UnEmailQ4 = ISNULL(@UnEmailQ3, 0) + ISNULL(@UnEmailQ4, 0)

	SET @InvalidEmailQ1 = ISNULL(@InvalidEmailBY, 0) + ISNULL(@InvalidEmailQ1, 0)
	SET @InvalidEmailQ2 = ISNULL(@InvalidEmailQ1, 0) + ISNULL(@InvalidEmailQ2, 0)
	SET @InvalidEmailQ3 = ISNULL(@InvalidEmailQ2, 0) + ISNULL(@InvalidEmailQ3, 0)
	SET @InvalidEmailQ4 = ISNULL(@InvalidEmailQ3, 0) + ISNULL(@InvalidEmailQ4, 0)

	SET @InvalidMobileQ1 = ISNULL(@InvalidMobileBY, 0) + ISNULL(@InvalidMobileQ1, 0)
	SET @InvalidMobileQ2 = ISNULL(@InvalidMobileQ1, 0) + ISNULL(@InvalidMobileQ2, 0)
	SET @InvalidMobileQ3 = ISNULL(@InvalidMobileQ2, 0) + ISNULL(@InvalidMobileQ3, 0)
	SET @InvalidMobileQ4 = ISNULL(@InvalidMobileQ3, 0) + ISNULL(@InvalidMobileQ4, 0)

	SET @4AResponsQ1 = ISNULL(@4AResponsBY, 0) + ISNULL(@4AResponsQ1, 0)
	SET @4AResponsQ2 = ISNULL(@4AResponsQ1, 0) + ISNULL(@4AResponsQ2, 0)
	SET @4AResponsQ3 = ISNULL(@4AResponsQ2, 0) + ISNULL(@4AResponsQ3, 0)
	SET @4AResponsQ4 = ISNULL(@4AResponsQ3, 0) + ISNULL(@4AResponsQ4, 0)
	
	SET @PromotionQ1 = ISNULL(@PromotionBY, 0) + ISNULL(@PromotionQ1, 0)
	SET @PromotionQ2 = ISNULL(@PromotionQ1, 0) + ISNULL(@PromotionQ2, 0)
	SET @PromotionQ3 = ISNULL(@PromotionQ2, 0) + ISNULL(@PromotionQ3, 0)
	SET @PromotionQ4 = ISNULL(@PromotionQ3, 0) + ISNULL(@PromotionQ4, 0)
	
-- Return
	SELECT
	 CASE @_Q1 WHEN 1 THEN @newQ1 ELSE NULL END AS newQ1, CASE @_Q2 WHEN 1 THEN @newQ2 ELSE NULL END AS newQ2, CASE @_Q3 WHEN 1 THEN @newQ3 ELSE NULL END AS newQ3, CASE @_Q4 WHEN 1 THEN @newQ4 ELSE NULL END AS newQ4, 
	 CASE @_Q1 WHEN 1 THEN @subscribeQ1 ELSE NULL END AS subscribeQ1, CASE @_Q2 WHEN 1 THEN @subscribeQ2 ELSE NULL END AS subscribeQ2, CASE @_Q3 WHEN 1 THEN @subscribeQ3 ELSE NULL END AS subscribeQ3, CASE @_Q4 WHEN 1 THEN @subscribeQ4 ELSE NULL END AS subscribeQ4, 
	 CASE @_Q1 WHEN 1 THEN @4AResponsQ1 ELSE NULL END AS "4AResponsQ1", CASE @_Q2 WHEN 1 THEN @4AResponsQ2 ELSE NULL END AS "4AResponsQ2", CASE @_Q3 WHEN 1 THEN @4AResponsQ3 ELSE NULL END AS "4AResponsQ3", CASE @_Q4 WHEN 1 THEN @4AResponsQ4 ELSE NULL END AS "4AResponsQ4", 
	 CASE @_Q1 WHEN 1 THEN @PromotionQ1 ELSE NULL END AS PromotionQ1, CASE @_Q2 WHEN 1 THEN @PromotionQ2 ELSE NULL END AS PromotionQ2, CASE @_Q3 WHEN 1 THEN @PromotionQ3 ELSE NULL END AS PromotionQ3, CASE @_Q4 WHEN 1 THEN @PromotionQ4 ELSE NULL END AS PromotionQ4, 
	 CASE @_Q1 WHEN 1 THEN @UnEmailQ1 ELSE NULL END AS UnEmailQ1, CASE @_Q2 WHEN 1 THEN @UnEmailQ2 ELSE NULL END AS UnEmailQ2, CASE @_Q3 WHEN 1 THEN @UnEmailQ3 ELSE NULL END AS UnEmailQ3, CASE @_Q4 WHEN 1 THEN @UnEmailQ4 ELSE NULL END AS UnEmailQ4, 
	 CASE @_Q1 WHEN 1 THEN @InvalidEmailQ1 ELSE NULL END AS InvalidEmailQ1, CASE @_Q2 WHEN 1 THEN @InvalidEmailQ2 ELSE NULL END AS InvalidEmailQ2, CASE @_Q3 WHEN 1 THEN @InvalidEmailQ3 ELSE NULL END AS InvalidEmailQ3, CASE @_Q4 WHEN 1 THEN @InvalidEmailQ4 ELSE NULL END AS InvalidEmailQ4, 
	 CASE @_Q1 WHEN 1 THEN @InvalidMobileQ1 ELSE NULL END AS InvalidMobileQ1, CASE @_Q2 WHEN 1 THEN @InvalidMobileQ2 ELSE NULL END AS InvalidMobileQ2, CASE @_Q3 WHEN 1 THEN @InvalidMobileQ3 ELSE NULL END AS InvalidMobileQ3, CASE @_Q4 WHEN 1 THEN @InvalidMobileQ4 ELSE NULL END AS InvalidMobileQ4

END
