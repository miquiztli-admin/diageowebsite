USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_DashboaRd_Black_Outbound]    Script Date: 10/01/2013 14:57:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_DashboaRd_Black_Outbound', N'P') IS NOT NULL
    DROP PROC Sp_Rep_DashboaRd_Black_Outbound;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <1/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_DashboaRd_Black_Outbound](@YYYY as integer)
AS
BEGIN

	SELECT 
	COUNT(DISTINCT(
		CASE 
			WHEN outboundTypeID = 1 AND quarterNum = 1 THEN consumerId
			ELSE NULL
		END
	)) AS edmQ1,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 1 AND quarterNum = 2 THEN consumerId
			ELSE NULL
		END
	)) AS edmQ2,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 1 AND quarterNum = 3 THEN consumerId
			ELSE NULL
		END
	)) AS edmQ3,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 1 AND quarterNum = 4 THEN consumerId
			ELSE NULL
		END
	)) AS edmQ4,
	COUNT(DISTINCT(
		CASE 
			WHEN outboundTypeID = 2 AND quarterNum = 1 THEN consumerId
			ELSE NULL
		END
	)) AS smsQ1,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 2 AND quarterNum = 2 THEN consumerId
			ELSE NULL
		END
	)) AS smsQ2,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 2 AND quarterNum = 3 THEN consumerId
			ELSE NULL
		END
	)) AS smsQ3,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 2 AND quarterNum = 4 THEN consumerId
			ELSE NULL
		END
	)) AS smsQ4,
	COUNT(DISTINCT(
		CASE 
			WHEN outboundTypeID = 3 AND quarterNum = 1 THEN consumerId
			ELSE NULL
		END
	)) AS ccQ1,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 3 AND quarterNum = 2 THEN consumerId
			ELSE NULL
		END
	)) AS ccQ2,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 3 AND quarterNum = 3 THEN consumerId
			ELSE NULL
		END
	)) AS ccQ3,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 3 AND quarterNum = 4 THEN consumerId
			ELSE NULL
		END
	)) AS ccQ4,
	COUNT(DISTINCT(
		CASE 
			WHEN outboundTypeID = 1 AND quarterNum = 1 AND lower(result) = 'success' THEN consumerId
			ELSE NULL
		END
	)) AS edmDelQ1,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 1 AND quarterNum = 2 AND lower(result) = 'success' THEN consumerId
			ELSE NULL
		END
	)) AS edmDelQ2,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 1 AND quarterNum = 3 AND lower(result) = 'success' THEN consumerId
			ELSE NULL
		END
	)) AS edmDelQ3,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 1 AND quarterNum = 4 AND lower(result) = 'success' THEN consumerId
			ELSE NULL
		END
	)) AS edmDelQ4,
	COUNT(DISTINCT(
		CASE 
			WHEN outboundTypeID = 1 AND quarterNum = 1 AND openStatus <> 'NONE' THEN consumerId
			ELSE NULL
		END
	)) AS edmOpenQ1,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 1 AND quarterNum = 2 AND openStatus <> 'NONE' THEN consumerId
			ELSE NULL
		END
	)) AS edmOpenQ2,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 1 AND quarterNum = 3 AND openStatus <> 'NONE' THEN consumerId
			ELSE NULL
		END
	)) AS edmOpenQ3,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 1 AND quarterNum = 4 AND openStatus <> 'NONE' THEN consumerId
			ELSE NULL
		END
	)) AS edmOpenQ4,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 1 AND quarterNum = 1 AND clickLink = 'Y' THEN consumerId
			ELSE NULL
		END
	)) AS edmClickQ1,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 1 AND quarterNum = 2 AND clickLink = 'Y' THEN consumerId
			ELSE NULL
		END
	)) AS edmClickQ2,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 1 AND quarterNum = 3 AND clickLink = 'Y' THEN consumerId
			ELSE NULL
		END
	)) AS edmClickQ3,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 1 AND quarterNum = 4 AND clickLink = 'Y' THEN consumerId
			ELSE NULL
		END
	)) AS edmClickQ4,
	COUNT(DISTINCT(
		CASE 
			WHEN outboundTypeID = 2 AND quarterNum = 1 AND lower(result) = 'success' THEN consumerId
			ELSE NULL
		END
	)) AS smsResponseQ1,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 2 AND quarterNum = 2 AND lower(result) = 'success' THEN consumerId
			ELSE NULL
		END
	)) AS smsResponseQ2,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 2 AND quarterNum = 3 AND lower(result) = 'success' THEN consumerId
			ELSE NULL
		END
	)) AS smsResponseQ3,
	COUNT(DISTINCT(
		CASE
			WHEN outboundTypeID = 2 AND quarterNum = 4 AND lower(result) = 'success' THEN consumerId
			ELSE NULL
		END
	)) AS smsResponseQ4
	FROM vw_rpt_outbound_consumer_by_quarter
	WHERE quarterYear = @YYYY
	AND (brandPreJwBlack = 'T' OR survey4aJwBlack IS NOT NULL)
	AND outboundTypeID IN (1, 2, 3)

END
