USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_Black_DigitalTouchPoint_Graph]    Script Date: 10/01/2013 14:34:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_Black_DigitalTouchPoint_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_Black_DigitalTouchPoint_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <1/10/2013>
-- Update date: <12/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_Black_DigitalTouchPoint_Graph](@YYYY as integer)
AS
BEGIN

	SELECT tmp.period AS Period, 
	ISNULL(rpt.EDM, 0) AS EDM,
	ISNULL(rpt.SMS, 0) AS SMS
	FROM tbl_m_rpt_dashb_templ_graph tmp
		LEFT OUTER JOIN (
			SELECT quarterNum AS tmp_num,
			COUNT(DISTINCT(
				CASE outboundTypeID
					WHEN 1 THEN eventId
					ELSE NULL
				END
			)) AS EDM,
			COUNT(DISTINCT(
				CASE outboundTypeID
					WHEN 2 THEN eventId
					ELSE NULL
				END
			)) AS SMS
			FROM vw_rpt_outboundEvent_by_quarter
			WHERE quarterYear = @YYYY
			AND OutboundTypeID IN (1, 2)
			AND brandPreJWBlack ='T'
			GROUP BY quarterNum
			
			UNION ALL 
			SELECT 5 AS tmp_num,
			COUNT(DISTINCT(
				CASE outboundTypeID
					WHEN 1 THEN eventId
					ELSE NULL
				END
			)) AS EDM,
			COUNT(DISTINCT(
				CASE outboundTypeID
					WHEN 2 THEN eventId
					ELSE NULL
				END
			)) AS SMS
			FROM vw_rpt_outboundEvent_by_quarter
			WHERE quarterYear = @YYYY
			AND brandPreJWBlack ='T'
			AND OutboundTypeID IN (1, 2)
			
			UNION ALL 
			SELECT 6 AS tmp_num, 0 AS EDM, 0 AS SMS
		) rpt ON tmp.num = rpt.tmp_num 

END
