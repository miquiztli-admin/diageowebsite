USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_DashboaRd_Black_Recruit_Graph]    Script Date: 10/03/2013 14:02:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_DashboaRd_Black_Recruit_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_DashboaRd_Black_Recruit_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <03/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_DashboaRd_Black_Recruit_Graph](@YYYY as integer)
AS
BEGIN

	Declare @newQ1 bigint,@newQ2 bigint,@newQ3 bigint,@newQ4 bigint
	Declare @subscribeQ1 bigint,@subscribeQ2 bigint,@subscribeQ3 bigint,@subscribeQ4 bigint
	Declare @4AResponsQ1 bigint,@4AResponsQ2 bigint,@4AResponsQ3 bigint,@4AResponsQ4 bigint
	Declare @PromotionQ1 bigint,@PromotionQ2 bigint,@PromotionQ3 bigint,@PromotionQ4 bigint

	Declare @YTDNewRecruit bigint,@YTDSubscribe bigint,@YTDResponse bigint,@YTDPromotion bigint
	Declare @YearTargetNewRecruit bigint,@YearTargetSubscribe bigint,@YearTargetResponse bigint,@YearTargetPromotion bigint

	DECLARE @cCursor CURSOR
	DECLARE @tmpQuarterNum int, @tmpVal1 bigint, @tmpVal2 bigint, @tmpVal3 bigint, @tmpVal4 bigint, @tmpVal5 bigint

	--Create Temp Table
	Begin
	Create Table #ResultTable ( period varchar(20),category varchar(50),subscribe bigint,response4A bigint,promotion bigint,NewRecruit bigint)
	End

	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarterNum, 
		COUNT(DISTINCT(consumerId)),
		COUNT(DISTINCT(subscribe))
		FROM vw_rpt_consumer_register_by_quarter
		WHERE quarterYear = @YYYY
		AND (brandPreJwBlack ='T' OR (survey4aJwBlack IS NOT NULL /*AND len(survey4aJwBlack) > 0*/))
		GROUP BY quarterNum
		ORDER BY quarterNum ASC
		
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpVal1, @tmpVal2
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
		BEGIN
			SET @newQ1 = @tmpVal1
			SET @subscribeQ1 = @tmpVal2
		END
		ELSE IF (@tmpQuarterNum = 2)
		BEGIN
			SET @newQ2 = @tmpVal1
			SET @subscribeQ2 = @tmpVal2
		END
		ELSE IF (@tmpQuarterNum = 3)
		BEGIN
			SET @newQ3 = @tmpVal1
			SET @subscribeQ3 = @tmpVal2
		END
		ELSE IF (@tmpQuarterNum = 4)
		BEGIN
			SET @newQ4 = @tmpVal1
			SET @subscribeQ4 = @tmpVal2
		END
		
		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpVal1, @tmpVal2
	END
	
	CLOSE @cCursor

--Promotion Participation
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarterNum, COUNT(DISTINCT(redeem_consumer))
		FROM vw_rpt_dashboard_promotion_by_quarter
		WHERE quarterYear = @YYYY
		GROUP BY quarterNum
		ORDER BY quarterNum ASC
		
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpVal1
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
			SET @PromotionQ1 = @tmpVal1
		ELSE IF (@tmpQuarterNum = 2)
			SET @PromotionQ2 = @tmpVal1
		ELSE IF (@tmpQuarterNum = 3)
			SET @PromotionQ3 = @tmpVal1
		ELSE IF (@tmpQuarterNum = 4)
			SET @PromotionQ4 = @tmpVal1
		
		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpVal1
	END
	
	CLOSE @cCursor

	--4A Response/Updates
	Begin
		set @4AResponsQ1=0
		set @4AResponsQ2=0
		set @4AResponsQ3=0
		set @4AResponsQ4=0
	End

	--4A Response/Updates
	Begin
		set @4AResponsQ1=0
		set @4AResponsQ2=0
		set @4AResponsQ3=0
		set @4AResponsQ4=0
	End

	--YTD 
	Begin
		set @YTDNewRecruit=@newQ1+@newQ2+@newQ3+@newQ4
		set @YTDSubscribe=@subscribeQ1+@subscribeQ2+@subscribeQ3+@subscribeQ4
		set @YTDResponse=0
		set @YTDPromotion=@PromotionQ1+@PromotionQ2+@PromotionQ3+@PromotionQ4
	End

	--Year Target
	Begin
		set @YearTargetNewRecruit=0
		set @YearTargetSubscribe=0
		set @YearTargetResponse=0
		set @YearTargetPromotion=0
	End

	--insert Q1
	insert into #ResultTable
	values('Q1','',@subscribeQ1,@4AResponsQ1,@PromotionQ1,@newQ1)

	--insert Q2
	insert into #ResultTable
	values('Q2','',@subscribeQ2,@4AResponsQ2,@PromotionQ2,@newQ2)

	--insert Q3
	insert into #ResultTable
	values('Q3','',@subscribeQ3,@4AResponsQ3,@PromotionQ3,@newQ3)

	--insert Q4
	insert into #ResultTable
	values('Q4','',@subscribeQ4,@4AResponsQ4,@PromotionQ4,@newQ4)

	--insert YTD
	insert into #ResultTable
	values('YTD','',@YTDSubscribe,@YTDResponse,@YTDPromotion,@YTDNewRecruit)

	--insert Target
	insert into #ResultTable
	values('Target','',@YearTargetSubscribe,@YearTargetResponse,@YearTargetPromotion,@YearTargetNewRecruit)

	select * from #ResultTable

	Drop Table #ResultTable

END
