USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_Black_DB_Graph_Contactable]    Script Date: 09/29/2013 17:23:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_Black_DB_Graph_Contactable', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_Black_DB_Graph_Contactable;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_Black_DB_Graph_Contactable](@YYYY as integer)
AS
BEGIN

	SELECT tmp.period, 
	ISNULL(rpt.Email, 0) AS Email,
	ISNULL(rpt.Mobile, 0) AS Mobile,
	ISNULL(rpt.Both, 0) AS Both
	FROM tbl_m_rpt_dashb_templ_graph tmp
		LEFT OUTER JOIN (
			SELECT quarterNum AS tmp_num,
			'Q' + CAST(quarterNum AS VARCHAR(1)) AS period, 
			COUNT(DISTINCT(emailOnly)) AS Email, 
			COUNT(DISTINCT(mobileOnly)) AS Mobile, 
			COUNT(DISTINCT(mobileEmail)) AS Both
			FROM vw_rpt_consumer_by_quarter
			WHERE quarterYear = @YYYY
			AND (brandPreJwBlack ='T' OR (survey4aJwBlack IS NOT NULL /*AND len(survey4aJwBlack) > 0*/))
			AND contactable IS NOT NULL
			GROUP BY quarterNum
			
			UNION ALL
			SELECT 5 AS tmp_num, 'YTD' as period,
			COUNT(DISTINCT(emailOnly)) AS Email, 
			COUNT(DISTINCT(mobileOnly)) AS Mobile, 
			COUNT(DISTINCT(mobileEmail)) AS Both
			FROM vw_rpt_consumer_by_quarter
			WHERE quarterYear = @YYYY
			AND (brandPreJwBlack ='T' OR (survey4aJwBlack IS NOT NULL /*AND len(survey4aJwBlack) > 0*/))
			AND contactable IS NOT NULL
			
			UNION ALL
			SELECT 6 AS tmp_num, 'Target' as period, 0 as Email, 0 as Mobile, 0 as Both
		) rpt ON tmp.num = rpt.tmp_num 
	
/*
Begin
Create Table #ResultTable ( period varchar(20),Email bigint,Mobile bigint,Both bigint)
End

SET @ContactableBYear = @EmailBY + @MobileBY + @EmailMobileBY
SET @ContactableQ1 = @Emailq1 + @MobileQ1 + @EmailMobileQ1
SET @ContactableQ2 = @Emailq2 + @MobileQ2 + @EmailMobileQ2
SET @ContactableQ3 = @Emailq3 + @MobileQ3 + @EmailMobileQ3
SET @ContactableQ4 = @Emailq4 + @MobileQ4 + @EmailMobileQ4

--YTD 
Begin
	set @YTDEmail=0
	set @YTDMobile=0
	set @YTDBoth=0
End

--Year Target
Begin
	set @YearTargetEmail=0
	set @YearTargetMobile=0
	set @YearTargetBoth=0
End

--insert Q1
insert into #ResultTable
values('Q1',@EmailQ1,@MobileQ1,@EmailMobileQ1)

--insert Q2
insert into #ResultTable
values('Q2',@EmailQ2,@MobileQ2,@EmailMobileQ2)

--insert Q3
insert into #ResultTable
values('Q3',@EmailQ3,@MobileQ3,@EmailMobileQ3)

--insert Q4
insert into #ResultTable
values('Q4',@EmailQ4,@MobileQ4,@EmailMobileQ4)

--insert YTD
insert into #ResultTable
values('YTD',@EmailYTD, @MobileYTD,@EmailMobileYTD)

--insert Target
insert into #ResultTable
values('Target',@YearTargetEmail,@YearTargetMobile,@YearTargetBoth)

----insert Q1
--insert into #ResultTable
--values('Q1',24,44,23)

----insert Q2
--insert into #ResultTable
--values('Q2',13,43,22)

----insert Q3
--insert into #ResultTable
--values('Q3',34,52,11)

----insert Q4
--insert into #ResultTable
--values('Q4',33,44,55)

----insert YTD
--insert into #ResultTable
--values('YTD',34, 23,44)

----insert Target
--insert into #ResultTable
--values('Target',25,34,55)

select * from #ResultTable

Drop Table #ResultTable
*/

END
