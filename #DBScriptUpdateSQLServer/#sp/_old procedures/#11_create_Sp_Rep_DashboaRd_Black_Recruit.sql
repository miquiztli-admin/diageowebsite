USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_DashboaRd_Black_Recruit]    Script Date: 10/01/2013 16:56:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_DashboaRd_Black_Recruit', N'P') IS NOT NULL
    DROP PROC Sp_Rep_DashboaRd_Black_Recruit;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_DashboaRd_Black_Recruit](@YYYY as integer)
AS
BEGIN

Declare @newQ1 bigint,@newQ2 bigint,@newQ3 bigint,@newQ4 bigint
Declare @subscribeQ1 bigint,@subscribeQ2 bigint,@subscribeQ3 bigint,@subscribeQ4 bigint
Declare @4AResponsQ1 bigint,@4AResponsQ2 bigint,@4AResponsQ3 bigint,@4AResponsQ4 bigint
Declare @PromotionQ1 bigint,@PromotionQ2 bigint,@PromotionQ3 bigint,@PromotionQ4 bigint
Declare @UnEmailQ1 bigint,@UnEmailQ2 bigint,@UnEmailQ3 bigint,@UnEmailQ4 bigint
Declare @InvalidEmailQ1 bigint,@InvalidEmailQ2 bigint,@InvalidEmailQ3 bigint,@InvalidEmailQ4 bigint
Declare @InvalidMobileQ1 bigint,@InvalidMobileQ2 bigint,@InvalidMobileQ3 bigint,@InvalidMobileQ4 bigint

	DECLARE @cCursor CURSOR
	DECLARE @tmpQuarterNum int, @tmpVal1 bigint, @tmpVal2 bigint, @tmpVal3 bigint, @tmpVal4 bigint, @tmpVal5 bigint

	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarterNum, COUNT(DISTINCT(consumerId)),
		COUNT(DISTINCT(subscribe)),
		COUNT(DISTINCT(unsubscribe_email)),
		COUNT(DISTINCT(invalid_email)),
		COUNT(DISTINCT(invalid_mobile))
		FROM vw_rpt_consumer_register_by_quarter
		WHERE quarterYear = @YYYY
		AND (brandPreJwBlack ='T' OR (survey4aJwBlack IS NOT NULL /*AND len(survey4aJwBlack) > 0*/))
		GROUP BY quarterNum
		ORDER BY quarterNum ASC
		
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpVal1, @tmpVal2, @tmpVal3, @tmpVal4, @tmpVal5
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
		BEGIN
			SET @newQ1 = @tmpVal1
			SET @subscribeQ1 = @tmpVal2
			SET @UnEmailQ1 = @tmpVal3
			SET @InvalidEmailQ1 = @tmpVal4
			SET @InvalidMobileQ1 = @tmpVal5
		END
		ELSE IF (@tmpQuarterNum = 2)
		BEGIN
			SET @newQ2 = @tmpVal1
			SET @subscribeQ2 = @tmpVal2
			SET @UnEmailQ2 = @tmpVal3
			SET @InvalidEmailQ2 = @tmpVal4
			SET @InvalidMobileQ2 = @tmpVal5
		END
		ELSE IF (@tmpQuarterNum = 3)
		BEGIN
			SET @newQ3 = @tmpVal1
			SET @subscribeQ3 = @tmpVal2
			SET @UnEmailQ3 = @tmpVal3
			SET @InvalidEmailQ3 = @tmpVal4
			SET @InvalidMobileQ3 = @tmpVal5
		END
		ELSE IF (@tmpQuarterNum = 4)
		BEGIN
			SET @newQ4 = @tmpVal1
			SET @subscribeQ4 = @tmpVal2
			SET @UnEmailQ4 = @tmpVal3
			SET @InvalidEmailQ4 = @tmpVal4
			SET @InvalidMobileQ4 = @tmpVal5
		END
		
		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpVal1, @tmpVal2, @tmpVal3, @tmpVal4, @tmpVal5
	END
	
	CLOSE @cCursor

--Promotion Participation
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarterNum, COUNT(DISTINCT(redeem_consumer))
		FROM vw_rpt_dashboard_promotion_by_quarter
		WHERE quarterYear = @YYYY
		GROUP BY quarterNum
		ORDER BY quarterNum ASC
		
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpVal1
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
			SET @PromotionQ1 = @tmpVal1
		ELSE IF (@tmpQuarterNum = 2)
			SET @PromotionQ2 = @tmpVal1
		ELSE IF (@tmpQuarterNum = 3)
			SET @PromotionQ3 = @tmpVal1
		ELSE IF (@tmpQuarterNum = 4)
			SET @PromotionQ4 = @tmpVal1
		
		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpVal1
	END
	
	CLOSE @cCursor

	--4A Response/Updates
	Begin
		set @4AResponsQ1=0
		set @4AResponsQ2=0
		set @4AResponsQ3=0
		set @4AResponsQ4=0
	End

	SELECT
	 @newQ1 AS newQ1, @newQ2 AS newQ2, @newQ3 AS newQ3, @newQ4 AS newQ4, 
	 @subscribeQ1 AS subscribeQ1, @subscribeQ2 AS subscribeQ2, @subscribeQ3 AS subscribeQ3, @subscribeQ4 AS subscribeQ4, 
	 @4AResponsQ1 AS "4AResponsQ1", @4AResponsQ2 AS "4AResponsQ2", @4AResponsQ3 AS "4AResponsQ3", @4AResponsQ4 AS "4AResponsQ4", 
	 @PromotionQ1 AS PromotionQ1, @PromotionQ2 AS PromotionQ2, @PromotionQ3 AS PromotionQ3, @PromotionQ4 AS PromotionQ4, 
	 @UnEmailQ1 AS UnEmailQ1, @UnEmailQ2 AS UnEmailQ2, @UnEmailQ3 AS UnEmailQ3, @UnEmailQ4 AS UnEmailQ4, 
	 @InvalidEmailQ1 AS InvalidEmailQ1, @InvalidEmailQ2 AS InvalidEmailQ2, @InvalidEmailQ3 AS InvalidEmailQ3, @InvalidEmailQ4 AS InvalidEmailQ4, 
	 @InvalidMobileQ1 AS InvalidMobileQ1, @InvalidMobileQ2 AS InvalidMobileQ2, @InvalidMobileQ3 AS InvalidMobileQ3, @InvalidMobileQ4 AS InvalidMobileQ4

END
