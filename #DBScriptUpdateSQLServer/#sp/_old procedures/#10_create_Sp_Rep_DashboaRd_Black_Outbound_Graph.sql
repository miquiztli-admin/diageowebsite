USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_DashboaRd_Black_Outbound_Graph]    Script Date: 10/01/2013 16:35:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_DashboaRd_Black_Outbound_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_DashboaRd_Black_Outbound_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <1/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_DashboaRd_Black_Outbound_Graph](@YYYY as integer)
AS
BEGIN

	SELECT tmp.period AS Period, 
	ISNULL(rpt.EDM, 0) AS EDM,
	ISNULL(rpt.SMS, 0) AS SMS
	FROM tbl_m_rpt_dashb_templ_graph tmp
		LEFT OUTER JOIN (
			SELECT quarterNum AS tmp_num,
			COUNT(DISTINCT(
				CASE outboundTypeID
					WHEN 1 THEN consumerId
					ELSE NULL
				END
			)) AS EDM,
			COUNT(DISTINCT(
				CASE outboundTypeID
					WHEN 2 THEN consumerId
					ELSE NULL
				END
			)) AS SMS
			FROM vw_rpt_outbound_consumer_by_quarter
			WHERE quarterYear = @YYYY
			AND (brandPreJwBlack = 'T' OR survey4aJwBlack IS NOT NULL)
			AND outboundTypeID IN (1, 2)
			GROUP BY quarterNum
			
			UNION ALL
			SELECT 5 AS tmp_num,
			COUNT(DISTINCT(
				CASE outboundTypeID
					WHEN 1 THEN consumerId
					ELSE NULL
				END
			)) AS EDM,
			COUNT(DISTINCT(
				CASE outboundTypeID
					WHEN 2 THEN consumerId
					ELSE NULL
				END
			)) AS SMS
			FROM vw_rpt_outbound_consumer_by_quarter
			WHERE quarterYear = @YYYY
			AND (brandPreJwBlack = 'T' OR survey4aJwBlack IS NOT NULL)
			AND outboundTypeID IN (1, 2)
			
			UNION ALL
			SELECT 6 AS tmp_num, 0 AS EDM, 0 AS SMS
			
		) rpt ON tmp.num = rpt.tmp_num 


END
