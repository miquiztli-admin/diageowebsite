USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_Black_DB]    Script Date: 09/28/2013 08:51:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_Black_DB', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_Black_DB;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_Black_DB](@YYYY as integer)
AS
BEGIN

	Declare @DBVolumeBYear bigint,@DBQ1 bigint,@DBQ2 bigint,@DBQ3 bigint,@DBQ4 bigint,@DBYTD bigint
	Declare @ContactableBYear bigint, @ContactableQ1 bigint,@ContactableQ2 bigint,@ContactableQ3 bigint,@ContactableQ4 bigint,@ContactableYTD bigint
	Declare @EmailBY bigint, @EmailQ1 bigint,@EmailQ2 bigint,@EmailQ3 bigint,@EmailQ4 bigint,@EmailYTD bigint
	Declare @MobileBY bigint,@MobileQ1 bigint,@MobileQ2 bigint,@MobileQ3 bigint,@MobileQ4 bigint,@MobileYTD bigint
	Declare @EmailMobileBY bigint, @EmailMobileQ1 bigint,@EmailMobileQ2 bigint,@EmailMobileQ3 bigint,@EmailMobileQ4 bigint,@EmailMobileYTD bigint
	Declare @AdorerBY bigint, @AdorerQ1 bigint,@AdorerQ2 bigint,@AdorerQ3 bigint, @AdorerQ4 bigint,@AdorerYTD bigint
	Declare @AdopterBY bigint, @AdopterQ1 bigint,@AdopterQ2 bigint,@AdopterQ3 bigint, @AdopterQ4 bigint,@AdopterYTD bigint
	Declare @AvailableBY bigint,@AvailableQ1 bigint,@AvailableQ2 bigint,@AvailableQ3 bigint, @AvailableQ4 bigint,@AvailableYTD bigint
	Declare @AcceptBY bigint,@AcceptQ1 bigint,@AcceptQ2 bigint,@AcceptQ3 bigint, @AcceptQ4 bigint,@AcceptYTD bigint
	Declare @RejectBY bigint,@RejectQ1 bigint,@RejectQ2 bigint,@RejectQ3 bigint, @RejectQ4 bigint,@RejectYTD bigint
	Declare @NABY bigint,@NAQ1 bigint,@NAQ2 bigint,@NAQ3 bigint, @NAQ4 bigint,@NAYTD bigint
	Declare @HVCContactableBY bigint,@HVCContactableQ1 bigint,@HVCContactableQ2 bigint,@HVCContactableQ3 bigint,@HVCContactableQ4 bigint,@HVCContactableYTD bigint

	DECLARE @cCursor CURSOR
	DECLARE @tmpQuarterNum int,@tmpDBVol bigint,@tmpContactable bigint,@tmpMobileOnly bigint,@tmpEmailOnly bigint,@tmpEmailMobile bigint
	DECLARE @tmpAdorer bigint, @tmpAdopter bigint, @tmpAvailable bigint, @tmpAccept bigint, @tmpReject bigint, @tmpNA bigint
	DECLARE @tmpFilter int, @tmpValue bigint
	
	SELECT @DBVolumeBYear = COUNT(DISTINCT(consumerId)), 
	@ContactableBYear = COUNT(DISTINCT(contactable)), 
	@MobileBY = COUNT(DISTINCT(mobileOnly)), 
	@EmailBY = COUNT(DISTINCT(emailOnly)), 
	@EmailMobileBY = COUNT(DISTINCT(mobileEmail))
	FROM vw_rpt_consumer_by_quarter
	WHERE quarterYear = @YYYY - 1
	AND (brandPreJwBlack ='T' OR (survey4aJwBlack IS NOT NULL /*AND len(survey4aJwBlack) > 0*/))
		
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarterNum, COUNT(DISTINCT(consumerId)), COUNT(DISTINCT(contactable)), 
		COUNT(DISTINCT(mobileOnly)), COUNT(DISTINCT(emailOnly)), COUNT(DISTINCT(mobileEmail))
		FROM vw_rpt_consumer_by_quarter
		WHERE quarterYear = @YYYY
		AND (brandPreJwBlack ='T' OR (survey4aJwBlack IS NOT NULL /*AND len(survey4aJwBlack) > 0*/))
		GROUP BY quarterNum
		ORDER BY quarterNum ASC
		
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpDBVol, @tmpContactable, @tmpMobileOnly, @tmpEmailOnly, @tmpEmailMobile
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
		BEGIN
			SET @DBQ1 = @tmpDBVol
			SET @ContactableQ1 = @tmpContactable
			SET @EmailQ1 = @tmpEmailOnly
			SET @MobileQ1 = @tmpMobileOnly
			SET @EmailMobileQ1 = @tmpEmailMobile
		END
		ELSE IF (@tmpQuarterNum = 2)
		BEGIN
			SET @DBQ2 = @tmpDBVol
			SET @ContactableQ2 = @tmpContactable
			SET @EmailQ2 = @tmpEmailOnly
			SET @MobileQ2 = @tmpMobileOnly
			SET @EmailMobileQ2 = @tmpEmailMobile
		END
		ELSE IF (@tmpQuarterNum = 3)
		BEGIN
			SET @DBQ3 = @tmpDBVol
			SET @ContactableQ3 = @tmpContactable
			SET @EmailQ3 = @tmpEmailOnly
			SET @MobileQ3 = @tmpMobileOnly
			SET @EmailMobileQ3 = @tmpEmailMobile
		END
		ELSE IF (@tmpQuarterNum = 4)
		BEGIN
			SET @DBQ4 = @tmpDBVol
			SET @ContactableQ4 = @tmpContactable
			SET @EmailQ4 = @tmpEmailOnly
			SET @MobileQ4 = @tmpMobileOnly
			SET @EmailMobileQ4 = @tmpEmailMobile
		END

		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpDBVol, @tmpContactable, @tmpMobileOnly, @tmpEmailOnly, @tmpEmailMobile
	END

	CLOSE @cCursor

	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT survey4aJwBlack, COUNT(DISTINCT(consumerId))
		FROM vw_rpt_consumer_by_quarter
		WHERE quarterYear = @YYYY - 1
		AND survey4aJwBlack IS NOT NULL 
		--AND len(survey4aJwBlack) > 0
		/*++ ITOS condition */
		/*and (a.activeCallCenter='T' or a.activeEmail='T' or a.activeMobile='T') 
		and (a.blockCallCenter='F' or a.blockEmail='F' or a.blockMobile='F')*/
		/*-- ITOS condition */
		GROUP BY survey4aJwBlack
		ORDER BY survey4aJwBlack ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = 1
			SET @AdorerBY = @tmpValue
		ELSE IF @tmpFilter = 2
			SET @AdopterBY = @tmpValue
		ELSE IF @tmpFilter = 3
			SET @AvailableBY = @tmpValue
		ELSE IF @tmpFilter = 4
			SET @AcceptBY = @tmpValue
		ELSE IF @tmpFilter = 5
			SET @RejectBY = @tmpValue
		ELSE -- Case have more than 6 type
			SET @NABY = ISNULL(@NABY, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	
	CLOSE @cCursor
	
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarterNum, survey4aJwBlack, COUNT(DISTINCT(consumerId))
		FROM vw_rpt_consumer_by_quarter
		WHERE quarterYear = @YYYY
		AND survey4aJwBlack IS NOT NULL 
		--AND len(survey4aJwBlack) > 0
		/*++ ITOS condition */
		/*and (a.activeCallCenter='T' or a.activeEmail='T' or a.activeMobile='T') 
		and (a.blockCallCenter='F' or a.blockEmail='F' or a.blockMobile='F')*/
		/*-- ITOS condition */
		GROUP BY quarterNum, survey4aJwBlack
		ORDER BY quarterNum, survey4aJwBlack ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
		BEGIN
			IF @tmpFilter = 1
				SET @AdorerQ1 = @tmpValue
			ELSE IF @tmpFilter = 2
				SET @AdopterQ1 = @tmpValue
			ELSE IF @tmpFilter = 3
				SET @AvailableQ1 = @tmpValue
			ELSE IF @tmpFilter = 4
				SET @AcceptQ1 = @tmpValue
			ELSE IF @tmpFilter = 5
				SET @RejectQ1 = @tmpValue
			ELSE -- Case have more than 6 type
				SET @NAQ1 = ISNULL(@NAQ1, 0) + @tmpValue
		END
		ELSE IF (@tmpQuarterNum = 2)
		BEGIN
			IF @tmpFilter = 1
				SET @AdorerQ2 = @tmpValue
			ELSE IF @tmpFilter = 2
				SET @AdopterQ2 = @tmpValue
			ELSE IF @tmpFilter = 3
				SET @AvailableQ2 = @tmpValue
			ELSE IF @tmpFilter = 4
				SET @AcceptQ2 = @tmpValue
			ELSE IF @tmpFilter = 5
				SET @RejectQ2 = @tmpValue
			ELSE -- Case have more than 6 type
				SET @NAQ2 = ISNULL(@NAQ2, 0) + @tmpValue
		END
		ELSE IF (@tmpQuarterNum = 3)
		BEGIN
			IF @tmpFilter = 1
				SET @AdorerQ3 = @tmpValue
			ELSE IF @tmpFilter = 2
				SET @AdopterQ3 = @tmpValue
			ELSE IF @tmpFilter = 3
				SET @AvailableQ3 = @tmpValue
			ELSE IF @tmpFilter = 4
				SET @AcceptQ3 = @tmpValue
			ELSE IF @tmpFilter = 5
				SET @RejectQ3 = @tmpValue
			ELSE -- Case have more than 6 type
				SET @NAQ3 = ISNULL(@NAQ3, 0) + @tmpValue
		END
		ELSE IF (@tmpQuarterNum = 4)
		BEGIN
			IF @tmpFilter = 1
				SET @AdorerQ4 = @tmpValue
			ELSE IF @tmpFilter = 2
				SET @AdopterQ4 = @tmpValue
			ELSE IF @tmpFilter = 3
				SET @AvailableQ4 = @tmpValue
			ELSE IF @tmpFilter = 4
				SET @AcceptQ4 = @tmpValue
			ELSE IF @tmpFilter = 5
				SET @RejectQ4 = @tmpValue
			ELSE -- Case have more than 6 type
				SET @NAQ4 = ISNULL(@NAQ4, 0) + @tmpValue
		END

		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpFilter, @tmpValue
	END
	
	CLOSE @cCursor
/*
	SET @ContactableQ2 = @ContactableQ1 + @ContactableQ2
	SET @ContactableQ3 = @ContactableQ2 + @ContactableQ3
	SET @ContactableQ4 = @ContactableQ3 + @ContactableQ4

	SET @EmailQ2 = @EmailQ1 + @EmailQ2
	SET @EmailQ3 = @EmailQ2 + @EmailQ3
	SET @EmailQ4 = @EmailQ3 + @EmailQ4

	SET @MobileQ2 = @MobileQ1 + @MobileQ2
	SET @MobileQ3 = @MobileQ2 + @MobileQ3
	SET @MobileQ4 = @MobileQ3 + @MobileQ4

	SET @EmailMobileQ2 = @EmailMobileQ1 + @EmailMobileQ2
	SET @EmailMobileQ3 = @EmailMobileQ2 + @EmailMobileQ3
	SET @EmailMobileQ4 = @EmailMobileQ3 + @EmailMobileQ4

	SET @AdorerQ2 = @AdorerQ1 + @AdorerQ2
	SET @AdorerQ3 = @AdorerQ2 + @AdorerQ3
	SET @AdorerQ4 = @AdorerQ3 + @AdorerQ4

	SET @AdopterQ2 = @AdopterQ1 + @AdopterQ2
	SET @AdopterQ3 = @AdopterQ2 + @AdopterQ3
	SET @AdopterQ4 = @AdopterQ3 + @AdopterQ4

	SET @AvailableQ2 = @AvailableQ1 + @AvailableQ2
	SET @AvailableQ3 = @AvailableQ2 + @AvailableQ3
	SET @AvailableQ4 = @AvailableQ3 + @AvailableQ4

	SET @AcceptQ2 = @AcceptQ1 + @AcceptQ2
	SET @AcceptQ3 = @AcceptQ2 + @AcceptQ3
	SET @AcceptQ4 = @AcceptQ3 + @AcceptQ4

	SET @RejectQ2 = @RejectQ1 + @RejectQ2
	SET @RejectQ3 = @RejectQ2 + @RejectQ3
	SET @RejectQ4 = @RejectQ3 + @RejectQ4

	SET @NAQ2 = @NAQ1 + @NAQ2
	SET @NAQ3 = @NAQ2 + @NAQ3
	SET @NAQ4 = @NAQ3 + @NAQ4
*/
	SET @DBYTD = ISNULL(@DBQ1, 0) + ISNULL(@DBQ2, 0) + ISNULL(@DBQ3, 0) + ISNULL(@DBQ4, 0)
	SET @ContactableYTD = ISNULL(@ContactableQ1, 0) + ISNULL(@ContactableQ2, 0) + ISNULL(@ContactableQ3, 0) + ISNULL(@ContactableQ4, 0)	
	SET @EmailYTD = ISNULL(@EmailQ1, 0) + ISNULL(@EmailQ2, 0) + ISNULL(@EmailQ3, 0) + ISNULL(@EmailQ4, 0)
	SET @MobileYTD = ISNULL(@MobileQ1, 0) + ISNULL(@MobileQ2, 0) + ISNULL(@MobileQ3, 0) + ISNULL(@MobileQ4, 0)
	SET @EmailMobileYTD = ISNULL(@EmailMobileQ1, 0) + ISNULL(@EmailMobileQ2, 0) + ISNULL(@EmailMobileQ3, 0) + ISNULL(@EmailMobileQ4, 0)	
	SET @AdorerYTD = ISNULL(@AdorerQ1, 0) + ISNULL(@AdorerQ2, 0) + ISNULL(@AdorerQ3, 0) + ISNULL(@AdorerQ4, 0)
	SET @AdopterYTD = ISNULL(@AdopterQ1, 0) + ISNULL(@AdopterQ2, 0) + ISNULL(@AdopterQ3, 0) + ISNULL(@AdopterQ4, 0)
	SET @AvailableYTD = ISNULL(@AvailableQ1, 0) + ISNULL(@AvailableQ2, 0) + ISNULL(@AvailableQ3, 0) + ISNULL(@AvailableQ4, 0)
	SET @AcceptYTD = ISNULL(@AcceptQ1, 0) + ISNULL(@AcceptQ2, 0) + ISNULL(@AcceptQ3, 0) + ISNULL(@AcceptQ4, 0)
	SET @RejectYTD = ISNULL(@RejectQ1, 0) + ISNULL(@RejectQ2, 0) + ISNULL(@RejectQ3, 0) + ISNULL(@RejectQ4, 0)
	SET @NAYTD = ISNULL(@NAQ1, 0) + ISNULL(@NAQ2, 0) + ISNULL(@NAQ3, 0) + ISNULL(@NAQ4, 0)

	--HVCContactable
	set @HVCContactableQ1 = ISNULL(@AdorerQ1, 0)+ ISNULL(@AdopterQ1, 0)
	set @HVCContactableQ2 = ISNULL(@AdorerQ2, 0)+ ISNULL(@AdopterQ2, 0)
	set @HVCContactableQ3 = ISNULL(@AdorerQ3, 0)+ ISNULL(@AdopterQ3, 0)
	set @HVCContactableQ4 = ISNULL(@AdorerQ4, 0)+ ISNULL(@AdopterQ4, 0)
	set @HVCContactableBY = ISNULL(@HVCContactableQ1, 0)+ ISNULL(@HVCContactableQ2, 0)+ ISNULL(@HVCContactableQ3, 0)+ ISNULL(@HVCContactableQ4, 0)
	set @HVCContactableYTD = ISNULL(@AdorerYTD, 0) + ISNULL(@AdopterYTD, 0)

	SELECT @DBVolumeBYear AS DBVolumeBYear,@DBQ1 AS DBQ1,@DBQ2 AS DBQ2,@DBQ3 AS DBQ3,@DBQ4 AS DBQ4,@DBYTD AS DBYTD,
	 @ContactableBYear AS ContactableBYear,@ContactableQ1 AS ContactableQ1,@ContactableQ2 AS ContactableQ2,@ContactableQ3 AS ContactableQ3,@ContactableQ4 AS ContactableQ4,@ContactableYTD AS ContactableYTD,
	 @EmailBY AS EmailBY,@EmailQ1 AS EmailQ1,@EmailQ2 AS EmailQ2,@EmailQ3 AS EmailQ3,@EmailQ4 AS EmailQ4,@EmailYTD AS EmailYTD,
	 @MobileBY AS MobileBY,@MobileQ1 AS MobileQ1,@MobileQ2 AS MobileQ2,@MobileQ3 AS MobileQ3,@MobileQ4 AS MobileQ4,@MobileYTD AS MobileYTD,
	 @EmailMobileBY AS EmailMobileBY,@EmailMobileQ1 AS EmailMobileQ1,@EmailMobileQ2 AS EmailMobileQ2,@EmailMobileQ3 AS EmailMobileQ3,@EmailMobileQ4 AS EmailMobileQ4,@EmailMobileYTD AS EmailMobileYTD,
	 @HVCContactableBY AS HVCContactableBY,@HVCContactableQ1 AS HVCContactableQ1,@HVCContactableQ2 AS HVCContactableQ2,@HVCContactableQ3 AS HVCContactableQ3,@HVCContactableQ4 AS HVCContactableQ4,@HVCContactableYTD AS HVCContactableYTD,
	 @AdorerBY AS AdorerBY,@AdorerQ1 AS AdorerQ1,@AdorerQ2 AS AdorerQ2,@AdorerQ3 AS AdorerQ3,@AdorerQ4 AS AdorerQ4,@AdorerYTD AS AdorerYTD,
	 @AdopterBY AS AdopterBY,@AdopterQ1 AS AdopterQ1,@AdopterQ2 AS AdopterQ2,@AdopterQ3 AS AdopterQ3,@AdopterQ4 AS AdopterQ4,@AdopterYTD AS AdopterYTD,
	 @AvailableBY AS AvailableBY,@AvailableQ1 AS AvailableQ1,@AvailableQ2 AS AvailableQ2,@AvailableQ3 AS AvailableQ3,@AvailableQ4 AS AvailableQ4,@AvailableYTD AS AvailableYTD,
	 @AcceptBY AS AcceptBY,@AcceptQ1 AS AcceptQ1,@AcceptQ2 AS AcceptQ2,@AcceptQ3 AS AcceptQ3,@AcceptQ4 AS AcceptQ4,@AcceptYTD AS AcceptYTD,
	 @RejectBY AS RejectBY,@RejectQ1 AS RejectQ1,@RejectQ2 AS RejectQ2,@RejectQ3 AS RejectQ3,@RejectQ4 AS RejectQ4,@RejectYTD AS RejectYTD,
	 @NABY AS NABY,@NAQ1 AS NAQ1,@NAQ2 AS NAQ2,@NAQ3 AS NAQ3,@NAQ4 AS NAQ4,@NAYTD AS NAYTD

END
