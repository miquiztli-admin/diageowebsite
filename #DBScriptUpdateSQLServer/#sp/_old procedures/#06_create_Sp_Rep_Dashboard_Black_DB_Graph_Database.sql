USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_Black_DB_Graph_Database]    Script Date: 10/01/2013 12:45:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_Black_DB_Graph_Database', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_Black_DB_Graph_Database;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_Black_DB_Graph_Database](@YYYY as integer)
AS
BEGIN

	SELECT tmp.period, 
	ISNULL(rpt.DatabaseVolume, 0) AS DatabaseVolume,
	ISNULL(rpt.ContactableMass, 0) AS ContactableMass,
	ISNULL(rpt.HVCContactable, 0) AS HVCContactable
	FROM tbl_m_rpt_dashb_templ_graph tmp
		LEFT OUTER JOIN (
			SELECT quarterNum AS tmp_num,
			'Q' + CAST(quarterNum AS VARCHAR(1)) AS period, 
			COUNT(DISTINCT(consumerID)) AS DatabaseVolume, 
			COUNT(DISTINCT(contactable)) AS ContactableMass, 
			COUNT(DISTINCT(
				CASE survey4aJwBlack
					WHEN 1 THEN consumerID --Adorer
					WHEN 2 THEN consumerID --Adopter
					ELSE NULL
				END
			)) AS HVCContactable
			FROM vw_rpt_consumer_by_quarter
			WHERE quarterYear = @YYYY
			AND (brandPreJwBlack ='T' OR (survey4aJwBlack IS NOT NULL /*AND len(survey4aJwBlack) > 0*/))
			GROUP BY quarterNum
			
			UNION ALL
			SELECT 5 AS tmp_num,
			'YTD' as period,
			COUNT(DISTINCT(consumerID)) AS DatabaseVolume, 
			COUNT(DISTINCT(contactable)) AS ContactableMass, 
			COUNT(DISTINCT(
				CASE survey4aJwBlack
					WHEN 1 THEN consumerID --Adorer
					WHEN 2 THEN consumerID --Adopter
					ELSE NULL
				END
			)) AS HVCContactable
			FROM vw_rpt_consumer_by_quarter
			WHERE quarterYear = @YYYY
			AND (brandPreJwBlack ='T' OR (survey4aJwBlack IS NOT NULL /*AND len(survey4aJwBlack) > 0*/))
			
			UNION ALL
			SELECT 6 AS tmp_num, 'Target' as period, 0 as DatabaseVolume, 0 as ContactableMass, 0 as HVCContactable
		) rpt ON tmp.num = rpt.tmp_num 
	
END
