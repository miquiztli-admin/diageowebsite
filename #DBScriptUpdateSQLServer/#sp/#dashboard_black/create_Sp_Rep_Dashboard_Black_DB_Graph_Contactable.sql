USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_Black_DB_Graph_Contactable]    Script Date: 09/29/2013 17:23:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_Black_DB_Graph_Contactable', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_Black_DB_Graph_Contactable;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Update date: <13/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_Black_DB_Graph_Contactable](@YYYY as integer)
AS
BEGIN

	Declare @EmailBY bigint = 0, @EmailQ1 bigint = 0,@EmailQ2 bigint = 0,@EmailQ3 bigint = 0,@EmailQ4 bigint = 0,@EmailYTD bigint = 0,@EmailTarget bigint = 0
	Declare @MobileBY bigint = 0,@MobileQ1 bigint = 0,@MobileQ2 bigint = 0,@MobileQ3 bigint = 0,@MobileQ4 bigint = 0,@MobileYTD bigint = 0,@MobileTarget bigint = 0
	Declare @EmailMobileBY bigint = 0, @EmailMobileQ1 bigint = 0,@EmailMobileQ2 bigint = 0,@EmailMobileQ3 bigint = 0,@EmailMobileQ4 bigint = 0,@EmailMobileYTD bigint = 0,@EmailMobileTarget bigint = 0
	DECLARE @cCursor CURSOR
	DECLARE @tmpQuarterNum int,@tmpMobileOnly bigint,@tmpEmailOnly bigint,@tmpEmailMobile bigint
	DECLARE @tmpFilter int, @tmpValue bigint
	
	DECLARE @ResultTable TABLE (period varchar(20), Email bigint, Mobile bigint, Both bigint)

	DECLARE @_Q1 INT, @_Q2 INT, @_Q3 INT, @_Q4 INT
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarter,
		CASE
			WHEN MIN(DATEADD(MONTH, month_num - 1, CONVERT(DATE, '01-01-' + CAST(@YYYY + year_shift AS CHAR(4))))) > GETDATE() THEN 0
			ELSE 1
		END
		FROM tbl_m_rpt_quarter_month t
		GROUP BY t.quarter
	
	OPEN @cCursor
	FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpFilter = 1)
			SET @_Q1 = @tmpValue
		ELSE IF (@tmpFilter = 2)
			SET @_Q2 = @tmpValue
		ELSE IF (@tmpFilter = 3)
			SET @_Q3 = @tmpValue
		ELSE IF (@tmpFilter = 4)
			SET @_Q4 = @tmpValue
		
		FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue
	END
	CLOSE @cCursor
	
	SELECT 
	@MobileBY = ISNULL(COUNT(DISTINCT(mobileOnly)), 0),
	@EmailBY = ISNULL(COUNT(DISTINCT(emailOnly)), 0),
	@EmailMobileBY = ISNULL(COUNT(DISTINCT(both)), 0)
	FROM vw_rpt_dashboardConsumer_by_quarter
	WHERE upd_quarterYear < @YYYY
	AND no_all IS NULL
	AND (brandPreJwBlack ='T') -- OR (survey4aJwBlack IS NOT NULL AND len(survey4aJwBlack) > 0)
		
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT upd_quarterNum, 
		ISNULL(COUNT(DISTINCT(mobileOnly)), 0), 
		ISNULL(COUNT(DISTINCT(emailOnly)), 0), 
		ISNULL(COUNT(DISTINCT(both)), 0)
		FROM vw_rpt_dashboardConsumer_by_quarter
		WHERE upd_quarterYear = @YYYY
		AND no_all IS NULL
		AND (brandPreJwBlack ='T') -- OR (survey4aJwBlack IS NOT NULL /*AND len(survey4aJwBlack) > 0*/))
		GROUP BY upd_quarterNum
		ORDER BY upd_quarterNum ASC
		
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpMobileOnly, @tmpEmailOnly, @tmpEmailMobile
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
		BEGIN
			SET @EmailQ1 = @tmpEmailOnly
			SET @MobileQ1 = @tmpMobileOnly
			SET @EmailMobileQ1 = @tmpEmailMobile
		END
		ELSE IF (@tmpQuarterNum = 2)
		BEGIN
			SET @EmailQ2 = @tmpEmailOnly
			SET @MobileQ2 = @tmpMobileOnly
			SET @EmailMobileQ2 = @tmpEmailMobile
		END
		ELSE IF (@tmpQuarterNum = 3)
		BEGIN
			SET @EmailQ3 = @tmpEmailOnly
			SET @MobileQ3 = @tmpMobileOnly
			SET @EmailMobileQ3 = @tmpEmailMobile
		END
		ELSE IF (@tmpQuarterNum = 4)
		BEGIN
			SET @EmailQ4 = @tmpEmailOnly
			SET @MobileQ4 = @tmpMobileOnly
			SET @EmailMobileQ4 = @tmpEmailMobile
		END

		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpMobileOnly, @tmpEmailOnly, @tmpEmailMobile
	END
	CLOSE @cCursor

	SET @EmailQ1 = ISNULL(@EmailBY, 0) + ISNULL(@EmailQ1, 0)
	SET @EmailQ2 = ISNULL(@EmailQ1, 0) + ISNULL(@EmailQ2, 0)
	SET @EmailQ3 = ISNULL(@EmailQ2, 0) + ISNULL(@EmailQ3, 0)
	SET @EmailQ4 = ISNULL(@EmailQ3, 0) + ISNULL(@EmailQ4, 0)

	SET @MobileQ1 = ISNULL(@MobileBY, 0) + ISNULL(@MobileQ1, 0)
	SET @MobileQ2 = ISNULL(@MobileQ1, 0) + ISNULL(@MobileQ2, 0)
	SET @MobileQ3 = ISNULL(@MobileQ2, 0) + ISNULL(@MobileQ3, 0)
	SET @MobileQ4 = ISNULL(@MobileQ3, 0) + ISNULL(@MobileQ4, 0)

	SET @EmailMobileQ1 = ISNULL(@EmailMobileBY, 0) + ISNULL(@EmailMobileQ1, 0)
	SET @EmailMobileQ2 = ISNULL(@EmailMobileQ1, 0) + ISNULL(@EmailMobileQ2, 0)
	SET @EmailMobileQ3 = ISNULL(@EmailMobileQ2, 0) + ISNULL(@EmailMobileQ3, 0)
	SET @EmailMobileQ4 = ISNULL(@EmailMobileQ3, 0) + ISNULL(@EmailMobileQ4, 0)

	SET @EmailYTD = ISNULL(@EmailQ4, 0)
	SET @MobileYTD = ISNULL(@MobileQ4, 0)
	SET @EmailMobileYTD = ISNULL(@EmailMobileQ4, 0)	
	
	IF @_Q1 = 1
		INSERT INTO @ResultTable VALUES('Q1', @EmailQ1, @MobileQ1, @EmailMobileQ1)
	ELSE
		INSERT INTO @ResultTable VALUES('Q1', NULL, NULL, NULL)

	IF @_Q2 = 1
		INSERT INTO @ResultTable VALUES('Q2', @EmailQ2, @MobileQ2, @EmailMobileQ2)
	ELSE 
		INSERT INTO @ResultTable VALUES('Q2', NULL, NULL, NULL)

	IF @_Q3 = 1
		INSERT INTO @ResultTable VALUES('Q3', @EmailQ3, @MobileQ3, @EmailMobileQ3)
	ELSE
		INSERT INTO @ResultTable VALUES('Q3', NULL, NULL, NULL)

	IF @_Q4 = 1
		INSERT INTO @ResultTable VALUES('Q4', @EmailQ4, @MobileQ4, @EmailMobileQ4)
	ELSE
		INSERT INTO @ResultTable VALUES('Q4', NULL, NULL, NULL)
	
	INSERT INTO @ResultTable VALUES('YTD', @EmailYTD, @MobileYTD, @EmailMobileYTD)
	INSERT INTO @ResultTable VALUES('Target', @EmailTarget, @MobileTarget, @EmailMobileTarget)

	SELECT * FROM @ResultTable

END
