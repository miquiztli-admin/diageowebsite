USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_DashboaRd_ST_Recruit_Graph]    Script Date: 10/01/2013 23:21:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_DashboaRd_ST_Recruit_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_DashboaRd_ST_Recruit_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_ST_Recruit_Graph]
	@YYYY AS INT
AS
BEGIN

	DECLARE @NewDiageoQ1 BIGINT,@NewDiageoQ2 BIGINT,@NewDiageoQ3 BIGINT,@NewDiageoQ4 BIGINT,@NewDiageoYTD bigint
	DECLARE @UNSUBOLDYEAR BIGINT,@UNSUBQ1 BIGINT,@UNSUBQ2 BIGINT,@UNSUBQ3 BIGINT,@UNSUBQ4 BIGINT,@UNSUBYTD BIGINT
	DECLARE @IEMAILOLDYEAR BIGINT,@IEMAILQ1 BIGINT,@IEMAILQ2 BIGINT,@IEMAILQ3 BIGINT,@IEMAILQ4 BIGINT,@IEMAILYTD BIGINT
	DECLARE @IBMOBILEOLDYEAR BIGINT,@IBMOBILEQ1 BIGINT,@IBMOBILEQ2 BIGINT,@IBMOBILEQ3 BIGINT,@IBMOBILEQ4 BIGINT,@IBMOBILEYTD BIGINT

	DECLARE @cCursor CURSOR
	DECLARE @tmpQuarterNum int, @tmpVal3 bigint, @tmpVal4 bigint, @tmpVal5 bigint

	DECLARE @ResultTable TABLE(period varchar(20),NewDiageoQ2 bigint,UNSUB bigint,IEMAIL bigint,IBMOBILE bigint)

	SELECT 
	@NewDiageoQ1 = ISNULL(COUNT(DISTINCT(CASE WHEN cre_quarterNum = 1 THEN diageoBrandPre ELSE NULL END)), 0),
	@NewDiageoQ2 = ISNULL(COUNT(DISTINCT(CASE WHEN cre_quarterNum = 2 THEN diageoBrandPre ELSE NULL END)), 0),
	@NewDiageoQ3 = ISNULL(COUNT(DISTINCT(CASE WHEN cre_quarterNum = 3 THEN diageoBrandPre ELSE NULL END)), 0),
	@NewDiageoQ4 = ISNULL(COUNT(DISTINCT(CASE WHEN cre_quarterNum = 4 THEN diageoBrandPre ELSE NULL END)), 0)
	FROM vw_rpt_dashb_smart_by_quarter
	WHERE cre_quarterYear = @YYYY
	
	SELECT
	@UNSUBOLDYEAR = ISNULL(COUNT(DISTINCT(unsubscribe_email)), 0),
	@IEMAILOLDYEAR = ISNULL(COUNT(DISTINCT(invalid_email)), 0),
	@IBMOBILEOLDYEAR = ISNULL(COUNT(DISTINCT(invalid_block_mobile)), 0)
	FROM vw_rpt_dashb_smart_by_quarter
	WHERE upd_quarterYear < @YYYY
	AND diageoBrandPre IS NOT NULL
	
	SET @cCursor = CURSOR FAST_FORWARD
	FOR	
		SELECT upd_quarterNum,
		ISNULL(COUNT(DISTINCT(unsubscribe_email)), 0),
		ISNULL(COUNT(DISTINCT(invalid_email)), 0),
		ISNULL(COUNT(DISTINCT(invalid_block_mobile)), 0)
		FROM vw_rpt_dashb_smart_by_quarter
		WHERE upd_quarterYear = @YYYY
		AND diageoBrandPre IS NOT NULL
		GROUP BY upd_quarterNum

	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpVal3, @tmpVal4, @tmpVal5
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
		BEGIN
			SET @UNSUBQ1 = @tmpVal3
			SET @IEMAILQ1 = @tmpVal4
			SET @IBMOBILEQ1 = @tmpVal5
		END
		ELSE IF (@tmpQuarterNum = 2)
		BEGIN
			SET @UNSUBQ2 = @tmpVal3
			SET @IEMAILQ2 = @tmpVal4
			SET @IBMOBILEQ2 = @tmpVal5
		END
		ELSE IF (@tmpQuarterNum = 3)
		BEGIN
			SET @UNSUBQ3 = @tmpVal3
			SET @IEMAILQ3 = @tmpVal4
			SET @IBMOBILEQ3 = @tmpVal5
		END
		ELSE IF (@tmpQuarterNum = 4)
		BEGIN
			SET @UNSUBQ4 = @tmpVal3
			SET @IEMAILQ4 = @tmpVal4
			SET @IBMOBILEQ4 = @tmpVal5
		END
	
		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpVal3, @tmpVal4, @tmpVal5
	END
	CLOSE @cCursor
	
	SET @UNSUBQ1 = ISNULL(@UNSUBOLDYEAR, 0) + ISNULL(@UNSUBQ1, 0)
	SET @UNSUBQ2 = ISNULL(@UNSUBQ1, 0) + ISNULL(@UNSUBQ2, 0)
	SET @UNSUBQ3 = ISNULL(@UNSUBQ2, 0) + ISNULL(@UNSUBQ3, 0)
	SET @UNSUBQ4 = ISNULL(@UNSUBQ3, 0) + ISNULL(@UNSUBQ4, 0)
	
	SET @IEMAILQ1 = ISNULL(@IEMAILOLDYEAR, 0) + ISNULL(@IEMAILQ1, 0)
	SET @IEMAILQ2 = ISNULL(@IEMAILQ1, 0) + ISNULL(@IEMAILQ2, 0)
	SET @IEMAILQ3 = ISNULL(@IEMAILQ2, 0) + ISNULL(@IEMAILQ2, 0)
	SET @IEMAILQ4 = ISNULL(@IEMAILQ3, 0) + ISNULL(@IEMAILQ4, 0)
	
	SET @IBMOBILEQ1 = ISNULL(@IBMOBILEOLDYEAR, 0) + ISNULL(@IBMOBILEQ1, 0)
	SET @IBMOBILEQ2 = ISNULL(@IBMOBILEQ1, 0) + ISNULL(@IBMOBILEQ2, 0)
	SET @IBMOBILEQ3 = ISNULL(@IBMOBILEQ2, 0) + ISNULL(@IBMOBILEQ3, 0)
	SET @IBMOBILEQ4 = ISNULL(@IBMOBILEQ3, 0) + ISNULL(@IBMOBILEQ4, 0)
	
	SET @NewDiageoYTD = ISNULL(@NewDiageoQ1, 0) + ISNULL(@NewDiageoQ2, 0) + ISNULL(@NewDiageoQ3, 0) + ISNULL(@NewDiageoQ4, 0) 
	SET @UNSUBYTD = ISNULL(@UNSUBQ4, 0) 
	SET @IEMAILYTD = ISNULL(@IEMAILQ4, 0) 
	SET @IBMOBILEYTD = ISNULL(@IBMOBILEQ4, 0) 
	
	INSERT INTO @ResultTable
	VALUES('Q1', @NewDiageoQ1, @UNSUBQ1, @IEMAILQ1, @IBMOBILEQ1),
		('Q2', @NewDiageoQ2, @UNSUBQ2, @IEMAILQ2, @IBMOBILEQ2),
		('Q3', @NewDiageoQ3, @UNSUBQ3, @IEMAILQ3, @IBMOBILEQ3),
		('Q4', @NewDiageoQ4, @UNSUBQ4, @IEMAILQ4, @IBMOBILEQ4),
		('YTD', @NewDiageoYTD, @UNSUBYTD, @IEMAILYTD, @IBMOBILEYTD),
		('Target', 0, 0, 0, 0)

	SELECT * FROM @ResultTable
END
