USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_ST_Database_Graph_Contactable]    Script Date: 10/01/2013 22:36:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_ST_Database_Graph_Contactable', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_ST_Database_Graph_Contactable;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_ST_Database_Graph_Contactable](
	@YYYY AS INT
)
AS
BEGIN

	DECLARE @CMEMAILOLDYEAR BIGINT,@CMMOBILEOLDYEAR BIGINT,@CMBothOLDYEAR BIGINT
	DECLARE @CMEMAILYTD BIGINT,@CMMOBILEYTD BIGINT,@CMBothYTD BIGINT
	
	SELECT 
	@CMEMAILOLDYEAR = ISNULL(COUNT(DISTINCT(emailOnly)), 0), 
	@CMMOBILEOLDYEAR = ISNULL(COUNT(DISTINCT(mobileOnly)), 0), 
	@CMBothOLDYEAR = ISNULL(COUNT(DISTINCT(both)), 0)
	FROM vw_rpt_dashb_smart_by_quarter
	WHERE upd_quarterYear < @YYYY
	AND diageoBrandPre IS NOT NULL
		
	SELECT 
	@CMEMAILYTD = ISNULL(@CMEMAILOLDYEAR, 0) + ISNULL(COUNT(DISTINCT(emailOnly)), 0), 
	@CMMOBILEYTD = ISNULL(@CMMOBILEOLDYEAR, 0) + ISNULL(COUNT(DISTINCT(mobileOnly)), 0), 
	@CMBothYTD = ISNULL(@CMBothOLDYEAR, 0) + ISNULL(COUNT(DISTINCT(both)), 0)
	FROM vw_rpt_dashb_smart_by_quarter
	WHERE upd_quarterYear = @YYYY
	AND diageoBrandPre IS NOT NULL
	AND no_all IS NULL
	
	SELECT 'Email Only' AS Name, @CMEMAILYTD as Summary
	UNION
	SELECT 'Mobile Only' AS Name, @CMMOBILEYTD as Summary
	UNION
	SELECT 'Both' AS Name, @CMBothYTD as Summary
	
END
