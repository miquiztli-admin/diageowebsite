USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_DashboaRd_ST_Recruit_Graph]    Script Date: 10/01/2013 23:21:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_DashboaRd_ST_Recruit_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_DashboaRd_ST_Recruit_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_DashboaRd_ST_Recruit_Graph]
	@YYYY AS INT
AS
BEGIN

	SELECT tmp.period,
	ISNULL(rpt.NewDiageoQ2, 0) AS NewDiageoQ2,
	ISNULL(rpt.UNSUB, 0) AS UNSUB,
	ISNULL(rpt.IEMAIL, 0) AS IEMAIL,
	ISNULL(rpt.IBMOBILE, 0) AS IBMOBILE
	FROM tbl_m_rpt_dashb_templ_graph tmp
		LEFT OUTER JOIN (
			SELECT quarterNum AS tmp_num,
			COUNT(DISTINCT(
				CASE 
					WHEN (brandPreBenmore IS NOT NULL OR brandPreHennessy IS NOT NULL 
						OR brandPreJwBlack IS NOT NULL OR brandPreJwGold IS NOT NULL 
						OR brandPreJwGreen IS NOT NULL OR brandPreJwRed IS NOT NULL 
						OR brandPreSmirnoff IS NOT NULL ) THEN consumerId
					ELSE NULL
				END
			)) AS NewDiageoQ2,
			COUNT(DISTINCT(unsubscribe_email)) AS UNSUB,
			COUNT(DISTINCT(invalid_email)) AS IEMAIL,
			COUNT(DISTINCT(invalid_block_mobile)) AS IBMOBILE
			FROM vw_rpt_recruit_by_quarter
			WHERE quarterYear = @YYYY
			GROUP BY quarterNum	

			UNION ALL
			SELECT 5 AS tmp_num,
			COUNT(DISTINCT(
				CASE 
					WHEN (brandPreBenmore IS NOT NULL OR brandPreHennessy IS NOT NULL 
						OR brandPreJwBlack IS NOT NULL OR brandPreJwGold IS NOT NULL 
						OR brandPreJwGreen IS NOT NULL OR brandPreJwRed IS NOT NULL 
						OR brandPreSmirnoff IS NOT NULL ) THEN consumerId
					ELSE NULL
				END
			)) AS NewDiageoQ2,
			COUNT(DISTINCT(unsubscribe_email)) AS UNSUB,
			COUNT(DISTINCT(invalid_email)) AS IEMAIL,
			COUNT(DISTINCT(invalid_block_mobile)) AS IBMOBILE
			FROM vw_rpt_recruit_by_quarter
			WHERE quarterYear = @YYYY
			
			UNION ALL
			SELECT 6 AS tmp_num, 0 AS NewDiageoQ2,
			0 AS UNSUB,	0 AS IEMAIL, 0 AS IBMOBILE

		) rpt ON tmp.num = rpt.tmp_num 
END
