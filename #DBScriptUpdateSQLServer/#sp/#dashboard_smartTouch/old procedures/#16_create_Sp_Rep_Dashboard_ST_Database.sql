USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_ST_Database]    Script Date: 10/01/2013 22:11:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'Sp_Rep_Dashboard_ST_Database', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_ST_Database;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_ST_Database]
	@YYYY AS INT
AS
BEGIN

	DECLARE @DVOLDYEAR BIGINT,@DVQ1 BIGINT,@DVQ2 BIGINT,@DVQ3 BIGINT,@DVQ4 BIGINT,@DVYTD BIGINT
	DECLARE @CMOLDYEAR BIGINT,@CMQ1 BIGINT,@CMQ2 BIGINT,@CMQ3 BIGINT,@CMQ4 BIGINT,@CMYTD BIGINT
	DECLARE @CMEMAILOLDYEAR BIGINT,@CMEMAILQ1 BIGINT,@CMEMAILQ2 BIGINT,@CMEMAILQ3 BIGINT,@CMEMAILQ4 BIGINT,@CMEMAILYTD BIGINT
	DECLARE @CMMOBILEOLDYEAR BIGINT,@CMMOBILEQ1 BIGINT,@CMMOBILEQ2 BIGINT,@CMMOBILEQ3 BIGINT,@CMMOBILEQ4 BIGINT,@CMMOBILEYTD BIGINT
	DECLARE @CMBothOLDYEAR BIGINT,@CMBothQ1 BIGINT,@CMBothQ2 BIGINT,@CMBothQ3 BIGINT,@CMBothQ4 BIGINT,@CMBothYTD BIGINT

		
	--Create Temp Table
	BEGIN
	
	CREATE TABLE #ResultTable  ( DVOLDYEAR BIGINT,DVQ1 BIGINT,DVQ2 BIGINT,DVQ3 BIGINT,DVQ4 BIGINT,DVYTD BIGINT,
								 CMOLDYEAR BIGINT,CMQ1 BIGINT,CMQ2 BIGINT,CMQ3 BIGINT,CMQ4 BIGINT,CMYTD BIGINT,
								 CMEMAILOLDYEAR BIGINT,CMEMAILQ1 BIGINT,CMEMAILQ2 BIGINT,CMEMAILQ3 BIGINT,CMEMAILQ4 BIGINT,CMEMAILYTD BIGINT,
								 CMMOBILEOLDYEAR BIGINT,CMMOBILEQ1 BIGINT,CMMOBILEQ2 BIGINT,CMMOBILEQ3 BIGINT,CMMOBILEQ4 BIGINT,CMMOBILEYTD BIGINT,
								 CMBothOldYear bigint,CMBothQ1 bigint, CMBothQ2 bigint, CMBothQ3 bigint, CMBothQ4 bigint,CMBothYTD bigint
							   )
	END

	DECLARE @cCursor CURSOR
	DECLARE @tmpQuarterNum int,@tmpDBVol bigint,@tmpContactable bigint,@tmpMobileOnly bigint,@tmpEmailOnly bigint,@tmpEmailMobile bigint
	DECLARE @tmpFilter int, @tmpValue bigint
	
	SELECT @DVOLDYEAR = COUNT(DISTINCT(consumerId)), 
	@CMOLDYEAR = COUNT(DISTINCT(contactable)), 
	@CMMOBILEOLDYEAR = COUNT(DISTINCT(mobileOnly)), 
	@CMEMAILOLDYEAR = COUNT(DISTINCT(emailOnly)), 
	@CMBothOLDYEAR = COUNT(DISTINCT(mobileEmail))
	FROM vw_rpt_consumer_register_by_quarter
	WHERE quarterYear = @YYYY - 1
		
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarterNum, COUNT(DISTINCT(consumerId)), COUNT(DISTINCT(contactable)), 
		COUNT(DISTINCT(mobileOnly)), COUNT(DISTINCT(emailOnly)), COUNT(DISTINCT(mobileEmail))
		FROM vw_rpt_consumer_register_by_quarter
		WHERE quarterYear = @YYYY
		GROUP BY quarterNum
		ORDER BY quarterNum ASC
		
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpDBVol, @tmpContactable, @tmpMobileOnly, @tmpEmailOnly, @tmpEmailMobile
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
		BEGIN
			SET @DVQ1 = @tmpDBVol
			SET @CMQ1 = @tmpContactable
			SET @CMEMAILQ1 = @tmpEmailOnly
			SET @CMMOBILEQ1 = @tmpMobileOnly
			SET @CMBothQ1 = @tmpEmailMobile
		END
		ELSE IF (@tmpQuarterNum = 2)
		BEGIN
			SET @DVQ2 = @tmpDBVol
			SET @CMQ2 = @tmpContactable
			SET @CMEMAILQ2 = @tmpEmailOnly
			SET @CMMOBILEQ2 = @tmpMobileOnly
			SET @CMBothQ2 = @tmpEmailMobile
		END
		ELSE IF (@tmpQuarterNum = 3)
		BEGIN
			SET @DVQ3 = @tmpDBVol
			SET @CMQ3 = @tmpContactable
			SET @CMEMAILQ3 = @tmpEmailOnly
			SET @CMMOBILEQ3 = @tmpMobileOnly
			SET @CMBothQ3 = @tmpEmailMobile
		END
		ELSE IF (@tmpQuarterNum = 4)
		BEGIN
			SET @DVQ4 = @tmpDBVol
			SET @CMQ4 = @tmpContactable
			SET @CMEMAILQ4 = @tmpEmailOnly
			SET @CMMOBILEQ4 = @tmpMobileOnly
			SET @CMBothQ4 = @tmpEmailMobile
		END

		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpDBVol, @tmpContactable, @tmpMobileOnly, @tmpEmailOnly, @tmpEmailMobile
	END

	CLOSE @cCursor

	SET @DVYTD = ISNULL(@DVQ1, 0) + ISNULL(@DVQ2, 0) + ISNULL(@DVQ3, 0) + ISNULL(@DVQ4, 0) 
	SET @CMYTD = ISNULL(@CMQ1, 0) + ISNULL(@CMQ2, 0) + ISNULL(@CMQ3, 0) + ISNULL(@CMQ4, 0) 
	SET @CMEMAILYTD = ISNULL(@CMEMAILQ1, 0) + ISNULL(@CMEMAILQ2, 0) + ISNULL(@CMEMAILQ3, 0) + ISNULL(@CMEMAILQ4, 0) 
	SET @CMMOBILEYTD = ISNULL(@CMMOBILEQ1, 0) + ISNULL(@CMMOBILEQ2, 0) + ISNULL(@CMMOBILEQ3, 0) + ISNULL(@CMMOBILEQ4, 0) 
	SET @CMBothYTD = ISNULL(@CMBothQ1, 0) + ISNULL(@CMBothQ2, 0) + ISNULL(@CMBothQ3, 0) + ISNULL(@CMBothQ4, 0) 

	INSERT INTO #ResultTable
	VALUES(
				@DVOLDYEAR,@DVQ1,@DVQ2,@DVQ3,@DVQ4,@DVYTD,
				@CMOLDYEAR,@CMQ1,@CMQ2,@CMQ3,@CMQ4,@CMYTD,
				@CMEMAILOLDYEAR,@CMEMAILQ1,@CMEMAILQ2,@CMEMAILQ3,@CMEMAILQ4,@CMEMAILYTD,
				@CMMOBILEOLDYEAR,@CMMOBILEQ1,@CMMOBILEQ2,@CMMOBILEQ3,@CMMOBILEQ4,@CMMOBILEYTD,
				@CMBothOLDYEAR,@CMBothQ1,@CMBothQ2,@CMBothQ3,@CMBothQ4,@CMBothYTD
		  )

	SELECT * FROM #ResultTable
	DROP TABLE #ResultTable
	
END
