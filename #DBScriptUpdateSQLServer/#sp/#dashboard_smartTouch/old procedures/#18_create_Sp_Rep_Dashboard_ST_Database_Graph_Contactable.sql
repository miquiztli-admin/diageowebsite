USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_ST_Database_Graph_Contactable]    Script Date: 10/01/2013 22:36:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_ST_Database_Graph_Contactable', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_ST_Database_Graph_Contactable;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_ST_Database_Graph_Contactable](
	@YYYY AS INT
)
AS
BEGIN

	DECLARE @CMEMAILYTD bigint,@CMMOBILEYTD bigint,@CMBothYTD bigint
	
	SELECT 
	@CMEMAILYTD = COUNT(DISTINCT(emailOnly)), 
	@CMMOBILEYTD = COUNT(DISTINCT(mobileOnly)), 
	@CMBothYTD = COUNT(DISTINCT(mobileEmail))
	FROM vw_rpt_consumer_register_by_quarter
	WHERE quarterYear = @YYYY
	AND contactable IS NOT NULL
	
	SELECT 'Email Only' AS Name, @CMEMAILYTD as Summary
	UNION
	SELECT 'Mobile Only' AS Name, @CMMOBILEYTD as Summary
	UNION
	SELECT 'Both' AS Name, @CMBothYTD as Summary
	
END
