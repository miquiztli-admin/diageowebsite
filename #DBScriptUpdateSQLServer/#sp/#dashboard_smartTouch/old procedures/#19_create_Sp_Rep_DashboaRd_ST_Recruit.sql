USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_DashboaRd_ST_Recruit]    Script Date: 10/01/2013 22:50:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'dbo.Sp_Rep_DashboaRd_ST_Recruit', N'P') IS NOT NULL
    DROP PROC Sp_Rep_DashboaRd_ST_Recruit;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_DashboaRd_ST_Recruit]
	@YYYY AS INT
AS
BEGIN

	DECLARE @NRECRUITOLDYEAR BIGINT,@NRECRUITQ1 BIGINT,@NRECRUITQ2 BIGINT,@NRECRUITQ3 BIGINT,@NRECRUITQ4 BIGINT,@NRECRUITYTD BIGINT
	DECLARE @NewDiageoQ1 BIGINT,@NewDiageoQ2 BIGINT,@NewDiageoQ3 BIGINT,@NewDiageoQ4 BIGINT,@NewDiageoYTD bigint
	DECLARE @UNSUBOLDYEAR BIGINT,@UNSUBQ1 BIGINT,@UNSUBQ2 BIGINT,@UNSUBQ3 BIGINT,@UNSUBQ4 BIGINT,@UNSUBYTD BIGINT
	DECLARE @IEMAILOLDYEAR BIGINT,@IEMAILQ1 BIGINT,@IEMAILQ2 BIGINT,@IEMAILQ3 BIGINT,@IEMAILQ4 BIGINT,@IEMAILYTD BIGINT
	DECLARE @IBMOBILEOLDYEAR BIGINT,@IBMOBILEQ1 BIGINT,@IBMOBILEQ2 BIGINT,@IBMOBILEQ3 BIGINT,@IBMOBILEQ4 BIGINT,@IBMOBILEYTD BIGINT
		
	DECLARE @cCursor CURSOR
	DECLARE @tmpQuarterNum int,@tmpVal1 bigint, @tmpVal2 bigint, @tmpVal3 bigint, @tmpVal4 bigint, @tmpVal5 bigint
	DECLARE @tmpFilter int, @tmpValue bigint

	--Create Temp Table
	BEGIN
	
	CREATE TABLE #ResultTable  ( NRECRUITQ1 bigint,NRECRUITQ2 bigint,NRECRUITQ3 bigint,NRECRUITQ4 bigint, NRECRUITYTD bigint,
								NewDiageoQ1 bigint,NewDiageoQ2 bigint,NewDiageoQ3 bigint,NewDiageoQ4 bigint,NewDiageoYTD bigint,
								 UNSUBQ1 bigint,UNSUBQ2 bigint,UNSUBQ3 bigint,UNSUBQ4 bigint, UNSUBQ5 bigint,
								 IEMAILOLDYEAR BIGINT,IEMAILQ1 BIGINT,IEMAILQ2 BIGINT,IEMAILQ3 BIGINT,IEMAILQ4 BIGINT,IEMAILYTD BIGINT,
								 IBMOBILEOLDYEAR BIGINT,IBMOBILEQ1 BIGINT,IBMOBILEQ2 BIGINT,IBMOBILEQ3 BIGINT,IBMOBILEQ4 BIGINT,IBMOBILEYTD BIGINT								 							
							   )
	END

	SELECT
	@UNSUBOLDYEAR = COUNT(DISTINCT(unsubscribe_email)),
	@IEMAILOLDYEAR = COUNT(DISTINCT(invalid_email)),
	@IBMOBILEOLDYEAR = COUNT(DISTINCT(invalid_block_mobile))
	FROM vw_rpt_recruit_by_quarter
	WHERE quarterYear = @YYYY - 1
	
	SET @cCursor = CURSOR FAST_FORWARD
	FOR	
		SELECT quarterNum,
		COUNT(DISTINCT(consumerId)),
		COUNT(DISTINCT(
			CASE 
				WHEN (brandPreBenmore IS NOT NULL OR brandPreHennessy IS NOT NULL 
					OR brandPreJwBlack IS NOT NULL OR brandPreJwGold IS NOT NULL 
					OR brandPreJwGreen IS NOT NULL OR brandPreJwRed IS NOT NULL 
					OR brandPreSmirnoff IS NOT NULL ) THEN consumerId
				ELSE NULL
			END
		)),
		COUNT(DISTINCT(unsubscribe_email)),
		COUNT(DISTINCT(invalid_email)),
		COUNT(DISTINCT(invalid_block_mobile))
		FROM vw_rpt_recruit_by_quarter
		WHERE quarterYear = @YYYY
		GROUP BY quarterNum

	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpVal1, @tmpVal2, @tmpVal3, @tmpVal4, @tmpVal5
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
		BEGIN
			SET @NRECRUITQ1 = @tmpVal1
			SET @NewDiageoQ1 = @tmpVal2
			SET @UNSUBQ1 = @tmpVal3
			SET @IEMAILQ1 = @tmpVal4
			SET @IBMOBILEQ1 = @tmpVal5
		END
		ELSE IF (@tmpQuarterNum = 2)
		BEGIN
			SET @NRECRUITQ2 = @tmpVal1
			SET @NewDiageoQ2 = @tmpVal2
			SET @UNSUBQ2 = @tmpVal3
			SET @IEMAILQ2 = @tmpVal4
			SET @IBMOBILEQ2 = @tmpVal5
		END
		ELSE IF (@tmpQuarterNum = 3)
		BEGIN
			SET @NRECRUITQ3 = @tmpVal1
			SET @NewDiageoQ3 = @tmpVal2
			SET @UNSUBQ3 = @tmpVal3
			SET @IEMAILQ3 = @tmpVal4
			SET @IBMOBILEQ3 = @tmpVal5
		END
		ELSE IF (@tmpQuarterNum = 4)
		BEGIN
			SET @NRECRUITQ4 = @tmpVal1
			SET @NewDiageoQ4 = @tmpVal2
			SET @UNSUBQ4 = @tmpVal3
			SET @IEMAILQ4 = @tmpVal4
			SET @IBMOBILEQ4 = @tmpVal5
		END
	
		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpVal1, @tmpVal2, @tmpVal3, @tmpVal4, @tmpVal5
	END
	
	SET @NRECRUITYTD = ISNULL(@NRECRUITQ1, 0) + ISNULL(@NRECRUITQ2, 0) + ISNULL(@NRECRUITQ3, 0) + ISNULL(@NRECRUITQ4, 0) 
	SET @NewDiageoYTD = ISNULL(@NewDiageoQ1, 0) + ISNULL(@NewDiageoQ2, 0) + ISNULL(@NewDiageoQ3, 0) + ISNULL(@NewDiageoQ4, 0) 
	SET @UNSUBYTD = ISNULL(@UNSUBQ1, 0) + ISNULL(@UNSUBQ2, 0) + ISNULL(@UNSUBQ3, 0) + ISNULL(@UNSUBQ4, 0) 
	SET @IEMAILYTD = ISNULL(@IEMAILQ1, 0) + ISNULL(@IEMAILQ2, 0) + ISNULL(@IEMAILQ3, 0) + ISNULL(@IEMAILQ4, 0) 
	SET @IBMOBILEYTD = ISNULL(@IBMOBILEQ1, 0) + ISNULL(@IBMOBILEQ2, 0) + ISNULL(@IBMOBILEQ3, 0) + ISNULL(@IBMOBILEQ4, 0) 
	
	INSERT INTO #ResultTable
	VALUES(
			@NRECRUITQ1 ,@NRECRUITQ2 ,@NRECRUITQ3 ,@NRECRUITQ4, @NRECRUITYTD,
			@NewDiageoQ1,@NewDiageoQ2,@NewDiageoQ3,@NewDiageoQ4,@NewDiageoYTD,
			@UNSUBQ1,@UNSUBQ2,@UNSUBQ3,@UNSUBQ4, @UNSUBYTD,
			@IEMAILOLDYEAR,@IEMAILQ1,@IEMAILQ2,@IEMAILQ3,@IEMAILQ4,@IEMAILYTD,
			@IBMOBILEOLDYEAR,@IBMOBILEQ1,@IBMOBILEQ2,@IBMOBILEQ3,@IBMOBILEQ4,@IBMOBILEYTD
		  )
		  
	select * from #ResultTable

	Drop Table #ResultTable
	
END
