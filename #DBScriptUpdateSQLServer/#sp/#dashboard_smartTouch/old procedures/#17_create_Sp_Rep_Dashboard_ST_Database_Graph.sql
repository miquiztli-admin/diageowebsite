USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_ST_Database_Graph]    Script Date: 10/01/2013 22:25:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_ST_Database_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_ST_Database_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_ST_Database_Graph]
	@YYYY AS INT
AS
BEGIN

	SELECT tmp.period, 
	ISNULL(rpt.DatabaseVolume, 0) AS DatabaseVolume,
	ISNULL(rpt.ContactableMass, 0) AS ContactableMass,
	ISNULL(rpt.HVCContactable, 0) AS HVCContactable
	FROM tbl_m_rpt_dashb_templ_graph tmp
		LEFT OUTER JOIN (
			SELECT quarterNum AS tmp_num,
			COUNT(DISTINCT(consumerID)) AS DatabaseVolume, 
			COUNT(DISTINCT(contactable)) AS ContactableMass, 
			/*COUNT(DISTINCT(
				CASE survey4aJwBlack
					WHEN 1 THEN consumerID --Adorer
					WHEN 2 THEN consumerID --Adopter
					ELSE NULL
				END
			))*/0 AS HVCContactable
			FROM vw_rpt_consumer_register_by_quarter
			WHERE quarterYear = @YYYY
			GROUP BY quarterNum
			
			UNION ALL
			SELECT 5 AS tmp_num,
			COUNT(DISTINCT(consumerID)) AS DatabaseVolume, 
			COUNT(DISTINCT(contactable)) AS ContactableMass, 
			/*COUNT(DISTINCT(
				CASE survey4aJwBlack
					WHEN 1 THEN consumerID --Adorer
					WHEN 2 THEN consumerID --Adopter
					ELSE NULL
				END
			))*/0 AS HVCContactable
			FROM vw_rpt_consumer_register_by_quarter
			WHERE quarterYear = @YYYY
			
			UNION ALL
			SELECT 6 AS tmp_num, 0 AS DatabaseVolume, 0 AS ContactableMass, 0 AS HVCContactable

		) rpt ON tmp.num = rpt.tmp_num 
	
END
