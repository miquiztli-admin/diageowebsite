USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_ST_BrandPreference_Graph]    Script Date: 10/01/2013 21:54:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'Sp_Rep_Dashboard_ST_BrandPreference_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_ST_BrandPreference_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_ST_BrandPreference_Graph]
	@YYYY AS INT
AS
BEGIN

	DECLARE @BLACKYTD BIGINT,@SMYTD BIGINT,@REDYTD BIGINT,@DBYTD BIGINT,@CBYTD BIGINT,@OTYTD BIGINT,@NVYTD BIGINT
			
	--Create Temp Table
	BEGIN
		CREATE TABLE #ResultTable  (Name varchar(20),Summary bigint) 
	END

	SELECT 
	@BLACKYTD = COUNT(DISTINCT(brandPreJwBlack)),
	@SMYTD = COUNT(DISTINCT(brandPreSmirnoff)), 
	@REDYTD = COUNT(DISTINCT(brandPreJwRed)), 
	@DBYTD = COUNT(DISTINCT(brandPreOther)), 
	@CBYTD = COUNT(DISTINCT(brandPreCompetitors)), 
	@OTYTD = COUNT(DISTINCT(brandPreOther)), 
	@NVYTD = COUNT(DISTINCT(consumerID))/*, 
	@BLYTD = COUNT(DISTINCT(brandPreBaileys)), 
	@BMFCYTD = COUNT(DISTINCT(brandPreBenmore)),
	@GOLDYTD = COUNT(DISTINCT(brandPreJwGold)),
	@GREENYTD = COUNT(DISTINCT(brandPreJwGreen)),
	@ASYTD = COUNT(DISTINCT(brandPreAbsolut)),
	@BLTYTD = COUNT(DISTINCT(brandPreBallentine)),
	@BL289YTD = COUNT(DISTINCT(brandPreBlend)),
	@CVYTD = COUNT(DISTINCT(brandPreChivas)),
	@DWYTD = COUNT(DISTINCT(brandPreDewar)),
	@HNYTD = COUNT(DISTINCT(brandPreHennessy)),
	@100PIYTD = COUNT(DISTINCT(brandPre100Pipers)),
	@100P8YYTD = COUNT(DISTINCT(brandPre100Pipers8Y))*/
	FROM vw_rpt_consumer_register_by_quarter
	WHERE quarterYear = @YYYY
	AND brandPre IS NOT NULL
	
	--insert 
	insert into #ResultTable
	values('JW Black Label',@BLACKYTD)

	insert into #ResultTable
	values('Smirnoff',@SMYTD)

	insert into #ResultTable
	values('JW Red Label',@REDYTD)

	insert into #ResultTable
	values('Other Diageo Brands',@DBYTD)

	insert into #ResultTable
	values('Competitor Brands',@CBYTD)

	insert into #ResultTable
	values('Other',@OTYTD)

	insert into #ResultTable
	values('No Value',@NVYTD)	


	SELECT Name
		, replace(convert(varchar,cast(Summary AS money ),1),'.00','') AS Summary 
	FROM #ResultTable
	
END
