USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_ST_BrandPreference]    Script Date: 10/01/2013 18:38:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_ST_BrandPreference', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_ST_BrandPreference;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_ST_BrandPreference](
	@YYYY AS INT
)
AS
BEGIN

	DECLARE @BLACKOLDYEAR BIGINT,@BLACKQ1 BIGINT,@BLACKQ2 BIGINT,@BLACKQ3 BIGINT,@BLACKQ4 BIGINT,@BLACKYTD BIGINT
	DECLARE @SMOLDYEAR BIGINT,@SMQ1 BIGINT,@SMQ2 BIGINT,@SMQ3 BIGINT,@SMQ4 BIGINT,@SMYTD BIGINT
	DECLARE @REDOLDYEAR BIGINT,@REDQ1 BIGINT,@REDQ2 BIGINT,@REDQ3 BIGINT,@REDQ4 BIGINT,@REDYTD BIGINT
	DECLARE @DBOLDYEAR BIGINT,@DBQ1 BIGINT,@DBQ2 BIGINT,@DBQ3 BIGINT,@DBQ4 BIGINT,@DBYTD BIGINT
	DECLARE @CBOLDYEAR BIGINT,@CBQ1 BIGINT,@CBQ2 BIGINT,@CBQ3 BIGINT,@CBQ4 BIGINT,@CBYTD BIGINT
	DECLARE @OTOLDYEAR BIGINT,@OTQ1 BIGINT,@OTQ2 BIGINT,@OTQ3 BIGINT,@OTQ4 BIGINT,@OTYTD BIGINT
	DECLARE @NVOLDYEAR BIGINT,@NVQ1 BIGINT,@NVQ2 BIGINT,@NVQ3 BIGINT,@NVQ4 BIGINT,@NVYTD BIGINT
			
	---------------------------------------------------------------------------
	DECLARE @BLOLDYEAR BIGINT,@BLQ1 BIGINT,@BLQ2 BIGINT,@BLQ3 BIGINT,@BLQ4 BIGINT,@BLYTD BIGINT
	DECLARE @BMFCOLDYEAR BIGINT,@BMFCQ1 BIGINT,@BMFCQ2 BIGINT,@BMFCQ3 BIGINT,@BMFCQ4 BIGINT,@BMFCYTD BIGINT
	DECLARE @GOLDOLDYEAR BIGINT,@GOLDQ1 BIGINT,@GOLDQ2 BIGINT,@GOLDQ3 BIGINT,@GOLDQ4 BIGINT,@GOLDYTD BIGINT
	DECLARE @GREENOLDYEAR BIGINT,@GREENQ1 BIGINT,@GREENQ2 BIGINT,@GREENQ3 BIGINT,@GREENQ4 BIGINT,@GREENYTD BIGINT
	DECLARE @ASOLDYEAR BIGINT,@ASQ1 BIGINT,@ASQ2 BIGINT,@ASQ3 BIGINT,@ASQ4 BIGINT,@ASYTD BIGINT
	DECLARE @BLTOLDYEAR BIGINT,@BLTQ1 BIGINT,@BLTQ2 BIGINT,@BLTQ3 BIGINT,@BLTQ4 BIGINT,@BLTYTD BIGINT
	DECLARE @BL289OLDYEAR BIGINT,@BL289Q1 BIGINT,@BL289Q2 BIGINT,@BL289Q3 BIGINT,@BL289Q4 BIGINT,@BL289YTD BIGINT
	DECLARE @CVOLDYEAR BIGINT,@CVQ1 BIGINT,@CVQ2 BIGINT,@CVQ3 BIGINT,@CVQ4 BIGINT,@CVYTD BIGINT
	DECLARE @DWOLDYEAR BIGINT,@DWQ1 BIGINT,@DWQ2 BIGINT,@DWQ3 BIGINT,@DWQ4 BIGINT,@DWYTD BIGINT
	DECLARE @HNOLDYEAR BIGINT,@HNQ1 BIGINT,@HNQ2 BIGINT,@HNQ3 BIGINT,@HNQ4 BIGINT,@HNYTD BIGINT
	DECLARE @100PIOLDYEAR BIGINT,@100PIQ1 BIGINT,@100PIQ2 BIGINT,@100PIQ3 BIGINT,@100PIQ4 BIGINT,@100PIYTD BIGINT
	DECLARE @100P8YOLDYEAR BIGINT,@100P8YQ1 BIGINT,@100P8YQ2 BIGINT,@100P8YQ3 BIGINT,@100P8YQ4 BIGINT,@100P8YYTD BIGINT
			
			
	--Create Temp Table
	BEGIN
	
	CREATE TABLE #ResultTable  ( 	
									 BLACKOLDYEAR BIGINT,BLACKQ1 BIGINT,BLACKQ2 BIGINT,BLACKQ3 BIGINT,BLACKQ4 BIGINT,BLACKYTD BIGINT,
									 SMOLDYEAR BIGINT,SMQ1 BIGINT,SMQ2 BIGINT,SMQ3 BIGINT,SMQ4 BIGINT,SMYTD BIGINT,
									 REDOLDYEAR BIGINT,REDQ1 BIGINT,REDQ2 BIGINT,REDQ3 BIGINT,REDQ4 BIGINT,REDYTD BIGINT,
									 DBOLDYEAR BIGINT,DBQ1 BIGINT,DBQ2 BIGINT,DBQ3 BIGINT,DBQ4 BIGINT,DBYTD BIGINT,
									 CBOLDYEAR BIGINT,CBQ1 BIGINT,CBQ2 BIGINT,CBQ3 BIGINT,CBQ4 BIGINT,CBYTD BIGINT,
									 OTOLDYEAR BIGINT,OTQ1 BIGINT,OTQ2 BIGINT,OTQ3 BIGINT,OTQ4 BIGINT,OTYTD BIGINT,
									 NVOLDYEAR BIGINT,NVQ1 BIGINT,NVQ2 BIGINT,NVQ3 BIGINT,NVQ4 BIGINT,NVYTD BIGINT,	
									 BLOLDYEAR BIGINT,BLQ1 BIGINT,BLQ2 BIGINT,BLQ3 BIGINT,BLQ4 BIGINT,BLYTD BIGINT,
									 BMFCOLDYEAR BIGINT,BMFCQ1 BIGINT,BMFCQ2 BIGINT,BMFCQ3 BIGINT,BMFCQ4 BIGINT,BMFCYTD BIGINT,
									 GOLDOLDYEAR BIGINT,GOLDQ1 BIGINT,GOLDQ2 BIGINT,GOLDQ3 BIGINT,GOLDQ4 BIGINT,GOLDYTD BIGINT,
									 GREENOLDYEAR BIGINT,GREENQ1 BIGINT,GREENQ2 BIGINT,GREENQ3 BIGINT,GREENQ4 BIGINT,GREENYTD BIGINT,
									 ASOLDYEAR BIGINT,ASQ1 BIGINT,ASQ2 BIGINT,ASQ3 BIGINT,ASQ4 BIGINT,ASYTD BIGINT,
									 BLTOLDYEAR BIGINT,BLTQ1 BIGINT,BLTQ2 BIGINT,BLTQ3 BIGINT,BLTQ4 BIGINT,BLTYTD BIGINT,
									 BL289OLDYEAR BIGINT,BL289Q1 BIGINT,BL289Q2 BIGINT,BL289Q3 BIGINT,BL289Q4 BIGINT,BL289YTD BIGINT,
									 CVOLDYEAR BIGINT,CVQ1 BIGINT,CVQ2 BIGINT,CVQ3 BIGINT,CVQ4 BIGINT,CVYTD BIGINT,
									 DWOLDYEAR BIGINT,DWQ1 BIGINT,DWQ2 BIGINT,DWQ3 BIGINT,DWQ4 BIGINT,DWYTD BIGINT,
									 HNOLDYEAR BIGINT,HNQ1 BIGINT,HNQ2 BIGINT,HNQ3 BIGINT,HNQ4 BIGINT,HNYTD BIGINT,
									 ONEHUNDREADPIOLDYEAR BIGINT,ONEHUNDREADPIQ1 BIGINT,ONEHUNDREADPIQ2 BIGINT,ONEHUNDREADPIQ3 BIGINT,ONEHUNDREADPIQ4 BIGINT,ONEHUNDREADPIYTD BIGINT,
									 ONEHUNDREADP8YOLDYEAR BIGINT,ONEHUNDREAD8YQ1 BIGINT,ONEHUNDREADP8YQ2 BIGINT,ONEHUNDREADP8YQ3 BIGINT,ONEHUNDREADP8YQ4 BIGINT,ONEHUNDREADP8YYTD BIGINT								 							
							   )
	END


	SELECT 
	@BLACKOLDYEAR = COUNT(DISTINCT(brandPreJwBlack)),
	@SMOLDYEAR = COUNT(DISTINCT(brandPreSmirnoff)), 
	@REDOLDYEAR = COUNT(DISTINCT(brandPreJwRed)), 
	@DBOLDYEAR = COUNT(DISTINCT(brandPreOther)), 
	@CBOLDYEAR = COUNT(DISTINCT(brandPreCompetitors)), 
	@OTOLDYEAR = COUNT(DISTINCT(brandPreOther)), 
	@NVOLDYEAR = COUNT(DISTINCT(consumerID)), 
	@BLOLDYEAR = COUNT(DISTINCT(brandPreBaileys)), 
	@BMFCOLDYEAR = COUNT(DISTINCT(brandPreBenmore)),
	@GOLDOLDYEAR = COUNT(DISTINCT(brandPreJwGold)),
	@GREENOLDYEAR = COUNT(DISTINCT(brandPreJwGreen)),
	@ASOLDYEAR = COUNT(DISTINCT(brandPreAbsolut)),
	@BLTOLDYEAR = COUNT(DISTINCT(brandPreBallentine)),
	@BL289OLDYEAR = COUNT(DISTINCT(brandPreBlend)),
	@CVOLDYEAR = COUNT(DISTINCT(brandPreChivas)),
	@DWOLDYEAR = COUNT(DISTINCT(brandPreDewar)),
	@HNOLDYEAR = COUNT(DISTINCT(brandPreHennessy)),
	@100PIOLDYEAR = COUNT(DISTINCT(brandPre100Pipers)),
	@100P8YOLDYEAR = COUNT(DISTINCT(brandPre100Pipers8Y))
	FROM vw_rpt_consumer_register_by_quarter
	WHERE quarterYear = @YYYY - 1
	AND brandPre IS NOT NULL
	
	SELECT 
	@BLACKYTD = COUNT(DISTINCT(brandPreJwBlack)),
	@SMYTD = COUNT(DISTINCT(brandPreSmirnoff)), 
	@REDYTD = COUNT(DISTINCT(brandPreJwRed)), 
	@DBYTD = COUNT(DISTINCT(brandPreOther)), 
	@CBYTD = COUNT(DISTINCT(brandPreCompetitors)), 
	@OTYTD = COUNT(DISTINCT(brandPreOther)), 
	@NVYTD = COUNT(DISTINCT(consumerID)), 
	@BLYTD = COUNT(DISTINCT(brandPreBaileys)), 
	@BMFCYTD = COUNT(DISTINCT(brandPreBenmore)),
	@GOLDYTD = COUNT(DISTINCT(brandPreJwGold)),
	@GREENYTD = COUNT(DISTINCT(brandPreJwGreen)),
	@ASYTD = COUNT(DISTINCT(brandPreAbsolut)),
	@BLTYTD = COUNT(DISTINCT(brandPreBallentine)),
	@BL289YTD = COUNT(DISTINCT(brandPreBlend)),
	@CVYTD = COUNT(DISTINCT(brandPreChivas)),
	@DWYTD = COUNT(DISTINCT(brandPreDewar)),
	@HNYTD = COUNT(DISTINCT(brandPreHennessy)),
	@100PIYTD = COUNT(DISTINCT(brandPre100Pipers)),
	@100P8YYTD = COUNT(DISTINCT(brandPre100Pipers8Y))
	FROM vw_rpt_consumer_register_by_quarter
	WHERE quarterYear = @YYYY
	AND brandPre IS NOT NULL
	
	SELECT 
	@BLACKQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreJwBlack ELSE NULL END)),
	@SMQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreSmirnoff ELSE NULL END)), 
	@REDQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreJwRed ELSE NULL END)), 
	@DBQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreOther ELSE NULL END)), 
	@CBQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreCompetitors ELSE NULL END)), 
	@OTQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreOther ELSE NULL END)), 
	@NVQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN consumerID ELSE NULL END)), 
	@BLQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreBaileys ELSE NULL END)), 
	@BMFCQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreBenmore ELSE NULL END)),
	@GOLDQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreJwGold ELSE NULL END)),
	@GREENQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreJwGreen ELSE NULL END)),
	@ASQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreAbsolut ELSE NULL END)),
	@BLTQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreBallentine ELSE NULL END)),
	@BL289Q1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreBlend ELSE NULL END)),
	@CVQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreChivas ELSE NULL END)),
	@DWQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreDewar ELSE NULL END)),
	@HNQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPreHennessy ELSE NULL END)),
	@100PIQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPre100Pipers ELSE NULL END)),
	@100P8YQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN brandPre100Pipers8Y ELSE NULL END)),
	@BLACKQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreJwBlack ELSE NULL END)),
	@SMQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreSmirnoff ELSE NULL END)), 
	@REDQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreJwRed ELSE NULL END)), 
	@DBQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreOther ELSE NULL END)), 
	@CBQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreCompetitors ELSE NULL END)), 
	@OTQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreOther ELSE NULL END)), 
	@NVQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN consumerID ELSE NULL END)), 
	@BLQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreBaileys ELSE NULL END)), 
	@BMFCQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreBenmore ELSE NULL END)),
	@GOLDQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreJwGold ELSE NULL END)),
	@GREENQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreJwGreen ELSE NULL END)),
	@ASQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreAbsolut ELSE NULL END)),
	@BLTQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreBallentine ELSE NULL END)),
	@BL289Q2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreBlend ELSE NULL END)),
	@CVQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreChivas ELSE NULL END)),
	@DWQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreDewar ELSE NULL END)),
	@HNQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPreHennessy ELSE NULL END)),
	@100PIQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPre100Pipers ELSE NULL END)),
	@100P8YQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN brandPre100Pipers8Y ELSE NULL END)),
	@BLACKQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreJwBlack ELSE NULL END)),
	@SMQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreSmirnoff ELSE NULL END)), 
	@REDQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreJwRed ELSE NULL END)), 
	@DBQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreOther ELSE NULL END)), 
	@CBQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreCompetitors ELSE NULL END)), 
	@OTQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreOther ELSE NULL END)), 
	@NVQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN consumerID ELSE NULL END)), 
	@BLQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreBaileys ELSE NULL END)), 
	@BMFCQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreBenmore ELSE NULL END)),
	@GOLDQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreJwGold ELSE NULL END)),
	@GREENQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreJwGreen ELSE NULL END)),
	@ASQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreAbsolut ELSE NULL END)),
	@BLTQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreBallentine ELSE NULL END)),
	@BL289Q3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreBlend ELSE NULL END)),
	@CVQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreChivas ELSE NULL END)),
	@DWQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreDewar ELSE NULL END)),
	@HNQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPreHennessy ELSE NULL END)),
	@100PIQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPre100Pipers ELSE NULL END)),
	@100P8YQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN brandPre100Pipers8Y ELSE NULL END)),
	@BLACKQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreJwBlack ELSE NULL END)),
	@SMQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreSmirnoff ELSE NULL END)), 
	@REDQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreJwRed ELSE NULL END)), 
	@DBQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreOther ELSE NULL END)), 
	@CBQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreCompetitors ELSE NULL END)), 
	@OTQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreOther ELSE NULL END)), 
	@NVQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN consumerID ELSE NULL END)), 
	@BLQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreBaileys ELSE NULL END)), 
	@BMFCQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreBenmore ELSE NULL END)),
	@GOLDQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreJwGold ELSE NULL END)),
	@GREENQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreJwGreen ELSE NULL END)),
	@ASQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreAbsolut ELSE NULL END)),
	@BLTQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreBallentine ELSE NULL END)),
	@BL289Q4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreBlend ELSE NULL END)),
	@CVQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreChivas ELSE NULL END)),
	@DWQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreDewar ELSE NULL END)),
	@HNQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPreHennessy ELSE NULL END)),
	@100PIQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPre100Pipers ELSE NULL END)),
	@100P8YQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN brandPre100Pipers8Y ELSE NULL END))
	FROM vw_rpt_consumer_register_by_quarter
	WHERE quarterYear = @YYYY
	AND brandPre IS NOT NULL
	
	INSERT INTO #ResultTable
	VALUES(
			@BLACKOLDYEAR,@BLACKQ1 ,@BLACKQ2 ,@BLACKQ3 ,@BLACKQ4 ,@BLACKYTD ,
			@SMOLDYEAR ,@SMQ1 ,@SMQ2 ,@SMQ3 ,@SMQ4 ,@SMYTD ,
									 @REDOLDYEAR ,@REDQ1 ,@REDQ2 ,@REDQ3 ,@REDQ4 ,@REDYTD ,
									 @DBOLDYEAR ,@DBQ1 ,@DBQ2 ,@DBQ3 ,@DBQ4 ,@DBYTD ,
									 @CBOLDYEAR ,@CBQ1 ,@CBQ2 ,@CBQ3 ,@CBQ4 ,@CBYTD ,
									 @OTOLDYEAR ,@OTQ1 ,@OTQ2 ,@OTQ3 ,@OTQ4 ,@OTYTD ,
									 @NVOLDYEAR ,@NVQ1 ,@NVQ2 ,@NVQ3 ,@NVQ4 ,@NVYTD ,	
									 @BLOLDYEAR ,@BLQ1 ,@BLQ2 ,@BLQ3 ,@BLQ4 ,@BLYTD ,
									 @BMFCOLDYEAR ,@BMFCQ1 ,@BMFCQ2 , @BMFCQ3 ,@BMFCQ4 ,@BMFCYTD ,
									 @GOLDOLDYEAR ,@GOLDQ1 ,@GOLDQ2 ,@GOLDQ3 ,@GOLDQ4 ,@GOLDYTD ,
									 @GREENOLDYEAR ,@GREENQ1 ,@GREENQ2 ,@GREENQ3 ,@GREENQ4 ,@GREENYTD ,
									 @ASOLDYEAR ,@ASQ1 ,@ASQ2 ,@ASQ3 ,@ASQ4 ,@ASYTD ,
									 @BLTOLDYEAR ,@BLTQ1 ,@BLTQ2 ,@BLTQ3 ,@BLTQ4 ,@BLTYTD ,
									 @BL289OLDYEAR ,@BL289Q1 ,@BL289Q2 ,@BL289Q3 ,@BL289Q4 ,@BL289YTD ,
									 @CVOLDYEAR ,@CVQ1 ,@CVQ2 ,@CVQ3 ,@CVQ4 ,@CVYTD ,
									 @DWOLDYEAR ,@DWQ1 ,@DWQ2 ,@DWQ3 ,@DWQ4 ,@DWYTD ,
									 @HNOLDYEAR ,@HNQ1 ,@HNQ2 ,@HNQ3 ,@HNQ4 ,@HNYTD ,
									 @100PIOLDYEAR ,@100PIQ1 ,@100PIQ2 ,@100PIQ3 ,@100PIQ4 ,@100PIYTD ,
									 @100P8YOLDYEAR ,@100P8YQ1 ,@100P8YQ2 ,@100P8YQ3 ,@100P8YQ4 ,@100P8YYTD 								 							
		  )

	SELECT replace(convert(varchar,cast(BLACKOLDYEAR AS money ),1),'.00','') AS BLACKOLDYEAR ,replace(convert(varchar,cast(BLACKQ1 AS money ),1),'.00','') AS BLACKQ1 ,replace(convert(varchar,cast(BLACKQ2 AS money ),1),'.00','') AS BLACKQ2 ,replace(convert(varchar,cast(BLACKQ3 AS money ),1),'.00','') AS BLACKQ3 ,replace(convert(varchar,cast(BLACKQ4 AS money ),1),'.00','') AS BLACKQ4 ,replace(convert(varchar,cast(BLACKYTD AS money ),1),'.00','') AS BLACKYTD ,
									 replace(convert(varchar,cast(SMOLDYEAR AS money ),1),'.00','') AS SMOLDYEAR ,replace(convert(varchar,cast(SMQ1 AS money ),1),'.00','') AS SMQ1 ,replace(convert(varchar,cast(SMQ2 AS money ),1),'.00','') AS SMQ2 ,replace(convert(varchar,cast(SMQ3 AS money ),1),'.00','') AS SMQ3 ,replace(convert(varchar,cast(SMQ4 AS money ),1),'.00','') AS SMQ4 ,replace(convert(varchar,cast(SMYTD AS money ),1),'.00','') AS SMYTD ,
									 replace(convert(varchar,cast(REDOLDYEAR AS money ),1),'.00','') AS REDOLDYEAR ,replace(convert(varchar,cast(REDQ1 AS money ),1),'.00','') AS REDQ1 ,replace(convert(varchar,cast(REDQ2 AS money ),1),'.00','') AS REDQ2 ,replace(convert(varchar,cast(REDQ3 AS money ),1),'.00','') AS REDQ3 ,replace(convert(varchar,cast(REDQ4 AS money ),1),'.00','') AS REDQ4 ,replace(convert(varchar,cast(REDYTD AS money ),1),'.00','') AS REDYTD ,
									 replace(convert(varchar,cast(DBOLDYEAR AS money ),1),'.00','') AS DBOLDYEAR ,replace(convert(varchar,cast(DBQ1 AS money ),1),'.00','') AS DBQ1 ,replace(convert(varchar,cast(DBQ2 AS money ),1),'.00','') AS DBQ2 ,replace(convert(varchar,cast(DBQ3 AS money ),1),'.00','') AS DBQ3 ,replace(convert(varchar,cast(DBQ4 AS money ),1),'.00','') AS DBQ4 ,replace(convert(varchar,cast(DBYTD AS money ),1),'.00','') AS DBYTD ,
									 replace(convert(varchar,cast(CBOLDYEAR AS money ),1),'.00','') AS CBOLDYEAR ,replace(convert(varchar,cast(CBQ1 AS money ),1),'.00','') AS CBQ1 ,replace(convert(varchar,cast(CBQ2 AS money ),1),'.00','') AS CBQ2 ,replace(convert(varchar,cast(CBQ3 AS money ),1),'.00','') AS CBQ3 ,replace(convert(varchar,cast(CBQ4 AS money ),1),'.00','') AS CBQ4 ,replace(convert(varchar,cast(CBYTD AS money ),1),'.00','') AS CBYTD ,
									 replace(convert(varchar,cast(OTOLDYEAR AS money ),1),'.00','') AS OTOLDYEAR ,replace(convert(varchar,cast(OTQ1 AS money ),1),'.00','') AS OTQ1 ,replace(convert(varchar,cast(OTQ2 AS money ),1),'.00','') AS OTQ2 ,replace(convert(varchar,cast(OTQ3 AS money ),1),'.00','') AS OTQ3 ,replace(convert(varchar,cast(OTQ4 AS money ),1),'.00','') AS OTQ4 ,replace(convert(varchar,cast(OTYTD AS money ),1),'.00','') AS OTYTD ,
									 replace(convert(varchar,cast(NVOLDYEAR AS money ),1),'.00','') AS NVOLDYEAR ,replace(convert(varchar,cast(NVQ1 AS money ),1),'.00','') AS NVQ1 ,replace(convert(varchar,cast(NVQ2 AS money ),1),'.00','') AS NVQ2 ,replace(convert(varchar,cast(NVQ3 AS money ),1),'.00','') AS NVQ3 ,replace(convert(varchar,cast(NVQ4 AS money ),1),'.00','') AS NVQ4 ,replace(convert(varchar,cast(NVYTD AS money ),1),'.00','') AS NVYTD ,	
									 replace(convert(varchar,cast(BLOLDYEAR AS money ),1),'.00','') AS BLOLDYEAR ,replace(convert(varchar,cast(BLQ1 AS money ),1),'.00','') AS BLQ1 ,replace(convert(varchar,cast(BLQ2 AS money ),1),'.00','') AS BLQ2 ,replace(convert(varchar,cast(BLQ3 AS money ),1),'.00','') AS BLQ3 ,replace(convert(varchar,cast(BLQ4 AS money ),1),'.00','') AS BLQ4 ,replace(convert(varchar,cast(BLYTD AS money ),1),'.00','') AS BLYTD ,
									 replace(convert(varchar,cast(BMFCOLDYEAR AS money ),1),'.00','') AS BMFCOLDYEAR ,replace(convert(varchar,cast(BMFCQ1 AS money ),1),'.00','') AS BMFCQ1 ,replace(convert(varchar,cast(BMFCQ2 AS money ),1),'.00','') AS BMFCQ2 ,replace(convert(varchar,cast(BMFCQ3 AS money ),1),'.00','') AS BMFCQ3 ,replace(convert(varchar,cast(BMFCQ4 AS money ),1),'.00','') AS BMFCQ4 ,replace(convert(varchar,cast(BMFCYTD AS money ),1),'.00','') AS BMFCYTD ,
									 replace(convert(varchar,cast(GOLDOLDYEAR AS money ),1),'.00','') AS GOLDOLDYEAR ,replace(convert(varchar,cast(GOLDQ1 AS money ),1),'.00','') AS GOLDQ1 ,replace(convert(varchar,cast(GOLDQ2 AS money ),1),'.00','') AS GOLDQ2 ,replace(convert(varchar,cast(GOLDQ3 AS money ),1),'.00','') AS GOLDQ3 ,replace(convert(varchar,cast(GOLDQ4 AS money ),1),'.00','') AS GOLDQ4 ,replace(convert(varchar,cast(GOLDYTD AS money ),1),'.00','') AS GOLDYTD ,
									 replace(convert(varchar,cast(GREENOLDYEAR AS money ),1),'.00','') AS GREENOLDYEAR ,replace(convert(varchar,cast(GREENQ1 AS money ),1),'.00','') AS GREENQ1 ,replace(convert(varchar,cast(GREENQ2 AS money ),1),'.00','') AS GREENQ2 ,replace(convert(varchar,cast(GREENQ3 AS money ),1),'.00','') AS GREENQ3 ,replace(convert(varchar,cast(GREENQ4 AS money ),1),'.00','') AS GREENQ4 ,replace(convert(varchar,cast(GREENYTD AS money ),1),'.00','') AS GREENYTD ,
									 replace(convert(varchar,cast(ASOLDYEAR AS money ),1),'.00','') AS ASOLDYEAR ,replace(convert(varchar,cast(ASQ1 AS money ),1),'.00','') AS ASQ1 ,replace(convert(varchar,cast(ASQ2 AS money ),1),'.00','') AS ASQ2 ,replace(convert(varchar,cast(ASQ3 AS money ),1),'.00','') AS ASQ3 ,replace(convert(varchar,cast(ASQ4 AS money ),1),'.00','') AS ASQ4 ,replace(convert(varchar,cast(ASYTD AS money ),1),'.00','') AS ASYTD ,
									 replace(convert(varchar,cast(BLTOLDYEAR AS money ),1),'.00','') AS BLTOLDYEAR ,replace(convert(varchar,cast(BLTQ1 AS money ),1),'.00','') AS BLTQ1 ,replace(convert(varchar,cast(BLTQ2 AS money ),1),'.00','') AS BLTQ2 ,replace(convert(varchar,cast(BLTQ3 AS money ),1),'.00','') AS BLTQ3 ,replace(convert(varchar,cast(BLTQ4 AS money ),1),'.00','') AS BLTQ4 ,replace(convert(varchar,cast(BLTYTD AS money ),1),'.00','') AS BLTYTD ,
									 replace(convert(varchar,cast(BL289OLDYEAR AS money ),1),'.00','') AS BL289OLDYEAR ,replace(convert(varchar,cast(BL289Q1 AS money ),1),'.00','') AS BL289Q1 ,replace(convert(varchar,cast(BL289Q2 AS money ),1),'.00','') AS BL289Q2 ,replace(convert(varchar,cast(BL289Q3 AS money ),1),'.00','') AS BL289Q3 ,replace(convert(varchar,cast(BL289Q4 AS money ),1),'.00','') AS BL289Q4 ,replace(convert(varchar,cast(BL289YTD AS money ),1),'.00','') AS BL289YTD ,
									 replace(convert(varchar,cast(CVOLDYEAR AS money ),1),'.00','') AS CVOLDYEAR ,replace(convert(varchar,cast(CVQ1 AS money ),1),'.00','') AS CVQ1 ,replace(convert(varchar,cast(CVQ2 AS money ),1),'.00','') AS CVQ2 ,replace(convert(varchar,cast(CVQ3 AS money ),1),'.00','') AS CVQ3 ,replace(convert(varchar,cast(CVQ4 AS money ),1),'.00','') AS CVQ4 ,replace(convert(varchar,cast(CVYTD AS money ),1),'.00','') AS CVYTD ,
									 replace(convert(varchar,cast(DWOLDYEAR AS money ),1),'.00','') AS DWOLDYEAR ,replace(convert(varchar,cast(DWQ1 AS money ),1),'.00','') AS DWQ1 ,replace(convert(varchar,cast(DWQ2 AS money ),1),'.00','') AS DWQ2 ,replace(convert(varchar,cast(DWQ3 AS money ),1),'.00','') AS DWQ3 ,replace(convert(varchar,cast(DWQ4 AS money ),1),'.00','') AS DWQ4 ,replace(convert(varchar,cast(DWYTD AS money ),1),'.00','') AS DWYTD ,
									 replace(convert(varchar,cast(HNOLDYEAR AS money ),1),'.00','') AS HNOLDYEAR ,replace(convert(varchar,cast(HNQ1 AS money ),1),'.00','') AS HNQ1 ,replace(convert(varchar,cast(HNQ2 AS money ),1),'.00','') AS HNQ2 ,replace(convert(varchar,cast(HNQ3 AS money ),1),'.00','') AS HNQ3 ,replace(convert(varchar,cast(HNQ4 AS money ),1),'.00','') AS HNQ4 ,replace(convert(varchar,cast(HNYTD AS money ),1),'.00','') AS HNYTD ,
									 replace(convert(varchar,cast(ONEHUNDREADPIOLDYEAR AS money ),1),'.00','') AS ONEHUNDREADPIOLDYEAR ,replace(convert(varchar,cast(ONEHUNDREADPIQ1 AS money ),1),'.00','') AS ONEHUNDREADPIQ1 ,replace(convert(varchar,cast(ONEHUNDREADPIQ2 AS money ),1),'.00','') AS ONEHUNDREADPIQ2 ,replace(convert(varchar,cast(ONEHUNDREADPIQ3 AS money ),1),'.00','') AS ONEHUNDREADPIQ3 ,replace(convert(varchar,cast(ONEHUNDREADPIQ4 AS money ),1),'.00','') AS ONEHUNDREADPIQ4 ,replace(convert(varchar,cast(ONEHUNDREADPIYTD AS money ),1),'.00','') AS ONEHUNDREADPIYTD ,
									 replace(convert(varchar,cast(ONEHUNDREADP8YOLDYEAR AS money ),1),'.00','') AS ONEHUNDREADP8YOLDYEAR ,replace(convert(varchar,cast(ONEHUNDREAD8YQ1 AS money ),1),'.00','') AS ONEHUNDREAD8YQ1 ,replace(convert(varchar,cast(ONEHUNDREADP8YQ2 AS money ),1),'.00','') AS ONEHUNDREADP8YQ2 ,replace(convert(varchar,cast(ONEHUNDREADP8YQ3 AS money ),1),'.00','') AS ONEHUNDREADP8YQ3 ,replace(convert(varchar,cast(ONEHUNDREADP8YQ4 AS money ),1),'.00','') AS ONEHUNDREADP8YQ4 ,replace(convert(varchar,cast(ONEHUNDREADP8YYTD AS money ),1),'.00','') AS ONEHUNDREADP8YYTD  FROM #ResultTable
	DROP TABLE #ResultTable
	
END
