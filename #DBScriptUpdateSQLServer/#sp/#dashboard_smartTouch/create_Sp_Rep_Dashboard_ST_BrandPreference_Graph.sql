USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_ST_BrandPreference_Graph]    Script Date: 10/01/2013 21:54:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'Sp_Rep_Dashboard_ST_BrandPreference_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_ST_BrandPreference_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_ST_BrandPreference_Graph]
	@YYYY AS INT
AS
BEGIN

	DECLARE @BLACKYTD BIGINT,@SMYTD BIGINT,@REDYTD BIGINT,@DBYTD BIGINT,@CBYTD BIGINT,@OTYTD BIGINT,@NVYTD BIGINT
	DECLARE @ResultTable TABLE (Name varchar(20),Summary bigint) 

	SELECT 
	@BLACKYTD = ISNULL(COUNT(DISTINCT(brandPreJwBlack)), 0),
	@SMYTD = ISNULL(COUNT(DISTINCT(brandPreSmirnoff)), 0), 
	@REDYTD = ISNULL(COUNT(DISTINCT(brandPreJwRed)), 0), 
	@DBYTD = ISNULL(COUNT(DISTINCT(diageoBrandPreOthers)), 0), 
	@CBYTD = ISNULL(COUNT(DISTINCT(brandPreCompetitors)), 0), 
	@OTYTD = ISNULL(COUNT(DISTINCT(brandPreOthers)), 0), 
	@NVYTD = ISNULL(COUNT(DISTINCT(
		CASE WHEN (
			brandPreJwBlack IS NULL
			AND brandPreJwRed IS NULL
			AND brandPreSmirnoff IS NULL
			AND diageoBrandPreOthers IS NULL
			AND brandPreCompetitors IS NULL
			AND brandPreOthers IS NULL
			) THEN consumerId 
			ELSE NULL 
		END)), 0)
	FROM vw_rpt_dashb_smart_by_quarter
	WHERE upd_quarterYear <= @YYYY
	AND no_all IS NULL
	
	--insert 
	INSERT INTO @ResultTable
	VALUES ('JW Black Label',@BLACKYTD),
		('Smirnoff',@SMYTD),
		('JW Red Label',@REDYTD),
		('Other Diageo Brands',@DBYTD),
		('Competitor Brands',@CBYTD),
		('Other',@OTYTD),
		('No Value',@NVYTD)


	SELECT Name, REPLACE(CONVERT(VARCHAR, CAST(Summary AS MONEY), 1),'.00', '') AS Summary 
	FROM @ResultTable
	
END
