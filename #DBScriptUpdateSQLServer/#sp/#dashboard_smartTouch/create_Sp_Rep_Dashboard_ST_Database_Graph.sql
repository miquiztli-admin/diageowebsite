USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_ST_Database_Graph]    Script Date: 10/01/2013 22:25:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_ST_Database_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_ST_Database_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_ST_Database_Graph]
	@YYYY AS INT
AS
BEGIN

	DECLARE @DVOLDYEAR BIGINT,@DVQ1 BIGINT,@DVQ2 BIGINT,@DVQ3 BIGINT,@DVQ4 BIGINT,@DVYTD BIGINT
	DECLARE @CMOLDYEAR BIGINT,@CMQ1 BIGINT,@CMQ2 BIGINT,@CMQ3 BIGINT,@CMQ4 BIGINT,@CMYTD BIGINT

	DECLARE @cCursor CURSOR
	DECLARE @tmpQuarterNum int,@tmpDBVol bigint,@tmpContactable bigint
	
	SELECT @DVOLDYEAR = ISNULL(COUNT(DISTINCT(consumerId)), 0), 
	@CMOLDYEAR = ISNULL(COUNT(DISTINCT(CASE WHEN no_all IS NULL THEN consumerId ELSE NULL END)), 0)
	FROM vw_rpt_dashb_smart_by_quarter
	WHERE upd_quarterYear < @YYYY
	AND diageoBrandPre IS NOT NULL
		
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT upd_quarterNum, 
		ISNULL(COUNT(DISTINCT(consumerId)), 0), 
		ISNULL(COUNT(DISTINCT(CASE WHEN no_all IS NULL THEN consumerId ELSE NULL END)), 0)
		FROM vw_rpt_dashb_smart_by_quarter
		WHERE upd_quarterYear = @YYYY
		AND diageoBrandPre IS NOT NULL
		GROUP BY upd_quarterNum
		ORDER BY upd_quarterNum ASC
		
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpDBVol, @tmpContactable
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
		BEGIN
			SET @DVQ1 = @tmpDBVol
			SET @CMQ1 = @tmpContactable
		END
		ELSE IF (@tmpQuarterNum = 2)
		BEGIN
			SET @DVQ2 = @tmpDBVol
			SET @CMQ2 = @tmpContactable
		END
		ELSE IF (@tmpQuarterNum = 3)
		BEGIN
			SET @DVQ3 = @tmpDBVol
			SET @CMQ3 = @tmpContactable
		END
		ELSE IF (@tmpQuarterNum = 4)
		BEGIN
			SET @DVQ4 = @tmpDBVol
			SET @CMQ4 = @tmpContactable
		END

		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpDBVol, @tmpContactable
	END

	CLOSE @cCursor

	SET @DVQ1 = ISNULL(@DVOLDYEAR, 0) + ISNULL(@DVQ1, 0)
	SET @DVQ2 = ISNULL(@DVQ1, 0) + ISNULL(@DVQ2, 0)
	SET @DVQ3 = ISNULL(@DVQ2, 0) + ISNULL(@DVQ3, 0)
	SET @DVQ4 = ISNULL(@DVQ3, 0) + ISNULL(@DVQ4, 0)
	
	SET @CMQ1 = ISNULL(@CMOLDYEAR, 0) + ISNULL(@CMQ1, 0)
	SET @CMQ2 = ISNULL(@CMQ1, 0) + ISNULL(@CMQ2, 0)
	SET @CMQ3 = ISNULL(@CMQ2, 0) + ISNULL(@CMQ3, 0)
	SET @CMQ4 = ISNULL(@CMQ3, 0) + ISNULL(@CMQ4, 0)
	
	SET @DVYTD = ISNULL(@DVQ4, 0) 
	SET @CMYTD = ISNULL(@CMQ4, 0) 

	SELECT tmp.period, 
	ISNULL(rpt.DatabaseVolume, 0) AS DatabaseVolume,
	ISNULL(rpt.ContactableMass, 0) AS ContactableMass,
	ISNULL(rpt.HVCContactable, 0) AS HVCContactable
	FROM tbl_m_rpt_dashb_templ_graph tmp
		LEFT OUTER JOIN (
			SELECT 1 AS tmp_num,
			ISNULL(@DVQ1, 0) AS DatabaseVolume, 
			ISNULL(@CMQ1, 0) AS ContactableMass, 
			0 AS HVCContactable
			
			UNION ALL
			SELECT 2 AS tmp_num,
			ISNULL(@DVQ2, 0) AS DatabaseVolume, 
			ISNULL(@CMQ2, 0) AS ContactableMass, 
			0 AS HVCContactable
			
			UNION ALL
			SELECT 3 AS tmp_num,
			ISNULL(@DVQ3, 0) AS DatabaseVolume, 
			ISNULL(@CMQ3, 0) AS ContactableMass, 
			0 AS HVCContactable

			UNION ALL
			SELECT 4 AS tmp_num,
			ISNULL(@DVQ4, 0) AS DatabaseVolume, 
			ISNULL(@CMQ4, 0) AS ContactableMass, 
			0 AS HVCContactable

			UNION ALL
			SELECT 5 AS tmp_num,
			ISNULL(@DVYTD, 0) AS DatabaseVolume, 
			ISNULL(@CMYTD, 0) AS ContactableMass, 
			0 AS HVCContactable
			
			UNION ALL
			SELECT 6 AS tmp_num, 0 AS DatabaseVolume, 0 AS ContactableMass, 0 AS HVCContactable

		) rpt ON tmp.num = rpt.tmp_num 
	
END
