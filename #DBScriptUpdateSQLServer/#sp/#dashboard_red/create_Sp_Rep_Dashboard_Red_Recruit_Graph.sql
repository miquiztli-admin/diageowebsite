USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_Red_Recruit_Graph]    Script Date: 10/03/2013 14:02:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_Red_Recruit_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_Red_Recruit_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <03/10/2013>
-- Update date: <13/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_Red_Recruit_Graph](@YYYY as integer)
AS
BEGIN

	DECLARE @ResultTable TABLE (period varchar(20),category varchar(50),subscribe bigint,response4A bigint,promotion bigint,NewRecruit bigint)

	Declare @newQ1 bigint = 0,@newQ2 bigint = 0,@newQ3 bigint = 0,@newQ4 bigint = 0
	Declare @subscribeBY bigint = 0,@subscribeQ1 bigint = 0,@subscribeQ2 bigint = 0,@subscribeQ3 bigint = 0,@subscribeQ4 bigint = 0
	Declare @4AResponsBY bigint = 0,@4AResponsQ1 bigint = 0,@4AResponsQ2 bigint = 0,@4AResponsQ3 bigint = 0,@4AResponsQ4 bigint = 0
	Declare @PromotionBY bigint = 0,@PromotionQ1 bigint = 0,@PromotionQ2 bigint = 0,@PromotionQ3 bigint = 0,@PromotionQ4 bigint = 0
	Declare @YTDNewRecruit bigint,@YTDSubscribe bigint,@YTDResponse bigint,@YTDPromotion bigint
	Declare @YearTargetNewRecruit bigint,@YearTargetSubscribe bigint,@YearTargetResponse bigint,@YearTargetPromotion bigint

	DECLARE @cCursor CURSOR
	DECLARE @tmpQuarterNum int, @tmpVal1 bigint, @tmpVal2 bigint
	DECLARE @tmpFilter int, @tmpValue bigint
	
	DECLARE @_Q1 INT, @_Q2 INT, @_Q3 INT, @_Q4 INT
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarter,
		CASE
			WHEN MIN(DATEADD(MONTH, month_num - 1, CONVERT(DATE, '01-01-' + CAST(@YYYY + year_shift AS CHAR(4))))) > GETDATE() THEN 0
			ELSE 1
		END
		FROM tbl_m_rpt_quarter_month t
		GROUP BY t.quarter
	
	OPEN @cCursor
	FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpFilter = 1)
			SET @_Q1 = @tmpValue
		ELSE IF (@tmpFilter = 2)
			SET @_Q2 = @tmpValue
		ELSE IF (@tmpFilter = 3)
			SET @_Q3 = @tmpValue
		ELSE IF (@tmpFilter = 4)
			SET @_Q4 = @tmpValue
		
		FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue
	END
	CLOSE @cCursor
	
	SELECT 
	@newQ1 = COUNT(DISTINCT(CASE WHEN cre_quarterNum = 1 THEN consumerId ELSE NULL END)),
	@newQ2 = COUNT(DISTINCT(CASE WHEN cre_quarterNum = 2 THEN consumerId ELSE NULL END)),
	@newQ3 = COUNT(DISTINCT(CASE WHEN cre_quarterNum = 3 THEN consumerId ELSE NULL END)),
	@newQ4 = COUNT(DISTINCT(CASE WHEN cre_quarterNum = 4 THEN consumerId ELSE NULL END))
	FROM vw_rpt_dashboardConsumer_by_quarter
	WHERE cre_quarterYear = @YYYY
	AND brandPreJwRed ='T'

	SELECT
	@subscribeBY = ISNULL(COUNT(DISTINCT(subscribe)), 0),
	@4AResponsBY = ISNULL(COUNT(DISTINCT(CASE WHEN (survey4aJwRed IS NOT NULL AND survey4aJwRed != '') THEN consumerId ELSE NULL END)), 0) -- 4A Response
	FROM vw_rpt_dashboardConsumer_by_quarter
	WHERE upd_quarterYear < @YYYY
	AND brandPreJwRed ='T'
	
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT upd_quarterNum, 
		ISNULL(COUNT(DISTINCT(subscribe)), 0),
		ISNULL(COUNT(DISTINCT(CASE WHEN (survey4aJwRed IS NOT NULL AND survey4aJwRed != '') THEN consumerId ELSE NULL END)), 0) -- 4A Response
		FROM vw_rpt_dashboardConsumer_by_quarter
		WHERE upd_quarterYear = @YYYY
		AND brandPreJwRed ='T'
		GROUP BY upd_quarterNum
		ORDER BY upd_quarterNum ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpVal1, @tmpVal2
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
		BEGIN
			SET @subscribeQ1 = @tmpVal1
			SET @4AResponsQ1 = @tmpVal2
		END
		ELSE IF (@tmpQuarterNum = 2)
		BEGIN
			SET @subscribeQ2 = @tmpVal1
			SET @4AResponsQ2 = @tmpVal2
		END
		ELSE IF (@tmpQuarterNum = 3)
		BEGIN
			SET @subscribeQ3 = @tmpVal1
			SET @4AResponsQ3 = @tmpVal2
		END
		ELSE IF (@tmpQuarterNum = 4)
		BEGIN
			SET @subscribeQ4 = @tmpVal1
			SET @4AResponsQ4 = @tmpVal2
		END
		
		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpVal1, @tmpVal2
	END
	
	CLOSE @cCursor

--Promotion Participation
	SELECT 
	@PromotionBY = COUNT(DISTINCT(redeem_consumer))
	FROM vw_rpt_dashboard_promotion_by_quarter
	WHERE quarterYear < @YYYY

	SELECT 
	@PromotionQ1 = COUNT(DISTINCT(CASE WHEN quarterNum = 1 THEN redeem_consumer ELSE NULL END)),
	@PromotionQ2 = COUNT(DISTINCT(CASE WHEN quarterNum = 2 THEN redeem_consumer ELSE NULL END)),
	@PromotionQ3 = COUNT(DISTINCT(CASE WHEN quarterNum = 3 THEN redeem_consumer ELSE NULL END)),
	@PromotionQ4 = COUNT(DISTINCT(CASE WHEN quarterNum = 4 THEN redeem_consumer ELSE NULL END))
	FROM vw_rpt_dashboard_promotion_by_quarter
	WHERE quarterYear = @YYYY
	GROUP BY quarterNum
	ORDER BY quarterNum ASC

--SUM value
	SET @subscribeQ1 = ISNULL(@subscribeBY, 0) + ISNULL(@subscribeQ1, 0)
	SET @subscribeQ2 = ISNULL(@subscribeQ1, 0) + ISNULL(@subscribeQ2, 0)
	SET @subscribeQ3 = ISNULL(@subscribeQ2, 0) + ISNULL(@subscribeQ3, 0)
	SET @subscribeQ4 = ISNULL(@subscribeQ3, 0) + ISNULL(@subscribeQ4, 0)

	SET @4AResponsQ1 = ISNULL(@4AResponsBY, 0) + ISNULL(@4AResponsQ1, 0)
	SET @4AResponsQ2 = ISNULL(@4AResponsQ1, 0) + ISNULL(@4AResponsQ2, 0)
	SET @4AResponsQ3 = ISNULL(@4AResponsQ2, 0) + ISNULL(@4AResponsQ3, 0)
	SET @4AResponsQ4 = ISNULL(@4AResponsQ3, 0) + ISNULL(@4AResponsQ4, 0)
	
	SET @PromotionQ1 = ISNULL(@PromotionBY, 0) + ISNULL(@PromotionQ1, 0)
	SET @PromotionQ2 = ISNULL(@PromotionQ1, 0) + ISNULL(@PromotionQ2, 0)
	SET @PromotionQ3 = ISNULL(@PromotionQ2, 0) + ISNULL(@PromotionQ3, 0)
	SET @PromotionQ4 = ISNULL(@PromotionQ3, 0) + ISNULL(@PromotionQ4, 0)

	--YTD 
	Begin
		set @YTDNewRecruit = ISNULL(@newQ1, 0) + ISNULL(@newQ2, 0) + ISNULL(@newQ3, 0) + ISNULL(@newQ4, 0)
		set @YTDSubscribe = ISNULL(@subscribeQ4, 0)
		set @YTDResponse = ISNULL(@4AResponsQ4, 0)
		set @YTDPromotion = ISNULL(@PromotionQ4, 0)
	End

	--Year Target
	Begin
		set @YearTargetNewRecruit = 0
		set @YearTargetSubscribe = 0
		set @YearTargetResponse = 0
		set @YearTargetPromotion = 0
	End

	IF @_Q1 = 1
		INSERT INTO @ResultTable VALUES ('Q1','',@subscribeQ1,@4AResponsQ1,@PromotionQ1,@newQ1)
	ELSE
		INSERT INTO @ResultTable VALUES ('Q1','', NULL, NULL, NULL, NULL)

	IF @_Q2 = 1
		INSERT INTO @ResultTable VALUES ('Q2','',@subscribeQ2,@4AResponsQ2,@PromotionQ2,@newQ2)
	ELSE
		INSERT INTO @ResultTable VALUES ('Q2','', NULL, NULL, NULL, NULL)
	
	IF @_Q3 = 1
		INSERT INTO @ResultTable VALUES ('Q3','',@subscribeQ3,@4AResponsQ3,@PromotionQ3,@newQ3)
	ELSE
		INSERT INTO @ResultTable VALUES ('Q3','', NULL, NULL, NULL, NULL)
	
	IF @_Q4 = 1
		INSERT INTO @ResultTable VALUES ('Q4','',@subscribeQ4,@4AResponsQ4,@PromotionQ4,@newQ4)
	ELSE
		INSERT INTO @ResultTable VALUES ('Q4','', NULL, NULL, NULL, NULL)

	INSERT INTO @ResultTable VALUES ('YTD','',@YTDSubscribe,@YTDResponse,@YTDPromotion,@YTDNewRecruit)
	INSERT INTO @ResultTable VALUES ('Target','',@YearTargetSubscribe,@YearTargetResponse,@YearTargetPromotion,@YearTargetNewRecruit)

	SELECT * FROM @ResultTable

END
