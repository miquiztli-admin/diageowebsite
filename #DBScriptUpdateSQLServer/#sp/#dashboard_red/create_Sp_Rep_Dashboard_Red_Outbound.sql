USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_Red_Outbound]    Script Date: 10/01/2013 14:57:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_Red_Outbound', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_Red_Outbound;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <1/10/2013>
-- Update date: <12/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_Red_Outbound](@YYYY as integer)
AS
BEGIN

	Declare @edmQ1 bigint,@edmQ2 bigint,@edmQ3 bigint,@edmQ4 bigint
	Declare @smsQ1 bigint,@smsQ2 bigint,@smsQ3 bigint,@smsQ4 bigint
	Declare @ccQ1 bigint,@ccQ2 bigint,@ccQ3 bigint,@ccQ4 bigint
	Declare @edmDelQ1 bigint,@edmDelQ2 bigint,@edmDelQ3 bigint,@edmDelQ4 bigint
	Declare @edmOpenQ1 bigint,@edmOpenQ2 bigint,@edmOpenQ3 bigint,@edmOpenQ4 bigint
	Declare @edmClickQ1 bigint,@edmClickQ2 bigint,@edmClickQ3 bigint,@edmClickQ4 bigint
	Declare @smsResponseQ1 bigint,@smsResponseQ2 bigint,@smsResponseQ3 bigint,@smsResponseQ4 bigint
	
	DECLARE @cCursor CURSOR
	DECLARE @tmpFilter int, @tmpValue bigint, @tmpVal2 bigint, @tmpVal3 bigint, @tmpVal4 bigint, @tmpVal5 bigint, @tmpVal6 bigint, @tmpVal7 bigint
	
	DECLARE @_Q1 INT, @_Q2 INT, @_Q3 INT, @_Q4 INT
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarter,
		CASE
			WHEN MIN(DATEADD(MONTH, month_num - 1, CONVERT(DATE, '01-01-' + CAST(@YYYY + year_shift AS CHAR(4))))) > GETDATE() THEN 0
			ELSE 1
		END
		FROM tbl_m_rpt_quarter_month t
		GROUP BY t.quarter
	
	OPEN @cCursor
	FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpFilter = 1)
			SET @_Q1 = @tmpValue
		ELSE IF (@tmpFilter = 2)
			SET @_Q2 = @tmpValue
		ELSE IF (@tmpFilter = 3)
			SET @_Q3 = @tmpValue
		ELSE IF (@tmpFilter = 4)
			SET @_Q4 = @tmpValue
		
		FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue
	END
	CLOSE @cCursor
	
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarterNum,
		ISNULL(COUNT(DISTINCT(EDM)), 0),
		ISNULL(COUNT(DISTINCT(SMS)), 0),
		ISNULL(COUNT(DISTINCT(CC)), 0),
		ISNULL(COUNT(DISTINCT(EDMDeliver)), 0),
		ISNULL(COUNT(DISTINCT(EDMOpen)), 0),
		ISNULL(COUNT(DISTINCT(EDMClickLink)), 0),
		ISNULL(COUNT(DISTINCT(SMSDeliver)), 0)
		FROM vw_rpt_outboundEvent_by_quarter
		WHERE quarterYear = @YYYY
		AND brandPreJwRed = 'T'
		AND outboundTypeID IN (1, 2)
		GROUP BY quarterNum
		
	OPEN @cCursor
	FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue, @tmpVal2, @tmpVal3, @tmpVal4, @tmpVal5, @tmpVal6, @tmpVal7
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpFilter = 1)
		BEGIN
			SET @edmQ1 = @tmpValue
			SET @smsQ1 = @tmpVal2
			SET @ccQ1 = @tmpVal3
			SET @edmDelQ1 = @tmpVal4
			SET @edmOpenQ1 = @tmpVal5
			SET @edmClickQ1 = @tmpVal6
			SET @smsResponseQ1 = @tmpVal7
		END
		ELSE IF (@tmpFilter = 2)
		BEGIN
			SET @edmQ2 = @tmpValue
			SET @smsQ2 = @tmpVal2
			SET @ccQ2 = @tmpVal3
			SET @edmDelQ2 = @tmpVal4
			SET @edmOpenQ2 = @tmpVal5
			SET @edmClickQ2 = @tmpVal6
			SET @smsResponseQ2 = @tmpVal7
		END
		ELSE IF (@tmpFilter = 3)
		BEGIN
			SET @edmQ3 = @tmpValue
			SET @smsQ3 = @tmpVal2
			SET @ccQ3 = @tmpVal3
			SET @edmDelQ3 = @tmpVal4
			SET @edmOpenQ3 = @tmpVal5
			SET @edmClickQ3 = @tmpVal6
			SET @smsResponseQ3 = @tmpVal7
		END
		ELSE IF (@tmpFilter = 4)
		BEGIN
			SET @edmQ4 = @tmpValue
			SET @smsQ4 = @tmpVal2
			SET @ccQ4 = @tmpVal3
			SET @edmDelQ4 = @tmpVal4
			SET @edmOpenQ4 = @tmpVal5
			SET @edmClickQ4 = @tmpVal6
			SET @smsResponseQ4 = @tmpVal7
		END
		
		FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue, @tmpVal2, @tmpVal3, @tmpVal4, @tmpVal5, @tmpVal6, @tmpVal7
	END
	CLOSE @cCursor
	
	SELECT 
	CASE WHEN @_Q1 = 1 THEN ISNULL(@edmQ1, 0) ELSE NULL END AS edmQ1,
	CASE WHEN @_Q2 = 1 THEN ISNULL(@edmQ2, 0) ELSE NULL END AS edmQ2,
	CASE WHEN @_Q3 = 1 THEN ISNULL(@edmQ3, 0) ELSE NULL END edmQ3,
	CASE WHEN @_Q4 = 1 THEN ISNULL(@edmQ4, 0) ELSE NULL END edmQ4,
	CASE WHEN @_Q1 = 1 THEN ISNULL(@smsQ1, 0) ELSE NULL END AS smsQ1,
	CASE WHEN @_Q2 = 1 THEN ISNULL(@smsQ2, 0) ELSE NULL END AS smsQ2,
	CASE WHEN @_Q3 = 1 THEN ISNULL(@smsQ3, 0) ELSE NULL END AS smsQ3,
	CASE WHEN @_Q4 = 1 THEN ISNULL(@smsQ4, 0) ELSE NULL END AS smsQ4,
	CASE WHEN @_Q1 = 1 THEN ISNULL(@ccQ1, 0) ELSE NULL END AS ccQ1,
	CASE WHEN @_Q2 = 1 THEN ISNULL(@ccQ2, 0) ELSE NULL END AS ccQ2,
	CASE WHEN @_Q3 = 1 THEN ISNULL(@ccQ3, 0) ELSE NULL END AS ccQ3,
	CASE WHEN @_Q4 = 1 THEN ISNULL(@ccQ4, 0) ELSE NULL END AS ccQ4,
	CASE WHEN @_Q1 = 1 THEN ISNULL(@edmDelQ1, 0) ELSE NULL END AS edmDelQ1,
	CASE WHEN @_Q2 = 1 THEN ISNULL(@edmDelQ2, 0) ELSE NULL END AS edmDelQ2,
	CASE WHEN @_Q3 = 1 THEN ISNULL(@edmDelQ3, 0) ELSE NULL END AS edmDelQ3,
	CASE WHEN @_Q4 = 1 THEN ISNULL(@edmDelQ4, 0) ELSE NULL END AS edmDelQ4,
	CASE WHEN @_Q1 = 1 THEN ISNULL(@edmOpenQ1, 0) ELSE NULL END AS edmOpenQ1,
	CASE WHEN @_Q2 = 1 THEN ISNULL(@edmOpenQ2, 0) ELSE NULL END AS edmOpenQ2,
	CASE WHEN @_Q3 = 1 THEN ISNULL(@edmOpenQ3, 0) ELSE NULL END AS edmOpenQ3,
	CASE WHEN @_Q4 = 1 THEN ISNULL(@edmOpenQ4, 0) ELSE NULL END AS edmOpenQ4,
	CASE WHEN @_Q1 = 1 THEN ISNULL(@edmClickQ1, 0) ELSE NULL END AS edmClickQ1,
	CASE WHEN @_Q2 = 1 THEN ISNULL(@edmClickQ2, 0) ELSE NULL END AS edmClickQ2,
	CASE WHEN @_Q3 = 1 THEN ISNULL(@edmClickQ3, 0) ELSE NULL END AS edmClickQ3,
	CASE WHEN @_Q4 = 1 THEN ISNULL(@edmClickQ4, 0) ELSE NULL END AS edmClickQ4,
	CASE WHEN @_Q1 = 1 THEN ISNULL(@smsResponseQ1, 0) ELSE NULL END AS smsResponseQ1,
	CASE WHEN @_Q2 = 1 THEN ISNULL(@smsResponseQ2, 0) ELSE NULL END AS smsResponseQ2,
	CASE WHEN @_Q3 = 1 THEN ISNULL(@smsResponseQ3, 0) ELSE NULL END AS smsResponseQ3,
	CASE WHEN @_Q4 = 1 THEN ISNULL(@smsResponseQ4, 0) ELSE NULL END AS smsResponseQ4

END
