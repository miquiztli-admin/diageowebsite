USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_Red_DB_Graph_Database]    Script Date: 10/01/2013 12:45:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_Red_DB_Graph_Database', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_Red_DB_Graph_Database;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Update date: <13/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_Red_DB_Graph_Database](@YYYY as integer)
AS
BEGIN

	DECLARE @DBVolumeBYear bigint = 0,@DBQ1 bigint = 0,@DBQ2 bigint = 0,@DBQ3 bigint = 0,@DBQ4 bigint = 0,@DBYTD bigint = 0
	DECLARE @ContactableBYear bigint = 0, @ContactableQ1 bigint = 0,@ContactableQ2 bigint = 0,@ContactableQ3 bigint = 0,@ContactableQ4 bigint = 0,@ContactableYTD bigint = 0
	DECLARE @HVCContactableBY bigint = 0,@HVCContactableQ1 bigint = 0,@HVCContactableQ2 bigint = 0,@HVCContactableQ3 bigint = 0,@HVCContactableQ4 bigint = 0,@HVCContactableYTD bigint = 0
	DECLARE @cCursor CURSOR
	DECLARE @tmpQuarterNum int,@tmpDBVol bigint,@tmpContactable bigint,@tmpHVCContactable bigint
	
	DECLARE @ResultTable TABLE (period varchar(20), DatabaseVolume bigint, ContactableMass bigint, HVCContactable bigint)

	DECLARE @tmpFilter int, @tmpValue bigint
	DECLARE @_Q1 INT, @_Q2 INT, @_Q3 INT, @_Q4 INT
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarter,
		CASE
			WHEN MIN(DATEADD(MONTH, month_num - 1, CONVERT(DATE, '01-01-' + CAST(@YYYY + year_shift AS CHAR(4))))) > GETDATE() THEN 0
			ELSE 1
		END
		FROM tbl_m_rpt_quarter_month t
		GROUP BY t.quarter
	
	OPEN @cCursor
	FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpFilter = 1)
			SET @_Q1 = @tmpValue
		ELSE IF (@tmpFilter = 2)
			SET @_Q2 = @tmpValue
		ELSE IF (@tmpFilter = 3)
			SET @_Q3 = @tmpValue
		ELSE IF (@tmpFilter = 4)
			SET @_Q4 = @tmpValue
		
		FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue
	END
	CLOSE @cCursor

	SELECT @DBVolumeBYear = ISNULL(COUNT(DISTINCT(consumerId)), 0), 
	@ContactableBYear = ISNULL(COUNT(DISTINCT(CASE WHEN no_all IS NULL THEN consumerId ELSE NULL END)), 0),
	@HVCContactableBY = ISNULL(COUNT(DISTINCT(
		CASE survey4aJwRed
			WHEN 1 THEN consumerID --Adorer
			WHEN 2 THEN consumerID --Adopter
			ELSE NULL
		END)), 0)
	FROM vw_rpt_dashboardConsumer_by_quarter
	WHERE upd_quarterYear < @YYYY
	AND (brandPreJwRed ='T') -- OR (survey4aJwRed IS NOT NULL AND len(survey4aJwRed) > 0)
	
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT upd_quarterNum, 
		ISNULL(COUNT(DISTINCT(consumerId)), 0), 
		ISNULL(COUNT(DISTINCT(CASE WHEN no_all IS NULL THEN consumerId ELSE NULL END)), 0),
		ISNULL(COUNT(DISTINCT(
			CASE survey4aJwRed
				WHEN 1 THEN consumerID --Adorer
				WHEN 2 THEN consumerID --Adopter
				ELSE NULL
			END)), 0)
		FROM vw_rpt_dashboardConsumer_by_quarter
		WHERE upd_quarterYear = @YYYY
		AND (brandPreJwRed ='T') -- OR (survey4aJwRed IS NOT NULL /*AND len(survey4aJwRed) > 0*/))
		GROUP BY upd_quarterNum
		ORDER BY upd_quarterNum ASC
		
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpDBVol, @tmpContactable, @tmpHVCContactable
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
		BEGIN
			SET @DBQ1 = @tmpDBVol
			SET @ContactableQ1 = @tmpContactable
			SET @HVCContactableQ1 = @tmpHVCContactable
		END
		ELSE IF (@tmpQuarterNum = 2)
		BEGIN
			SET @DBQ2 = @tmpDBVol
			SET @ContactableQ2 = @tmpContactable
			SET @HVCContactableQ2 = @tmpHVCContactable
		END
		ELSE IF (@tmpQuarterNum = 3)
		BEGIN
			SET @DBQ3 = @tmpDBVol
			SET @ContactableQ3 = @tmpContactable
			SET @HVCContactableQ3 = @tmpHVCContactable
		END
		ELSE IF (@tmpQuarterNum = 4)
		BEGIN
			SET @DBQ4 = @tmpDBVol
			SET @ContactableQ4 = @tmpContactable
			SET @HVCContactableQ4 = @tmpHVCContactable
		END

		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpDBVol, @tmpContactable, @tmpHVCContactable
	END
	CLOSE @cCursor

	SET @DBQ1 = ISNULL(@DBVolumeBYear, 0) + ISNULL(@DBQ1, 0)
	SET @DBQ2 = ISNULL(@DBQ1, 0) + ISNULL(@DBQ2, 0)
	SET @DBQ3 = ISNULL(@DBQ2, 0) + ISNULL(@DBQ3, 0)
	SET @DBQ4 = ISNULL(@DBQ3, 0) + ISNULL(@DBQ4, 0)

	SET @ContactableQ1 = ISNULL(@ContactableBYear, 0) + ISNULL(@ContactableQ1, 0)
	SET @ContactableQ2 = ISNULL(@ContactableQ1, 0) + ISNULL(@ContactableQ2, 0)
	SET @ContactableQ3 = ISNULL(@ContactableQ2, 0) + ISNULL(@ContactableQ3, 0)
	SET @ContactableQ4 = ISNULL(@ContactableQ3, 0) + ISNULL(@ContactableQ4, 0)	

	SET @HVCContactableQ1 = ISNULL(@HVCContactableBY, 0) + ISNULL(@HVCContactableQ1, 0)
	SET @HVCContactableQ2 = ISNULL(@HVCContactableQ1, 0) + ISNULL(@HVCContactableQ2, 0)
	SET @HVCContactableQ3 = ISNULL(@HVCContactableQ2, 0) + ISNULL(@HVCContactableQ3, 0)
	SET @HVCContactableQ4 = ISNULL(@HVCContactableQ3, 0) + ISNULL(@HVCContactableQ4, 0)

	SET @DBYTD = ISNULL(@DBQ4, 0)
	SET @ContactableYTD = ISNULL(@ContactableQ4, 0)	
	SET @HVCContactableYTD = ISNULL(@HVCContactableQ4, 0)
	
	IF @_Q1 = 1
		INSERT INTO @ResultTable VALUES ('Q1', @DBQ1, @ContactableQ1, @HVCContactableQ1)
	ELSE
		INSERT INTO @ResultTable VALUES ('Q1', NULL, NULL, NULL)
	
	IF @_Q2 = 1
		INSERT INTO @ResultTable VALUES ('Q2', @DBQ2, @ContactableQ2, @HVCContactableQ2)
	ELSE
		INSERT INTO @ResultTable VALUES ('Q2', NULL, NULL, NULL)

	IF @_Q3 = 1
		INSERT INTO @ResultTable VALUES ('Q3', @DBQ3, @ContactableQ3, @HVCContactableQ3)
	ELSE
		INSERT INTO @ResultTable VALUES ('Q3', NULL, NULL, NULL)

	IF @_Q4 = 1
		INSERT INTO @ResultTable VALUES ('Q4', @DBQ4, @ContactableQ4, @HVCContactableQ4)
	ELSE
		INSERT INTO @ResultTable VALUES ('Q4', NULL, NULL, NULL)

	INSERT INTO @ResultTable VALUES ('YTD', @DBYTD, @ContactableYTD, @HVCContactableYTD)
	INSERT INTO @ResultTable VALUES ('Target', 0, 0, 0)

	SELECT * FROM @ResultTable
	
END
