USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_Red_Outbound_Graph]    Script Date: 10/01/2013 16:35:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_Red_Outbound_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_Red_Outbound_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <1/10/2013>
-- Update date: <12/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_Red_Outbound_Graph](@YYYY as integer)
AS
BEGIN

	Declare @EDMQ1 bigint,@EDMQ2 bigint,@EDMQ3 bigint,@EDMQ4 bigint
	Declare @SMSQ1 bigint,@SMSQ2 bigint,@SMSQ3 bigint,@SMSQ4 bigint

	Declare @YTDEDM bigint,@YTDSMS bigint
	Declare @YearTargetEDM bigint,@YearTargetSMS bigint

	DECLARE @ResultTable TABLE (Period varchar(20), EDM bigint ,SMS bigint)

	INSERT INTO @ResultTable
	SELECT tmp.period AS Period, 
	CASE WHEN QValid = 1 THEN ISNULL(rpt.EDM, 0) ELSE NULL END AS EDM,
	CASE WHEN QValid = 1 THEN ISNULL(rpt.SMS, 0) ELSE NULL END AS SMS
	FROM tbl_m_rpt_dashb_templ_graph tmp
		LEFT OUTER JOIN (
			SELECT quarterNum AS tmp_num,
			COUNT(DISTINCT(
				CASE outboundTypeID
					WHEN 1 THEN consumerId
					ELSE NULL
				END
			)) AS EDM,
			COUNT(DISTINCT(
				CASE outboundTypeID
					WHEN 2 THEN consumerId
					ELSE NULL
				END
			)) AS SMS
			FROM vw_rpt_outboundEvent_by_quarter
			WHERE quarterYear = @YYYY
			AND brandPreJwRed = 'T'
			AND outboundTypeID IN (1, 2)
			GROUP BY quarterNum	
		) rpt ON tmp.num = rpt.tmp_num
		INNER JOIN (
			SELECT quarter,
			CASE
				WHEN MIN(DATEADD(MONTH, month_num - 1, CONVERT(DATE, '01-01-' + CAST(@YYYY + year_shift AS CHAR(4))))) > GETDATE() THEN 0
				ELSE 1
			END AS QValid
			FROM tbl_m_rpt_quarter_month t
			GROUP BY t.quarter
		) AS fil ON tmp.num = fil.quarter 
	WHERE tmp.num < 5
			
	INSERT INTO @ResultTable
	SELECT 'YTD', SUM(EDM), SUM(SMS)
	FROM @ResultTable
	
	SET @YearTargetEDM = 0
	SET @YearTargetSMS = 0
	INSERT INTO @ResultTable
	VALUES('Target',@YearTargetEDM, @YearTargetSMS)
	
	SELECT * FROM @ResultTable

END
