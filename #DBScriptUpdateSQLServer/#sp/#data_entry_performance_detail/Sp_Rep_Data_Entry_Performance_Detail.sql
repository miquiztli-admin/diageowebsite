USE [DiageoDB_Edge]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Data_Entry_Performance_Detail]    Script Date: 11/13/2013 13:53:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Data_Entry_Performance_Detail', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Data_Entry_Performance_Detail;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <12/11/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Data_Entry_Performance_Detail](
	@Startdate1 varchar(max), @Enddate1 varchar(max), @Topic int, @Outlet varchar(max)--, @PG_Input int
)
AS
BEGIN
/*	SELECT p.Staff_Code, p.Staff_Name, p.Outlet_Name, s.daysCount AS actual_Day, p.Engagement,
	p.NewRecruit, p.type_Repeat, ISNULL(s.EngagePerTraffic, 0) AS EngagePerTraffic
	FROM (
		SELECT Staff_Code, Staff_Name, Outlet_Name, SUM(Engagement) AS Engagement,
		SUM(NewType) AS NewRecruit, SUM(RepeatType) AS type_Repeat
		FROM vw_rpt_dataEntryPerformance
		WHERE RepeatTransactionDate BETWEEN @Startdate1 AND @Enddate1
		AND (
			(@Topic = 2 AND DATEPART(DW, RepeatTransactionDate) IN (1,2,3,4,5)) --Week Day
			OR (@Topic = 3 AND DATEPART(DW, RepeatTransactionDate) IN (6,7)) -- Week End
			OR @Topic NOT IN (2,3)
		)
		GROUP BY Staff_Code, Staff_Name, Outlet_Name
	) p	LEFT OUTER JOIN (
			SELECT eventStaff.Staff_Code, eventStaff.Outlet_Name,
			AVG(CASE
				WHEN eventInfo.Count_Staff = 0 THEN 0 
				WHEN (eventInfo.TrafficAmount / eventInfo.Count_Staff ) = 0 THEN 0
				ELSE eventInfo.Engagement / (eventInfo.TrafficAmount / eventInfo.Count_Staff)
			END) AS EngagePerTraffic, COUNT(eventStaff.T_Date) AS daysCount
			/*, AVG(eventInfo.Count_Staff) AS AverageCountStaff
			, AVG(eventInfo.TrafficAmount) AS AverageSumTraffic, AVG(eventInfo.Engagement) AS AverageCountEngage*/
			FROM (
				SELECT DISTINCT Staff_Code, Outlet_Name, CAST(RepeatTransactionDate AS DATE) AS T_Date
				FROM vw_rpt_dataEntryPerformance
				WHERE (RepeatTransactionDate BETWEEN @Startdate1 AND @Enddate1)
				AND (
					(@Topic = 2 AND DATEPART(DW, RepeatTransactionDate) IN (1,2,3,4,5)) --Week Day
					OR (@Topic = 3 AND DATEPART(DW, RepeatTransactionDate) IN (6,7)) -- Week End
					OR @Topic NOT IN (2,3)
				)
			) AS eventStaff INNER JOIN (
					SELECT Outlet_Name, CAST(RepeatTransactionDate AS DATE) AS T_Date, 
					CAST(SUM(RepeatType) AS FLOAT) AS Engagement,
					CAST(SUM(ISNULL(Traffic_Amount, 0)) AS FLOAT) AS TrafficAmount, 
					CAST(COUNT(DISTINCT Staff_Code) AS FLOAT) AS Count_Staff
					FROM vw_rpt_dataEntryPerformance
					WHERE (RepeatTransactionDate BETWEEN @Startdate1 AND @Enddate1)
					AND (
						(@Topic = 2 AND DATEPART(DW, RepeatTransactionDate) IN (1,2,3,4,5)) --Week Day
						OR (@Topic = 3 AND DATEPART(DW, RepeatTransactionDate) IN (6,7)) -- Week End
						OR @Topic NOT IN (2,3)
					)
					GROUP BY Outlet_Name, CAST(RepeatTransactionDate AS DATE)
				) AS eventInfo ON eventInfo.Outlet_Name = eventStaff.Outlet_Name AND eventInfo.T_Date = eventStaff.T_Date
			GROUP BY eventStaff.Staff_Code, eventStaff.Outlet_Name
		) AS s ON p.Outlet_Name = s.Outlet_Name AND p.Staff_Code = s.Staff_Code
	ORDER BY p.Staff_Code, p.Outlet_Name
*/

	SELECT s.Staff_Code, s.Staff_Name, o.Outlet_Name, main.ActualDate AS ActualDate,
	main.Engagement AS Engagement,
	main.Engage_Traffic AS Engage_Traffic,
	main.NewRecruit_Engage AS NewRecruit_Engage,
	main.Repeat_Engage AS Repeat_Engage
	FROM (
		SELECT R.Repeat_Outlet, R.Repeat_CreateBy, COUNT(R.RepeatTransactionDate) AS ActualDate,
		ISNULL(SUM(R.AllType), 0) AS Engagement,
		CASE 
			WHEN SUM(A.AverageTraffic) > 0 THEN
			(CAST(ISNULL(SUM(R.AllType), 0) AS FLOAT) / CAST(SUM(A.AverageTraffic) AS FLOAT))
			ELSE 0
		END
		AS Engage_Traffic,
		CASE 
			WHEN SUM(R.AllType) > 0 THEN
			(CAST(ISNULL(SUM(R.NewType), 0) AS FLOAT) / CAST(SUM(R.AllType) AS FLOAT))
			ELSE 0
		END
		AS NewRecruit_Engage,
		CASE 
			WHEN SUM(R.AllType) > 0 THEN
			(CAST(ISNULL(SUM(R.RepeatType), 0) AS FLOAT) / CAST(SUM(R.AllType) AS FLOAT))
			ELSE 0
		END
		AS Repeat_Engage
		FROM (
			SELECT Repeat_Outlet, Repeat_CreateBy, RepeatTransactionDate,
			SUM(Case when UPPER(Repeat_Type) = 'R' then 1 else 0 end) as RepeatType,
			SUM(Case when UPPER(Repeat_Type) = 'N' then 1 else 0 end) as NewType,
			SUM(Case when Repeat_Type <> '' then 1 else 0 end) as AllType
			FROM 
			(
				SELECT Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, 
				CONVERT(date, Repeat_TransactionDate) as RepeatTransactionDate
				FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
				WHERE Convert(date, [Repeat_TransactionDate]) between convert(date, @Startdate1) and convert(date, @Enddate1)
				GROUP BY Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, Repeat_Consumer, CONVERT(date, Repeat_TransactionDate)
			) R1
			GROUP BY Repeat_Outlet, Repeat_CreateBy, RepeatTransactionDate
		) AS R
		INNER JOIN 	(
			SELECT R.Repeat_Outlet, R.RepeatTransactionDate, 
			CAST(Traffic.Traffic AS FLOAT) / CAST(R.NoPG AS FLOAT) AS AverageTraffic
			FROM (
				SELECT [Traffic_Outlet], SUM([Traffic_Amount]) AS Traffic, 
				CONVERT(DATE, [Traffic_Date]) AS TrafficDate
				FROM [SmartTouchDB].[dbo].[ST_tsTraffic]
				WHERE Convert(date, [Traffic_Date]) between convert(date, @Startdate1) and convert(date, @Enddate1)
				GROUP BY [Traffic_Outlet], CONVERT(DATE, [Traffic_Date])
			) AS Traffic
			INNER JOIN (
				SELECT Repeat_Outlet, RepeatTransactionDate,
				COUNT(DISTINCT(Repeat_CreateBy)) AS NoPG
				FROM 
				(
					SELECT Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, 
					CONVERT(date, Repeat_TransactionDate) as RepeatTransactionDate
					FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
					WHERE Convert(date, [Repeat_TransactionDate]) between convert(date, @Startdate1) and convert(date, @Enddate1)
					GROUP BY Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, Repeat_Consumer, CONVERT(date, Repeat_TransactionDate)
				) R1
				GROUP BY Repeat_Outlet, RepeatTransactionDate
			) R ON R.Repeat_Outlet = Traffic.Traffic_Outlet
				AND R.RepeatTransactionDate = Traffic.TrafficDate
		) AS A ON R.Repeat_Outlet = A.Repeat_Outlet
			AND R.RepeatTransactionDate = A.RepeatTransactionDate
		WHERE (
			(@Topic = 2 AND DATEPART(DW, R.RepeatTransactionDate) IN (1,2,3,4,5)) --Week Day
			OR (@Topic = 3 AND DATEPART(DW, R.RepeatTransactionDate) IN (6,7)) -- Week End
			OR @Topic NOT IN (2,3)
		)
		GROUP BY R.Repeat_Outlet, R.Repeat_CreateBy
	) AS main
	INNER JOIN SmartTouchDB.[dbo].ST_mOutlet o ON main.Repeat_Outlet = o.Outlet_ID
	INNER JOIN SmartTouchDB.[dbo].ST_mStaff s ON main.Repeat_CreateBy = s.Staff_ID
	ORDER BY s.Staff_Code, s.Staff_Name, o.Outlet_Name

END