USE [DiageoDB_Edge]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Total_Commission_By_Data_Entry]    Script Date: 04/24/2014 12:17:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Daily_Data_Entry_Performance', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Daily_Data_Entry_Performance;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <24/4/2014>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Daily_Data_Entry_Performance](
	@StaffIDs varchar(max), @OutletIDs varchar(max),
	@Startdate varchar(max),@Enddate varchar(max),
	@SortBy int, @OrderBy int
)
AS
BEGIN
	Declare @StaffCount integer = 0
	Declare @OutletCount integer = 0
	Declare @Startdate1 varchar(max), @Enddate1 varchar(max)
	SET @Startdate1 = @Startdate
	SET @Enddate1 = @Enddate
	
	DECLARE @QueryStaff TABLE(Staff_ID INT)
	DECLARE @QueryOutlet TABLE(Outlet_ID INT)
	
	DECLARE @separator_position INT -- This is used to locate each separator character
	DECLARE @array_value VARCHAR(50) -- this holds each array value as it is returned
	IF LEFT(@StaffIDs, 1) <> '0'
	BEGIN
		IF RIGHT(LTRIM(RTRIM(@StaffIDs)), 1) <> ',' SET @StaffIDs = LTRIM(RTRIM(@StaffIDs)) + ','
		WHILE PATINDEX('%,%', @StaffIDs) <> 0 
		BEGIN
			SELECT @separator_position = PATINDEX('%,%', @StaffIDs)
			SELECT @array_value = LTRIM(RTRIM(LEFT(@StaffIDs, @separator_position - 1)))
			
			IF LEN(@array_value) > 0
			BEGIN
				INSERT INTO @QueryStaff(Staff_ID) VALUES(@array_value)
			END
			-- replace processed strings with an empty string
			SELECT  @StaffIDs = STUFF(@StaffIDs, 1, @separator_position, '')
		END
	END
	
	SELECT @StaffCount = COUNT(*) FROM @QueryStaff
	
	IF LEFT(@OutletIDs, 1) <> '0'
	BEGIN
		IF RIGHT(LTRIM(RTRIM(@OutletIDs)), 1) <> ',' SET @OutletIDs = LTRIM(RTRIM(@OutletIDs)) + ','
		WHILE PATINDEX('%,%', @OutletIDs) <> 0 
		BEGIN
			SELECT @separator_position = PATINDEX('%,%', @OutletIDs)
			SELECT @array_value = LTRIM(RTRIM(LEFT(@OutletIDs, @separator_position - 1)))
			
			IF LEN(@array_value) > 0
			BEGIN
				INSERT INTO @QueryOutlet(Outlet_ID) VALUES(@array_value)
			END
			-- replace processed strings with an empty string
			SELECT  @OutletIDs = STUFF(@OutletIDs, 1, @separator_position, '')
		END
	END
	
	SELECT @OutletCount = COUNT(*) FROM @QueryOutlet
	
	SELECT q.*,
	CASE 
		WHEN @SortBy = 1 AND @OrderBy = 0 THEN q.PG_Name
		WHEN @SortBy = 2 AND @OrderBy = 0 THEN q.Outlet
		WHEN @SortBy = 3 AND @OrderBy = 0 THEN CONVERT(varchar(20), q.DisplayDate, 120)
		WHEN @SortBy = 4 AND @OrderBy = 0 THEN REPLACE(STR(CAST(q.Daily_Traffic AS DECIMAL(10, 2)), 10), SPACE(1), '0')
		WHEN @SortBy = 5 AND @OrderBy = 0 THEN REPLACE(STR(CAST(q.Daily_Engagement AS DECIMAL(10, 2)), 10), SPACE(1), '0')
		WHEN @SortBy = 6 AND @OrderBy = 0 THEN REPLACE(STR(CAST(q.Daily_New AS DECIMAL(10, 2)), 10), SPACE(1), '0')
		WHEN @SortBy = 7 AND @OrderBy = 0 THEN REPLACE(STR(CAST(q.Daily_Repeat AS DECIMAL(10, 2)), 10), SPACE(1), '0')
		WHEN @SortBy = 8 AND @OrderBy = 0 THEN REPLACE(STR(CAST(q.Engagement_Traffic AS DECIMAL(10, 2)), 10), SPACE(1), '0')
		WHEN @SortBy = 9 AND @OrderBy = 0 THEN REPLACE(STR(CAST(q.New_Engagement AS DECIMAL(10, 2)), 10), SPACE(1), '0')
		ELSE NULL
	END AS sort_asc,
	CASE 
		WHEN @SortBy = 1 AND @OrderBy <> 0 THEN q.PG_Name
		WHEN @SortBy = 2 AND @OrderBy <> 0 THEN q.Outlet
		WHEN @SortBy = 3 AND @OrderBy <> 0 THEN CONVERT(varchar(20), q.DisplayDate, 120)
		WHEN @SortBy = 4 AND @OrderBy <> 0 THEN REPLACE(STR(CAST(q.Daily_Traffic AS DECIMAL(10, 2)), 10), SPACE(1), '0')
		WHEN @SortBy = 5 AND @OrderBy <> 0 THEN REPLACE(STR(CAST(q.Daily_Engagement AS DECIMAL(10, 2)), 10), SPACE(1), '0')
		WHEN @SortBy = 6 AND @OrderBy <> 0 THEN REPLACE(STR(CAST(q.Daily_New AS DECIMAL(10, 2)), 10), SPACE(1), '0')
		WHEN @SortBy = 7 AND @OrderBy <> 0 THEN REPLACE(STR(CAST(q.Daily_Repeat AS DECIMAL(10, 2)), 10), SPACE(1), '0')
		WHEN @SortBy = 8 AND @OrderBy <> 0 THEN REPLACE(STR(CAST(q.Engagement_Traffic AS DECIMAL(10, 2)), 10), SPACE(1), '0')
		WHEN @SortBy = 9 AND @OrderBy <> 0 THEN REPLACE(STR(CAST(q.New_Engagement AS DECIMAL(10, 2)), 10), SPACE(1), '0')
		ELSE NULL
	END AS sort_desc
	FROM (
		SELECT 
		S.Staff_Name AS PG_Name,
		O.Outlet_Name AS Outlet,
		main.RepeatTransactionDate AS DisplayDate,
		ISNULL(main.Traffic, 0) AS Daily_Traffic,
		ISNULL(main.AllType, 0) AS Daily_Engagement,
		ISNULL(main.NewType, 0) AS Daily_New,
		ISNULL(main.RepeatType, 0) AS Daily_Repeat,
		CASE
			WHEN main.Traffic != 0 THEN
				(CAST(ISNULL(main.AllType, 0) AS FLOAT) / CAST(ISNULL(main.Traffic, 1) AS FLOAT))
			ELSE 0
		END AS Engagement_Traffic, 
		CASE 
			WHEN main.Traffic != 0 THEN
				(CAST(ISNULL(main.NewType, 0) AS FLOAT) / CAST(ISNULL(main.AllType, 1) AS FLOAT))
			ELSE 0 
		END AS New_Engagement	
		FROM (
			SELECT R.Repeat_Outlet, R.RepeatTransactionDate, Repeat_CreateBy,
			ISNULL(T.Traffic, 0) AS TrafficAll,
			CAST(ISNULL(T.Traffic, 0) AS FLOAT) / CAST(ISNULL(PG.NoPG, 1) AS FLOAT) AS Traffic, 
			R.AllType, R.RepeatType, R.NewType
			FROM (
				SELECT Repeat_Outlet, RepeatTransactionDate, Repeat_CreateBy, 
				SUM(CASE WHEN UPPER(Repeat_Type) = 'R' then 1 else 0 end) AS RepeatType,
				SUM(CASE WHEN UPPER(Repeat_Type) = 'N' then 1 else 0 end) AS NewType,
				SUM(CASE WHEN Repeat_Type <> '' then 1 else 0 end) AS AllType
				FROM (
					SELECT Repeat_Type, Repeat_Outlet, Repeat_CreateBy, 
					CONVERT(date, Repeat_TransactionDate) AS RepeatTransactionDate
					FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
					WHERE CONVERT(date, Repeat_TransactionDate) between convert(date, @Startdate1) and convert(date, @Enddate1)
					GROUP BY Repeat_Type, Repeat_Outlet, Repeat_CreateBy, Repeat_Consumer, CONVERT(date, Repeat_TransactionDate)
				) R1
				GROUP BY Repeat_Outlet, RepeatTransactionDate, Repeat_CreateBy
			) R LEFT OUTER JOIN (
				SELECT Repeat_Outlet, RepeatTransactionDate,
				COUNT(DISTINCT(Repeat_CreateBy)) AS NoPG
				FROM (
					SELECT Repeat_Type, Repeat_Outlet, Repeat_CreateBy, 
					CONVERT(date, Repeat_TransactionDate) AS RepeatTransactionDate
					FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
					WHERE CONVERT(date, Repeat_TransactionDate) between convert(date, @Startdate1) and convert(date, @Enddate1)
					GROUP BY Repeat_Type, Repeat_Outlet, Repeat_CreateBy, Repeat_Consumer, CONVERT(date, Repeat_TransactionDate)
				) R1
				GROUP BY Repeat_Outlet, RepeatTransactionDate			
			) PG ON PG.Repeat_Outlet = R.Repeat_Outlet
				AND PG.RepeatTransactionDate = R.RepeatTransactionDate
			LEFT OUTER JOIN (
				SELECT Traffic_Outlet, CONVERT(Date, Traffic_Date) AS TrafficDate, 
				SUM(Traffic_Amount) as Traffic
				FROM [SmartTouchDB].[dbo].[ST_tsTraffic]
				WHERE CONVERT(Date, Traffic_Date) between Convert(date, @Startdate1) and Convert(date, @Enddate1)
				GROUP BY Traffic_Outlet, CONVERT(Date, Traffic_Date)
			) AS T ON T.Traffic_Outlet = R.Repeat_Outlet
				AND T.TrafficDate = R.RepeatTransactionDate
			WHERE ISNULL(T.Traffic, 0) > 0
		) main 
			LEFT JOIN [SmartTouchDB].[dbo].[ST_mStaff] S ON S.[Staff_ID] = main.Repeat_CreateBy
			LEFT JOIN [SmartTouchDB].[dbo].[ST_mOutlet] O ON O.[Outlet_ID] = main.Repeat_Outlet
		WHERE S.Staff_Name IS NOT NULL
		AND O.Outlet_Name IS NOT NULL
		AND (@OutletCount = '0' OR main.Repeat_Outlet IN (SELECT Outlet_ID FROM @QueryOutlet))
		AND (@StaffCount = '0' OR main.Repeat_CreateBy IN (SELECT Staff_ID FROM @QueryStaff))
	) AS q
	ORDER BY sort_asc, sort_desc DESC;
	
/*
SET @str = '
SELECT 	CONVERT(DATE,L.Login_Date) AS Dates
		,O.[Outlet_Name] AS Outlet
		,S.[Staff_Code] AS PGCODE
		,S.[Staff_Name] AS PGName
		,ISNULL(RE.NPG,0) AS NoNewPerPG
		,ISNULL(RE.EPG,0) AS NoEngagementPerPG
		,ISNULL(PGT.PGT,0) AS NoTrafficperPG
		,ISNULL(ET.ET,0) AS EngagementTraffic
		,ISNULL(ET.NT,0) AS NewEngagement	
						
FROM [SmartTouchDB].[dbo].[ST_mStaff] S
LEFT JOIN  [SmartTouchDB].[dbo].[ST_tsLogin] L ON [Login_Staff] = [Staff_ID]
LEFT JOIN  [SmartTouchDB].[dbo].[ST_mOutlet] O ON [Outlet_ID] = [Login_Outlet]

LEFT JOIN	(SELECT DISTINCT Repeat_CreateBy
				   ,[Repeat_Outlet]
				   ,CONVERT(DATE,[Repeat_CreateDate])AS RDate
				   ,CONVERT(DATE,[Traffic_Date]) AS TDate
				   ,(SUM(CASE WHEN [Repeat_Type] <> ''''   THEN 1 ELSE 0 END))AS EPG
				   ,(CONVERT(MONEY,(SUM(CASE WHEN [Repeat_Type] <> '''' THEN 1 ELSE 0 END)))/
				    ISNULL(NULLIF(CONVERT(MONEY,[Traffic_Amount]),0),NULL)) 
				   *(CONVERT(MONEY,100)) AS ET
				   ,(CONVERT(MONEY,(SUM(CASE WHEN [Repeat_Type] = ''N'' THEN 1 ELSE 0 END)))/
				   ISNULL(NULLIF(CONVERT(MONEY,[Traffic_Amount]),0),NULL))
				   *(CONVERT(MONEY,100)) AS NT
			FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
			JOIN [SmartTouchDB].[dbo].[ST_tsTraffic] ON [Traffic_Outlet] = [Repeat_Outlet]
			WHERE CONVERT(DATE,[Traffic_Date]) >= Convert(datetime,'''+@Startdate1+''') AND CONVERT(DATE,Traffic_Date) <= Convert(datetime,'''+@Enddate1+''') 
			AND CONVERT(DATE,[Repeat_CreateDate]) >= Convert(datetime,'''+@Startdate1+''') AND CONVERT(DATE,[Repeat_CreateDate]) <= Convert(datetime,'''+@Enddate1+''')
			AND CONVERT(DATE,[Repeat_CreateDate]) = CONVERT(DATE,[Traffic_Date])
			GROUP BY [Repeat_CreateBy]
					,[Repeat_Outlet]
					,[Traffic_Amount]
					,CONVERT(DATE,[Repeat_CreateDate])
					,CONVERT(DATE,[Traffic_Date])
			)ET	ON S.[Staff_ID] = ET.[Repeat_CreateBy]
					AND L.[Login_Outlet] = ET.[Repeat_Outlet]
					--AND CONVERT(DATE,L.Login_Date) = CONVERT(DATE,[Repeat_CreateDate])
					--AND CONVERT(DATE,L.Login_Date) = CONVERT(DATE,[Traffic_Date])
LEFT JOIN  (SELECT [Repeat_Outlet]
					,Repeat_CreateBy
					,(SUM(CASE WHEN [Repeat_Type] = ''N'' THEN 1 ELSE 0 END))AS NPG
					,(SUM(CASE WHEN [Repeat_Type] <> '''' THEN 1 ELSE 0 END))AS EPG
			FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
			WHERE CONVERT(DATE,[Repeat_CreateDate]) >= Convert(datetime,'''+@Startdate1+''') AND CONVERT(DATE,[Repeat_CreateDate]) <= Convert(datetime,'''+@Enddate1+''') 
			GROUP BY [Repeat_Outlet]
					,[Repeat_CreateBy]
			)RE   ON  S.[Staff_ID] = RE.[Repeat_CreateBy]
					AND L.[Login_Outlet] = RE.[Repeat_Outlet]
LEFT JOIN	(SELECT [Traffic_Outlet]
			,SUM([Traffic_Amount]) AS PGT								
			FROM [SmartTouchDB].[dbo].[ST_tsTraffic]
			WHERE CONVERT(DATE,[Traffic_Date]) >= Convert(datetime,'''+@Startdate1+''') AND CONVERT(DATE,Traffic_Date) <= Convert(datetime,'''+@Enddate1+''') 
			GROUP BY [Traffic_Outlet]
			)PGT	ON L.[Login_Outlet] = PGT.[Traffic_Outlet]
					
WHERE CONVERT(DATE,[Login_Date]) >= Convert(datetime,'''+@Startdate1+''') AND CONVERT(DATE,[Login_Date]) <= Convert(datetime,'''+@Enddate1+''')
AND CONVERT(DATE,L.Login_Date) = CONVERT(DATE,ET.RDate)
AND CONVERT(DATE,L.Login_Date) = CONVERT(DATE,ET.TDate)
'+@WhereOutlet+'
--AND [Login_Staff] = @PG_Input
GROUP BY S.[Staff_Code] 
		,S.[Staff_Name]
		,O.[Outlet_Name]
		,CONVERT(DATE,L.Login_Date)
		,RE.EPG
		,RE.NPG
		,PGT.PGT
		,ET.ET
		,ET.NT
ORDER BY CONVERT(DATE,L.Login_Date) ASC
		,S.[Staff_Code] ASC
		,O.[Outlet_Name] ASC
'
	print(@str)
	EXEC(@str)
*/
End
