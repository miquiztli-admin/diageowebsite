USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_Smirnoff_DB_Graph_4A]    Script Date: 09/29/2013 17:22:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_Smirnoff_DB_Graph_4A', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_Smirnoff_DB_Graph_4A;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Update date: <13/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_Smirnoff_DB_Graph_4A](@YYYY as integer)
AS
BEGIN

	SELECT m.disp_name as Name, cq.Summary 
	FROM tbl_m_rpt_survey_type m 
		LEFT OUTER JOIN
			(
				SELECT survey4aSmirnoff, 
				ISNULL(COUNT(DISTINCT(consumerId)), 0) AS Summary
				FROM vw_rpt_dashboardConsumer_by_quarter
				WHERE upd_quarterYear <= @YYYY
				AND brandPreSmirnoff ='T'
				AND no_all IS NULL
				GROUP BY survey4aSmirnoff
			)  cq ON m.survey_val = cq.survey4aSmirnoff
	ORDER BY cq.survey4aSmirnoff ASC

END
