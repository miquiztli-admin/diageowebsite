USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_Smirnoff_DigitalTouchPoint]    Script Date: 10/01/2013 12:58:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_Smirnoff_DigitalTouchPoint', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_Smirnoff_DigitalTouchPoint;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <1/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_Smirnoff_DigitalTouchPoint](@YYYY as integer)
AS
BEGIN
	DECLARE @EDMQ1 BIGINT, @EDMQ2 BIGINT, @EDMQ3 BIGINT, @EDMQ4 BIGINT
	DECLARE @SMSQ1 BIGINT, @SMSQ2 BIGINT, @SMSQ3 BIGINT, @SMSQ4 BIGINT
	DECLARE @cCursor CURSOR
	DECLARE @tmpFilter int, @tmpValue bigint, @tmpVal2 bigint
	
	DECLARE @_Q1 INT, @_Q2 INT, @_Q3 INT, @_Q4 INT
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarter,
		CASE
			WHEN MIN(DATEADD(MONTH, month_num - 1, CONVERT(DATE, '01-01-' + CAST(@YYYY + year_shift AS CHAR(4))))) > GETDATE() THEN 0
			ELSE 1
		END
		FROM tbl_m_rpt_quarter_month t
		GROUP BY t.quarter
	
	OPEN @cCursor
	FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpFilter = 1)
			SET @_Q1 = @tmpValue
		ELSE IF (@tmpFilter = 2)
			SET @_Q2 = @tmpValue
		ELSE IF (@tmpFilter = 3)
			SET @_Q3 = @tmpValue
		ELSE IF (@tmpFilter = 4)
			SET @_Q4 = @tmpValue
		
		FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue
	END
	CLOSE @cCursor
	
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarterNum, OutboundTypeID,
		ISNULL(COUNT(DISTINCT(eventId)), 0)
		FROM vw_rpt_outboundEvent_by_quarter
		WHERE quarterYear = @YYYY
		AND OutboundTypeID IN (1, 2)
		AND brandPreSmirnoff ='T'
		GROUP BY quarterNum, OutboundTypeID
	OPEN @cCursor
	FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue, @tmpVal2
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpFilter = 1 AND @tmpValue = 1)
			SET @EDMQ1 = @tmpVal2
		ELSE IF (@tmpFilter = 2 AND @tmpValue = 1)
			SET @EDMQ2 = @tmpVal2
		ELSE IF (@tmpFilter = 3 AND @tmpValue = 1)
			SET @EDMQ3 = @tmpVal2
		ELSE IF (@tmpFilter = 4 AND @tmpValue = 1)
			SET @EDMQ4 = @tmpVal2
		ELSE IF (@tmpFilter = 1 AND @tmpValue = 2)
			SET @SMSQ1 = @tmpVal2
		ELSE IF (@tmpFilter = 2 AND @tmpValue = 2)
			SET @SMSQ2 = @tmpVal2
		ELSE IF (@tmpFilter = 3 AND @tmpValue = 2)
			SET @SMSQ3 = @tmpVal2
		ELSE IF (@tmpFilter = 4 AND @tmpValue = 2)
			SET @SMSQ4 = @tmpVal2
		
		FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue, @tmpVal2
	END
	CLOSE @cCursor
	
	SELECT 
	CASE WHEN @_Q1 = 1 THEN ISNULL(@EDMQ1, 0) ELSE NULL END AS EDMQ1,
	CASE WHEN @_Q2 = 1 THEN ISNULL(@EDMQ2, 0) ELSE NULL END AS EDMQ2,
	CASE WHEN @_Q3 = 1 THEN ISNULL(@EDMQ3, 0) ELSE NULL END AS EDMQ3,
	CASE WHEN @_Q4 = 1 THEN ISNULL(@EDMQ4, 0) ELSE NULL END AS EDMQ4,
	CASE WHEN @_Q1 = 1 THEN ISNULL(@SMSQ1, 0) ELSE NULL END AS SMSQ1,
	CASE WHEN @_Q2 = 1 THEN ISNULL(@SMSQ2, 0) ELSE NULL END AS SMSQ2,
	CASE WHEN @_Q3 = 1 THEN ISNULL(@SMSQ3, 0) ELSE NULL END AS SMSQ3,
	CASE WHEN @_Q4 = 1 THEN ISNULL(@SMSQ4, 0) ELSE NULL END AS SMSQ4

END
