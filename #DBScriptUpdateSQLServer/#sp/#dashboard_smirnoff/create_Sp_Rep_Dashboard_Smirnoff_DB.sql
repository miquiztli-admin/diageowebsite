USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Dashboard_Smirnoff_DB]    Script Date: 09/28/2013 08:51:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'dbo.Sp_Rep_Dashboard_Smirnoff_DB', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Dashboard_Smirnoff_DB;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <28/9/2013>
-- Update date: <13/10/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Dashboard_Smirnoff_DB](@YYYY as integer)
AS
BEGIN

	Declare @DBVolumeBYear bigint = 0,@DBQ1 bigint = 0,@DBQ2 bigint = 0,@DBQ3 bigint = 0,@DBQ4 bigint = 0,@DBYTD bigint = 0
	Declare @ContactableBYear bigint = 0, @ContactableQ1 bigint = 0,@ContactableQ2 bigint = 0,@ContactableQ3 bigint = 0,@ContactableQ4 bigint = 0,@ContactableYTD bigint = 0
	Declare @EmailBY bigint = 0, @EmailQ1 bigint = 0,@EmailQ2 bigint = 0,@EmailQ3 bigint = 0,@EmailQ4 bigint = 0,@EmailYTD bigint = 0
	Declare @MobileBY bigint = 0,@MobileQ1 bigint = 0,@MobileQ2 bigint = 0,@MobileQ3 bigint = 0,@MobileQ4 bigint = 0,@MobileYTD bigint = 0
	Declare @EmailMobileBY bigint = 0, @EmailMobileQ1 bigint = 0,@EmailMobileQ2 bigint = 0,@EmailMobileQ3 bigint = 0,@EmailMobileQ4 bigint = 0,@EmailMobileYTD bigint = 0
	Declare @AdorerBY bigint = 0, @AdorerQ1 bigint = 0,@AdorerQ2 bigint = 0,@AdorerQ3 bigint = 0, @AdorerQ4 bigint = 0,@AdorerYTD bigint = 0
	Declare @AdopterBY bigint = 0, @AdopterQ1 bigint = 0,@AdopterQ2 bigint = 0,@AdopterQ3 bigint = 0, @AdopterQ4 bigint = 0,@AdopterYTD bigint = 0
	Declare @AvailableBY bigint = 0,@AvailableQ1 bigint = 0,@AvailableQ2 bigint = 0,@AvailableQ3 bigint = 0, @AvailableQ4 bigint = 0,@AvailableYTD bigint = 0
	Declare @AcceptBY bigint = 0,@AcceptQ1 bigint = 0,@AcceptQ2 bigint = 0,@AcceptQ3 bigint = 0, @AcceptQ4 bigint = 0,@AcceptYTD bigint = 0
	Declare @RejectBY bigint = 0,@RejectQ1 bigint = 0,@RejectQ2 bigint = 0,@RejectQ3 bigint = 0, @RejectQ4 bigint = 0,@RejectYTD bigint = 0
	Declare @NABY bigint = 0,@NAQ1 bigint = 0,@NAQ2 bigint = 0,@NAQ3 bigint = 0, @NAQ4 bigint = 0,@NAYTD bigint = 0
	Declare @HVCContactableBY bigint = 0,@HVCContactableQ1 bigint = 0,@HVCContactableQ2 bigint = 0,@HVCContactableQ3 bigint = 0,@HVCContactableQ4 bigint = 0,@HVCContactableYTD bigint = 0

	DECLARE @tmpQuarterNum int,@tmpDBVol bigint,@tmpContactable bigint,@tmpMobileOnly bigint,@tmpEmailOnly bigint,@tmpEmailMobile bigint
	DECLARE @tmpAdorer bigint, @tmpAdopter bigint, @tmpAvailable bigint, @tmpAccept bigint, @tmpReject bigint, @tmpNA bigint
	DECLARE @cCursor CURSOR
	DECLARE @tmpFilter int, @tmpValue bigint
	
	DECLARE @_Q1 INT, @_Q2 INT, @_Q3 INT, @_Q4 INT
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT quarter,
		CASE
			WHEN MIN(DATEADD(MONTH, month_num - 1, CONVERT(DATE, '01-01-' + CAST(@YYYY + year_shift AS CHAR(4))))) > GETDATE() THEN 0
			ELSE 1
		END
		FROM tbl_m_rpt_quarter_month t
		GROUP BY t.quarter
	
	OPEN @cCursor
	FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpFilter = 1)
			SET @_Q1 = @tmpValue
		ELSE IF (@tmpFilter = 2)
			SET @_Q2 = @tmpValue
		ELSE IF (@tmpFilter = 3)
			SET @_Q3 = @tmpValue
		ELSE IF (@tmpFilter = 4)
			SET @_Q4 = @tmpValue
		
		FETCH NEXT FROM @cCursor INTO @tmpFilter, @tmpValue
	END
	CLOSE @cCursor
	
	SELECT @DBVolumeBYear = ISNULL(COUNT(DISTINCT(consumerId)), 0), 
	@ContactableBYear = ISNULL(COUNT(DISTINCT(CASE WHEN no_all IS NULL THEN consumerId ELSE NULL END)), 0), -- contactable
	@MobileBY = ISNULL(COUNT(DISTINCT(mobileOnly)), 0),
	@EmailBY = ISNULL(COUNT(DISTINCT(emailOnly)), 0),
	@EmailMobileBY = ISNULL(COUNT(DISTINCT(both)), 0)
	FROM vw_rpt_dashboardConsumer_by_quarter
	WHERE upd_quarterYear < @YYYY
	AND (brandPreSmirnoff ='T') -- OR (survey4aSmirnoff IS NOT NULL AND len(survey4aSmirnoff) > 0)
		
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT upd_quarterNum, 
		ISNULL(COUNT(DISTINCT(consumerId)), 0), 
		ISNULL(COUNT(DISTINCT(CASE WHEN no_all IS NULL THEN consumerId ELSE NULL END)), 0), -- contactable
		ISNULL(COUNT(DISTINCT(mobileOnly)), 0), 
		ISNULL(COUNT(DISTINCT(emailOnly)), 0), 
		ISNULL(COUNT(DISTINCT(both)), 0)
		FROM vw_rpt_dashboardConsumer_by_quarter
		WHERE upd_quarterYear = @YYYY
		AND (brandPreSmirnoff ='T') -- OR (survey4aSmirnoff IS NOT NULL /*AND len(survey4aSmirnoff) > 0*/))
		GROUP BY upd_quarterNum
		ORDER BY upd_quarterNum ASC
		
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpDBVol, @tmpContactable, @tmpMobileOnly, @tmpEmailOnly, @tmpEmailMobile
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
		BEGIN
			SET @DBQ1 = @tmpDBVol
			SET @ContactableQ1 = @tmpContactable
			SET @EmailQ1 = @tmpEmailOnly
			SET @MobileQ1 = @tmpMobileOnly
			SET @EmailMobileQ1 = @tmpEmailMobile
		END
		ELSE IF (@tmpQuarterNum = 2)
		BEGIN
			SET @DBQ2 = @tmpDBVol
			SET @ContactableQ2 = @tmpContactable
			SET @EmailQ2 = @tmpEmailOnly
			SET @MobileQ2 = @tmpMobileOnly
			SET @EmailMobileQ2 = @tmpEmailMobile
		END
		ELSE IF (@tmpQuarterNum = 3)
		BEGIN
			SET @DBQ3 = @tmpDBVol
			SET @ContactableQ3 = @tmpContactable
			SET @EmailQ3 = @tmpEmailOnly
			SET @MobileQ3 = @tmpMobileOnly
			SET @EmailMobileQ3 = @tmpEmailMobile
		END
		ELSE IF (@tmpQuarterNum = 4)
		BEGIN
			SET @DBQ4 = @tmpDBVol
			SET @ContactableQ4 = @tmpContactable
			SET @EmailQ4 = @tmpEmailOnly
			SET @MobileQ4 = @tmpMobileOnly
			SET @EmailMobileQ4 = @tmpEmailMobile
		END

		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpDBVol, @tmpContactable, @tmpMobileOnly, @tmpEmailOnly, @tmpEmailMobile
	END
	CLOSE @cCursor

	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT survey4aSmirnoff, ISNULL(COUNT(DISTINCT(consumerId)), 0)
		FROM vw_rpt_dashboardConsumer_by_quarter
		WHERE upd_quarterYear < @YYYY
		AND (brandPreSmirnoff ='T')
		AND no_all IS NULL
		GROUP BY survey4aSmirnoff
		ORDER BY survey4aSmirnoff ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF @tmpFilter = 1
			SET @AdorerBY = @tmpValue
		ELSE IF @tmpFilter = 2
			SET @AdopterBY = @tmpValue
		ELSE IF @tmpFilter = 3
			SET @AvailableBY = @tmpValue
		ELSE IF @tmpFilter = 4
			SET @AcceptBY = @tmpValue
		ELSE IF @tmpFilter = 5
			SET @RejectBY = @tmpValue
		ELSE -- Case have more than 6 type
			SET @NABY = ISNULL(@NABY, 0) + @tmpValue

		FETCH NEXT FROM @cCursor
			INTO @tmpFilter, @tmpValue
	END
	CLOSE @cCursor
	
	SET @cCursor = CURSOR FAST_FORWARD
	FOR
		SELECT upd_quarterNum, survey4aSmirnoff, ISNULL(COUNT(DISTINCT(consumerId)), 0)
		FROM vw_rpt_dashboardConsumer_by_quarter
		WHERE upd_quarterYear = @YYYY
		AND (brandPreSmirnoff ='T')
		AND no_all IS NULL
		GROUP BY upd_quarterNum, survey4aSmirnoff
		ORDER BY upd_quarterNum, survey4aSmirnoff ASC
				
	OPEN @cCursor
	FETCH NEXT FROM @cCursor
		INTO @tmpQuarterNum, @tmpFilter, @tmpValue
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF (@tmpQuarterNum = 1)
		BEGIN
			IF @tmpFilter = 1
				SET @AdorerQ1 = @tmpValue
			ELSE IF @tmpFilter = 2
				SET @AdopterQ1 = @tmpValue
			ELSE IF @tmpFilter = 3
				SET @AvailableQ1 = @tmpValue
			ELSE IF @tmpFilter = 4
				SET @AcceptQ1 = @tmpValue
			ELSE IF @tmpFilter = 5
				SET @RejectQ1 = @tmpValue
			ELSE -- Case have more than 6 type
				SET @NAQ1 = ISNULL(@NAQ1, 0) + @tmpValue
		END
		ELSE IF (@tmpQuarterNum = 2)
		BEGIN
			IF @tmpFilter = 1
				SET @AdorerQ2 = @tmpValue
			ELSE IF @tmpFilter = 2
				SET @AdopterQ2 = @tmpValue
			ELSE IF @tmpFilter = 3
				SET @AvailableQ2 = @tmpValue
			ELSE IF @tmpFilter = 4
				SET @AcceptQ2 = @tmpValue
			ELSE IF @tmpFilter = 5
				SET @RejectQ2 = @tmpValue
			ELSE -- Case have more than 6 type
				SET @NAQ2 = ISNULL(@NAQ2, 0) + @tmpValue
		END
		ELSE IF (@tmpQuarterNum = 3)
		BEGIN
			IF @tmpFilter = 1
				SET @AdorerQ3 = @tmpValue
			ELSE IF @tmpFilter = 2
				SET @AdopterQ3 = @tmpValue
			ELSE IF @tmpFilter = 3
				SET @AvailableQ3 = @tmpValue
			ELSE IF @tmpFilter = 4
				SET @AcceptQ3 = @tmpValue
			ELSE IF @tmpFilter = 5
				SET @RejectQ3 = @tmpValue
			ELSE -- Case have more than 6 type
				SET @NAQ3 = ISNULL(@NAQ3, 0) + @tmpValue
		END
		ELSE IF (@tmpQuarterNum = 4)
		BEGIN
			IF @tmpFilter = 1
				SET @AdorerQ4 = @tmpValue
			ELSE IF @tmpFilter = 2
				SET @AdopterQ4 = @tmpValue
			ELSE IF @tmpFilter = 3
				SET @AvailableQ4 = @tmpValue
			ELSE IF @tmpFilter = 4
				SET @AcceptQ4 = @tmpValue
			ELSE IF @tmpFilter = 5
				SET @RejectQ4 = @tmpValue
			ELSE -- Case have more than 6 type
				SET @NAQ4 = ISNULL(@NAQ4, 0) + @tmpValue
		END

		FETCH NEXT FROM @cCursor
			INTO @tmpQuarterNum, @tmpFilter, @tmpValue
	END
	CLOSE @cCursor

	SET @DBQ1 = ISNULL(@DBVolumeBYear, 0) + ISNULL(@DBQ1, 0)
	SET @DBQ2 = ISNULL(@DBQ1, 0) + ISNULL(@DBQ2, 0)
	SET @DBQ3 = ISNULL(@DBQ2, 0) + ISNULL(@DBQ3, 0)
	SET @DBQ4 = ISNULL(@DBQ3, 0) + ISNULL(@DBQ4, 0)

	SET @ContactableQ1 = ISNULL(@ContactableBYear, 0) + ISNULL(@ContactableQ1, 0)
	SET @ContactableQ2 = ISNULL(@ContactableQ1, 0) + ISNULL(@ContactableQ2, 0)
	SET @ContactableQ3 = ISNULL(@ContactableQ2, 0) + ISNULL(@ContactableQ3, 0)
	SET @ContactableQ4 = ISNULL(@ContactableQ3, 0) + ISNULL(@ContactableQ4, 0)

	SET @EmailQ1 = ISNULL(@EmailBY, 0) + ISNULL(@EmailQ1, 0)
	SET @EmailQ2 = ISNULL(@EmailQ1, 0) + ISNULL(@EmailQ2, 0)
	SET @EmailQ3 = ISNULL(@EmailQ2, 0) + ISNULL(@EmailQ3, 0)
	SET @EmailQ4 = ISNULL(@EmailQ3, 0) + ISNULL(@EmailQ4, 0)

	SET @MobileQ1 = ISNULL(@MobileBY, 0) + ISNULL(@MobileQ1, 0)
	SET @MobileQ2 = ISNULL(@MobileQ1, 0) + ISNULL(@MobileQ2, 0)
	SET @MobileQ3 = ISNULL(@MobileQ2, 0) + ISNULL(@MobileQ3, 0)
	SET @MobileQ4 = ISNULL(@MobileQ3, 0) + ISNULL(@MobileQ4, 0)

	SET @EmailMobileQ1 = ISNULL(@EmailMobileBY, 0) + ISNULL(@EmailMobileQ1, 0)
	SET @EmailMobileQ2 = ISNULL(@EmailMobileQ1, 0) + ISNULL(@EmailMobileQ2, 0)
	SET @EmailMobileQ3 = ISNULL(@EmailMobileQ2, 0) + ISNULL(@EmailMobileQ3, 0)
	SET @EmailMobileQ4 = ISNULL(@EmailMobileQ3, 0) + ISNULL(@EmailMobileQ4, 0)

	SET @AdorerQ1 = ISNULL(@AdorerBY, 0) + ISNULL(@AdorerQ1, 0)
	SET @AdorerQ2 = ISNULL(@AdorerQ1, 0) + ISNULL(@AdorerQ2, 0)
	SET @AdorerQ3 = ISNULL(@AdorerQ2, 0) + ISNULL(@AdorerQ3, 0)
	SET @AdorerQ4 = ISNULL(@AdorerQ3, 0) + ISNULL(@AdorerQ4, 0)

	SET @AdopterQ1 = ISNULL(@AdopterBY, 0) + ISNULL(@AdopterQ1, 0)
	SET @AdopterQ2 = ISNULL(@AdopterQ1, 0) + ISNULL(@AdopterQ2, 0)
	SET @AdopterQ3 = ISNULL(@AdopterQ2, 0) + ISNULL(@AdopterQ3, 0)
	SET @AdopterQ4 = ISNULL(@AdopterQ3, 0) + ISNULL(@AdopterQ4, 0)

	SET @AvailableQ1 = ISNULL(@AvailableBY, 0) + ISNULL(@AvailableQ1, 0)
	SET @AvailableQ2 = ISNULL(@AvailableQ1, 0) + ISNULL(@AvailableQ2, 0)
	SET @AvailableQ3 = ISNULL(@AvailableQ2, 0) + ISNULL(@AvailableQ3, 0)
	SET @AvailableQ4 = ISNULL(@AvailableQ3, 0) + ISNULL(@AvailableQ4, 0)

	SET @AcceptQ1 = ISNULL(@AcceptBY, 0) + ISNULL(@AcceptQ1, 0)
	SET @AcceptQ2 = ISNULL(@AcceptQ1, 0) + ISNULL(@AcceptQ2, 0)
	SET @AcceptQ3 = ISNULL(@AcceptQ2, 0) + ISNULL(@AcceptQ3, 0)
	SET @AcceptQ4 = ISNULL(@AcceptQ3, 0) + ISNULL(@AcceptQ4, 0)

	SET @RejectQ1 = ISNULL(@RejectBY, 0) + ISNULL(@RejectQ1, 0)
	SET @RejectQ2 = ISNULL(@RejectQ1, 0) + ISNULL(@RejectQ2, 0)
	SET @RejectQ3 = ISNULL(@RejectQ2, 0) + ISNULL(@RejectQ3, 0)
	SET @RejectQ4 = ISNULL(@RejectQ3, 0) + ISNULL(@RejectQ4, 0)

	SET @NAQ1 = ISNULL(@NABY, 0) + ISNULL(@NAQ1, 0)
	SET @NAQ2 = ISNULL(@NAQ1, 0) + ISNULL(@NAQ2, 0)
	SET @NAQ3 = ISNULL(@NAQ2, 0) + ISNULL(@NAQ3, 0)
	SET @NAQ4 = ISNULL(@NAQ3, 0) + ISNULL(@NAQ4, 0)

	SET @DBYTD = /*ISNULL(@DBQ1, 0) + ISNULL(@DBQ2, 0) + ISNULL(@DBQ3, 0) + */ISNULL(@DBQ4, 0)
	SET @ContactableYTD = /*ISNULL(@ContactableQ1, 0) + ISNULL(@ContactableQ2, 0) + ISNULL(@ContactableQ3, 0) + */ISNULL(@ContactableQ4, 0)	
	SET @EmailYTD = /*ISNULL(@EmailQ1, 0) + ISNULL(@EmailQ2, 0) + ISNULL(@EmailQ3, 0) + */ISNULL(@EmailQ4, 0)
	SET @MobileYTD = /*ISNULL(@MobileQ1, 0) + ISNULL(@MobileQ2, 0) + ISNULL(@MobileQ3, 0) + */ISNULL(@MobileQ4, 0)
	SET @EmailMobileYTD = /*ISNULL(@EmailMobileQ1, 0) + ISNULL(@EmailMobileQ2, 0) + ISNULL(@EmailMobileQ3, 0) + */ISNULL(@EmailMobileQ4, 0)	
	SET @AdorerYTD = /*ISNULL(@AdorerQ1, 0) + ISNULL(@AdorerQ2, 0) + ISNULL(@AdorerQ3, 0) + */ISNULL(@AdorerQ4, 0)
	SET @AdopterYTD = /*ISNULL(@AdopterQ1, 0) + ISNULL(@AdopterQ2, 0) + ISNULL(@AdopterQ3, 0) + */ISNULL(@AdopterQ4, 0)
	SET @AvailableYTD = /*ISNULL(@AvailableQ1, 0) + ISNULL(@AvailableQ2, 0) + ISNULL(@AvailableQ3, 0) + */ISNULL(@AvailableQ4, 0)
	SET @AcceptYTD = /*ISNULL(@AcceptQ1, 0) + ISNULL(@AcceptQ2, 0) + ISNULL(@AcceptQ3, 0) + */ISNULL(@AcceptQ4, 0)
	SET @RejectYTD = /*ISNULL(@RejectQ1, 0) + ISNULL(@RejectQ2, 0) + ISNULL(@RejectQ3, 0) + */ISNULL(@RejectQ4, 0)
	SET @NAYTD = /*ISNULL(@NAQ1, 0) + ISNULL(@NAQ2, 0) + ISNULL(@NAQ3, 0) + */ISNULL(@NAQ4, 0)

	--HVCContactable
	set @HVCContactableQ1 = ISNULL(@AdorerQ1, 0)+ ISNULL(@AdopterQ1, 0)
	set @HVCContactableQ2 = ISNULL(@AdorerQ2, 0)+ ISNULL(@AdopterQ2, 0)
	set @HVCContactableQ3 = ISNULL(@AdorerQ3, 0)+ ISNULL(@AdopterQ3, 0)
	set @HVCContactableQ4 = ISNULL(@AdorerQ4, 0)+ ISNULL(@AdopterQ4, 0)
	set @HVCContactableBY = /*ISNULL(@HVCContactableQ1, 0)+ ISNULL(@HVCContactableQ2, 0)+ ISNULL(@HVCContactableQ3, 0)+ */ISNULL(@HVCContactableQ4, 0)
	set @HVCContactableYTD = ISNULL(@AdorerYTD, 0) + ISNULL(@AdopterYTD, 0)

	SELECT @DBVolumeBYear AS DBVolumeBYear,CASE @_Q1 WHEN 1 THEN @DBQ1 ELSE NULL END AS DBQ1,CASE @_Q2 WHEN 1 THEN @DBQ2 ELSE NULL END AS DBQ2,CASE @_Q3 WHEN 1 THEN @DBQ3 ELSE NULL END AS DBQ3,CASE @_Q4 WHEN 1 THEN @DBQ4 ELSE NULL END AS DBQ4,@DBYTD AS DBYTD,
	 @ContactableBYear AS ContactableBYear,CASE @_Q1 WHEN 1 THEN @ContactableQ1 ELSE NULL END AS ContactableQ1,CASE @_Q2 WHEN 1 THEN @ContactableQ2 ELSE NULL END AS ContactableQ2,CASE @_Q3 WHEN 1 THEN @ContactableQ3 ELSE NULL END AS ContactableQ3,CASE @_Q4 WHEN 1 THEN @ContactableQ4 ELSE NULL END AS ContactableQ4,@ContactableYTD AS ContactableYTD,
	 @EmailBY AS EmailBY,CASE @_Q1 WHEN 1 THEN @EmailQ1 ELSE NULL END AS EmailQ1,CASE @_Q2 WHEN 1 THEN @EmailQ2 ELSE NULL END AS EmailQ2,CASE @_Q3 WHEN 1 THEN @EmailQ3 ELSE NULL END AS EmailQ3,CASE @_Q4 WHEN 1 THEN @EmailQ4 ELSE NULL END AS EmailQ4,@EmailYTD AS EmailYTD,
	 @MobileBY AS MobileBY,CASE @_Q1 WHEN 1 THEN @MobileQ1 ELSE NULL END AS MobileQ1,CASE @_Q2 WHEN 1 THEN @MobileQ2 ELSE NULL END AS MobileQ2,CASE @_Q3 WHEN 1 THEN @MobileQ3 ELSE NULL END AS MobileQ3,CASE @_Q4 WHEN 1 THEN @MobileQ4 ELSE NULL END AS MobileQ4,@MobileYTD AS MobileYTD,
	 @EmailMobileBY AS EmailMobileBY,CASE @_Q1 WHEN 1 THEN @EmailMobileQ1 ELSE NULL END AS EmailMobileQ1,CASE @_Q2 WHEN 1 THEN @EmailMobileQ2 ELSE NULL END AS EmailMobileQ2,CASE @_Q3 WHEN 1 THEN @EmailMobileQ3 ELSE NULL END AS EmailMobileQ3,CASE @_Q4 WHEN 1 THEN @EmailMobileQ4 ELSE NULL END AS EmailMobileQ4,@EmailMobileYTD AS EmailMobileYTD,
	 @HVCContactableBY AS HVCContactableBY,CASE @_Q1 WHEN 1 THEN @HVCContactableQ1 ELSE NULL END AS HVCContactableQ1,CASE @_Q2 WHEN 1 THEN @HVCContactableQ2 ELSE NULL END AS HVCContactableQ2,CASE @_Q3 WHEN 1 THEN @HVCContactableQ3 ELSE NULL END AS HVCContactableQ3,CASE @_Q4 WHEN 1 THEN @HVCContactableQ4 ELSE NULL END AS HVCContactableQ4,@HVCContactableYTD AS HVCContactableYTD,
	 @AdorerBY AS AdorerBY,CASE @_Q1 WHEN 1 THEN @AdorerQ1 ELSE NULL END AS AdorerQ1,CASE @_Q2 WHEN 1 THEN @AdorerQ2 ELSE NULL END AS AdorerQ2,CASE @_Q3 WHEN 1 THEN @AdorerQ3 ELSE NULL END AS AdorerQ3,CASE @_Q4 WHEN 1 THEN @AdorerQ4 ELSE NULL END AS AdorerQ4,@AdorerYTD AS AdorerYTD,
	 @AdopterBY AS AdopterBY,CASE @_Q1 WHEN 1 THEN @AdopterQ1 ELSE NULL END AS AdopterQ1,CASE @_Q2 WHEN 1 THEN @AdopterQ2 ELSE NULL END AS AdopterQ2,CASE @_Q3 WHEN 1 THEN @AdopterQ3 ELSE NULL END AS AdopterQ3,CASE @_Q4 WHEN 1 THEN @AdopterQ4 ELSE NULL END AS AdopterQ4,@AdopterYTD AS AdopterYTD,
	 @AvailableBY AS AvailableBY,CASE @_Q1 WHEN 1 THEN @AvailableQ1 ELSE NULL END AS AvailableQ1,CASE @_Q2 WHEN 1 THEN @AvailableQ2 ELSE NULL END AS AvailableQ2,CASE @_Q3 WHEN 1 THEN @AvailableQ3 ELSE NULL END AS AvailableQ3,CASE @_Q4 WHEN 1 THEN @AvailableQ4 ELSE NULL END AS AvailableQ4,@AvailableYTD AS AvailableYTD,
	 @AcceptBY AS AcceptBY,CASE @_Q1 WHEN 1 THEN @AcceptQ1 ELSE NULL END AS AcceptQ1,CASE @_Q2 WHEN 1 THEN @AcceptQ2 ELSE NULL END AS AcceptQ2,CASE @_Q3 WHEN 1 THEN @AcceptQ3 ELSE NULL END AS AcceptQ3,CASE @_Q4 WHEN 1 THEN @AcceptQ4 ELSE NULL END AS AcceptQ4,@AcceptYTD AS AcceptYTD,
	 @RejectBY AS RejectBY,CASE @_Q1 WHEN 1 THEN @RejectQ1 ELSE NULL END AS RejectQ1,CASE @_Q2 WHEN 1 THEN @RejectQ2 ELSE NULL END AS RejectQ2,CASE @_Q3 WHEN 1 THEN @RejectQ3 ELSE NULL END AS RejectQ3,CASE @_Q4 WHEN 1 THEN @RejectQ4 ELSE NULL END AS RejectQ4,@RejectYTD AS RejectYTD,
	 @NABY AS NABY,CASE @_Q1 WHEN 1 THEN @NAQ1 ELSE NULL END AS NAQ1,CASE @_Q2 WHEN 1 THEN @NAQ2 ELSE NULL END AS NAQ2,CASE @_Q3 WHEN 1 THEN @NAQ3 ELSE NULL END AS NAQ3,CASE @_Q4 WHEN 1 THEN @NAQ4 ELSE NULL END AS NAQ4,@NAYTD AS NAYTD

END
