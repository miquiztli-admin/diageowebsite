USE [DiageoDB_Edge]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Total_Commission_By_Outlet]    Script Date: 03/25/2014 17:11:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Total_Commission_By_Outlet', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Total_Commission_By_Outlet;
GO
-- =============================================
-- Author:		<Inferno>
-- Create date: <24/4/2014>
-- Update date: <24/4/2014>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Total_Commission_By_Outlet](@Startdate varchar(max),@Enddate varchar(max),@Outlet varchar(max))--, @PG_Input int)
	-- Add the parameters for the stored procedure here
AS
BEGIN
	Declare @OutletCount integer = 0
	Declare @Startdate1 varchar(max),@Enddate1 varchar(max)
	SET @Startdate1 = @Startdate--CONVERT(varchar,@Startdate)
	SET @Enddate1 = @Enddate--CONVERT(varchar,@Enddate)
	
	DECLARE @QueryOutlet TABLE(Outlet_ID INT)
	IF @Outlet <> '0'
	BEGIN
		DECLARE @separator_position INT -- This is used to locate each separator character
		DECLARE @array_value VARCHAR(50) -- this holds each array value as it is returned

		IF RIGHT(LTRIM(RTRIM(@Outlet)), 1) <> ',' SET @Outlet = LTRIM(RTRIM(@Outlet)) + ','
		WHILE PATINDEX('%,%', @Outlet) <> 0 
		BEGIN
			
			SELECT @separator_position = PATINDEX('%,%',@Outlet)
			SELECT @array_value = LTRIM(RTRIM(LEFT(@Outlet, @separator_position - 1)))
			
			IF LEN(@array_value) > 0
			BEGIN
				INSERT INTO @QueryOutlet(Outlet_ID) VALUES(@array_value)
			END
			-- replace processed strings with an empty string
			SELECT  @Outlet = STUFF(@Outlet, 1, @separator_position, '')
		END
	END
	
	SELECT @OutletCount = COUNT(*) FROM @QueryOutlet
	
	SELECT O.Outlet_Name, 
	ISNULL(R.NoAllPG, 0) AS NoAllPG, 
	ISNULL(MonthlyTraffic, 0) AS MonthlyTraffic, 
	ISNULL(CAST(MonthlyTraffic AS FLOAT) * 0.85, 0) AS MonthlyTraffic_15, 
	ISNULL(UniqueEngage, 0) AS UniqueEngage,
	ISNULL(R.RepeatType,0) AS RepeatType,
	ISNULL(R.NewType,0) AS NewType,
	ISNULL(R.AllType,0) AS Engage,
	(CAST(ISNULL(R.AllType,0) AS FLOAT) / CAST(ISNULL(CAST(MonthlyTraffic AS FLOAT) * 0.85, 1) AS FLOAT)) AS Engage_Traffic,
	(CAST(ISNULL(R.NewType,0) AS FLOAT) / CAST(ISNULL(R.AllType,1) AS FLOAT)) AS New_Engage,
	dbo.fnc_CalculateCommissionByDataEntry(
		ISNULL(R.AllType,0), 
		ISNULL(MonthlyTraffic, 0) * 0.85,
		ISNULL(R.NewType,0)
	) AS Commission
	FROM (
		SELECT * FROM [SmartTouchDB].[dbo].[ST_mOutlet]
		WHERE Outlet_Status = 'T'
	) O 
	LEFT JOIN (
		SELECT [Traffic_Outlet], SUM([Traffic_Amount]) AS MonthlyTraffic
		FROM [SmartTouchDB].[dbo].[ST_tsTraffic]
		WHERE Convert(date, [Traffic_Date]) between convert(date, @Startdate1) and convert(date, @Enddate1)
		GROUP BY [Traffic_Outlet]
	) T ON T.Traffic_Outlet = O.Outlet_ID
	LEFT JOIN (
		SELECT [Repeat_Outlet], COUNT([Repeat_Outlet]) UniqueEngage
		FROM (
			SELECT [Repeat_Outlet],Repeat_Consumer
			FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
			WHERE Convert(date, [Repeat_TransactionDate]) between convert(date, @Startdate1) and convert(date, @Enddate1)
			GROUP BY [Repeat_Outlet], Repeat_Consumer
		) U1
		GROUP BY [Repeat_Outlet]
	) U ON U.Repeat_Outlet = O.Outlet_ID
	LEFT JOIN (
		SELECT [Repeat_Outlet], 
		COUNT(distinct(Repeat_CreateBy)) as NoAllPG, 
		SUM(Case when UPPER(Repeat_Type) = 'R' then 1 else 0 end) as RepeatType,
		SUM(Case when UPPER(Repeat_Type) = 'N' then 1 else 0 end) as NewType,
		SUM(Case when Repeat_Type <> '' then 1 else 0 end) as AllType
		FROM 
		(
			SELECT Repeat_Type, [Repeat_Outlet], Repeat_CreateBy
			FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
			WHERE Convert(date, [Repeat_TransactionDate]) between convert(date, @Startdate1) and convert(date, @Enddate1)
			GROUP BY Repeat_Type, [Repeat_Outlet], Repeat_CreateBy, Repeat_Consumer, CONVERT(date, Repeat_TransactionDate)
		) R1
		GROUP BY [Repeat_Outlet]
	) R ON R.Repeat_Outlet = O.Outlet_ID
	WHERE (@OutletCount = '0' OR R.Repeat_Outlet IN (SELECT Outlet_ID FROM @QueryOutlet))
	AND O.Outlet_Name IS NOT NULL
	ORDER BY O.Outlet_Name

END