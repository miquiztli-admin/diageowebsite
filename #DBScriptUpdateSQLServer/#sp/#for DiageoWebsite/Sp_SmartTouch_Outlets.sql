USE [DiageoDB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_SmartTouch_Outlets', N'P') IS NOT NULL
    DROP PROC Sp_SmartTouch_Outlets;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <26/04/2014>
-- Description:	<Create New SP to use in Diageo Report Web controls >
-- =============================================
CREATE PROCEDURE [dbo].[Sp_SmartTouch_Outlets]
	@ErrorCode int OUTPUT
AS
BEGIN
	SELECT Outlet_ID, Outlet_Name 
	FROM [SmartTouchDB].[dbo].[ST_mOutlet]
	WHERE LOWER(Outlet_Status) = 't'
	ORDER BY Outlet_Name;
	
	SELECT @ErrorCode=@@ERROR;
END