USE [DiageoDB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_SmartTouch_Staffs', N'P') IS NOT NULL
    DROP PROC Sp_SmartTouch_Staffs;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <26/04/2014>
-- Description:	<Create New SP to use in Diageo Report Web controls >
-- =============================================
CREATE PROCEDURE [dbo].[Sp_SmartTouch_Staffs]
	@ErrorCode int OUTPUT
AS
BEGIN
	SELECT Staff_ID, Staff_Name 
	FROM [SmartTouchDB].[dbo].[ST_mStaff]
	WHERE LOWER(Staff_Status) = 't'
	ORDER BY Staff_Name;
	
	SELECT @ErrorCode=@@ERROR;
END