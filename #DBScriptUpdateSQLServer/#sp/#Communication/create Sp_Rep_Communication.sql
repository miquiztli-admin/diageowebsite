USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Communication]    Script Date: 11/02/2013 17:41:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'Sp_Rep_Communication', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Communication;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <06/11/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Communication](
	@Event as integer, @DateFrom as datetime, @DateTo as datetime, @Event1 as integer, @DateFrom1 as datetime, @DateTo1 as datetime, @EDM as bit, @SMS as bit, @CCenter as bit
)
AS
BEGIN

	-- Avoid parameter sniffing problems in SSRS
	DECLARE /*@LEvent VARCHAR(10), */@LDateFrom DATETIME, @LDateTo DATETIME, @LDateFrom1 DATETIME, @LDateTo1 DATETIME, @LEDM BIT, @LSMS BIT, @LCCenter BIT
	DECLARE @LEvent integer, @LEvent1 integer
	
	--SET @LEvent = LTRIM(RTRIM(@Event))
	SET @LDateFrom = @DateFrom
	SET @LDateTo = @DateTo
	SET @LDateFrom1 = @DateFrom1
	SET @LDateTo1 = @DateTo1
	SET @LEDM = @EDM
	SET @LSMS = @SMS
	SET @LCCenter = @CCenter
	-- Avoid parameter sniffing problems in SSRS

	/*DECLARE @comma_position INT -- This is used to locate each separator character (,)
	DECLARE @array_value VARCHAR(10) -- this holds each array value as it is returned
	
	SET @comma_position = PATINDEX('%,%', @LEvent)
	IF @comma_position <> 0
	BEGIN
		SET @LEvent1 = CONVERT(INT, LTRIM(RTRIM(LEFT(@LEvent, @comma_position - 1))))
		SET @LEvent2 = CONVERT(INT, LTRIM(RTRIM(RIGHT(@LEvent, LEN(@LEvent) - @comma_position))))
	END
	ELSE
	BEGIN
		SET @LEvent1 = CONVERT(INT, LTRIM(RTRIM(@LEvent)))
		SET @LEvent2 = 0
	END*/	
	SET @LEvent = @Event
	SET @LEvent1 = @Event1
	
	SELECT e.eventId, eventName AS Campaign,
	COUNT(o.EDMSent) AS edmSent,
	COUNT(o.EDMOpen) AS edmOpen,
	COUNT(o.EDMClickLink) AS edmClick,
	COUNT(o.EDMDeliver) AS edmDeliver,
	COUNT(o.SMSSent) AS smsSent,
	COUNT(o.SMSDeliver) AS smsSuccess,
	COUNT(o.SMSFail) AS smsFail,
	COUNT(o.CCenterSent) AS ccSent,
	COUNT(o.CCenterSuccess) AS ccSuccess,
	COUNT(o.CCenterFail) AS ccFail
	FROM DiageoDB..OutboundEventMaster AS e 
		LEFT OUTER JOIN (
			SELECT 2 AS ordering, c.*
			FROM vw_rpt_outboundCommunication c
			WHERE eventId = @LEvent
			AND (senddate BETWEEN @LDateFrom AND @LDateTo)
			AND (
				(EDM = ISNULL(@LEDM, 0))
				OR (SMS = ISNULL(@LSMS, 0))
				OR (Ccenter = ISNULL(@LCCenter, 0))
				)
				
			UNION ALL

			SELECT 1 AS ordering, c.*
			FROM vw_rpt_outboundCommunication c
			WHERE eventId = @LEvent1
			AND (senddate BETWEEN @LDateFrom1 AND @LDateTo1)
			AND (
				(EDM = ISNULL(@LEDM, 0))
				OR (SMS = ISNULL(@LSMS, 0))
				OR (Ccenter = ISNULL(@LCCenter, 0))
				)
			) AS o ON o.eventId = e.eventId
	WHERE (e.eventId = @LEvent OR e.eventID = @LEvent1)
	GROUP BY e.eventId, e.eventName, ISNULL(o.ordering, 0)
	ORDER BY ISNULL(o.ordering, 0) DESC
	
END
