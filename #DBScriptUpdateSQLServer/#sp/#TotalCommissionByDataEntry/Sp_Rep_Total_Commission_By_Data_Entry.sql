USE [DiageoDB_Edge]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Total_Commission_By_Data_Entry]    Script Date: 04/24/2014 12:17:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Total_Commission_By_Data_Entry', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Total_Commission_By_Data_Entry;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <24/4/2014>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Total_Commission_By_Data_Entry](
	@Startdate varchar(max),@Enddate varchar(max),@Outlet varchar(max)--, @PG_Input int)
)
AS
BEGIN
	Declare @OutletCount integer = 0
	Declare @str varchar(max)
	Declare @Startdate1 varchar(max),@Enddate1 varchar(max)
	SET @Startdate1 = @Startdate--CONVERT(varchar,@Startdate)
	SET @Enddate1 = @Enddate--CONVERT(varchar,@Enddate)
	
	DECLARE @QueryOutlet TABLE(Outlet_ID INT)
	IF @Outlet <> '0'
	BEGIN
		DECLARE @separator_position INT -- This is used to locate each separator character
		DECLARE @array_value VARCHAR(50) -- this holds each array value as it is returned

		IF RIGHT(LTRIM(RTRIM(@Outlet)), 1) <> ',' SET @Outlet = LTRIM(RTRIM(@Outlet)) + ','
		WHILE PATINDEX('%,%', @Outlet) <> 0 
		BEGIN
			
			SELECT @separator_position = PATINDEX('%,%',@Outlet)
			SELECT @array_value = LTRIM(RTRIM(LEFT(@Outlet, @separator_position - 1)))
			
			IF LEN(@array_value) > 0
			BEGIN
				INSERT INTO @QueryOutlet(Outlet_ID) VALUES(@array_value)
			END
			-- replace processed strings with an empty string
			SELECT  @Outlet = STUFF(@Outlet, 1, @separator_position, '')
		END
	END
	
	SELECT @OutletCount = COUNT(*) FROM @QueryOutlet
	
	SELECT S.Staff_Code, S.Staff_Name, O.Outlet_Name,
	main.Actual_Day, CAST(main.Commission AS DECIMAL(10, 2)) AS Commission
	FROM (
		SELECT m.Repeat_CreateBy AS Staff_ID, m.Repeat_Outlet AS Outlet_ID,
		COUNT(DISTINCT(m.RepeatTransactionDate)) AS Actual_Day,
		SUM(AverageComm.AverageCommission) AS Commission
		FROM (
			SELECT Repeat_CreateBy, Repeat_Outlet, 
			CONVERT(Date, Repeat_TransactionDate) AS RepeatTransactionDate
			FROM [SmartTouchDB].[dbo].[ST_tsRepeat]	
			WHERE CONVERT(Date, Repeat_TransactionDate) Between Convert(date, @Startdate1) and Convert(date, @Enddate1)
			GROUP BY Repeat_CreateBy, Repeat_Outlet, CONVERT(Date, Repeat_TransactionDate)	
		) AS m 
		LEFT OUTER JOIN (
			SELECT R.Repeat_Outlet, R.RepeatTransactionDate,
			dbo.fnc_CalculateDailyCommission(
				ISNULL(MonthlyR.AllType, 0),
				CAST(ISNULL(MonthlyT.Traffic, 0) AS FLOAT) * 0.85,
				ISNULL(MonthlyR.NewType, 0),
				ISNULL(R.AllType, 0),
				ISNULL(R.NewType, 0)
			) / R.NoPG AS AverageCommission
			FROM (
				SELECT Repeat_Outlet, RepeatTransactionDate,
				COUNT(DISTINCT(Repeat_CreateBy)) AS NoPG,
				SUM(CASE WHEN UPPER(Repeat_Type) = 'R' then 1 else 0 end) AS RepeatType,
				SUM(CASE WHEN UPPER(Repeat_Type) = 'N' then 1 else 0 end) AS NewType,
				SUM(CASE WHEN Repeat_Type <> '' then 1 else 0 end) AS AllType
				FROM (
					SELECT Repeat_Type, Repeat_Outlet, Repeat_CreateBy, 
					CONVERT(date, Repeat_TransactionDate) AS RepeatTransactionDate
					FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
					WHERE CONVERT(date, Repeat_TransactionDate) between convert(date, @Startdate1) and convert(date, @Enddate1)
					GROUP BY Repeat_Type, Repeat_Outlet, Repeat_CreateBy, Repeat_Consumer, CONVERT(date, Repeat_TransactionDate)
				) R1
				GROUP BY Repeat_Outlet, RepeatTransactionDate
			) R INNER JOIN (
				SELECT Repeat_Outlet,
				SUM(CASE WHEN UPPER(Repeat_Type) = 'N' then 1 else 0 end) AS NewType,
				SUM(CASE WHEN Repeat_Type <> '' then 1 else 0 end) AS AllType
				FROM (
					SELECT Repeat_Type, Repeat_Outlet
					FROM [SmartTouchDB].[dbo].[ST_tsRepeat]
					WHERE CONVERT(date, Repeat_TransactionDate) between convert(date, @Startdate1) and convert(date, @Enddate1)
					GROUP BY Repeat_Type, Repeat_Outlet, Repeat_CreateBy, Repeat_Consumer, CONVERT(date, Repeat_TransactionDate)
				) R1
				GROUP BY Repeat_Outlet
			) AS MonthlyR ON R.Repeat_Outlet = MonthlyR.Repeat_Outlet
			INNER JOIN (
				SELECT Traffic_Outlet, SUM(Traffic_Amount) as Traffic
				FROM [SmartTouchDB].[dbo].[ST_tsTraffic]
				WHERE CONVERT(Date, Traffic_Date) between Convert(date, @Startdate1) and Convert(date, @Enddate1)
				GROUP BY Traffic_Outlet
			) AS MonthlyT ON MonthlyT.Traffic_Outlet = R.Repeat_Outlet
		) AS AverageComm ON m.Repeat_Outlet = AverageComm.Repeat_Outlet
			AND m.RepeatTransactionDate = AverageComm.RepeatTransactionDate
		GROUP BY m.Repeat_CreateBy, m.Repeat_Outlet
	) AS main 
		LEFT JOIN [SmartTouchDB].[dbo].[ST_mStaff] S ON main.Staff_ID = S.Staff_ID
		LEFT JOIN [SmartTouchDB].[dbo].[ST_mOutlet] O ON main.Outlet_ID = O.Outlet_ID
	WHERE S.Staff_Name IS NOT NULL
	AND (@OutletCount = '0' OR main.Outlet_ID IN (SELECT Outlet_ID FROM @QueryOutlet))
	ORDER BY s.Staff_Code

END
