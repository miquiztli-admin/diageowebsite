USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_TurnupCompare_All]    Script Date: 11/02/2013 18:41:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_TurnupCompare_All', N'P') IS NOT NULL
    DROP PROC Sp_Rep_TurnupCompare_All;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <14/11/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_TurnupCompare_All](
	@Event as integer, @DateFrom as datetime, @DateTo as datetime,
	@Event1 as integer, @DateFrom1 as datetime, @DateTo1 as datetime,
	@EDM as bit,@SMS as bit,@CCenter as bit
)
AS
BEGIN
	
	-- Avoid parameter sniffing problems in SSRS
	DECLARE @LEvent INT, @LDateFrom DATETIME, @LDateTo DATETIME, @LEvent1 INT, @LDateFrom1 DATETIME, @LDateTo1 DATETIME, @LEDM BIT, @LSMS BIT, @LCCenter BIT
	SET @LEvent = @Event
	SET @LDateFrom = @DateFrom
	SET @LDateTo = @DateTo
	SET @LEvent1 = @Event1
	SET @LDateFrom1 = @DateFrom1
	SET @LDateTo1 = @DateTo1
	SET @LEDM = @EDM
	SET @LSMS = @SMS
	SET @LCCenter = @CCenter
	-- Avoid parameter sniffing problems in SSRS

	DECLARE @tmpSent TABLE (TypeValue INT, GroupingValue VARCHAR(10), Summary BIGINT)
	DECLARE @tmpComm TABLE (TypeValue INT, GroupingValue VARCHAR(10), Summary BIGINT)
	DECLARE @ResultTable TABLE (Seq INT, EventID INT, EventName Varchar(100), TurnUpType VarChar(100), Communicationrate decimal(10,2))
	DECLARE @EventName Varchar(100)

-- Event Name
	SELECT
	@EventName = eventName
	FROM DiageoDB..OutboundEventMaster
	WHERE eventId = @LEvent
	
-- Turn up sent by gender
	INSERT INTO @tmpSent
	SELECT 0, lower(gender), COUNT(DISTINCT(consumerId))
	FROM vw_rpt_turnup_sent
	WHERE (senddate BETWEEN @LDateFrom and @LDateTo)
	AND eventId = @LEvent
	AND (
		(EDM = ISNULL(@LEDM, 0))
		OR (SMS = ISNULL(@LSMS, 0))
		OR (Ccenter = ISNULL(@LCCenter, 0))
	)
	GROUP BY lower(gender)
	
-- Turn up from communication by gender
	INSERT INTO @tmpComm
	SELECT 0, lower(gender), COUNT(DISTINCT(redeem_consumer))
	FROM vw_rpt_turnup_comm
	WHERE (senddate BETWEEN @LDateFrom and @LDateTo)
	AND eventId = @LEvent
	AND (
		(EDM = ISNULL(@LEDM, 0))
		OR (SMS = ISNULL(@LSMS, 0))
		OR (Ccenter = ISNULL(@LCCenter, 0))
	)
	GROUP BY lower(gender)
 
-- Turn up sent by age
	INSERT INTO @tmpSent
	SELECT 1, ageRange, COUNT(DISTINCT(s.consumerId))
	FROM (
		SELECT consumerId,
		CASE
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 20 AND 24 THEN 1 
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 25 AND 29 THEN 2
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 30 AND 34 THEN 3
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 35 AND 39 THEN 4
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) >= 40 THEN 5
		END AS ageRange
		FROM vw_rpt_turnup_sent
		WHERE (senddate BETWEEN @LDateFrom and @LDateTo)
		AND eventId = @LEvent
		AND (
			(EDM = ISNULL(@LEDM, 0))
			OR (SMS = ISNULL(@LSMS, 0))
			OR (Ccenter = ISNULL(@LCCenter, 0))
		)
	) AS s
	GROUP BY s.ageRange

-- Turn up communication by age
	INSERT INTO @tmpComm
	SELECT 1, ageRange, COUNT(DISTINCT(s.redeem_consumer))
	FROM (
		SELECT redeem_consumer,
		CASE
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 20 AND 24 THEN 1
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 25 AND 29 THEN 2
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 30 AND 34 THEN 3
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 35 AND 39 THEN 4
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) >= 40 THEN 5
		END AS ageRange
		FROM vw_rpt_turnup_comm
		WHERE (senddate BETWEEN @LDateFrom and @LDateTo)
		AND eventId = @LEvent
		AND (
			(EDM = ISNULL(@LEDM, 0))
			OR (SMS = ISNULL(@LSMS, 0))
			OR (Ccenter = ISNULL(@LCCenter, 0))
		)
	) AS s
	GROUP BY s.ageRange

-- Turn up sent by Brand
	INSERT INTO @tmpSent
	SELECT 2, s.Brand, COUNT(DISTINCT(s.consumerId)) 
	FROM vw_rpt_turnup_sent s
	WHERE (s.senddate BETWEEN @LDateFrom and @LDateTo)
	AND s.eventId = @LEvent
	AND s.Brand IS NOT NULL
	AND (
		(s.EDM = ISNULL(@LEDM, 0))
		OR (s.SMS = ISNULL(@LSMS, 0))
		OR (s.Ccenter = ISNULL(@LCCenter, 0))
	)
	GROUP BY s.Brand

-- Turn up communication by Brand
	INSERT INTO @tmpComm
	SELECT 2, c.Brand, COUNT(DISTINCT(c.redeem_consumer)) 
	FROM vw_rpt_turnup_comm c
	WHERE (c.senddate BETWEEN @LDateFrom and @LDateTo)
	AND c.eventId = @LEvent
	AND c.Brand IS NOT NULL
	AND (
		(c.EDM = ISNULL(@LEDM, 0))
		OR (c.SMS = ISNULL(@LSMS, 0))
		OR (c.Ccenter = ISNULL(@LCCenter, 0))
	)
	GROUP BY c.Brand

-- Turn up sent 4A
	INSERT INTO @tmpSent
	SELECT 3, s.survey4a, COUNT(DISTINCT(s.consumerId)) 
	FROM vw_rpt_turnup_sent AS s
	WHERE (s.senddate BETWEEN @LDateFrom and @LDateTo)
	AND s.eventId = @LEvent
	AND (
		(s.EDM = ISNULL(@LEDM, 0))
		OR (s.SMS = ISNULL(@LSMS, 0))
		OR (s.Ccenter = ISNULL(@LCCenter, 0))
	)
	GROUP BY s.survey4a
	
-- Turn up communication 4A
	INSERT INTO @tmpComm
	SELECT 3, c.survey4a, COUNT(DISTINCT(c.redeem_consumer)) 
	FROM vw_rpt_turnup_comm c
	WHERE (c.senddate BETWEEN @LDateFrom and @LDateTo)
	AND c.eventId = @LEvent
	AND (
		(c.EDM = ISNULL(@LEDM, 0))
		OR (c.SMS = ISNULL(@LSMS, 0))
		OR (c.Ccenter = ISNULL(@LCCenter, 0))
	)	
	GROUP BY c.survey4a	

--insert Communication Rate of Gender
	INSERT INTO @ResultTable
	SELECT 10 + c.TypeValue, @LEvent, @EventName,
	template.Name AS Name, 
	AVG(CASE WHEN ISNULL(s.Summary, 0) = 0 THEN 0 ELSE (CONVERT(NUMERIC, ISNULL(c.Summary, 0)) / CONVERT(NUMERIC, ISNULL(s.Summary, 0))) END) * 100 Ratio
	FROM (
		SELECT 0 AS TypeValue, 'Communication Rate of Gender' AS Name
		UNION
		SELECT 1 AS TypeValue, 'Communication Rate of Age' AS Name
		UNION
		SELECT 2 AS TypeValue, 'Communication Rate of BrandPerference' AS Name
		UNION
		SELECT 3 AS TypeValue, 'Communication Rate of 4A''s' AS Name
	) template LEFT OUTER JOIN @tmpComm c ON template.TypeValue = c.TypeValue
		LEFT OUTER JOIN @tmpSent s ON c.TypeValue = s.TypeValue
			AND c.GroupingValue = s.GroupingValue 
	GROUP BY c.TypeValue, template.Name

	DELETE FROM @tmpSent
	DELETE FROM @tmpComm

-- Event Name
	SELECT
	@EventName = eventName
	FROM DiageoDB..OutboundEventMaster
	WHERE eventId = @LEvent1
	
-- Turn up sent by gender
	INSERT INTO @tmpSent
	SELECT 0, lower(gender), COUNT(DISTINCT(consumerId))
	FROM vw_rpt_turnup_sent
	WHERE (senddate BETWEEN @LDateFrom1 and @LDateTo1)
	AND eventId = @LEvent1
	AND (
		(EDM = ISNULL(@LEDM, 0))
		OR (SMS = ISNULL(@LSMS, 0))
		OR (Ccenter = ISNULL(@LCCenter, 0))
	)
	GROUP BY lower(gender)
	
-- Turn up from communication by gender
	INSERT INTO @tmpComm
	SELECT 0, lower(gender), COUNT(DISTINCT(redeem_consumer))
	FROM vw_rpt_turnup_comm
	WHERE (senddate BETWEEN @LDateFrom1 and @LDateTo1)
	AND eventId = @LEvent1
	AND (
		(EDM = ISNULL(@LEDM, 0))
		OR (SMS = ISNULL(@LSMS, 0))
		OR (Ccenter = ISNULL(@LCCenter, 0))
	)
	GROUP BY lower(gender)
 
-- Turn up sent by age
	INSERT INTO @tmpSent
	SELECT 1, ageRange, COUNT(DISTINCT(s.consumerId))
	FROM (
		SELECT consumerId,
		CASE
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 20 AND 24 THEN 1 
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 25 AND 29 THEN 2
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 30 AND 34 THEN 3
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 35 AND 39 THEN 4
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) >= 40 THEN 5
		END AS ageRange
		FROM vw_rpt_turnup_sent
		WHERE (senddate BETWEEN @LDateFrom1 and @LDateTo1)
		AND eventId = @LEvent1
		AND (
			(EDM = ISNULL(@LEDM, 0))
			OR (SMS = ISNULL(@LSMS, 0))
			OR (Ccenter = ISNULL(@LCCenter, 0))
		)
	) AS s
	GROUP BY s.ageRange

-- Turn up communication by age
	INSERT INTO @tmpComm
	SELECT 1, ageRange, COUNT(DISTINCT(s.redeem_consumer))
	FROM (
		SELECT redeem_consumer,
		CASE
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 20 AND 24 THEN 1
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 25 AND 29 THEN 2
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 30 AND 34 THEN 3
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 35 AND 39 THEN 4
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) >= 40 THEN 5
		END AS ageRange
		FROM vw_rpt_turnup_comm
		WHERE (senddate BETWEEN @LDateFrom1 and @LDateTo1)
		AND eventId = @LEvent1
		AND (
			(EDM = ISNULL(@LEDM, 0))
			OR (SMS = ISNULL(@LSMS, 0))
			OR (Ccenter = ISNULL(@LCCenter, 0))
		)
	) AS s
	GROUP BY s.ageRange

-- Turn up sent by Brand
	INSERT INTO @tmpSent
	SELECT 2, s.Brand, COUNT(DISTINCT(s.consumerId)) 
	FROM vw_rpt_turnup_sent s
	WHERE (s.senddate BETWEEN @LDateFrom1 and @LDateTo1)
	AND s.eventId = @LEvent1
	AND s.Brand IS NOT NULL
	AND (
		(s.EDM = ISNULL(@LEDM, 0))
		OR (s.SMS = ISNULL(@LSMS, 0))
		OR (s.Ccenter = ISNULL(@LCCenter, 0))
	)
	GROUP BY s.Brand

-- Turn up communication by Brand
	INSERT INTO @tmpComm
	SELECT 2, c.Brand, COUNT(DISTINCT(c.redeem_consumer)) 
	FROM vw_rpt_turnup_comm c
	WHERE (c.senddate BETWEEN @LDateFrom1 and @LDateTo1)
	AND c.eventId = @LEvent1
	AND c.Brand IS NOT NULL
	AND (
		(c.EDM = ISNULL(@LEDM, 0))
		OR (c.SMS = ISNULL(@LSMS, 0))
		OR (c.Ccenter = ISNULL(@LCCenter, 0))
	)
	GROUP BY c.Brand

-- Turn up sent 4A
	INSERT INTO @tmpSent
	SELECT 3, s.survey4a, COUNT(DISTINCT(s.consumerId)) 
	FROM vw_rpt_turnup_sent AS s
	WHERE (s.senddate BETWEEN @LDateFrom1 and @LDateTo1)
	AND s.eventId = @LEvent1
	AND (
		(s.EDM = ISNULL(@LEDM, 0))
		OR (s.SMS = ISNULL(@LSMS, 0))
		OR (s.Ccenter = ISNULL(@LCCenter, 0))
	)
	GROUP BY s.survey4a
	
-- Turn up communication 4A
	INSERT INTO @tmpComm
	SELECT 3, c.survey4a, COUNT(DISTINCT(c.redeem_consumer)) 
	FROM vw_rpt_turnup_comm c
	WHERE (c.senddate BETWEEN @LDateFrom1 and @LDateTo1)
	AND c.eventId = @LEvent1
	AND (
		(c.EDM = ISNULL(@LEDM, 0))
		OR (c.SMS = ISNULL(@LSMS, 0))
		OR (c.Ccenter = ISNULL(@LCCenter, 0))
	)	
	GROUP BY c.survey4a	

--insert Communication Rate of Gender
	INSERT INTO @ResultTable
	SELECT 20 + c.TypeValue, @LEvent1, @EventName,
	template.Name AS Name, 
	AVG(CASE WHEN ISNULL(s.Summary, 0) = 0 THEN 0 ELSE (CONVERT(NUMERIC, ISNULL(c.Summary, 0)) / CONVERT(NUMERIC, ISNULL(s.Summary, 0))) END) * 100 Ratio
	FROM (
		SELECT 0 AS TypeValue, 'Communication Rate of Gender' AS Name
		UNION
		SELECT 1 AS TypeValue, 'Communication Rate of Age' AS Name
		UNION
		SELECT 2 AS TypeValue, 'Communication Rate of BrandPerference' AS Name
		UNION
		SELECT 3 AS TypeValue, 'Communication Rate of 4A''s' AS Name
	) template LEFT OUTER JOIN @tmpComm c ON template.TypeValue = c.TypeValue
		LEFT OUTER JOIN @tmpSent s ON c.TypeValue = s.TypeValue
			AND c.GroupingValue = s.GroupingValue 
	GROUP BY c.TypeValue, template.Name

	SELECT * FROM @ResultTable
	ORDER BY Seq

END
