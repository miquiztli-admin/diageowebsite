USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_TurnupCompare_Brand_Graph]    Script Date: 11/02/2013 18:40:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_TurnupCompare_Brand_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_TurnupCompare_Brand_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <14/11/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_TurnupCompare_Brand_Graph](
	@Event as integer, @DateFrom as datetime, @DateTo as datetime,
	@Event1 as integer, @DateFrom1 as datetime, @DateTo1 as datetime,
	@EDM as bit,@SMS as bit,@CCenter as bit
)
AS
BEGIN
	
	-- Avoid parameter sniffing problems in SSRS
	DECLARE @LEvent INT, @LDateFrom DATETIME, @LDateTo DATETIME, @LEvent1 INT, @LDateFrom1 DATETIME, @LDateTo1 DATETIME, @LEDM BIT, @LSMS BIT, @LCCenter BIT
	SET @LEvent = @Event
	SET @LDateFrom = @DateFrom
	SET @LDateTo = @DateTo
	SET @LEvent1 = @Event1
	SET @LDateFrom1 = @DateFrom1
	SET @LDateTo1 = @DateTo1
	SET @LEDM = @EDM
	SET @LSMS = @SMS
	SET @LCCenter = @CCenter
	-- Avoid parameter sniffing problems in SSRS

	DECLARE @EventName VARCHAR(100)
	Declare @JWGSent bigint,@JWGCom bigint,@JWGAll bigint
	Declare @JWBSent bigint,@JWBCom bigint,@JWBAll bigint
	Declare @SmirnoffSent bigint,@SmirnoffCom bigint,@SmirnoffAll bigint
	Declare @JWRSent bigint,@JWRCom bigint,@JWRAll bigint
	Declare @BMSent bigint,@BMCom bigint,@BMAll bigint
	Declare @Param1 bigint,@Param2 bigint,@Param3 bigint
	Declare @ResultTable TABLE (Seq INT, EventID INT, EventName VARCHAR(100), Gtype Varchar(200),JWG bigint,JWB bigint,Smirnoff bigint,JWR bigint,BM bigint)

-- Event Name
	SELECT
	@EventName = eventName
	FROM DiageoDB..OutboundEventMaster
	WHERE eventId = @LEvent

--Sent
	SELECT 
	@JWGSent = COUNT(DISTINCT(CASE WHEN s.Brand = 'JWG' THEN s.consumerId ELSE NULL END)),
	@JWBSent = COUNT(DISTINCT(CASE WHEN s.Brand = 'JWB' THEN s.consumerId ELSE NULL END)),
	@SmirnoffSent = COUNT(DISTINCT(CASE WHEN s.Brand = 'SMI' THEN s.consumerId ELSE NULL END)),
	@JWRSent = COUNT(DISTINCT(CASE WHEN s.Brand = 'JWR' THEN s.consumerId ELSE NULL END)),
	@BMSent = COUNT(DISTINCT(CASE WHEN s.Brand = 'BEN' THEN s.consumerId ELSE NULL END))
	FROM vw_rpt_turnup_sent s
	WHERE (s.senddate BETWEEN @LDateFrom and @LDateTo)
	AND s.eventId = @LEvent
	AND s.Brand IS NOT NULL
	AND (
		(s.EDM = ISNULL(@LEDM, 0))
		OR (s.SMS = ISNULL(@LSMS, 0))
		OR (s.Ccenter = ISNULL(@LCCenter, 0))
	)

	-- Turn up communication
	SELECT
	@JWGCom = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWG' THEN c.redeem_consumer ELSE NULL END)),
	@JWBCom = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWB' THEN c.redeem_consumer ELSE NULL END)),
	@SmirnoffCom = COUNT(DISTINCT(CASE WHEN c.Brand = 'SMI' THEN c.redeem_consumer ELSE NULL END)),
	@JWRCom = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWR' THEN c.redeem_consumer ELSE NULL END)),
	@BMCom = COUNT(DISTINCT(CASE WHEN c.Brand = 'BEN' THEN c.redeem_consumer ELSE NULL END))
	FROM vw_rpt_turnup_comm c
	WHERE (c.senddate BETWEEN @LDateFrom and @LDateTo)
	AND c.eventId = @LEvent
	AND c.Brand IS NOT NULL
	AND (
		(c.EDM = ISNULL(@LEDM, 0))
		OR (c.SMS = ISNULL(@LSMS, 0))
		OR (c.Ccenter = ISNULL(@LCCenter, 0))
	)

	-- Turn up all
	SELECT
	@JWGAll = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWG' THEN c.redeem_consumer ELSE NULL END)),
	@JWBAll = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWB' THEN c.redeem_consumer ELSE NULL END)),
	@SmirnoffAll = COUNT(DISTINCT(CASE WHEN c.Brand = 'SMI' THEN c.redeem_consumer ELSE NULL END)),
	@JWRAll = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWR' THEN c.redeem_consumer ELSE NULL END)),
	@BMAll = COUNT(DISTINCT(CASE WHEN c.Brand = 'BEN' THEN c.redeem_consumer ELSE NULL END))
	FROM vw_rpt_turnup_comm c
	WHERE (c.senddate BETWEEN @LDateFrom and @LDateTo)
	AND c.Brand IS NOT NULL
	AND (
		(c.EDM = ISNULL(@LEDM, 0))
		OR (c.SMS = ISNULL(@LSMS, 0))
		OR (c.Ccenter = ISNULL(@LCCenter, 0))
	)
	INSERT INTO @ResultTable
	VALUES
		(10, @LEvent, @EventName, 'Sent',@JWGSent,@JWBSent,@SmirnoffSent,@JWRSent,@BMSent),
		(11, @LEvent, @EventName, 'Turn up from communication',@JWGCom,@JWBCom,@SmirnoffCom,@JWRCom,@BMCom),
		(12, @LEvent, @EventName, 'All Turn up',@JWGAll,@JWBAll,@SmirnoffAll,@JWRAll,@BMAll) 

-- Event Name
	SELECT
	@EventName = eventName
	FROM DiageoDB..OutboundEventMaster
	WHERE eventId = @LEvent1

--Sent
	SELECT 
	@JWGSent = COUNT(DISTINCT(CASE WHEN s.Brand = 'JWG' THEN s.consumerId ELSE NULL END)),
	@JWBSent = COUNT(DISTINCT(CASE WHEN s.Brand = 'JWB' THEN s.consumerId ELSE NULL END)),
	@SmirnoffSent = COUNT(DISTINCT(CASE WHEN s.Brand = 'SMI' THEN s.consumerId ELSE NULL END)),
	@JWRSent = COUNT(DISTINCT(CASE WHEN s.Brand = 'JWR' THEN s.consumerId ELSE NULL END)),
	@BMSent = COUNT(DISTINCT(CASE WHEN s.Brand = 'BEN' THEN s.consumerId ELSE NULL END))
	FROM vw_rpt_turnup_sent s
	WHERE (s.senddate BETWEEN @DateFrom1 and @LDateTo1)
	AND s.eventId = @LEvent1
	AND s.Brand IS NOT NULL
	AND (
		(s.EDM = ISNULL(@LEDM, 0))
		OR (s.SMS = ISNULL(@LSMS, 0))
		OR (s.Ccenter = ISNULL(@LCCenter, 0))
	)

	-- Turn up communication
	SELECT
	@JWGCom = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWG' THEN c.redeem_consumer ELSE NULL END)),
	@JWBCom = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWB' THEN c.redeem_consumer ELSE NULL END)),
	@SmirnoffCom = COUNT(DISTINCT(CASE WHEN c.Brand = 'SMI' THEN c.redeem_consumer ELSE NULL END)),
	@JWRCom = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWR' THEN c.redeem_consumer ELSE NULL END)),
	@BMCom = COUNT(DISTINCT(CASE WHEN c.Brand = 'BEN' THEN c.redeem_consumer ELSE NULL END))
	FROM vw_rpt_turnup_comm c
	WHERE (c.senddate BETWEEN @DateFrom1 and @LDateTo1)
	AND c.eventId = @LEvent1
	AND c.Brand IS NOT NULL
	AND (
		(c.EDM = ISNULL(@LEDM, 0))
		OR (c.SMS = ISNULL(@LSMS, 0))
		OR (c.Ccenter = ISNULL(@LCCenter, 0))
	)

	-- Turn up all
	SELECT
	@JWGAll = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWG' THEN c.redeem_consumer ELSE NULL END)),
	@JWBAll = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWB' THEN c.redeem_consumer ELSE NULL END)),
	@SmirnoffAll = COUNT(DISTINCT(CASE WHEN c.Brand = 'SMI' THEN c.redeem_consumer ELSE NULL END)),
	@JWRAll = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWR' THEN c.redeem_consumer ELSE NULL END)),
	@BMAll = COUNT(DISTINCT(CASE WHEN c.Brand = 'BEN' THEN c.redeem_consumer ELSE NULL END))
	FROM vw_rpt_turnup_comm c
	WHERE (c.senddate BETWEEN @DateFrom1 and @LDateTo1)
	AND c.Brand IS NOT NULL
	AND (
		(c.EDM = ISNULL(@LEDM, 0))
		OR (c.SMS = ISNULL(@LSMS, 0))
		OR (c.Ccenter = ISNULL(@LCCenter, 0))
	)
	INSERT INTO @ResultTable
	VALUES
		(20, @LEvent1, @EventName, 'Sent',@JWGSent,@JWBSent,@SmirnoffSent,@JWRSent,@BMSent),
		(21, @LEvent1, @EventName, 'Turn up from communication',@JWGCom,@JWBCom,@SmirnoffCom,@JWRCom,@BMCom),
		(22, @LEvent1, @EventName, 'All Turn up',@JWGAll,@JWBAll,@SmirnoffAll,@JWRAll,@BMAll) 

	SELECT * FROM @ResultTable
	ORDER BY Seq

END
