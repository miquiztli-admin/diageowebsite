USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_TurnupCompare_Gender]    Script Date: 11/02/2013 18:38:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'dbo.Sp_Rep_TurnupCompare_Gender', N'P') IS NOT NULL
    DROP PROC Sp_Rep_TurnupCompare_Gender;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <13/11/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_TurnupCompare_Gender](
	@Event as integer, @DateFrom as datetime, @DateTo as datetime,
	@Event1 as integer, @DateFrom1 as datetime, @DateTo1 as datetime,
	@EDM as bit,@SMS as bit,@CCenter as bit
)
AS
BEGIN

	-- Avoid parameter sniffing problems in SSRS
	DECLARE @LEvent INT, @LDateFrom DATETIME, @LDateTo DATETIME, @LEvent1 INT, @LDateFrom1 DATETIME, @LDateTo1 DATETIME, @LEDM BIT, @LSMS BIT, @LCCenter BIT
	SET @LEvent = @Event
	SET @LDateFrom = @DateFrom
	SET @LDateTo = @DateTo
	SET @LEvent1 = @Event1
	SET @LDateFrom1 = @DateFrom1
	SET @LDateTo1 = @DateTo1
	SET @LEDM = @EDM
	SET @LSMS = @SMS
	SET @LCCenter = @CCenter
	-- Avoid parameter sniffing problems in SSRS

	Declare @FemaleSent bigint,@FemaleCom bigint,@FemaleAll bigint
	Declare @MaleSent bigint,@MaleCom bigint,@MaleAll bigint
	Declare @UnknownSent bigint,@UnknownCom bigint,@UnknownAll bigint

	DECLARE @EventName VARCHAR(100)
	Declare @ResultTable TABLE (Seq INT, EventID INT, EventName VARCHAR(100), Gender Varchar(20),sents bigint,communicate bigint,allturnup bigint)

-- Event Name
	SELECT
	@EventName = eventName
	FROM DiageoDB..OutboundEventMaster
	WHERE eventId = @LEvent

-- Turn up sent
	SELECT 
	@MaleSent = COUNT(DISTINCT(CASE WHEN lower(gender) = 'male' THEN consumerId ELSE NULL END)),
	@FemaleSent = COUNT(DISTINCT(CASE WHEN lower(gender) = 'female' THEN consumerId ELSE NULL END)),
	@UnknownSent = COUNT(DISTINCT(CASE WHEN (gender IS NULL OR (lower(gender) != 'male' AND lower(gender) != 'female')) THEN consumerId ELSE NULL END))
	FROM vw_rpt_turnup_sent
	WHERE (senddate BETWEEN @LDateFrom and @LDateTo)
	AND eventId = @LEvent
	AND (
		(EDM = ISNULL(@LEDM, 0))
		OR (SMS = ISNULL(@LSMS, 0))
		OR (Ccenter = ISNULL(@LCCenter, 0))
	)
	
-- Turn up from communication 
	SELECT 
	@MaleCom = COUNT(DISTINCT(CASE WHEN lower(gender) = 'male' THEN redeem_consumer ELSE NULL END)),
	@FemaleCom = COUNT(DISTINCT(CASE WHEN lower(gender) = 'female' THEN redeem_consumer ELSE NULL END)),
	@UnknownCom = COUNT(DISTINCT(CASE WHEN (gender IS NULL OR (lower(gender) != 'male' AND lower(gender) != 'female')) THEN redeem_consumer ELSE NULL END))
	FROM vw_rpt_turnup_comm
	WHERE (senddate BETWEEN @LDateFrom and @LDateTo)
	AND eventId = @LEvent
	AND (
		(EDM = ISNULL(@LEDM, 0))
		OR (SMS = ISNULL(@LSMS, 0))
		OR (Ccenter = ISNULL(@LCCenter, 0))
	)
 
-- All Turn up
	SELECT 
	@MaleAll = COUNT(DISTINCT(CASE WHEN lower(gender) = 'male' THEN redeem_consumer ELSE NULL END)),
	@FemaleAll = COUNT(DISTINCT(CASE WHEN lower(gender) = 'female' THEN redeem_consumer ELSE NULL END)),
	@UnknownAll = COUNT(DISTINCT(CASE WHEN (gender IS NULL OR (lower(gender) != 'male' AND lower(gender) != 'female')) THEN redeem_consumer ELSE NULL END))
	FROM vw_rpt_turnup_comm
	WHERE (senddate BETWEEN @LDateFrom and @LDateTo) 
	AND (
		(EDM = ISNULL(@LEDM, 0))
		OR (SMS = ISNULL(@LSMS, 0))
		OR (Ccenter = ISNULL(@LCCenter, 0))
	)
 
	INSERT INTO @ResultTable
	VALUES 
		(11, @Event, @EventName, 'Female',@FemaleSent,@FemaleCom,@FemaleAll),
		(12, @Event, @EventName, 'Male',@MaleSent,@MaleCom,@MaleAll),
		(13, @Event, @EventName, 'Unknown',@UnknownSent,@UnknownCom,@UnknownAll)

-- Event Name
	SELECT
	@EventName = eventName
	FROM DiageoDB..OutboundEventMaster
	WHERE eventId = @LEvent1

-- Turn up sent
	SELECT 
	@MaleSent = COUNT(DISTINCT(CASE WHEN lower(gender) = 'male' THEN consumerId ELSE NULL END)),
	@FemaleSent = COUNT(DISTINCT(CASE WHEN lower(gender) = 'female' THEN consumerId ELSE NULL END)),
	@UnknownSent = COUNT(DISTINCT(CASE WHEN (gender IS NULL OR (lower(gender) != 'male' AND lower(gender) != 'female')) THEN consumerId ELSE NULL END))
	FROM vw_rpt_turnup_sent
	WHERE (senddate BETWEEN @LDateFrom1 and @LDateTo1)
	AND eventId = @LEvent1
	AND (
		(EDM = ISNULL(@LEDM, 0))
		OR (SMS = ISNULL(@LSMS, 0))
		OR (Ccenter = ISNULL(@LCCenter, 0))
	)
	
-- Turn up from communication 
	SELECT 
	@MaleCom = COUNT(DISTINCT(CASE WHEN lower(gender) = 'male' THEN redeem_consumer ELSE NULL END)),
	@FemaleCom = COUNT(DISTINCT(CASE WHEN lower(gender) = 'female' THEN redeem_consumer ELSE NULL END)),
	@UnknownCom = COUNT(DISTINCT(CASE WHEN (gender IS NULL OR (lower(gender) != 'male' AND lower(gender) != 'female')) THEN redeem_consumer ELSE NULL END))
	FROM vw_rpt_turnup_comm
	WHERE (senddate BETWEEN @LDateFrom1 and @LDateTo1)
	AND eventId = @LEvent1
	AND (
		(EDM = ISNULL(@LEDM, 0))
		OR (SMS = ISNULL(@LSMS, 0))
		OR (Ccenter = ISNULL(@LCCenter, 0))
	)
 
-- All Turn up
	SELECT 
	@MaleAll = COUNT(DISTINCT(CASE WHEN lower(gender) = 'male' THEN redeem_consumer ELSE NULL END)),
	@FemaleAll = COUNT(DISTINCT(CASE WHEN lower(gender) = 'female' THEN redeem_consumer ELSE NULL END)),
	@UnknownAll = COUNT(DISTINCT(CASE WHEN (gender IS NULL OR (lower(gender) != 'male' AND lower(gender) != 'female')) THEN redeem_consumer ELSE NULL END))
	FROM vw_rpt_turnup_comm
	WHERE (senddate BETWEEN @LDateFrom1 and @LDateTo1) 
	AND (
		(EDM = ISNULL(@LEDM, 0))
		OR (SMS = ISNULL(@LSMS, 0))
		OR (Ccenter = ISNULL(@LCCenter, 0))
	)
 
	INSERT INTO @ResultTable
	VALUES 
		(21, @Event1, @EventName, 'Female',@FemaleSent,@FemaleCom,@FemaleAll),
		(22, @Event1, @EventName, 'Male',@MaleSent,@MaleCom,@MaleAll),
		(23, @Event1, @EventName, 'Unknown',@UnknownSent,@UnknownCom,@UnknownAll)


	SELECT * FROM @ResultTable
	ORDER BY Seq

END
