USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Turnup_Age_Graph]    Script Date: 11/02/2013 18:41:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Turnup_Age_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Turnup_Age_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <02/11/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Turnup_Age_Graph](
	@Event as integer, @DateFrom as datetime, @DateTo as datetime, @EDM as bit, @SMS as bit, @CCenter as bit
)
AS
BEGIN
	
	-- Avoid parameter sniffing problems in SSRS
	DECLARE @LEvent INT, @LDateFrom DATETIME, @LDateTo DATETIME, @LEDM BIT, @LSMS BIT, @LCCenter BIT
	SET @LEvent = @Event
	SET @LDateFrom = @DateFrom
	SET @LDateTo = @DateTo
	SET @LEDM = @EDM
	SET @LSMS = @SMS
	SET @LCCenter = @CCenter
	-- Avoid parameter sniffing problems in SSRS

	DECLARE @ResultTable TABLE (Gtype Varchar(200), A2124 bigint, A2529 bigint, A3034 bigint, A3539 bigint, A40 bigint)

	-- Turn up sent
	INSERT INTO @ResultTable
	SELECT 'Sent', 
	COUNT(DISTINCT(CASE WHEN ageRange = 1 THEN s.consumerId ELSE NULL END)),
	COUNT(DISTINCT(CASE WHEN ageRange = 2 THEN s.consumerId ELSE NULL END)),
	COUNT(DISTINCT(CASE WHEN ageRange = 3 THEN s.consumerId ELSE NULL END)),
	COUNT(DISTINCT(CASE WHEN ageRange = 4 THEN s.consumerId ELSE NULL END)),
	COUNT(DISTINCT(CASE WHEN ageRange = 5 THEN s.consumerId ELSE NULL END))
	FROM (
		SELECT consumerId,
		CASE
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 20 AND 24 THEN 1 
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 25 AND 29 THEN 2
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 30 AND 34 THEN 3
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 35 AND 39 THEN 4
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) >= 40 THEN 5
		END AS ageRange
		FROM vw_rpt_turnup_sent
		WHERE (senddate BETWEEN @LDateFrom and @LDateTo)
		AND eventId = @LEvent
		AND (
			(EDM = ISNULL(@LEDM, 0))
			OR (SMS = ISNULL(@LSMS, 0))
			OR (Ccenter = ISNULL(@LCCenter, 0))
		)
	) AS s

	-- Turn up communication
	INSERT INTO @ResultTable
	SELECT 'Turn up from communication', 
	COUNT(DISTINCT(CASE WHEN ageRange = 1 THEN s.redeem_consumer ELSE NULL END)),
	COUNT(DISTINCT(CASE WHEN ageRange = 2 THEN s.redeem_consumer ELSE NULL END)),
	COUNT(DISTINCT(CASE WHEN ageRange = 3 THEN s.redeem_consumer ELSE NULL END)),
	COUNT(DISTINCT(CASE WHEN ageRange = 4 THEN s.redeem_consumer ELSE NULL END)),
	COUNT(DISTINCT(CASE WHEN ageRange = 5 THEN s.redeem_consumer ELSE NULL END))
		FROM (
		SELECT redeem_consumer,
		CASE
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 20 AND 24 THEN 1
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 25 AND 29 THEN 2
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 30 AND 34 THEN 3
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 35 AND 39 THEN 4
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) >= 40 THEN 5
		END AS ageRange
		FROM vw_rpt_turnup_comm
		WHERE (senddate BETWEEN @LDateFrom and @LDateTo)
		AND eventId = @LEvent
		AND (
			(EDM = ISNULL(@LEDM, 0))
			OR (SMS = ISNULL(@LSMS, 0))
			OR (Ccenter = ISNULL(@LCCenter, 0))
		)
	) AS s	

	-- Turn up all
	INSERT INTO @ResultTable
	SELECT 'All Turn up', 
	COUNT(DISTINCT(CASE WHEN ageRange = 1 THEN s.redeem_consumer ELSE NULL END)),
	COUNT(DISTINCT(CASE WHEN ageRange = 2 THEN s.redeem_consumer ELSE NULL END)),
	COUNT(DISTINCT(CASE WHEN ageRange = 3 THEN s.redeem_consumer ELSE NULL END)),
	COUNT(DISTINCT(CASE WHEN ageRange = 4 THEN s.redeem_consumer ELSE NULL END)),
	COUNT(DISTINCT(CASE WHEN ageRange = 5 THEN s.redeem_consumer ELSE NULL END))
		FROM (
		SELECT redeem_consumer,
		CASE
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 20 AND 24 THEN 1
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 25 AND 29 THEN 2
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 30 AND 34 THEN 3
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 35 AND 39 THEN 4
			WHEN DATEDIFF(year, dateofbirth, GETDATE()) >= 40 THEN 5
		END AS ageRange
		FROM vw_rpt_turnup_comm
		WHERE (senddate BETWEEN @LDateFrom and @LDateTo)
		AND (
			(EDM = ISNULL(@LEDM, 0))
			OR (SMS = ISNULL(@LSMS, 0))
			OR (Ccenter = ISNULL(@LCCenter, 0))
		)
	) AS s	

	SELECT * FROM @ResultTable
	
END

