USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Turnup_4A_Graph]    Script Date: 11/02/2013 18:41:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Turnup_4A_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Turnup_4A_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <02/11/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Turnup_4A_Graph](
	@Event as integer,@DateFrom as datetime,@DateTo as datetime,@EDM as bit,@SMS as bit,@CCenter as bit
)
AS
BEGIN

	-- Avoid parameter sniffing problems in SSRS
	DECLARE @LEvent INT, @LDateFrom DATETIME, @LDateTo DATETIME, @LEDM BIT, @LSMS BIT, @LCCenter BIT
	SET @LEvent = @Event
	SET @LDateFrom = @DateFrom
	SET @LDateTo = @DateTo
	SET @LEDM = @EDM
	SET @LSMS = @SMS
	SET @LCCenter = @CCenter
	-- Avoid parameter sniffing problems in SSRS

	Declare @AdorerSent bigint,@AdorerCom bigint,@AdorerAll bigint
	Declare @AdopterSent bigint,@AdopterCom bigint,@AdopterAll bigint
	Declare @AvailableSent bigint,@AvailableCom bigint,@AvailableAll bigint
	Declare @AccepterSent bigint,@AccepterCom bigint,@AccepterAll bigint
	Declare @RejecterSent bigint,@RejecterCom bigint,@RejecterAll bigint
	Declare @NASent bigint,@NACom bigint,@NAAll bigint

	DECLARE @ResultTable TABLE (Gtype VARCHAR(200), indexs INT, Adorer BIGINT, Adopter BIGINT, 
		Acceptor BIGINT, Available BIGINT, Rejector BIGINT, NA BIGINT)

	-- Turn up sent
	SELECT 
	@AdorerSent = COUNT(DISTINCT(CASE WHEN survey4a = 1 THEN s.consumerId ELSE NULL END)),
	@AdopterSent = COUNT(DISTINCT(CASE WHEN survey4a = 2 THEN s.consumerId ELSE NULL END)),
	@AvailableSent = COUNT(DISTINCT(CASE WHEN survey4a = 3 THEN s.consumerId ELSE NULL END)),
	@AccepterSent = COUNT(DISTINCT(CASE WHEN survey4a = 4 THEN s.consumerId ELSE NULL END)),
	@RejecterSent = COUNT(DISTINCT(CASE WHEN survey4a = 5 THEN s.consumerId ELSE NULL END)),
	@NASent = COUNT(DISTINCT(CASE WHEN survey4a IS NULL THEN s.consumerId ELSE NULL END))
	FROM vw_rpt_turnup_sent AS s
	WHERE (s.senddate BETWEEN @LDateFrom and @LDateTo)
	AND s.eventId = @LEvent
	AND (
		(s.EDM = ISNULL(@LEDM, 0))
		OR (s.SMS = ISNULL(@LSMS, 0))
		OR (s.Ccenter = ISNULL(@LCCenter, 0))
	)
	
	-- Turn up communication
	SELECT
	@AdorerCom = COUNT(DISTINCT(CASE WHEN survey4a = 1 THEN c.redeem_consumer ELSE NULL END)),
	@AdopterCom = COUNT(DISTINCT(CASE WHEN survey4a = 2 THEN c.redeem_consumer ELSE NULL END)),
	@AvailableCom = COUNT(DISTINCT(CASE WHEN survey4a = 3 THEN c.redeem_consumer ELSE NULL END)),
	@AccepterCom = COUNT(DISTINCT(CASE WHEN survey4a = 4 THEN c.redeem_consumer ELSE NULL END)),
	@RejecterCom = COUNT(DISTINCT(CASE WHEN survey4a = 5 THEN c.redeem_consumer ELSE NULL END)),
	@NACom = COUNT(DISTINCT(CASE WHEN survey4a IS NULL THEN c.redeem_consumer ELSE NULL END))
	FROM vw_rpt_turnup_comm c
	WHERE (c.senddate BETWEEN @LDateFrom and @LDateTo)
	AND c.eventId = @LEvent
	AND (
		(c.EDM = ISNULL(@LEDM, 0))
		OR (c.SMS = ISNULL(@LSMS, 0))
		OR (c.Ccenter = ISNULL(@LCCenter, 0))
	)		
		
	-- Turn up all
	SELECT
	@AdorerAll = COUNT(DISTINCT(CASE WHEN survey4a = 1 THEN c.redeem_consumer ELSE NULL END)),
	@AdopterAll = COUNT(DISTINCT(CASE WHEN survey4a = 2 THEN c.redeem_consumer ELSE NULL END)),
	@AvailableAll = COUNT(DISTINCT(CASE WHEN survey4a = 3 THEN c.redeem_consumer ELSE NULL END)),
	@AccepterAll = COUNT(DISTINCT(CASE WHEN survey4a = 4 THEN c.redeem_consumer ELSE NULL END)),
	@RejecterAll = COUNT(DISTINCT(CASE WHEN survey4a = 5 THEN c.redeem_consumer ELSE NULL END)),
	@NAAll = COUNT(DISTINCT(CASE WHEN survey4a IS NULL THEN c.redeem_consumer ELSE NULL END))
	FROM vw_rpt_turnup_comm c
	WHERE (c.senddate BETWEEN @LDateFrom and @LDateTo)
	AND (
		(c.EDM = ISNULL(@LEDM, 0))
		OR (c.SMS = ISNULL(@LSMS, 0))
		OR (c.Ccenter = ISNULL(@LCCenter, 0))
	)		
		
	INSERT INTO @ResultTable
	VALUES 
		('Sent', 1, @AdorerSent, @AdopterSent, @AvailableSent, @AccepterSent, @RejecterSent, @NASent),
		('Turn up from communication', 2, @AdorerCom, @AdopterCom, @AvailableCom, @AccepterCom, @RejecterCom, @NACom),
		('All Turn up', 3, @AdorerAll, @AdopterAll, @AvailableAll, @AccepterAll, @RejecterAll, @NAAll)

	SELECT * FROM @ResultTable

END
