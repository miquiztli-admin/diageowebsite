USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Turnup_Gender_Graph]    Script Date: 11/02/2013 18:38:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID (N'dbo.Sp_Rep_Turnup_Gender_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Turnup_Gender_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <02/11/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE  PROCEDURE [dbo].[Sp_Rep_Turnup_Gender_Graph](
	@Event as integer,@DateFrom as datetime,@DateTo as datetime,@EDM as bit,@SMS as bit,@CCenter as bit
)
AS
BEGIN

	-- Avoid parameter sniffing problems in SSRS
	DECLARE @LEvent INT, @LDateFrom DATETIME, @LDateTo DATETIME, @LEDM BIT, @LSMS BIT, @LCCenter BIT
	SET @LEvent = @Event
	SET @LDateFrom = @DateFrom
	SET @LDateTo = @DateTo
	SET @LEDM = @EDM
	SET @LSMS = @SMS
	SET @LCCenter = @CCenter
	-- Avoid parameter sniffing problems in SSRS
	
	Declare @FemaleSent bigint,@FemaleCom bigint,@FemaleAll bigint
	Declare @MaleSent bigint,@MaleCom bigint,@MaleAll bigint
	Declare @UnknownSent bigint,@UnknownCom bigint,@UnknownAll bigint
	Declare @ResultTable TABLE (Gtype Varchar(200), Female bigint, Male bigint, Unknown bigint)

-- Turn up sent
	SELECT 
	@MaleSent = COUNT(DISTINCT(CASE WHEN lower(gender) = 'male' THEN consumerId ELSE NULL END)),
	@FemaleSent = COUNT(DISTINCT(CASE WHEN lower(gender) = 'female' THEN consumerId ELSE NULL END)),
	@UnknownSent = COUNT(DISTINCT(CASE WHEN (gender IS NULL OR (lower(gender) != 'male' AND lower(gender) != 'female')) THEN consumerId ELSE NULL END))
	FROM vw_rpt_turnup_sent
	WHERE (senddate BETWEEN @LDateFrom and @LDateTo)
	AND eventId = @LEvent
	AND (
		(EDM = ISNULL(@LEDM, 0))
		OR (SMS = ISNULL(@LSMS, 0))
		OR (Ccenter = ISNULL(@LCCenter, 0))
	)

-- Turn up from communication 
	SELECT 
	@MaleCom = COUNT(DISTINCT(CASE WHEN lower(gender) = 'male' THEN redeem_consumer ELSE NULL END)),
	@FemaleCom = COUNT(DISTINCT(CASE WHEN lower(gender) = 'female' THEN redeem_consumer ELSE NULL END)),
	@UnknownCom = COUNT(DISTINCT(CASE WHEN (gender IS NULL OR (lower(gender) != 'male' AND lower(gender) != 'female')) THEN redeem_consumer ELSE NULL END))
	FROM vw_rpt_turnup_comm
	WHERE (senddate BETWEEN @LDateFrom and @LDateTo)
	AND eventId = @LEvent
	AND (
		(EDM = ISNULL(@LEDM, 0))
		OR (SMS = ISNULL(@LSMS, 0))
		OR (Ccenter = ISNULL(@LCCenter, 0))
	)

-- All Turn up
	SELECT 
	@MaleAll = COUNT(DISTINCT(CASE WHEN lower(gender) = 'male' THEN redeem_consumer ELSE NULL END)),
	@FemaleAll = COUNT(DISTINCT(CASE WHEN lower(gender) = 'female' THEN redeem_consumer ELSE NULL END)),
	@UnknownAll = COUNT(DISTINCT(CASE WHEN (gender IS NULL OR (lower(gender) != 'male' AND lower(gender) != 'female')) THEN redeem_consumer ELSE NULL END))
	FROM vw_rpt_turnup_comm
	WHERE (senddate BETWEEN @LDateFrom and @LDateTo) 
	AND (
		(EDM = ISNULL(@LEDM, 0))
		OR (SMS = ISNULL(@LSMS, 0))
		OR (Ccenter = ISNULL(@LCCenter, 0))
	)
  
	INSERT INTO @ResultTable
	VALUES 
		('Sent',@FemaleSent, @MaleSent, @UnknownSent),
		('Turn up from communication', @FemaleCom, @MaleCom, @UnknownCom),
		('All Turn up',@FemaleAll, @MaleAll, @UnknownAll)

	SELECT * FROM @ResultTable

END
