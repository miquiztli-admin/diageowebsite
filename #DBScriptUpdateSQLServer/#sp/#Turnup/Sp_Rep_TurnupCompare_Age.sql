USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_TurnupCompare_Age]    Script Date: 11/02/2013 18:41:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_TurnupCompare_Age', N'P') IS NOT NULL
    DROP PROC Sp_Rep_TurnupCompare_Age;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <14/11/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_TurnupCompare_Age](
	@Event as integer, @DateFrom as datetime, @DateTo as datetime,
	@Event1 as integer, @DateFrom1 as datetime, @DateTo1 as datetime,
	@EDM as bit,@SMS as bit,@CCenter as bit
)
AS
BEGIN
	
	-- Avoid parameter sniffing problems in SSRS
	DECLARE @LEvent INT, @LDateFrom DATETIME, @LDateTo DATETIME, @LEvent1 INT, @LDateFrom1 DATETIME, @LDateTo1 DATETIME, @LEDM BIT, @LSMS BIT, @LCCenter BIT
	SET @LEvent = @Event
	SET @LDateFrom = @DateFrom
	SET @LDateTo = @DateTo
	SET @LEvent1 = @Event1
	SET @LDateFrom1 = @DateFrom1
	SET @LDateTo1 = @DateTo1
	SET @LEDM = @EDM
	SET @LSMS = @SMS
	SET @LCCenter = @CCenter
	-- Avoid parameter sniffing problems in SSRS
	DECLARE @EventName VARCHAR(100)
	DECLARE @EventName1 VARCHAR(100)
	
-- Event Name
	SELECT
	@EventName = eventName
	FROM DiageoDB..OutboundEventMaster
	WHERE eventId = @LEvent

-- Event Name
	SELECT
	@EventName1 = eventName
	FROM DiageoDB..OutboundEventMaster
	WHERE eventId = @LEvent1

	SELECT 1 AS Seq, @LEvent AS EventID, @EventName AS EventName, template.age, ISNULL(sents.Summary, 0) AS TurnSent, 
	ISNULL(comm.Summary, 0) AS TurnCom, ISNULL(t_all.Summary, 0) AS TurnAll
	FROM (
		SELECT '20-24' AS age
		UNION
		SELECT '25-29' AS age
		UNION
		SELECT '30-34' AS age
		UNION
		SELECT '35-39' AS age
		UNION
		SELECT '40+' AS age
	) AS template LEFT OUTER JOIN (
			-- Turn up sent
			SELECT s.ageRange AS age, COUNT(DISTINCT(s.consumerId)) AS Summary
			FROM (
				SELECT consumerId,
				CASE
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 20 AND 24 THEN '20-24' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 25 AND 29 THEN '25-29' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 30 AND 34 THEN '30-34' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 35 AND 39 THEN '35-39' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) >= 40 THEN '40+'
				END AS ageRange
				FROM vw_rpt_turnup_sent
				WHERE (senddate BETWEEN @LDateFrom and @LDateTo)
				AND eventId = @LEvent
				AND (
					(EDM = ISNULL(@LEDM, 0))
					OR (SMS = ISNULL(@LSMS, 0))
					OR (Ccenter = ISNULL(@LCCenter, 0))
				)
			) AS s
			GROUP BY s.ageRange
		) AS sents ON template.age = sents.age
		LEFT OUTER JOIN (
			-- Turn up communication
			SELECT s.ageRange AS age, COUNT(DISTINCT(s.redeem_consumer)) AS Summary
			FROM (
				SELECT redeem_consumer,
				CASE
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 20 AND 24 THEN '20-24' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 25 AND 29 THEN '25-29' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 30 AND 34 THEN '30-34' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 35 AND 39 THEN '35-39' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) >= 40 THEN '40+'
				END AS ageRange
				FROM vw_rpt_turnup_comm
				WHERE (senddate BETWEEN @LDateFrom and @LDateTo)
				AND eventId = @LEvent
				AND (
					(EDM = ISNULL(@LEDM, 0))
					OR (SMS = ISNULL(@LSMS, 0))
					OR (Ccenter = ISNULL(@LCCenter, 0))
				)
			) AS s
			GROUP BY s.ageRange
		) AS comm ON template.age = comm.age
		LEFT OUTER JOIN (
			-- Turn up all
			SELECT s.ageRange AS age, COUNT(DISTINCT(s.redeem_consumer)) AS Summary
			FROM (
				SELECT redeem_consumer,
				CASE
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 20 AND 24 THEN '20-24' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 25 AND 29 THEN '25-29' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 30 AND 34 THEN '30-34' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 35 AND 39 THEN '35-39' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) >= 40 THEN '40+'
				END AS ageRange
				FROM vw_rpt_turnup_comm
				WHERE (senddate BETWEEN @LDateFrom and @LDateTo)
				AND (
					(EDM = ISNULL(@LEDM, 0))
					OR (SMS = ISNULL(@LSMS, 0))
					OR (Ccenter = ISNULL(@LCCenter, 0))
				)
			) AS s
			GROUP BY s.ageRange
		) AS t_all ON template.age = t_all.age

	UNION ALL
	
	SELECT 2 AS Seq, @LEvent1 AS EventID, @EventName1 AS EventName, template.age, ISNULL(sents.Summary, 0) AS TurnSent, 
	ISNULL(comm.Summary, 0) AS TurnCom, ISNULL(t_all.Summary, 0) AS TurnAll
	FROM (
		SELECT '20-24' AS age
		UNION
		SELECT '25-29' AS age
		UNION
		SELECT '30-34' AS age
		UNION
		SELECT '35-39' AS age
		UNION
		SELECT '40+' AS age
	) AS template LEFT OUTER JOIN (
		-- Turn up sent
			SELECT s.ageRange AS age, COUNT(DISTINCT(s.consumerId)) AS Summary
			FROM (
				SELECT consumerId,
				CASE
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 20 AND 24 THEN '20-24' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 25 AND 29 THEN '25-29' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 30 AND 34 THEN '30-34' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 35 AND 39 THEN '35-39' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) >= 40 THEN '40+'
				END AS ageRange
				FROM vw_rpt_turnup_sent
				WHERE (senddate BETWEEN @LDateFrom1 and @LDateTo1)
				AND eventId = @LEvent1
				AND (
					(EDM = ISNULL(@LEDM, 0))
					OR (SMS = ISNULL(@LSMS, 0))
					OR (Ccenter = ISNULL(@LCCenter, 0))
				)
			) AS s
			GROUP BY s.ageRange
		) AS sents ON template.age = sents.age
		LEFT OUTER JOIN (
			-- Turn up communication
			SELECT s.ageRange AS age, COUNT(DISTINCT(s.redeem_consumer)) AS Summary
			FROM (
				SELECT redeem_consumer,
				CASE
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 20 AND 24 THEN '20-24' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 25 AND 29 THEN '25-29' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 30 AND 34 THEN '30-34' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 35 AND 39 THEN '35-39' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) >= 40 THEN '40+'
				END AS ageRange
				FROM vw_rpt_turnup_comm
				WHERE (senddate BETWEEN @LDateFrom1 and @LDateTo1)
				AND eventId = @LEvent1
				AND (
					(EDM = ISNULL(@LEDM, 0))
					OR (SMS = ISNULL(@LSMS, 0))
					OR (Ccenter = ISNULL(@LCCenter, 0))
				)
			) AS s
			GROUP BY s.ageRange
		) AS comm ON template.age = comm.age
		LEFT OUTER JOIN (
			-- Turn up all
			SELECT s.ageRange AS age, COUNT(DISTINCT(s.redeem_consumer)) AS Summary
			FROM (
				SELECT redeem_consumer,
				CASE
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 20 AND 24 THEN '20-24' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 25 AND 29 THEN '25-29' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 30 AND 34 THEN '30-34' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) BETWEEN 35 AND 39 THEN '35-39' 
					WHEN DATEDIFF(year, dateofbirth, GETDATE()) >= 40 THEN '40+'
				END AS ageRange
				FROM vw_rpt_turnup_comm
				WHERE (senddate BETWEEN @LDateFrom1 and @LDateTo1)
				AND (
					(EDM = ISNULL(@LEDM, 0))
					OR (SMS = ISNULL(@LSMS, 0))
					OR (Ccenter = ISNULL(@LCCenter, 0))
				)
			) AS s
			GROUP BY s.ageRange
		) AS t_all ON template.age = t_all.age
		
		ORDER BY 1, 4

END

