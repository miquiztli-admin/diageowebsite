USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_TurnupCompare_4A]    Script Date: 11/02/2013 18:41:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_TurnupCompare_4A', N'P') IS NOT NULL
    DROP PROC Sp_Rep_TurnupCompare_4A;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <14/11/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_TurnupCompare_4A](
	@Event as integer, @DateFrom as datetime, @DateTo as datetime,
	@Event1 as integer, @DateFrom1 as datetime, @DateTo1 as datetime,
	@EDM as bit,@SMS as bit,@CCenter as bit
)
AS
BEGIN
	
	-- Avoid parameter sniffing problems in SSRS
	DECLARE @LEvent INT, @LDateFrom DATETIME, @LDateTo DATETIME, @LEvent1 INT, @LDateFrom1 DATETIME, @LDateTo1 DATETIME, @LEDM BIT, @LSMS BIT, @LCCenter BIT
	SET @LEvent = @Event
	SET @LDateFrom = @DateFrom
	SET @LDateTo = @DateTo
	SET @LEvent1 = @Event1
	SET @LDateFrom1 = @DateFrom1
	SET @LDateTo1 = @DateTo1
	SET @LEDM = @EDM
	SET @LSMS = @SMS
	SET @LCCenter = @CCenter
	-- Avoid parameter sniffing problems in SSRS

	DECLARE @EventName VARCHAR(100)
	DECLARE @EventName1 VARCHAR(100)

-- Event Name
	SELECT
	@EventName = eventName
	FROM DiageoDB..OutboundEventMaster
	WHERE eventId = @LEvent

-- Event Name
	SELECT
	@EventName1 = eventName
	FROM DiageoDB..OutboundEventMaster
	WHERE eventId = @LEvent1
	
	SELECT 1, @LEvent AS EventID, @EventName AS EventName,  
	template.survey4a, template.disp AS Type, 
	ISNULL(sents.Summary, 0) AS Sent4A, ISNULL(comm.Summary, 0) AS Com4A, ISNULL(t_all.Summary, 0) AS All4A
	FROM (
		SELECT 1 AS survey4a, 'Adorer' AS disp
		UNION
		SELECT 2 AS survey4a, 'Adopter' AS disp
		UNION
		SELECT 3 AS survey4a, 'Acceptor' AS disp
		UNION
		SELECT 4 AS survey4a, 'Available' AS disp
		UNION
		SELECT 5 AS survey4a, 'Rejector' AS disp
		UNION
		SELECT 6 AS survey4a, 'NA' AS disp
	) template LEFT OUTER JOIN (
			-- Turn up sent
			SELECT ISNULL(s.survey4a, 6) AS survey4a, COUNT(DISTINCT(s.consumerId)) AS Summary
			FROM vw_rpt_turnup_sent AS s
			WHERE (s.senddate BETWEEN @LDateFrom and @LDateTo)
			AND s.eventId = @LEvent
			AND (
				(s.EDM = ISNULL(@LEDM, 0))
				OR (s.SMS = ISNULL(@LSMS, 0))
				OR (s.Ccenter = ISNULL(@LCCenter, 0))
			)
			GROUP BY s.survey4a
		) AS sents ON template.survey4a = sents.survey4a
		LEFT OUTER JOIN (
			-- Turn up communication
			SELECT ISNULL(c.survey4a, 6) AS survey4a, COUNT(DISTINCT(c.redeem_consumer)) AS Summary
			FROM vw_rpt_turnup_comm c
			WHERE (c.senddate BETWEEN @LDateFrom and @LDateTo)
			AND c.eventId = @LEvent
			AND (
				(c.EDM = ISNULL(@LEDM, 0))
				OR (c.SMS = ISNULL(@LSMS, 0))
				OR (c.Ccenter = ISNULL(@LCCenter, 0))
			)
			GROUP BY c.survey4a
		) AS comm ON template.survey4a = comm.survey4a
		LEFT OUTER JOIN (
			-- Turn up all
			SELECT ISNULL(c.survey4a, 6) AS survey4a, COUNT(DISTINCT(c.redeem_consumer)) AS Summary
			FROM vw_rpt_turnup_comm c
			WHERE (c.senddate BETWEEN @LDateFrom and @LDateTo)
			AND (
				(c.EDM = ISNULL(@LEDM, 0))
				OR (c.SMS = ISNULL(@LSMS, 0))
				OR (c.Ccenter = ISNULL(@LCCenter, 0))
			)
			GROUP BY c.survey4a
		) AS t_all ON template.survey4a = t_all.survey4a
		
	UNION
		
	SELECT 2, @LEvent1 AS EventID, @EventName1 AS EventName,  
	template.survey4a, template.disp AS Type, 
	ISNULL(sents.Summary, 0) AS Sent4A, ISNULL(comm.Summary, 0) AS Com4A, ISNULL(t_all.Summary, 0) AS All4A
	FROM (
		SELECT 1 AS survey4a, 'Adorer' AS disp
		UNION
		SELECT 2 AS survey4a, 'Adopter' AS disp
		UNION
		SELECT 3 AS survey4a, 'Acceptor' AS disp
		UNION
		SELECT 4 AS survey4a, 'Available' AS disp
		UNION
		SELECT 5 AS survey4a, 'Rejector' AS disp
		UNION
		SELECT 6 AS survey4a, 'NA' AS disp
	) template LEFT OUTER JOIN (
			-- Turn up sent
			SELECT ISNULL(s.survey4a, 6) AS survey4a, COUNT(DISTINCT(s.consumerId)) AS Summary
			FROM vw_rpt_turnup_sent AS s
			WHERE (s.senddate BETWEEN @LDateFrom1 and @LDateTo1)
			AND s.eventId = @LEvent1
			AND (
				(s.EDM = ISNULL(@LEDM, 0))
				OR (s.SMS = ISNULL(@LSMS, 0))
				OR (s.Ccenter = ISNULL(@LCCenter, 0))
			)
			GROUP BY s.survey4a
		) AS sents ON template.survey4a = sents.survey4a
		LEFT OUTER JOIN (
			-- Turn up communication
			SELECT ISNULL(c.survey4a, 6) AS survey4a, COUNT(DISTINCT(c.redeem_consumer)) AS Summary
			FROM vw_rpt_turnup_comm c
			WHERE (c.senddate BETWEEN @LDateFrom1 and @LDateTo1)
			AND c.eventId = @LEvent1
			AND (
				(c.EDM = ISNULL(@LEDM, 0))
				OR (c.SMS = ISNULL(@LSMS, 0))
				OR (c.Ccenter = ISNULL(@LCCenter, 0))
			)
			GROUP BY c.survey4a
		) AS comm ON template.survey4a = comm.survey4a
		LEFT OUTER JOIN (
			-- Turn up all
			SELECT ISNULL(c.survey4a, 6) AS survey4a, COUNT(DISTINCT(c.redeem_consumer)) AS Summary
			FROM vw_rpt_turnup_comm c
			WHERE (c.senddate BETWEEN @LDateFrom1 and @LDateTo1)
			AND (
				(c.EDM = ISNULL(@LEDM, 0))
				OR (c.SMS = ISNULL(@LSMS, 0))
				OR (c.Ccenter = ISNULL(@LCCenter, 0))
			)
			GROUP BY c.survey4a
		) AS t_all ON template.survey4a = t_all.survey4a
				
	ORDER BY 1, 4
END
