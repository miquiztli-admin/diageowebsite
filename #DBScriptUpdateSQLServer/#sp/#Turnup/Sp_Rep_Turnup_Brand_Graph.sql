USE [DiageoDBTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Rep_Turnup_Brand_Graph]    Script Date: 11/02/2013 18:40:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF OBJECT_ID (N'dbo.Sp_Rep_Turnup_Brand_Graph', N'P') IS NOT NULL
    DROP PROC Sp_Rep_Turnup_Brand_Graph;
GO
-- =============================================
-- Author:		<Inferno>
-- Update date: <02/11/2013>
-- Description:	<Replace old stored procedure, designed to fast update for all procedures with similar job>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_Rep_Turnup_Brand_Graph](
	@Event as integer,@DateFrom as datetime,@DateTo as datetime,@EDM as bit,@SMS as bit,@CCenter as bit
)
AS
BEGIN

	-- Avoid parameter sniffing problems in SSRS
	DECLARE @LEvent INT, @LDateFrom DATETIME, @LDateTo DATETIME, @LEDM BIT, @LSMS BIT, @LCCenter BIT
	SET @LEvent = @Event
	SET @LDateFrom = @DateFrom
	SET @LDateTo = @DateTo
	SET @LEDM = @EDM
	SET @LSMS = @SMS
	SET @LCCenter = @CCenter
	-- Avoid parameter sniffing problems in SSRS

	Declare @JWGSent bigint,@JWGCom bigint,@JWGAll bigint
	Declare @JWBSent bigint,@JWBCom bigint,@JWBAll bigint
	Declare @SmirnoffSent bigint,@SmirnoffCom bigint,@SmirnoffAll bigint
	Declare @JWRSent bigint,@JWRCom bigint,@JWRAll bigint
	Declare @BMSent bigint,@BMCom bigint,@BMAll bigint
	Declare @Param1 bigint,@Param2 bigint,@Param3 bigint
	DECLARE @ResultTable TABLE (Gtype Varchar(200),JWG bigint,JWB bigint,Smirnoff bigint,JWR bigint,BM bigint)

--Sent
	SELECT 
	@JWGSent = COUNT(DISTINCT(CASE WHEN s.Brand = 'JWG' THEN s.consumerId ELSE NULL END)),
	@JWBSent = COUNT(DISTINCT(CASE WHEN s.Brand = 'JWB' THEN s.consumerId ELSE NULL END)),
	@SmirnoffSent = COUNT(DISTINCT(CASE WHEN s.Brand = 'SMI' THEN s.consumerId ELSE NULL END)),
	@JWRSent = COUNT(DISTINCT(CASE WHEN s.Brand = 'JWR' THEN s.consumerId ELSE NULL END)),
	@BMSent = COUNT(DISTINCT(CASE WHEN s.Brand = 'BEN' THEN s.consumerId ELSE NULL END))
	FROM vw_rpt_turnup_sent s
	WHERE (s.senddate BETWEEN @LDateFrom and @LDateTo)
	AND s.eventId = @LEvent
	AND s.Brand IS NOT NULL
	AND (
		(s.EDM = ISNULL(@LEDM, 0))
		OR (s.SMS = ISNULL(@LSMS, 0))
		OR (s.Ccenter = ISNULL(@LCCenter, 0))
	)

	-- Turn up communication
	SELECT
	@JWGCom = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWG' THEN c.redeem_consumer ELSE NULL END)),
	@JWBCom = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWB' THEN c.redeem_consumer ELSE NULL END)),
	@SmirnoffCom = COUNT(DISTINCT(CASE WHEN c.Brand = 'SMI' THEN c.redeem_consumer ELSE NULL END)),
	@JWRCom = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWR' THEN c.redeem_consumer ELSE NULL END)),
	@BMCom = COUNT(DISTINCT(CASE WHEN c.Brand = 'BEN' THEN c.redeem_consumer ELSE NULL END))
	FROM vw_rpt_turnup_comm c
	WHERE (c.senddate BETWEEN @LDateFrom and @LDateTo)
	AND c.eventId = @LEvent
	AND c.Brand IS NOT NULL
	AND (
		(c.EDM = ISNULL(@LEDM, 0))
		OR (c.SMS = ISNULL(@LSMS, 0))
		OR (c.Ccenter = ISNULL(@LCCenter, 0))
	)

	-- Turn up all
	SELECT
	@JWGAll = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWG' THEN c.redeem_consumer ELSE NULL END)),
	@JWBAll = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWB' THEN c.redeem_consumer ELSE NULL END)),
	@SmirnoffAll = COUNT(DISTINCT(CASE WHEN c.Brand = 'SMI' THEN c.redeem_consumer ELSE NULL END)),
	@JWRAll = COUNT(DISTINCT(CASE WHEN c.Brand = 'JWR' THEN c.redeem_consumer ELSE NULL END)),
	@BMAll = COUNT(DISTINCT(CASE WHEN c.Brand = 'BEN' THEN c.redeem_consumer ELSE NULL END))
	FROM vw_rpt_turnup_comm c
	WHERE (c.senddate BETWEEN @LDateFrom and @LDateTo)
	AND c.Brand IS NOT NULL
	AND (
		(c.EDM = ISNULL(@LEDM, 0))
		OR (c.SMS = ISNULL(@LSMS, 0))
		OR (c.Ccenter = ISNULL(@LCCenter, 0))
	)
	INSERT INTO @ResultTable
	VALUES
		('Sent',@JWGSent,@JWBSent,@SmirnoffSent,@JWRSent,@BMSent),
		('Turn up from communication',@JWGCom,@JWBCom,@SmirnoffCom,@JWRCom,@BMCom),
		('All Turn up',@JWGAll,@JWBAll,@SmirnoffAll,@JWRAll,@BMAll) 

	SELECT * FROM @ResultTable

END
