/*
select
	ISNULL(COUNT(DISTINCT(both)), 0) as both,
	ISNULL(COUNT(DISTINCT(emailOnly)), 0) email_only, 
	ISNULL(COUNT(DISTINCT(mobileOnly)), 0) mobile_only, 
	ISNULL(COUNT(DISTINCT(no_all)), 0) no_all, 
	ISNULL(COUNT(DISTINCT(CASE WHEN no_all IS NULL THEN consumerId ELSE NULL END)), 0) contactable, -- contactable
	ISNULL(COUNT(DISTINCT(consumerId)), 0) db
from vw_rpt_dashboardConsumer_by_quarter
where brandPreJwBlack ='T'
and (
	upd_quarterYear < 2014
	or (upd_quarterYear = 2014 and upd_quarterNum = 1)
)
*/
-------- Test compare condition ---------------------
with sum1 as ( 
SELECT consumerid,
CASE 
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Both'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Both'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Both'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Email Only'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND  blockEmail ='T' and (blockCallCenter ='F' or blockMobile = 'F') THEN 'Mobile Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND  blockEmail ='T' and (blockCallCenter ='T' or blockMobile = 'F') THEN 'Mobile Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND  blockEmail ='T' and (blockCallCenter ='F' or blockMobile = 'T') THEN 'Mobile Only'
WHEN  (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND ((blockCallCenter ='F' or blockMobile = 'F') ) THEN 'Mobile Only'
WHEN  (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND ((blockCallCenter ='T' or blockMobile = 'F') ) THEN 'Mobile Only'
WHEN  (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND ((blockCallCenter ='F' or blockMobile = 'T') ) THEN 'Mobile Only'

WHEN  (email IS  null or email ='') AND  (mobile IS null or mobile ='') THEN 'No all'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '') 
AND ((blockCallCenter IS null OR blockCallCenter = '') AND (blockMobile IS null OR blockMobile = '') AND (blockEmail IS null OR blockEmail = '')) THEN 'No all'



WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '') 
AND (blockCallCenter IS null OR blockCallCenter = '') AND (blockMobile = 'T') AND (blockEmail IS null OR blockEmail = '')  THEN 'No all'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '')  AND
(blockCallCenter = 'T') AND (blockMobile IS null OR blockMobile = '') AND (blockEmail IS null OR blockEmail = '')  THEN 'No all'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '')  AND
(blockCallCenter IS null OR blockCallCenter = '') AND (blockMobile IS null OR blockMobile = '') AND (blockEmail = 'T')  THEN 'No all'

WHEN (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='T' AND blockEmail ='T' AND blockMobile = 'T') THEN 'No all'


WHEN (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockEmail ='T') THEN 'No all'

WHEN (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND (blockCallCenter ='T' or blockMobile = 'T' ) THEN 'No all'
END AS 'Type',email,mobile,blockCallCenter,blockEmail,blockMobile
FROM diageodb..Consumer
WHERE (updateDate <='2013-09-30 23:59:59.000') and brandPreJwBlack ='T')  --Q1 and Johnnie Walker black 
select consumerId, updatedate, mobile, blockMobile, blockCallCenter, email, blockEmail, brandPreJwBlack
from diageodb..Consumer where consumerid in (
	select no_all
	from vw_rpt_dashboardConsumer_by_quarter
	where brandPreJwBlack ='T'
	and (
		upd_quarterYear < 2014
		or (upd_quarterYear = 2014 and upd_quarterNum = 1)
	)
	and consumerId not in (
		select consumerid
		from sum1
		where type = 'No all'
	)
)


------ incorrect condition -------
with sum1 as ( 
SELECT consumerid,
CASE 
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Both'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Both'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Both'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Email Only'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND  blockEmail ='T' and (blockCallCenter ='F' or blockMobile = 'F') THEN 'Mobile Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND  blockEmail ='T' and (blockCallCenter ='T' or blockMobile = 'F') THEN 'Mobile Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND  blockEmail ='T' and (blockCallCenter ='F' or blockMobile = 'T') THEN 'Mobile Only'
WHEN  (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND ((blockCallCenter ='F' or blockMobile = 'F') ) THEN 'Mobile Only'
WHEN  (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND ((blockCallCenter ='T' or blockMobile = 'F') ) THEN 'Mobile Only'
WHEN  (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND ((blockCallCenter ='F' or blockMobile = 'T') ) THEN 'Mobile Only'

WHEN  (email IS  null or email ='') AND  (mobile IS null or mobile ='') THEN 'No all'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '') 
AND ((blockCallCenter IS null OR blockCallCenter = '') AND (blockMobile IS null OR blockMobile = '') AND (blockEmail IS null OR blockEmail = '')) THEN 'No all'



WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '') 
AND (blockCallCenter IS null OR blockCallCenter = '') AND (blockMobile = 'T') AND (blockEmail IS null OR blockEmail = '')  THEN 'No all'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '')  AND
(blockCallCenter = 'T') AND (blockMobile IS null OR blockMobile = '') AND (blockEmail IS null OR blockEmail = '')  THEN 'No all'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '')  AND
(blockCallCenter IS null OR blockCallCenter = '') AND (blockMobile IS null OR blockMobile = '') AND (blockEmail = 'T')  THEN 'No all'

WHEN (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='T' AND blockEmail ='T' AND blockMobile = 'T') THEN 'No all'


WHEN (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockEmail ='T') THEN 'No all'

WHEN (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND (blockCallCenter ='T' or blockMobile = 'T' ) THEN 'No all'
END AS 'Type',email,mobile,blockCallCenter,blockEmail,blockMobile
FROM diageodb..Consumer
WHERE (updateDate <='2013-09-30 23:59:59.000') and brandPreJwBlack ='T')  --Q1 and Johnnie Walker black 

select consumerId,email,mobile,blockCallCenter,blockEmail,blockMobile,type
from sum1
--where consumerId = 889658
where ISNULL(email, '') != ''
and ISNULL(mobile, '') != ''
and blockEmail = 'T'
and blockmobile = 'T'
and blockcallcenter = 'T'
