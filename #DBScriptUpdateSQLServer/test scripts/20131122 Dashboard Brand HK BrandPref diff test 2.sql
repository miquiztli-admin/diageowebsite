with sum1 as ( 
SELECT consumerid,
CASE 
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Both'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Both'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Both'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Email Only'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND  blockEmail ='T' and (blockCallCenter ='F' and blockMobile = 'F') THEN 'Mobile Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND  blockEmail ='T' and (blockCallCenter ='T' and blockMobile = 'F') THEN 'Mobile Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND  blockEmail ='T' and (blockCallCenter ='F' and blockMobile = 'T') THEN 'Mobile Only'
WHEN  (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND ((blockCallCenter ='F' and blockMobile = 'F') ) THEN 'Mobile Only'
WHEN  (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND ((blockCallCenter ='T' and blockMobile = 'F') ) THEN 'Mobile Only'
WHEN  (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND ((blockCallCenter ='F' and blockMobile = 'T') ) THEN 'Mobile Only'

WHEN  (email IS  null or email ='') AND  (mobile IS null or mobile ='') THEN 'No all'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '') 
AND ((blockCallCenter IS null OR blockCallCenter = '') AND (blockMobile IS null OR blockMobile = '') AND (blockEmail IS null OR blockEmail = '')) THEN 'No all'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '') 
AND (blockCallCenter IS null OR blockCallCenter = '') AND (blockMobile = 'T') AND (blockEmail IS null OR blockEmail = '')  THEN 'No all'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '')  AND
(blockCallCenter = 'T') AND (blockMobile IS null OR blockMobile = '') AND (blockEmail IS null OR blockEmail = '')  THEN 'No all'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '')  AND
(blockCallCenter IS null OR blockCallCenter = '') AND (blockMobile IS null OR blockMobile = '') AND (blockEmail = 'T')  THEN 'No all'
WHEN (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='T' AND blockEmail ='T' AND blockMobile = 'T') THEN 'No all'
WHEN (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockEmail ='T') THEN 'No all'
WHEN (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND (blockCallCenter ='T' or blockMobile = 'T' ) THEN 'No all'
END AS 'Type',brandPre100Pipers,brandPre100Pipers8Y,brandPreAbsolut,brandPreBaileys,brandPreBallentine,brandPreBlend,brandPreChivas,brandPreDewar,
survey4a100Pipers,survey4a100Pipers8Y,survey4aAbsolut,survey4aBaileys,survey4aBallentine,survey4aBlend,survey4aChivas,survey4aDewar --competitor Brand
,brandPreHennessy,survey4aHennessy,brandPreBenmore,survey4aBenmore,brandPreJwGold,survey4aJwGold,brandPreJwGreen,survey4aJwGreen  -- other Diageo Brand
,brandPreJwRed,survey4aJwRed  -- Johnnie walker Red
,brandPreSmirnoff,survey4aSmirnoff -- Smirnoff
,brandPreJwBlack,survey4aJwBlack --Johnnie Walker black
,brandPreNotdrink,survey4aNotdrink, brandPreOther,survey4aOthers --other

FROM diageodb..Consumer
WHERE (updateDate <='2013-06-30 23:59:59.000') AND (registerPlatformId = '0' or registerPlatformId = '1') 
)
/*
select  count(consumerid) 
from sum1
WHERE ((ISNULL(brandPreOther, 'F')!= 'T'  AND  survey4aOthers is NULL and ISNULL(brandPreNotdrink, 'F') != 'T' AND survey4aNotdrink is NULL) --other
	and (ISNULL(brandPre100Pipers, 'F') !='T' and ISNULL(brandPre100Pipers8Y, 'F') !='T' AND ISNULL(brandPreAbsolut, 'F') !='T' AND ISNULL(brandPreBaileys, 'F') !='T' AND ISNULL(brandPreBallentine, 'F') !='T' AND ISNULL(brandPreBlend, 'F') !='T' and ISNULL(brandPreChivas, 'F') !='T' and ISNULL(brandPreDewar, 'F') !='T' and survey4a100Pipers is NULL and survey4a100Pipers8Y is NULL and survey4aAbsolut is null and survey4aBaileys is NULL and survey4aBallentine is NULL and survey4aBlend  is NULL and survey4aChivas  is NULL or survey4aDewar is NULL) -- competitor
	and (ISNULL(brandPreSmirnoff, 'F')!= 'T' AND survey4aSmirnoff is NULL) --smirnoff
	and (ISNULL(brandPreJwBlack, 'F') != 'T' AND survey4aJwBlack is null) -- Johnnie walker black
	and (ISNULL(brandPreJwRed, 'F')!= 'T' AND survey4aJwRed is null )
	and (ISNULL(brandPreHennessy, 'F') != 'T' AND survey4aHennessy is NULL and ISNULL(brandPreBenmore, 'F') != 'T' AND survey4aBenmore is NULL and ISNULL(brandPreJwGold, 'F') != 'T' and survey4aJwGold is null and ISNULL(brandPreJwGreen, 'F') != 'T' AND survey4aJwGreen is NULL)
) 
and type != 'No all'
*/
select * from vw_rpt_dashb_hk_by_quarter--diageodb..consumer --
where consumerid in ( 
	select consumerid
	from sum1
	WHERE (
		(
			ISNULL(brandPreOther, 'F')!= 'T' AND survey4aOthers is NULL and ISNULL(brandPreNotdrink, 'F') != 'T' 
			AND survey4aNotdrink is NULL
		) --other
		and (
			ISNULL(brandPre100Pipers, 'F') !='T' and ISNULL(brandPre100Pipers8Y, 'F') !='T' 
			AND ISNULL(brandPreAbsolut, 'F') !='T' AND ISNULL(brandPreBaileys, 'F') !='T' 
			AND ISNULL(brandPreBallentine, 'F') !='T' AND ISNULL(brandPreBlend, 'F') !='T' 
			and ISNULL(brandPreChivas, 'F') !='T' and ISNULL(brandPreDewar, 'F') !='T' 
			and survey4a100Pipers is NULL and survey4a100Pipers8Y is NULL and survey4aAbsolut is null 
			and survey4aBaileys is NULL and survey4aBallentine is NULL and survey4aBlend  is NULL 
			and survey4aChivas  is NULL and survey4aDewar is NULL
		) -- competitor
		and (ISNULL(brandPreSmirnoff, 'F')!= 'T' AND survey4aSmirnoff is NULL) --smirnoff
		and (ISNULL(brandPreJwBlack, 'F') != 'T' AND survey4aJwBlack is null) -- Johnnie walker black
		and (ISNULL(brandPreJwRed, 'F')!= 'T' AND survey4aJwRed is null )
		and (ISNULL(brandPreBaileys, 'F')!= 'T' AND survey4aBaileys is null )
		and (
			ISNULL(brandPreHennessy, 'F') != 'T' AND survey4aHennessy is NULL 
			and ISNULL(brandPreBenmore, 'F') != 'T' AND survey4aBenmore is NULL 
			and ISNULL(brandPreJwGold, 'F') != 'T' and survey4aJwGold is null 
			and ISNULL(brandPreJwGreen, 'F') != 'T' AND survey4aJwGreen is NULL
		)
	) 
	and type != 'No all'
	and consumerid not in (
		select consumerid from vw_rpt_dashb_hk_by_quarter
		where upd_quarterYear <= 2013
		and no_all is null
		and (
			diageoBrandPre IS NULL
			AND diageoBrandPreOthers IS NULL
			AND brandPreCompetitors IS NULL
			AND brandPreOthers IS NULL
		)
	)
)
order by 1