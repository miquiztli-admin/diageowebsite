SELECT DISTINCT c.consumerId, c.brandPre100Pipers, c.brandPre100Pipers8Y, 
c.brandPreAbsolut, c.brandPreBaileys, c.brandPreBallentine, c.brandPreBenmore, 
c.brandPreBlend, c.brandPreChivas, c.brandPreDewar, c.brandPreHennessy, c.brandPreJwBlack, 
c.brandPreJwGold, c.brandPreJwGreen, c.brandPreJwRed, c.brandPreSmirnoff, c.brandPreNotdrink, 
c.brandPreText, c.brandPreOther, c.survey4aBaileys, c.survey4aBenmore, c.survey4aJwBlack, 
c.survey4aJwGold, c.survey4aJwGreen, c.survey4aJwRed, c.survey4aSmirnoff, c.survey4aAbsolut, 
c.survey4aBallentine, c.survey4aBlend, c.survey4aChivas, c.survey4aDewar, c.survey4aHennessy, 
c.survey4a100Pipers, c.survey4a100Pipers8Y, c.survey4aOthers, c.survey4aNotdrink, c.survey4aText,
 c.surveyP4wBaileys, c.surveyP4wBenmore, c.surveyP4wJwBlack, c.surveyP4wJwGold, c.surveyP4wJwGreen, 
c.surveyP4wJwRed, c.surveyP4wSmirnoff, c.surveyP4wNonAbsolute, c.surveyP4wNonBallentine, 
c.surveyP4wNonBlend, c.surveyP4wNonChivas, c.surveyP4wNonDewar, c.surveyP4wNonHennessy, 
c.surveyP4wNon100Pipers, c.surveyP4wNon100Pipers8yrs, c.surveyP4wNonOthers, 
c.surveyP4wNonNotdrink, c.surveyP4wNonText, c.subScribeCallCenter_DG,
YEAR(c.createDate) + (-1 * qm.year_shift) AS quarterYear, qm.quarter AS quarterNum,
CASE
	WHEN ( 
			/*c.activeEmail = 'F' AND c.blockEmail = 'T' AND c.email IS NULL
			AND */c.activeCallCenter = 'T' AND c.blockCallCenter = 'F' AND mobile IS NOT NULL
			/*AND c.activeMobile = 'T' AND c.blockMobile = 'F' AND c.mobile IS NOT NULL*/
		) THEN 1
	ELSE 0
END AS mobileOnly,
CASE
	WHEN (
			c.activeEmail = 'T' AND c.blockEmail = 'F' AND email IS NOT NULL
			/*AND c.activeCallCenter = 'F' AND c.blockCallCenter = 'T' AND c.mobile IS NULL*/
			/*AND c.activeMobile = 'F' AND c.blockMobile = 'T' AND c.mobile IS NOT NULL*/
		) THEN 1
	ELSE 0
END AS emailOnly,
CASE
	WHEN ( 
			c.activeEmail = 'T' AND c.blockEmail = 'F' AND c.email IS NOT NULL
			AND c.activeCallCenter = 'T' AND c.blockCallCenter = 'F' AND c.mobile IS NOT NULL
			/*AND c.activeMobile = 'T' AND c.blockMobile = 'F' AND c.mobile IS NOT NULL*/
		) THEN 1
	ELSE 0
END AS mobileAndEmail
FROM tbl_m_rpt_quarter_month qm 
	LEFT OUTER JOIN Consumer c ON MONTH(c.createDate) = qm.month_num
	JOIN PlatformMember p ON c.consumerId = p.consumerId
