	SELECT eventStaff.Staff_Code, eventStaff.Outlet_Name,
	AVG(CASE
		WHEN eventInfo.Count_Staff = 0 THEN 0 
		WHEN (eventInfo.TrafficAmount / eventInfo.Count_Staff ) = 0 THEN 0
		ELSE eventInfo.Engagement / (eventInfo.TrafficAmount / eventInfo.Count_Staff)
	END) AS EngagePerTraffic, COUNT(eventStaff.T_Date) AS daysCount
	, AVG(eventInfo.Count_Staff) AS AverageCountStaff
	, AVG(eventInfo.TrafficAmount) AS AverageSumTraffic, AVG(eventInfo.Engagement) AS AverageCountEngage
	FROM (
		SELECT DISTINCT Staff_Code, Outlet_Name, CAST(Repeat_TransactionDate AS DATE) AS T_Date
		FROM vw_rpt_dataEntryPerformance
		WHERE (Repeat_TransactionDate BETWEEN '2012-06-01' AND '2012-09-30')
		AND (
			(1 = 2 AND DATEPART(DW, Repeat_TransactionDate) IN (1,2,3,4,5)) --Week Day
			OR (1 = 3 AND DATEPART(DW, Repeat_TransactionDate) IN (6,7)) -- Week End
			OR 1 NOT IN (2,3)
		)
	) AS eventStaff INNER JOIN
		(
			SELECT Outlet_Name, CAST(Repeat_TransactionDate AS DATE) AS T_Date, 
			CAST(COUNT(repeat_id) AS FLOAT) AS Engagement,
			CAST(SUM(ISNULL(Traffic_Amount, 0)) AS FLOAT) AS TrafficAmount, 
			CAST(COUNT(DISTINCT Staff_Code) AS FLOAT) AS Count_Staff
			FROM vw_rpt_dataEntryPerformance
			WHERE (Repeat_TransactionDate BETWEEN '2012-06-01' AND '2012-09-30')
			AND (
				(1 = 2 AND DATEPART(DW, Repeat_TransactionDate) IN (1,2,3,4,5)) --Week Day
				OR (1 = 3 AND DATEPART(DW, Repeat_TransactionDate) IN (6,7)) -- Week End
				OR 1 NOT IN (2,3)
			)
			GROUP BY Outlet_Name, CAST(Repeat_TransactionDate AS DATE)
		) AS eventInfo ON eventInfo.Outlet_Name = eventStaff.Outlet_Name AND eventInfo.T_Date = eventStaff.T_Date
	GROUP BY eventStaff.Staff_Code, eventStaff.Outlet_Name
	ORDER BY eventStaff.Staff_Code, eventStaff.Outlet_Name
	