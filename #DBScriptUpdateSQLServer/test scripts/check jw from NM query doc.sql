WITH summary AS(SELECT  c.consumerId,
CASE 
WHEN MONTH(c.createDate) IN (7,8,9)  THEN 'Q1'
WHEN MONTH(c.createDate) IN (10,11,12)  THEN 'Q2'
WHEN MONTH(c.createDate) IN (1,2,3)  THEN 'Q3'
WHEN MONTH(c.createDate) IN (4,5,6)  THEN 'Q4'
END AS 'Quarter'
FROM Consumer c JOIN PlatformMember p ON   
c.consumerId = p.consumerId 						
WHERE  c.createDate BETWEEN '2012-07-01 00:00:00' AND '2013-06-30 23:59:59' 
AND (activeEmail = 'T' OR activeMobile = 'T' ) 
AND (blockMobile = 'F' OR blockemail = 'F')  
AND p.platformId = 2
AND ((blockMobile = 'F' AND blockCallCenter = 'F') or blockemail = 'F') 
AND (email IS NOT NULL or mobile IS NOT NULL) 
AND p.platformId = 2 
AND (brandPreJwBlack ='T'))
SELECT Quarter,count(DISTINCT consumerId) FROM summary
GROUP BY Quarter
