WITH Sum1 AS
(
SELECT 
CASE 
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Both'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Both'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Both'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Email Only'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND  blockEmail ='T' and (blockCallCenter ='F' or blockMobile = 'F') THEN 'Mobile Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND  blockEmail ='T' and (blockCallCenter ='T' or blockMobile = 'F') THEN 'Mobile Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND  blockEmail ='T' and (blockCallCenter ='F' or blockMobile = 'T') THEN 'Mobile Only'
WHEN  (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND ((blockCallCenter ='F' or blockMobile = 'F') ) THEN 'Mobile Only'
WHEN  (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND ((blockCallCenter ='T' or blockMobile = 'F') ) THEN 'Mobile Only'
WHEN  (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND ((blockCallCenter ='F' or blockMobile = 'T') ) THEN 'Mobile Only'

WHEN  (email IS  null or email ='') AND  (mobile IS null or mobile ='') THEN 'No all'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '') 
AND ((blockCallCenter IS null OR blockCallCenter = '') AND (blockMobile IS null OR blockMobile = '') AND (blockEmail IS null OR blockEmail = '')) THEN 'No all'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '') 
AND (blockCallCenter IS null OR blockCallCenter = '') AND (blockMobile = 'T') AND (blockEmail IS null OR blockEmail = '')  THEN 'No all'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '')  AND
(blockCallCenter = 'T') AND (blockMobile IS null OR blockMobile = '') AND (blockEmail IS null OR blockEmail = '')  THEN 'No all'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '')  AND
(blockCallCenter IS null OR blockCallCenter = '') AND (blockMobile IS null OR blockMobile = '') AND (blockEmail = 'T')  THEN 'No all'

WHEN (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='T' AND blockEmail ='T' AND blockMobile = 'T') THEN 'No all'

WHEN (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockEmail ='T') THEN 'No all'

WHEN (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND (blockCallCenter ='T' or blockMobile = 'T' ) THEN 'No all'

END AS 'Type',consumerId,
brandPreJwGold,survey4aJwGold,
brandPreJwBlack,survey4aJwBlack,
brandPreSmirnoff,survey4aSmirnoff,
brandPreBenmore,survey4aBenmore,
brandPreJwRed,survey4aJwRed,
email, mobile, blockEmail, blockMobile, blockCallCenter

FROM diageodb..Consumer 
WHERE updateDate <='2012-06-30 23:59:00.000'
)

SELECT consumerID, email, mobile, blockEmail, blockMobile, blockCallCenter, type
FROM sum1
WHERE ((ISNULL(email, '') = '' OR blockEmail = 'T') AND (ISNULL(mobile, '') = '' OR blockCallCenter = 'T' OR blockMobile = 'T'))