WITH T AS
(
SELECT 
case 
when c.survey4aJwGold = '1'  THEN 'Adorer'
when c.survey4aJwGold = '2'  THEN 'Adopter'
when c.survey4aJwGold = '3'  THEN 'Accepter'
when c.survey4aJwGold = '4'  THEN 'Available'
when c.survey4aJwGold = '5'  THEN 'Rejector'

when c.survey4aJwBlack = '1'  THEN 'Adorer'
when c.survey4aJwBlack = '2'  THEN 'Adopter'
when c.survey4aJwBlack = '3'  THEN 'Accepter'
when c.survey4aJwBlack = '4'  THEN 'Available'
when c.survey4aJwBlack = '5'  THEN 'Rejector'

when c.survey4aSmirnoff = '1'  THEN 'Adorer'
when c.survey4aSmirnoff = '2'  THEN 'Adopter'
when c.survey4aSmirnoff = '3'  THEN 'Accepter'
when c.survey4aSmirnoff = '4'  THEN 'Available'
when c.survey4aSmirnoff = '5'  THEN 'Rejector'

when c.survey4aJwRed = '1'  THEN 'Adorer'
when c.survey4aJwRed = '2'  THEN 'Adopter'
when c.survey4aJwRed = '3'  THEN 'Accepter'
when c.survey4aJwRed = '4'  THEN 'Available'
when c.survey4aJwRed = '5'  THEN 'Rejector'

when c.survey4aBenmore = '1'  THEN 'Adorer'
when c.survey4aBenmore = '2'  THEN 'Adopter'
when c.survey4aBenmore = '3'  THEN 'Accepter'
when c.survey4aBenmore = '4'  THEN 'Available'
when c.survey4aBenmore = '5'  THEN 'Rejector'
ELSE  'N/A'
END AS 'A', c.*
FROM  DiageoDB.dbo.Consumer c
INNER JOIN DiageoDB.dbo.Outbound o on o.consumerId = c.consumerId
WHERE (o.senddate BETWEEN '2012-06-01 00:00:00.000' and '2012-09-30 23:59:59.000') AND o.eventId = '5' 
)

SELECT A,consumerid, survey4aJwGold, survey4aJwBlack, survey4aJwRed, survey4aSmirnoff, survey4aBenmore 
FROM T
WHERE A = 'Adopter'
AND consumerId not in (
	SELECT consumerID FROM vw_rpt_turnup_sent WHERE (senddate BETWEEN '2012-06-01 00:00:00.000' and '2012-09-30 23:59:59.000')
		AND eventId = 5 AND survey4a = 2
)
