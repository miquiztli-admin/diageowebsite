with sum1 as ( 
SELECT consumerid,
CASE 
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Both'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Both'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Both'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='F' AND blockEmail ='F' AND blockMobile = 'T') THEN 'Email Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockCallCenter ='T' AND blockEmail ='F' AND blockMobile = 'F') THEN 'Email Only'

WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND  blockEmail ='T' and (blockCallCenter ='F' or blockMobile = 'F') THEN 'Mobile Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND  blockEmail ='T' and (blockCallCenter ='T' or blockMobile = 'F') THEN 'Mobile Only'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND  blockEmail ='T' and (blockCallCenter ='F' or blockMobile = 'T') THEN 'Mobile Only'
WHEN  (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND ((blockCallCenter ='F' or blockMobile = 'F') ) THEN 'Mobile Only'
WHEN  (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND ((blockCallCenter ='T' or blockMobile = 'F') ) THEN 'Mobile Only'
WHEN  (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND ((blockCallCenter ='F' or blockMobile = 'T') ) THEN 'Mobile Only'

WHEN  (email IS  null or email ='') AND  (mobile IS null or mobile ='') THEN 'No all'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '') 
AND ((blockCallCenter IS null OR blockCallCenter = '') AND (blockMobile IS null OR blockMobile = '') AND (blockEmail IS null OR blockEmail = '')) THEN 'No all'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '') 
AND (blockCallCenter IS null OR blockCallCenter = '') AND (blockMobile = 'T') AND (blockEmail IS null OR blockEmail = '')  THEN 'No all'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '')  AND
(blockCallCenter = 'T') AND (blockMobile IS null OR blockMobile = '') AND (blockEmail IS null OR blockEmail = '')  THEN 'No all'
WHEN  (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile != '')  AND
(blockCallCenter IS null OR blockCallCenter = '') AND (blockMobile IS null OR blockMobile = '') AND (blockEmail = 'T')  THEN 'No all'
WHEN (email IS NOT null AND email !='') AND (mobile IS NOT null AND mobile !='') AND (blockCallCenter ='T' AND blockEmail ='T' AND blockMobile = 'T') THEN 'No all'
WHEN (email IS NOT null AND email !='') AND (mobile IS null or mobile ='') AND (blockEmail ='T') THEN 'No all'
WHEN (email IS  null or email ='') AND (mobile IS NOT null AND mobile != '') AND (blockCallCenter ='T' or blockMobile = 'T' ) THEN 'No all'
END AS 'Type',brandPre100Pipers,brandPre100Pipers8Y,brandPreAbsolut,brandPreBaileys,brandPreBallentine,brandPreBlend,brandPreChivas,brandPreDewar,
survey4a100Pipers,survey4a100Pipers8Y,survey4aAbsolut,survey4aBaileys,survey4aBallentine,survey4aBlend,survey4aChivas,survey4aDewar --competitor Brand
,brandPreHennessy,survey4aHennessy,brandPreBenmore,survey4aBenmore,brandPreJwGold,survey4aJwGold,brandPreJwGreen,survey4aJwGreen  -- other Diageo Brand
,brandPreJwRed,survey4aJwRed  -- Johnnie walker Red
,brandPreSmirnoff,survey4aSmirnoff -- Smirnoff
,brandPreJwBlack,survey4aJwBlack --Johnnie Walker black
,brandPreNotdrink,survey4aNotdrink, brandPreOther,survey4aOthers --other

FROM Consumer
WHERE (updateDate <='2013-09-30 23:59:59.000') AND registerPlatformId = '2' 
)  
/*SELECT updateDate, email, mobile, blockEmail, blockCallCenter, blockMobile,
brandPreNotdrink, brandPre100Pipers, brandPre100Pipers8Y, brandPreAbsolut, brandPreBaileys, brandPreBallentine, 
brandPreBlend, brandPreChivas, brandPreDewar, brandPreSmirnoff,
brandPreJwBlack, brandPreJwRed, brandPreHennessy, brandPreBenmore, brandPreJwGold, brandPreJwGreen,
survey4aNotdrink, survey4a100Pipers, survey4a100Pipers8Y, survey4aAbsolut, survey4aBaileys, survey4aBallentine, 
survey4aBlend, survey4aChivas, survey4aDewar, survey4aSmirnoff,
survey4aJwBlack, survey4aJwRed, survey4aHennessy, survey4aBenmore, survey4aJwGold, survey4aJwGreen
from diageodb..Consumer */
select * from sum1
where consumerId in (
	SELECT consumerId
	FROM vw_rpt_dashb_smart_by_quarter
	WHERE (upd_quarterYear < 2014 or (upd_quarterYear = 2014 and upd_quarterNum = 1))
	AND no_all IS NULL
	AND (
		diageoBrandPre IS NULL
		AND diageoBrandPreOthers IS NULL
		AND brandPreCompetitors IS NULL
		AND brandPreOthers IS NULL
	)
	AND consumerId not in (
		select consumerid 
		from sum1
		WHERE ( (brandPreNotdrink != 'T' or brandPreNotdrink is null) AND survey4aNotdrink is NULL) 
		and ((brandPre100Pipers !='T'or brandPre100Pipers is null) 
		and (brandPre100Pipers8Y !='T'or brandPre100Pipers8Y is NULL) 
		AND (brandPreAbsolut !='T' or brandPreAbsolut is NULL) 
		AND (brandPreBaileys !='T' or brandPreBaileys is null) 
		AND (brandPreBallentine !='T' or brandPreBallentine is null) 
		AND (brandPreBlend !='T' or brandPreBlend is null) 
		and (brandPreChivas !='T'or brandPreChivas is NULL) 
		and (brandPreDewar !='T' or brandPreDewar is null) 
		and survey4a100Pipers is NULL and survey4a100Pipers8Y is NULL and survey4aAbsolut is null and survey4aBaileys is NULL and survey4aBallentine is NULL and survey4aBlend  is NULL and survey4aChivas  is NULL or survey4aDewar is NULL) 
		and ((brandPreSmirnoff!= 'T' or brandPreSmirnoff is null) AND survey4aSmirnoff is NULL) 
		and ((brandPreJwBlack != 'T'Or brandPreJWblack is NULL) AND survey4aJwBlack is null) 
		and ((brandPreJwRed!= 'T' or brandPreJwRed is null) AND survey4aJwRed is null )
		and ((brandPreHennessy != 'T' or brandPreHennessy is null)AND survey4aHennessy is NULL 
		and (brandPreBenmore != 'T' or  brandPreBenmore is null ) AND survey4aBenmore is NULL 
		and (brandPreJwGold != 'T' or brandPreJwGold is null) and survey4aJwGold is null 
		and (brandPreJwGreen != 'T' or brandPreJwGreen  is null) AND survey4aJwGreen is NULL)
		and type != 'No all'
 	)
 )