﻿<%@ page language="C#" autoeventwireup="true" inherits="Reports_Report_ComparingDataEntryPerformance, App_Web_report_comparingdataentryperformance.aspx.dfa151d5" %>
<%@ Register Assembly="ITOS.Utility.CustomControl" Namespace="ITOS.Utility.CustomControl" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Jquery/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="../Jquery/DatePicker/js/jquery-ui-1.8.10.offset.datepicker.min.js"></script>
    <link type="text/css" href="../Jquery/DatePicker/css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        var jqCriteria = jQuery.noConflict();
        jqCriteria(document).ready(function() {
            jqCriteria("#txtTransactionDateFrom").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });
            jqCriteria("#txtTransactionDateTo").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });

            jqCriteria("#ddlTopic").change(function() {
                doSetVisibleControlPanel();
            });
        });
        function doSetVisibleControlPanel() {
            if (jqCriteria("#ddlTopic").val() == 4) {
                jqCriteria("#trDDLOutboundType").css("display", "table-row");
            } else {
                jqCriteria("#trDDLOutboundType").css("display", "none");
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <fieldset>
            <legend class="ntextblack">Comparing Data Entry Performance Report</legend>
                <table align="center">
                    <tr>
                        <td align="right" class="ntextblack">
                            Date Range:<span class="ntextred">*</span>
                        </td>
                        <td>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtTransactionDateFrom" runat="server"></asp:TextBox>
                            <asp:TextBox ID="txtTransactionDateTo" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right" class="ntextblack">
                            Select Topic:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlTopic" runat="server">
                                <asp:ListItem Value="">- Select Topic -</asp:ListItem>
                                <asp:ListItem Value="1">All</asp:ListItem>
                                <asp:ListItem Value="2">Week Day</asp:ListItem>
                                <asp:ListItem Value="3">Week End</asp:ListItem>
                                <asp:ListItem Value="4">Event</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trDDLOutboundType" style="display:none;">
                        <td colspan="2" align="right" class="ntextblack">
                            Outlet :
                        </td>
                        <td>
                            <asp:CheckBoxList ID="cbOutlet" runat="server" RepeatColumns="2" 
                                RepeatDirection="Horizontal" BorderStyle="Dashed" BorderWidth="1" Font-Size="Smaller" TextAlign="Right" Height="200px">
                                <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3"><label class="ntextred" id="lbErrorMessage" runat="server" 
                                visible="false">Error message display here if any</label></td>
                    </tr>              
                </table>
                <table align="center" style="width:900px">        
                    <tr>
                        <td align="center">
                        <asp:Button ID="btnViewReport" CssClass="ntextblack" runat="server" 
                            Text="ViewReport" onclick="btnViewReport_Click"/>&nbsp;
                        <asp:Button ID="btnExport" CssClass="ntextblack" runat="server" 
                            Text="ExportReport" onclick="btnExport_Click"/>&nbsp;
                        <asp:Button ID="btnCancel" CssClass="ntextblack" runat="server" Text="Cancel" 
                            onclick="btnCancel_Click" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <iframe id="iframeReport" frameborder="0" name="iframeReport" scrolling="auto" 
            style="width: 100%; height: 1000px"></iframe>
    </form>
</body>
</html>
