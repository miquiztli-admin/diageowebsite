﻿<%@ page language="C#" autoeventwireup="true" inherits="Reports_ReportPerformance, App_Web_reportperformance.aspx.dfa151d5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="ITOS.Utility.CustomControl" Namespace="ITOS.Utility.CustomControl" TagPrefix="cc1" %>
<%@ Register assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Jquery/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="../Jquery/DatePicker/js/jquery-ui-1.8.10.offset.datepicker.min.js"></script>
    <link type="text/css" href="../Jquery/DatePicker/css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    
    <style type="text/css">
        .ntab
        {
        	border-color:Black;
            background-color:#2890C7;
            color:White;
            font:bold 13px Arial, Helvetica, sans-serif;
        }
        .ntextred
        {
            color:Red;
            font:bold 13px Arial, Helvetica, sans-serif;
        }
        .nbtn1
        {
        	border-color:Black;
            color:Green;
            font:bold 18px Arial, Helvetica, sans-serif;
        }
        .nbtn2
        {
        	border-color:Black;
            color:Red;
            font:bold 18px Arial, Helvetica, sans-serif;
        }
    </style>
    
    <script language="javascript" type="text/javascript">
        var jqCriteria = jQuery.noConflict();
        jqCriteria(document).ready(function() {
            jqCriteria("#txtTransactionDateFrom").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });
            jqCriteria("#txtTransactionDateTo").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });
        });
    </script>

</head>
<body>
    <form id="FrmUserSearch" runat="server">
    <asp:ToolkitScriptManager ID="scriptManager" EnablePartialRendering="false" EnableScriptGlobalization="true" EnableScriptLocalization="true" runat="server">
    </asp:ToolkitScriptManager>  
    <asp:UpdatePanel ID="upHome" runat="server">
    <ContentTemplate>    
    <div>
        <fieldset>
        <legend class="ntextblack">&nbsp;Performance Report</legend>
            <table align="center">
                <tr>
                    <td colspan="2" align="right" class="ntextblack">
                        Show :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlShow" AutoPostBack="true" runat="server" CssClass="ntextblackdata"
                            Width="250px" onselectedindexchanged="ddlShow_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="ntextblack">
                        Event Date From<span class="ntextred">*</span>
                    </td>
                    <td>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtTransactionDateFrom" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtTransactionDateTo" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right" class="ntextblack">
                        Outlet :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlOutlet" runat="server" CssClass="ntextblackdata"
                            Width="250px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr id="trDDLOutboundType" runat="server" visible="false">
                    <td colspan="2" align="right" class="ntextblack">
                        Outbound Type :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlOutboundType" runat="server" CssClass="ntextblackdata"
                            Width="250px">
                        </asp:DropDownList>
                    </td>
                </tr>               
            </table>
        <table align="center" style="width:900px">        
           <tr>
                <td align="center">
                    <asp:Button ID="btnViewReport" CssClass="ntextblack" runat="server" 
                        Text="ViewReport" onclick="btnViewReport_Click"/>&nbsp;
                    <asp:Button ID="btnCancel" CssClass="ntextblack" runat="server" Text="Cancel" 
                        onclick="btnCancel_Click" />
                </td>
           </tr>
        </table>
        </fieldset>
    </div>
        </ContentTemplate>
    <Triggers>
    </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>