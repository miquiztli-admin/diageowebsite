﻿<%@ page language="C#" autoeventwireup="true" inherits="Reports_CommunicationReport, App_Web_communicationreport.aspx.dfa151d5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title></title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Jquery/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="../Jquery/DatePicker/js/jquery-ui-1.8.10.offset.datepicker.min.js"></script>
    <link type="text/css" href="../Jquery/DatePicker/css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    
    <script type="text/javascript" language="javascript">
        var jqCriteria = jQuery.noConflict();
        jqCriteria(document).ready(function() {
            jqCriteria("#txtActivePeriodFrom").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd', isBuddhist: false });
            jqCriteria("#txtActivePeriodTo").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd', isBuddhist: false });
            jqCriteria("#txtComparePeriodFrom").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd', isBuddhist: false });
            jqCriteria("#txtComparePeriodTo").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'yy-mm-dd', isBuddhist: false });

            /*jqCriteria('#ddlReport').change(function() {
                if (document.getElementById('ddlReport').value == 2) {
                    jqCriteria('#Compare').hide();
                    jqCriteria('#Communicate').hide();
                } else {
                    jqCriteria('#Compare').show();
                    jqCriteria('#Communicate').show();
                }
            });*/
            jqCriteria('#cbAllField').change(function() {
                doManageComparePanel();
            });
        });

        function doManageComparePanel() {
            var _blnDisabled = ! jqCriteria('#cbAllField').is(':checked');
            jqCriteria('#ddlEvent0').attr('disabled', _blnDisabled);
            jqCriteria('#txtComparePeriodFrom').attr('disabled', _blnDisabled);
            jqCriteria('#txtComparePeriodTo').attr('disabled', _blnDisabled);        
        }
</script>
</head>
<body onload="doManageComparePanel();">
    <form id="form1" runat="server">
        <div>
            <table class="ntextblack">
                <tr>
                    <td>
                        <label class="ntextblack">Criteria :</label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlReport" runat="server" Height="16px" Width="159px" >
                            <asp:ListItem Value="">- Select Report -</asp:ListItem>
                            <asp:ListItem Value="1">Communication Report</asp:ListItem>
                            <asp:ListItem Value="2">Turn up Report</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <br />
            <fieldset id="FieldSet" runat="server">
            <legend id="HeaderPage" runat="server" class="ntextblack">Data Condition</legend>
            <table align="center" style="width: 100%; border-style: solid; border-color: Black" class="ntextblack">
                <tr>
                    <td >
                        <label class="ntextblack">Criteria :</label>
                    </td>
                    <td style="text-align: left"> 
                        <asp:DropDownList ID="ddlEvent" runat="server" Height="16px" 
                        Width="159px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="ntextblack">
                        Period :</label>
                    </td>
                    <td>
                        from
                        <asp:TextBox ID="txtActivePeriodFrom" runat="server"></asp:TextBox>
                        &nbsp;to
                        <asp:TextBox ID="txtActivePeriodTo" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table> 
            </fieldset>
         </div>
         <div>
            <table align="center" style="width: 100%; border-style: solid; border-color: White" class="ntextblack">
                <tr>
                    <td class="ntextblack">
                    <div id="Compare" runat="server">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="cbAllField" runat="server" type="checkbox"  /> Compare
                    </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="Communicate" runat="server">
                            <fieldset id="FieldSet0" runat="server">
                            <legend id="HeaderPage0" runat="server" class="ntextblack">Data Condition</legend>
                            <table align="center" style="width: 100%; border-style: solid; border-color: Black">
                                <tr>
                                    <td >
                                        <label class="ntextblack">Criteria :</label>
                                    </td>
                                    <td style="text-align: left"> 
                                        <asp:DropDownList ID="ddlEvent0" runat="server" Height="16px" 
                                            Width="159px">
                                                <asp:ListItem Value="1">Halloween Party</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="style7">
                                        <label class="ntextblack">
                                        Period :</label></td>
                                    <td class="style2">
                                        from
                                        <asp:TextBox ID="txtComparePeriodFrom" runat="server"></asp:TextBox>
                                        &nbsp;to
                                        <asp:TextBox ID="txtComparePeriodTo" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                     </div>
                  </td>
                </tr>
            </table>
        </div>
        <div>
            <asp:Button ID="btn_ViewReport" runat="server" CssClass="ntextblack" Text="View Report" 
                Width="80px" onclick="btn_ViewReport_Click"  />
            <asp:Button ID="btn_RemoveCondition" runat="server" CssClass="ntextblack" 
                Text="Cancel" Width="80px"  />
        </div>
        <iframe id="iframeReport" frameborder="0" name="iframeReport" scrolling="auto" 
            style="width: 100%; height: 1000px"></iframe>
    </form>
</body>
</html>
