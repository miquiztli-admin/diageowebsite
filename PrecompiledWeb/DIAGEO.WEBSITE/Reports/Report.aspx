﻿<%@ page language="C#" autoeventwireup="true" inherits="Reports_Report, App_Web_report.aspx.dfa151d5" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="ITOS.Utility.CustomControl" Namespace="ITOS.Utility.CustomControl" TagPrefix="cc1" %>
<%@ Register assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>View Report</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../Style/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="../Style/block.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery.blockUI.js" type="text/javascript"></script>
    <base target="_self"/>
</head>
<body>
    <form id="FrmUserSearch" runat="server">
    <asp:ToolkitScriptManager ID="scriptManager" EnablePartialRendering="true" EnableScriptGlobalization="true" EnableScriptLocalization="true" runat="server"></asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="upHome" runat="server">
    <ContentTemplate>    
    <div>
        <fieldset>
        <legend class="ntextblack">Report</legend>
            <table align="center">
                <tr>
                    <td align="right">
                        <asp:Label ID="lblYear" CssClass="ntextblack" runat="server" Text="Year : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtYear" CssClass="ntextblackdata" MaxLength="4" Onkeypress="Javascript:return validateNumKey();"
                            runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="ntextblack">
                        Show :
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlShow" runat="server" CssClass="ntextblackdata"
                            Width="170px">
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        <table align="center" style="width:900px">        
           <tr>
                <td align="center">
                    <asp:Button ID="btnViewReport" CssClass="ntextblack" runat="server" 
                        Text="ViewReport" onclick="btnViewReport_Click"/>&nbsp;
                    <asp:Button ID="btnCancel" CssClass="ntextblack" runat="server" Text="Cancel" 
                        onclick="btnCancel_Click" />
                </td>
           </tr>
        </table>
        </fieldset>
    </div>
    </ContentTemplate>
    <Triggers>
    </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>