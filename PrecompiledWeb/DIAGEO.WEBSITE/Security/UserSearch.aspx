﻿<%@ page language="C#" autoeventwireup="true" inherits="Security_UserSearch, App_Web_usersearch.aspx.cecc3084" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="ITOS.Utility.CustomControl" Namespace="ITOS.Utility.CustomControl" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>User Search</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../Style/block.css" rel="stylesheet" type="text/css" />
    <link href="../Style/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    <script type="text/javascript">  
      function  blockUI()
      {
        $.blockUI();
      }
    </script>
     
    <script type="text/javascript">  
      function  unblockUI()
      {
        $.unblockUI();
      }
    </script>
    
</head>
<body>
    <form id="FrmUserSearch" runat="server">
    <asp:ToolkitScriptManager ID="scriptManager" EnablePartialRendering="true" EnableScriptGlobalization="true" EnableScriptLocalization="true" runat="server"></asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="upHome" runat="server">
    <ContentTemplate>    
    <div>
        <fieldset>
        <legend class="ntextblack">User</legend>
        <table align="center">
            <tr>
                <td align="right"><asp:Label ID="lblUserName" CssClass="ntextblack" runat="server" Text="User Name : "></asp:Label></td>
                <td align="left"><asp:TextBox ID="txtUserName" CssClass="ntextblackdata" runat="server"></asp:TextBox></td>                
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lblGroup" CssClass="ntextblack" runat="server" Text="Group : "></asp:Label></td>
                <td align="left"><asp:DropDownList ID="ddlGroup" Width="150px" runat="server"></asp:DropDownList>
                </td>                
            </tr>  
            <tr>
                <td align="right"><asp:Label ID="lblEmail" CssClass="ntextblack" runat="server" Text="Email : "></asp:Label></td>
                <td align="left"><asp:TextBox ID="txtEmail" CssClass="ntextblackdata" MaxLength="100" runat="server" Width="150px"></asp:TextBox></td>                
            </tr> 
            <tr>
                <td align="right"><asp:Label ID="lbliActive" CssClass="ntextblack" runat="server" Text="Active : "></asp:Label></td>
                <td align="left">
                    <asp:RadioButtonList ID="rdoActive" CssClass="ntextblackdata" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>                        
                    </asp:RadioButtonList>
                </td>                
            </tr>                                                
        </table>
        <table align="center" style="width:900px">        
           <tr>
                <td align="center">
                    <asp:Button ID="btnSearch" CssClass="ntextblack" runat="server" Text="Search" onclick="btnSearch_Click" OnClientClick="blockUI();"/>&nbsp;
                    <asp:Button ID="btnCancel" CssClass="ntextblack" runat="server" Text="Cancel" 
                        onclick="btnCancel_Click" />
                </td>
           </tr>
           <tr>
                <td align="right" style="width:900px"><asp:Button ID="btnNew" Width="100px" CssClass="ntextblack" 
                        runat="server" Text="New" onclick="btnNew_Click"/></td>
           </tr>
           <tr>
                <td align="center">
                    <asp:DataGrid ID="dgSearch" Width="900px" HeaderStyle-CssClass="gridheaderrowstyle" 
                        runat="server" AutoGenerateColumns="false" 
                        onitemdatabound="dgSearch_ItemDataBound" 
                        onitemcommand="dgSearch_ItemCommand" ItemStyle-CssClass="gridrowstyle" AlternatingItemStyle-CssClass="gridalternativerowstyle">
                        <Columns>
                            <asp:BoundColumn DataField="UserName" HeaderText="User Name" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="GroupName" HeaderText="Group Name" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Email" HeaderText="Email" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ActiveWord" HeaderText="Active"></asp:BoundColumn>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" Width="100px" runat="server" Text="Edit" CssClass="ntextblack" CommandName="Edit" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="UserId" HeaderText="UserId" Visible="false"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>               
        </table>
        <table id="tableNavigation" visible="false" runat="server" class="ntextblack" align="center">
            <tr><td><cc1:MultiPageNavigation id="pgSearch" runat="server" HaveTextBox="true" CssClass="ntextblackdata" NavigatorAlign="right" OnGoClick="pgSearch_GoClick" OnPageClick="pgSearch_PageClick" StringNextPage=">>" StringPreviousPage="<<" OnRowPerPageSelectIndexChanged="pgSearch_RowPerPageSelectIndexChanged"></cc1:MultiPageNavigation> </td></tr>
        </table>
        </fieldset>
    </div>
    </ContentTemplate>
    <Triggers>
    </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
