﻿<%@ page language="C#" autoeventwireup="true" inherits="ExtracConsumerMasterData, App_Web_extracconsumermasterdata.aspx.4bda1638" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/jquery-1.6.2.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.8.16.custom.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Jquery/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="../Jquery/DatePicker/js/jquery-ui-1.8.10.offset.datepicker.min.js"></script>
    <link type="text/css" href="../Jquery/DatePicker/css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
    var jqCriteria = jQuery.noConflict();
    jqCriteria(document).ready(function() {
        //us_CreateDatepicker($, '#txtDateBirthFrom,#txtDateBirthTo,#txtActivePeriodFrom,#txtActivePeriodTo');
        setControl();
        jqCriteria("#txtDateBirthFrom").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });
        jqCriteria("#txtDateBirthTo").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });
        jqCriteria("#txtActivePeriodFrom").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });
        jqCriteria("#txtActivePeriodTo").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });

    });
    
    function setControl() {
        jqCriteria("#ddl1").hide();
        jqCriteria("#ddl2").hide();
        jqCriteria("#ddl3").hide();
        jqCriteria("#ddl4").hide();
        jqCriteria("#ddl5").hide();
        jqCriteria("#ddl6").hide();
        jqCriteria("#ddl7").hide();
        if (jqCriteria("#ddlCrit").val() == '1') {
            jqCriteria("#ddl1").show();
        }
        if (jqCriteria("#ddlCrit").val() == "2") {
            jqCriteria("#ddl1").show();
            jqCriteria("#ddl2").show();
        }
        if (jqCriteria("#ddlCrit").val() == "3") {
            jqCriteria("#ddl3").show();
        }
        if (jqCriteria("#ddlCrit").val() == "4") {
            jqCriteria("#ddl4").show();
        }
        if (jqCriteria("#ddlCrit").val() == "5") {
            jqCriteria("#ddl5").show();
        }
        if (jqCriteria("#ddlCrit").val() == "6") {
            jqCriteria("#ddl6").show();
        }
        if (jqCriteria("#ddlCrit").val() == "7") {
            jqCriteria("#ddl7").show();
        }
    }
    function SelectAll(id) {
        //get reference of GridView control
        var grid = document.getElementById("<%= dgSearch.ClientID %>");
        //variable to contain the cell of the grid
        var cell;

        if (grid.rows.length > 0) {
            //loop starts from 1. rows[0] points to the header.
            for (i = 1; i < grid.rows.length; i++) {
                //get the reference of first column
                cell = grid.rows[i].cells[0];

                //loop according to the number of childNodes in the cell
                for (j = 0; j < cell.childNodes.length; j++) {
                    //if childNode type is CheckBox                 
                    if (cell.childNodes[j].type == "checkbox") {
                        //assign the status of the Select All checkbox to the cell checkbox within the grid
                        cell.childNodes[j].checked = document.getElementById(id).checked;
                    }
                }
            }
        }
    }

    function Deselect(id) {
        if (id.checked == false) {
            if (document.getElementById("cbAllField").checked) {
                document.getElementById("cbAllField").checked = false;
            }
        }
    }
    
    </script>
    <style type="text/css">
        #ddl1
        {
            width: 126px;
        }
        .style2
        {
            width: 130px;
        }
        .style3
        {
            width: 678px;
        }
        .style4
        {
            width: 226px;
        }
        .style5
        {
            width: 160px;
            text-align: right;
        }
        .style6
        {
            text-align: right;
        }
        .style7
        {
            width: 189px;
            text-align: right;
        }
        </style>
    
</head>
<body>
    <form id="form1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server" ></asp:ScriptManager>
    <script type="text/javascript" language="javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function() {
            // re-bind your jQuery events here
            jqCriteria("#txtDateBirthFrom").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });
            jqCriteria("#txtDateBirthTo").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });
            jqCriteria("#txtActivePeriodFrom").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });
            jqCriteria("#txtActivePeriodTo").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });
        });
    </script>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
    <ContentTemplate>
    <div>
        <fieldset id="FieldSet" runat="server">
        <legend id="HeaderPage" runat="server" class="ntextblack">Set Records Criteria</legend>
        <table align="center" style="width: 100%; border-style: solid; border-color: Black">
            <tr>
                <td class="style7">
                    <label class="ntextblack">Export :</label>
                </td>
                <td  colspan="5" style="text-align: left"> 
                    <asp:RadioButtonList ID="rdlExport" runat="server" CssClass="ntextblack" 
                        Height="26px" RepeatColumns="2" Width="492px">
                        <asp:ListItem Value="All">All Records</asp:ListItem>
                        <asp:ListItem Value="Restricted">Restricted Records</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="style7">
                    <label class="ntextblack">Criteria :</label>
                </td>
                <td class="style2" colspan="2">
                    <asp:DropDownList ID="ddlCrit" runat="server"  AutoPostBack="true" 
                        onselectedindexchanged="ddlCrit_SelectedIndexChanged1">
                        <asp:ListItem Value="1">Register</asp:ListItem>
                        <asp:ListItem Value="2">Repeat</asp:ListItem>
                        <asp:ListItem Value="3">Age</asp:ListItem>
                        <asp:ListItem Value="4">Gender</asp:ListItem>
                        <asp:ListItem Value="5">Date of Birth</asp:ListItem>
                        <asp:ListItem Value="6">Province</asp:ListItem>
                        <asp:ListItem Value="7">Active Period</asp:ListItem>
                    </asp:DropDownList>
              </td>
              <td class="style3" colspan="3">
                 <div class="ntextblack" style="width:80%" id="ddl1" runat="server">
                    <asp:DropDownList ID="ddlOutlet" runat="server" Width="200px">
                            <asp:ListItem>Outlet</asp:ListItem>
                        </asp:DropDownList>
                 </div>
                 <div class="ntextblack" id="ddl2"  runat="server">
                        <asp:DropDownList ID="ddlOutlet2" runat="server"
                            Width="199px">
                            <asp:ListItem>Outlet</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Frequency :&nbsp;<asp:TextBox ID="txtFrequencyFrom" runat="server" Width="34px" OnKeyPress="validateNumKey()"></asp:TextBox>&nbsp; 
                        to&nbsp;
                        <asp:TextBox ID="txtFrequencyTo" runat="server" Width="34px" OnKeyPress="validateNumKey()"></asp:TextBox>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Recently :
                        <asp:DropDownList ID="ddlRecentOutlet" runat="server" 
                            Width="160px">
                            <asp:ListItem Value="">All</asp:ListItem>
                        </asp:DropDownList>
                  </div>
                    <div class="ntextblack" id="ddl3"   runat="server">
                        Range :                         
                        <asp:TextBox ID="txtRangeFrom" runat="server" Width="34px" OnKeyPress="validateNumKey()"></asp:TextBox>&nbsp; to&nbsp;                         
                        <asp:TextBox ID="txtRangeTo" runat="server" Width="34px" OnKeyPress="validateNumKey()"></asp:TextBox>
                    </div>
                    <div class="ntextblack" id="ddl4"   runat="server">
                    <asp:DropDownList ID="ddlGender" runat="server" Width="200px">
                            <asp:ListItem Value="">All</asp:ListItem>
                            <asp:ListItem Value="Female">Female</asp:ListItem>
                            <asp:ListItem Value="Male">Male</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="ntextblack" id="ddl5"   runat="server">
                        <asp:TextBox ID="txtDateBirthFrom" runat="server" 
                            OnKeyPress="validateDateTimeFormat()" MaxLength="8"></asp:TextBox>
                        &nbsp;&nbsp; to&nbsp;&nbsp;
                        <asp:TextBox ID="txtDateBirthTo" runat="server" 
                            OnKeyPress="validateDateTimeFormat()" MaxLength="8"></asp:TextBox>
                    </div>
                    <div class="ntextblack" id="ddl6"  runat="server">
                    <asp:DropDownList ID="ddlProvince" runat="server" Width="200px">
                            <asp:ListItem Value="">Unknown</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="ntextblack" id="ddl7"  runat="server">
                        <asp:TextBox ID="txtActivePeriodFrom" runat="server" 
                            OnKeyPress="validateDateTimeFormat()" MaxLength="8"></asp:TextBox>
                        &nbsp; &nbsp;to&nbsp;&nbsp;
                        <asp:TextBox ID="txtActivePeriodTo" runat="server" 
                            OnKeyPress="validateDateTimeFormat()" MaxLength="8"></asp:TextBox>
                    </div>
                    
                </td>
            </tr>
            <tr>
                <td class="style6" colspan="2">
                    &nbsp;</td>
                <td class="style6">
                    &nbsp;</td>
                <td class="style6">
                    &nbsp;</td>
                <td class="style6">
                    &nbsp;</td>
                <td class="style6">
                <table width="100%"><tr><td>
                    <asp:RadioButtonList ID="rdlMatch" runat="server" Font-Bold="True" 
                        Font-Names="Century Gothic" Font-Size="12px" Height="12px" RepeatColumns="2" 
                        Width="273px">
                        <asp:ListItem Value="Match all">Match all</asp:ListItem>
                        <asp:ListItem Value="Match any">Match any</asp:ListItem>
                    </asp:RadioButtonList>
                    </td></tr></table>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="width:100%">
                    <div>
                        <asp:Button ID="btn_AddCondition" runat="server" CssClass="ntextblack" 
                            onclick="btn_AddCondition_Click" Text="Add" Width="80px" />
                        <asp:Button ID="btn_RemoveCondition" runat="server" CssClass="ntextblack" 
                            onclick="btn_RemoveCondition_Click" Text="Remove" Width="80px" />
                    </div>
                    <asp:DataGrid ID="dgCriteria" runat="server" 
                        AlternatingItemStyle-CssClass="gridalternativerowstyle" 
                        AutoGenerateColumns="false" HeaderStyle-CssClass="gridheaderrowstyle" 
                        ItemStyle-CssClass="gridrowstyle" style="text-align: center" Width="900px">
                        <Columns>
                            <asp:TemplateColumn>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="cbAllCriteria" runat="server" AutoPostBack="true" 
                                        OnCheckedChanged="ClearCheck" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <input ID="cbTypeCriteria" runat="server" type="checkbox" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="Type" HeaderStyle-HorizontalAlign="Center" 
                                HeaderText="Type" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Value" HeaderStyle-HorizontalAlign="Center" 
                                HeaderText="Value" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RangeFrom" HeaderStyle-HorizontalAlign="Center" 
                                HeaderText="RangeFrom" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="RangeTo" HeaderStyle-HorizontalAlign="Center" 
                                HeaderText="RangeTo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="DateFrom" HeaderStyle-HorizontalAlign="Center" 
                                HeaderText="DateFrom" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn DataField="DateTo" HeaderStyle-HorizontalAlign="Center" 
                                HeaderText="DateTo" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
        </table>
        
        </fieldset>
    </div>
     <div>
        <fieldset id="FieldSet1" runat="server">
        <legend id="Legend1" runat="server" class="ntextblack">Display Properties</legend>
        <table align="center" style="width: 100%; border-style: solid; border-color: Black" class="ntextblack">
            <tr>
                <td class="style4">
                    <asp:Label ID="Label1" runat="server" Text="Column Properties" 
                        style="text-decoration: underline"></asp:Label>
                </td>
                <td style="text-align: right">
                    You can control the display of column by checking them.</td>
            </tr>
            <tr>
                <td colspan="2">
                <table width="100%">
                    <tr>
                        <td class="ntextblack">
                        <div>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="cbAllField" runat="server" type="checkbox" onclick="SelectAll('cbAllField')" /> All Fields
                        </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <div id="ogrid" style="overflow:scroll; height:166px; width:917px; text-align:left">
                            <asp:DataGrid ID="dgSearch" Width="900px" HeaderStyle-CssClass="gridheaderrowstyle"
                                    runat="server" AutoGenerateColumns="false"  ItemStyle-CssClass="gridrowstyle" 
                                    AlternatingItemStyle-CssClass="gridalternativerowstyle" 
                                     style="text-align: center">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Show" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <input id="cbField" type="checkbox" runat="server" onclick="Deselect(this);" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Column Name" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center">
                                             <ItemTemplate>
                                                   <asp:Label id="lbColumnName" Text='<%#DataBinder.Eval(Container.DataItem, "name") %>' runat="server" />
                                             </ItemTemplate>
                                      </asp:TemplateColumn>                                        
                                        
                                  </Columns>
                                </asp:DataGrid>
                                </div>
                          </td>
                       </tr>
                    </table>
                </td>
            </tr>
            </table>
        
        </fieldset>
    </div>
     <div>
        <fieldset id="FieldSet2" runat="server">
        <legend id="Legend2" runat="server" class="ntextblack">Export Properties</legend>
        <table align="center" style="width: 100%; border-style: solid; border-color: Black">
            <tr class="ntextblack">
                <td class="style5">
                    
                    File Format : </td>
                <td>
                    
                    <asp:DropDownList ID="ddlFileFormat" runat="server" Height="17px" Width="265px">
                            <asp:ListItem>Comma Separated Values(CSV)</asp:ListItem>
                        </asp:DropDownList>
                    
                </td>
            </tr>
            
        </table>
        </fieldset>
    </div>
    <p style="text-align: right">
        <asp:Button ID="btnCancel"  runat="server" Text="Cancel" Font-Bold="True" 
            Font-Names="Century Gothic" Font-Size="12px"  />
                            &nbsp;
        <asp:Button ID="btnExport" runat="server" Text="Export" 
            onclick="btnExport_Click" Font-Bold="True" Font-Names="Century Gothic" 
            Font-Size="12px" />
    </p>
     </fieldset>
    </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
