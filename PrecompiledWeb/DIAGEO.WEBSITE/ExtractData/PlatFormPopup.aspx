﻿<%@ page language="C#" autoeventwireup="true" inherits="ExtractData_PlatFormPopup, App_Web_platformpopup.aspx.4bda1638" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Platform</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="FrmPlatform" runat="server">   
    <asp:ToolkitScriptManager ID="scriptManager" EnablePartialRendering="true" EnableScriptGlobalization="true" EnableScriptLocalization="true" runat="server">
    </asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="upHome" runat="server">
    <ContentTemplate>  
    <table align="center">
        <tr><td align="right">
             <asp:Button ID="btnSelect" runat="server" Text="Select" onclick="btnSelect_Click" />
        </td></tr>
        <tr><td align="center">
            <asp:DataGrid ID="dgPlatform" runat="server" Width="900px" HeaderStyle-CssClass="gridheaderrowstyle"
                AutoGenerateColumns="false" ItemStyle-CssClass="gridrowstyle" 
                AlternatingItemStyle-CssClass="gridalternativerowstyle">
                <Columns>
                    <asp:TemplateColumn>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkPlatform" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:BoundColumn DataField="name" HeaderText="Platform Name"></asp:BoundColumn>
                    <asp:BoundColumn DataField="platformId" Visible="false"></asp:BoundColumn>
                </Columns>            
            </asp:DataGrid>
        </td></tr>        
    </table>
     </ContentTemplate><Triggers></Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
