﻿<%@ page language="C#" autoeventwireup="true" inherits="ExtractData_ExtractionConsumerData, App_Web_extractionconsumerdata.aspx.4bda1638" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script type="text/javascript" src="../Jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="../Jquery/DatePicker/js/jquery-ui-1.8.10.offset.datepicker.min.js"></script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="../Jquery/DatePicker/css/ui-lightness/jquery-ui-1.8.10.custom.css" rel="stylesheet" />
    <script type="text/javascript">
        function ConfirmSave(count) {

            if (confirm("Total record of export is " + count.toString() + ". Do you want to export ?")) 
            {
                document.getElementById('btnExportDataInvisible').click();
            }
            
        }
    </script>
</head>
<body>
    <form id="FrmExtractConsumer" runat="server">
    <asp:ToolkitScriptManager ID="scriptManager" EnablePartialRendering="true" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" runat="server">
    </asp:ToolkitScriptManager>    
    <script type="text/javascript">
         function pageLoad() {
             $("#txtRegisterDateFrom").unbind();
             $("#txtRegisterDateFrom").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });
             $("#txtRegisterDateTo").unbind();
             $("#txtRegisterDateTo").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });

             $("#txtLastVisitDateFrom").unbind();
             $("#txtLastVisitDateFrom").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });
             $("#txtLastVisitDateTo").unbind();
             $("#txtLastVisitDateTo").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });

             $("#txtDateOfBirthFrom").unbind();
             $("#txtDateOfBirthFrom").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });
             $("#txtDateOfBirthTo").unbind();
             $("#txtDateOfBirthTo").datepicker({ changeMonth: true, changeYear: true, dateFormat: 'dd/mm/yy', isBuddhist: false });

         }  
    </script>
    
    <asp:UpdatePanel ID="upHome" runat="server">
        <ContentTemplate>
            <table align="center" style="width: 800px; border-style: solid; border-color: Black">
                <tr>
                    <td align="left" style="width: 200px">
                        <asp:Label ID="lblPlatform" CssClass="ntextblack" runat="server" Text="Platform"></asp:Label>
                    </td>
                    <td align="left" style="width: 200px">
                        <asp:Button ID="btnChoosePlatform" CssClass="ntextblack" runat="server" Text="Choose Platform"
                            OnClick="btnChoosePlatform_Click" />
                    </td>
                    <td style="width: 200px">
                        <asp:ListBox ID="listPlatform" Height="150px" CssClass="ntextblack" ForeColor="White"
                            BackColor="#99ccff" Width="100%" runat="server"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                    <td style="border-width: thin; border-top-style: solid; border-bottom-style: solid;
                        border-color: Gray" align="left">
                        <asp:Label ID="lblBrand" runat="server" CssClass="ntextblack" Text="Brand Preference"></asp:Label>
                    </td>
                    <td style="border-width: thin; border-top-style: solid; border-bottom-style: solid;
                        border-color: Gray" align="left" colspan="2">
                        <table>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkBaileys" runat="server" AutoPostBack="True" OnCheckedChanged="chkBaileys_CheckedChanged" />&nbsp;
                                    <asp:Label ID="lblBaileys" CssClass="ntextblack" runat="server" Text="Baileys"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkBenmore" runat="server" AutoPostBack="True" OnCheckedChanged="chkBenmore_CheckedChanged" />&nbsp;
                                    <asp:Label ID="lblBenmore" CssClass="ntextblack" runat="server" Text="Benmore"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkBenmoreJwBlack" runat="server" AutoPostBack="True" OnCheckedChanged="chkBenmoreJwBlack_CheckedChanged" />&nbsp;
                                    <asp:Label ID="lblJwBlack" CssClass="ntextblack" runat="server" Text="JwBlack"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkJwGold" runat="server" AutoPostBack="True" OnCheckedChanged="chkJwGold_CheckedChanged" />&nbsp;
                                    <asp:Label ID="lblJwGold" CssClass="ntextblack" runat="server" Text="JwGold"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkJwGreen" runat="server" OnCheckedChanged="chkJwGreen_CheckedChanged"
                                        AutoPostBack="True" />&nbsp;
                                    <asp:Label ID="lblJwGreen" CssClass="ntextblack" runat="server" Text="JwGreen"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkJwRed" runat="server" OnCheckedChanged="chkJwRed_CheckedChanged"
                                        AutoPostBack="True" />&nbsp;
                                    <asp:Label ID="lblJwRed" CssClass="ntextblack" runat="server" Text="JwRed"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkSmirnoff" runat="server" OnCheckedChanged="chkSmirnoff_CheckedChanged"
                                        AutoPostBack="True" />&nbsp;
                                    <asp:Label ID="lblSmimoff" CssClass="ntextblack" runat="server" Text="Smimoff"></asp:Label>
                            </tr>
                    </td>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkBallentine" runat="server" AutoPostBack="True" OnCheckedChanged="chkBallentine_CheckedChanged" />&nbsp;
                            <asp:Label ID="lblBallantines" CssClass="ntextblack" runat="server" Text="Ballentine"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkBlend" runat="server" AutoPostBack="True" OnCheckedChanged="chkBlend_CheckedChanged" />&nbsp;
                            <asp:Label ID="lblBlend" CssClass="ntextblack" runat="server" Text="Blend"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkChivas" runat="server" AutoPostBack="True" OnCheckedChanged="chkChivas_CheckedChanged" />&nbsp;
                            <asp:Label ID="lblChivas" CssClass="ntextblack" runat="server" Text="Chivas"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkDewar" runat="server" OnCheckedChanged="chkDewar_CheckedChanged"
                                AutoPostBack="True" />&nbsp;
                            <asp:Label ID="lblDewar" CssClass="ntextblack" runat="server" Text="Dewar"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chk100Pipers" runat="server" OnCheckedChanged="chk100Pipers_CheckedChanged"
                                AutoPostBack="True" />&nbsp;
                            <asp:Label ID="lbl100Piper" CssClass="ntextblack" runat="server" Text="100 Pipers"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chk100Pipers8yrs" runat="server" OnCheckedChanged="chk100Pipers8yrs_CheckedChanged"
                                AutoPostBack="True" />&nbsp;
                            <asp:Label ID="lbl100Pipers8yrs" CssClass="ntextblack" runat="server" Text="100Pipers8yrs"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkAbsolute" runat="server" OnCheckedChanged="chkAbsolute_CheckedChanged"
                                AutoPostBack="True" />&nbsp;
                            <asp:Label ID="lblAbsolute" CssClass="ntextblack" runat="server" Text="Absolute"></asp:Label>
                        </td>
                    </tr>
            </table>
            </td> </tr>
            <tr>
                <td style="border-width: thin; border-bottom-style: solid; border-color: Gray" align="left">
                    <asp:Label ID="lblSurvey" runat="server" CssClass="ntextblack" Text="Survey"></asp:Label>
                </td>
                <td style="border-width: thin; border-bottom-style: solid; border-color: Gray" colspan="2">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" CssClass="ntextblack" runat="server" Text="Baileys"></asp:Label>
                                <asp:CheckBoxList ID="chkBaileysList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                    runat="server" AutoPostBack="True" OnSelectedIndexChanged="chkBaileysList_SelectedIndexChanged">
                                    <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label3" CssClass="ntextblack" runat="server" Text="Benmore"></asp:Label>
                                <asp:CheckBoxList ID="chkFourABenmoreList" RepeatDirection="Horizontal" runat="server"
                                    CssClass="ntextblack" OnSelectedIndexChanged="chkFourABenmoreList_SelectedIndexChanged"
                                    AutoPostBack="True">
                                    <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label4" CssClass="ntextblack" runat="server" Text="JwBlack"></asp:Label>
                                <asp:CheckBoxList ID="chkJwBlackList" RepeatDirection="Horizontal" runat="server"
                                    CssClass="ntextblack" OnSelectedIndexChanged="chkJwBlackList_SelectedIndexChanged"
                                    AutoPostBack="True">
                                    <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label5" CssClass="ntextblack" runat="server" Text="JwGold"></asp:Label>
                                <asp:CheckBoxList ID="chkJwGoldList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                    runat="server" OnSelectedIndexChanged="chkJwGoldList_SelectedIndexChanged" AutoPostBack="True">
                                    <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label6" CssClass="ntextblack" runat="server" Text="JwGreen"></asp:Label>
                                <asp:CheckBoxList ID="chkJwGreenList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                    runat="server" OnSelectedIndexChanged="chkJwGreenList_SelectedIndexChanged" AutoPostBack="True">
                                    <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label7" CssClass="ntextblack" runat="server" Text="JwRed"></asp:Label>
                                <asp:CheckBoxList ID="chkJwRedList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                    runat="server" OnSelectedIndexChanged="chkJwRedList_SelectedIndexChanged" AutoPostBack="True">
                                    <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label8" CssClass="ntextblack" runat="server" Text="Smirnoff"></asp:Label>
                                <asp:CheckBoxList ID="chkSmirnoffList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                    runat="server" OnSelectedIndexChanged="chkSmirnoffList_SelectedIndexChanged"
                                    AutoPostBack="True">
                                    <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label10" CssClass="ntextblack" runat="server" Text="Ballentine"></asp:Label>
                                <asp:CheckBoxList ID="chkBallentineList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                    runat="server" OnSelectedIndexChanged="chkBallentineList_SelectedIndexChanged"
                                    AutoPostBack="True">
                                    <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label9" CssClass="ntextblack" runat="server" Text="Blend"></asp:Label>
                                <asp:CheckBoxList ID="chkBlendList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                    runat="server" OnSelectedIndexChanged="chkBlendList_SelectedIndexChanged" AutoPostBack="True">
                                    <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label11" CssClass="ntextblack" runat="server" Text="Chivas"></asp:Label>
                                <asp:CheckBoxList ID="chkChivasList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                    runat="server" OnSelectedIndexChanged="chkChivasList_SelectedIndexChanged" AutoPostBack="True">
                                    <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label12" CssClass="ntextblack" runat="server" Text="Dewar"></asp:Label>
                                <asp:CheckBoxList ID="chkDewarList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                    runat="server" OnSelectedIndexChanged="chkDewarList_SelectedIndexChanged" AutoPostBack="True">
                                    <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label13" CssClass="ntextblack" runat="server" Text="100Pipers"></asp:Label>
                                <asp:CheckBoxList ID="chk100PipersList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                    runat="server" OnSelectedIndexChanged="chk100PipersList_SelectedIndexChanged"
                                    AutoPostBack="True">
                                    <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label14" CssClass="ntextblack" runat="server" Text="100Pipers8yrs"></asp:Label>
                                <asp:CheckBoxList ID="chk100Pipers8yrsList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                    runat="server" OnSelectedIndexChanged="chk100Pipers8yrsList_SelectedIndexChanged"
                                    AutoPostBack="True">
                                    <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label15" CssClass="ntextblack" runat="server" Text="Absolute"></asp:Label>
                                <asp:CheckBoxList ID="chkAbsoluteList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                    runat="server" OnSelectedIndexChanged="chkAbsoluteList_SelectedIndexChanged"
                                    AutoPostBack="True">
                                    <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblGender" runat="server" Text="Gender" CssClass="ntextblack"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:RadioButtonList ID="rdoGender" runat="server" CssClass="ntextblack">
                        <asp:ListItem Text="Male" Value="MALE" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Female" Value="FEMALE"></asp:ListItem>
                        <asp:ListItem Text="All" Value=""></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblAge" runat="server" Text="Age" CssClass="ntextblack"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtAgeFrom" CssClass="ntextblack" runat="server" Width="50px"></asp:TextBox>
                    <asp:Label ID="lblTo" runat="server" Text="To" CssClass="ntextblack"></asp:Label>
                    <asp:TextBox ID="txtAgeTo" runat="server" Width="50px" CssClass="ntextblack"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblProvince" CssClass="ntextblack" runat="server" Text="Province"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:DropDownList ID="ddlProvince" CssClass="ntextblack" runat="server">
                        <asp:ListItem Text="Please Select" Value=""></asp:ListItem>
                        <asp:ListItem Text="เชียงใหม่" Value="เชียงใหม่"></asp:ListItem>
                        <asp:ListItem Text="เชียงราย" Value="เชียงราย"></asp:ListItem>
                        <asp:ListItem Text="เพชรบุรี" Value="เพชรบุรี"></asp:ListItem>
                        <asp:ListItem Text="เพชรบูรณ์" Value="เพชรบูรณ์"></asp:ListItem>
                        <asp:ListItem Text="เลย" Value="เลย"></asp:ListItem>
                        <asp:ListItem Text="แพร่" Value="แพร่"></asp:ListItem>
                        <asp:ListItem Text="แม่ฮ่องสอน" Value="แม่ฮ่องสอน"></asp:ListItem>
                        <asp:ListItem Text="กระบี่" Value="กระบี่"></asp:ListItem>
                        <asp:ListItem Text="กรุงเทพมหานคร" Value="กรุงเทพมหานคร"></asp:ListItem>
                        <asp:ListItem Text="กาญจนบุรี" Value="กาญจนบุรี"></asp:ListItem>
                        <asp:ListItem Text="กาฬสินธุ์" Value="กาฬสินธุ์"></asp:ListItem>
                        <asp:ListItem Text="กำแพงเพชร" Value="กำแพงเพชร"></asp:ListItem>
                        <asp:ListItem Text="ขอนแก่น" Value="ขอนแก่น"></asp:ListItem>
                        <asp:ListItem Text="จันทบุรี" Value="จันทบุรี"></asp:ListItem>
                        <asp:ListItem Text="ฉะเชิงเทรา" Value="ฉะเชิงเทรา"></asp:ListItem>
                        <asp:ListItem Text="ชลบุรี" Value="ชลบุรี"></asp:ListItem>
                        <asp:ListItem Text="ชัยนาท" Value="ชัยนาท"></asp:ListItem>
                        <asp:ListItem Text="ชัยภูมิ" Value="ชัยภูมิ"></asp:ListItem>
                        <asp:ListItem Text="ชุมพร" Value="ชุมพร"></asp:ListItem>
                        <asp:ListItem Text="ตรัง" Value="ตรัง"></asp:ListItem>
                        <asp:ListItem Text="ตราด" Value="ตราด"></asp:ListItem>
                        <asp:ListItem Text="ตาก" Value="ตาก"></asp:ListItem>
                        <asp:ListItem Text="นครนายก" Value="นครนายก"></asp:ListItem>
                        <asp:ListItem Text="นครปฐม" Value="นครปฐม"></asp:ListItem>
                        <asp:ListItem Text="นครพนม" Value="นครพนม"></asp:ListItem>
                        <asp:ListItem Text="นครราชสีมา" Value="นครราชสีมา"></asp:ListItem>
                        <asp:ListItem Text="นครศรีธรรมราช" Value="นครศรีธรรมราช"></asp:ListItem>
                        <asp:ListItem Text="นครสวรรค์" Value="นครสวรรค์"></asp:ListItem>
                        <asp:ListItem Text="นนทบุรี" Value="นนทบุรี"></asp:ListItem>
                        <asp:ListItem Text="นราธิวาส" Value="นราธิวาส"></asp:ListItem>
                        <asp:ListItem Text="น่าน" Value="น่าน"></asp:ListItem>
                        <asp:ListItem Text="บึงกาฬ" Value="บึงกาฬ"></asp:ListItem>
                        <asp:ListItem Text="บุรีรัมย์" Value="บุรีรัมย์"></asp:ListItem>
                        <asp:ListItem Text="ปทุมธานี" Value="ปทุมธานี"></asp:ListItem>
                        <asp:ListItem Text="ประจวบคีรีขันธ์" Value="ประจวบคีรีขันธ์"></asp:ListItem>
                        <asp:ListItem Text="ปราจีนบุรี" Value="ปราจีนบุรี"></asp:ListItem>
                        <asp:ListItem Text="ปัตตานี" Value="ปัตตานี"></asp:ListItem>
                        <asp:ListItem Text="พระนครศรีอยุธยา" Value="พระนครศรีอยุธยา"></asp:ListItem>
                        <asp:ListItem Text="พะเยา" Value="พะเยา"></asp:ListItem>
                        <asp:ListItem Text="พังงา" Value="พังงา"></asp:ListItem>
                        <asp:ListItem Text="พัทลุง" Value="พัทลุง"></asp:ListItem>
                        <asp:ListItem Text="พิจิตร" Value="พิจิตร"></asp:ListItem>
                        <asp:ListItem Text="พิษณุโลก" Value="พิษณุโลก"></asp:ListItem>
                        <asp:ListItem Text="ภูเก็ต" Value="ภูเก็ต"></asp:ListItem>
                        <asp:ListItem Text="มหาสารคาม" Value="มหาสารคาม"></asp:ListItem>
                        <asp:ListItem Text="มุกดาหาร" Value="มุกดาหาร"></asp:ListItem>
                        <asp:ListItem Text="ยโสธร" Value="ยโสธร"></asp:ListItem>
                        <asp:ListItem Text="ยะลา" Value="ยะลา"></asp:ListItem>
                        <asp:ListItem Text="ร้อยเอ็ด" Value="ร้อยเอ็ด"></asp:ListItem>
                        <asp:ListItem Text="ระนอง" Value="ระนอง"></asp:ListItem>
                        <asp:ListItem Text="ระยอง" Value="ระยอง"></asp:ListItem>
                        <asp:ListItem Text="ราชบุรี" Value="ราชบุรี"></asp:ListItem>
                        <asp:ListItem Text="ลพบุรี" Value="ลพบุรี"></asp:ListItem>
                        <asp:ListItem Text="ลำปาง" Value="ลำปาง"></asp:ListItem>
                        <asp:ListItem Text="ลำพูน" Value="ลำพูน"></asp:ListItem>
                        <asp:ListItem Text="ศรีสะเกษ" Value="ศรีสะเกษ"></asp:ListItem>
                        <asp:ListItem Text="สกลนคร" Value="สกลนคร"></asp:ListItem>
                        <asp:ListItem Text="สงขลา" Value="สงขลา"></asp:ListItem>
                        <asp:ListItem Text="สตูล" Value="สตูล"></asp:ListItem>
                        <asp:ListItem Text="สมุทรปราการ" Value="สมุทรปราการ"></asp:ListItem>
                        <asp:ListItem Text="สมุทรสงคราม" Value="สมุทรสงคราม"></asp:ListItem>
                        <asp:ListItem Text="สมุทรสาคร" Value="สมุทรสาคร"></asp:ListItem>
                        <asp:ListItem Text="สระแก้ว" Value="สระแก้ว"></asp:ListItem>
                        <asp:ListItem Text="สระบุรี" Value="สระบุรี"></asp:ListItem>
                        <asp:ListItem Text="สิงห์บุรี" Value="สิงห์บุรี"></asp:ListItem>
                        <asp:ListItem Text="สุโขทัย" Value="สุโขทัย"></asp:ListItem>
                        <asp:ListItem Text="สุพรรณบุรี" Value="สุพรรณบุรี"></asp:ListItem>
                        <asp:ListItem Text="สุราษฎร์ธานี" Value="สุราษฎร์ธานี"></asp:ListItem>
                        <asp:ListItem Text="สุรินทร์" Value="สุรินทร์"></asp:ListItem>
                        <asp:ListItem Text="หนองคาย" Value="หนองคาย"></asp:ListItem>
                        <asp:ListItem Text="หนองบัวลำภู" Value="หนองบัวลำภู"></asp:ListItem>
                        <asp:ListItem Text="อ่างทอง" Value="อ่างทอง"></asp:ListItem>
                        <asp:ListItem Text="อำนาจเจริญ" Value="อำนาจเจริญ"></asp:ListItem>
                        <asp:ListItem Text="อุดรธานี" Value="อุดรธานี"></asp:ListItem>
                        <asp:ListItem Text="อุตรดิตถ์" Value="อุตรดิตถ์"></asp:ListItem>
                        <asp:ListItem Text="อุทัยธานี" Value="อุทัยธานี"></asp:ListItem>
                        <asp:ListItem Text="อุบลราชธานี" Value="อุบลราชธานี"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblOutlet" CssClass="ntextblack" runat="server" Text="Register Outlet"></asp:Label>
                </td>
                <td>
                    <asp:Button ID="btnChooseOutletRegister" CssClass="ntextblack" runat="server" Text="Choose Outlet"
                        OnClick="btnChooseOutletRegister_Click" />
                </td>
                <td>
                    <asp:ListBox ID="listOutletRegister" CssClass="ntextblack" Height="150px" ForeColor="White"
                        BackColor="#99ccff" Width="100%" runat="server"></asp:ListBox>
                </td>
            </tr>
            <tr>
                <td style="width: 200px">
                    <asp:Label ID="lblRegisterDate" CssClass="ntextblack" runat="server" Text="Register Date"></asp:Label>
                </td>
                <td style="width: 400px" colspan="2">
                    <input type="text" id="txtRegisterDateFrom" style="width: 100px" name="txtRegisterDateFrom"
                        runat="server" />&nbsp;
                    <asp:Label ID="lblToRegisterDate" runat="server" CssClass="ntextblack" Text="To"></asp:Label>&nbsp;
                    <input type="text" id="txtRegisterDateTo" style="width: 100px" name="txtRegisterDateTo"
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblLastVisitOutlet" CssClass="ntextblack" runat="server" Text="Last Visit Outlet"></asp:Label>
                </td>
                <td>
                    <asp:Button ID="btnChooseOutletLastVisit" CssClass="ntextblack" runat="server" Text="Choose Outlet"
                        OnClick="btnChooseOutletLastVisit_Click" />
                </td>
                <td>
                    <asp:ListBox ID="listOutletLastVisit" CssClass="ntextblack" Height="150px" ForeColor="White"
                        BackColor="#99ccff" Width="100%" runat="server"></asp:ListBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblLastVisitDate" CssClass="ntextblack" runat="server" Text="Last Visit Date"></asp:Label>
                </td>
                <td colspan="2">
                    <input type="text" id="txtLastVisitDateFrom" cssclass="ntextblack" style="width: 100px"
                        name="txtLastVisitDateFrom" runat="server" />&nbsp;
                    <asp:Label ID="lblTOLastVisit" runat="server" CssClass="ntextblack" Text="To"></asp:Label>&nbsp;
                    <input type="text" id="txtLastVisitDateTo" cssclass="ntextblack" style="width: 100px"
                        name="txtLastVisitDateTo" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblDateOfBirth" CssClass="ntextblack" runat="server" Text="Date of birth"></asp:Label>
                </td>
                <td colspan="2">
                    <input type="text" id="txtDateOfBirthFrom" cssclass="ntextblack" style="width: 100px"
                        name="txtDateOfBirthFrom" runat="server" />&nbsp;
                    <asp:Label ID="lblTODateOfBirth" runat="server" CssClass="ntextblack" Text="To"></asp:Label>&nbsp;
                    <input type="text" id="txtDateOfBirthTo" cssclass="ntextblack" style="width: 100px"
                        name="txtDateOfBirthTo" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblEmailISActive" CssClass="ntextblack" runat="server" Text="Email"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:CheckBox ID="chkEmail" CssClass="ntextblack" runat="server" />
                    &nbsp;<asp:Label ID="lblActive" CssClass="ntextblack" runat="server" Text="Active"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblMobileISActive" CssClass="ntextblack" runat="server" Text="Mobile"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:CheckBox ID="chkMobile" CssClass="ntextblack" runat="server" />
                    &nbsp;<asp:Label ID="lblMobile" CssClass="ntextblack" runat="server" Text="Active"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCallcenterISActive" CssClass="ntextblack" runat="server" Text="CallCenter"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:CheckBox ID="chkCallcenter" CssClass="ntextblack" runat="server" />
                    &nbsp;<asp:Label ID="lblCallcenter" CssClass="ntextblack" runat="server" Text="Active"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblSubscribe" CssClass="ntextblack" runat="server" Text="Subscribe"></asp:Label>
                </td>
                <td>
                    <asp:Button ID="btnChooseSubscribe" CssClass="ntextblack" runat="server" Text="Choose Platform"
                        OnClick="btnChooseSubscribe_Click" />
                </td>
                <td>
                    <asp:ListBox ID="listPlatformCallcenter" CssClass="ntextblack" Height="150px" ForeColor="White"
                        BackColor="#99ccff" Width="100%" runat="server"></asp:ListBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblExport" CssClass="ntextblack" runat="server" Text="Export"></asp:Label>
                </td>
                <td colspan="2">
                    <asp:RadioButtonList ID="rdoExport" runat="server" CssClass="ntextblack" RepeatDirection="Horizontal">
                        <asp:ListItem Text="xls" Value="xls" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="csv" Value="csv"></asp:ListItem>
                        <asp:ListItem Text="pdf" Value="pdf"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            </table>
            <br />
            <table align="center">
                <tr>
                    <td>
                        <asp:UpdateProgress ID="upro" runat="server" DisplayAfter="0" DynamicLayout="false">
                            <ProgressTemplate>
                                &nbsp;loading...
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="btnExportData" CssClass="ntextblack" runat="server" Text="Export Data"
                            OnClick="btnExportData_Click" />
                    </td>
                </tr>
                <tr>
                    <td style="display: none">
                        <asp:Button ID="btnRefresh" runat="server" CssClass="ntextblack" OnClick="btnRefresh_Click" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
     <table style="display:none">
            <tr><td>
            <asp:Button ID="btnExportDataInvisible" CssClass="ntextblack" runat="server" 
                        onclick="btnExportDataInvisible_Click" />
            </td></tr>
        </table>  
    </form>
</body>
</html>
