﻿<%@ page language="C#" autoeventwireup="true" inherits="ExtractData_EventVipPopUp, App_Web_eventvippopup.aspx.4bda1638" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <base target="_self"/>
    <script language="javascript" type="text/javascript">
        function ValidateUpload() {
            if (document.getElementById('FileUpload').value == '') {
                alert('คุณยังไม่ได้เลือกไฟล์');
                return false;
            }
            else
                return true;
        }
        function ClearBrowse() {
            document.getElementById('FileUpload').value = '';
            return true;
        }
        function DialogConfirm() {
            if (document.getElementById('FileUpload').value == '') 
            {
                alert('คุณยังไม่ได้เลือกไฟล์');
                return false;
            }

            if (confirm('คุญต้องการอัพโหลดข้อมูลไฟล์นี้ ใช่รึไม่?'))
                return true;
            else
                return false;
        }
    </script>
</head>
<body>
    <form id="UploadFile" runat="server">
    <div>
        <fieldset id="FieldSet" runat="server">
        <legend id="HeaderPage" runat="server" class="ntextblack">Upload</legend>
        <br />
        <table align="center" style="vertical-align:middle">
            <tr>
                <td align="center">
                    <asp:Label ID="lblUpload" runat="server" Text="อัพโหลดไฟล์ : " CssClass="ntextblack"></asp:Label>&nbsp;
                    <asp:FileUpload ID="FileUpload" runat="server"/>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            </table>
        <br />
        <table align="center">
            <tr>
                <td>
                    <asp:Button ID="btnUpload" runat="server" Text="Upload File" CssClass="ntextblack" 
                        Width="120px" Visible="true" OnClientClick="return DialogConfirm();" onclick="btnUpload_Click" />
                </td>
            </tr>
        </table>
        </fieldset>
    </div>
    </form>
</body>
</html>
