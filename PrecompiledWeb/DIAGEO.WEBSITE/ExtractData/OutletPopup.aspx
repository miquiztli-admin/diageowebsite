﻿<%@ page language="C#" autoeventwireup="true" inherits="ExtractData_PlatFormPopup, App_Web_outletpopup.aspx.4bda1638" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Outlet</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="FrmOutlet" runat="server">   
    <asp:ToolkitScriptManager ID="scriptManager" EnablePartialRendering="true" EnableScriptGlobalization="true" EnableScriptLocalization="true" runat="server">
    </asp:ToolkitScriptManager>
        <asp:UpdatePanel ID="upHome" runat="server">
        <ContentTemplate>  
            <table align="center">
                <tr>
                    <td align="center">
                        <table>
                            <tr>
                                <td align="center">
                                    <asp:TextBox ID="txtSearchOutlet" runat="server" Width="200px" CssClass="ntextblackdata"></asp:TextBox>&nbsp;&nbsp;
                                    <asp:Button ID="btnSearchOutlet" runat="server" Width="80px" 
                                        CssClass="ntextblack" Text="Search" onclick="btnSearchOutlet_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                         <asp:Button ID="btnSelect" runat="server" Text="Select" Width="80px" CssClass="ntextblack" onclick="btnSelect_Click" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:DataGrid ID="dgOutlet" Width="900px" HeaderStyle-CssClass="gridheaderrowstyle"
                            runat="server" AutoGenerateColumns="false" ItemStyle-CssClass="gridrowstyle" 
                            AlternatingItemStyle-CssClass="gridalternativerowstyle">
                            <Columns>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkHeaderOutlet" runat="server" AutoPostBack="true" OnCheckedChanged="chkHeaderOutlet_OnCheckedChanged"/>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkOutlet" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="name" HeaderText="Outlet Name"/>
                                <asp:BoundColumn DataField="outletId" Visible="false"></asp:BoundColumn>                                                          
                            </Columns>            
                        </asp:DataGrid>
                    </td>
                </tr>        
            </table>
         </ContentTemplate>
         <Triggers>
         </Triggers>
        </asp:UpdatePanel>
    </form>
</body>
</html>
