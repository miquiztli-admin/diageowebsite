﻿<%@ page language="C#" autoeventwireup="true" inherits="Master_OutboundEvent, App_Web_outboundevent.aspx.28963a75" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Event</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <base target="_self" />
    <style type="text/css">
        .style1
        {
            text-align: center;
        }
    </style>
</head>
<body style="background-color: Silver">
    <form id="FrmUser" runat="server">
     <asp:ToolkitScriptManager ID="scriptManager" EnablePartialRendering="true" EnableScriptGlobalization="true" EnableScriptLocalization="true" runat="server">
    </asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="upHome" runat="server">
    <ContentTemplate>  
    
   
    
    <div style="background-color: Silver">
        <fieldset>
            <legend class="ntextblack">Event</legend>
            <br />
            
               
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="1" Height="200px">
        <asp:TabPanel runat="server" HeaderText="TabPanel1" ID="TabPanel1">
        </asp:TabPanel>
        <asp:TabPanel ID="TabPanel2" runat="server" HeaderText="TabPanel2">
        </asp:TabPanel>
    </asp:TabContainer>
            
               <table align="center" style="background-color: Silver">
                    <tr>
                        <td align="right">
                            <asp:Label ID="lblEventName" CssClass="ntextblack" runat="server" Text="Event Name : "></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtEventName" ReadOnly="true" Enable="false" CssClass="ntextblackdata"
                                runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="lblStartAge" CssClass="ntextblack" runat="server" Text="StartAge : "></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtStartAge" ReadOnly="true" Enable="false" CssClass="ntextblackdata"
                                runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="lblEndAge" CssClass="ntextblack" runat="server" Text="EndAge : "></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtEndAge" ReadOnly="true" Enable="false" CssClass="ntextblackdata"
                                runat="server" MaxLength="10"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="border-width: thin; border-top-style: solid; border-bottom-style: solid;
                            border-color: Gray" align="left">
                            <asp:Label ID="lblBrand" runat="server" CssClass="ntextblack" Text="Brand Preference"></asp:Label>
                        </td>
                        <td style="border-width: thin; border-top-style: solid; border-bottom-style: solid;
                            border-color: Gray" align="left" colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkBaileys" runat="server" AutoPostBack="True" />&nbsp;
                                        <asp:Label ID="lblBaileys" CssClass="ntextblack" runat="server" Text="Baileys"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkBenmore" runat="server" AutoPostBack="True" />&nbsp;
                                        <asp:Label ID="lblBenmore" CssClass="ntextblack" runat="server" Text="Benmore"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkBenmoreJwBlack" runat="server" AutoPostBack="True" />&nbsp;
                                        <asp:Label ID="lblJwBlack" CssClass="ntextblack" runat="server" Text="JwBlack"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkJwGold" runat="server" AutoPostBack="True" />&nbsp;
                                        <asp:Label ID="lblJwGold" CssClass="ntextblack" runat="server" Text="JwGold"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkJwGreen" runat="server" AutoPostBack="True" />&nbsp;
                                        <asp:Label ID="lblJwGreen" CssClass="ntextblack" runat="server" Text="JwGreen"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkJwRed" runat="server" AutoPostBack="True" />&nbsp;
                                        <asp:Label ID="lblJwRed" CssClass="ntextblack" runat="server" Text="JwRed"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="chkSmirnoff" runat="server" AutoPostBack="True" />&nbsp;
                                        <asp:Label ID="lblSmimoff" CssClass="ntextblack" runat="server" Text="Smimoff"></asp:Label>
                                </tr>
                        </td>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkBallentine" runat="server" AutoPostBack="True" />&nbsp;
                                <asp:Label ID="lblBallantines" CssClass="ntextblack" runat="server" Text="Ballentine"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkBlend" runat="server" AutoPostBack="True" />&nbsp;
                                <asp:Label ID="lblBlend" CssClass="ntextblack" runat="server" Text="Blend"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkChivas" runat="server" AutoPostBack="True" />&nbsp;
                                <asp:Label ID="lblChivas" CssClass="ntextblack" runat="server" Text="Chivas"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkDewar" runat="server" AutoPostBack="True" />&nbsp;
                                <asp:Label ID="lblDewar" CssClass="ntextblack" runat="server" Text="Dewar"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chk100Pipers" runat="server" AutoPostBack="True" />&nbsp;
                                <asp:Label ID="lbl100Piper" CssClass="ntextblack" runat="server" Text="100 Pipers"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chk100Pipers8yrs" runat="server" AutoPostBack="True" />&nbsp;
                                <asp:Label ID="lbl100Pipers8yrs" CssClass="ntextblack" runat="server" Text="100Pipers8yrs"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkAbsolute" runat="server" AutoPostBack="True" />&nbsp;
                                <asp:Label ID="lblAbsolute" CssClass="ntextblack" runat="server" Text="Absolute"></asp:Label>
                            </td>
                        </tr>
                </table>
                </td> </tr>
                <tr>
                    <td style="border-width: thin; border-bottom-style: solid; border-color: Gray" align="left">
                        <asp:Label ID="lblSurvey" runat="server" CssClass="ntextblack" Text="Survey"></asp:Label>
                    </td>
                    <td style="border-width: thin; border-bottom-style: solid; border-color: Gray" colspan="2">
                        <table>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" CssClass="ntextblack" runat="server" Text="Baileys"></asp:Label>
                                    <asp:CheckBoxList ID="chkBaileysList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                        runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label3" CssClass="ntextblack" runat="server" Text="Benmore"></asp:Label>
                                    <asp:CheckBoxList ID="chkFourABenmoreList" RepeatDirection="Horizontal" runat="server"
                                        CssClass="ntextblack" AutoPostBack="True">
                                        <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label4" CssClass="ntextblack" runat="server" Text="JwBlack"></asp:Label>
                                    <asp:CheckBoxList ID="chkJwBlackList" RepeatDirection="Horizontal" runat="server"
                                        CssClass="ntextblack" AutoPostBack="True">
                                        <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label5" CssClass="ntextblack" runat="server" Text="JwGold"></asp:Label>
                                    <asp:CheckBoxList ID="chkJwGoldList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                        runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label6" CssClass="ntextblack" runat="server" Text="JwGreen"></asp:Label>
                                    <asp:CheckBoxList ID="chkJwGreenList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                        runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label7" CssClass="ntextblack" runat="server" Text="JwRed"></asp:Label>
                                    <asp:CheckBoxList ID="chkJwRedList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                        runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label8" CssClass="ntextblack" runat="server" Text="Smirnoff"></asp:Label>
                                    <asp:CheckBoxList ID="chkSmirnoffList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                        runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label10" CssClass="ntextblack" runat="server" Text="Ballentine"></asp:Label>
                                    <asp:CheckBoxList ID="chkBallentineList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                        runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label9" CssClass="ntextblack" runat="server" Text="Blend"></asp:Label>
                                    <asp:CheckBoxList ID="chkBlendList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                        runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label11" CssClass="ntextblack" runat="server" Text="Chivas"></asp:Label>
                                    <asp:CheckBoxList ID="chkChivasList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                        runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label12" CssClass="ntextblack" runat="server" Text="Dewar"></asp:Label>
                                    <asp:CheckBoxList ID="chkDewarList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                        runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label13" CssClass="ntextblack" runat="server" Text="100Pipers"></asp:Label>
                                    <asp:CheckBoxList ID="chk100PipersList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                        runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label14" CssClass="ntextblack" runat="server" Text="100Pipers8yrs"></asp:Label>
                                    <asp:CheckBoxList ID="chk100Pipers8yrsList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                        runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label15" CssClass="ntextblack" runat="server" Text="Absolute"></asp:Label>
                                    <asp:CheckBoxList ID="chkAbsoluteList" CssClass="ntextblack" RepeatDirection="Horizontal"
                                        runat="server" AutoPostBack="True">
                                        <asp:ListItem Text="Adorers" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Adopters" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Acceptors" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Availables" Value="4"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lbliActive" CssClass="ntextblack" runat="server" Text="Active : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:RadioButtonList ID="rdoActive" CssClass="ntextblackdata" runat="server" RepeatDirection="Horizontal"
                            TabIndex="1">
                            <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="btnSave" CssClass="ntextblack" runat="server" Text="บันทึก" OnClick="btnSave_Click"
                            TabIndex="2" />&nbsp;
                        <asp:Button ID="btnCancel" CssClass="ntextblack" runat="server" Text="ยกเลิก" OnClick="btnCancel_Click"
                            TabIndex="3" />
                    </td>
                </tr>
                </table>
        </fieldset>
    </div>
    
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
