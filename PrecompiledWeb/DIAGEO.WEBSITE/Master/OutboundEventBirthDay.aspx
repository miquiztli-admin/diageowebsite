﻿<%@ page language="C#" autoeventwireup="true" inherits="Master_OutboundEventBirthDay, App_Web_outboundeventbirthday.aspx.28963a75" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Birthday Condition</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../Style/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="../Style/block.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery.blockUI.js" type="text/javascript"></script>
    <base target="_self"/>
    <style type="text/css">
        .ntab
        {
        	border-color:Black;
            background-color:#2890C7;
            color:White;
            font:bold 13px Arial, Helvetica, sans-serif;
        }
        .ntextred
        {
            color:Red;
            font:bold 13px Arial, Helvetica, sans-serif;
        }
        .nbtn1
        {
        	border-color:Black;
            color:Green;
            font:bold 18px Arial, Helvetica, sans-serif;
        }
        .nbtn2
        {
        	border-color:Black;
            color:Red;
            font:bold 18px Arial, Helvetica, sans-serif;
        }
    </style>
    <style type="text/css">
        .style1
        {
            height: 29px;
        }
    </style>
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    <script type="text/javascript">  
      function  blockUI()
      {
        $.blockUI({message: '<h1><img src="../Image/Load.gif" /></h1>'});
      }
    </script>
     
    <script type="text/javascript">  
      function  unblockUI()
      {
        $.unblockUI();
      }
    </script>
    </head>
<body>
    <form id="FrmUser" runat="server">
    <div>
        <fieldset>
        <legend class="ntextblack">Birthday Condition</legend>
        <br />
            <table align="center">
                <tr>
                    <td align="right">
                        <asp:Label ID="lblEventName" CssClass="ntextblack" runat="server" Text="Birthday Condition Name : "></asp:Label>
                    </td>
                    <td align="left" colspan="2">
                        <asp:TextBox ID="txtEventName" ReadOnly="true" Enable="false" CssClass="ntextblackdata"
                            runat="server" MaxLength="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="width: 200px">
                        <asp:Label ID="lblPlatform1" CssClass="ntextblack" runat="server" Text="Platform :"></asp:Label>
                    </td>
                    <td align="left" style="width: 200px">
                        <asp:Button ID="btnChoosePlatform" CssClass="ntextblack" runat="server" Text="Choose Platform"
                            OnClick="btnChoosePlatform_Click" Width="150px" />
                    </td>
                    <td style="width: 400px">
                        <asp:Label ID="lblTextPlatform" runat="server" CssClass="ntextblack" Style="vertical-align: middle"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblOutlet1" CssClass="ntextblack" runat="server" Text="Register Outlet :"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="btnChooseOutletRegister" CssClass="ntextblack" runat="server" Text="Choose Outlet"
                            OnClick="btnChooseOutletRegister_Click" Width="150px"/>
                    </td>
                    <td style="width: 400px">
                        <asp:Label ID="lblTextOutlet" runat="server" CssClass="ntextblack" Style="vertical-align: middle"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label1" CssClass="ntextblack" runat="server" Text="Brand Preference : "></asp:Label>
                    </td>
                    <td align="left" colspan="2">
                        <table align="left">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chbBaileys" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label8" CssClass="ntextblack" runat="server" Text="Baileys "></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chbBenmore" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label2" CssClass="ntextblack" runat="server" Text="Benmore "></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chbJwBlack" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label3" CssClass="ntextblack" runat="server" Text="JwBlack "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chbJwGold" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label4" CssClass="ntextblack" runat="server" Text="JwGold "></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chbJwGreen" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label5" CssClass="ntextblack" runat="server" Text="JwGreen "></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chbJwRed" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label6" CssClass="ntextblack" runat="server" Text="JwRed "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chbSmirnoff" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label7" CssClass="ntextblack" runat="server" Text="Smirnoff "></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chbAbsolut" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label9" CssClass="ntextblack" runat="server" Text="Absolut "></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chbBallentine" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label10" CssClass="ntextblack" runat="server" Text="Ballentine "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chbBlend285" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label11" CssClass="ntextblack" runat="server" Text="Blend 285 "></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chbChivas" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label12" CssClass="ntextblack" runat="server" Text="Chivas "></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chbDewar" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label13" CssClass="ntextblack" runat="server" Text="Dewar "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chb100Pipers" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label14" CssClass="ntextblack" runat="server" Text="100 Pipers "></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chb100Pipers8Y" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label15" CssClass="ntextblack" runat="server" Text="100 Pipers8Y "></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chbHennessy" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label16" CssClass="ntextblack" runat="server" Text="Hennessy "></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chbOther" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label17" CssClass="ntextblack" runat="server" Text="Other "></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chbNotDrink" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label18" CssClass="ntextblack" runat="server" Text="Not Drink "></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lblGender" runat="server" Text="Gender :" CssClass="ntextblack"></asp:Label>
                    </td>
                    <td colspan="2">
                        <asp:RadioButtonList ID="rdoGender" runat="server" CssClass="ntextblack" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Male" Value="MALE" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Female" Value="FEMALE"></asp:ListItem>
                            <asp:ListItem Text="All" Value=""></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label19" CssClass="ntextblack" runat="server" Text="Valid Opt-in : "></asp:Label>
                    </td>
                    <td align="left" colspan="2">
                        <table align="left">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chbDiageoOptIn" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="lblDiageoOptIn" CssClass="ntextblack" runat="server" Text=" Diageo Opt-in "></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chbOutletOptIn" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="lblOutletOptIn" CssClass="ntextblack" runat="server" Text=" Outlet Opt-in "></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label24" CssClass="ntextblack" runat="server" Text="Age : "></asp:Label>
                    </td>
                    <td colspan="2">
                        <asp:Label ID="Label22" CssClass="ntextblack" runat="server" Text="From : "></asp:Label>&nbsp;
                        <asp:TextBox ID="txtStartAge" ReadOnly="true" Enable="false" 
                            CssClass="ntextblackdata" runat="server" MaxLength="3" 
                            OnKeyPress="validateNumKey()"></asp:TextBox>&nbsp;
                        <asp:Label ID="Label23" CssClass="ntextblack" runat="server" Text="To : "></asp:Label>&nbsp;
                        <asp:TextBox ID="txtEndAge" ReadOnly="true" Enable="false" 
                            CssClass="ntextblackdata" runat="server" MaxLength="3" 
                            OnKeyPress="validateNumKey()"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                <td align="right">
                    <asp:Label ID="Label33" CssClass="ntextblack" runat="server" Text="Province : "></asp:Label>
                </td>
                <td>
                    <asp:Button ID="btnChooseProvince" CssClass="ntextblack" runat="server" Text="Choose Province"
                        OnClick="btnChooseProvince_Click" Width="150px"/>
                </td>
                <td style="width: 400px">
                    <asp:Label ID="lblTextProvice" runat="server" CssClass="ntextblack" Style="vertical-align: middle"></asp:Label>
                </td>
            </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label26" CssClass="ntextblack" runat="server" Text="Email Active : "></asp:Label>
                    </td>
                    <td align="left" colspan="2">
                        <asp:RadioButtonList ID="rdoEmailActive" CssClass="ntextblackdata" runat="server" RepeatDirection="Horizontal"
                            TabIndex="1">
                            <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label27" CssClass="ntextblack" runat="server" Text="Mobile Active : "></asp:Label>
                    </td>
                    <td align="left" colspan="2">
                        <asp:RadioButtonList ID="rdoMobileActive" CssClass="ntextblackdata" runat="server" RepeatDirection="Horizontal"
                            TabIndex="1">
                            <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label28" CssClass="ntextblack" runat="server" Text="Include Bounce : "></asp:Label>
                    </td>
                    <td align="left" colspan="2">
                        <table align="left">
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chbHardbounceSMS" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label29" CssClass="ntextblack" runat="server" Text=" Hardbounce SMS "></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chbHardbounceCallCenter" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label30" CssClass="ntextblack" runat="server" Text="Hardbounce Call Center "></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="chbHardbounceEDM" CssClass="ntextblackdata" runat="server" />
                                    <asp:Label ID="Label31" CssClass="ntextblack" runat="server" Text="Hardbounce EDM "></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="Label32" CssClass="ntextblack" runat="server" Text="Exclude Block List : "></asp:Label>
                    </td>
                    <td align="left" colspan="2">
                        <asp:CheckBox ID="chbYes" CssClass="ntextblackdata" runat="server" />
                        <asp:Label ID="Label35" CssClass="ntextblack" runat="server" Text="Yes "></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Label ID="lbliActive" CssClass="ntextblack" runat="server" Text="Active : "></asp:Label>
                    </td>
                    <td align="left" colspan="2">
                        <asp:RadioButtonList ID="rdoActive" CssClass="ntextblackdata" runat="server" RepeatDirection="Horizontal"
                            TabIndex="1">
                            <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="display: none" align="center" class="style1">
                        <asp:Button ID="btnRefresh" runat="server" CssClass="ntextblack" OnClick="btnRefresh_Click" />
                    </td>
                </tr>
            </table>
            </fieldset>
            <table align="center" style="width: 300px">
                <tr>
                    <td colspan="3" align="center">
                        <asp:Button ID="btnSave" CssClass="ntab" runat="server" Text="SAVE" OnClientClick="blockUI();" OnClick="btnSave_Click"
                            TabIndex="2" Width="120px" />&nbsp;
                        <asp:Button ID="btnCancel" CssClass="ntab" runat="server" Text="CANCEL" OnClick="btnCancel_Click"
                            TabIndex="3" Width="120px" />
                    </td>
                </tr>
            </table>
        <br />
    </div>
    </form>
</body>
</html>
