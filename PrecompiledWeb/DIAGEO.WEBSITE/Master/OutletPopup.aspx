﻿<%@ page language="C#" autoeventwireup="true" inherits="Master_OutletPopup, App_Web_outletpopup.aspx.28963a75" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Outlet</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="FrmOutlet" runat="server">   
    <asp:ToolkitScriptManager ID="scriptManager" EnablePartialRendering="true" EnableScriptGlobalization="true" EnableScriptLocalization="true" runat="server">
    </asp:ToolkitScriptManager>
    <asp:UpdatePanel ID="upHome" runat="server">
    <ContentTemplate>  
    <table align="center">
        <tr><td align="right">
             <asp:Button ID="btnSelect" runat="server" Text="Select" onclick="btnSelect_Click" />
        </td></tr>
        <tr><td align="center">
            <asp:GridView ID="gvOutlet" DataKeyNames="code" Width="900px" HeaderStyle-CssClass="gridheaderrowstyle"
            runat="server" AutoGenerateColumns="false" ItemStyle-CssClass="gridrowstyle" AlternatingItemStyle-CssClass="gridalternativerowstyle">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox runat="server" ID="HeaderLevelCheckBox" AutoPostBack="True" 
                                oncheckedchanged="HeaderLevelCheckBox_CheckedChanged" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkOutlet" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="name" HeaderText="Name" ItemStyle-HorizontalAlign="Left"/>                                                           
                </Columns>            
                <HeaderStyle CssClass="gridheaderrowstyle" />
            </asp:GridView>
        </td></tr>        
    </table>
     </ContentTemplate><Triggers></Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
