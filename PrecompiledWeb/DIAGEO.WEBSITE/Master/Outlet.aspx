﻿<%@ page language="C#" autoeventwireup="true" inherits="Master_Outlet, App_Web_outlet.aspx.28963a75" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Outlet</title>
    <link href="../Style/StyleSheet.css" rel="stylesheet" type="text/css" />
    <script src="../Jquery/jquery-1.4.4.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
    <script src="../Jquery/jquery.blockUI.js" type="text/javascript"></script>
    <script src="../Jquery/js_function.js" type="text/javascript"></script>
    <base target="_self"/>
</head>
<body style="background-color:Silver">
    <form id="FrmUser" runat="server">
    <div style="background-color:Silver">
        <fieldset>
        <legend class="ntextblack">Outlet</legend>
        <br />
        <table align="center" style="background-color:Silver">
            <tr>
                <td align="right"><asp:Label ID="lblOutletCode" CssClass="ntextblack" runat="server" Text="Outlet Code : "></asp:Label></td>
                <td align="left"><asp:TextBox ID="txtOutletCode" ReadOnly="true" Enable="false" 
                        CssClass="ntextblackdata" runat="server" MaxLength="10"></asp:TextBox></td>                
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lblOutletName" CssClass="ntextblack" runat="server" Text="Outlet Name : "></asp:Label></td>
                <td align="left">
                    <asp:TextBox ID="txtOutletName" ReadOnly="true" Enable="false" 
                        CssClass="ntextblackdata" runat="server" Width="250px" MaxLength="50"></asp:TextBox></td>                
            </tr>
            <tr>
                    <td align="right">
                        <asp:Label ID="lblZone" CssClass="ntextblack" runat="server" Text="Zone : "></asp:Label>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlZone" runat="server" Width="150px">
                        </asp:DropDownList>
                    </td>
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lblAddress" CssClass="ntextblack" runat="server" Text="Address : "></asp:Label></td>
                <td align="left"><asp:TextBox ID="txtAddress" ReadOnly="true" Enable="false" 
                        CssClass="ntextblackdata" runat="server" MaxLength="100" 
                        TextMode="MultiLine" Width="250px"></asp:TextBox></td>                
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lblTel" CssClass="ntextblack" runat="server" Text="Telephone : "></asp:Label></td>
                <td align="left"><asp:TextBox ID="txtTel" ReadOnly="true" Enable="false" 
                        CssClass="ntextblackdata" OnKeyPress="validateNumKey()" runat="server" MaxLength="10"></asp:TextBox></td>                
            </tr>
            <tr>
                <td align="right"><asp:Label ID="lbliActive" CssClass="ntextblack" runat="server" Text="Active : "></asp:Label></td>
                <td align="left">
                    <asp:RadioButtonList ID="rdoActive" CssClass="ntextblackdata" runat="server" 
                        RepeatDirection="Horizontal" TabIndex="1">
                        <asp:ListItem Text="Active" Value="T" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Inactive" Value="F"></asp:ListItem>                        
                    </asp:RadioButtonList>
                </td>                
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnSave" CssClass="ntextblack" runat="server" Text="บันทึก" 
                        onclick="btnSave_Click" TabIndex="2" />&nbsp;
                    <asp:Button ID="btnCancel" CssClass="ntextblack" runat="server" Text="ยกเลิก" 
                        onclick="btnCancel_Click" TabIndex="3" />                    
                </td>
            </tr>          
        </table>
        <br />
        </fieldset>
    </div>
    </form>
</body>
</html>
