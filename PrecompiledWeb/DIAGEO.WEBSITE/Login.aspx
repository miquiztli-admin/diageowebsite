﻿<%@ page language="C#" autoeventwireup="true" inherits="Security_Login, App_Web_login.aspx.cdcab7d2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <link href="Style/StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="FrmLogin" runat="server">
    <div>
        <br />
        <table align="center" height=80px
            style="border-bottom-style:none; background-color:Black; width:1024px; ">
            <tr align=center>
                <td>&nbsp;<asp:Label ID="lblDIAGEO" runat="server" ForeColor="White" 
                        Font-Bold="True" Font-Size="XX-Large" Text="DIAGEO HIPKINGDOM ONTRADE" 
                        Font-Names="Century Gothic" ></asp:Label>&nbsp;</td>
            </tr>
        </table>
        <div></div>
        <table align="center" style ="width:1024px; ">
            <tr>
                <td align=center style="width:300px"><img src="Image/Diageo.png"   /></td>
                <td style="width:600px">
                    <table width="400px" style="border: 1px solid Black; border-spacing:1px;  margin-left: 0px;">
                    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                    <tr>
                        <td align="CENTER"><asp:Label ID="lblUserName" runat="server" Text="USERNAME:" 
                                Font-Names="Century Gothic"></asp:Label></td>
                        <td align="left"><asp:TextBox ID="txtUserName" MaxLength="100" CssClass="ntextblack" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="CENTER"><asp:Label ID="lblPassword"  runat="server" Text="PASSWORD:" 
                                Font-Names="Century Gothic"></asp:Label></td>
                        <td align="left"><asp:TextBox ID="txtPassword" MaxLength="100" 
                                CssClass="ntextblack" runat="server" TextMode="Password"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="RIGHT">
                            &nbsp;
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" 
                                onclick="btnCancel_Click" Visible="False" />
                            <asp:Button ID="btnLogin" runat="server" Text="LOGIN" onclick="btnLogin_Click" 
                                BackColor="Black" BorderColor="Black" Font-Names="Century Gothic" 
                                ForeColor="White" BorderStyle="None" Font-Size="Medium" />
                                &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <div align="right">
                                <asp:Label ID="lblVersion" CssClass="ntextwhite" runat="server" color="gray" align="right" Text="version 1.0.0.0"></asp:Label>
                            </div>
                        </td>
                    </tr>                
                </table></td>
            </tr>
 

        </table>
    </div>
    </form>
</body>
</html>
