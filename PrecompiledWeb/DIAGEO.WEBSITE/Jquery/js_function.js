﻿function popupFullscreen(url) 
{
 params  = 'width='+screen.width;
 params += ', height='+screen.height;
 params += ', top=0, left=0'
 params += ', fullscreen=yes';

 newwin=window.open(url,'windowname4', params);
 if (window.focus) {newwin.focus()}
 return false;
}

function us_Adjustiframe() {
    //parent.AdjustiFrame(document.body.clientHeight + 40);
}
function us_validateEngChar(isUppperCase) {
    //alert(event.keyCode);
    var inputKey = event.keyCode;
    var returnCode = true;
    if ((inputKey > 64 && inputKey < 91) || (inputKey > 96 && inputKey < 123)) {
        if (isUppperCase) {
            var val = String.fromCharCode(inputKey);
            val = val.toUpperCase();
            event.keyCode = val.charCodeAt(0);
        }

        return;
    }
    else {
        returnCode = false;
        event.keyCode = 0;
    }
    event.returnValue = returnCode;
}

function us_validateThaiChar() {
    //var code = event.keyCode;
    //var character = String.fromCharCode(code);
    //alert(event.keyCode);
    var inputKey = event.keyCode;
    var returnCode = true;
    if (inputKey > 3584 && inputKey < 3662 || inputKey == 32)
        return;
    else {
        returnCode = false;
        event.keyCode = 0;
    }
    event.returnValue = returnCode;
}
function us_ToUpper(ctrl) {
    if (ctrl != null)
        ctrl.value = ctrl.value.toUpperCase();
}
function us_ToTHDate(ctrl) {
    if (ctrl != null) {
        var dateSplit = ctrl.value.split('/');
        var strYear = "";
        var strTHYear = "";
        for (var i = 0; i < dateSplit.length; i++) {
            if (dateSplit[i].length = 4) {
                strYear = dateSplit[i];
                var year = parseInt(dateSplit[i]);
                // maximum
                strTHYear = strYear
                if (year < 2355) {
                    year = year + 543;
                    strTHYear = year.toString();
                }
            }
        }
        ctrl.value = ctrl.value.replace(strYear, strTHYear);
    }
}

function us_ToENDate(ctrl) {
    if (ctrl != null) {
        var dateSplit = ctrl.value.split('/');
        var strYear = "";
        var strTHYear = "";
        for (var i = 0; i < dateSplit.length; i++) {
            if (dateSplit[i].length = 4) {
                strYear = dateSplit[i];
                var year = parseInt(dateSplit[i]);
                // maximum
                strTHYear = strYear
                if (year > 2355) {
                    year = year - 543;
                    strTHYear = year.toString();
                }
            }
        }
        ctrl.value = ctrl.value.replace(strYear, strTHYear);
    }
}

function us_validateEngChar(isUppperCase) {
    //alert(event.keyCode);
    var inputKey = event.keyCode;
    var returnCode = true;
    if ((inputKey > 64 && inputKey < 91) || (inputKey > 96 && inputKey < 123)) {
        if (isUppperCase) {
            var val = String.fromCharCode(inputKey);
            val = val.toUpperCase();
            event.keyCode = val.charCodeAt(0);
        }

        return;
    }
    else {
        returnCode = false;
        event.keyCode = 0;
    }
    event.returnValue = returnCode;
}

function us_validateThaiChar() {
    //var code = event.keyCode;
    //var character = String.fromCharCode(code);
    //alert(event.keyCode);
    var inputKey = event.keyCode;
    var returnCode = true;
    if (inputKey > 3584 && inputKey < 3662 || inputKey == 32)
        return;
    else {
        returnCode = false;
        event.keyCode = 0;
    }
    event.returnValue = returnCode;
}


function ToUpper(ctrl) {
    var inputKey = event.keyCode;    
    if ((inputKey > 64 && inputKey < 91) || (inputKey > 96 && inputKey < 123)) {           
         var val = String.fromCharCode(inputKey);
         val = val.toUpperCase();
         event.keyCode = val.charCodeAt(0);
    }
    var text = ctrl.value;   
    ctrl.value = text.trim();
    
}
function ToLower(ctrl) {

    var text = ctrl.value;

    ctrl.value = text.toLowerCase();

}
//Ex ; <input type="text" name="curr_idname" size="5" maxlength="4" 
//    onkeypress="return numeralsOnly(event)" onkeyup="autofocus(this, 4, 'next_idname', event)">&nbsp;&nbsp;


function autofocus(field, limit, next, evt) {

    evt = (evt) ? evt : event;
    var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode : 
        ((evt.which) ? evt.which : 0));
               
     // focus on limit or user enter
    if (charCode == '13' || field.value.length == limit) {

        next.focus();
    }
}



function setbg(element , color)
{
    element.style.background=color;
}

function setfocuseffect(element)
{
    element.style.border = "1px solid #FF0099";
    element.select();
}

function setfocus(ctrl) {
    try 
    {
        if(ctrl != null)
            ctrl.focus();
    }
    catch(er){
    
    }
}

function setnofocuseffect(element)
{
    element.style.border = "1px solid #999999";
           
}

function changestyle(element , classname )
{
    element.className = classname;
}

function openPrint(theURL) 
{
    var printPage = null;
    window.opener = self;
    printPage = window.open(theURL,'w_print', 'resizable=no, scrollbars=yes , left=0,top=0 ,HEIGHT=320,WIDTH=300 ');
    printPage.focus();
    //window.showModalDialog(theURL, 'w_print' ,"toolbar=0,titlebar=0,status=1,menubar=0,scrollbars=1,resizable=1,dialogWidth:300px,dialogHeight:320px,top=0,left=0");
}

var popupObj = null;
function openPrintQueue(theURL) 
{
    //var printPage = null;
    window.opener = self;
    popupObj = window.open(theURL,'w_print', 'resizable=no, scrollbars=yes , left=0,top=0 ,HEIGHT=320,WIDTH=300 ');
    popupObj.focus();
    //window.showModalDialog(theURL, 'w_print' ,"toolbar=0,titlebar=0,status=1,menubar=0,scrollbars=1,resizable=1,dialogWidth:300px,dialogHeight:320px,top=0,left=0");
}

function windowPopup(Name,LinkUrl,width,height)
{
    
	var leftPosition = (screen.width - width) / 2 - 4;
	var topPosition =  (screen.height - height) / 2 - 22;
	window.open(LinkUrl,Name,"toolbar=0,titlebar=0,status=1,menubar=0,scrollbars=1,resizable=1,width=" + width + ",height=" + height + ",top=" + topPosition + ",left=" + leftPosition);
}

function reportPopup(Name,LinkUrl)
{
    var width = 800;
    var height = 600;
	var leftPosition = (screen.width - width) / 2 - 4;
	var topPosition =  (screen.height - height) / 2 - 22;
	window.open(LinkUrl,Name,"toolbar=0,titlebar=0,status=1,menubar=0,scrollbars=1,resizable=1,width=" + width + ",height=" + height + ",top=" + topPosition + ",left=" + leftPosition);
}

function windowPopupDialog(Name,LinkUrl,width,height)
{
	var leftPosition = (screen.width - width) / 2 - 4;
	var topPosition =  (screen.height - height) / 2 - 22;
	window.showModalDialog(LinkUrl,Name,"toolbar=0,titlebar=0,status=1,menubar=0,scrollbars=1,resizable=1,dialogWidth:" + width + "px,dialogHeight:" + height + "px,top=" + topPosition + ",left=" + leftPosition);
	//window.showModalDialog(LinkUrl,Name,"scrollbars=1,resizable=1,dialogWidth:"+360+"px;dialogHeight:"+380+"px");
}
function IsEmpty(szStr)
    {
        for(var i=0; i<szStr.length; i++)
        {
	        if(szStr.charAt(i) != ' ')
		        return false;
        }
        return true;
    }
function validateNumKeyAndDot () {
    
	var inputKey =  event.keyCode;
	var returnCode = true;
	
	if ( inputKey > 47 && inputKey < 58 ) // numbers
		return;
	else if (inputKey > 44 && inputKey < 47) // dot & neg sign
		return;
	else if (inputKey == 13)
	    return;
	else
	{
		returnCode = false;
		event.keyCode = 0;
	}
	event.returnValue = returnCode;
}

function validateDateTimeFormat() {
    var inputKey = event.keyCode;
    var returnCode = true;

    if (inputKey > 46 && inputKey < 58) // numbers
        return;
    else {
        returnCode = false;
        event.keyCode = 0;
    }
    event.returnValue = returnCode;
}

function validateNumKeyAndDotNonNegative ()
{
	var inputKey =  event.keyCode;
	var returnCode = true;
	
	if ( inputKey > 47 && inputKey < 58 ) // numbers
		return;
	else if (inputKey == 46) // dot & neg sign
		return;
	else if (inputKey == 13)
	    return;
	else
	{
		returnCode = false;
		event.keyCode = 0;
	}
	event.returnValue = returnCode;
}



function validateNegativeNumKey ()
{	
	var inputKey =  event.keyCode;
	var returnCode = true;
	if (inputKey > 47 && inputKey < 58) // numbers
		return;
	else
	{
		returnCode = false;
		event.keyCode = 0;
	}
	event.returnValue = returnCode;
}


function validateNumKey ()
{	
	var inputKey =  event.keyCode;
	var returnCode = true;
	if ((inputKey > 47 && inputKey < 58)||inputKey == 45) // numbers
		return;
	else
	{
		returnCode = false;
		event.keyCode = 0;
	}
	event.returnValue = returnCode;
}
function AddCommaAndDotFor7Digit(Control)
	{
		if(IsEmpty(Control.value))
			Control.value = "";
		else
			{
				var str1
				str1 = Control.value.split('.');
				if (str1.length == '1')
						Control.value =  str1 + '.0000000';
				Control.value = RoundNum7Digit(Control.value);	
			}
	}
function RoundNum7Digit(num)
  {
	if (num.split('.').length < 2)
	{
		return num;
	}
	else
	{
		num = MoneyToNumber(num);
		var fullNum, dotNum;
		fullNum = num.split('.')[0];
		dotNum = num.split('.')[1];
		if (dotNum.length < 8)
		{
			return fullNum + '.' + dotNum;
		}
		else
		{
			var firstNum,secondNum,thirdNum;
			firstNum = dotNum.substr(0,1);
			secondNum = dotNum.substr(1,1);
			thirdNum = dotNum.substr(2,1);
			fourNum = dotNum.substr(3,1);
			fiveNum = dotNum.substr(4,1);
			sixNum = dotNum.substr(5,1);
			sevenNum = dotNum.substr(6,1);
			eightNum = dotNum.substr(7,1);
			if (parseInt(eightNum) < 5)
			{
				return fullNum + '.' + firstNum.toString() + secondNum.toString() + thirdNum.toString() + fourNum.toString() + fiveNum.toString() + sixNum.toString() + sevenNum.toString();
			}
			else
			{
				if (parseInt(sevenNum) == 9)
				{
					if (parseInt(sixNum) == 9)
					{
						if (parseInt(fiveNum) == 9)
					    {
					        if (parseInt(fourNum) == 9)
					        {
    					        if (parseInt(thirdNum) == 9)
					            {
        					        if (parseInt(secondNum) == 9)
				                    {
            					        if (parseInt(firstNum) == 9)
			                            {
				                            fullNum = parseFloat(fullNum) + 1;
				                            return fullNum + '.' + '0000000';
			                            }
			                            else
			                            {
				                            firstNum = parseInt(firstNum) + 1;
				                            return fullNum + '.' + firstNum.toString() + '000000';
			                            }
					                }
					                else
					                {
					                    secondNum = parseInt(secondNum) + 1;
					                    return fullNum + '.' + firstNum.toString() + secondNum.toString() + '00000';
					                } 
						        }
						        else
						        {
						            thirdNum = parseInt(thirdNum) + 1;
						            return fullNum + '.' + firstNum.toString() + secondNum.toString() + thirdNum.toString() + '0000';
						        }
						    }
						    else
						    {
						        fourNum = parseInt(fourNum) + 1;
						        return fullNum + '.' + firstNum.toString() + secondNum.toString() + thirdNum.toString() + fourNum.toString() + '000';
						    }
						}
						else
						{
						    fiveNum = parseInt(fiveNum) + 1;
						    return fullNum + '.' + firstNum.toString() + secondNum.toString() + thirdNum.toString() + fourNum.toString() + fiveNum.toString() + '00';
						}
					}
					else
					{
						sixNum = parseInt(sixNum) + 1;
						return fullNum + '.' + firstNum.toString() + secondNum.toString() + thirdNum.toString() + fourNum.toString() + fiveNum.toString() + sixNum.toString() + '0';
					}
				}
				else
				{
					sevenNum = parseInt(sevenNum) + 1;
					return fullNum + '.' + firstNum.toString() + secondNum.toString() + thirdNum.toString() + fourNum.toString() + fiveNum.toString() + sixNum.toString() + sevenNum.toString();
				}
			}
		}
	}
  }
function AddCommaAndDot(Control) {
    
		if(IsEmpty(Control.value))
			Control.value = '0.00';
		else
			{
				var str1
				str1 = Control.value.split('.');
				if (str1.length == '1')
						Control.value =  str1 + '.00';
				Control.value = RoundNum(Control.value);	
			}
	}
	function AddCommaAndDotInAmount(Control) {
	    
	    if(!IsEmpty(Control.value)){
			var CCY,Amt
			CCY = Control.value.substr(0,3);
			Amt = Control.value.substr(3,(Control.value.length-3));
			
			var Unexcept = /[^A-Za-z ]/
            
	        if (CCY.search(Unexcept) != -1)
	        {
		        //alert("CCY ='"+CCY+"'" + " String is not A-Z")
		        Control.value = "";
		        alert('Invalid Format');
		        Control.focus(); 
	        }
	        else 
	        {
		        //alert("CCY ='"+CCY+"'" + " String contain A-Z");
		        var Unexcept = /[^0-9,. ]/
                if(!IsEmpty(Amt)){
                    if(Amt.substr(0,1) == '.' || Amt.substr(0,1) == ','){
                        alert('Invalid Number');
                        Control.value = "";
                        Control.focus();
                        return;
                    }
                    if(Amt.indexOf('.') != Amt.lastIndexOf('.')){
                        alert('Invalid Number');
                        Control.value = "";
                        Control.focus();
                        return;
                    }
	                if (Amt.search(Unexcept) != -1)
	                {
		                //alert("Amt ='"+Amt+"'" + " String is not 0-9, .")
		                alert('Invalid Number');
		                Control.value = "";
		                Control.focus();
		                return;
	                }
	                else 
	                {
		                //alert("Amt ='"+Amt+"'" + " String contain 0-9 .");
		                //add comma and dot
		                var  str = Amt.replace(',','');
				        str = Amt.split('.');
				        if(str.length == 1){
				            Amt = str + ".00";
				        }
				        if (str.length == 2)
				        {
				            if(str[1].length == 0){
				                Amt = str[0] + ".00";
				            }   
				            else if(str[1].length == 1){
				                Amt = str[0] + "." + str[1] + "0";
				            }
				            else{
				                Amt = str[0] + "." + str[1];
				            }
				        }
				        Amt = RoundNum(Amt);
	                }
	            }
	            else
	            {
	                //alert('Amt is empty');
	                Amt = '0.00';
	            }
	            
	            Control.value = CCY.toUpperCase() + Amt;
	        }	        
		}
	}
 function RoundNum(num)
  {
	if (num.split('.').length < 2)
	{
		return num;
	}
	else
	{
		num = MoneyToNumber(num);
		var fullNum, dotNum;
		fullNum = num.split('.')[0];
		dotNum = num.split('.')[1];
		if (dotNum.length < 3)
		{
			return AddCommas(fullNum) + '.' + dotNum;
		}
		else
		{
			var firstNum,secondNum,thirdNum;
			firstNum = dotNum.substr(0,1);
			secondNum = dotNum.substr(1,1);
			thirdNum = dotNum.substr(2,1);
			if (parseInt(thirdNum) < 5)
			{
				return AddCommas(fullNum) + '.' + firstNum.toString() + secondNum.toString();
			}
			else
			{
				if (parseInt(secondNum) == 9)
				{
					if (parseInt(firstNum) == 9)
					{
						fullNum = parseFloat(fullNum) + 1;
						return AddCommas(fullNum) + '.' + '00';
					}
					else
					{
						firstNum = parseInt(firstNum) + 1;
						return AddCommas(fullNum) + '.' + firstNum.toString() + '0';
					}
				}
				else
				{
					secondNum = parseInt(secondNum) + 1;
					return AddCommas(fullNum) + '.' + firstNum.toString() + secondNum.toString();
				}
			}
		}
	}
  }
function MoneyToNumber(num)
{
    return (num.replace(/,/g, ''));

}
function AddCommas(num)
{
	//num = num * 1;
    numArr=new String(num).split('').reverse();
    for (i=3;i<numArr.length;i+=3)
    {
         numArr[i]+=',';
    }
    return numArr.reverse().join('');
}

function CurrencyFormatted(amount)
{
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt((i + .005) * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}

function PercentFormatted(amount) {

    var i = parseFloat(amount);
    if (isNaN(i)) { i = 0.00; }
    var minus = '';
    if (i < 0) { minus = '-'; }
    i = Math.abs(i);
    i = parseInt((i + .005) * 100);
    i = i / 100;
    s = new String(i);
    if (s.indexOf('.') < 0) { s += '.00'; }
    if (s.indexOf('.') == (s.length - 2)) { s += '0'; }
    s = minus + s;
    var j = parseFloat(s);
    if (j > 100.00) { s = '100.00'; }
    return s;
}

function CommaFormatted(amount) // support minus -
{
	var delimiter = ","; // replace comma if desired
	var a = amount.split('.',2)
	var d = a[1];
	var i = parseInt(a[0]);
	if(isNaN(i)) { return ''; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	var n = new String(i);
	var a = [];
	while(n.length > 3)
	{
		var nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
	}
	if(n.length > 0) { a.unshift(n); }
	n = a.join(delimiter);
	if(d.length < 1) { amount = n; }
	else { amount = n + '.' + d; }
	amount = minus + amount;
	return amount;
}
// end of function CommaFormatted()


function CheckDate(Control)
{
    var year,month,day;
    var leap = 0;
    if(!IsEmpty(Control.value))
    {
      if(Control.value.length == 10)
      { 
         year = Control.value.substring(6,10);
         if(isNaN(parseFloat(year)))
            {
               alert('Date is incorrect!!');
               Control.value = '';	
		       Control.focus(); 
		       return false;
            }
         if ((parseFloat(year) < parseFloat(1900)) || (parseFloat(year) > parseFloat(2099)))
            {
                alert('Date is incorrect!!');
                Control.value = '';
		        Control.focus(); 
		        return false;
            }
          month = Control.value.substring(3,5);
          if(isNaN(parseFloat(month)))
            {
               alert('Date is incorrect!!');
               Control.value = '';	
		       Control.focus(); 
		       return false;
            }
          if ((parseFloat(month) < parseFloat(1)) || (parseFloat(month) > parseFloat(12))) 
            {
                alert('Date is incorrect!!');
                Control.value = '';	
		        Control.focus(); 
		        return false;
            }
          day = Control.value.substring(0,2);
          if(isNaN(parseFloat(day)))
            {
               alert('Date is incorrect!!');
               Control.value = '';	
		       Control.focus(); 
		       return false;
            }
          if ((parseFloat(day) < parseFloat(1)) || (parseFloat(day) > parseFloat(31)))
            {
               alert('Date is incorrect!!');
               Control.value = '';	
		       Control.focus(); 
		       return false;
		    } 
		  if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0))
            leap = 1;
          if ((month == 2) && (leap == 1) && (day > 29))
            {
                alert('Date is incorrect!!');
                Control.value = '';	
		        Control.focus(); 
		        return false;
            }
          if ((month == 2) && (leap != 1) && (day > 28)) 
            {
                alert('Date is incorrect!!');
                Control.value = '';	
		        Control.focus(); 
		        return false;
            }
          if ((day > 31) && ((month == "01") || (month == "03") || (month == "05") || (month == "07") || (month == "08") || (month == "10") || (month == "12")))
            {
                alert('Date is incorrect!!');
                Control.value = '';	
		        Control.focus(); 
		        return false;
            }
          if ((day > 30) && ((month == "04") || (month == "06") || (month == "09") || (month == "11"))) 
            {
                alert('Date is incorrect!!');
                Control.value = '';	
		        Control.focus(); 
		        return false;
            }
        }  
        else
           {
                alert('Date is incorrect!!');
                Control.value = '';	
	            Control.focus(); 
	            return false;
            }
    }  
   // else
        // is empty 
}
		   
function AddBackSpace(Control)
{
if(!IsEmpty(Control.value))
	{
		if((Control.value.length < 8))
		{
		    alert('Date is incorrect!!');	
		    Control.focus(); 
		    return false; 
		}
		if((Control.value.length == 8))
		{
		    var str1,str2,str3;
			str1 = Control.value.substring(0,2);
			str2 = Control.value.substring(2,4);
			str3 = Control.value.substring(4,8);
			Control.value = str1 + '/' + str2 + '/' + str3;
		} 
	}
}

function validateNumberLength(data,max)
{
	var inputKey =  event.keyCode;
	var val = data.value;
	var returnCode = true;
	
	if (( inputKey > 47 && inputKey < 58 ) || inputKey == 46) // numbers
	{
		val = val + String.fromCharCode( inputKey);
		if (Trim(val )== "")
			Trim(val) = 0;
		if ( max >= parseFloat(MoneyToNumber(val)) )
		{
			return;
		}
		else
		{
			returnCode = false;
			event.keyCode = 0;
		}
	}
	else
	{
			returnCode = false;
			event.keyCode = 0;
	}	
	event.returnValue = returnCode;
	return;
}

function SetCurrency(objNum,max)
{
    var num = objNum.value.replace('$','');
    var ent, dec, dot;
    dec = '';
    dot = '';
    //var thirdNum='';

    if (num != '' && num != objNum.oldvalue)
    {
        num = MoneyToNumber(num);
        if (!isNaN(num))
         {
			var ev = (navigator.appName.indexOf('Netscape') != -1)?Event:event;
			ent = num.split('.')[0];
			dec = num.split('.')[1];
			if (dec || ev.keyCode == 190)
			{
				dot = '.';
				if (isNaN(dec))//.toString() == 'undefined')
					dec = '00';
				else
				{
					if (dec.toString().length > 2)
					{
						//thirdNum = dec.toString().substr(2,3);
						dec = dec.toString().substr(0,3);
					}
				}
			}
			else
			{
				dec = '';
				dot = '';
			}
            if (num <= max)//9999999999)
            {
		        objNum.value = AddCommas(ent) + dot + dec;
				if (objNum.value != 'undefined')
					objNum.oldvalue = objNum.value;
				else
					objNum.oldvalue = '0' + dot + dec;
			}
		}
     // objNum.value = '$' + objNum.oldvalue;
        if (objNum.oldvalue == 'undefined')
     		objNum.oldvalue = '0' + dot + dec;
		objNum.value = objNum.oldvalue;
        if (objNum.value == 'undefined')
     		objNum.value = '0' + dot + dec;
    }
    objNum.value = RoundNum(objNum.value);
}
function validateDateFromTo(idDateFrom,idDateTo)
{
    var objDateFrom = document.getElementById(idDateFrom);
    var objDateTo = document.getElementById(idDateTo);
    if (objDateFrom.value == '')
    {
        alert("Please input DateFrom");
        return false;
    }
    if (objDateTo.value == '')
    {
        alert("Please input DateTo");
        return false;
    }
    
    
    var D_datefrom = objDateFrom.value.split("/");
    var D_dateto = objDateTo.value.split("/");
    var cus_Date_from = Date.parse(D_datefrom[1] + "/" + D_datefrom[0] + "/" + D_datefrom[2]);
    var cus_Date_to = Date.parse(D_dateto[1] + "/" + D_dateto[0] + "/" + D_dateto[2]);
    if (cus_Date_from > cus_Date_to) {
        alert("Incorrect DateFrom");
        return false;
    }
    return true;
}


function MoneyToNumber(num)
{
    return (num.replace(/,/g, ''));
}

function RTrim(String)
{
	var i = 0;
	var j = String.length - 1;

	if (String == null)
		return (false);

	for(j = String.length - 1; j >= 0; j--)
	{
		if (String.substr(j, 1) != ' ' &&
			String.substr(j, 1) != '\t')
		break;
	}

	if (i <= j)
		return (String.substr(i, (j+1)-i));
	else
		return ('');
}

function LTrim(String)
{
	var i = 0;
	var j = String.length - 1;

	if (String == null)
		return (false);

	for (i = 0; i < String.length; i++)
	{
		if (String.substr(i, 1) != ' ' &&
		    String.substr(i, 1) != '\t')
			break;
	}

	if (i <= j)
		return (String.substr(i, (j+1)-i));
	else
		return ('');
}

function Trim(String)
{
	if (String == null)
		return (false);

	return RTrim(LTrim(String));
}

function MoneyToNumber(num)
{
    return (num.replace(/,/g, ''));

}

function getElement(aID)
{
    return window.parent.document.all(aID);
}

function getIFrameDocument(aID){ 
    var rv = null; 
    var frame=getElement(aID);
    if (frame.contentDocument)
        rv = frame.contentDocument;
    else // bad Internet Explorer  ;)
        rv = document.frames[aID].document;
        
    return rv;
}

function adjustMyFrameHeight()
{

    try{
    var frame = getElement("mainfrm");
    if(frame != null)
    {
        alert(window.screen.height - frame.offsetParent.offsetTop);
        var defaultWidth =  (window.screen.height-frame.offsetParent.offsetTop)- 250;
        if((parseInt(frame.contentWindow.document.body.scrollHeight)) < defaultWidth)
        {   
            frame.height = defaultWidth + 100;
        }
        else
        {    
            
            frame.height = parseInt(frame.contentWindow.document.body.scrollHeight) + 100;        
            
            // IE bug fix
            frame.width = parseInt(frame.contentWindow.document.body.scrollWidth);
            
        }
    }
    }catch(err){}
    
}

function raiseEventDateChange(txtDate, btnDate)
{
    var objTxtDate = document.getElementById(txtDate);
    var objBtnDate = document.getElementById(btnDate);
    
    AddBackSpace(objTxtDate);
    if (CheckDate(objTxtDate) == null)
    {
        objBtnDate.click();
    }else{
        objBtnDate.click();
        objTxtDate.focus();
        return ;
    }
}

function SetProgressPosition(e) {

    var posx = 0;
    var posy = 0;
    if (!e) var e = window.event;
    if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY;
    }
    else if (e.clientX || e.clientY) {
        posx = e.clientX + document.documentElement.scrollLeft;
        posy = e.clientY + document.documentElement.scrollTop;
    }
    document.getElementById('divProgress').style.left = posx - 8 + "px";
    document.getElementById('divProgress').style.top = posy - 8 + "px";
}

function AddCommaForCurrency(Control) {
    if (us_IsEmpty(Control.value))
        Control.value = '';
    else {
        var str1
        str1 = Control.value.split('.');
        if (str1.length == '1')
            Control.value = str1 + '.00';
        var result = us_RoundNum(Control.value);
        if (result.length > 0)
            Control.value = result.split('.')[0];
    }
}