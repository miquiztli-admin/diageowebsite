﻿using System;
using System.Collections.Generic;
using System.Text;

using iTextSharp.text;
using iTextSharp.text.pdf;

using System.Data;
using System.IO;
using System.Drawing;

namespace iTextSharp.text.pdf
{
    /// <summary>
    /// Utility to export System.Data.DataTable to a PDF document.
    /// This utility uses iTextSharp (version 4.0.4.0) to create PDF document.
    /// You should add a reference to iTextSharp.dll into your main project.
    /// </summary>
    public class DataTable2PDFDocument
    {
        #region "Public Properties"
        /// <summary>
        /// Gets or sets the background color for the header row. Default value is iTextSharp.text.Color(230, 230, 230).
        /// </summary>
        public Color HeaderBgColor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the background color for table rows. Default value is iTextSharp.text.Color(255, 255, 255).
        /// </summary>
        public Color RowBgColor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the background color for alternate table rows. Default value is iTextSharp.text.Color(255, 255, 255).
        /// </summary>
        public Color AlternateRowBgColor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the color for table border. Default value is iTextSharp.text.Color(192, 192, 192).
        /// </summary>
        public Color BorderColor
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the width for table border. Default value is 1.
        /// </summary>
        public int BorderWidth
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the padding for table cells. Default value is 2.
        /// </summary>
        public int CellPadding
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the spacing for table cells. Default value is 0.
        /// </summary>
        public int CellSpacing
        {
            get;
            set;
        }
        #endregion

        #region "Private Properties"
        //private Document PDFDocument
        //{
        //    get;
        //    set;
        //}
        #endregion

        #region "Constructors & Initializers"
        /// <summary>
        /// Initializes a new instance of iTextSharp.text.pdf.DataTable2PDFDocument class using the docPath.
        /// </summary>
        /// <param name="docPath">Physical path of a empty PDF document.</param>
        //public DataTable2PDFDocument(String docPath)
        //{
        //    Document doc = new Document(new Rectangle(12000, 3000));
        //    PdfWriter.GetInstance(doc, new FileStream(docPath, FileMode.Create));
        //    init(doc);
        //}

        /// <summary>
        /// Initializes a new instance of iTextSharp.text.pdf.DataTable2PDFDocument class using the iTextSharp.text.Document.
        /// </summary>
        /// <param name="doc">iTextSharp.text.Document instance which needs to be populated with data table</param>
        //public DataTable2PDFDocument(Document doc)
        //{
        //    init(doc);
        //}

        //private void init(Document doc)
        //{
        //    PDFDocument = doc;

        //    HeaderBgColor = new Color(230, 230, 230);
        //    BorderColor = new Color(192, 192, 192);
        //    RowBgColor = new Color(255, 255, 255);
        //    AlternateRowBgColor = new Color(255, 255, 255);
        //    BorderWidth = 1;
        //    CellPadding = 2;
        //    CellSpacing = 0;
        //}
        #endregion

        #region "Public Methods"
        /// <summary>
        /// Generates the PDF document with all data available in the DataTable.
        /// </summary>
        /// <param name="dataTable">System.Data.DataTable</param>
        /// <param name="fields">String array for DataTable fields</param>
        /// <returns>iTextSharp.text.Document</returns>
        //public Document GeneratePDF(System.Data.DataTable dataTable, String[] fields)
        //{
        //    return GeneratePDF(dataTable, fields, fields);
        //}

        /// <summary>
        /// Generates the PDF document with all data available in the DataTable.
        /// </summary>
        /// <param name="dataTable">System.Data.DataTable</param>
        /// <param name="fields">String array for DataTable fields</param>
        /// <param name="headers">String array for table header in the PDF document</param>
        /// <returns>iTextSharp.text.Document</returns>
        //public Document GeneratePDF(System.Data.DataTable dataTable, String[] fields, String[] headers)
        //{
        //    PDFDocument.Open();

        //    iTextSharp.text.Table table = new iTextSharp.text.Table(headers.Length);
        //    table.BorderWidth = BorderWidth;
        //    table.BorderColor = BorderColor;

        //    table.Padding = CellPadding;
        //    table.Spacing = CellSpacing;
        //    Cell cell = null;

        //    foreach (String header in headers)
        //    {
        //        cell = new Cell(header);
        //        cell.Header = true;
        //        cell.BackgroundColor = HeaderBgColor;
        //        cell.BorderColor = BorderColor;
        //        cell.HorizontalAlignment = Element.ALIGN_CENTER;
        //        cell.Width = 100;
        //        table.AddCell(cell);
        //    }

        //    int rowIndex = 0;
        //    Color rowBgColor;

        //    foreach (DataRow dr in dataTable.Rows)
        //    {

        //        if ((rowIndex % 2) == 0)
        //        {
        //            rowBgColor = RowBgColor;
        //        }
        //        else
        //        {
        //            rowBgColor = AlternateRowBgColor;
        //        }

        //        foreach (String field in fields)
        //        {
        //            cell = new Cell(dr[field].ToString());
        //            cell.BorderColor = BorderColor;

        //            cell.BackgroundColor = rowBgColor;
        //            table.AddCell(cell);
        //        }

        //        rowIndex++;
        //    }

        //    PDFDocument.Add(table);
        //    PDFDocument.Close();

        //    return PDFDocument;
        //}
        #endregion
    }
}
