- 20131212 requirements for calculate 4A Type had changed by New Media tester 
  from [3 = available, 4 = acceptor] to [4 = available, 3 = acceptor]
	Due to fast update for unit test state, I changed database view to manipulate value before SP and report code
	generate value to display instead of change whole controls to follow new requirements 
	(too much time consume and risk of bugs, may have to retest everything over again).
** Please check this manipulation before commit any change to report code/ SP, or test data.