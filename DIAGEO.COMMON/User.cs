﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace DIAGEO.COMMON
{
    public class User : System.Web.UI.Page
    {
        public string UserID { get { return Session["UserID"].ToString(); } set { Session["UserID"] = value; } }
        public string UserGroupID { get { return Session["UserGroupID"].ToString(); } set { Session["UserGroupID"] = value; } }
        public string UserName { get { return Session["UserName"].ToString(); } set { Session["UserName"] = value; } }
        public string Password { get { return Session["Password"].ToString(); } set { Session["Password"] = value; } }
        public string PasswordNonEncode { get { return Session["PasswordNonEncode"].ToString(); } set { Session["PasswordNonEncode"] = value; } }
        public string Email { get { return Session["Email"].ToString(); } set { Session["Email"] = value; } }
        public string MenuID { get { return Session["MenuID"].ToString(); } set { Session["MenuID"] = value; } }
        public string NewPassword { get { return Session["NewPassword"].ToString(); } set { Session["NewPassword"] = value; } }       
    }
}
