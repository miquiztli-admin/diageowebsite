﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DIAGEO.COMMON
{
    public class ExtractData : System.Web.UI.Page
    {
        public DataTable Platform 
        { 
            get 
            {
                if (Session["Platform"] == null)
                    return null;
                else
                    return Session["Platform"] as DataTable; 
            } 
            set 
            { 
                Session["Platform"] = value; } 
        }

        public DataTable PlatformCallCenter
        {
            get
            {
                if (Session["PlatformCallCenter"] == null)
                    return null;
                else
                    return Session["PlatformCallCenter"] as DataTable;
            }
            set
            {
                Session["PlatformCallCenter"] = value;
            }
        }

        public DataTable OutletRegisterName
        {
            get
            {
                if (Session["OutletRegisterName"] == null)
                    return null;
                else
                    return Session["OutletRegisterName"] as DataTable;
            }
            set
            {
                Session["OutletRegisterName"] = value;
            }
        }

        public DataTable LastVisitOutlet
        {
            get
            {
                if (Session["LastVisitOutlet"] == null)
                    return null;
                else
                    return Session["LastVisitOutlet"] as DataTable;
            }
            set
            {
                Session["LastVisitOutlet"] = value;
            }
        }
    }
}
