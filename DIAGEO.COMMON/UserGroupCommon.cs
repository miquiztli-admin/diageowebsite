﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace DIAGEO.COMMON
{
    public class UserGroupCommon : System.Web.UI.Page
    {
        public string GroupID { get { return Session["GroupID"].ToString(); } set { Session["GroupID"] = value; } }
        public string Name { get { return Session["Name"].ToString(); } set { Session["Name"] = value; } }
    }
}
