﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DIAGEO.COMMON 
{
    public class Outlet : System.Web.UI.Page
    {
        public DataTable OutletData
        {
            get
            {
                if (Session["OutletData"] == null)
                    return null;
                else
                    return Session["OutletData"] as DataTable;
            }
            set
            {
                Session["OutletData"] = value;
            }
        }

    }
}
