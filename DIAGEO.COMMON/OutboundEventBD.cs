﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DIAGEO.COMMON
{
    public class OutboundEventBD : System.Web.UI.Page
    {
        public DataTable PlatformDataForBD
        {
            get
            {
                if (Session["PlatformDataForBD"] == null)
                    return null;
                else
                    return Session["PlatformDataForBD"] as DataTable;
            }
            set
            {
                Session["PlatformDataForBD"] = value;
            }
        }

        public DataTable OutletDataForBD
        {
            get
            {
                if (Session["OutletDataForBD"] == null)
                    return null;
                else
                    return Session["OutletDataForBD"] as DataTable;
            }
            set
            {
                Session["OutletDataForBD"] = value;
            }
        }

        public DataTable ProvinceDataForBD
        {
            get
            {
                if (Session["ProvinceDataForBD"] == null)
                    return null;
                else
                    return Session["ProvinceDataForBD"] as DataTable;
            }
            set
            {
                Session["ProvinceDataForBD"] = value;
            }
        }
    }
}
